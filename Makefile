#
#  Makefile for ESPIA driver and test programs
#
#  make <opts> <cmd>
#    <opts>: [TARGET_KERN=kernel] [QTDIR=qtdir] [COMMON_INCL=common_incl]
#    <cmd>: clean | all | driver | lib | test-progs | install
#             copy2depot | load-module | remove-module | run-test

# include platform/target analysis code

ifndef COMMON_INCL
COMMON_INCL	= $(shell pwd)/common/include
endif # COMMON_INCL

COMMON_INCL_TST	= $(shell test -d $(COMMON_INCL) && echo "OK")
ifneq ($(COMMON_INCL_TST), OK)
$(error Variable COMMON_INCL not defined and could not be guessed)
endif # COMMON_INCL_TST

ifndef COMMON_DIR
COMMON_DIR	= $(COMMON_INCL)/..
endif

include $(COMMON_INCL)/makeplat.inc

ifndef COMMON_LIB
COMMON_LIB	= $(COMMON_DIR)/lib/$(ARCH)
endif

ifndef PYSRC_DIR
PYSRC_DIR=pysrc
endif

DRVNAME		= espia
SECADDEFS	= DRVNAME=$(DRVNAME)
MAKEARGS	= $(SECADDEFS) \
		  COMMON_INCL=$(COMMON_INCL) COMMON_LIB=$(COMMON_LIB)
MAKE_COMMON	= $(MAKE) -C $(COMMON_DIR)
MAKE_CMD	= $(MAKE) -C src $(MAKEARGS)
MAKE_PYSRC	= $(MAKE) -C $(PYSRC_DIR) $(MAKEARGS)

DRVEXT		= ko

DRV_MODULES	= src/$(DRVNAME).$(DRVEXT)
DRV_INCL	= src/scdxipci.h \
		  src/scdxipci_registers.h \
		  src/scdxipci_lib.h \
		  src/espia_lib.h \
		  src/focla_lib.h
DRV_BIN		= src/$(DRVNAME)_test
DRV_LIB		= src/lib$(DRVNAME).so

# targets
.PHONY: all driver lib test-progs clean install

all:		driver lib test-progs $(MAKE_COPYBIN)

driver:
	$(MAKE_CMD) driver

lib:
	$(MAKE_CMD) lib

test-progs:
	$(MAKE_COMMON) all
	$(MAKE_CMD) test-progs

clean:		$(CLEAN_COPYBIN)
	$(MAKE_PYSRC) clean
	$(MAKE_CMD) clean
	$(MAKE_COMMON) clean

load-module:	remove-module
	sudo $(MAKE_CMD) load-module

remove-module:
	sudo $(MAKE_CMD) remove-module

run-test:
	$(MAKE_CMD) run-test

run-test-espia:
	$(MAKE_CMD) run-test-espia

run-test-frelon-serial:
	$(MAKE_CMD) run-test-frelon-serial

ifndef BLISSADM
BLISSADM	= /users/blissadm
endif

BLISSADM_DRV	= $(BLISSADM)/driver/$(OS)

BLISSADM_MOD	= modules/$(ARCH)/$(TARGET_KERN)
BLISSADM_BIN	= bin/$(ARCH)
BLISSADM_LIB	= lib/$(ARCH)

install:	lib test-progs
	cp -f $(DRV_INCL) $(PREFIX)/include
	cp -f $(DRV_LIB) $(PREFIX)/lib
	cp -f $(DRV_BIN) $(PREFIX)/bin

copy2blissadm:
	cp -f $(DRV_MODULES) $(BLISSADM_DRV)/$(BLISSADM_MOD)
	cp -f $(DRV_BIN)     $(BLISSADM_DRV)/$(BLISSADM_BIN)
	cp -f $(DRV_LIB)     $(BLISSADM_DRV)/$(BLISSADM_LIB)
#	$(MAKE_PYSRC) copy2blissadm

BAK_DIR		 = $(shell date "+../backup/espia/%Y-%m-%d-%H%M")
BAK_EXCL	 = --exclude=core\* --exclude=./SECAD_CD \
		   --exclude=./doc/Hardware
backup:
	mkdir -p $(BAK_DIR)
	tar -cf - $(BAK_EXCL) . | tar -C $(BAK_DIR) -xf -
	du -s $(BAK_DIR)

PYTHON_FILES	= __init__.py EspiaAcqLib.py espia_acq_lib.py _espia_acq_lib.so
DEPOT_PYTHON_DIR= $(DEPOT_BASE)/python/$(OS)/Espia

ifeq ($(COPY2DEPOT), YES)
copy2depot:	$(DEPOT_TARGET)

copy2depot_swig:	copy2depot
	cd pysrc && cp $(PYTHON_FILES) $(DEPOT_PYTHON_DIR)
endif

include $(COMMON_INCL)/maketarget.inc
include $(COMMON_INCL)/makecvstarget.inc


swig:
	$(MAKE_PYSRC) swig
