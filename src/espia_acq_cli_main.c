/****************************************************************************
 * File:	espia_test_main.c
 * $Header: /segfs/bliss/cvs/driver/ESRF/espia/src/espia_acq_cli_main.c,v 2.1 2008/10/22 17:04:33 ahoms Exp $
 * Project:	SECAD Espia card Linux driver
 * Description:	Espia library test program main function
 * Author(s):	SECAD S.A.
 *		A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 ****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif /* _GNU_SOURCE */

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/mman.h>

#include "espia_lib.h"
#include "esrfdebug.h"
#include "app_framework.h"
#include "imageapi.h"
#include "focla_lib.h"

#include "espia_acq_lib.h"
#include "espia_acq_cli.h"

// the program name
char *progname;


void usage();


/*--------------------------------------------------------------------------
 * voila the main program
 *--------------------------------------------------------------------------*/

int main(int argc, char **argv)
{
	app_context_t context;
	char *ptr;
	int ret, aux;

	progname = argv[0];
	while ((ptr = strchr(progname, '/')) != NULL)
		progname = ++ptr;

	if (argc > 2)
		usage();

	context = alloc_context("espia_test", argc, argv);
	if (context == CONTEXT_INVALID) {
		fprintf(stderr, "Error allocating context\n");
		return 1;
	}

	ret = cli_test_init(context, argc, argv);
	if (ret != 0)
		goto out;

	ret = exec_application(context);

	aux = cli_test_cleanup(context);
	if ((aux != 0) && (ret == 0))
		ret = aux;

 out:
	if (ret < 0)
		free_context(context);
	return (ret == 0) ? 0 : 1;
}


/*--------------------------------------------------------------------------
 * usage
 *--------------------------------------------------------------------------*/

void usage()
{
	fprintf(stderr, " Usage: %s [dev_nr]\n", progname);
	exit(1);
}


