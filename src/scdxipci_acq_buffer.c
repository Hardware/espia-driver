/****************************************************************************
 * File:	scdxipci_acq_buffer.c
 * $Header: /segfs/bliss/cvs/driver/ESRF/espia/src/scdxipci_acq_buffer.c,v 2.27 2017/11/15 12:19:30 ahoms Exp $
 * Project:	SECAD Espia card Linux driver
 * Description:	Driver acquisition buffer code
 * Author(s):	SECAD S.A.
 *		A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 *
 * $Log: scdxipci_acq_buffer.c,v $
 * Revision 2.27  2017/11/15 12:19:30  ahoms
 * * Ported to gcc-4.9, Linux 3.16 kernels and Qt4
 * * Avoid 64-bit DMA if pci_set_dma_mask(64) fails
 * * Refactored FW capabilities definition, now using capability sets
 * * Improved error checking and reporting in espia_[acq_]lib
 *
 * Revision 2.26  2016/03/07 11:39:37  ahoms
 * * Fixed bug crashing PC when a board with unknown firmware issues an IRQ
 * * Moved KEEP_RELEASED_PAGES compilation flag to acq. driver option
 * * Added GOOD_RESET_SG board capability, introduced in firmware 2014.12.03,
 *   restoring the previously disabled DMA to 64-bit pages functionality
 *
 * Revision 2.25  2015/08/19 18:02:37  ahoms
 * * Changed garbage area allocation from kmalloc(KERNEL) to
 *   scdxipci_frame_page_alloc(BUFF_AUX) to ensure a 32-bit address
 * * Added KEEP_RELEASED_PAGES compilation flag to detect anomally
 *   written pages by Espia-Express after they have been released
 *
 * Revision 2.24  2012/06/15 10:33:35  ahoms
 * * Fixed problems in RedHat EL 4 with 64-bit pages:
 *   + Typo bug in ESRFVER_REMAP_PAGE_32 conditional,
 *     hiding error when remapping user pages
 *   + Added systematic verification of potential problems by
 *     checking num_physpages [SCDXIPCI_ERR_TOOMUCHRAM]
 * * Added Espia-Express 2012.05.22 and 2012.06.13 FW checksums
 * * Fixed EspiaAcqLib.FoclaDevClass.getTriggerMode signature
 *
 * Revision 2.23  2011/10/17 18:40:01  ahoms
 * * Removed compiler warning
 *
 * Revision 2.22  2011/09/30 19:36:18  ahoms
 * * Added user-defined SG table validation checking before
 *   acq. start since it is persistent after frame size change
 *
 * Revision 2.21  2011/05/06 11:43:51  ahoms
 * * Moved SG kernel thread to a high priority tasklet (faster than net)
 * * Map next SG table at end of IRQ hander as last resort to avoid overrun
 * * Increased max. number of acc. IRQs before EOF tasklet to 16
 * * Added kernel flash CRC verification and reporting in board_id info
 * * Added Espia-Express flash board_id reading; included firmware 2011.04.22
 *
 * Revision 2.20  2010/12/21 14:50:37  ahoms
 * * Using SSE2 (ASM) memcpy code from William Chan frame copy to hi-mem
 * * Added EspiaExpress device ID to the supported board list
 *
 * Revision 2.19  2009/02/02 16:02:17  ahoms
 * Always verify that all the active acq. cb threads are already waiting
 * for the first frame (and wait if not) before the [SG] DMA is started.
 *
 * Revision 2.18  2008/09/20 23:02:27  ahoms
 * Fixed bug in scdxipci_find_by_buffer_frame returning internal error;
 * improved parameter checking before sending to wait for the frame.
 *
 * Revision 2.17  2008/05/02 10:47:32  ahoms
 * Added simu_cond_flags module parameter for simulating error conditions.
 * Fixed bug crashing the PC when pci_map_frame was called after stop_acq.
 * Allow eof_tasklet to process up to 3 EOF events before aborting.
 * Pass all the EOF events to memcpy thread if active to
 * warantee sequencial frame order (prev. commit); protect agains abort.
 * Fixed bug creating a dead-lock when buffersave file open failed.
 *
 * Revision 2.16  2008/04/28 13:50:00  ahoms
 * Moved SG table mapping to dedicated thread.
 * Increased the number of SG tables per devices to 5,
 * and decreased the number of aux. frames also to 5.
 * Added SG table availability to acq. statistics.
 * Lock buffers used by the memcpy or SG threads so
 * they are not freed until the processing finishes.
 * Corrected bug in scdxipci_frame_copy kunmap parameters.
 * Using string buffer when generating EDF header espia_test.
 *
 * Revision 2.15  2008/04/16 07:46:39  ahoms
 * Added focla_open/close and serial_read/write/flush;
 * secured all Focla operations with a (recursive) mutex.
 * Implemented FOCLA CL CC pulses as a dedicated thread.
 * Allow retrieval of auto_save information with buffersave;
 * protected auto_save internal information with a mutex.
 * Fixed bug crashing the PC when using all the buffers kernel table.
 * Empty the EOF list before starting a new acquisition.
 *
 * Revision 2.14  2008/03/26 15:11:59  ahoms
 * Implemented aux. frame memory copy in a kernel thread.
 * Increased number of aux. buffers to 10.
 * Buffer pointer array is now vmalloc'ed.
 * Changed acq. name to "espia_%d_acq" instead of "Acq. %p".
 *
 * Revision 2.13  2008/03/18 08:50:28  ahoms
 * Moved next frame SG table preparation from IRQ handler to EOF tasklet.
 * Limited to one the number of frames handled in EOF tasklet.
 * Corrected file names in serial_mmap_close [MP].
 *
 * Revision 2.12  2008/03/02 17:32:21  ahoms
 * Added support for driver options; implemented DEBUG_LEVEL and NO_FIFO_RESET.
 * Fixed bug crashing PC on redhate5 when SG is active without a garbage region.
 * Dev. SG lists for PCI frame mapping are now vmalloc'ed at driver startup.
 * Added cal. time, time of day, and time of frame in EDF header (espia_test)
 * Disable IRQs during memcopy & lock acq. when updating stats. in EOF tasklet
 * scdxipci_dma_align must now be called with dev. locked.
 * Check specified devices to set_dev before allocating acq_dev array.
 *
 * Revision 2.11  2008/02/12 11:31:32  ahoms
 * Increased the maximum SG table length to 128 KB (16 K desc, 64 MB frames)
 * Added SG table overflow checking.
 * Using vmalloc for user defined SG tables.
 *
 * Revision 2.10  2008/02/08 14:20:41  ahoms
 * Ported to x86_64 architecture.
 * Implemented multiple SG tables for low frame switch overhead,
 * next frame is mapped just after the DMA is started.
 * Input data FIFO is not erased on start from second frame on.
 *
 * Revision 2.9  2007/11/01 21:25:43  ahoms
 * Corrected ROI handling in acq_set_sg; always unmap the current
 * the current frame if device is not busy in another acquisition.
 * Added static RCS Id to scdxipci_file_ops and scdxipci_acq_buffer.
 *
 * Revision 2.8  2007/10/23 15:25:18  ahoms
 * Ported to Linux 2.6.18; using class_device instead of class_simple.
 * Unified code with Linux 2.4 through esrfversion.h.
 * Changed scdxipci_private.h macros with static inline functions.
 * Espia and FOCLA parameter tables are no longer exported (for future
 * binary compatibility), they can be browsed with get_param_data.
 * Updated espia parameters: DO_NOT_RESET_FIFOS and ESPIA_125MHZ.
 * Added TRIG and TEST_IMAGE FOCLA parameters; implemented focla_strerror.
 * Implemented retrieval of card ID and firmware data.
 * Using generic image interface in espia_test.
 * Improved scdxipci_find_by_buffer_frame code.
 *
 * Revision 2.7  2007/08/23 17:20:40  ahoms
 * Ported to Linux 2.6: using pci_name/enable_device/set_master;
 * using "espia%d" for resource allocation name; not setting PCI dev. name;
 * implemented sysfs class_simple devices; serial Tx uses kthread.
 * [Temporarly] disabled "very high vemory" (64-bit) buffers.
 * Adapted to new circ_buffer_getc/putc flags (CIRC_BUFFER_KTHREAD).
 * Moved app_framework, circ_buffer and image to driver common.
 * Making proper dev. initialization sequence.
 * Improved buffer array management so it does not have empty entries;
 * using long long integer arithmetic instead of double in get_frame/AFN.
 * Moved focla_param to espia_param, implemented basic Espia parameters.
 * Changed write_register to accept the mask of bits to modify;
 * verify the proper register offset alignment.
 * Renamed focla_lib functions to follow espia_lib style.
 * Fixed several espia_test bugs.
 *
 * Revision 2.6  2007/07/10 19:09:59  ahoms
 * Remove acknowledgement potential serial IRQs on every FIFO read
 * (big overhead); temporally disable interrupts instead.
 * Check if serial FIFO becomes full and warn a message.
 * Fixed SCDXIPCI_IOC_WRITE_REGISTER code to return the original reg. value.
 *
 * Revision 2.5  2007/07/06 14:47:49  ahoms
 * Simplified cb thread termination: removed
 * synchronization and cancelation (bug in SuSE 8.2 pthread)
 *
 * Revision 2.4  2007/07/03 14:08:45  ahoms
 * Implemented Espia RoI through SG lists: xfers
 * with invalid frame addr. are dump into a garbage buffer.
 * Check SG desc. are properly aligned if using 64-bit xfers.
 * Let RESET_SCATTER_GATHER active when aborting an acq. so
 * next frame from CCD is aborted immediately.
 *
 * Revision 2.3  2007/04/25 16:50:52  ahoms
 * Support LARGEFILE (> 2GB) in espia_test program
 *
 * Revision 2.2  2007/02/20 17:22:10  ahoms
 * Fixed acq_dev locking with a 5 usec delay before reacquiring lock.
 * Protect driver get_frame when freeing (loosing) buffers.
 * Using a new optimized algorithm in acq_frame_nr search.
 * Added acq_frame_nr info to acq. stats; incl. cb_nr in espia_cb_data.
 * Implemented parallel port debug, including heartbeat timer.
 * Using new app_framework context interface (alloc, init, err/out).
 * Improved error checking and report. in driver and espia_test.
 * In espia_test:
 *   Centralized callback management
 *   Implemented RoI Accumulation buffer
 *   Allow saving multiple acq. frames in a single file
 *   Acq. status shows last acq/saved frame from callbacks
 *
 * Revision 2.1  2007/01/13 12:30:00  ahoms
 * Added user-specified scatter gather lists.
 * Implemented meta device (minor=255), controlling
 * acquisitions on one or more real devices.
 * Acquisition lock independent of devices.
 * Dev. cmds (ser-line, reg. R/W, etc.) forbidden in meta acq.
 * Completed pci_unmap_frame by clearing dev. sg-list;
 * clear the sg-list pointer register on driver exit.
 * End-of-frame treatment (including memcpy if necessary)
 * is done in tasklet (IRQ enabled).
 * Increased to infitity nr. of retries when
 * allocating 32-bit only frame pages.
 * Implemented vert. flip sg-list on 1 and 2 adapters.
 * Added espia_frame_dim; always passed to frame post-op.
 *
 * Revision 2.0  2006/12/13 17:43:19  ahoms
 * Merged release 1.99.1.14 into main trunk
 *
 * Revision 1.99.1.14  2006/12/01 15:55:15  ahoms
 * Corrected serial read timeout to expire when the user requested.
 * Prepared term. search in drv. serial read: not implemented yet in circ_buffer
 * Avoid extra serial interrupts by acknowledging after every read.
 * Added espia_read/write/register; corrected reg. checking in driver.
 * Added frame post-op functionality with buffer-based thread protection;
 * implemented interlaced pixel swap, vert. flip and check for NULL lines.
 * Added buffer clear, auto-save and live-image functionalities to espia_test.
 * Modify kernel mapping of 64-bit frame pages to user space.
 * Set the time in the frame notifying that acq. finished.
 * Split espia_test to separate main from test routines (for Python wrapper).
 * Check for NULL pointers in parameters to espia_lib functions.
 * Avoid registering acq. callbacks when an acq. is running.
 * Using thread synchronization when terminating or finishing callback threads.
 * Fixed bug of EACH_BUFFER callback aborting when acq. finished.
 * Implemented proper clean-up in espia_close;
 * do the same in espia_test close_dev.
 * Using __attribute__ unused for RCS ID strings.
 *
 * Revision 1.99.1.13  2006/10/23 13:33:35  ahoms
 * Added support of 64-bit memory pages, not directly accessible by the card;
 * using temporary aux. kernel buffer when frames contain such pages.
 * Implemented acquisition statistics, to be read by the user.
 * Free already allocated buffers when buffer allocation fails.
 * Fixed bug when waiting for cb. thread that already finished.
 * Added buffer dump and save in espia test program.
 *
 * Revision 1.99.1.12  2006/10/03 17:54:36  ahoms
 * Added FOCLA interface library
 *
 * Revision 1.99.1.11  2006/10/03 09:50:37  ahoms
 * Both driver and library check for the same version string in dev. open.
 * Access buffers in kernel from an array instead of a linked list;
 * all user space layers specify buffers by their nr and not their addr.
 * Removed espia_frame_info, moved buffer_info and frame_address to driver.
 * Library now specifies the frame_size instead of the frames_per_buffer.
 * Cancel acq. threads only outside callback execution.
 * Added Qt-based buffer display with efficient thread mechanism.
 * Using standard wait_event_interruptible_timeout return code.
 *
 * Revision 1.99.1.10  2006/06/07 09:22:39  ahoms
 * Included new Espia firmware (05 May 2006): new subsystem vendor/dev ID.
 * Activate the PCI burst transfer mode in espia_open.
 *
 * Revision 1.99.1.9  2006/04/13 16:56:14  ahoms
 * Changed espia_ser_read_str to wait only for first terminator
 *
 * Revision 1.99.1.8  2005/12/09 15:15:58  ahoms
 * Fixed bug in circ_buffer misinterpreting character 0xff.
 * Increased kernel serial buffers to 128KB (one PRIAM config. block).
 * Corrected debug message in espia_ser_write.
 *
 * Revision 1.99.1.7  2005/10/25 19:49:14  ahoms
 * Added CCD status and serial Rx callback support.
 * Return timeout as a (negative) error.
 * Removed flags from espia_start_acq.
 *
 * Revision 1.99.1.6  2005/09/20 14:58:22  ahoms
 * Atomic register read/write from user space.
 * Improved serial line blocking and timing; added block xfer.
 * Implemented DMA overrun notification in callbacks.
 * Added CCD status with blocking capabilities.
 *
 * Revision 1.99.1.5  2005/08/28 22:09:26  ahoms
 * Added DMA acq. active call; introduced the acq. run number
 * to solve the problem of wait requests between acquisitions.
 * Implemented espia_ser_read_str and espia_frame_address.
 * Using thread mutex & cond. to activate/deactivate callbacks.
 * Implemented ESPIA_EACH_BUFFER/FRAME in callbacks.
 *
 * Revision 1.99.1.4  2005/08/23 02:50:42  ahoms
 * Implemented buffer and acquisition functions in espia_lib
 *
 * Revision 1.99.1.3  2005/08/21 23:45:33  ahoms
 * Implemented image acquisition and (flexible, blocking) get frame.
 *
 * Revision 1.99.1.2  2005/08/17 23:24:54  ahoms
 * Implemented buffer memory mapping with multiple frames (big buffer).
 * Using linked lists and lookaside caches for optimum memory use.
 *
 * Revision 1.99.1.1  2005/08/15 21:35:04  ahoms
 * Using new circ_buffer features; Tx kernel thread always alive.
 * Improved timeout and interrupt control.
 * Renamed scdxipci_dev field minor by dev_nr; open possibility of
 * having several minors per device.
 *
 * Revision 1.99  2005/08/12 15:05:46  ahoms
 * Rewritten driver from scratch. Using kernel PCI driver interface.
 * Implemented a generic menu-driver test application framework.
 * Implemented serial line with circular buffers and TX thread.
 *
 ****************************************************************************/

#include "scdxipci_private.h"

#include <linux/module.h>
#include <linux/highmem.h>
#include <linux/interrupt.h>
#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,11,0)
#include <uapi/linux/sched.h>
#include <uapi/linux/sched/types.h>
#endif

/*--------------------------------------------------------------------------
 * RCS Id 
 *--------------------------------------------------------------------------*/

static char rcs_id[] __attribute__((unused)) = 
	"$Header: /segfs/bliss/cvs/driver/ESRF/espia/src/scdxipci_acq_buffer.c,v 2.27 2017/11/15 12:19:30 ahoms Exp $";


/*--------------------------------------------------------------------------
 * Virtual memory area operations
 *--------------------------------------------------------------------------*/

struct vm_operations_struct scdxipci_vm_operations = {
	open:		scdxipci_vm_open,
	close:		scdxipci_vm_close,
};


/*--------------------------------------------------------------------------
 * Acquisiton alloc/free
 *--------------------------------------------------------------------------*/

struct data_acq *scdxipci_acq_alloc(int minor)
{
	struct data_acq *acq;
	int len, meta_acq = (minor == SCDXIPCI_META_DEV);
	char buffer[16] = "meta";

	FENTRY("scdxipci_acq_alloc");
	// create the acquisition associated to this file
	len = sizeof(struct data_acq);
	acq = (struct data_acq *) kmalloc(len, GFP_KERNEL);
	if (acq == NULL) {
		DEB_ERRORS("Error allocating acquisition structure "
			   "(%d bytes)\n", len);
		return NULL;
	}
	memset(acq, 0, len);

	spin_lock_init(&acq->lock);
	INIT_LIST_HEAD(&acq->wait_list);
	INIT_LIST_HEAD(&acq->eof_data_list);
	INIT_LIST_HEAD(&acq->memcpy_data_list);
	init_waitqueue_head(&acq->wait_wq);
	init_waitqueue_head(&acq->memcpy_wq);
	tasklet_init(&acq->eof_task, scdxipci_acq_eof_tasklet, 
		     (unsigned long) acq);
	acq_frame_nr_init(acq);

	acq->is_meta = meta_acq;
	if (!meta_acq)
		sprintf(buffer, "%d", minor);
	sprintf(acq->name, "espia%s", buffer);

	if (scdxipci_acq_frame_size(acq, PAGE_SIZE) < 0) {
		scdxipci_acq_free(acq);
		return NULL;
	}

	return acq;
}

void scdxipci_acq_free(struct data_acq *acq)
{
	struct img_buffer *buffer;
	unsigned long buffer_nr;
	struct scdxipci_stats *stats = &acq->stats;

	FENTRY("scdxipci_acq_free");
	DEB_PARAMS("Freeing %s\n", acq->name);

	scdxipci_acq_stop(acq);

	// all buffers must be freed already ... but check
	if (acq->buffer_arr) {
		acq_for_each_good_buffer(buffer_nr, buffer, acq) {
			DEB_ERRORS("Warning!! %s still in %s! Freeing...\n", 
				   buffer->name, acq->name);
			scdxipci_buffer_free(buffer);
		}
		vfree(acq->buffer_arr);
	}

	stats = &acq->stats;
	if (stats->nr_pages || stats->nr_pages64) {
		DEB_ERRORS("Warning!! Unbalanced nr. of pages: "
			   "nr_pages=%ld, nr_pages64=%ld\n", 
			   stats->nr_pages, stats->nr_pages64);
	}

	if (acq->aux_buffer)
		scdxipci_buffer_free(acq->aux_buffer);

	if (acq->nr_dev > 0)
		scdxipci_acq_dev_free(acq->dev, acq->nr_dev);

	if (acq->garbage_len > 0)
		scdxipci_frame_page_free(acq->garbage_page);

	scdxipci_check_kept_pages(acq);

	kfree(acq);
}


/*--------------------------------------------------------------------------
 * Acq. dev alloc/free
 *--------------------------------------------------------------------------*/

struct acq_dev *scdxipci_acq_dev_alloc(struct data_acq *acq, 
				       unsigned long nr_dev)
{
	unsigned long len, i;
	struct acq_dev *acq_dev_arr, *acq_dev;

	FENTRY("scdxipci_acq_dev_alloc");

	len = sizeof(struct acq_dev) * nr_dev;
	acq_dev_arr = (struct acq_dev *) kmalloc(len, GFP_KERNEL);
	if (acq_dev_arr == NULL) {
		DEB_ERRORS("Error allocating %ld acq. dev structure(s) "
			   "(%ld bytes)\n", nr_dev, len);
		return NULL;
	}
	memset(acq_dev_arr, 0, len);

	acq_dev = acq_dev_arr;
	for (i = 0; i < nr_dev; i++, acq_dev++) {
		acq_dev->acq = acq;
		acq_dev->idx = i;
		tasklet_init(&acq_dev->sg_task, scdxipci_acq_dev_sg_tasklet,
			     (unsigned long) acq_dev);
	}

	return acq_dev_arr;
}

void scdxipci_acq_dev_free(struct acq_dev *acq_dev_arr, unsigned long nr_dev)
{
	struct acq_dev *acq_dev = acq_dev_arr;
	unsigned long i;

	for (i = 0; i < nr_dev; i++, acq_dev++) {
		if (acq_dev->sg_table.len > 0)
			vfree(acq_dev->sg_table.ptr);
	}

	kfree(acq_dev_arr);
}


/*--------------------------------------------------------------------------
 * Acq. frame size
 * Set the size of each frame (if frame_size != -1). 
 * return previous value
 *--------------------------------------------------------------------------*/

int scdxipci_acq_frame_size(struct data_acq *acq, unsigned long frame_size)
{
	unsigned long flags, nr_pages, prev_size, i;
	struct img_buffer *buffer, *old_buffer;
	int ret;

	FENTRY("scdxipci_acq_frame_size");

	scdxipci_lock_acq(acq, flags);

	prev_size = acq->default_frame_size;
	ret = prev_size;

	DEB_PARAMS("frame_size=%ld, prev_frame_size=%ld\n", 
		   frame_size, prev_size);
	if (is_invalid(frame_size))
		goto unlock;

	nr_pages = PAGE_ALIGN(frame_size) / PAGE_SIZE;
	if ((frame_size == 0) || (nr_pages > get_num_physpages())) {
		DEB_ERRORS("Invalid frame size: %ld\n", frame_size);
		ret = -EINVAL;
		goto unlock;
	}

	old_buffer = acq->aux_buffer;
	if (frame_size != prev_size) {
		if (is_current_acq(acq)) {
			DEB_ERRORS("Acquisition is running!\n");
			ret = SCDXIPCI_ERR_RUNNING;
			goto unlock;
		}

		for (i = 0; i < acq->nr_dev; i++)
			acq->dev[i].sg_table_validated = 0;

		acq->aux_buffer = NULL;
		acq->default_frame_size = 0;
	}

	if (acq->aux_buffer != NULL)
		goto unlock;

	scdxipci_unlock_acq(acq, flags);

	if (old_buffer != NULL)
		scdxipci_buffer_free(old_buffer);

	buffer = scdxipci_buffer_alloc(acq, SCDXIPCI_BUFF_AUX, 
				       SCDXIPCI_NR_AUX_FRAMES, nr_pages, NULL);
	if (buffer != NULL) {
		scdxipci_buffer_set_name(buffer, "Aux. buffer");
		acq->default_frame_size = frame_size;
	} else {
		DEB_ERRORS("Could not allocate 32-bit only aux. buffer\n");
		ret = SCDXIPCI_ERR_AUXBUFF;
	}
	scdxipci_lock_acq(acq, flags);

	acq->aux_buffer = buffer;

 unlock:
	scdxipci_unlock_acq(acq, flags);
	
	return ret;
}


/*--------------------------------------------------------------------------
 * Buffer alloc/free/set_name
 *--------------------------------------------------------------------------*/

struct img_buffer *scdxipci_buffer_alloc(struct data_acq *acq, int buffer_type,
					 int nr_frames, 
					 unsigned long frame_pages, 
					 struct vm_area_struct *vma)
{
	struct img_buffer *buffer;
	struct img_frame *frame;
	struct list_head *list, *aux, bad_frame_list;
	unsigned long vaddr;
	int i, bad_frames, ok = 0; 
	void *ptr;

	FENTRY("scdxipci_buffer_alloc");

#ifdef ESRFVER_REMAP_PAGE_32
	if (system_has_page64()) {
		DEB_ERRORS("num_physpages=%lx\n", get_num_physpages());
		DEB_ERRORS("System has 64-bit pages that cannot be "
			   "remaped by kernel!\n");
		DEB_ERRORS("Reduce kernel RAM with \"mem=0xffffffff\" "
			   "boot option\n");
		if (buffer_type == SCDXIPCI_BUFF_NORM) {
			acq->error = SCDXIPCI_ERR_TOOMUCHRAM;
			return NULL;
		}
	}
#endif

	acq->error = -ENOMEM;

	if ((nr_frames == 0) || (frame_pages == 0)) {
		DEB_ERRORS("invalid values: nr_frames=%d, frame_pages=%ld\n",
			   nr_frames, frame_pages);
		acq->error = -EINVAL;
		return NULL;
	}

	ptr = kmem_cache_alloc(buffer_cache, GFP_KERNEL);
	if (ptr == NULL) {
		DEB_ERRORS("Failed to allocate buffer structure (%ld bytes)\n",
			   (long) sizeof(*buffer));
		return NULL;
	}
	buffer = (struct img_buffer *) ptr;
	memset(buffer, 0, sizeof(*buffer));

	buffer->acq = acq;
	buffer->nr = SCDXIPCI_INVALID;
	INIT_LIST_HEAD(&buffer->frame_list);
	atomic_set(&buffer->open_count, 0);
	atomic_set(&buffer->use_count, 0);
	init_waitqueue_head(&buffer->use_wq);

	INIT_LIST_HEAD(&bad_frame_list);

	vaddr = vma ? vma->vm_start : 0;
	for (i = 0; i < nr_frames; i++) {
		for (bad_frames = 0; 1; bad_frames++) {
			frame = scdxipci_frame_alloc(buffer_type, frame_pages,
						     vma, vaddr);
			ok = (frame && !(SCDXIPCI_BUFF_PAGE_32 && 
					 img_frame_has_page64(frame)));
			if (ok || !frame)
				break;
			list_add_tail(&frame->frame_list, &bad_frame_list);
		}
		if (!ok) {
			DEB_ERRORS("error: Could not allocate 32-bit frame "
				   "(%ld pages) after %d retries\n", 
				   frame_pages, bad_frames);
			break;
		}

		frame->buffer = buffer;
		frame->frame_nr = i;
		list_add_tail(&frame->frame_list, &buffer->frame_list);

		vaddr += frame_pages * PAGE_SIZE;
	}

	list_for_each_safe(list, aux, &bad_frame_list) {
		frame = img_frame_from_list(list);
		scdxipci_frame_free(frame);
	}

	if (!ok)
		goto error;

	if (vma) {
		if (scdxipci_buffer_map(buffer, vma) != 0)
			goto error;
		buffer->vma = vma;
	}

	return buffer;

 error:
	scdxipci_buffer_free(buffer);
	return NULL;
}

void scdxipci_buffer_free(struct img_buffer *buffer)
{
	struct data_acq *acq = buffer->acq;
	struct list_head *list, *aux;
	struct img_frame *frame;
	unsigned long nr_pages;

	FENTRY("scdxipci_buffer_free");
	DEB_PARAMS("Freeing %s\n", buffer->name);

	scdxipci_acq_stop(acq);
	
	wait_event_interruptible(buffer->use_wq, !img_buffer_is_used(buffer));

	if (!is_invalid(buffer->nr))
		scdxipci_buffer_remove(acq, buffer);

	if (buffer->vma) {  
		// all its pages were mapped
		img_buffer_pages(buffer, &nr_pages, NULL); 
		scdxipci_buffer_unmap(buffer, buffer->vma, nr_pages);
	}

	list_for_each_safe(list, aux, &buffer->frame_list) {
		frame = img_frame_from_list(list);
		scdxipci_frame_free(frame);
	}

	kmem_cache_free(buffer_cache, buffer);
}


void scdxipci_buffer_set_name(struct img_buffer *buffer, char *name)
{
	FENTRY("scdxipci_buffer_set_name");

	strcpy(buffer->name, name);
	DEB_PARAMS("Allocated %s for %s, nr_frames=%ld, frame_pages=%ld\n", 
		   buffer->name, buffer->acq->name, img_buffer_frames(buffer),
		   img_frame_pages(img_buffer_first_frame(buffer)));
}

/*--------------------------------------------------------------------------
 * Buffer add/remove
 *--------------------------------------------------------------------------*/

int scdxipci_buffer_add(struct data_acq *acq, struct img_buffer *buffer)
{
	unsigned long flags, buffer_nr, len, oldlen, nr_pages, nr_pages64;
	void *arr;
	int ret = SCDXIPCI_OK;

	FENTRY("scdxipci_buffer_add");

	img_buffer_pages(buffer, &nr_pages, &nr_pages64);

	scdxipci_lock_acq(acq, flags);
	buffer_nr = acq_first_buffer_nr(acq, SCDXIPCI_UNUSED);
	if (is_invalid(buffer_nr)) {
		scdxipci_unlock_acq(acq, flags);

		// allocate bigger array (PAGE_SIZE granularity)
		oldlen = acq->buffer_arr_len;
		len = oldlen + PAGE_SIZE;
		arr = vmalloc(len);
		if (arr == NULL) {
			DEB_ERRORS("error allocating buffer list (%ld bytes)\n",
				   len);
			return -ENOMEM;
		}
		memset(arr, 0, len);

		scdxipci_lock_acq(acq, flags);
		if (oldlen > 0) {
			memcpy(arr, acq->buffer_arr, oldlen);
			vfree(acq->buffer_arr);
		}

		acq->buffer_arr = (struct img_buffer **) arr;
		acq->buffer_arr_len = len;
		DEB_PARAMS("%sallocated %s buffer array with %ld bytes\n",
			   oldlen ? "re" : "", acq->name, len);

		buffer_nr = acq_first_buffer_nr(acq, SCDXIPCI_UNUSED);
	}
	buffer->nr = buffer_nr;
	acq->buffer_arr[buffer_nr] = buffer;
	if (buffer_nr >= acq->nr_buffers)
		acq->nr_buffers = buffer_nr + 1;

	// if first buffer, set curr_frame to a valid value
	if (acq->curr_frame == NULL)
		acq->curr_frame = img_buffer_first_frame(buffer);

	acq->stats.nr_pages   += nr_pages;
	acq->stats.nr_pages64 += nr_pages64;

	scdxipci_unlock_acq(acq, flags);

	if (nr_pages64 > 0) {
		ret = scdxipci_memcpy_thread_start(acq);
		if (ret < 0) 
			DEB_ERRORS("Error starting memcpy thread!\n");
	}

	return ret;
}

void scdxipci_buffer_remove(struct data_acq *acq, struct img_buffer *buffer)
{
	struct img_buffer *cbuffer;
	struct img_frame *frame;
	unsigned long flags, nr_pages, nr_pages64, buffer_nr, nr_buffers;
	int stop_memcpy_thread = 0;

	FENTRY("scdxipci_buffer_remove");

	img_buffer_pages(buffer, &nr_pages, &nr_pages64);

	scdxipci_lock_acq(acq, flags);
	if (acq_buffer(acq, buffer->nr) != buffer) {
		DEB_ERRORS("error: buffer %s is NOT #%ld in %s\n",
			   buffer->name, buffer->nr, acq->name);
		goto unlock;
	}

	acq->stats.nr_pages   -= nr_pages;
	acq->stats.nr_pages64 -= nr_pages64;

	// check current frame will remain valid
	acq->buffer_arr[buffer->nr] = NULL;
	frame = acq->curr_frame;
	if (frame && (frame->buffer == buffer)) {
		cbuffer = acq_first_buffer(acq);
		frame = NULL;
		if (cbuffer != NULL)
			frame = img_buffer_first_frame(cbuffer);
		acq->curr_frame = frame; 
		acq_frame_nr_init(acq);
		acq->tot_frames = 1;
	}

	// update nr of buffers
	nr_buffers = 0;
	for (buffer_nr = 0; buffer_nr < acq->nr_buffers; buffer_nr++)
		if (acq->buffer_arr[buffer_nr] != NULL)
			nr_buffers = buffer_nr + 1;
	acq->nr_buffers = nr_buffers;
	
	if (acq->nr_buffers > 0) {
		// loosing buffers: notify until completely empty
		acq->removed_buffers++;
	} else {
		// ok, no more buffers
		acq->removed_buffers = 0;
		stop_memcpy_thread = 1;
	}

 unlock:
	scdxipci_unlock_acq(acq, flags);

	if (stop_memcpy_thread)
		scdxipci_memcpy_thread_stop(acq);
}


/*--------------------------------------------------------------------------
 * Frame alloc/free
 *--------------------------------------------------------------------------*/

struct img_frame *scdxipci_frame_alloc(int buffer_type,
				       int frame_pages, 
				       struct vm_area_struct *vma,
				       unsigned long vaddr)
{
	struct img_frame *frame;
	struct img_frame_page *fpage, **pfpage;
	void *ptr;
	int i;
	gfp_t gfp_flags;

	FENTRY("scdxipci_frame_alloc");

	ptr = kmem_cache_alloc(frame_cache, GFP_KERNEL);
	if (ptr == NULL) {
		DEB_ERRORS("Failed to allocate frame structure (%ld bytes)\n", 
			   (long) sizeof(*frame));
		return NULL;
	}
	frame = (struct img_frame *) ptr;
	memset(frame, 0, sizeof(*frame));
	INIT_LIST_HEAD(&frame->frame_list);
	frame->frame_nr = SCDXIPCI_INVALID;
	img_frame_init(frame);

	gfp_flags = img_frame_gfp_flags(buffer_type);
	pfpage = &frame->first_page;
	for (i = 0; i < frame_pages; i++, frame->nr_pages++) {
		fpage = scdxipci_frame_page_alloc(gfp_flags, vma, vaddr);
		if (fpage == NULL) {
			scdxipci_frame_free(frame);
			return NULL;
		}
		if (img_frame_page_is64(fpage))
			frame->nr_pages64++;

		*pfpage = fpage;
		pfpage = &fpage->next;

		vaddr += PAGE_SIZE;
	}

	return frame;
}

void scdxipci_frame_free(struct img_frame *frame)
{
	struct img_buffer *buffer = frame->buffer;
	struct data_acq *acq = buffer ? buffer->acq : NULL;
	struct scdxipci_dev *scd_dev;
	struct img_frame_page *fpage, **pfpage;
	int nr_pages;
	unsigned long acq_flags, dev_flags, i;

	FENTRY("scdxipci_frame_free");

	list_del(&frame->frame_list);

	if (acq) {
		scdxipci_lock_acq(acq, acq_flags);
		for (i = 0; i < acq->nr_dev; i++) {
			scd_dev = acq->dev[i].dev;
			scdxipci_lock_dev(scd_dev, dev_flags);
			scdxipci_unmap_unavailable_frame(scd_dev, frame);
			scdxipci_unlock_dev(scd_dev, dev_flags);
		}
		scdxipci_unlock_acq(acq, acq_flags);
	}

	nr_pages = img_frame_pages(frame);
	for (pfpage = &frame->first_page; (fpage = *pfpage); nr_pages--) {
		*pfpage = fpage->next;
		if (acq && acq->keep_released_pages) {
			void *p;
			if (!acq->deleted_frame_pages)
				acq->deleted_frame_pages = fpage;
			else
				acq->last_deleted_frame_page->next = fpage;
			acq->last_deleted_frame_page = fpage;
			fpage->next = NULL;
			acq->nr_deleted_pages++;
			p = kmap(fpage->page);
			memset(p, SCDXIPCI_RELEASED_PAGE_MARKER, PAGE_SIZE);
			kunmap(fpage->page);
		} else {
			scdxipci_frame_page_free(fpage);
		}
	}
	if (nr_pages != 0)
		DEB_ERRORS("Inconsistent frame nr_pages: %d\n", nr_pages);

	kmem_cache_free(frame_cache, frame);
}


/*--------------------------------------------------------------------------
 * End-Of-Frame update
 *--------------------------------------------------------------------------*/

void scdxipci_acq_eof_tasklet(unsigned long data)
{
	struct data_acq *acq = (struct data_acq *) data;
	struct eof_data *eof_data = NULL;
	struct list_head *list;
	unsigned long flags;
	int pass_to_memcpy, ret, pass;

	FENTRY("scdxipci_acq_eof_tasklet");

	pass_to_memcpy = ret = 0;

	scdxipci_lock_acq(acq, flags);
	PPDEB_EOF_SET(PPDEB_EOF_LOCK_ACQ);

	// if no EOF data -> ISR error -> abort acq.
	if (list_empty(&acq->eof_data_list)) {
		DEB_ERRORS("Empty %s EOF list: aborting\n", acq->name);
		ret = SCDXIPCI_ERR_INTERNAL;
		goto unlock;
	}

	for (pass = 0; !list_empty(&acq->eof_data_list); pass++) {
		if (pass >= SCDXIPCI_MAX_EOF_TSKT_RETRIES) {
			DEB_ERRORS("Too many EOF in %s list\n", acq->name);
			ret = SCDXIPCI_ERR_INTERNAL;
			goto unlock;
		} else if (simu_cond(EOFT_OVERRUN)) {
			DEB_ERRORS("Simulating %s EOF tasklet overrun\n", 
				   acq->name);
			ret = SCDXIPCI_ERR_INTERNAL;
			goto unlock;
		}

		list = acq->eof_data_list.next;
		list_del(list);
		eof_data = list_entry(list, struct eof_data, list);

		// if memcpy thread is active, pass all the EOFs through
		pass_to_memcpy = scdxipci_memcpy_thread_active(acq);
		if (pass_to_memcpy) {
			list_add_tail(list, &acq->memcpy_data_list);
			wake_up_interruptible(&acq->memcpy_wq);
		} else {
			// should have been active?
			if (eof_data->ready_frame != eof_data->mapped_frame) {
				DEB_ERRORS("Must copy frame but thread not "
					   "active!\n");
				ret = SCDXIPCI_ERR_INTERNAL;
			}
			scdxipci_frame_finish(acq, eof_data);
			eof_data = NULL;
		}
	}

 unlock:	
	if ((ret < 0) && (eof_data != NULL))
		eof_data->error = ret;

	scdxipci_unlock_acq(acq, flags);
	PPDEB_EOF_SET(PPDEB_EOF_OUT);

	if ((ret < 0) && !pass_to_memcpy)
		scdxipci_acq_stop(acq);

	return;
}

/*--------------------------------------------------------------------------
 * End-Of-Frame cleanup
 *
 * This will treat all unhandled EOF data structures in the tasklet
 * and memcpy lists. It will return if the last EOF indicating end
 * of acquisition was treated
 *--------------------------------------------------------------------------*/

int scdxipci_acq_eof_cleanup(struct data_acq *acq)
{
	struct list_head *list, *aux_list;
	struct eof_data *eof_data;
	int i, end;

	FENTRY("scdxipci_acq_eof_cleanup");

	// empty the EOF tasklet and memcopy lists (should be already empty)
	end = i = 0;
	list_for_each_safe(list, aux_list, &acq->eof_data_list) {
		list_del(list);
		eof_data = list_entry(list, struct eof_data, list);
		if (eof_data->acq_end)
			end = 1;
		scdxipci_frame_finish(acq, eof_data);
		i++;
	}
	if (i > 0)
		DEB_PARAMS("Warning: %d EOF events found!\n", i);

	i = 0;
	list_for_each_safe(list, aux_list, &acq->memcpy_data_list) {
		list_del(list);
		eof_data = list_entry(list, struct eof_data, list);
		if (eof_data->acq_end)
			end = 1;
		scdxipci_frame_finish(acq, eof_data);
		i++;
	}
	if (i > 0)
		DEB_PARAMS("Warning: %d memcpy events found!\n", i);

	return end;
}



/*--------------------------------------------------------------------------
 * SG management
 *--------------------------------------------------------------------------*/

int scdxipci_acq_dev_update_sg_data(struct acq_dev *acq_dev)
{
	struct scdxipci_dev *scd_dev = acq_dev->dev;
	struct data_acq *acq = acq_dev->acq;
	struct img_frame *frame, *next_frame;
	struct sg_data *sg_data, *prev_sg_data, *curr_sg_data;
	unsigned long acq_flags, dev_flags, total, finished;
	int ret, start;
	char *name, frame_name[SCDXIPCI_FRAME_NAME_LEN];

	FENTRY("scdxipci_acq_dev_update_sg_data");

	name = scd_dev->name;

	scdxipci_lock_acq(acq, acq_flags);
	scdxipci_lock_dev(scd_dev, dev_flags);

	if (!is_current_acq_dev(acq_dev)) {
		ret = SCDXIPCI_ERR_ACQ;
		goto unlock;
	}

	// this does the real mapping and update all the variables
	// first lock the frame (buffer) and unlock dev & acq because
	// the real mapping process can take some time
#define map_frame_update(acq_dev, sg_data, frame, finished)		\
	do {								\
		frame = img_frame_get(frame);				\
		sg_data->being_mapped = 1;				\
		scdxipci_unlock_dev(acq_dev->dev, dev_flags);		\
		scdxipci_unlock_acq(acq_dev->acq, acq_flags);		\
									\
		ret = acq_dev_map_frame(acq_dev, sg_data, frame);	\
									\
		scdxipci_lock_acq(acq_dev->acq, acq_flags);		\
		scdxipci_lock_dev(acq_dev->dev, dev_flags);		\
		sg_data->being_mapped = 0;				\
		img_frame_put(frame);					\
		if (ret < 0)						\
			goto unlock;					\
		sg_data = scdxipci_next_sg_data(acq_dev->dev, sg_data);	\
		acq_dev->dev->oldest_sg_data = sg_data;			\
		finished++;						\
	} while (0)
	
	total = acq->tot_frames;

	finished = acq_dev->finished_frames;
	curr_sg_data = scdxipci_get_curr_sg_data(scd_dev);
	sg_data = scd_dev->oldest_sg_data;
	start = (sg_data == NULL);
	if (start) {
		sg_data = curr_sg_data;
		frame = acq->curr_frame;
	} else {
		prev_sg_data = scdxipci_prev_sg_data(scd_dev, sg_data);
		frame = prev_sg_data->buffer_frame;
	}

	if (frame == NULL) {
		DEB_ERRORS("%s curr_frame is NULL!!\n", name);
		ret = SCDXIPCI_ERR_INTERNAL;
		goto unlock;
	}

	DEB_PARAMS("sg_data_idx=%ld, frame=%s\n", 
		   scdxipci_sg_data_idx(scd_dev, sg_data),
		   img_frame_name(frame_name, frame));
	
	if (start)
		map_frame_update(acq_dev, sg_data, frame, finished);
	
	while (sg_data != curr_sg_data) {
		if ((total > 0) && (finished == total))
			break;
		else if (!is_current_acq_dev(acq_dev)) {
			ret = SCDXIPCI_ERR_ACQ;
			goto unlock;
		}
		
		next_frame = acq_next_frame(acq, frame);
		if (next_frame == NULL) {
			DEB_ERRORS("Could not find %s next frame!!\n", name);
			ret = SCDXIPCI_ERR_INTERNAL;
			goto unlock;
		} else if (next_frame == frame)
			break;
		
		frame = next_frame;
		DEB_PARAMS("sg_data_idx=%ld, frame=%s\n", 
			   scdxipci_sg_data_idx(scd_dev, sg_data),
			   img_frame_name(frame_name, frame));
		
		map_frame_update(acq_dev, sg_data, frame, finished);
	}
	ret = 0;
	
 unlock:
	scdxipci_unlock_dev(scd_dev, dev_flags);
	scdxipci_unlock_acq(acq, acq_flags);

#undef map_frame_update

	return ret;
}

void scdxipci_acq_dev_sg_tasklet(unsigned long data)
{
	struct acq_dev *acq_dev = (struct acq_dev *) data;
	scdxipci_acq_dev_update_sg_data(acq_dev);
}


/*--------------------------------------------------------------------------
 * memcpy thread
 *--------------------------------------------------------------------------*/

int scdxipci_memcpy_thread_start(struct data_acq *acq)
{
	kthread_t memcpy_thread;
	unsigned long flags;
	int ret, keep_running;

	FENTRY("scdxipci_memcpy_thread_start");
	
	memcpy_thread = kthread_create(scdxipci_memcpy_thread, acq, "%s_memcp",
				       acq->name);
	if (IS_ERR(memcpy_thread)) {
		ret = PTR_ERR(memcpy_thread);
		DEB_ERRORS("could not start memcpy thread: ret=%d\n", ret);
		return ret;
	}

	scdxipci_lock_acq(acq, flags);
	keep_running = !scdxipci_memcpy_thread_active(acq);
	if (keep_running) 
		acq->memcpy_thread = memcpy_thread;
	scdxipci_unlock_acq(acq, flags);

	if (keep_running)
		wake_up_process(kthread_task(memcpy_thread));
	else
		kthread_stop(memcpy_thread);

	return SCDXIPCI_OK;
}

void scdxipci_memcpy_thread_stop(struct data_acq *acq)
{
	kthread_t memcpy_thread;
	unsigned long flags;
	int ret;

	FENTRY("scdxipci_memcpy_thread_stop");
	DEB_PARAMS("Stopping %s memcpy thread\n", acq->name);

	scdxipci_lock_acq(acq, flags);
	memcpy_thread = acq->memcpy_thread;
	acq->memcpy_thread = 0;
	scdxipci_unlock_acq(acq, flags);

	if (memcpy_thread == 0)
		return;

	ret = kthread_stop(memcpy_thread);
	if (ret < 0)
		DEB_ERRORS("Error: Could not stop memcpy thread (ret=%d)!!\n", 
			   ret);
}

int scdxipci_memcpy_thread(void *data)
{
	struct data_acq *acq = (struct data_acq *) data;
	struct img_frame *frame, *mapped;
	struct eof_data *eof_data;
#if LINUX_VERSION_CODE < KERNEL_VERSION(5, 9, 0)
	struct sched_param sched_par;
#endif
	unsigned long flags;
	char name[SCDXIPCI_FRAME_NAME_LEN];
	int ret;

	FENTRY("scdxipci_memcpy_thread");

#define eof_data_ready \
	(((eof_data = scdxipci_memcpy_eof_data(acq)) != NULL) || \
	 kthread_should_stop())

	DEB_PARAMS("Entering %s memcpy thread!\n", acq->name);

#if LINUX_VERSION_CODE > KERNEL_VERSION(5, 9, 0)
	sched_set_fifo(current);
#else
	sched_par.sched_priority = SCDXIPCI_MEMCPY_SCHED_PRIORITY;
	ret = sched_setscheduler(current, SCDXIPCI_MEMCPY_SCHED_POLICY, 
				 &sched_par);
	if (ret < 0) 
		DEB_ERRORS("Error setting memcpy scheduler: %d\n", ret);
#endif
	while (!kthread_should_stop()) {
		eof_data = NULL;
		wait_event_interruptible(acq->memcpy_wq, eof_data_ready);
		if (eof_data == NULL)
			continue;

		frame  = eof_data->ready_frame;
		mapped = eof_data->mapped_frame;
		ret = eof_data->error;
		if ((frame != mapped) && (ret >= 0)) {
			DEB_PARAMS("Frame %s has 64-bit pages, copying from"
				   " aux. buffer frame %ld (%ld pages)\n", 
				   img_frame_name(name, frame), 
				   mapped->frame_nr, img_frame_pages(frame));
			ret = scdxipci_frame_copy(frame, mapped);
			eof_data->error = ret;
		}
			
		scdxipci_lock_acq(acq, flags);
		scdxipci_frame_finish(acq, eof_data);
		scdxipci_unlock_acq(acq, flags);

		if (ret < 0)
			scdxipci_acq_stop(acq);
	}

	DEB_PARAMS("Exiting %s memcpy thread!\n", acq->name);

#undef eof_data_ready

	return 0;
}

struct eof_data *scdxipci_memcpy_eof_data(struct data_acq *acq)
{
	unsigned long flags;
	struct eof_data *eof_data = NULL;
	struct list_head *list;

	scdxipci_lock_acq(acq, flags);
	if (!list_empty(&acq->memcpy_data_list)) {
		list = acq->memcpy_data_list.next;
		list_del(list);
		eof_data = list_entry(list, struct eof_data, list);
	}
	scdxipci_unlock_acq(acq, flags);

	return eof_data;
}

/*--------------------------------------------------------------------------
 * Frame copy
 *--------------------------------------------------------------------------*/

int scdxipci_frame_copy(struct img_frame *dst, struct img_frame *src)
{
	void *srcp, *dstp;
	struct img_frame_page *src_fp, *dst_fp;
	int nr_pages, i, ret;
	ktime_t t0, t1;
	struct data_acq *acq = img_frame_acq(dst);
	struct scdxipci_stats *stats;
	char name[SCDXIPCI_FRAME_NAME_LEN];
	unsigned long flags;

	FENTRY("scdxipci_frame_copy");

	nr_pages = img_frame_pages(dst);
	src_fp = src->first_page;
	dst_fp = dst->first_page;

	if (acq != NULL)
		t0 = ktime_get();

	for (i = 0; i < nr_pages; i++) {
		if (!src_fp || !dst_fp) {
			DEB_ERRORS("Invalid frame page: src=0x%p, dst=0x%p\n", 
				   src_fp, dst_fp);
			return SCDXIPCI_ERR_INTERNAL;
		}

		ret = -ENOMEM;
		dstp = NULL;
		srcp = kmap(src_fp->page);
		if (srcp == NULL) {
			DEB_ERRORS("Cannot kmap page 0x%p (#%d of src frame "
				   "%s)\n", src_fp->page, i, 
				   img_frame_name(name, src));
			goto out;
		}
		dstp = kmap(dst_fp->page);
		if (dstp == NULL) {
			DEB_ERRORS("Cannot kmap page 0x%p (#%d of dst frame "
				   "%s)\n", dst_fp->page, i, 
				   img_frame_name(name, dst));
			goto out;
		}

		if (img_frame_page_is64(src_fp)) {
			DEB_ERRORS("Error: Source page %d is 64-bit!!\n", i);
			ret = SCDXIPCI_ERR_INTERNAL;
			goto out;
		}
		if (img_frame_page_is64(dst_fp)) 
			DEB_PARAMS("Dest. page %d is 64-bit\n", i);

		scdxipci_copy_page(dstp, srcp);
		ret = 0;

	out:
		if (dstp != NULL)
			kunmap(dst_fp->page);
		if (srcp != NULL)
			kunmap(src_fp->page);
		if (ret < 0)
			return ret;

		src_fp = src_fp->next;
		dst_fp = dst_fp->next;
	}

	if (acq != NULL) {
		stats = &acq->stats;
		t1 = ktime_get();

		scdxipci_lock_acq(acq, flags);
		stats->copy_pages += nr_pages;
		stats->copy_usec += scdxipci_elapsed_usec(t0, t1);
		stats->copy_count++;
		scdxipci_unlock_acq(acq, flags);
	}

	return nr_pages;
}


/*--------------------------------------------------------------------------
 * Frame finish
 *--------------------------------------------------------------------------*/

void scdxipci_frame_finish(struct data_acq *acq, struct eof_data *eof_data)
{
	struct img_frame *frame, *mapped, *new;
	struct img_buffer *buffer;
	unsigned long end;

	FENTRY("scdxipci_frame_finish");

	frame  = eof_data->ready_frame;
	mapped = eof_data->mapped_frame;
	new    = eof_data->next_frame;
	end    = eof_data->acq_end;

	kfree(eof_data);

	img_frame_put(frame);
	img_frame_put(mapped);
	img_frame_put(new);

	if (!is_current_acq(acq)) {
		DEB_PARAMS("%s aborted!\n", acq->name);
		return;
	}

	buffer = frame->buffer;
	
	frame->round_count++;
	acq->acq_frame_nr++;
	frame->acq_frame_nr = acq->acq_frame_nr;
	
	DEB_INTS("%s, frame_nr=%ld, count=%ld, acq_frame_nr=%ld\n", 
		 buffer->name, frame->frame_nr, frame->round_count,
		 acq->acq_frame_nr);
	
	if (buffer != new->buffer)
		DEB_INTS("%s, count=%ld\n", buffer->name, 
			 img_buffer_round_count(buffer));
	
	acq->curr_frame = new;
	PPDEB_EOF_SET(PPDEB_EOF_WAKEUP);
	scdxipci_wakeup(acq, frame);

	if (end) 
		scdxipci_check_kept_pages(acq);

	if (end) {
		PPDEB_EOF_SET(PPDEB_EOF_END);
		scdxipci_acq_end_cleanup(acq);
	}
}


/*--------------------------------------------------------------------------
 * Acq. end cleanup
 *
 * This does the clean-up at the end of acquisition, called when the acq.
 * ends (after its last frame), or it's aborted.
 * Note: acq. must be locked, but devices should NOT be locked
 *--------------------------------------------------------------------------*/

void scdxipci_acq_end_cleanup(struct data_acq *acq)
{
	struct scdxipci_dev *scd_dev;
	unsigned long flags, i;

	FENTRY("scdxipci_acq_end_cleanup");

	if (is_current_acq(acq)) {
		// Release the Espia devices
		for (i = 0; i < acq->nr_dev; i++) {
			scd_dev = acq->dev[i].dev;
			scdxipci_lock_dev(scd_dev, flags);
			scd_dev->curr_acq = NULL;
			scdxipci_unlock_dev(scd_dev, flags);
		}
	}

	// Wakeup possible sleeping cb threads
	scdxipci_wakeup_all(acq);
	acq_inc_run_nr(acq);
	DEB_PARAMS("%s run nr: %ld\n", acq->name, acq_get_run_nr(acq));
}


/*--------------------------------------------------------------------------
 * Frame page alloc/free
 *--------------------------------------------------------------------------*/

struct img_frame_page *scdxipci_frame_page_alloc(gfp_t gfp_flags,
						 struct vm_area_struct *vma,
						 unsigned long vaddr)
{
	struct img_frame_page *fpage;
	struct page *page;
	void *ptr;

	FENTRY("scdxipci_frame_page_alloc");

	ptr = kmem_cache_alloc(frame_page_cache, GFP_KERNEL);
	if (ptr == NULL) {
		DEB_ERRORS("Failed to allocate frame page stc (%ld bytes)\n",
			   (long) sizeof(*fpage));
		goto out_cache_alloc;
	}
	fpage = (struct img_frame_page *) ptr;
	memset(fpage, 0, sizeof(*fpage));

	page = alloc_page(gfp_flags);
	if (page == NULL) {
		DEB_ERRORS("Failed to allocate new page\n");
		goto out_alloc_page;
	}

	// clear the page - a security hole?
	ptr = kmap(page);
	if (!ptr) {
		DEB_ERRORS("Failed to kmap page\n");
		goto out_kmap;
	}
	clear_page(ptr);
	kunmap(page);

#ifdef ESRFVER_REMAP_PAGE_RESERVED	
	SetPageReserved(page);
#endif

	fpage->page = page;
	return fpage;

 out_kmap:
	__free_page(page);
 out_alloc_page:
	kmem_cache_free(frame_page_cache, fpage);
 out_cache_alloc:
	return NULL;
}


void scdxipci_frame_page_free(struct img_frame_page *fpage)
{
	if (fpage->page) {
#ifdef ESRFVER_REMAP_PAGE_RESERVED
		ClearPageReserved(fpage->page);
#endif
		__free_page(fpage->page);
	}
	kmem_cache_free(frame_page_cache, fpage);
}


/*--------------------------------------------------------------------------
 * Buffer map/unmap
 *--------------------------------------------------------------------------*/

int scdxipci_buffer_map(struct img_buffer *buffer, struct vm_area_struct *vma)
{
	struct img_frame *frame;
	struct img_frame_page *fpage;
	struct list_head *list;
	struct page *page;
	int ret;
	pgprot_t prot;
	unsigned long vaddr, nr_pages, real_nr_pages;

	FENTRY("scdxipci_buffer_map");

	nr_pages = 0;
	vaddr = vma->vm_start;
	prot  = vma->vm_page_prot;
	list_for_each(list, &buffer->frame_list) {
		frame = img_frame_from_list(list);
		for (fpage = frame->first_page; fpage; fpage = fpage->next) {
			page = fpage->page;
			ret = remap_pfn_range(vma, vaddr, page_to_pfn(page), 
					      PAGE_SIZE, prot);
			if (ret < 0) {
				DEB_ERRORS("Failed to remap page nr %ld\n",
					   nr_pages);
				goto error;
			} 
#ifdef ESRFVER_REMAP_PAGE_FORCE
			if (img_frame_page_is64(fpage)) {
				ret = map_single_page(vma, vaddr, page, prot);
				if (ret < 0) {
					DEB_ERRORS("Failed to force map page "
						   "nr %ld\n", nr_pages);
					goto error;
				}
			}
#endif
			vaddr += PAGE_SIZE;
			nr_pages++;
		}
	}
	img_buffer_pages(buffer, &real_nr_pages, NULL);
	DEB_PARAMS("mapped %ld pages, img_buffer_pages()=%ld\n", nr_pages,
		   real_nr_pages);

	vma->vm_ops = &scdxipci_vm_operations;
	vma->vm_private_data = buffer;
	atomic_inc(&buffer->open_count);

	return 0;

 error:
	scdxipci_buffer_unmap(buffer, vma, nr_pages);
	return ret;
}

void scdxipci_buffer_unmap(struct img_buffer *buffer, 
			   struct vm_area_struct *vma, unsigned long nr_pages)
{
	FENTRY("scdxipci_buffer_unmap");
	DEB_PARAMS("unmapping %s, nr_pages=%ld\n", buffer->name, nr_pages);

#ifdef ESRFVER_REMAP_PAGE_FORCE
	unmap_page_range(vma, vma->vm_start, nr_pages * PAGE_SIZE);
#endif
}


/*--------------------------------------------------------------------------
 * Virtual memory area operations
 *--------------------------------------------------------------------------*/

void scdxipci_vm_open(struct vm_area_struct *vma)
{
	struct img_buffer *buffer = img_buffer_from_vma(vma);

	FENTRY("scdxipci_vm_open");
	atomic_inc(&buffer->open_count);
	DEB_PARAMS("%s\n", buffer->name);
}


void scdxipci_vm_close(struct vm_area_struct *vma)
{
	struct img_buffer *buffer = img_buffer_from_vma(vma);

	FENTRY("scdxipci_vm_close");
	DEB_PARAMS("%s\n", buffer->name);
	if (atomic_dec_and_test(&buffer->open_count)) {
		DEB_PARAMS("freeing %s\n", buffer->name);
		scdxipci_buffer_free(buffer);
	}
}


/*--------------------------------------------------------------------------
 * buffer from user addr
 *--------------------------------------------------------------------------*/

struct img_buffer *scdxipci_buffer_from_uadd(struct data_acq *acq, void *uadd)
{
	struct img_buffer *buffer;
	unsigned long buffer_nr;

	FENTRY("scdxipci_buffer_from_uadd");
	
	acq_for_each_good_buffer(buffer_nr, buffer, acq)
		if (img_buffer_uadd(buffer) == uadd)
			return buffer;

	DEB_ERRORS("Warning: buffer@uadd 0x%p not in %s!! Erased?\n", uadd,
		   acq->name);
	return NULL;
}


/*--------------------------------------------------------------------------
 * check buffer
 *--------------------------------------------------------------------------*/

int scdxipci_buffer_check(struct data_acq *acq, struct img_buffer *buffer)
{
	FENTRY("scdxipci_buffer_check");

	if (buffer == NULL) {
		DEB_ERRORS("invalid NULL buffer\n");
	} else if (buffer->acq != acq) {
		DEB_ERRORS("%s belongs to %s, not to %s\n", buffer->name,
			   buffer->acq->name, acq->name);
	} else if (list_empty(&buffer->frame_list)) {
		DEB_ERRORS("%s is empty!\n", buffer->name);
	} else if (buffer->nr >= acq->nr_buffers) {
		DEB_ERRORS("buffer #%ld is greater than %s nr of buff (%ld)\n",
			   buffer->nr, acq->name, acq->nr_buffers);
	} else if (acq->buffer_arr[buffer->nr] != buffer) {
		DEB_ERRORS("%s is not %s buffer #%ld\n", buffer->name, 
			   acq->name, buffer->nr);
	} else 
		return SCDXIPCI_OK;

	return -EINVAL;
}


/*--------------------------------------------------------------------------
 * Acquisition start/stop
 *--------------------------------------------------------------------------*/

int scdxipci_acq_start(struct data_acq *acq, unsigned long start_buffer_nr,
		       unsigned long nr_frames)
{
	struct scdxipci_dev *scd_dev;
	struct acq_dev *acq_dev;
	struct img_buffer *start_buffer;
	struct img_frame *start_frame;
	struct sg_data *sg_data;
	struct scdxipci_sg_table *sg_table;
	unsigned long i, acq_flags, dev_flags = 0;
	int aux_ret, ret;
	
	FENTRY("scdxipci_acq_start");

	scdxipci_lock_acq(acq, acq_flags);

	// must have an aux. 32-bit buffer allocated
	if (acq->aux_buffer == NULL) {
		DEB_ERRORS("%s does not have 32-bit only aux. buffer\n",
			   acq->name);
		ret = SCDXIPCI_ERR_AUXBUFF;
		goto unlock_acq;
	}

	// must also have dev. defined
	if (acq->nr_dev == 0) {
		DEB_ERRORS("error: %s has no device defined yet", acq->name);
		ret = SCDXIPCI_ERR_ACQNODEV;
		goto unlock_acq;
	} 

	// perform per-device lock and checkings
	ret = 0;
	acq_dev = acq->dev;
	for (i = 0; i < acq->nr_dev; i++, acq_dev++) {
		// lock device and check it is free. note same dev_flags!
		scd_dev = acq_dev->dev;
		scdxipci_lock_dev(scd_dev, dev_flags);
		aux_ret = 0;
		if (scd_dev->curr_acq != NULL) {
			DEB_ERRORS("%s still running on dev #%d!\n", 
				   scd_dev->curr_acq->acq->name, scd_dev->nr);
			aux_ret = -EBUSY;
		}
		// verify SG table has been validated
		if (!acq_dev->sg_table_validated) {
			sg_table = &acq_dev->sg_table;
			aux_ret = acq_dev_validate_sg_table(acq_dev, sg_table);
			acq_dev->sg_table_validated = (aux_ret >= 0);
		}
		if (ret == 0)
			ret = aux_ret;
	}
	if (ret < 0)
		goto unlock_dev;

	// initialize all frames. get the start buffer and start frame
	start_frame = scdxipci_get_start_frame(acq, start_buffer_nr, 
					       &start_buffer);
	if (start_frame == NULL) {
		ret = -EINVAL;
		goto unlock_dev;
	}
	acq->curr_frame = start_frame;

	// map the start frame 
	acq_dev = acq->dev;
	for (i = 0; i < acq->nr_dev; i++, acq_dev++) {
		acq_dev->aux_buffer_curr_frame = 0;
		acq_dev->finished_frames = 0;
		scd_dev = acq_dev->dev;
		scd_dev->curr_acq = acq_dev;
		scd_dev->oldest_sg_data = NULL;

		// if only one SG table, map it now
		sg_data = scdxipci_first_sg_data(scd_dev);
		if (scdxipci_nr_sg_data(scd_dev) == 1) {
			// manually map the start frame
			ret = acq_dev_map_frame(acq_dev, sg_data, start_frame);
			if (ret < 0)
				goto unlock_dev;
		}

		// tell the board where to start from
		scdxipci_set_curr_sg_data(scd_dev, sg_data);
	}

	// almost start the game ...
	DEB_PARAMS("Starting %s, nr_frames=%ld, %s\n", acq->name, nr_frames, 
		   start_buffer->name);

	acq_frame_nr_init(acq);
	acq->tot_frames = nr_frames;
	acq->start_buffer = start_buffer;
	acq->start_time = ktime_get();

	ret = 0;

 unlock_dev:
	// unlock device and acq. and cleanup if error
	for (i = 0; i < acq->nr_dev; i++) {
		scd_dev = acq->dev[i].dev;
		if ((ret < 0) && (ret != -EBUSY))
			scd_dev->curr_acq = NULL;
		scdxipci_unlock_dev(scd_dev, dev_flags);
	}
	if (ret < 0) {
		acq->curr_frame = NULL;
		goto unlock_acq;
	}

	// for more than one SG table, prepare them now
	if (scdxipci_nr_sg_data(scd_dev) > 1) {
		scdxipci_unlock_acq(acq, acq_flags);
		for (i = 0; i < acq->nr_dev; i++) {
			ret = scdxipci_acq_dev_update_sg_data(&acq->dev[i]);
			if (ret < 0) {
				scdxipci_acq_stop(acq);
				return ret;
			}
		}
		scdxipci_lock_acq(acq, acq_flags);
	}

	// ... and start the devices and release their locks
	for (i = 0; i < acq->nr_dev; i++) {
		scd_dev = acq->dev[i].dev;
		scdxipci_lock_dev(scd_dev, dev_flags);
		scdxipci_activate_dma(scd_dev, SCDXIPCI_FRAME_FIRST);
		scdxipci_unlock_dev(scd_dev, dev_flags);
	}

 unlock_acq:
	scdxipci_unlock_acq(acq, acq_flags);

	return ret;
}


void scdxipci_acq_stop(struct data_acq *acq)
{
	struct scdxipci_dev *scd_dev;
	unsigned long acq_flags, dev_flags, i, end_treated;
	
	FENTRY("scdxipci_acq_stop");

	scdxipci_lock_acq(acq, acq_flags);

	if (is_current_acq(acq)) {
		DEB_PARAMS("%s aborted (acq. %ld of %ld frames). "
			   "Waking up all waiters\n", acq->name, 
			   acq->acq_frame_nr, acq->tot_frames);
		for (i = 0; i < acq->nr_dev; i++) {
			scd_dev = acq->dev[i].dev;
			scdxipci_lock_dev(scd_dev, dev_flags);
			scdxipci_abort_dma(scd_dev);
			scdxipci_unlock_dev(scd_dev, dev_flags);
		}
	}

	end_treated = scdxipci_acq_eof_cleanup(acq);
	if (!end_treated)
		scdxipci_acq_end_cleanup(acq);

	scdxipci_unlock_acq(acq, acq_flags);
}


/*--------------------------------------------------------------------------
 * get start frame / map next frame
 *--------------------------------------------------------------------------*/

struct img_frame *scdxipci_get_start_frame(struct data_acq *acq,
					   unsigned long start_buffer_nr,
					   struct img_buffer **start_buffer)
{
	struct img_buffer *buffer;
	struct img_frame *frame;
	struct list_head *list;
	unsigned long buffer_nr;
	
	FENTRY("scdxipci_get_start_frame");

	// check if start_buffer is still there
	*start_buffer = acq_buffer(acq, start_buffer_nr);
	if (*start_buffer == NULL) {
		DEB_ERRORS("Start buffer #%ld not in %s!!!\n", 
			   start_buffer_nr, acq->name);
		return NULL;
	}

	// prepare buffers and frame
	acq_for_each_good_buffer(buffer_nr, buffer, acq) {
		list_for_each(list, &buffer->frame_list) {
			frame = img_frame_from_list(list);
			img_frame_init(frame);
		}
	}

	frame = img_buffer_first_frame(*start_buffer);
	atomic_set(&frame->dev_count, acq->nr_dev);

	return frame;
}


/*--------------------------------------------------------------------------
 * acq. wait for the specified frame and return its info
 *--------------------------------------------------------------------------*/

int scdxipci_acq_wait_get_frame(struct data_acq *acq, 
				struct img_frame_info *finfo, 
				unsigned long timeout, unsigned long thread_id)
{
	struct img_frame *frame;
	struct acq_wait wait;
	int ret, block, ask_acq_frame;
	unsigned long flags, acq_run_nr, facq_run_nr;

	FENTRY("scdxipci_acq_wait_get_frame");

	block = (timeout != SCDXIPCI_NO_BLOCK);
	ask_acq_frame = (is_any(finfo->buffer_nr) && is_any(finfo->frame_nr));
	facq_run_nr = finfo->acq_run_nr;

	scdxipci_lock_acq(acq, flags);

	if (is_empty_acq(acq)) {	// some basic check
		DEB_ERRORS("%s is empty!\n", acq->name);
		ret = SCDXIPCI_ERR_ACQ;
		goto out;
	} else if (acq->removed_buffers) {
		DEB_ERRORS("%s is loosing buffers, cannot search frames", 
			   acq->name);
		ret = SCDXIPCI_ERR_ACQ;
		goto out;
	}

	acq_run_nr = acq_get_run_nr(acq);
	if ((facq_run_nr == acq_run_nr) && !is_current_acq(acq)) {
		if (!block) {
			ret = SCDXIPCI_ERR_NOTREADY;
			goto out;
		}
		frame = NULL;
		ret = SCDXIPCI_OK;
	} else if (ask_acq_frame) {
		PPDEB_GETFRM_SET(ACQ_NR, 1);
		ret = scdxipci_find_by_acq_frame_nr(acq, finfo, &frame, 
						    &block);
		PPDEB_GETFRM_SET(ACQ_NR, 0);
	} else {
		PPDEB_GETFRM_SET(BUFF_NR, 1);
		ret = scdxipci_find_by_buffer_frame(acq, finfo, &frame, 
						    &block);
		PPDEB_GETFRM_SET(BUFF_NR, 0);
	}

	if (ret < 0)
		goto out;

	if (block) {
		if (facq_run_nr < acq_run_nr) {
			DEB_PARAMS("Requested frame of %s run nr %ld will "
				   "never arrive\n", acq->name, facq_run_nr);
			ret = SCDXIPCI_ERR_WONTARR;
			goto out;
		}

		init_waitqueue_head(&wait.wq);
		wait.woken_up = 0;
		wait.frame_info = finfo;
		wait.thread_id = thread_id;
		DEB_PARAMS("Adding thread %ld to frame wait list\n", thread_id);
		list_add_tail(&wait.wait_list, &acq->wait_list);
		wake_up_interruptible(&acq->wait_wq);
	} else if (frame == NULL) {
		DEB_ERRORS("Not block, no frame and no error!!!!");
		ret = SCDXIPCI_ERR_INTERNAL;
	} else
		scdxipci_update_frame_info(acq, finfo, frame);

 out:
	scdxipci_unlock_acq(acq, flags);
	if ((ret < 0) || !block)
		return ret;

	ret = scdxipci_acq_block(&wait, timeout);

	scdxipci_lock_acq(acq, flags);
	list_del_init(&wait.wait_list);
	scdxipci_unlock_acq(acq, flags);

	return ret;
}


int scdxipci_find_by_acq_frame_nr(struct data_acq *acq, 
				  struct img_frame_info *finfo, 
				  struct img_frame **pframe, int *block)
{
	unsigned long curr_count, count, new_first_count, old_first_count;
	unsigned long nr_buffers, buffer_nr, nr_frames, frame_nr;
	unsigned long iter, max_iter, step; 
	int wrapped, first_ready, good_buffer;
	long long aux;
	struct img_frame *curr_frame, *oldest_frame, *newest_frame;
	struct img_frame *first_frame, *last_frame;
	struct img_buffer *oldest_buffer, *newest_buffer, *buffer;
	struct scdxipci_stats *stats = &acq->stats;
	ktime_t t0, t1;

	FENTRY("scdxipci_find_by_acq_frame_nr");

	// check if not asking too much
	count = finfo->acq_frame_nr;
	if ((acq->tot_frames > 0) && !is_any(count) && 
	    (count >= acq->tot_frames)) {
		DEB_ERRORS("%s will have only %ld frames, asked frame %ld\n", 
			   acq->name, acq->tot_frames, count);
		return SCDXIPCI_ERR_FRAMENR;
	}

	curr_count = acq->acq_frame_nr;
	curr_frame = acq->curr_frame;
	first_ready = !is_invalid(curr_count);
	DEB_PARAMS("count=%ld, curr_count=%ld, first_ready=%d\n",
		   count, curr_count, first_ready);
	stats->afn_call++;

	// asked for the newest frame?
	newest_frame = first_ready ? acq_prev_frame(acq, curr_frame) : NULL;
	if (first_ready && (newest_frame->acq_frame_nr != curr_count)) {
		DEB_ERRORS("FATAL error: newest_frame(%ld)!=curr_count(%ld)\n",
			   newest_frame->acq_frame_nr, curr_count);
		return SCDXIPCI_ERR_INTERNAL;
	}
	if (is_any(count)) {
		*pframe = newest_frame;
		if (!*block && !*pframe) {
			DEB_PARAMS("no frame available yet\n");
			return SCDXIPCI_ERR_NOTREADY;
		}
		return SCDXIPCI_OK;
	}
	// asked for a specific one: is it ready?
	if (!first_ready || (count > curr_count)) {
		*pframe = NULL;
		DEB_PARAMS("frame %ld not reay yet\n", count);
		return *block ? SCDXIPCI_OK : SCDXIPCI_ERR_NOTREADY;
	}

	// check for overrun
	oldest_frame = img_buffer_first_frame(acq->start_buffer);
	wrapped = !is_invalid(curr_frame->acq_frame_nr);
	if (wrapped) {
		if (is_current_acq(acq))
			oldest_frame = acq_next_frame(acq, curr_frame);
		else
			oldest_frame = curr_frame;
		DEB_PARAMS("wrapped: oldest frame is %ld\n", 
			  oldest_frame->acq_frame_nr);
	}
	frame_nr = oldest_frame->acq_frame_nr;
	if (is_invalid(frame_nr)) {
		DEB_ERRORS("FATAL error: oldest_frame is invalid!\n");
		return SCDXIPCI_ERR_INTERNAL;
	} else if (count < frame_nr) {
		DEB_PARAMS("frame %ld is no longer available\n", count);
		return SCDXIPCI_ERR_OVERRUN;
	}

	// take t0
	t0 = ktime_get();

	// the frame is available, find it! 
	// first check if it's in newest frame's buffer
	oldest_buffer = oldest_frame->buffer;
	old_first_count = oldest_frame->acq_frame_nr;
	newest_buffer = newest_frame->buffer;
	first_frame = img_buffer_first_frame(newest_buffer);
	last_frame = newest_frame;
	new_first_count = first_frame->acq_frame_nr;
	good_buffer = (count >= new_first_count);
	DEB_PARAMS("newest_buffer-nr=%ld, new_first_count=%ld, good_buff=%d\n",
		  newest_buffer->nr, new_first_count, good_buffer);

	if (!good_buffer) {
		// then check in oldest frame's buffer
		first_frame = oldest_frame;
		last_frame = img_buffer_last_frame(oldest_buffer);
		good_buffer = (count <= last_frame->acq_frame_nr);
		DEB_PARAMS("oldest_buffer-nr=%ld, good_buffer=%d\n",
			   oldest_buffer->nr, good_buffer);
	}

	if (!good_buffer) {
		// search will start in oldest frame's next buffer
		oldest_buffer = acq_next_buffer(acq, oldest_buffer);
		first_frame = img_buffer_first_frame(oldest_buffer);
		old_first_count = first_frame->acq_frame_nr;
	}

	max_iter = SCDXIPCI_AFN_MAX_ITER;
	for (iter = 0; !good_buffer && (iter < max_iter); iter++) {
		DEB_PARAMS("old_first_count=%ld, new_first_count=%ld\n",
			   old_first_count, new_first_count);

		// interpolate: consider all buffers have same size
		nr_buffers = newest_buffer->nr - oldest_buffer->nr;
		if (newest_buffer->nr < oldest_buffer->nr)
			nr_buffers += acq->nr_buffers;
		if ((count < old_first_count) || 
		    new_first_count <= old_first_count) {
			DEB_ERRORS("FATAL error: count(%ld) or new_first(%ld) <"
				   " old_first(%ld)!\n", count, new_first_count,
				   old_first_count);
			return SCDXIPCI_ERR_INTERNAL;
		}
		nr_frames = new_first_count - old_first_count;
		aux  = count - old_first_count;
		aux *= nr_buffers;
		do_div(aux, nr_frames);
		buffer_nr  = aux + oldest_buffer->nr;
		buffer_nr %= acq->nr_buffers;
		while (!(buffer = acq_buffer(acq, buffer_nr)))
			buffer_nr = (buffer_nr + 1) % acq->nr_buffers;
		first_frame = img_buffer_first_frame(buffer);
		last_frame = img_buffer_last_frame(buffer);
		frame_nr = first_frame->acq_frame_nr;
		good_buffer = ((count >= frame_nr) && 
			       (count <= last_frame->acq_frame_nr));
		DEB_PARAMS("iter=%ld, buffer_nr=%ld, frame_nr=%ld, good_buffer="
			   "%d\n", iter, buffer_nr, frame_nr, good_buffer);
		if (good_buffer)
			continue;
		if (frame_nr > count) {
			newest_buffer = buffer;
			new_first_count = frame_nr;
		} else {
			oldest_buffer = acq_next_buffer(acq, buffer);
			first_frame = img_buffer_first_frame(oldest_buffer);
			old_first_count = first_frame->acq_frame_nr;
		}
	}
	if (iter == max_iter) {
		DEB_ERRORS("FATAL error: couldn't find frame %ld (curr=%ld) "
			   "after %ld iters!\n", count, curr_count, iter);
		return SCDXIPCI_ERR_INTERNAL;
	}

	// ok got the good buffer
	frame_nr = first_frame->acq_frame_nr;
	DEB_PARAMS("first_frame-nr=%ld, last_frame-nr=%ld\n", 
		   frame_nr, last_frame->acq_frame_nr);
	if ((count < frame_nr) || (count > last_frame->acq_frame_nr)) {
		DEB_ERRORS("FATAL error: count(%ld) < first(%ld) or > last(%ld)"
			   "\n", count, frame_nr, last_frame->acq_frame_nr);
		return SCDXIPCI_ERR_INTERNAL;
	}
	if ((count - frame_nr) < (last_frame->acq_frame_nr - count)) {
		DEB_PARAMS("approching from oldest\n");
		*pframe = first_frame;
		for (step = 0; (*pframe)->acq_frame_nr != count; step++)
			*pframe = acq_next_frame(acq, *pframe);
	} else {
		DEB_PARAMS("approching from newest\n");
		*pframe = last_frame;
		for (step = 0; (*pframe)->acq_frame_nr != count; step++)
			*pframe = acq_prev_frame(acq, *pframe);
	}

	// update statistics
	t1 = ktime_get();
	stats->afn_usec += scdxipci_elapsed_usec(t0, t1);
	stats->afn_count++;
	stats->afn_iter += iter;
	if (iter > stats->afn_max_iter)
		stats->afn_max_iter = iter;
	stats->afn_step += step;
	if (step > stats->afn_max_step)
		stats->afn_max_step = step;
	
	// Uff!
	DEB_PARAMS("got it!\n");
	*block = 0;

	return SCDXIPCI_OK;
}

int scdxipci_find_by_buffer_frame(struct data_acq *acq, 
				  struct img_frame_info *finfo, 
				  struct img_frame **pframe, int *block) 
{
	int ret, count, any_buffer, any_frame, any_count, curr_count;
	struct img_buffer *buffer;
	struct img_frame *curr_frame, *last_frame;

	FENTRY("scdxipci_find_by_buffer_frame");

	curr_frame = acq->curr_frame;
	last_frame = acq_prev_frame(acq, curr_frame);
	if (is_invalid(last_frame->acq_frame_nr))
		last_frame = NULL;
	if (!*block && !last_frame)
		return SCDXIPCI_ERR_NOTREADY;

	// First check the request parameters
	any_buffer = is_any(finfo->buffer_nr);
	if (!any_buffer) {
		buffer = img_buffer_from_finfo(acq, finfo);
		if (buffer == NULL) {
			DEB_ERRORS("Invalid buffer_nr: %ld\n", 
				   finfo->buffer_nr);
			return SCDXIPCI_ERR_BUFFER;
		}
	} else if (last_frame)
		buffer = last_frame->buffer;
	else
		buffer = acq_first_buffer(acq); // just for checking frame_nr

	ret = scdxipci_buffer_check(acq, buffer);
	if (ret < 0)
		return ret;

	any_frame = is_any(finfo->frame_nr);
	if ((!any_frame) && (finfo->frame_nr >= img_buffer_frames(buffer))) {
		DEB_ERRORS("Invalid frame_nr: %ld\n", finfo->frame_nr);
		return SCDXIPCI_ERR_FRAMENR;
	}

	any_count = is_any(finfo->round_count);
	if (*block && (any_buffer || any_frame || any_count || !last_frame)) {
		*pframe = NULL;
		return SCDXIPCI_OK;
	}

	if (!any_frame) {
		*pframe = img_buffer_get_frame(buffer, finfo->frame_nr);
		if (*pframe == NULL) {
			DEB_ERRORS("img_buffer_get_frame(%ld) failed!\n", 
				   finfo->frame_nr);
			return SCDXIPCI_ERR_INTERNAL;
		}
	} else if (last_frame->buffer == buffer) {
		*pframe = last_frame;
	} else 
		*pframe = img_buffer_last_frame(buffer);

	count = finfo->round_count;
	curr_count = (*pframe)->round_count;
	if (is_any(count)) {
		// frame is OK
	} else if (count < curr_count)		// Note: both are ints!
		return SCDXIPCI_ERR_OVERRUN;
	else if (count == curr_count)
		*block = 0;
	else if (*block)
		*pframe = NULL;
	else
		return SCDXIPCI_ERR_NOTREADY;
	
	return SCDXIPCI_OK;
}


/*--------------------------------------------------------------------------
 * update frame info
 *--------------------------------------------------------------------------*/

void scdxipci_update_frame_info(struct data_acq *acq,
				struct img_frame_info *finfo, 
				struct img_frame *frame)
{
	ktime_t *t0, *t1, now;

	FENTRY("scdxipci_update_frame_info");

	t0 = &acq->start_time;
	if (frame) {
		t1 = &frame->time_stamp;
	} else {
		t1 = &now;
		*t1 = ktime_get();
	}
	
	finfo->buffer_nr = frame ? frame->buffer->nr : SCDXIPCI_INVALID;
	finfo->frame_nr = frame ? frame->frame_nr : SCDXIPCI_INVALID;
	finfo->round_count = frame ? frame->round_count : SCDXIPCI_INVALID;
	finfo->acq_frame_nr = frame ? frame->acq_frame_nr : SCDXIPCI_INVALID;
	finfo->pixels = frame ? frame->nr_pixels : 0;
	finfo->time_us = scdxipci_elapsed_usec(*t0, *t1);

	DEB_PARAMS("buffer_nr=%ld, frame_nr=%ld, round_count=%ld, acq_frame_nr="
		   "%ld, pixels=%ld, time_us=%ld\n", finfo->buffer_nr, 
		   finfo->frame_nr, finfo->round_count, finfo->acq_frame_nr, 
		   finfo->pixels, finfo->time_us);
}


/*--------------------------------------------------------------------------
 * acq. block/wakeup
 *--------------------------------------------------------------------------*/

int scdxipci_acq_block(struct acq_wait *wait, unsigned long timeout)
{
	unsigned long ticks;
	long ret;

	FENTRY("scdxipci_acq_block");

	ticks = scdxipci_timeout(timeout);
	DEB_PARAMS("timeout=%ld, ticks=%ld\n", timeout, ticks);
	ret = wait_event_interruptible_timeout(wait->wq, wait->woken_up, 
					       ticks);
	if (ret == 0)
		ret = -ETIMEDOUT;
	DEB_PARAMS("%s\n", (ret > 0) ? "Woken up" : 
		          ((ret == -ERESTARTSYS) ? "Interrupted" : "Timeout"));
	return (ret < 0) ? ret : 0;
}

void scdxipci_wakeup(struct data_acq *acq, struct img_frame *frame)
{
	struct img_frame_info *finfo;
	struct img_buffer *buffer, *fbuffer;
	struct list_head *list, *aux;
	struct acq_wait *wait;
	unsigned long ecount, bcount, frame_nr, acq_frame_nr;
	int i, wakeup, wokenup;

	FENTRY("scdxipci_wakeup");

	buffer = frame ? frame->buffer : NULL;
	frame_nr = frame ? frame->frame_nr : SCDXIPCI_ANY;
	bcount = buffer ? img_buffer_round_count(buffer) : SCDXIPCI_ANY;
	DEB_INTS("%s, %s, frame_nr=%ld\n", acq->name, 
		 buffer ? buffer->name : "buffer=NULL", frame_nr);

	i = wokenup = 0;
	list_for_each_safe(list, aux, &acq->wait_list) {
		wait = list_entry(list, struct acq_wait, wait_list);
		finfo = wait->frame_info;
		fbuffer = img_buffer_from_finfo(acq, finfo);
		ecount = acq_frame_nr = SCDXIPCI_ANY;  // default: wakeup all
		if (frame) {
			ecount = !is_any(finfo->frame_nr) ? frame->round_count 
							  : bcount;
			if (is_any(finfo->buffer_nr) && is_any(finfo->frame_nr))
				acq_frame_nr = acq->acq_frame_nr;
		}
		wakeup =  (!buffer || !fbuffer || 
			   (buffer == fbuffer));
		wakeup &= (is_any(frame_nr) || is_any(finfo->frame_nr) ||
			   (frame_nr == finfo->frame_nr));
		wakeup &= (is_any(ecount) || is_any(finfo->round_count) ||
			   (ecount == finfo->round_count));
		wakeup &= (is_any(acq_frame_nr) || is_any(finfo->acq_frame_nr)||
			   (acq_frame_nr == finfo->acq_frame_nr));
		if (wakeup) {
			DEB_PARAMS("waking up wait #%d\n", i);
			scdxipci_update_frame_info(acq, finfo, frame);
			wait->woken_up = 1;
			wake_up_interruptible(&wait->wq);
			list_del_init(&wait->wait_list);
			wokenup++;
		}
		i++;
	}
	DEB_INTS("Woken up %d processes\n", wokenup);
}


/*--------------------------------------------------------------------------
 * check wait
 *--------------------------------------------------------------------------*/

int scdxipci_acq_check_wait(struct data_acq *acq, unsigned long thread_id,
			    unsigned long timeout)
{
	int ret;
	unsigned long ticks;

	FENTRY("scdxipci_acq_check_wait");

#define thread_waiting	scdxipci_thread_is_waiting(acq, thread_id)

	ticks = scdxipci_timeout(timeout);
	DEB_PARAMS("thread_id=%ld, timeout=%ld, ticks=%ld\n", 
		   thread_id, timeout, ticks);
	ret = wait_event_interruptible_timeout(acq->wait_wq, thread_waiting,
					       ticks);
	if (ret == 0)
		ret = -ETIMEDOUT;
	DEB_PARAMS("%s\n", (ret > 0) ? "Found" : 
		          ((ret == -ERESTARTSYS) ? "Interrupted" : "Timeout"));
	return (ret < 0) ? ret : 0;

#undef thread_waiting
}

/*--------------------------------------------------------------------------
 * get stats
 *--------------------------------------------------------------------------*/

int scdxipci_get_stats(struct data_acq *acq, struct scdxipci_stats *usr_stats)
{
	unsigned long flags, i, nr_dev_stats;
	struct scdxipci_dev_stats *dev_stats;
	struct scdxipci_stats *stats = &acq->stats;

	scdxipci_lock_acq(acq, flags);
	*usr_stats = *stats;

	dev_stats = stats->dev_stats;
	nr_dev_stats = scdxipci_min(acq->nr_dev, SCDXIPCI_NR_DEV_STATS);
	for (i = 0; i < nr_dev_stats; i++, dev_stats++) {
		dev_stats->sg_avail_acc = dev_stats->sg_avail_count = 0;
		dev_stats->sg_avail_min = 0;
	}

	stats->copy_pages = stats->copy_usec = stats->copy_count = 0;
	stats->afn_call = stats->afn_usec = stats->afn_count = 0;
	stats->afn_iter = stats->afn_step  = 0;
	stats->afn_max_iter = stats->afn_max_step = 0;
	scdxipci_unlock_acq(acq, flags);

	return SCDXIPCI_OK;
}


/*--------------------------------------------------------------------------
 * validate sg table
 *--------------------------------------------------------------------------*/

int acq_dev_validate_sg_table(struct acq_dev *acq_dev, 
			      struct scdxipci_sg_table *sg_table)
{
	struct data_acq *acq = acq_dev->acq;
	struct scdxipci_sg_block *sg_desc;
	unsigned long i, frame_size, desc_len, garb_len;
        unsigned int addr;
	int align;

	FENTRY("acq_dev_validate_sg_table");

	align = scdxipci_dma_align(acq_dev->dev);

	// check the table is valid
	frame_size = acq->default_frame_size;
	garb_len = 0;
	sg_desc = sg_table->ptr;
	for (i = 0; i < sg_table->len; i++, sg_desc++) {
		addr = sg_desc->address;
		desc_len = sg_desc->size * 4;	// size is in dwords
		if (is_invalid32(addr)) {
			if (desc_len > garb_len)
				garb_len = desc_len;
		} else if ((addr >= frame_size) || 
			   ((addr + desc_len) > frame_size)) {
			DEB_ERRORS("SG desc. #%ld has invalid addr (%d) or "
				   "len (%ld)\n", i, addr, desc_len);
			return -EINVAL;
		} else if ((addr % PAGE_SIZE) + desc_len > PAGE_SIZE) {
			DEB_ERRORS("SG desc. #%ld cross page boundary: "
				   "addr=%d, size=%ld\n", i, addr, desc_len);
			return SCDXIPCI_ERR_BOUNDARY;
		} else if (((addr % align) != 0) || ((desc_len % align) != 0)) {
			DEB_ERRORS("SG desc. #%ld has invalid alignment "
				   "(must be %d): addr=%d, size=%ld\n", 
				   i, align, addr, desc_len);
			return SCDXIPCI_ERR_DMAALIGN;
		}
	}

	return garb_len;
}


/*--------------------------------------------------------------------------
 * set sg table
 *--------------------------------------------------------------------------*/

int scdxipci_acq_set_sg(struct data_acq *acq, unsigned long dev_idx,
			struct scdxipci_sg_table *sg)
{
	struct scdxipci_sg_block *new_sg_table, *old_sg_table;
	struct scdxipci_sg_table *sg_table;
	struct acq_dev *acq_dev;
	struct scdxipci_dev *scd_dev;
	unsigned long acq_flags, dev_flags, garb_len, len;
	unsigned long dma_pfn, dma_64bit_pfn_mask;
	struct img_frame_page *garb_page;
	gfp_t gfp_flags;
	void *ptr;
	int ret;

	FENTRY("scdxipci_acq_set_sg");
	DEB_PARAMS("dev_idx=%ld, sg-len=%ld\n", dev_idx, sg->len);

	if (sg->len > SCDXIPCI_SG_MAX_BLOCKS) {
		DEB_ERRORS("invalid SG table length: %ld (max=%ld)\n",
			   sg->len, (long) SCDXIPCI_SG_MAX_BLOCKS);
		return SCDXIPCI_ERR_BIGSG;
	}

	new_sg_table = old_sg_table = NULL;
	if (sg->len > 0) {
		// alloc copy of SG descriptor table ...
		len = sg->len * sizeof(*new_sg_table);
		ptr = vmalloc(len);
		if (ptr == NULL) {
			DEB_ERRORS("error alloc. %ld SG blocks (%ld bytes)\n",
				   sg->len, len);
			return -ENOMEM;
		}
		new_sg_table = (struct scdxipci_sg_block *) ptr;

		// ... and fill it 
		memcpy(new_sg_table, sg->ptr, len);
	}

	scdxipci_lock_acq(acq, acq_flags);

	ret = garb_len = 0;
	if (acq->nr_dev == 0) {
		DEB_ERRORS("%s has no dev. defined yet!\n", acq->name);
		ret = SCDXIPCI_ERR_ACQNODEV;
	} else if (dev_idx >= acq->nr_dev) {
		DEB_ERRORS("invalid dev. idx: %ld (acq->nr_dev=%ld)\n",
			   dev_idx, acq->nr_dev);
		ret = -EINVAL;
	} else if (is_current_acq(acq)){
		DEB_ERRORS("%s is running: can't change SG table\n", acq->name);
		ret = SCDXIPCI_ERR_RUNNING;
	}
	if (ret < 0)
		goto unlock;

	acq_dev = &acq->dev[dev_idx];
	scd_dev = acq_dev->dev;
	scdxipci_lock_dev(scd_dev, dev_flags);
	if (scd_dev->curr_acq != NULL) {
		DEB_ERRORS("%s is running: can't change SG table\n", 
			   scd_dev->name);
		ret = SCDXIPCI_ERR_DEVBUSY;
	} else {
		scdxipci_unmap_acq(scd_dev, acq);
		ret = acq_dev_validate_sg_table(acq_dev, sg);
		if (ret >= 0)
			garb_len = ret;
	}
	scdxipci_unlock_dev(scd_dev, dev_flags);

	if (ret < 0)
		goto unlock;

	// check if garbage area needs to be (re)allocated
	if (garb_len > acq->garbage_len) {
		if (acq->garbage_len > 0) {
			scdxipci_frame_page_free(acq->garbage_page);
			acq->garbage_len = 0;
		}
	} else
		garb_len = 0;

	// remember existing one (to free later) and install new
	sg_table = &acq_dev->sg_table;
	if (sg_table->len > 0)
		old_sg_table = sg_table->ptr;
	sg_table->ptr = new_sg_table;
	sg_table->len = sg->len;
	acq_dev->sg_table_validated = 1;
	new_sg_table = NULL;
		       
 unlock:
	scdxipci_unlock_acq(acq, acq_flags);

	if (new_sg_table != NULL)
		vfree(new_sg_table);
	if (old_sg_table != NULL)
		vfree(old_sg_table);

	if (ret < 0)
		return ret;

	if (garb_len > 0) {
		DEB_PARAMS("allocating %ld bytes for %s garbage area\n",
			   garb_len, acq->name);
		if (garb_len > PAGE_SIZE) {
			DEB_ERRORS("Oops: allocating garbage area larger than "
				   "one page: %ld bytes\n", garb_len);
			return -ENOMEM;
		}
		gfp_flags = img_frame_gfp_flags(SCDXIPCI_BUFF_AUX);
		garb_page = scdxipci_frame_page_alloc(gfp_flags, NULL, 0);
		if (garb_page == NULL) {
			DEB_ERRORS("error alloc. garbage area (%ld bytes)\n", 
				   garb_len);
			return -ENOMEM;
		} else if (!acq_dev_has_dma64(acq_dev)) {
			dma_pfn = page_to_pfn(garb_page->page);
			dma_64bit_pfn_mask = 0xffffffffLL << (32 - PAGE_SHIFT);
			DEB_PARAMS("dma_pfn=0x%08lx, "
				   "dma_64bit_pfn_mask=0x%08lx\n",
				   dma_pfn, dma_64bit_pfn_mask);
			dma_pfn &= dma_64bit_pfn_mask;
			DEB_PARAMS("dma_pfn[64-bit]=0x%08lx\n", dma_pfn);
			if (dma_pfn != 0) {
				DEB_ERRORS("Ooops: HW isn't 64-bit capable and "
					   "garbage area has 64-bit addr\n");
				scdxipci_frame_page_free(garb_page);
				return SCDXIPCI_ERR_MEMTOOHIGH;
			}
		}
	
		scdxipci_lock_acq(acq, acq_flags);
		acq->garbage_page = garb_page;
		acq->garbage_len = garb_len;
		scdxipci_unlock_acq(acq, acq_flags);
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * set dev list
 *--------------------------------------------------------------------------*/

int scdxipci_acq_set_dev(struct data_acq *acq, int *dev_list,
			 unsigned long nr_dev)
{
	struct scdxipci_dev *scd_dev;
	struct acq_dev *prev_dev_arr, *acq_dev_arr = NULL;
	unsigned long flags, i, prev_nr_dev;
	int nr, ret = 0;

	FENTRY("scdxipci_acq_set_dev");
	DEB_PARAMS("nr_dev=%ld\n", nr_dev);

	if (nr_dev > 0) {
		acq_dev_arr = scdxipci_acq_dev_alloc(acq, nr_dev);
		if (acq_dev_arr == NULL)
			return -ENOMEM;

		for (i = 0; i < nr_dev; i++) {
			nr = dev_list[i];
			scd_dev = scdxipci_find_dev(nr);
			if (scd_dev == NULL) {
				ret = -ENODEV;
				goto free;
			}
			DEB_PARAMS("adding dev. #%d at idx %ld\n", nr, i);
			acq_dev_arr[i].dev = scd_dev;
		}
	}

	scdxipci_lock_acq(acq, flags);

	if (is_current_acq(acq)) {
		DEB_ERRORS("%s is running: can't change dev list\n", acq->name);
		ret = SCDXIPCI_ERR_RUNNING;
		goto unlock;
	}

	prev_nr_dev = acq->nr_dev;
	if (prev_nr_dev > 0) {
		prev_dev_arr = acq->dev;
		acq->dev = NULL;
		acq->nr_dev = 0;
		scdxipci_unlock_acq(acq, flags);
		scdxipci_acq_dev_free(prev_dev_arr, prev_nr_dev);
		scdxipci_lock_acq(acq, flags);
	}
	if (nr_dev == 0)
		goto unlock;
	
	acq->dev = acq_dev_arr;
	acq->nr_dev = nr_dev;
	acq_dev_arr = NULL;

 unlock:
	scdxipci_unlock_acq(acq, flags);

 free:
	if (acq_dev_arr != NULL)
		scdxipci_acq_dev_free(acq_dev_arr, nr_dev);

	return ret;
}


