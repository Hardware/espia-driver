/****************************************************************************
 * File:	scdxipci_main.c
 * $Header: /segfs/bliss/cvs/driver/ESRF/espia/src/scdxipci_main.c,v 2.31 2017/11/15 12:19:30 ahoms Exp $
 * Project:	SECAD Espia card Linux driver
 * Description:	Driver module main file
 * Author(s):	SECAD S.A.
 *		A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 *
 * $Log: scdxipci_main.c,v $
 * Revision 2.31  2017/11/15 12:19:30  ahoms
 * * Ported to gcc-4.9, Linux 3.16 kernels and Qt4
 * * Avoid 64-bit DMA if pci_set_dma_mask(64) fails
 * * Refactored FW capabilities definition, now using capability sets
 * * Improved error checking and reporting in espia_[acq_]lib
 *
 * Revision 2.30  2016/03/07 11:39:37  ahoms
 * * Fixed bug crashing PC when a board with unknown firmware issues an IRQ
 * * Moved KEEP_RELEASED_PAGES compilation flag to acq. driver option
 * * Added GOOD_RESET_SG board capability, introduced in firmware 2014.12.03,
 *   restoring the previously disabled DMA to 64-bit pages functionality
 *
 * Revision 2.29  2015/08/19 18:02:37  ahoms
 * * Changed garbage area allocation from kmalloc(KERNEL) to
 *   scdxipci_frame_page_alloc(BUFF_AUX) to ensure a 32-bit address
 * * Added KEEP_RELEASED_PAGES compilation flag to detect anomally
 *   written pages by Espia-Express after they have been released
 *
 * Revision 2.28  2012/12/07 16:13:27  ahoms
 * * Using FW-specific capabilities (scd_dev_has_cap):
 *   + CAP_BAD_SER_TIMING: software fix (1 us wait) for problems due
 *     to bad serial line EspiaExpress FPGA timing
 * * Added 2012.12.03 Espia-Express firmware
 * * Modified espia_ser_read_str to not return ETIMEDOUT if some
 *   bytes were read but the specified terminator is not found
 * * Added Python EspiaAcqLib.EspiaDev[Class].resetLink method
 *
 * Revision 2.27  2012/10/23 09:17:28  ahoms
 * * Updated the Espia-Express flash length to the definitive value
 * * Added delay in flash init. sequence, forced reset-to-read-array
 * * Introduced first FW for final Espia-Express series,
 *   added ".P" suffix to prototype FWs
 *
 * Revision 2.26  2012/09/04 09:27:49  ahoms
 * * Fixed Espia-Express FLASH initialisation sequence (no read between writes)
 * * Added Espia-Express FW versions 2012.07.17 and 2012.08.02
 * * Added write/readRegister to EspiaDev[Class] in EspiaAcqLib.py
 *
 * Revision 2.25  2012/06/15 10:33:35  ahoms
 * * Fixed problems in RedHat EL 4 with 64-bit pages:
 *   + Typo bug in ESRFVER_REMAP_PAGE_32 conditional,
 *     hiding error when remapping user pages
 *   + Added systematic verification of potential problems by
 *     checking num_physpages [SCDXIPCI_ERR_TOOMUCHRAM]
 * * Added Espia-Express 2012.05.22 and 2012.06.13 FW checksums
 * * Fixed EspiaAcqLib.FoclaDevClass.getTriggerMode signature
 *
 * Revision 2.24  2012/04/18 12:22:42  ahoms
 * * Added DCMs USER_RESET_NOT control register bit to espia parameter list
 * * Included unified esrfversion.h global errno support (needed by syscalls)
 *
 * Revision 2.23  2011/05/06 11:43:51  ahoms
 * * Moved SG kernel thread to a high priority tasklet (faster than net)
 * * Map next SG table at end of IRQ hander as last resort to avoid overrun
 * * Increased max. number of acc. IRQs before EOF tasklet to 16
 * * Added kernel flash CRC verification and reporting in board_id info
 * * Added Espia-Express flash board_id reading; included firmware 2011.04.22
 *
 * Revision 2.22  2011/05/02 17:22:45  ahoms
 * * Support 64-bit address DMA (SG) on Espia-Express, requiring
 *   the usage of pci_set_consistent_dma_mask
 * * Calling pci_clear_master when releasing a Espia board
 * * Properly report frame valid pixels (dwords) when using SG rois
 * * Do not reset next_frame->nr_pixels in DMA IRQ handler if at
 *   end-of-acq., which is typically the first acquired frame
 * * Support Linux 2.4 kernel thread compatibility features
 *
 * Revision 2.21  2011/04/08 13:23:28  ahoms
 * * Implemented firmware reading in kernel at device probe
 * * Added scdxipci_board with ser_nr and firmware info,
 *   responsible of common board resource enabling/allocation,
 *   and the possibility of having 1, 2 or 4 logical scdxipci_devs
 * * Added HW_INFO IOCTL returning:
 *   + board/device types/numbers
 *   + flash board_firmware info (start, len, checksum, name)
 *     and reading
 *   + flash board_id info (start, len, ser/nr) and reading
 *
 * Revision 2.20  2011/02/10 11:11:08  ahoms
 * * Moved serial readout code from ISR to a kernel tasklet
 * * Do not disable INTERRUPT_ON_SERIAL in ISR; acknowledgement
 *   with INTERRUPT_ON_SERIAL_CLEAR is enough
 * * Do not print overflow messages on every serial byte transfer,
 *   instead notify only once per IRQ (including nb. of bytes)
 * * Iterate in a loop in ISR to be sure that all the IRQ sources
 *   have been ackowledged, necessary for edge-sensitive MSI implementation
 *
 * Revision 2.19  2011/01/28 09:13:15  ahoms
 * * Created struct scdxipci_dev_type to distinguish different Espia models;
 *   in the Espia-Express case, allocate an array of 4 struct scdxipci_dev.
 * * Separated logical sub-device initialisation/cleanup from PCI device
 *   detection scdxipci_probe/remove, now in scdxipci_dev_add/del.
 *
 * Revision 2.18  2010/12/21 14:50:37  ahoms
 * * Using SSE2 (ASM) memcpy code from William Chan frame copy to hi-mem
 * * Added EspiaExpress device ID to the supported board list
 *
 * Revision 2.17  2008/07/23 13:24:54  blissadm
 * Using generic IRQ handler definition (for Linux 2.6.19)
 *
 * Revision 2.16  2008/05/17 17:53:26  ahoms
 * Started re-organization of frame post-op structures; using library function
 * to retrie table entries for ensuring binary compatibility
 * Address frame post-ops by their names in espia_test; splited FramePostOpList.
 * First implementation of VERT_2_QUAD frame post-op for Aviex detector.
 * Implemented endless FOCLA CL CC pulse trains.
 * Added FOCLA_PULSE_WAIT_FRAME: wait for next frame before next pulse stage.
 * Return -EFAULT after an invalid user space pointer instead of -EACCES.
 * Using static _espia_lib_h_revision when checking compiled/running version.
 *
 * Revision 2.15  2008/05/02 10:47:33  ahoms
 * Added simu_cond_flags module parameter for simulating error conditions.
 * Fixed bug crashing the PC when pci_map_frame was called after stop_acq.
 * Allow eof_tasklet to process up to 3 EOF events before aborting.
 * Pass all the EOF events to memcpy thread if active to
 * warantee sequencial frame order (prev. commit); protect agains abort.
 * Fixed bug creating a dead-lock when buffersave file open failed.
 *
 * Revision 2.14  2008/04/28 13:50:00  ahoms
 * Moved SG table mapping to dedicated thread.
 * Increased the number of SG tables per devices to 5,
 * and decreased the number of aux. frames also to 5.
 * Added SG table availability to acq. statistics.
 * Lock buffers used by the memcpy or SG threads so
 * they are not freed until the processing finishes.
 * Corrected bug in scdxipci_frame_copy kunmap parameters.
 * Using string buffer when generating EDF header espia_test.
 *
 * Revision 2.13  2008/03/18 08:50:28  ahoms
 * Moved next frame SG table preparation from IRQ handler to EOF tasklet.
 * Limited to one the number of frames handled in EOF tasklet.
 * Corrected file names in serial_mmap_close [MP].
 *
 * Revision 2.12  2008/03/02 17:32:21  ahoms
 * Added support for driver options; implemented DEBUG_LEVEL and NO_FIFO_RESET.
 * Fixed bug crashing PC on redhate5 when SG is active without a garbage region.
 * Dev. SG lists for PCI frame mapping are now vmalloc'ed at driver startup.
 * Added cal. time, time of day, and time of frame in EDF header (espia_test).
 * Disable IRQs during memcopy & lock acq. when updating stats. in EOF tasklet
 * scdxipci_dma_align must now be called with dev. locked.
 * Check specified devices to set_dev before allocating acq_dev array.
 *
 * Revision 2.11  2008/02/13 19:25:06  ahoms
 * Ported to Linux 2.6.19
 *
 * Revision 2.10  2008/02/12 11:31:32  ahoms
 * Increased the maximum SG table length to 128 KB (16 K desc, 64 MB frames)
 * Added SG table overflow checking.
 * Using vmalloc for user defined SG tables.
 *
 * Revision 2.9  2008/02/08 14:20:41  ahoms
 * Ported to x86_64 architecture.
 * Implemented multiple SG tables for low frame switch overhead,
 * next frame is mapped just after the DMA is started.
 * Input data FIFO is not erased on start from second frame on.
 *
 * Revision 2.8  2007/10/23 15:25:18  ahoms
 * Ported to Linux 2.6.18; using class_device instead of class_simple.
 * Unified code with Linux 2.4 through esrfversion.h.
 * Changed scdxipci_private.h macros with static inline functions.
 * Espia and FOCLA parameter tables are no longer exported (for future
 * binary compatibility), they can be browsed with get_param_data.
 * Updated espia parameters: DO_NOT_RESET_FIFOS and ESPIA_125MHZ.
 * Added TRIG and TEST_IMAGE FOCLA parameters; implemented focla_strerror.
 * Implemented retrieval of card ID and firmware data.
 * Using generic image interface in espia_test.
 * Improved scdxipci_find_by_buffer_frame code.
 *
 * Revision 2.7  2007/08/23 17:20:40  ahoms
 * Ported to Linux 2.6: using pci_name/enable_device/set_master;
 * using "espia%d" for resource allocation name; not setting PCI dev. name;
 * implemented sysfs class_simple devices; serial Tx uses kthread.
 * [Temporarly] disabled "very high vemory" (64-bit) buffers.
 * Adapted to new circ_buffer_getc/putc flags (CIRC_BUFFER_KTHREAD).
 * Moved app_framework, circ_buffer and image to driver common.
 * Making proper dev. initialization sequence.
 * Improved buffer array management so it does not have empty entries;
 * using long long integer arithmetic instead of double in get_frame/AFN.
 * Moved focla_param to espia_param, implemented basic Espia parameters.
 * Changed write_register to accept the mask of bits to modify;
 * verify the proper register offset alignment.
 * Renamed focla_lib functions to follow espia_lib style.
 * Fixed several espia_test bugs.
 *
 * Revision 2.6  2007/07/10 19:09:59  ahoms
 * Remove acknowledgement potential serial IRQs on every FIFO read
 * (big overhead); temporally disable interrupts instead.
 * Check if serial FIFO becomes full and warn a message.
 * Fixed SCDXIPCI_IOC_WRITE_REGISTER code to return the original reg. value.
 *
 * Revision 2.5  2007/07/06 14:47:49  ahoms
 * Simplified cb thread termination: removed
 * synchronization and cancelation (bug in SuSE 8.2 pthread)
 *
 * Revision 2.4  2007/07/03 14:08:45  ahoms
 * Implemented Espia RoI through SG lists: xfers
 * with invalid frame addr. are dump into a garbage buffer.
 * Check SG desc. are properly aligned if using 64-bit xfers.
 * Let RESET_SCATTER_GATHER active when aborting an acq. so
 * next frame from CCD is aborted immediately.
 *
 * Revision 2.3  2007/04/25 16:50:52  ahoms
 * Support LARGEFILE (> 2GB) in espia_test program
 *
 * Revision 2.2  2007/02/20 17:22:10  ahoms
 * Fixed acq_dev locking with a 5 usec delay before reacquiring lock.
 * Protect driver get_frame when freeing (loosing) buffers.
 * Using a new optimized algorithm in acq_frame_nr search.
 * Added acq_frame_nr info to acq. stats; incl. cb_nr in espia_cb_data.
 * Implemented parallel port debug, including heartbeat timer.
 * Using new app_framework context interface (alloc, init, err/out).
 * Improved error checking and report. in driver and espia_test.
 * In espia_test:
 *   Centralized callback management
 *   Implemented RoI Accumulation buffer
 *   Allow saving multiple acq. frames in a single file
 *   Acq. status shows last acq/saved frame from callbacks
 *
 * Revision 2.1  2007/01/13 12:30:00  ahoms
 * Added user-specified scatter gather lists.
 * Implemented meta device (minor=255), controlling
 * acquisitions on one or more real devices.
 * Acquisition lock independent of devices.
 * Dev. cmds (ser-line, reg. R/W, etc.) forbidden in meta acq.
 * Completed pci_unmap_frame by clearing dev. sg-list;
 * clear the sg-list pointer register on driver exit.
 * End-of-frame treatment (including memcpy if necessary)
 * is done in tasklet (IRQ enabled).
 * Increased to infitity nr. of retries when
 * allocating 32-bit only frame pages.
 * Implemented vert. flip sg-list on 1 and 2 adapters.
 * Added espia_frame_dim; always passed to frame post-op.
 *
 * Revision 2.0  2006/12/13 17:43:19  ahoms
 * Merged release 1.99.1.14 into main trunk
 *
 * Revision 1.99.1.14  2006/12/01 15:55:15  ahoms
 * Corrected serial read timeout to expire when the user requested.
 * Prepared term. search in drv. serial read: not implemented yet in circ_buffer
 * Avoid extra serial interrupts by acknowledging after every read.
 * Added espia_read/write/register; corrected reg. checking in driver.
 * Added frame post-op functionality with buffer-based thread protection;
 * implemented interlaced pixel swap, vert. flip and check for NULL lines.
 * Added buffer clear, auto-save and live-image functionalities to espia_test.
 * Modify kernel mapping of 64-bit frame pages to user space.
 * Set the time in the frame notifying that acq. finished.
 * Split espia_test to separate main from test routines (for Python wrapper).
 * Check for NULL pointers in parameters to espia_lib functions.
 * Avoid registering acq. callbacks when an acq. is running.
 * Using thread synchronization when terminating or finishing callback threads.
 * Fixed bug of EACH_BUFFER callback aborting when acq. finished.
 * Implemented proper clean-up in espia_close;
 * do the same in espia_test close_dev.
 * Using __attribute__ unused for RCS ID strings.
 *
 * Revision 1.99.1.13  2006/10/23 13:33:35  ahoms
 * Added support of 64-bit memory pages, not directly accessible by the card;
 * using temporary aux. kernel buffer when frames contain such pages.
 * Implemented acquisition statistics, to be read by the user.
 * Free already allocated buffers when buffer allocation fails.
 * Fixed bug when waiting for cb. thread that already finished.
 * Added buffer dump and save in espia test program.
 *
 * Revision 1.99.1.12  2006/10/03 17:54:36  ahoms
 * Added FOCLA interface library
 *
 * Revision 1.99.1.11  2006/10/03 09:50:37  ahoms
 * Both driver and library check for the same version string in dev. open.
 * Access buffers in kernel from an array instead of a linked list;
 * all user space layers specify buffers by their nr and not their addr.
 * Removed espia_frame_info, moved buffer_info and frame_address to driver.
 * Library now specifies the frame_size instead of the frames_per_buffer.
 * Cancel acq. threads only outside callback execution.
 * Added Qt-based buffer display with efficient thread mechanism.
 * Using standard wait_event_interruptible_timeout return code.
 *
 * Revision 1.99.1.10  2006/06/07 09:22:39  ahoms
 * Included new Espia firmware (05 May 2006): new subsystem vendor/dev ID.
 * Activate the PCI burst transfer mode in espia_open.
 *
 * Revision 1.99.1.9  2006/04/13 16:56:14  ahoms
 * Changed espia_ser_read_str to wait only for first terminator
 *
 * Revision 1.99.1.8  2005/12/09 15:15:58  ahoms
 * Fixed bug in circ_buffer misinterpreting character 0xff.
 * Increased kernel serial buffers to 128KB (one PRIAM config. block).
 * Corrected debug message in espia_ser_write.
 *
 * Revision 1.99.1.7  2005/10/25 19:49:14  ahoms
 * Added CCD status and serial Rx callback support.
 * Return timeout as a (negative) error.
 * Removed flags from espia_start_acq.
 *
 * Revision 1.99.1.6  2005/09/20 14:58:22  ahoms
 * Atomic register read/write from user space.
 * Improved serial line blocking and timing; added block xfer.
 * Implemented DMA overrun notification in callbacks.
 * Added CCD status with blocking capabilities.
 *
 * Revision 1.99.1.5  2005/08/28 22:09:26  ahoms
 * Added DMA acq. active call; introduced the acq. run number
 * to solve the problem of wait requests between acquisitions.
 * Implemented espia_ser_read_str and espia_frame_address.
 * Using thread mutex & cond. to activate/deactivate callbacks.
 * Implemented ESPIA_EACH_BUFFER/FRAME in callbacks.
 *
 * Revision 1.99.1.4  2005/08/23 02:50:42  ahoms
 * Implemented buffer and acquisition functions in espia_lib
 *
 * Revision 1.99.1.3  2005/08/21 23:45:33  ahoms
 * Implemented image acquisition and (flexible, blocking) get frame.
 *
 * Revision 1.99.1.2  2005/08/17 23:24:54  ahoms
 * Implemented buffer memory mapping with multiple frames (big buffer).
 * Using linked lists and lookaside caches for optimum memory use.
 *
 * Revision 1.99.1.1  2005/08/15 21:35:04  ahoms
 * Using new circ_buffer features; Tx kernel thread always alive.
 * Improved timeout and interrupt control.
 * Renamed scdxipci_dev field minor by dev_nr; open possibility of
 * having several minors per device.
 *
 * Revision 1.99  2005/08/12 15:05:46  ahoms
 * Rewritten driver from scratch. Using kernel PCI driver interface.
 * Implemented a generic menu-driver test application framework.
 * Implemented serial line with circular buffers and TX thread.
 *
 * Revision 1.1-1.16  2005/03/11-2005/06/06  ahoms
 * Improved driver stability with spinlocks and (high)memory management.
 * Implemented reset_link, ser_put/get, read_ccd_status in driver.
 * Multiple frames acquisition independent of user program readout speed.
 *
 * Revision 1.0  2005/02/12 12:00:00  fernande
 * Multithread version
 *
 * Revision 0.1  2003/06/14 12:00:00  fernande
 * Initial Release from SECAD
 *
 ****************************************************************************/

#include "scdxipci_private.h"

#include <linux/init.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/highmem.h>
#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,11,0)
#include <linux/sched/signal.h>
#endif
#include <asm/uaccess.h>

/*--------------------------------------------------------------------------
 * Driver version and RCS Id 
 *--------------------------------------------------------------------------*/

char drv_revision[] = "$Name: v3_12 $";
static char rcs_id[] __attribute__((unused)) = 
	"$Header: /segfs/bliss/cvs/driver/ESRF/espia/src/scdxipci_main.c,v 2.31 2017/11/15 12:19:30 ahoms Exp $";


/*--------------------------------------------------------------------------
 * PCI device type list
 *--------------------------------------------------------------------------*/

struct pci_device_id scdxipci_dev_list[] = {
	{SCDXIPCI_VENDOR_ID,		SCDXIPCI_DEV_ID, 
	 SCDXIPCI_SUBVENDOR_ID_0,	SCDXIPCI_SUBDEV_ID_0, 
	 SCDXIPCI_CLASS,		SCDXIPCI_CLASS_MASK,
	 SCDXIPCI_ESPIA_PCI},
	{SCDXIPCI_VENDOR_ID,		SCDXIPCI_DEV_ID, 
	 SCDXIPCI_SUBVENDOR_ID_1,	SCDXIPCI_SUBDEV_ID_1, 
	 SCDXIPCI_CLASS,		SCDXIPCI_CLASS_MASK,
 	 SCDXIPCI_ESPIA_PCI},
	{SCDXIPCI_VENDOR_ID,		SCDXIPCI_DEV_ID_2, 
	 SCDXIPCI_SUBVENDOR_ID_2,	SCDXIPCI_SUBDEV_ID_2, 
	 SCDXIPCI_CLASS,		SCDXIPCI_CLASS_MASK,
 	 SCDXIPCI_ESPIA_EXPRESS},
	{0,}
};

struct firmware_data espia_pci_firmware_list[] = {
	{"2004.06.18", 0x2fe8},
	{"2006.05.05", 0x4df5},
	{"2007.07.04", 0xa306},
	{"2007.09.27", 0xae05},
	{"2008.07.22", 0xb9fd},
	{""}
};

#define FW(date, cksum, cap_set)				\
	{#date, cksum, capabilities: SCDXIPCI_CAP_SET_##cap_set}

struct firmware_data espia_express_firmware_list[] = {
	// ".P"-suffixed firmwares are for Prototype boards only
	FW(2011.04.01.P, 0x96dc, P1),
	FW(2011.04.08.P, 0xe510, P1),
	FW(2011.04.22.P, 0x180b, P1),// Good 32/64-bit RD [SG] & WR [data] TLP
	FW(2012.05.22.P, 0x4acb, P1),// Fix PCI-e link init. in different slots
	FW(2012.06.13.P, 0x1206, P1),// Fix differences in reset mechanisms
	FW(2012.07.17.P, 0x3221, P1),
	FW(2012.08.02.P, 0xb641, P2),// Fix serial line frame errors
	FW(2012.09.01,   0x7957, 1), // First Espia-Express firmware
	FW(2012.12.03,   0x7b3e, 2), // Fix serial line synch & transciever ctrl
	FW(2014.12.03,   0x07e4, 3), // Restore RESET_SCATTER_GATHER function
	{""}
};

#undef FW

struct board_type scdxipci_board_type_list[SCDXIPCI_NR_BRD_TYPES] = {
	{SCDXIPCI_ESPIA_PCI,		"Espia-PCI",		
	 1,				256,
	 0,				SCDXIPCI_PCI_FW_LEN, 
	 espia_pci_firmware_list,	
	 SCDXIPCI_PCI_BRD_ID_START,	SCDXIPCI_PCI_BRD_ID_LEN},
	{SCDXIPCI_ESPIA_EXPRESS,	"Espia-Express",	
	 4,				64,
	 0,				SCDXIPCI_EXPRESS_FW_LEN,
	 espia_express_firmware_list,	
	 SCDXIPCI_EXPRESS_BRD_ID_START,	SCDXIPCI_EXPRESS_BRD_ID_LEN},
};

/*--------------------------------------------------------------------------
 * PCI driver structure
 *--------------------------------------------------------------------------*/

struct pci_driver scdxipci_driver = {
	name:		SCDXIPCI_DRV_NAME,
	id_table:	scdxipci_dev_list,
	probe:		scdxipci_probe,
	remove:		scdxipci_remove,
};


/*--------------------------------------------------------------------------
 * Kernel module interface
 *--------------------------------------------------------------------------*/

MODULE_AUTHOR("SECAD S.A. & A. Homs, BLISS, ESRF (ahoms@esrf.fr)");
MODULE_DESCRIPTION("SECAD Espia card Linux driver");
MODULE_LICENSE("GPL");

DECLARE_GLOBAL_DEBUG_LEVEL(debug_level, SCDXIPCI_DEB_PROBE);
module_param(debug_level, int, S_IRUGO | S_IWUSR | S_IWGRP);
MODULE_PARM_DESC(debug_level, "Driver syslog debug level: 0=None, 6=INTS");

int major = 0;
module_param(major, int, S_IRUGO);
MODULE_PARM_DESC(major, "Driver major number: 0=Auto (default)");

int simu_cond_flags = 0;
module_param(simu_cond_flags, int, S_IRUGO | S_IWUSR);
MODULE_PARM_DESC(simu_cond_flags, "Flags for simulation of error conditions");

MODULE_DEVICE_TABLE(pci, scdxipci_dev_list);


/*--------------------------------------------------------------------------
 * Driver data
 *--------------------------------------------------------------------------*/

// flags containing the initialized subsystems
init_flags_t mod_init_flags;

#ifndef ESRFVER_NO_CLASS_DEV

// the class device attributes
#ifdef ESRFVER_CLASS_DEV_ATTRS
ESRFVER_CLASS_DEV_ATTR_TYPE scdxipci_class_dev_attr[] = {
#ifdef ESRFVER_CLASS_DEV_NO_DEVT
    __ATTR(dev, S_IRUGO, scdxipci_class_dev_read_dev, NULL),
#endif
    __ATTR_NULL,
};
#endif

// the device class
#define ESRFVER_CLASS_DEV_NO_OWNER 1
struct class scdxipci_class = {
#ifndef ESRFVER_CLASS_DEV_NO_OWNER
	owner:		THIS_MODULE,
#endif
	name:		SCDXIPCI_DRV_NAME,
#ifdef ESRFVER_CLASS_DEV_STC
	release:	scdxipci_class_dev_release,
	class_dev_attrs:scdxipci_class_dev_attr,
#else
	dev_release:	scdxipci_class_dev_release,
#endif
};

// the meta acq class device
ESRFVER_CLASS_DEV_TYPE *scdxipci_meta_class_dev;

#endif // ESRFVER_NO_CLASS_DEV

// the list of controlled devices
struct list_head dev_list;

// number of detected boards
int nr_board[SCDXIPCI_NR_BRD_TYPES];

// number of log devices
int nr_dev;

// the caches for buffers and frames
kmem_cache_t *buffer_cache, *frame_cache, *frame_page_cache;

// parport debug
unsigned short scdxipci_ppdeb_val[PARPORT_NR];
spinlock_t scdxipci_ppdeb_lock;

// kthread & sched_setschedule might need errno
#if ESRFVER_NEEDS_GLOBLAL_ERRNO(1, 1)
ESRFVER_DEFINE_GLOBAL_ERRNO;
#endif

/*--------------------------------------------------------------------------
 * Driver initialization / cleanup
 *--------------------------------------------------------------------------*/

module_init(scdxipci_driver_init);
module_exit(scdxipci_driver_exit);

void scdxipci_driver_exit_func(void);

int __init scdxipci_driver_init(void)
{
	int ret;
	init_flags_t *iflags = &mod_init_flags;
	char *drv_name = SCDXIPCI_DRV_NAME;
	char *rev, buffer[PRETTY_RCS_REV_LEN];
#ifndef ESRFVER_NO_CLASS_DEV
	int len;
	ESRFVER_CLASS_DEV_TYPE *class_dev;
#endif

#define mod_cache_alloc(x, X)						\
	x##_cache = kmem_cache_create(SCDXIPCI_DRV_NAME "_" #x,		\
				      sizeof(struct img_##x), 0, 0,	\
				      ESRFVER_KMEM_CACHE_CTOR_DTOR);	\
	if (x##_cache == NULL) { 					\
		DEB_ERRORS("Error creating " #x " cache"); 		\
		ret = -ENOMEM;						\
		goto err;						\
	}								\
	init_flags_set(iflags, MOD_INIT_##X##_CACHE);

	FENTRY("scdxipci_driver_init");

	rev = pretty_rcs_revision(buffer, drv_revision);
	printk("SECAD Espia card driver, %s\n", rev);

	init_flags_init(iflags);
	spin_lock_init(&scdxipci_ppdeb_lock);

	ret = register_chrdev(major, drv_name, &scdxipci_file_ops);
	if (ret < 0) {
		DEB_ERRORS("Error registering character device (major=%d)\n",
			   major);
		goto err;
	}
	major = ret;
	DEB_ALWAYS("Major: %d\n", major);
	init_flags_set(iflags, MOD_INIT_CHR_DEV);

#ifndef ESRFVER_NO_CLASS_DEV
	ret = class_register(&scdxipci_class);
	if (ret < 0) {
		DEB_ERRORS("Error registering class %s\n", drv_name);
		goto err;
	}
	init_flags_set(iflags, MOD_INIT_CLASS);
#endif

	mod_cache_alloc(frame_page, FRAME_PAGE);
	mod_cache_alloc(frame,      FRAME);
	mod_cache_alloc(buffer,     BUFFER);

	INIT_LIST_HEAD(&dev_list);
	ret = pci_register_driver(&scdxipci_driver);
	if (ret < 0) {
		DEB_ERRORS("Error registering PCI driver: ret=%d\n", ret);
		goto err;
	}
	init_flags_set(iflags, MOD_INIT_PCI_DRV);

#ifndef ESRFVER_NO_CLASS_DEV
	len = sizeof(*class_dev);
	class_dev = (ESRFVER_CLASS_DEV_TYPE *) kmalloc(len, GFP_KERNEL);
	if (class_dev == NULL) {
		ret = -ENOMEM;
		DEB_ERRORS("error allocating %smeta class device\n", drv_name);
		goto err;
	}
	memset(class_dev, 0, len);

	ESRFVER_CLASS_DEV_INITIALIZE(class_dev);
	class_dev->class = &scdxipci_class;
	ESRFVER_CLASS_DEV_SET_NAME(class_dev, "%smeta", drv_name);
	ESRFVER_CLASS_DEV_SET_DEV(class_dev, NULL);
#ifndef ESRFVER_CLASS_DEV_NO_DEVT
	class_dev->devt = scdxipci_cdev_nr_from_class(class_dev);
#endif       
	scdxipci_meta_class_dev = class_dev;
	init_flags_set(iflags, MOD_INIT_META_CLASS_DEV_OBJ);

	ret = ESRFVER_CLASS_DEV_ADD(class_dev);
	if (ret < 0) {
		DEB_ERRORS("error adding class device %s\n", 
			   ESRFVER_CLASS_DEV_NAME(class_dev));
		goto err;
	}
	init_flags_set(iflags, MOD_INIT_META_CLASS_DEV_ADD);
#endif
		
	return 0;

 err:
	scdxipci_driver_exit_func();
	return ret;

#undef mod_cache_alloc
}

void __exit scdxipci_driver_exit(void)
{
	scdxipci_driver_exit_func();
}

void scdxipci_driver_exit_func(void)
{
	init_flags_t *iflags = &mod_init_flags;
#ifdef ESRFVER_UNREGISTER_CHRDEV_RET
	int ret;
#endif

	FENTRY("scdxipci_driver_exit_func");

#define mod_exit(statement, what) \
	do { \
		DEB_PARAMS("%s\n", what); \
		statement; \
	} while (0)
#define mod_exit_check_statement(expr, what) \
	do { \
		if ((ret = (expr)) != 0) \
			DEB_ERRORS("Error %s (ret=%d)!!\n", what, ret); \
	} while (0)
#define mod_exit_check(expr, what) \
	mod_exit(mod_exit_check_statement(expr, what), what)
#ifdef ESRVER_KMEM_CACHE_DESTROY_RETURN
#define mod_exit_cache_destroy(expr, what)	mod_exit_check(expr, what)
#else
#define mod_exit_cache_destroy(expr, what)	mod_exit(expr, what)
#endif
#define mod_cache_destroy(x) \
	mod_exit_cache_destroy(kmem_cache_destroy(x##_cache), \
			       "destroying " #x " cache")

#ifdef ESRFVER_UNREGISTER_CHRDEV_RET
#define chrdev_exit(expr, what)		mod_exit_check(expr, what)
#else
#define chrdev_exit(expr, what)		mod_exit(expr, what)
#endif
	
#ifndef ESRFVER_NO_CLASS_DEV
	if (init_flags_check(iflags, MOD_INIT_META_CLASS_DEV_ADD))
		mod_exit(ESRFVER_CLASS_DEV_DEL(scdxipci_meta_class_dev),
			 "removing meta acq. class dev");
	if (init_flags_check(iflags, MOD_INIT_META_CLASS_DEV_OBJ))
		mod_exit(ESRFVER_CLASS_DEV_PUT(scdxipci_meta_class_dev),
			 "releasing meta acq. class dev");
#endif
	if (init_flags_check(iflags, MOD_INIT_PCI_DRV))
		mod_exit(pci_unregister_driver(&scdxipci_driver),
			 "unregistering PCI driver");
	if (init_flags_check(iflags, MOD_INIT_BUFFER_CACHE))
		mod_cache_destroy(buffer);
	if (init_flags_check(iflags, MOD_INIT_FRAME_CACHE))
		mod_cache_destroy(frame);
	if (init_flags_check(iflags, MOD_INIT_FRAME_PAGE_CACHE))
		mod_cache_destroy(frame_page);
#ifndef ESRFVER_NO_CLASS_DEV
	if (init_flags_check(iflags, MOD_INIT_CLASS))
		mod_exit(class_unregister(&scdxipci_class),
			 "unregistering device class");
#endif
	if (init_flags_check(iflags, MOD_INIT_CHR_DEV))
		chrdev_exit(unregister_chrdev(major, SCDXIPCI_DRV_NAME),
			    "unregistering character device");
	
#undef mod_cache_destroy
#undef mod_exit_cache_destroy
#undef mod_exit_check
#undef mod_exit_check_statement
#undef mod_exit

	DEB_ALWAYS("Driver unloaded\n");
}


/*--------------------------------------------------------------------------
 * Board probe / remove functions
 *--------------------------------------------------------------------------*/

int scdxipci_probe(struct pci_dev *dev, const struct pci_device_id *id)
{
	struct board_type *board_type;
	struct scdxipci_board *scd_board;
	struct scdxipci_dev *scd_dev;
	int id_type, idx, dev_per_board, len, ret, board_idx, no_dma64;
	unsigned long bus_addr;
	const char *pci_id;
	char name[SCDXIPCI_NAME_LEN];
	init_flags_t *iflags;

	FENTRY("scdxipci_probe");

	pci_id = pci_name(dev);
	id_type = id->driver_data;
	board_type = scdxipci_board_type_list;
	for (idx = 0; idx < SCDXIPCI_NR_BRD_TYPES; idx++, board_type++)
		if (board_type->type == id_type)
			break;
	if (idx == SCDXIPCI_NR_BRD_TYPES) {
		DEB_ERRORS("Invalid board type: %d\n", id_type);
		return -ENODEV;
	}

	dev_per_board = board_type->dev_per_board;
	board_idx = nr_board[board_type->type];
	len = sprintf(name, "%s #%d", board_type->name, board_idx);

	DEB_PROBE("Found %s @ %s\n", name, pci_id);

	// alloc the driver board structure
	len = sizeof(*scd_board);
	scd_board = (struct scdxipci_board *) kmalloc(len, GFP_KERNEL);
	if (scd_board == NULL) {
		DEB_ERRORS("Error allocating %d bytes for board structure\n",
			   len);
		return -ENOMEM;
	}
	memset(scd_board, 0, len);

	iflags = &scd_board->init_flags;
	init_flags_init(iflags);

	pci_set_drvdata(dev, scd_board);

	scd_board->dev = dev;
	scd_board->type = board_type;
	scd_board->idx = board_idx;
	strcpy(scd_board->name, name);

	// enable the PCI device
	ret = pci_enable_device(dev);
	if (ret < 0) {
		DEB_ERRORS("Error %d enabling PCI dev. %s\n", ret, 
			   pci_id);
		goto err;
	}
	init_flags_set(iflags, BRD_INIT_ENABLE);

	// request its region
	ret = pci_request_regions(dev, scd_board->name);
	if (ret != 0) {
		DEB_ERRORS("Could not allocate regions from PCI dev. "
			   "%s\n", pci_id);
		goto err;
	}
	init_flags_set(iflags, BRD_INIT_REGION); 

	// .. and map it
	bus_addr = pci_resource_start(dev, 0);
	len = board_type->regs_size * dev_per_board;
#if LINUX_VERSION_CODE < KERNEL_VERSION(5, 15, 0)
	scd_board->base_addr = (char *) ioremap_nocache(bus_addr, len);
#else	
	scd_board->base_addr = (char *) ioremap(bus_addr, len);
#endif	
	if (scd_board->base_addr == NULL) {
		DEB_ERRORS("Could not remap %d bytes of PCI memory "
			   "from dev. %s\n", len, pci_id);
		ret = -ENOMEM;
		goto err;
	}
	init_flags_set(iflags, BRD_INIT_MAP);

	// read serial number and find firmware
	scdxipci_init_board_id_firmware(scd_board);

	// PCI DMA init
	no_dma64 = 0;
#ifndef ESRFVER_NO_PCI_CONSISTENT_DMA
	ret = 1;
	if (id_type == SCDXIPCI_ESPIA_EXPRESS) {
#if LINUX_VERSION_CODE < KERNEL_VERSION(5, 19, 0)
		ret = pci_set_dma_mask(dev, DMA_BIT_MASK(64));
		DEB_PROBE("Can %sperform 64-bit DMA!\n", 
			  (ret == 0) ? "" : "not ");
		if (ret == 0)
			pci_set_consistent_dma_mask(dev, DMA_BIT_MASK(64));
	}
	if (ret != 0) {
		no_dma64 = 1;
                ret = pci_set_dma_mask(dev, DMA_BIT_MASK(32));
		if (ret == 0)
			pci_set_consistent_dma_mask(dev, DMA_BIT_MASK(32));
#else
		ret = dma_set_mask_and_coherent(&dev->dev, DMA_BIT_MASK(64));
		if (ret)
			ret = dma_set_mask_and_coherent(&dev->dev, DMA_BIT_MASK(32));
#endif
	}
	if (ret != 0) {
		DEB_ERRORS("Cannot set DMA masks!\n");
		ret = -ENOMEM;
		goto err;
        }
#endif
	pci_set_master(dev);
	init_flags_set(iflags, BRD_INIT_DMA);
	
	// alloc the driver log dev structures
	len = sizeof(*scd_dev) * dev_per_board;
	scd_dev = (struct scdxipci_dev *) kmalloc(len, GFP_KERNEL);
	if (scd_dev == NULL) {
		DEB_ERRORS("Error allocating %d bytes for %d dev. "
			   "structures\n", len, dev_per_board);
		ret = -ENOMEM;
		goto err;
	}
	memset(scd_dev, 0, len);
	
	scd_board->scd_dev = scd_dev;
	scd_board->nr_scd_dev = dev_per_board;
	init_flags_set(iflags, BRD_INIT_DEV_ALLOC); 

	for (idx = 0; idx < dev_per_board; idx++) {
		ret = scdxipci_dev_add(&scd_dev[idx], scd_board, idx);
		if (ret < 0) {
			while (--idx >= 0)
				scdxipci_dev_del(&scd_dev[idx]);
			goto err;
		}
		scd_dev[idx].no_dma64 = no_dma64;
	}
	init_flags_set(iflags, BRD_INIT_DEV_INIT); 

	nr_board[board_type->type]++;
	init_flags_set(iflags, BRD_INIT_INC_IDX); 

	return 0;

 err:
	scdxipci_remove(dev);
	return ret;
}


void scdxipci_remove(struct pci_dev *dev)
{
	struct scdxipci_board *scd_board;
	struct scdxipci_dev *scd_dev;
	struct board_type *board_type;
	int idx;
	const char *pci_id;
	init_flags_t *iflags;

	FENTRY("scdxipci_remove");

	scd_board = (struct scdxipci_board *) pci_get_drvdata(dev);
	board_type = scd_board->type;
	pci_id = pci_name(dev);

	DEB_PROBE("Removing %s @ %s\n", board_type->name, pci_id);

	iflags = &scd_board->init_flags;

	if (init_flags_check(iflags, BRD_INIT_INC_IDX))
		nr_board[board_type->type]--;

	scd_dev = scd_board->scd_dev;
	if (init_flags_check(iflags, BRD_INIT_DEV_INIT)) {
		for (idx = scd_board->nr_scd_dev - 1; idx >= 0; idx--) 
			scdxipci_dev_del(&scd_dev[idx]);
	}
	if (init_flags_check(iflags, BRD_INIT_DEV_ALLOC))
		kfree(scd_dev);

	if (init_flags_check(iflags, BRD_INIT_DMA))
		pci_clear_master(dev);
	if (init_flags_check(iflags, BRD_INIT_MAP))
		iounmap(scd_board->base_addr);
	if (init_flags_check(iflags, BRD_INIT_REGION))
		pci_release_regions(dev);
	if (init_flags_check(iflags, BRD_INIT_ENABLE))
		pci_disable_device(dev);

	kfree(scd_board);
	pci_set_drvdata(dev, NULL);
}


/*--------------------------------------------------------------------------
 * Device add / del functions
 *--------------------------------------------------------------------------*/

int scdxipci_dev_add(struct scdxipci_dev *scd_dev, 
		     struct scdxipci_board *scd_board, int idx)
{
	struct pci_dev *dev;
	int dev_nr, ret, regs_size;
	unsigned long bus_addr, val;
	struct timer_list *hb_timer;
	init_flags_t *iflags;
	const char *pci_id;
#ifndef ESRFVER_NO_CLASS_DEV
	ESRFVER_CLASS_DEV_TYPE *class_dev;
	int len;
#endif

	FENTRY("scdxipci_dev_add");

	dev = scd_board->dev;
	dev_nr = nr_dev;
	pci_id = pci_name(dev);
	DEB_PROBE("Adding dev. #%d @ %s-%d\n", dev_nr, pci_id, idx);

	iflags = &scd_dev->init_flags;
	init_flags_init(iflags);

	// init its members 
	scd_dev->board = scd_board;
	scd_dev->nr = dev_nr;
	scd_dev->idx = idx;
	scd_dev->type = SCDXIPCI_DEV_1_FO;
	spin_lock_init(&scd_dev->lock);
	INIT_LIST_HEAD(&scd_dev->dev_list);
	init_waitqueue_head(&scd_dev->ccd_status_wq);
	ESRFVER_TIMER_SETUP(&scd_dev->heartbeat, scdxipci_heartbeat_timer, 0);
	tasklet_init(&scd_dev->rx_ser_task, scdxipci_rx_ser_tasklet, 
		     (unsigned long) scd_dev);

	scd_dev->curr_acq = NULL;
	
	sprintf(scd_dev->name, "%s%d", SCDXIPCI_DRV_NAME, dev_nr);
	regs_size = scd_board->type->regs_size;

	bus_addr = pci_resource_start(dev, 0) + regs_size * idx;
	scd_dev->base_addr = scd_board->base_addr + regs_size * idx;

	DEB_PROBE("Map: %08lx -> %p\n", bus_addr, scd_dev->base_addr);

	// init the RX and TX circ. buffers
	ret = circ_buffer_init(&scd_dev->rx_buff, SCDXIPCI_SER_BUFF_LEN);
	if (ret != 0)
		goto err;
	init_flags_set(iflags, DEV_INIT_SER_RX);
	ret = circ_buffer_init(&scd_dev->tx_buff, SCDXIPCI_SER_BUFF_LEN);
	if (ret != 0)
		goto err;
	init_flags_set(iflags, DEV_INIT_SER_TX);

	// start the TX thread
	ret = scdxipci_ser_write_thread_start(scd_dev);
	if (ret < 0)
		goto err;
	init_flags_set(iflags, DEV_INIT_TX_THREAD);

	// alloc the SG tables and lists
	ret = scdxipci_init_sg_data(scd_dev);
	if (ret < 0)
		goto err;
	init_flags_set(iflags, DEV_INIT_SG_TABLE);

	ret = request_irq(dev->irq, scdxipci_irq_handler,
			  IRQF_SHARED, scd_dev->name, scd_dev);
	if (ret != 0) {
		DEB_ERRORS("Could not allocate IRQ %d for dev. #%d\n",
			   dev->irq, dev_nr);
		goto err;
	}
	DEB_PROBE("IRQ: %d\n", dev->irq);
	init_flags_set(iflags, DEV_INIT_IRQ);

	// now, prepare the device for real work
	val = scdxipci_read_reg(scd_dev, CONTROL_REGISTER);
	val |= INTERRUPT_ON_SERIAL;
	scdxipci_write_reg(scd_dev, CONTROL_REGISTER, val);

	// the device ready: add it to the list
	list_add_tail(&scd_dev->dev_list, &dev_list);
	nr_dev++;
	init_flags_set(iflags, DEV_INIT_LIST);

#ifndef ESRFVER_NO_CLASS_DEV
	// create sysfs class device
	len = sizeof(*class_dev);
	class_dev = (ESRFVER_CLASS_DEV_TYPE *) kmalloc(len, GFP_KERNEL);
	if (class_dev == NULL) {
		ret = -ENOMEM;
		DEB_ERRORS("error dev. #%d class device\n", dev_nr);
		goto err;
	}
	memset(class_dev, 0, len);

	ESRFVER_CLASS_DEV_INITIALIZE(class_dev);
	class_dev->class = &scdxipci_class;
	ESRFVER_CLASS_DEV_SET_NAME(class_dev, "%s%d", SCDXIPCI_DRV_NAME, dev_nr);
	ESRFVER_CLASS_DEV_SET_DEV(class_dev, &dev->dev);
	ESRFVER_CLASS_DEV_SET_DATA(class_dev, scd_dev);
#ifndef ESRFVER_CLASS_DEV_NO_DEVT
	class_dev->devt = scdxipci_cdev_nr_from_class(class_dev);
#endif       
	init_flags_set(iflags, DEV_INIT_CLASS_DEV_OBJ);

	// and add it 
	ret = ESRFVER_CLASS_DEV_ADD(class_dev);
	if (ret < 0) {
		DEB_ERRORS("error adding class device %s\n", 
			   ESRFVER_CLASS_DEV_NAME(class_dev));
		goto err;
	}
	scd_dev->class_dev = class_dev;
	init_flags_set(iflags, DEV_INIT_CLASS_DEV_ADD);
#endif

	// start the heartbeat timer
	if (dev_nr < 2) {
		hb_timer = &scd_dev->heartbeat;
		hb_timer->expires = PPDEB_HEARTBEAT_EXPIRES;
		add_timer(hb_timer);
		init_flags_set(iflags, DEV_INIT_HEARTBEAT);
	}

	return 0;
 err:
	scdxipci_dev_del(scd_dev);
	return ret;
}


void scdxipci_dev_del(struct scdxipci_dev *scd_dev)
{
	struct scdxipci_board *scd_board = scd_dev->board;
	struct pci_dev *dev = scd_board->dev;
	struct data_acq *acq;
	int dev_nr, idx;
	unsigned long val;
	init_flags_t *iflags;
	const char *pci_id;
#ifndef ESRFVER_NO_CLASS_DEV
	ESRFVER_CLASS_DEV_TYPE *class_dev;
#endif

	FENTRY("scdxipci_dev_del");

	dev_nr = scd_dev->nr;
	idx = scd_dev->idx;
	pci_id = pci_name(dev);
	DEB_PROBE("Freeing dev. #%d @ %s-%d\n", dev_nr, pci_id, idx);

	iflags = &scd_dev->init_flags;

	// stop the heartbeat timer
	if (init_flags_check(iflags, DEV_INIT_HEARTBEAT)) {
		del_timer(&scd_dev->heartbeat);
		PPDEB_HEARTBEAT_SET(scd_dev, 0);
	}

#ifndef ESRFVER_NO_CLASS_DEV
	// delete the class device from sysfs
	class_dev = scd_dev->class_dev;
	if (init_flags_check(iflags, DEV_INIT_CLASS_DEV_ADD))
		ESRFVER_CLASS_DEV_DEL(class_dev);

	// release the class device
	if (init_flags_check(iflags, DEV_INIT_CLASS_DEV_OBJ)) {
		ESRFVER_CLASS_DEV_SET_DATA(class_dev, NULL);
		ESRFVER_CLASS_DEV_SET_DEV(class_dev, NULL);
		ESRFVER_CLASS_DEV_PUT(class_dev);
	}
#endif

	// remove the device from the driver list
	if (init_flags_check(iflags, DEV_INIT_LIST)) {
		nr_dev--;
		list_del(&scd_dev->dev_list);
	}

	// stop the current acquisition: it can happen in hot-plug ...
	if (scd_dev->curr_acq != NULL) {
		acq = scd_dev->curr_acq->acq;
		DEB_ERRORS("Warning!! %s still alive! Stopping...", acq->name);
		scdxipci_acq_stop(acq);
	}

	// stop interrupts
	val = scdxipci_read_reg(scd_dev, CONTROL_REGISTER);
	val &= ~(INTERRUPT_ON_SERIAL | INTERRUPT_ON_STATUS_CHANGE);
	scdxipci_write_reg(scd_dev, CONTROL_REGISTER, val);

	// wake-up all sleeping processes
	wake_up_interruptible(&scd_dev->ccd_status_wq);

	// release all the resources
	if (init_flags_check(iflags, DEV_INIT_IRQ))
		free_irq(dev->irq, scd_dev);
	if (init_flags_check(iflags, DEV_INIT_SG_TABLE))
		scdxipci_release_sg_data(scd_dev);
	if (init_flags_check(iflags, DEV_INIT_TX_THREAD))
		scdxipci_ser_write_thread_stop(scd_dev);
	if (init_flags_check(iflags, DEV_INIT_SER_TX))
		circ_buffer_cleanup(&scd_dev->tx_buff);
	if (init_flags_check(iflags, DEV_INIT_SER_RX))
		circ_buffer_cleanup(&scd_dev->rx_buff);
}


/*--------------------------------------------------------------------------
 * Init/release SG data
 *--------------------------------------------------------------------------*/

int scdxipci_init_sg_data(struct scdxipci_dev *scd_dev)
{
	struct sg_data *sg_data;
	void *ptr;
	int len, ret, sglist_len, dev_nr = scd_dev->nr;
	unsigned long table_idx;

	FENTRY("scdxipci_init_sg_data");

	ret = 0;
	scdxipci_for_each_sg_data(sg_data, scd_dev) {
		// alloc the sg tables in contigous low (DMA, 24-bit) RAM ...
		table_idx = scdxipci_sg_data_idx(scd_dev, sg_data);
		len = SCDXIPCI_SG_TABLE_LEN;
		ptr =  kmalloc(len, GFP_KERNEL | GFP_DMA);
		if (ptr == NULL) {
			DEB_ERRORS("Could not allocate SG table %ld (%d bytes)"
				   " for dev. #%d\n", table_idx, len, dev_nr);
			ret = -ENOMEM;
			goto out;
		}
		memset(ptr, 0, len);
		sg_data->sg_table.ptr = (struct scdxipci_sg_block *) ptr;
		sg_data->sg_table.len = len / SCDXIPCI_SG_BLOCK_SIZE;

		sglist_len = SCDXIPCI_SG_MAX_BLOCKS;
		len = sglist_len * sizeof(*sg_data->sg_list);
		ptr = vmalloc(len);
		if (ptr == NULL) {
			DEB_ERRORS("Could not allocate SG list %ld (%d bytes)"
				   " for dev. #%d\n", table_idx, len, dev_nr);
			ret = -ENOMEM;
			goto out;
		}
		memset(ptr, 0, len);

		sg_data->sg_list = (struct scatterlist *) ptr;
		sg_data->sg_list_len = sglist_len;
	}

	// ... and tell the board where the first is
	scdxipci_set_curr_sg_data(scd_dev, scdxipci_first_sg_data(scd_dev));

 out:
	if (ret < 0)
		scdxipci_release_sg_data(scd_dev);

	return ret;
}

void scdxipci_release_sg_data(struct scdxipci_dev *scd_dev)
{
	struct sg_data *sg_data;

	scdxipci_clear_curr_sg_data(scd_dev);
	scdxipci_for_each_sg_data(sg_data, scd_dev) {
		if (sg_data->sg_list_len > 0)
			vfree(sg_data->sg_list);
		if (sg_data->sg_table.len > 0)
			kfree(sg_data->sg_table.ptr);
	}
}


#ifndef ESRFVER_NO_CLASS_DEV

/*--------------------------------------------------------------------------
 * Class Device functions
 *--------------------------------------------------------------------------*/

#ifdef ESRFVER_CLASS_DEV_NO_DEVT

ssize_t scdxipci_class_dev_read_dev(ESRFVER_CLASS_DEV_TYPE *class_dev, 
				    char *buffer)
{
	ssize_t len;
	dev_t cdev;

	FENTRY("scdxipci_class_dev_read_dev");

	cdev = scdxipci_cdev_nr_from_class(class_dev);
	len = print_dev_t(buffer, cdev);
	DEB_PARAMS("dev=%s", buffer);

	return len;
}

#endif // ESRFVER_CLASS_DEV_NO_DEVT

void scdxipci_class_dev_release(ESRFVER_CLASS_DEV_TYPE *class_dev)
{
	kfree(class_dev);
}

#endif // ESRFVER_NO_CLASS_DEV


/*--------------------------------------------------------------------------
 * ParPort debug Heartbeat timer
 *--------------------------------------------------------------------------*/

ESRFVER_TIMER_FUNCTION(scdxipci_heartbeat_timer, data)
{
	struct timer_list *hb_timer = ESRFVER_TIMER_FUNCTION_TIMER(data);
	struct scdxipci_dev *scd_dev = container_of(hb_timer,
						    struct scdxipci_dev,
						    heartbeat);
	int val, pattern = 0x0a;
	
	scd_dev->heartbeat_count++;
	val = (pattern >> (scd_dev->heartbeat_count % 10)) & 1;
	PPDEB_HEARTBEAT_SET(scd_dev, val);
	scd_dev->heartbeat.expires = PPDEB_HEARTBEAT_EXPIRES;
	add_timer(&scd_dev->heartbeat);
}


/*--------------------------------------------------------------------------
 * Find device
 *--------------------------------------------------------------------------*/

struct scdxipci_dev *scdxipci_find_dev(int dev_nr)
{
	struct scdxipci_dev *scd_dev;
	struct list_head *list;

	FENTRY("scdxipci_find_dev");

	list_for_each(list, &dev_list) {
		scd_dev = list_entry(list, struct scdxipci_dev, dev_list);
		if (scd_dev->nr == dev_nr)
			return scd_dev;
	}

	DEB_ERRORS("Error: could not find dev. #%d!\n", dev_nr);
	return NULL;
}


/*--------------------------------------------------------------------------
 * Register I/O
 *--------------------------------------------------------------------------*/

int scdxipci_check_reg(unsigned long reg_off)
{
	int align = sizeof(u32);
	unsigned long max_off = SCDXIPCI_REGS_SIZE - align;

	FENTRY("scdxipci_check_reg");
	if (reg_off > max_off) {
		DEB_ERRORS("Invalid register offset: %ld (max_off=%ld)\n",
			   reg_off, max_off);
		return -EINVAL;
	} else if (reg_off % align != 0) {
		DEB_ERRORS("Invalid register offset: %ld, must be %d-bytes "
			   "aligned\n", reg_off, align);
		return -EINVAL;
	} 

	return SCDXIPCI_OK;
}


u32 scdxipci_read_reg(struct scdxipci_dev *scd_dev, unsigned long reg_off)
{
	u32 val;

	FENTRY("scdxipci_read_reg");
	val = readl(scd_dev->base_addr + reg_off);
	DEB_REGS(" Reading register @ %02lx: 0x%08x\n", reg_off, val);
	return val;
}

void scdxipci_write_reg(struct scdxipci_dev *scd_dev, unsigned long reg_off, 
			u32 val)
{
	FENTRY("scdxipci_write_reg");
	DEB_REGS("Writing register @ %02lx: 0x%08x\n", reg_off, val);
	writel(val, scd_dev->base_addr + reg_off);
}


/*--------------------------------------------------------------------------
 * reset link
 *--------------------------------------------------------------------------*/

void scdxipci_reset_link(struct scdxipci_dev *scd_dev)
{
	u32 val;
	val = scdxipci_read_reg(scd_dev, CONTROL_REGISTER);
	scdxipci_write_reg(scd_dev, CONTROL_REGISTER, val |  USER_RESET_NOT); 
	scdxipci_write_reg(scd_dev, CONTROL_REGISTER, val & ~USER_RESET_NOT); 

	// let's wait some time before returning
	udelay(SCDXIPCI_RESETLINK_WAIT);
}


/*--------------------------------------------------------------------------
 * IRQ handlers
 *--------------------------------------------------------------------------*/

DEFINE_IRQ_HANDLER(scdxipci_irq_handler, irq, data)
{
	struct scdxipci_dev *scd_dev = (struct scdxipci_dev *) data;
	u32 status, control;
	DECLARE_IRQ_RET(ret, IRQ_NONE);

	FENTRY("scdxipci_irq_handler");
	PPDEB_DEV_SET(scd_dev, PPDEB_DEV_IN_IRQ);

	spin_lock(&scd_dev->lock);
	PPDEB_DEV_SET(scd_dev, PPDEB_DEV_LOCK_DEV);

	status = scdxipci_read_reg(scd_dev, STATUS_REGISTER);
	if (!(status & INTERRUPT))
		goto out;

	DEB_INTS("Interrupt dev. #%d\n", scd_dev->nr);

	do {
		if (status & INTERRUPT_SCATTER_GATHER) {
			// acknowledge the SG interrupt ...
			control = scdxipci_read_reg(scd_dev, CONTROL_REGISTER);
			control &= ~INTERRUPT_SCATTER_GATHER_ON; 
			scdxipci_write_reg(scd_dev, CONTROL_REGISTER, control);
			// ... and handle it
			scdxipci_dma_irq_handler(scd_dev);
		}
		
		if (status & SERIAL_DATA_AVAILABLE) {
			// acknowledge serial interrupt and start the tasklet
			control = scdxipci_read_reg(scd_dev, CONTROL_REGISTER);
			control |=  INTERRUPT_ON_SERIAL_CLEAR;
			scdxipci_write_reg(scd_dev, CONTROL_REGISTER, control);
			control &= ~INTERRUPT_ON_SERIAL_CLEAR;
			scdxipci_write_reg(scd_dev, CONTROL_REGISTER, control);
			// ... handle it ...
			tasklet_schedule(&scd_dev->rx_ser_task);
		}
		
		if (status & STATUS_CHANGED) {
			// acknowledge and stop the status change interrupt ...
			control = scdxipci_read_reg(scd_dev, CONTROL_REGISTER);
			control &= ~INTERRUPT_ON_STATUS_CHANGE;
			control |=  INTERRUPT_ON_STATUS_CHANGE_CLEAR;
			scdxipci_write_reg(scd_dev, CONTROL_REGISTER, control);
			control &= ~INTERRUPT_ON_STATUS_CHANGE_CLEAR;
			scdxipci_write_reg(scd_dev, CONTROL_REGISTER, control);
			// ... and handle it
			scdxipci_status_irq_handler(scd_dev);
		}

		status = scdxipci_read_reg(scd_dev, STATUS_REGISTER);
	} while (status & INTERRUPT);

	SET_IRQ_RET(ret, IRQ_HANDLED);

 out:
	spin_unlock(&scd_dev->lock);
	PPDEB_DEV_SET(scd_dev, PPDEB_DEV_OUT);

	RETURN_IRQ(ret);
}

/*--------------------------------------------------------------------------
 * DMA IRQ handler
 *
 * Called when there is an EOF interrupt, and in particular when the acq.
 * is aborted while data was being received. The device is locked by the
 * caller, and we must lock the acq. using the special try-unlock-lock-retry
 * mechanism. 
 *
 * If there is a new frame, an EOF structure will be created and added
 * to the corresponding list. The EOF tasklet is then activated, which
 * will process the data with IRQs enabled. If there is an error, the
 * tasklet is also activated without adding the EOF data, so the acq.
 * will be stopped (cleanup) in the tasklet.
 *
 * If the dev. has several SG tables, the next to use is assumed to be
 * ready. Otherwise, the table must be prepared for the next frame.
 *--------------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(5, 19, 0)
static inline
struct eof_data *acq_add_new_eof(struct data_acq *acq, 
#else
__always_inline struct eof_data *acq_add_new_eof(struct data_acq *acq, 
#endif
				 struct img_frame *frame, 
				 struct img_frame *mapped, 
				 struct img_frame *next, int end)
{
	struct eof_data *eof_data;
	int len;

	FENTRY("acq_new_eof");

	if (simu_cond(ISR_NOMEM)) {
		DEB_ERRORS("simulating no memory error!\n");
		return NULL;
	}

	len = sizeof(*eof_data);
	eof_data = (struct eof_data *) kmalloc(len, GFP_ATOMIC);
	if (eof_data == NULL) {
		DEB_ERRORS("could not atomically alloc. eof_data (%d bytes)\n",
			   len);
		return NULL;
	}
	memset(eof_data, 0, len);

	eof_data->ready_frame  = img_frame_get(frame);
	eof_data->mapped_frame = img_frame_get(mapped);
	eof_data->next_frame   = img_frame_get(next);
	eof_data->acq_end      = end;

	list_add_tail(&eof_data->list, &acq->eof_data_list);

	return eof_data;
}


#if LINUX_VERSION_CODE < KERNEL_VERSION(5, 19, 0)
static inline
int acq_dev_update_sg_data_stats(struct acq_dev *acq_dev,
#else
__always_inline int acq_dev_update_sg_data_stats(struct acq_dev *acq_dev,
#endif
				 struct sg_data *sg_data)
{
	struct data_acq *acq = acq_dev->acq;
	struct scdxipci_dev *scd_dev = acq_dev->dev;
	unsigned long curr_idx, oldest_idx, sg_avail, nr_sg;
	struct scdxipci_stats *stats = &acq->stats;
	struct scdxipci_dev_stats *dev_stats;

	FENTRY("acq_dev_update_sg_data_stats");

	nr_sg = scdxipci_nr_sg_data(scd_dev);
	curr_idx   = scdxipci_sg_data_idx(scd_dev, sg_data);
	oldest_idx = scdxipci_sg_data_idx(scd_dev, scd_dev->oldest_sg_data);
	if (oldest_idx < curr_idx)
		oldest_idx += nr_sg;
	sg_avail = oldest_idx - curr_idx;
	if (sg_avail == 0) {
		DEB_ERRORS("Aborting %s: SG table overrun!\n", acq->name);
		return SCDXIPCI_ERR_INTERNAL;
	} else if (simu_cond(ISR_SGOVER)) {
		DEB_ERRORS("Simulating %s SG table overrun!\n", acq->name);
		return SCDXIPCI_ERR_INTERNAL;
	}

	if (acq_dev->idx < SCDXIPCI_NR_DEV_STATS) {
		dev_stats = &stats->dev_stats[acq_dev->idx];
		dev_stats->sg_avail_acc += sg_avail;
		dev_stats->sg_avail_count++;
		if (dev_stats->sg_avail_min == 0)
			dev_stats->sg_avail_min = nr_sg;
		if (sg_avail < dev_stats->sg_avail_min)
			dev_stats->sg_avail_min = sg_avail;
	}

	return sg_avail;
}

#if LINUX_VERSION_CODE < KERNEL_VERSION(5, 19, 0)
static inline
int acq_dev_trigger_sg_data_map(struct acq_dev *acq_dev, int sg_avail)
#else
__always_inline int acq_dev_trigger_sg_data_map(struct acq_dev *acq_dev, int sg_avail)
#endif
{
	struct scdxipci_dev *scd_dev = acq_dev->dev;
	struct data_acq *acq = acq_dev->acq;
	struct sg_data *sg_data, *prev_sg_data;
	struct img_frame *frame;
	unsigned long next_finished;
	int ret;

	FENTRY("acq_dev_trigger_sg_data_map");

	next_finished = acq_dev->finished_frames + 1;
	if ((acq->tot_frames > 0) && (next_finished == acq->tot_frames))
		goto out;
	else if (sg_avail > 1)
		goto sched;

	sg_data = scd_dev->oldest_sg_data;
	if (sg_data->being_mapped) 
		goto sched;

	DEB_ERRORS("Warning! Last resort: mapping SG in IRQ!\n");

	prev_sg_data = scdxipci_prev_sg_data(scd_dev, sg_data);
	frame = acq_next_frame(acq, prev_sg_data->buffer_frame);
	ret = acq_dev_map_frame(acq_dev, sg_data, frame);
	if (ret < 0)
		return ret;
	scd_dev->oldest_sg_data = scdxipci_next_sg_data(scd_dev, sg_data);
 sched:
	tasklet_hi_schedule(&acq_dev->sg_task);
 out:
	return SCDXIPCI_OK;
}

void scdxipci_dma_irq_handler(struct scdxipci_dev *scd_dev)
{
	struct acq_dev *acq_dev;
	struct data_acq *acq;
	struct sg_data *sg_data;
	struct img_frame *frame, *map_frame, *new_frame;
	struct eof_data *eof_data = NULL;
	unsigned long tot_frames, end, nr_sg, dma_dwords, frame_dwords;
	ktime_t t;
	int frame_ready, start_flags, stop, sg_avail, one_frame, ret = 0;

	FENTRY("scdxipci_dma_irq_handler");

	// next step could block few usec, get time stamp now
	t = ktime_get();

	if (!scdxipci_lock_acq_from_dev(scd_dev)) {
		DEB_PARAMS("Acq. on dev #%d stopped!!\n", scd_dev->nr);
		return;
	} 
	PPDEB_DEV_SET(scd_dev, PPDEB_DEV_LOCK_ACQ);

	stop = frame_ready = 0;

	acq_dev = scd_dev->curr_acq;
	acq = acq_dev->acq;
	DEB_INTS("DMA int received, %s\n", acq->name);

	sg_data = scdxipci_get_curr_sg_data(scd_dev);
	frame = sg_data->buffer_frame;
	if (frame == NULL) {
		DEB_ERRORS("Warning: Buffer deleted from %s!!\n", acq->name);
		ret = SCDXIPCI_ERR_INTERNAL;
		goto out;
	} else if (simu_cond(ISR_NOFRAME)) {
		DEB_ERRORS("Simulating buffer deletion on %s!!\n", acq->name);
		ret = SCDXIPCI_ERR_INTERNAL;
		goto out;
	}

	// current frame info
	frame->time_stamp = t;
	dma_dwords = scdxipci_read_reg(scd_dev, ESPIA_PIXEL_COUNTER);
	frame_dwords = acq_dev_frame_dwords(acq_dev, sg_data, dma_dwords);
	frame->nr_pixels += frame_dwords * 2;

	map_frame = sg_data->mapped_frame;
	frame_ready = atomic_dec_and_test(&frame->dev_count);

	// check if acquisition finished
	acq_dev->finished_frames++;
	tot_frames = acq->tot_frames;
	end = ((tot_frames > 0) && (acq_dev->finished_frames == tot_frames));
	PPDEB_DEV_SET(scd_dev, ((end << 1) | frame_ready) + 4);

	// get the next frame, check and set the dev counts
	new_frame = acq_next_frame(acq, frame);
	if (!end && (atomic_read(&new_frame->dev_count) == 0)) {
		atomic_set(&new_frame->dev_count, acq->nr_dev);
		new_frame->nr_pixels = 0;
	}

	if (frame_ready) {
		eof_data = acq_add_new_eof(acq, frame, map_frame, new_frame, 
					   end);
		if (eof_data == NULL) {
			ret = -ENOMEM;
			goto out;
		}
		if (end)
			DEB_PARAMS("Finished %s, %ld frames acq.\n", acq->name,
				   acq_dev->finished_frames);
	}
	if (end)
		goto out;

	// check if has to change the SG table ...
	nr_sg = sg_avail = 1;
	one_frame = (new_frame == frame);
	if (one_frame)
		goto start_dma;

	nr_sg = scdxipci_nr_sg_data(scd_dev);
	if (nr_sg > 1) {
		// the new frame is already mapped in next SG table
		sg_data = scdxipci_next_sg_data(scd_dev, sg_data);
		if (sg_data->mapped_frame == NULL) {
			DEB_ERRORS("Aborting %s: invalid mapped frame!\n",
				   acq->name);
			ret = SCDXIPCI_ERR_INTERNAL;
			goto out;
		} else if (simu_cond(ISR_NOMAPPED)) {
			DEB_ERRORS("Simulating %s no mapped frame!\n",
				   acq->name);
			ret = SCDXIPCI_ERR_INTERNAL;
			goto out;
		}

		// update SG data statistics
		ret = acq_dev_update_sg_data_stats(acq_dev, sg_data);
		if (ret < 0)
			goto out;
		sg_avail = ret;
		scdxipci_set_curr_sg_data(scd_dev, sg_data);
	} else {
		// only one SG table, must map new frame
		ret = acq_dev_map_frame(acq_dev, sg_data, new_frame);
		if (ret < 0) {
			DEB_ERRORS("Aborting %s\n", acq->name);
			goto out;
		}
	}

 start_dma:
	// start next frame asap
	start_flags = acq->no_fifo_reset ? SCDXIPCI_FRAME_CONT : 
	                                   SCDXIPCI_FRAME_FIRST;
	scdxipci_activate_dma(scd_dev, start_flags);

	if (!one_frame && (nr_sg > 1))
		ret = acq_dev_trigger_sg_data_map(acq_dev, sg_avail);

 out:
	if (frame_ready || (ret < 0)) {
		if (eof_data != NULL)
			eof_data->error = ret;
		tasklet_schedule(&acq->eof_task);
	}

	scdxipci_unlock_acq_dev(scd_dev);
	PPDEB_DEV_SET(scd_dev, PPDEB_DEV_LOCK_DEV);
}

void scdxipci_status_irq_handler(struct scdxipci_dev *scd_dev)
{
	u32 val;

	FENTRY("scdxipci_status_irq_handler");
	DEB_PARAMS("Waking up processes waiting for dev. #%d\n", 
		   scd_dev->nr);

	val = scdxipci_read_reg(scd_dev, ESPIA_SERIAL_STATUS);
	scd_dev->ccd_status = (val >> 8) & 0xff;
	scd_dev->ccd_status_arrived = 1;
	wake_up_interruptible(&scd_dev->ccd_status_wq);
}

void scdxipci_rx_ser_tasklet(unsigned long data)
{
	struct scdxipci_dev *scd_dev = (struct scdxipci_dev *) data;
	unsigned long t, flags;
	u32 serdata;
	int ret, hw_fifo_full, sw_fifo_full, bad_timing;

	FENTRY("scdxipci_rx_ser_tasklet");

	scdxipci_lock_dev(scd_dev, flags);

	bad_timing = scd_dev_has_cap(scd_dev, SCDXIPCI_CAP_BAD_SER_TIMING);

	serdata = scdxipci_read_reg(scd_dev, ESPIA_SERIAL_STATUS);
	if (serdata & ESPIA_SERIAL_FIFO_EMPTY) {
		DEB_ERRORS("Warning: received a serial int. but "
			   "FIFO is empty\n");
		goto out;
	}

	t = CIRC_BUFFER_NO_BLOCK;
	hw_fifo_full = sw_fifo_full = 0;
	do {
		// check FIFO full 
		if (serdata & ESPIA_SERIAL_FIFO_FULL)
			hw_fifo_full++;

		// transfer one byte from FIFO
		scdxipci_write_reg(scd_dev, ESPIA_PIXEL_COUNTER, 0);
                // avoid clocking issues in FPGA
		if (bad_timing)
			udelay(1);
		// now read byte holder register
 		serdata = scdxipci_read_reg(scd_dev, ESPIA_SERIAL_STATUS);
		ret = circ_buffer_putc(&scd_dev->rx_buff, serdata, NULL, 0, t);
		if (ret < 0)
			sw_fifo_full++;
	} while (!(serdata & ESPIA_SERIAL_FIFO_EMPTY));

	if (hw_fifo_full > 0)
		DEB_ERRORS("Warning: board serial FIFO full (%d bytes)!!\n",
			  hw_fifo_full);
	if (sw_fifo_full > 0)
		DEB_ERRORS("Warning: serial RX buffer overflow (%d bytes)!!\n",
			  sw_fifo_full);

 out:
	scdxipci_unlock_dev(scd_dev, flags);
}


/*--------------------------------------------------------------------------
 * serial read
 *--------------------------------------------------------------------------*/

int scdxipci_get_term(char *usr_term, unsigned long term_len, char **term)
{
	FENTRY("scdxipci_get_term");

	*term = NULL;
	if (term_len == 0)
		return 0;

	*term = kmalloc(term_len, GFP_KERNEL);
	if (term == NULL) {
		DEB_ERRORS("error allocating term. copy (%ld bytes)\n", 
			   term_len);
		return -ENOMEM;
	}

	if (copy_from_user(*term, usr_term, term_len)) {
		DEB_ERRORS("error copying %ld bytes from user ptr 0x%p\n",
			   term_len, usr_term);
		scdxipci_put_term(*term);
		return -EFAULT;
	}

	return 0;
}

void scdxipci_put_term(char *term)
{
	if (term != NULL)
		kfree(term);
}

int scdxipci_ser_read(struct scdxipci_dev *scd_dev, char *usr_buff, 
		      unsigned long buff_len, char *usr_term,
		      unsigned long term_len, unsigned long timeout)
{
	struct circ_buffer_xfer_data xfer;
	char *term;
	int ret;

	FENTRY("scdxipci_ser_read");

	ret = scdxipci_get_term(usr_term, term_len, &term);
	if (ret < 0)
		return ret;

	circ_buffer_xfer_data_init(&xfer, usr_buff, buff_len, term, term_len, 
				   CIRC_BUFFER_READ | CIRC_BUFFER_USER);
	DEB_PARAMS("usr_buff=0x%p, len=%ld, term=0x%p, term_len=%ld, "
		   "xfer_flags=0x%04x, timeout=%ld\n", usr_buff, buff_len, 
		   term, term_len, xfer.flags, timeout);
	ret = circ_buffer_xfer(&scd_dev->rx_buff, &xfer, 
			       &scd_dev->lock, timeout);
	DEB_PARAMS("ret=%d\n", ret);
	if (ret == -ENOSYS) 
		ret = SCDXIPCI_ERR_NOIMPL;

	scdxipci_put_term(term);

	return ret;
}


/*--------------------------------------------------------------------------
 * serial write:	transfer bytes from user space to the serial line.
 *			the write thread will be waken up with the first byte.
 *--------------------------------------------------------------------------*/

int scdxipci_ser_write(struct scdxipci_dev *scd_dev, char *usr_buff, 
		       unsigned long len, unsigned long block_size, 
		       unsigned long delay, int block)
{
	struct circ_buffer_xfer_data xfer;
	int ret, real_delay, timeout;

	FENTRY("scdxipci_ser_write");

	if (len > 0) {
		real_delay = (delay > 0) ? delay : SCDXIPCI_TX_DELAY;
		timeout = 1000000; // 1 sec: just in case the load is high
		scd_dev->tx_delay = real_delay;
		scd_dev->tx_block_size = (block_size > 0) ? block_size : 1;
	} else
		real_delay = timeout = delay;

	circ_buffer_xfer_data_init(&xfer, usr_buff, len, NULL, 0, 
				   CIRC_BUFFER_WRITE | CIRC_BUFFER_USER);
	if (!block)
		timeout = CIRC_BUFFER_NO_BLOCK;

	ret = circ_buffer_xfer(&scd_dev->tx_buff, &xfer, 
			       &scd_dev->lock, timeout);
	DEB_PARAMS("usr_buff=0x%p, len=%ld, delay=%ld(real=%d), block=%d, "
		   "ret=%d\n", usr_buff, len, delay, real_delay, block, ret);
	return ret;
}


/*--------------------------------------------------------------------------
 * serial write thread start
 *--------------------------------------------------------------------------*/

int scdxipci_ser_write_thread_start(struct scdxipci_dev *scd_dev)
{
	int ret;
	kthread_t tx_thread;
	struct task_struct *tx_task;

	FENTRY("scdxipci_ser_write_start");

	tx_thread = kthread_run(scdxipci_ser_write_thread, scd_dev, 
				"%s_ser_tx", scd_dev->name);
	if (IS_ERR(tx_thread)) {
		ret = PTR_ERR(tx_thread);
		DEB_ERRORS("could not start write thread: ret=%d\n", ret);
		return ret;
	}
	scd_dev->tx_thread = tx_thread;
	tx_task = kthread_task(tx_thread);
	DEB_PROBE(" Tx thread: %d\n", (int) tx_task->pid);
	return 0;
}


/*--------------------------------------------------------------------------
 * serial write thread stop
 *--------------------------------------------------------------------------*/

int scdxipci_ser_write_thread_stop(struct scdxipci_dev *scd_dev)
{
	struct circ_buffer_xfer_data xfer;
	int ret, wait_ms;

	FENTRY("scdxipci_ser_write_thread_stop");

	// first wait a while to let the thread finish tx
	wait_ms = 10000;
	circ_buffer_xfer_data_init(&xfer, NULL, 0, NULL, 0, CIRC_BUFFER_WRITE);
	circ_buffer_xfer(&scd_dev->tx_buff, &xfer,
			 &scd_dev->lock, wait_ms * 1000UL);
	ret = circ_buffer_flush(&scd_dev->tx_buff, &scd_dev->lock);
	if (ret > 0)
		DEB_ERRORS("Warning: flushed %d bytes on Tx\n", ret);

	// stop the thread 
	DEB_PARAMS("Stopping %s Tx thread\n", scd_dev->name);
	ret = kthread_stop(scd_dev->tx_thread);
	if (ret < 0) {
		DEB_ERRORS("Error: Could not kill Tx thread (ret=%d)\n", ret);
		return -1;
	}

	DEB_PARAMS("%s Tx thread successfully killed\n", scd_dev->name);
	return 0;
}


/*--------------------------------------------------------------------------
 * serial write thread
 *--------------------------------------------------------------------------*/

int scdxipci_ser_write_thread(void *data)
{
	struct scdxipci_dev *scd_dev = (struct scdxipci_dev *) data;
	struct circ_buffer *buff = &scd_dev->tx_buff;
	unsigned long flags, msec, delay;
	int i, j, xfer_flags = CIRC_BUFFER_KTHREAD;

	FENTRY("scdxipci_ser_write_thread");

	while (!kthread_should_stop()) {
		DEB_PARAMS("%s: entering in blocking call\n", scd_dev->name);
		i = circ_buffer_getc(buff, &scd_dev->lock, xfer_flags,
				     CIRC_BUFFER_BLOCK_FOREVER);
		for (j = 1; i != -1; j++) {
			scdxipci_lock_dev(scd_dev, flags);
			scdxipci_write_reg(scd_dev, ESPIA_SERIAL_STATUS, 
					   (i & 0xff) | ESPIA_SERIAL_WRITE);
			scdxipci_unlock_dev(scd_dev, flags);

			// let the tx chain breath
			if ((j % scd_dev->tx_block_size) == 0) {
				delay = scd_dev->tx_delay;
				msec = delay / 1000;
				if (msec >= 1000 / HZ)
					msleep(msec);
				else
					udelay(delay);  
			}

			i = circ_buffer_getc(buff, &scd_dev->lock, xfer_flags,
					     CIRC_BUFFER_NO_BLOCK);
		}
		if (signal_pending(current)) {
			DEB_ALWAYS("Hey!!!!!!!!!!!!\n");
			// clean all signals, so next calls may block
			flush_signals(current);
		}
	}
	DEB_ALWAYS("%s: exiting\n", scd_dev->name);

	return 0;
}


/*--------------------------------------------------------------------------
 * DMA activate / abort
 *--------------------------------------------------------------------------*/

void scdxipci_activate_dma(struct scdxipci_dev *scd_dev, int flags)
{
	u32 val;

	FENTRY("scdxipci_activate_dma");
	DEB_PARAMS("starting dma on dev #%d: flags=0x%x\n", scd_dev->nr, flags);

	val = scdxipci_read_reg(scd_dev, CONTROL_REGISTER);
	val &= ~RESET_SCATTER_GATHER;
	val |=  SCATTER_GATHER_START | INTERRUPT_SCATTER_GATHER_ON;
	if (flags & SCDXIPCI_FRAME_CONT)
		val |= DO_NOT_RESET_FIFOS;
	else
		val &= ~DO_NOT_RESET_FIFOS;
	scdxipci_write_reg(scd_dev, CONTROL_REGISTER, val);
}


void scdxipci_abort_dma(struct scdxipci_dev *scd_dev)
{
	u32 control;

	FENTRY("scdxipci_abort_dma");
	DEB_PARAMS("aborting dma on dev #%d\n", scd_dev->nr);

	control = scdxipci_read_reg(scd_dev, CONTROL_REGISTER);
	control &= ~INTERRUPT_SCATTER_GATHER_ON; 
	control |=  RESET_SCATTER_GATHER;
	scdxipci_write_reg(scd_dev, CONTROL_REGISTER, control);
}


/*--------------------------------------------------------------------------
 * pci frame map/unmap
 *--------------------------------------------------------------------------*/

int scdxipci_pci_map_frame(struct scdxipci_dev *scd_dev, 
			   struct sg_data *sg_data, struct img_frame *frame)
{
	int frame_pages, len, offset, acq_sg, dev_dma_64bit;
	unsigned int addr, i, nr_blocks, sglist_len, dma_addr_up32, ok, ret;
	struct acq_dev *acq_dev = scd_dev->curr_acq;
	struct pci_dev *dev = scd_dev->board->dev;
	struct img_frame_page *fpage;
	struct scatterlist *sglist, *sg;
	struct scdxipci_sg_block *block, *acq_block;
	struct scdxipci_sg_table *acq_sg_table;
	struct data_acq *acq;
	struct page *page, *garbage_page = NULL;
	dma_addr_t dma_addr;

	FENTRY("scdxipci_pci_map_frame");

	if (acq_dev == NULL) {
		DEB_PARAMS("Dev. #%d acq. was aborted\n", scd_dev->nr);
		return SCDXIPCI_ERR_ACQ;
	}

	if (frame == sg_data->mapped_frame)
		return 0;
	else if (frame == NULL) {
		DEB_ERRORS("Invalid NULL frame\n");
		return -EINVAL;
	}

	// ... and also check consistency
	scdxipci_pci_unmap_frame(scd_dev, sg_data);  
	
	frame_pages = img_frame_pages(frame);
	acq_sg_table = &acq_dev->sg_table;
	acq_sg = (acq_sg_table->len > 0);
	sglist_len = acq_sg ? (int) acq_sg_table->len : frame_pages;
	if (sglist_len > sg_data->sg_list_len) {
		DEB_ERRORS("Error: SG list size too big: %d (max=%ld)\n",
			   sglist_len, sg_data->sg_list_len);
		return SCDXIPCI_ERR_BIGSG;
	} 
	sglist = sg_data->sg_list;
	len = sizeof(*sglist) * sglist_len;
	memset(sglist, 0, len);

	// set the scatter list
	sg = sglist;
	if (acq_sg) {
		acq = acq_dev->acq;
		if (acq->garbage_len > 0)
			garbage_page = acq->garbage_page->page;
		acq_block = acq_sg_table->ptr;
		sg_data->dma_dwords = sg_data->frame_dwords = 0;
		for (i = 0; i < sglist_len; i++, acq_block++, sg++) {
			addr = acq_block->address;
			if (is_invalid32(addr)) {
				if (!garbage_page) {
					DEB_ERRORS("Error: no garbage page!\n");
					return SCDXIPCI_ERR_INTERNAL;
				}
				page = garbage_page;
				offset = 0;
			} else {
				fpage = img_frame_get_page(frame, 
							   addr / PAGE_SIZE);
				page = fpage->page;
				offset = addr % PAGE_SIZE;
				sg_data->frame_dwords += acq_block->size;
			}
			sg_data->dma_dwords += acq_block->size;
			len = acq_block->size * 4;
			sg_set_page(sg, page, len, offset);
		}
	} else {
		fpage = frame->first_page;
		for (i = 0; i < sglist_len; i++, fpage = fpage->next, sg++)
			sg_set_page(sg, fpage->page, PAGE_SIZE, 0);
	}
	sg_data->sg_list_mapped = sglist_len;

#if LINUX_VERSION_CODE <= KERNEL_VERSION(5, 18, 0)
	nr_blocks = pci_map_sg(dev, sglist, sglist_len, PCI_DMA_FROMDEVICE);
#else
	nr_blocks = dma_map_sg(&dev->dev, sglist, sglist_len, DMA_FROM_DEVICE);
#endif
	sg_data->mapped_frame = frame;
	if (nr_blocks + 1 > sg_data->sg_table.len) {
		DEB_ERRORS("Too many SG descriptors needed: %d (max=%ld)\n",
			   nr_blocks, sg_data->sg_table.len - 1);
		ret = SCDXIPCI_ERR_BIGSG;
		goto out;
	}

	dev_dma_64bit = acq_dev_has_dma64(acq_dev);

	sg = sglist;
	block = sg_data->sg_table.ptr;
	for (i = 0; i < nr_blocks; i++, sg++, block++) {
		dma_addr = sg_dma_address(sg);
		dma_addr_up32 = (dma_addr & 0xffffffff00000000LL) >> 32;
		ok  = ((dma_addr_up32 == 0) || dev_dma_64bit);
		ok &= ((dma_addr_up32 & 0xff) == dma_addr_up32);
		if (!ok) {
			DEB_ERRORS("Block %d (%ld bytes) has addr. too high: "
				   "%lld\n", i, (long) sg_dma_len(sg), 
				   (long long) dma_addr);
			ret = SCDXIPCI_ERR_MEMTOOHIGH;
			goto out;
		}

		block->address = dma_addr & 0xffffffff;
		block->size    = sg_dma_len(sg) / 4; // in DWORD
		block->size   |= (dma_addr_up32 & 0xff) << 24;
	}
	block->size = 0;  // to signal the end of the SG list 
	wmb();

	ret = 0;
 out:
	if (ret < 0)
		scdxipci_pci_unmap_frame(scd_dev, sg_data);
		
	return 0;
}


void scdxipci_pci_unmap_frame(struct scdxipci_dev *scd_dev,
			      struct sg_data *sg_data)
{
	struct pci_dev *dev = scd_dev->board->dev;
	struct img_frame *frame = sg_data->mapped_frame;
	unsigned long table_idx;

	FENTRY("scdxipci_pci_unmap_frame");

	// clear the first SG descriptor: avoid future xfers
	sg_data->sg_table.ptr[0].size = 0;
	wmb();
	table_idx = scdxipci_sg_data_idx(scd_dev, sg_data);
	if (frame != NULL) {
		DEB_PARAMS("Dev #%d SG table %ld, mapped=%ld\n", 
			   scd_dev->nr, table_idx, sg_data->sg_list_mapped);
#if LINUX_VERSION_CODE <= KERNEL_VERSION(5, 18, 0)
		pci_unmap_sg(dev, sg_data->sg_list, sg_data->sg_list_mapped, 
			     PCI_DMA_FROMDEVICE);
#else
		dma_unmap_sg(&dev->dev, sg_data->sg_list, sg_data->sg_list_mapped, 
			     DMA_FROM_DEVICE);
#endif
	} else 
		DEB_PARAMS("Dev #%d SG table %ld mapped_frame "
			   "is NULL\n", scd_dev->nr, table_idx);
	sg_data->mapped_frame = NULL;
	sg_data->sg_list_mapped = 0;
}


/*--------------------------------------------------------------------------
 * CCD status
 *--------------------------------------------------------------------------*/

int scdxipci_ccd_status(struct scdxipci_dev *scd_dev, unsigned long timeout)
{
	int status, ask_block, do_block;
	unsigned long flags, ticks;
	u32 val;
	char *msg;
	long ret;

#define status_arrived (scd_dev->ccd_status_arrived != 0)

	FENTRY("scdxipci_ccd_status");
	ask_block = (timeout != SCDXIPCI_NO_BLOCK);
	do_block = 0;

	scdxipci_lock_dev(scd_dev, flags);
	if (ask_block && !status_arrived) {
		val = scdxipci_read_reg(scd_dev, CONTROL_REGISTER);
		val |= INTERRUPT_ON_STATUS_CHANGE;
		scdxipci_write_reg(scd_dev, CONTROL_REGISTER, val);
		do_block = 1;
	}
	scdxipci_unlock_dev(scd_dev, flags);

	if (do_block) {
		ticks = scdxipci_timeout(timeout);
		DEB_PARAMS("blocking: timeout=%ld, ticks=%ld\n", 
			   timeout, ticks);
		ret = wait_event_interruptible_timeout(scd_dev->ccd_status_wq,
						       status_arrived,
						       ticks);
		if (ret == 0)
			ret = -ETIMEDOUT;
		if (ret < 0) {
			msg = (ret == -ETIMEDOUT) ? "Timeout" : "Interrupted";
			DEB_PARAMS("%s\n", msg);
			return ret;
		}
	}

	scdxipci_lock_dev(scd_dev, flags);
	if (ask_block) {
		status = scd_dev->ccd_status;
		scd_dev->ccd_status_arrived = 0;
	} else {
		val = scdxipci_read_reg(scd_dev, ESPIA_SERIAL_STATUS);
		status = (val >> 8) & 0xff;
	}
	scdxipci_unlock_dev(scd_dev, flags);

#undef status_arrived

	DEB_PARAMS("ask_block=%d, status=0x%02x\n", ask_block, status);
	return status;
}


/*--------------------------------------------------------------------------
 * DMA align
 *--------------------------------------------------------------------------*/

int scdxipci_dma_align(struct scdxipci_dev *scd_dev)
{
	int align;
	u32 val;

	FENTRY("scdxipci_dma_align");

	val = scdxipci_read_reg(scd_dev, CONTROL_REGISTER);
	align = (val & USE_64_BITS) ? 8 : 4;
	DEB_PARAMS("align=%d\n", align);

	return align;
}


/*--------------------------------------------------------------------------
 * Handle hardware info
 *--------------------------------------------------------------------------*/

int scdxipci_hw_info(struct scdxipci_dev *scd_dev, int action,
		     struct scdxipci_hw_info *hw_info, int usr_spc)
{
	struct scdxipci_board *scd_board = scd_dev->board;
	struct board_type *board_type = scd_board->type;
	struct firmware_data *fw_data = scd_board->firmware;
	struct scdxipci_board_id_info *board_id_info = &hw_info->board_id;
	struct scdxipci_firmware_info *fw_info = &hw_info->firmware;
	int start, len, end, offset, ret = 0;
	u16 *ptr, val;

	FENTRY("scdxipci_hw_info");
	DEB_PARAMS("action=%d\n", action);

	if (action != SCDXIPCI_HW_INFO_GET) {
		DEB_ERRORS("Invalid action=%d\n", action);
		return -EINVAL;
	}

	hw_info->board_type	= board_type->type;
	hw_info->board_idx	= scd_board->idx;
	hw_info->dev_type	= scd_dev->type;
	hw_info->dev_idx	= scd_dev->idx;

	start = board_type->board_id_start;
	len   = board_type->board_id_len;
	ptr   = (u16 *) board_id_info->ptr;
	if ((len > 0) && (ptr != NULL) && ((int) board_id_info->len >= len)) {
		if (usr_spc && !ESRFVER_ACCESS_OK(ptr, len)) {
			DEB_ERRORS("Access error writing board ID "
				   "to user space!\n");
			return -EFAULT;
		}
		end = start + len;
		for (offset = start; offset < end; offset += 2, ptr++) {
			val = scdxipci_read_flash_word(scd_board, offset);
			if (!usr_spc) {
				*ptr = val;
				continue;
			} else if ((ret = __put_user(val, ptr)) != 0) {
				DEB_ERRORS("Error writing board ID to "
					   "user space!\n");
				return ret;
			}
		}
	}
	
	board_id_info->start	= start;
	board_id_info->len	= len;
	board_id_info->crc_ok	= scd_board->crc_ok;
	board_id_info->ser_nr	= scd_board->ser_nr;

	fw_info->start		= board_type->firmware_start;
	fw_info->len		= board_type->firmware_len;
	fw_info->checksum	= scd_board->firmware_checksum;
	strcpy(fw_info->name, fw_data ? fw_data->name : "Unknown");

	return ret;
}


/*--------------------------------------------------------------------------
 * Read flash
 *--------------------------------------------------------------------------*/

#define ADD_REG  FLASH_EPROM_FLASH_ADD_REGISTER
#define DATA_REG FLASH_EPROM_FLASH_DATA_REGISTER

void scdxipci_init_flash(struct scdxipci_board *scd_board)
{
	int offset;
	u16 val;

	FENTRY("scdxipci_init_flash");

	// Change configuration register
	DEB_FLASH("Changing flash configuration register\n");
	offset = 0xbdff;
	writel(offset, scd_board->base_addr + ADD_REG);
	udelay(SCDXIPCI_FLASH_READ_DELAY);
	val = 0x60;
	writel(val, scd_board->base_addr + DATA_REG);
	udelay(SCDXIPCI_FLASH_READ_DELAY);
	val = 0x03;
	writel(val, scd_board->base_addr + DATA_REG);
	udelay(SCDXIPCI_FLASH_READ_DELAY);

	// Reset to read array
	DEB_FLASH("Reseting flash to read array\n");
	offset = 0x0;
	writel(offset, scd_board->base_addr + ADD_REG);
	udelay(SCDXIPCI_FLASH_READ_DELAY);
	val = 0xff;
	writel(val, scd_board->base_addr + DATA_REG);
	udelay(SCDXIPCI_FLASH_READ_DELAY);
}

u16 scdxipci_read_flash_word(struct scdxipci_board *scd_board, int offset)
{
	u16 val;

	FENTRY("scdxipci_read_flash_word");

	writel(offset / sizeof(u16), scd_board->base_addr + ADD_REG);
	// Read the flash just to force the cache engine to flush the write
	val = (u16) (readl(scd_board->base_addr + DATA_REG) & 0xffff);
	udelay(SCDXIPCI_FLASH_READ_DELAY);
	val = (u16) (readl(scd_board->base_addr + DATA_REG) & 0xffff);
	DEB_FLASH("Reading flash word @ 0x%x: 0x%x\n", offset, val);
	return val;
}

u32 scdxipci_read_flash_dword(struct scdxipci_board *scd_board, int offset)
{
	u16 val_l = scdxipci_read_flash_word(scd_board, offset);
	u16 val_h = scdxipci_read_flash_word(scd_board, offset + sizeof(u16));
	return ((u32) val_h) << 16 | val_l;
}


/*--------------------------------------------------------------------------
 * CRC32 calculation
 *--------------------------------------------------------------------------*/

u32 scdxipci_crc32_table[256];

u32 scdxipci_crc32_init(void)
{
	u32 a, b, c, d;
	
	if (scdxipci_crc32_table[1] == 0) {
		for (a = 0; a < 256; a++) {
			c = a << 24;
			for (b = 0; b < 8; b++) {
				d = c << 1;
				if ((c & 0x80000000) != 0)
					d ^= SCDXIPCI_CRC32_POLY;
				c = d;
			}
			scdxipci_crc32_table[a] = c;
		}
	}

	return SCDXIPCI_CRC32_INIT;
}

u32 scdxipci_crc32_one(u8 data, u32 crc_acc)
{
	u8 a = data ^ (crc_acc >> 24);
	return (crc_acc << 8) ^ scdxipci_crc32_table[a];
}

u32 scdxipci_crc32_end(u32 crc_acc)
{
	return ~crc_acc;
}


/*--------------------------------------------------------------------------
 * Card ID and firmware
 *--------------------------------------------------------------------------*/

int scdxipci_init_board_id_firmware(struct scdxipci_board *scd_board)
{
	struct board_type *board_type = scd_board->type;
	struct firmware_data *fw_data;
	unsigned long offset, id_start, id_len, fw_start, fw_len, tot_len;
	unsigned long fw_end, crc_end = 0;
	u16 checksum, flash_val;
	u32 crc_acc, calc_crc, bid_crc, dw_val;
	char *name;

	FENTRY("scdxipci_init_board_id_firmware");

	id_len = board_type->board_id_len;
	fw_len = board_type->firmware_len;
	if (!id_len && !fw_len)
		return 0;

	// init flash
	scdxipci_init_flash(scd_board);

	id_start = board_type->board_id_start;
	fw_start = board_type->firmware_start;

	fw_end = fw_start + fw_len;
	tot_len = fw_end;
	if (id_len) {
		tot_len = id_start + id_len;
		crc_end = tot_len - sizeof(bid_crc);
	}

	DEB_PROBE("Reading flash firmware/ID (%ld bytes) ...\n", tot_len);

	crc_acc = scdxipci_crc32_init();
	checksum = 0;
	for (offset = 0; offset < tot_len; offset += sizeof(u16)) {
		flash_val = scdxipci_read_flash_word(scd_board, offset);
		if ((offset % 0x1000) == 0)
			schedule();

		if (fw_len && (offset >= fw_start) && (offset < fw_end))
			checksum += flash_val;

		if (!id_len || (offset >= crc_end))
			continue;

		crc_acc = scdxipci_crc32_one(flash_val & 0xff, crc_acc);
		crc_acc = scdxipci_crc32_one(flash_val >> 8,   crc_acc);
	}

	if (id_len) {
		calc_crc = scdxipci_crc32_end(crc_acc);
		offset  = id_start;
		offset += offsetof(struct scdxipci_card_id, CRC);
		bid_crc = scdxipci_read_flash_dword(scd_board, offset);
		scd_board->crc_ok = (calc_crc == bid_crc);
		if (!scd_board->crc_ok) {
			DEB_ERRORS("Error: FPGA flash CRC mismatch: "
				   "calc=0x%08x, bid=0x%08x\n", 
				   calc_crc, bid_crc);
			goto check_fw;
		}

		offset  = id_start;
		offset += offsetof(struct scdxipci_card_id, NumeroSerie);
		dw_val = scdxipci_read_flash_dword(scd_board, offset);
		scd_board->ser_nr = dw_val;
		DEB_PROBE("Serial nr: %0*ld, CRC: 0x%08x [OK]\n", 
			  scdxipci_ser_nr_len(dw_val), (long) dw_val, bid_crc);
	}

 check_fw:
	if (!fw_len)
		return 0;

	scd_board->firmware_checksum = checksum;
	scd_board->firmware = NULL;
	name = "Unknown";
	fw_data = board_type->firmware_array;
	while (strlen(fw_data->name) > 0) {
		if (fw_data->checksum == checksum) {
			scd_board->firmware = fw_data;
			name = fw_data->name;
			break;
		}
		fw_data++;
	}

	DEB_PROBE("Firmware %s [checksum=0x%04x]\n", name, checksum);

	return 0;
}


