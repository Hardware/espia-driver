/****************************************************************************
 * File:	scdxipci_lib.c
 * $Header: /segfs/bliss/cvs/driver/ESRF/espia/src/scdxipci_lib.c,v 2.15 2012/06/15 10:33:35 ahoms Exp $
 * Project:	SECAD Espia card Linux driver
 * Description:	Driver library source code
 * Author(s):	SECAD S.A.
 *		A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 ****************************************************************************/

#include "esrfdebug.h"
#include "scdxipci_lib.h"

#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <pthread.h>

#define MAX_NR_DEV		256

struct scdxipci_dev {
	int fd;
};

static struct scdxipci_dev devices[MAX_NR_DEV];


/*--------------------------------------------------------------------------
 * rcs id
 *--------------------------------------------------------------------------*/

static char rcs_id[] __attribute__((unused)) = 
	"$Header: /segfs/bliss/cvs/driver/ESRF/espia/src/scdxipci_lib.c,v 2.15 2012/06/15 10:33:35 ahoms Exp $"; 


/*--------------------------------------------------------------------------
 * helpers
 *--------------------------------------------------------------------------*/

static inline struct scdxipci_dev *get_pdev(scdxipci_t dev)
{
	struct scdxipci_dev *pdev = &devices[dev];
	if ((dev < MAX_NR_DEV) && (pdev->fd > -1))
		return pdev;
	return NULL;
}

static int get_free_dev()
{
	int i;
	static int first_time = 1;

	if (first_time) {
		first_time = 0;
		for (i = 0; i < MAX_NR_DEV; i++)
			devices[i].fd = -1;
	}

	for (i = 0; i < MAX_NR_DEV; i++)
		if (devices[i].fd == -1)
			break;
	if (i == MAX_NR_DEV)
		return SCDXIPCI_ERR_NOFREEDEV;

	return i;
}

#define scdxipci_check_ret(ret) \
	(((ret) == SCDXIPCI_ERR_STDLIB) ? -errno : (ret))

#define scdxipci_pdev_ioctl(pdevice, cmd, arg) \
({ \
	struct scdxipci_dev *pdev = (pdevice); \
	int ret = SCDXIPCI_ERR_NODEV; \
	if (pdev != NULL) { \
		ret = ioctl(pdev->fd, cmd, arg); \
		ret = scdxipci_check_ret(ret); \
	} \
	ret; \
})

#define scdxipci_ioctl(dev, cmd, arg) \
	scdxipci_pdev_ioctl(get_pdev(dev), cmd, arg)


/*--------------------------------------------------------------------------
 * error messages
 *--------------------------------------------------------------------------*/

static deb_const_str error_str_array[] = {
	{SCDXIPCI_OK,		"No error"},
	{SCDXIPCI_ERR_INTERNAL,	"Internal driver error"},
	{SCDXIPCI_ERR_ACQ,	"Acquisition empty or not running"},
	{SCDXIPCI_ERR_BUFFER,	"Invalid buffer. Empty acquisition?"},
	{SCDXIPCI_ERR_FRAMENR,	"Invalid frame number"},
	{SCDXIPCI_ERR_OVERRUN,	"Acquisition buffer overrun"},
	{SCDXIPCI_ERR_NOTREADY,	"Requested frame not ready yet"},
	{SCDXIPCI_ERR_WONTARR,  "Requested frame will never arrive"},
	{SCDXIPCI_ERR_TIMEOUT,	"Operation timed out"},
	{SCDXIPCI_ERR_VERSION,	"Incompatible driver/library versions"},
	{SCDXIPCI_ERR_AUXBUFF,	"Acquisition without 32-bit only aux. buffer"},
	{SCDXIPCI_ERR_NOIMPL,   "Functionality not implemented yet"},
	{SCDXIPCI_ERR_RUNNING,	"Acquisition running: must be stopped first"},
	{SCDXIPCI_ERR_ACQNODEV,	"Acquisition has no device defined yet"},
	{SCDXIPCI_ERR_META,	"Command not allowed on meta dev. acq"},
	{SCDXIPCI_ERR_NOMETA,	"Command only allowed on meta dev. acq"},
	{SCDXIPCI_ERR_LOSTBUFF,	"Acquisition loosing buffers: cannot query"},
	{SCDXIPCI_ERR_BOUNDARY,	"DMA transfer would cross a page boundary"},
	{SCDXIPCI_ERR_DMAALIGN,	"DMA transfer is not properly aligned"},
	{SCDXIPCI_ERR_DEVBUSY,	"Device is running another acquisition"},
	{SCDXIPCI_ERR_BIGSG,	"Requested frame/SG exceeds max. SG table size"},
	{SCDXIPCI_ERR_MEMTOOHIGH,"Image buffer mem. addr. too high for dev."},
	{SCDXIPCI_ERR_TOOMUCHRAM,"Too much RAM for OS: use boot option: "
	 			 "mem=0xffffffff"},
	// lib codes
	{SCDXIPCI_ERR_NOFREEDEV,"Too many devices open"},
	{SCDXIPCI_ERR_NODEV,	"Invalid device handle"},
	{SCDXIPCI_ERR_BUFFSIZE,	"Buffer has multiple non page-aligned frames"},
};

INIT_STATIC_CONST_STR_ARRAY_PREFIX(error_strings, error_str_array, "SCDXIPCI");

char *scdxipci_strerror(int error)
{
	if (error == SCDXIPCI_ERR_STDLIB)
		error = -errno;
	else if (error == -ENOSYS)
		error = SCDXIPCI_ERR_VERSION;
	return GET_CONST_STR(error_strings, error, strerror(-error));
}

/*--------------------------------------------------------------------------
 * open / close
 *--------------------------------------------------------------------------*/

int scdxipci_open(const char *devname, scdxipci_t *dev_ptr)
{
	int fd, i, ret;
	char *drv_revision = "$Name: v3_12 $";
	static struct scdxipci_ioc_version ver;

	i = get_free_dev();
	if (i < 0)
		return i;

	fd = open(devname, O_RDWR);
	if (fd < 0)
		return SCDXIPCI_ERR_STDLIB;

	devices[i].fd = fd;
	*dev_ptr = i;

	pretty_rcs_revision(ver.version, drv_revision);
	ret = scdxipci_ioctl(*dev_ptr, SCDXIPCI_IOC_VERSION, &ver);
	if (ret < 0) {
		scdxipci_close(*dev_ptr);
		*dev_ptr = SCDXIPCI_DEV_INVAL;
	}

	return ret;
}

int scdxipci_close(scdxipci_t dev)
{
	struct scdxipci_dev *pdev = get_pdev(dev);
	if (!pdev)
		return SCDXIPCI_ERR_NODEV;
	if (close(pdev->fd) < 0)
		return SCDXIPCI_ERR_STDLIB;

	pdev->fd = -1;
	return SCDXIPCI_OK;
}


/*--------------------------------------------------------------------------
 * driver option
 *--------------------------------------------------------------------------*/

int scdxipci_option(scdxipci_t dev, int option, int action, int *value_ptr)
{
	int ret;
	struct scdxipci_ioc_option opt;
	opt.option = option;
	opt.action = action;
	opt.value = *value_ptr;
	ret = scdxipci_ioctl(dev, SCDXIPCI_IOC_OPTION, &opt);
	if (ret < 0)
		return ret;

	*value_ptr = opt.value;
	return SCDXIPCI_OK;
}


/*--------------------------------------------------------------------------
 * register read/write
 *--------------------------------------------------------------------------*/

int scdxipci_read_register(scdxipci_t dev, unsigned long reg_off, 
			   unsigned int *data_ptr)
{
	int ret;
	struct scdxipci_ioc_register reg;
	reg.reg_off = reg_off;
	reg.data = *data_ptr;
	ret = scdxipci_ioctl(dev, SCDXIPCI_IOC_READ_REGISTER, &reg);
	*data_ptr = reg.data;
	return ret;
}


int scdxipci_write_register(scdxipci_t dev, unsigned long reg_off, 
			    unsigned int *data_ptr, unsigned int mask)
{
	int ret;
	struct scdxipci_ioc_register reg;
	reg.reg_off = reg_off;
	reg.data = *data_ptr;
	reg.mask = mask;
	ret = scdxipci_ioctl(dev, SCDXIPCI_IOC_WRITE_REGISTER, &reg);
	*data_ptr = reg.data;
	return ret;
}


/*--------------------------------------------------------------------------
 * reset link
 *--------------------------------------------------------------------------*/

int scdxipci_reset_link(scdxipci_t dev)
{
	return scdxipci_ioctl(dev, SCDXIPCI_IOC_RESET_LINK, 0);
}


/*--------------------------------------------------------------------------
 * serial read/write
 *--------------------------------------------------------------------------*/

int scdxipci_ser_read(scdxipci_t dev, char *buffer, 
		      unsigned long *nr_bytes_ptr, char *term, 
		      unsigned long term_bytes, unsigned long timeout)
{
	int ret;
	struct scdxipci_ioc_serial ser;
	ser.buffer     = buffer;
	ser.nr_bytes   = *nr_bytes_ptr;
	ser.term       = term;
	ser.term_bytes = term_bytes;
	ser.timeout    = timeout;
	ret = scdxipci_ioctl(dev, SCDXIPCI_IOC_SER_READ, &ser);
	*nr_bytes_ptr  = ser.nr_bytes;

	return ret;
}

int scdxipci_ser_write(scdxipci_t dev, char *buffer, 
		       unsigned long *nr_bytes_ptr, unsigned long block_size, 
		       unsigned long delay, int block)
{
	int ret;
	struct scdxipci_ioc_serial ser;
	ser.buffer = buffer;
	ser.nr_bytes = *nr_bytes_ptr;
	ser.timeout = delay & ~SCDXIPCI_TX_BLOCK;
	if (block)
		ser.timeout |= SCDXIPCI_TX_BLOCK;
	ser.block_size = block_size;
	ret = scdxipci_ioctl(dev, SCDXIPCI_IOC_SER_WRITE, &ser);
	*nr_bytes_ptr = ser.nr_bytes;

	return ret;
}


/*--------------------------------------------------------------------------
 * buffer frames
 *--------------------------------------------------------------------------*/

int scdxipci_frame_size(scdxipci_t dev, unsigned long *frame_size_ptr)
{
	int ret;

	ret = scdxipci_ioctl(dev, SCDXIPCI_IOC_FRAME_SIZE, *frame_size_ptr);
	if (ret < 0)
		return ret;

	*frame_size_ptr = ret;
	return SCDXIPCI_OK;
}


/*--------------------------------------------------------------------------
 * buffer map/unmap
 *--------------------------------------------------------------------------*/

int scdxipci_map_buffer(scdxipci_t dev, unsigned long nr_frames, int prot, 
			int flags, unsigned long *buffer_nr_ptr)
{
	int ret, size;
	void *addr;
	unsigned long frame_size;
	struct img_buffer_info buffer_info;

	struct scdxipci_dev *pdev = get_pdev(dev);
	if (!pdev)
		return SCDXIPCI_ERR_NODEV;

	if (nr_frames == 0)
		return -EINVAL;

	frame_size = SCDXIPCI_INVALID;
	ret = scdxipci_frame_size(dev, &frame_size);
	if (ret < 0)
		return ret;

	size = nr_frames * frame_size;
	if ((nr_frames > 1) && ((frame_size % getpagesize()) != 0))
		return SCDXIPCI_ERR_BUFFSIZE;

	addr = mmap(NULL, size, prot, flags, pdev->fd, 0);
	if (addr == MAP_FAILED)
		return SCDXIPCI_ERR_STDLIB;

	buffer_info.ptr = addr;
	buffer_info.nr = SCDXIPCI_INVALID;
	ret = scdxipci_buffer_info(dev, &buffer_info);
	if (ret < 0) {
		munmap(addr, size);
		return ret;
	}

	*buffer_nr_ptr = buffer_info.nr;
	return SCDXIPCI_OK;
}

int scdxipci_unmap_buffer(scdxipci_t dev, unsigned long buffer_nr)
{
	int ret, size;
	struct img_buffer_info buffer_info;
	struct scdxipci_dev *pdev = get_pdev(dev);
	if (!pdev)
		return SCDXIPCI_ERR_NODEV;

	buffer_info.nr = buffer_nr;
	buffer_info.ptr = NULL;
	ret = scdxipci_buffer_info(dev, &buffer_info);
	if (ret < 0)
		return ret;

	size = buffer_info.nr_frames * buffer_info.frame_size;
	return munmap(buffer_info.ptr, size);
}


/*--------------------------------------------------------------------------
 * buffer info / frame address
 *--------------------------------------------------------------------------*/

int scdxipci_buffer_info(scdxipci_t dev, struct img_buffer_info *binfo)
{
	struct scdxipci_ioc_buffer_info buffer_info = { *binfo };
	int ret;

	ret = scdxipci_ioctl(dev, SCDXIPCI_IOC_BUFFER_INFO, &buffer_info);
	*binfo = buffer_info.binfo;
	return ret;
}


int scdxipci_frame_address(scdxipci_t dev, unsigned long buffer_nr, 
			   unsigned long frame_nr, void **addr_ptr)
{
	struct img_buffer_info binfo;
	int ret;
	binfo.nr = buffer_nr;
	binfo.ptr = NULL;
	ret = scdxipci_buffer_info(dev, &binfo);
	if (ret < 0)
		return ret;
	if (frame_nr >= binfo.nr_frames)
		return SCDXIPCI_ERR_FRAMENR;
	*addr_ptr = (char *) binfo.ptr + frame_nr * binfo.frame_size;
	return SCDXIPCI_OK;
}

/*--------------------------------------------------------------------------
 * start/stop/active DMA
 *--------------------------------------------------------------------------*/

int scdxipci_start_dma(scdxipci_t dev, struct img_frame_info *finfo,
		       unsigned long timeout)
{
	int ret;
	struct scdxipci_ioc_dma dma;
	dma.finfo = *finfo;
	dma.timeout = timeout;
	dma.thread_id = (unsigned long) pthread_self();
	ret = scdxipci_ioctl(dev, SCDXIPCI_IOC_START_DMA, &dma);
	*finfo = dma.finfo;
	return ret;
}

int scdxipci_stop_dma(scdxipci_t dev)
{
	return scdxipci_ioctl(dev, SCDXIPCI_IOC_STOP_DMA, 0);
}

int scdxipci_dma_active(scdxipci_t dev, unsigned long *acq_run_nr_ptr)
{
	int ret;
	struct scdxipci_ioc_dma_act act;
	ret = scdxipci_ioctl(dev, SCDXIPCI_IOC_DMA_ACTIVE, &act);
	if (ret < 0)
		return ret;
	if (acq_run_nr_ptr != NULL)
		*acq_run_nr_ptr = act.acq_run_nr;
	return act.active;
}


/*--------------------------------------------------------------------------
 * get frame
 *--------------------------------------------------------------------------*/

int scdxipci_get_frame(scdxipci_t dev, struct img_frame_info *finfo,
		       unsigned long timeout)
{
	int ret;
	struct scdxipci_ioc_dma dma;
	dma.finfo = *finfo;
	dma.timeout = timeout;
	dma.thread_id = (unsigned long) pthread_self();
	ret = scdxipci_ioctl(dev, SCDXIPCI_IOC_GET_FRAME, &dma);
	*finfo = dma.finfo;
	return ret;
}


/*--------------------------------------------------------------------------
 * CCD status
 *--------------------------------------------------------------------------*/

int scdxipci_ccd_status(scdxipci_t dev, unsigned char *status_ptr, 
			unsigned long timeout)
{
	int ret;
	ret = scdxipci_ioctl(dev, SCDXIPCI_IOC_CCD_STATUS, timeout);
	if (ret < 0)
		return ret;
	*status_ptr = ret;
	return SCDXIPCI_OK;
}


/*--------------------------------------------------------------------------
 * get stats
 *--------------------------------------------------------------------------*/

int scdxipci_get_stats(scdxipci_t dev, struct scdxipci_stats *stats)
{
	int ret;
	struct scdxipci_ioc_stats ioc_stats;
	ret = scdxipci_ioctl(dev, SCDXIPCI_IOC_STATS, &ioc_stats);
	*stats = ioc_stats.stats;
	return ret;
}


/*--------------------------------------------------------------------------
 * set SG table
 *--------------------------------------------------------------------------*/

int scdxipci_set_sg(scdxipci_t dev, int dev_idx, struct scdxipci_sg_table *sg)
{
	struct scdxipci_ioc_sg ioc_sg;
	ioc_sg.dev_idx = dev_idx;
	ioc_sg.table = *sg;
	return scdxipci_ioctl(dev, SCDXIPCI_IOC_SET_SG, &ioc_sg);
}

/*--------------------------------------------------------------------------
 * set the device list for meta dev acq.
 *--------------------------------------------------------------------------*/

int scdxipci_set_dev(scdxipci_t dev, int *dev_list, unsigned long nr_dev)
{
	struct scdxipci_ioc_dev_list ioc_dev_list;
	ioc_dev_list.dev_list = dev_list;
	ioc_dev_list.nr_dev = nr_dev;
	return scdxipci_ioctl(dev, SCDXIPCI_IOC_SET_DEV, &ioc_dev_list);
}

/*--------------------------------------------------------------------------
 * check if the specified is already waiting, and wait if not
 *--------------------------------------------------------------------------*/

int scdxipci_check_wait(scdxipci_t dev, unsigned long thread_id,
			unsigned long timeout)
{
	struct scdxipci_ioc_chk_wait ioc_chk_wait;
	ioc_chk_wait.thread_id = thread_id;
	ioc_chk_wait.timeout = timeout;
	return scdxipci_ioctl(dev, SCDXIPCI_IOC_CHK_WAIT, &ioc_chk_wait);
}

/*--------------------------------------------------------------------------
 * get all the kernel managed hardware info
 *--------------------------------------------------------------------------*/

int scdxipci_hw_info(scdxipci_t dev, unsigned long action,  
		     struct scdxipci_hw_info *hw_info)
{
	struct scdxipci_ioc_hw_info ioc_hw_info;
	int ret;

	ioc_hw_info.action = action;
	ioc_hw_info.hw_info = *hw_info;

	ret = scdxipci_ioctl(dev, SCDXIPCI_IOC_HW_INFO, &ioc_hw_info); 
	*hw_info = ioc_hw_info.hw_info;
	return ret;
}

