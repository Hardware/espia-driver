/****************************************************************************
 * File:	scdxipci_lib.h
 * $Header: /segfs/bliss/cvs/driver/ESRF/espia/src/scdxipci_lib.h,v 2.11 2011/04/08 13:23:28 ahoms Exp $
 * Project:	SECAD Espia card Linux driver
 * Description:	Driver library header file
 * Author(s):	SECAD S.A.
 *		A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 ****************************************************************************/

#ifndef _SCDXIPCI_LIB_H
#define _SCDXIPCI_LIB_H

#include "scdxipci.h"

#define SCDXIPCI_DEV_INVAL	(-1)

// the type of the device handle
typedef int scdxipci_t;


#define SCDXIPCI_ERR_BASE_LIB		(SCDXIPCI_ERR_BASE - 100)
#define SCDXIPCI_ERR_NOFREEDEV	        (SCDXIPCI_ERR_BASE_LIB - 1)
#define SCDXIPCI_ERR_NODEV	        (SCDXIPCI_ERR_BASE_LIB - 2)
#define SCDXIPCI_ERR_BUFFSIZE	        (SCDXIPCI_ERR_BASE_LIB - 3)
#define SCDXIPCI_ERR_MAX_LIB		(SCDXIPCI_ERR_BASE_LIB - 3)

#ifdef __cplusplus
extern "C" {
#endif 

// return a message describing the specified error
char *scdxipci_strerror(int error);

// open a device and return its handle in *dev_ptr
int scdxipci_open(const char *devname, scdxipci_t *dev_ptr);

// close the device
int scdxipci_close(scdxipci_t dev);

// read and/or write a driver option
int scdxipci_option(scdxipci_t dev, int option, int action, int *value_ptr);

// read the specified register, return value in *data_ptr
int scdxipci_read_register (scdxipci_t dev, unsigned long reg_off, 
			    unsigned int *data_ptr);
// write on the specified register, modifying only the bits given by mask with
// the corresponding values in *data_ptr, where the original value is returned
int scdxipci_write_register(scdxipci_t dev, unsigned long reg_off, 
			    unsigned int *data_ptr, unsigned int mask);

// reset the aurora link
int scdxipci_reset_link(scdxipci_t dev);

// serial read. *nr_bytes_ptr is the length of the buffer when called,
// and the nr of transferred bytes when return. timeout is the maximum time
// to wait, in us; 0 means no block; SCDXIPCI_BLOCK_FOREVER block forever
int scdxipci_ser_read (scdxipci_t dev, char *buffer, 
		       unsigned long *nr_bytes_ptr, char *term,
		       unsigned long term_bytes, unsigned long timeout);
// idem. delay is time between character writes; block request to wait.
int scdxipci_ser_write(scdxipci_t dev, char *buffer, 
		       unsigned long *nr_bytes_ptr, unsigned long block_size,
		       unsigned long delay, int block);

// set the number of frame size (for future allocations)
int scdxipci_frame_size(scdxipci_t dev, unsigned long *frame_size_ptr);

// create a new acquisition buffer of size bytes, with the given protection
// and mapping flags. return the buffer number in *buff_nr_ptr
int scdxipci_map_buffer  (scdxipci_t dev, unsigned long nr_frames, int prot, 
			  int flags, unsigned long *buffer_nr_ptr);
// unmap and free the buffer buff_nr
int scdxipci_unmap_buffer(scdxipci_t dev, unsigned long buffer_nr);

// get the information about the buffer
int scdxipci_buffer_info(scdxipci_t dev, struct img_buffer_info *binfo);
// get the frame pointer, if buffer_nr == -1, assume acq_frame_nr
int scdxipci_frame_address(scdxipci_t dev, unsigned long buffer_nr, 
			   unsigned long frame_nr, void **addr_ptr);

// start the DMA acq. of finfo->frame_nr of frames, from finfo->buffer on
// (or first if NULL), and block for timeout us (if >0) or forever 
// (SCEXIPCI_BLOCK_FOREVER). if timeout > 0, wait for the end of the acq.
// and return the info of the last frame
int scdxipci_start_dma (scdxipci_t dev, struct img_frame_info *finfo,
			unsigned long timeout);
// get the info of the frame specified by finfo or of the last available if
// ANY specified. 
int scdxipci_get_frame (scdxipci_t dev, struct img_frame_info *finfo,
			unsigned long timeout);
// stop the current acquisition, if running
int scdxipci_stop_dma  (scdxipci_t dev);
// return the acquisition run number and if is running
int scdxipci_dma_active(scdxipci_t dev, unsigned long *acq_run_nr_ptr);

// get the CCD status byte; if requested, block until a new status is received
int scdxipci_ccd_status(scdxipci_t dev, unsigned char *status_ptr, 
			unsigned long timeout);

// get the acq statistics
int scdxipci_get_stats(scdxipci_t dev, struct scdxipci_stats *stats);

// set the SG table
int scdxipci_set_sg(scdxipci_t dev, int dev_idx, struct scdxipci_sg_table *sg);

// set the device list for meta dev acq.
int scdxipci_set_dev(scdxipci_t dev, int *dev_list,  unsigned long nr_dev);

// check if the specified is already waiting, and wait if not
int scdxipci_check_wait(scdxipci_t dev, unsigned long thread_id,  
			unsigned long timeout);

// manage hardware info.
int scdxipci_hw_info(scdxipci_t dev, unsigned long action,  
		     struct scdxipci_hw_info *hw_info);


#ifdef __cplusplus
}
#endif 

#endif /* _SCDXIPCI_LIB_H */
