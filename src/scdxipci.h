/****************************************************************************
 * File:	scdxipci.h
 * $Header: /segfs/bliss/cvs/driver/ESRF/espia/src/scdxipci.h,v 2.18 2016/03/07 11:39:37 ahoms Exp $
 * Project:	SECAD Espia card Linux driver
 * Description:	Driver public header file
 * Author(s):	SECAD S.A.
 *		A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 ****************************************************************************/

#ifndef _SCDXIPCI_H
#define _SCDXIPCI_H


/*--------------------------------------------------------------------------
 * The default device name
 *--------------------------------------------------------------------------*/

#ifndef SCDXIPCI_DRV_NAME
#define SCDXIPCI_DRV_NAME		"scdxipci"
#endif


/*--------------------------------------------------------------------------
 * Some defs
 *--------------------------------------------------------------------------*/

#include "scdxipci_registers.h"

#define SCDXIPCI_VERSION_LEN	50
#define SCDXIPCI_REGS_SIZE	256
#define SCDXIPCI_DEF_TX_DELAY	0
#define SCDXIPCI_TX_DELAY	5	// in us
#define SCDXIPCI_TX_BLOCK	(1UL << 31)
#define SCDXIPCI_NO_BLOCK	0
#define SCDXIPCI_BLOCK_FOREVER	((unsigned long) -1)

#define SCDXIPCI_INVALID	((unsigned long) -1)
#define SCDXIPCI_INVALID32	((unsigned int) -1)
#define SCDXIPCI_ENDLESS_ACQ	SCDXIPCI_INVALID
#define SCDXIPCI_ANY		SCDXIPCI_INVALID
#define SCDXIPCI_REQUEST	SCDXIPCI_INVALID

#define SCDXIPCI_META_DEV	255

#define SCDXIPCI_NR_DEV_STATS	4

enum {
	SCDXIPCI_ESPIA_PCI,
	SCDXIPCI_ESPIA_EXPRESS,
	SCDXIPCI_NR_BRD_TYPES
};

enum {
	SCDXIPCI_DEV_1_FO,
	SCDXIPCI_NR_DEV_TYPES
};

#define SCDXIPCI_FW_NAME_LEN	16

#define scdxipci_is(what, val) \
	((unsigned long)(val) == SCDXIPCI_##what)

/*--------------------------------------------------------------------------
 * IOCTL structures and commands
 *--------------------------------------------------------------------------*/

#include <linux/ioctl.h>


struct scdxipci_ioc_version {
	char version[SCDXIPCI_VERSION_LEN];
};

enum {
	SCDXIPCI_OPT_DEBUG_LEVEL,
	SCDXIPCI_OPT_NO_FIFO_RESET,
	SCDXIPCI_OPT_KEEP_RELEASED_PAGES,
	SCDXIPCI_NR_OPT
};

#define SCDXIPCI_OPT_RD		1
#define SCDXIPCI_OPT_WR		2
#define SCDXIPCI_OPT_RD_WR	(SCDXIPCI_OPT_RD | SCDXIPCI_OPT_WR)

struct scdxipci_ioc_option {
	unsigned int  option;
	unsigned int  action;
	unsigned int  value;
};

struct scdxipci_ioc_register {
	unsigned long reg_off;
	unsigned int  data;
	unsigned int  mask;
};

/*--------------------------------------------------------------------------
 * in start dma:
 *	buffer_nr    - buffer to start from
 *	acq_frame_nr - total nr of frames to acquire or 0=infinite
 *
 *   on return:
 *      like get frame asking last acq_frame_nr
 *
 * in get frame (if all=any and nobloc -> get last frame):
 *	buffer_nr    - buffer to get or SCDXIPCI_ANY
 *	frame_nr     - nr of frame to get or SCDXIPCI_ANY
 *      round_count  - frame (over)write idx to get or SCDXIPCI_ANY
 *	acq_frame_nr - acq. frame nr to get (if prev=any) or SCDXIPCI_ANY
 *
 *   on return, if not-overrun && (frame-ready || (block & not-interrupted)):
 *	buffer_nr    - buffer nr  (current, if asked any)
 *	buffer_ptr   - buffer ptr (current, if asked any)
 *	frame_nr     - nr of frame (current, if asked any)
 *      round_count  - frame (over)write idx (current, if asked any)
 *	acq_frame_nr - current acq. frame nr (if asked any)
 *      pixels       - nr. of pixes in frame
 *	time_us      - time-stamp of the frame
 *--------------------------------------------------------------------------*/
	
struct img_frame_info {
	void			*buffer_ptr;
	unsigned long		 buffer_nr;
	unsigned long	 	 frame_nr;
	unsigned long		 round_count;
	unsigned long		 acq_frame_nr;
	unsigned long		 acq_run_nr;
	unsigned long		 pixels;
	unsigned long		 time_us;
};	

#define finished_img_frame_info(finfo) \
	(scdxipci_is(INVALID, (finfo)->buffer_nr) &&	\
	 scdxipci_is(INVALID, (finfo)->frame_nr) && \
	 scdxipci_is(INVALID, (finfo)->round_count) && \
	 scdxipci_is(INVALID, (finfo)->acq_frame_nr))

#define scdxipci_fatal_error(ret) \
	(((ret) != SCDXIPCI_ERR_TIMEOUT) && ((ret) != SCDXIPCI_ERR_OVERRUN))

struct scdxipci_ioc_dma {
	struct img_frame_info	 finfo;
	unsigned long		 timeout;
	unsigned long		 thread_id;
};

struct scdxipci_ioc_dma_act {
	unsigned long		 acq_run_nr;
	int			 active;
};

struct scdxipci_ioc_serial {
	char			*buffer;
	unsigned long		 nr_bytes;
	char			*term;
	unsigned long		 term_bytes;
	unsigned long		 timeout;
	unsigned long		 block_size;
};

struct img_buffer_info {
	unsigned long		 nr;
	unsigned long		 nr_frames;
	unsigned long		 frame_size;
	void			*ptr;
};

struct scdxipci_ioc_buffer_info {
	struct img_buffer_info	 binfo;
};

struct scdxipci_dev_stats {
	unsigned long long	 sg_usec;
	unsigned long long	 sg_count;
	unsigned long long	 sg_avail_acc;
	unsigned long long	 sg_avail_count;
	unsigned long		 sg_avail_min;
};

struct scdxipci_stats {
	// SG data info
	struct scdxipci_dev_stats dev_stats[SCDXIPCI_NR_DEV_STATS];
	// memory copy info
	unsigned long long	 copy_usec;
	unsigned long long	 copy_pages;
	unsigned long long	 copy_count;
	unsigned long		 nr_pages64;
	unsigned long		 nr_pages;
	// get acq_frame_nr info
	unsigned long long	 afn_call;
	unsigned long long	 afn_count;
	unsigned long long	 afn_usec;
	unsigned long long	 afn_iter;
	unsigned long long	 afn_step;
	unsigned long 		 afn_max_iter;
	unsigned long 		 afn_max_step;
};

struct scdxipci_ioc_stats {
	struct scdxipci_stats	 stats;
};

struct scdxipci_sg_table {
	struct scdxipci_sg_block	*ptr;
	unsigned long			 len;
};

struct scdxipci_ioc_sg {
	unsigned long			 dev_idx;
	struct scdxipci_sg_table	 table;
};

struct scdxipci_ioc_dev_list {
	int				*dev_list;
	unsigned long			 nr_dev;
};

struct scdxipci_ioc_chk_wait {
	unsigned long			 thread_id;
	unsigned long			 timeout;
};

struct scdxipci_board_id_info {
	unsigned int			 start;
	unsigned int			 len;
	unsigned int			 crc_ok;
	unsigned int			 ser_nr;
	void				*ptr;
};

#define scdxipci_ser_nr_len(ser_nr) \
	(((ser_nr) < 100000) ? 6 : (((ser_nr) < 1000000) ? 7 : 8))

struct scdxipci_firmware_info {
	unsigned int			 start; 
	unsigned int			 len;
	unsigned int			 checksum;
	char				 name[SCDXIPCI_FW_NAME_LEN];
	void				*ptr;
};

struct scdxipci_hw_info {
	unsigned int			 board_type;
	unsigned int			 board_idx;
	unsigned int			 dev_type;
	unsigned int			 dev_idx;
	unsigned int			 capabilities;
	struct scdxipci_board_id_info	 board_id;
	struct scdxipci_firmware_info	 firmware;
};

enum {
	SCDXIPCI_HW_INFO_GET,
};

struct scdxipci_ioc_hw_info {
	unsigned int			 action;
	struct scdxipci_hw_info		 hw_info;
};

union scdxipci_ioc {
	struct scdxipci_ioc_version	 ver;
	struct scdxipci_ioc_option	 opt;
	struct scdxipci_ioc_register	 reg;
	struct scdxipci_ioc_dma		 dma;
	struct scdxipci_ioc_dma_act	 act;
	struct scdxipci_ioc_serial	 ser;
	struct scdxipci_ioc_buffer_info	 buff;
	struct scdxipci_ioc_stats	 stats;
	struct scdxipci_ioc_sg		 sg;
	struct scdxipci_ioc_dev_list	 dev;
	struct scdxipci_ioc_chk_wait     wait;
	struct scdxipci_ioc_hw_info      hw;
};


#define SCDXIPCI_IOC_MAGIC		0xBA
#define SCDXIPCI_IOC_ALL		0x00
#define SCDXIPCI_IOC_NORM		0x40
#define SCDXIPCI_IOC_META		0x80

#define SCD_IO(cmd, type)		\
	_IO  (SCDXIPCI_IOC_MAGIC, (cmd) | SCDXIPCI_IOC_##type)
#define SCD_IOW(cmd, type, stc)		\
	_IOW (SCDXIPCI_IOC_MAGIC, (cmd) | SCDXIPCI_IOC_##type, stc)
#define SCD_IOR(cmd, type, stc)		\
	_IOR (SCDXIPCI_IOC_MAGIC, (cmd) | SCDXIPCI_IOC_##type, stc)
#define SCD_IOWR(cmd, type, stc)	\
	_IOWR(SCDXIPCI_IOC_MAGIC, (cmd) | SCDXIPCI_IOC_##type, stc)

#define SCDXIPCI_IOC_VERSION		\
	SCD_IOWR( 0, ALL,  struct scdxipci_ioc_version)
#define SCDXIPCI_IOC_OPTION		\
	SCD_IOWR( 1, ALL,  struct scdxipci_ioc_option)
#define SCDXIPCI_IOC_READ_REGISTER	\
	SCD_IOWR( 2, NORM, struct scdxipci_ioc_register)
#define SCDXIPCI_IOC_WRITE_REGISTER	\
	SCD_IOWR( 3, NORM, struct scdxipci_ioc_register)
#define SCDXIPCI_IOC_RESET_LINK		\
	SCD_IO  ( 4, NORM)
#define SCDXIPCI_IOC_REQUEST_ASYNC	\
	SCD_IO  ( 5, ALL)
#define SCDXIPCI_IOC_ASYNC_STATUS	\
	SCD_IO  ( 6, ALL)
#define SCDXIPCI_IOC_FRAME_SIZE		\
	SCD_IO  ( 7, ALL)
#define SCDXIPCI_IOC_START_DMA		\
	SCD_IOWR( 8, ALL,  struct scdxipci_ioc_dma)
#define SCDXIPCI_IOC_GET_FRAME		\
	SCD_IOWR( 9, ALL,  struct scdxipci_ioc_dma)
#define SCDXIPCI_IOC_STOP_DMA		\
	SCD_IO  (10, ALL)
#define SCDXIPCI_IOC_DMA_ACTIVE		\
	SCD_IOR (11, ALL,  struct scdxipci_ioc_dma_act)
#define SCDXIPCI_IOC_SER_READ		\
	SCD_IOWR(12, NORM, struct scdxipci_ioc_serial)
#define SCDXIPCI_IOC_SER_WRITE		\
	SCD_IOWR(13, NORM, struct scdxipci_ioc_serial)
#define SCDXIPCI_IOC_CCD_STATUS		\
	SCD_IO  (14, NORM)
#define SCDXIPCI_IOC_BUFFER_INFO	\
	SCD_IOWR(15, ALL,  struct scdxipci_ioc_buffer_info)
#define SCDXIPCI_IOC_STATS		\
	SCD_IOR (16, ALL,  struct scdxipci_ioc_stats)
#define SCDXIPCI_IOC_SET_SG		\
	SCD_IOW (17, ALL,  struct scdxipci_ioc_sg)
#define SCDXIPCI_IOC_SET_DEV		\
	SCD_IOW (18, META, struct scdxipci_ioc_dev_list)
#define SCDXIPCI_IOC_CHK_WAIT		\
	SCD_IOW (19, ALL,  struct scdxipci_ioc_chk_wait)
#define SCDXIPCI_IOC_HW_INFO		\
	SCD_IOWR(20, NORM,  struct scdxipci_ioc_hw_info)


/*--------------------------------------------------------------------------
 * Error codes
 *--------------------------------------------------------------------------*/

#define SCDXIPCI_OK			0
#define SCDXIPCI_ERR_TIMEOUT		(-ETIMEDOUT)
#define SCDXIPCI_ERR_STDLIB		(-1)
#define SCDXIPCI_ERR_BASE	        (-1000)
#define SCDXIPCI_ERR_INTERNAL		(SCDXIPCI_ERR_BASE - 0)
#define SCDXIPCI_ERR_ACQ		(SCDXIPCI_ERR_BASE - 1)
#define SCDXIPCI_ERR_BUFFER		(SCDXIPCI_ERR_BASE - 2)
#define SCDXIPCI_ERR_FRAMENR		(SCDXIPCI_ERR_BASE - 3)
#define SCDXIPCI_ERR_OVERRUN		(SCDXIPCI_ERR_BASE - 4)
#define SCDXIPCI_ERR_NOTREADY		(SCDXIPCI_ERR_BASE - 5)
#define SCDXIPCI_ERR_WONTARR		(SCDXIPCI_ERR_BASE - 6)
#define SCDXIPCI_ERR_VERSION		(SCDXIPCI_ERR_BASE - 7)
#define SCDXIPCI_ERR_AUXBUFF		(SCDXIPCI_ERR_BASE - 8)
#define SCDXIPCI_ERR_NOIMPL		(SCDXIPCI_ERR_BASE - 9)
#define SCDXIPCI_ERR_RUNNING		(SCDXIPCI_ERR_BASE - 10)
#define SCDXIPCI_ERR_ACQNODEV		(SCDXIPCI_ERR_BASE - 11)
#define SCDXIPCI_ERR_META		(SCDXIPCI_ERR_BASE - 12)
#define SCDXIPCI_ERR_NOMETA		(SCDXIPCI_ERR_BASE - 13)
#define SCDXIPCI_ERR_LOSTBUFF		(SCDXIPCI_ERR_BASE - 14)
#define SCDXIPCI_ERR_BOUNDARY		(SCDXIPCI_ERR_BASE - 15)
#define SCDXIPCI_ERR_DMAALIGN		(SCDXIPCI_ERR_BASE - 16)
#define SCDXIPCI_ERR_DEVBUSY		(SCDXIPCI_ERR_BASE - 17)
#define SCDXIPCI_ERR_BIGSG		(SCDXIPCI_ERR_BASE - 18)
#define SCDXIPCI_ERR_MEMTOOHIGH		(SCDXIPCI_ERR_BASE - 19)
#define SCDXIPCI_ERR_TOOMUCHRAM		(SCDXIPCI_ERR_BASE - 20)
#define SCDXIPCI_ERR_MAX		(SCDXIPCI_ERR_BASE - 20)


/*--------------------------------------------------------------------------
 * The debug levels
 *--------------------------------------------------------------------------*/

enum {
	SCDXIPCI_DEB_NONE = 0,
	SCDXIPCI_DEB_ERRORS,
	SCDXIPCI_DEB_PROBE,
	SCDXIPCI_DEB_OPEN,
	SCDXIPCI_DEB_IOCTL,
	SCDXIPCI_DEB_PARAMS,
	SCDXIPCI_DEB_INTS,
	SCDXIPCI_DEB_SERIAL,
	SCDXIPCI_DEB_REGS,
	SCDXIPCI_DEB_FLASH,
	SCDXIPCI_DEB_MAX
};

#endif /* _SCDXIPCI_H */
