/****************************************************************************
 * File:	espia_test.c
 * $Header: /segfs/bliss/cvs/driver/ESRF/espia/src/espia_test.c,v 2.37 2020/07/23 11:00:41 ahoms Exp $
 * Project:	SECAD Espia card Linux driver
 * Description:	Espia library test program
 * Author(s):	SECAD S.A.
 *		A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 ****************************************************************************/

#include "espia_test.h"

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <regex.h>

#define FILE_SAVE_MODE	(S_IRUSR | S_IRGRP | S_IROTH | \
			 S_IWUSR | S_IWGRP | S_IWOTH)

#define ESPIA_TEST_SERIAL_FILENAME "/tmp/espia%d_serial_%s"

/*--------------------------------------------------------------------------
 * rcs id
 *--------------------------------------------------------------------------*/

static char rcs_id[] __attribute__((unused))  = 
	"$Header: /segfs/bliss/cvs/driver/ESRF/espia/src/espia_test.c,v 2.37 2020/07/23 11:00:41 ahoms Exp $";


/*--------------------------------------------------------------------------
 * the table with the application functions
 *--------------------------------------------------------------------------*/


struct funct_data all_funcs[] = {
	{"OptionList",    option_list, "List available driver options",
	 {{NULL}}},
	{"GetOption",     get_option, "Get one or all driver options",
	 {{"option",      PARAM_STR, "Option name or ALL"}, 
	  {NULL}}},
	{"SetOption",     set_option, "Set a driver option",
	 {{"option",      PARAM_STR, "Option name"}, 
	  {"val",         PARAM_INT, "Value [used only in Write]"}, 
	  {NULL}}},
	{"DebugLevel",    set_debug_level, "Set driver or lib. debug level",
	 {{"deb_lvl",     PARAM_INT, "Debug level [0=None, 3=Param, -1=Get]"}, 
	  {"drv",         PARAM_INT, "Set driver [=1] or lib [=0] deb lvl"}, 
	  {NULL}}},
	{"Open",          open_dev, "Open an Espia device",
	 {{"dev_nr",      PARAM_INT, "Number of device [0-254, 255=Meta]"}, 
	  {NULL}}},
	{"Select",        select_dev, "Select an Espia device as active",
	 {{"dev_nr",      PARAM_INT, "Number of device [0-255]"}, 
	  {NULL}}},
	{"Close",         close_dev, "Close the active Espia device",
	 {{NULL}}},
	{"DevList",       dev_list, "List all open devices",
	 {{NULL}}},
	{"ReadRegister",  read_register, "Read from a card register",
	 {{"reg_off",     PARAM_INT, "Register offset in bytes [0-255]"}, 
	  {NULL}}},
	{"WriteRegister", write_register, "Read/write an Espia register",
	 {{"reg_off",     PARAM_INT, "Register offset in bytes [0-255]"}, 
	  {"data",        PARAM_INT, "Value to write"}, 
	  {"mask",        PARAM_INT, "Mask of bits to modify"}, 
	  {NULL}}},
	{"ParamList",     param_list, "List available Espia parameters",
	 {{NULL}}},
	{"GetParam",      get_param, "Read one or all Espia parameters",
	 {{"param",       PARAM_STR, "Espia parameter name or ALL"}, 
	  {NULL}}},
	{"SetParam",      set_param, "Set the specified Espia parameter",
	 {{"param",       PARAM_STR, "Espia parameter name or ALL"}, 
	  {"val",         PARAM_INT, "Parameter value"}, 
	  {NULL}}},
	{"ResetLink",     reset_link, "Reset the Aurora link",
	 {{NULL}}},
	{"SerialRead",    serial_read, "Read bytes from the serial line",
	 {{"nr_bytes",    PARAM_INT, "Number of bytes to read"}, 
	  {"timeout",     PARAM_INT, "Timeout in us [0=Noblock, -1=Forever]"}, 
	  {"raw",         PARAM_INT, "Print raw string (not ASCII)"}, 
	  {NULL}}},
	{"SerialReadStr", serial_read_str, "Read the serial line until term.",
	 {{"nr_bytes",    PARAM_INT, "Number of bytes to read"}, 
	  {"term",        PARAM_STR, "Terminator string"}, 
	  {"timeout",     PARAM_INT, "Timeout in us [0=Noblock, -1=Forever]"}, 
	  {"raw",         PARAM_INT, "Print raw string (not ASCII)"}, 
	  {NULL}}},
	{"SerialWrite",   serial_write, "Write bytes to the serial line",
	 {{"string",      PARAM_STR, "String to write - can use \" or '"}, 
	  {"block_size",  PARAM_INT, "Nr. of bytes before a delay"}, 
	  {"delay",       PARAM_INT, "Time between writes, in us"}, 
	  {"block",       PARAM_INT, "Wait until end of TX [0=No, 1=Yes]"}, 
	  {NULL}}},
	{"SerialFlush",   serial_flush, "Flush the serial RX buffer",
	 {{NULL}}},
	{"SerialMmapInit",serial_mmap_init, "Initialize mmap files for serial "
					    "ops",
	 {{"max_bytes",   PARAM_INT, "Maximum nb of bytes in transfer"},
	  {NULL}}},
	{"SerialMmapClose",serial_mmap_close, "Close mmap files for serial ops",
	 {{NULL}}},
	{"SerialMmapRead",serial_mmap_read, "Read bytes from serial and write "
					 "to mmap file",
	 {{"nr_bytes",    PARAM_INT, "Number of bytes to read"},
	  {"timeout",     PARAM_INT, "Timeout in us [0=Noblock, -1=Forever]"},
	  {"print", 	  PARAM_INT, "Print out raw string read [0=No, 1=Yes]"},
	  {NULL}}},
	{"SerialMmapWrite", serial_mmap_write, "Write bytes read from mmap "
					       "file to serial",
	 {{"block_size",  PARAM_INT, "Nr. of bytes before a delay"},
	  {"delay",       PARAM_INT, "Time between writes, in us"},
	  {"block",       PARAM_INT, "Wait until end of TX [0=No, 1=Yes]"},
	  {"print",       PARAM_INT, "Print out raw str written [0=No, 1=Yes]"},
	  {NULL}}},
	{"FramePostOp",   frame_post_op, "Set active frame post-operations",
	 {{"op_list",     PARAM_STR, "Op list: op_name[,op_name...]"},
	  {NULL}}},
	{"FramePostOpList",frame_post_op_list, "List avail. frame post-op",
	 {{NULL}}},
	{"BufferAlloc",   buffer_alloc, "Alloc buffers, made of frames",
	 {{"nr_buffer",   PARAM_INT, "Nr. of buffers"}, 
	  {"buff_frm",    PARAM_INT, "Nr. of frames per buffer"}, 
	  {"width",       PARAM_INT, "Nr. of columns per frame"}, 
	  {"height",      PARAM_INT, "Nr. of rows per frame"}, 
	  {"depth",       PARAM_INT, "Image bits per pixel (in bytes)"}, 
	  {"display",     PARAM_INT, "Create a display for each buffer"}, 
	  {NULL}}},
	{"BufferFree",    buffer_free, "Free the allocated buffers",
	 {{NULL}}},
	{"BufferList",    buffer_list, "List the allocated buffers' info",
	 {{NULL}}},
	{"BufferDump",    buffer_dump, "Show the binary data in buffer",
	 {{"buffer_nr",   PARAM_INT, "Nr. of buffer"}, 
	  {"offset",      PARAM_INT, "Nr. bytes to skip at begin"}, 
	  {"nr_bytes",    PARAM_INT, "Nr. of bytes to dump"}, 
	  {NULL}}},
	{"BufferSave",    buffer_save, "Save buffer in a file",
	 {{"buffer_nr",   PARAM_INT, "Nr. of buffer [-1=Auto-save]"}, 
	  {"fname_prefix",PARAM_STR, "Name prefix of file to write [?=Get]"},
	  {"fname_idx",   PARAM_INT_OPT, "Index of file in serie [-1=Keep]"},
	  {"format",      PARAM_INT_OPT, "Format of file [0=Raw, 1=EDF]"}, 
	  {"file_frames", PARAM_INT_OPT, "Nr. of frames per file"}, 
	  {"overwrite",   PARAM_INT_OPT, "Force file truncation (overwrite)"}, 
	  {NULL}}},
	{"BufferClear",   buffer_clear, "Clear buffer to 0",
	 {{"buffer_nr",   PARAM_INT, "Nr. of buffer [-1=All]"}, 
	  {NULL}}},
#ifdef WITH_IMAGE
	{"BufferImage",   buffer_img, "Create buffer display window",
	 {{"buffer_nr",   PARAM_INT, "Nr. of buffer [-1=Live]"}, 
	  {NULL}}},
	{"BufferImageDel",buffer_img_del, "Destroy buffer display window",
	 {{"buffer_nr",   PARAM_INT, "Nr. of buffer [-1=Live]"}, 
	  {NULL}}},
	{"BufferImageRates",buffer_img_rates, "Print buffer display rates",
	 {{"buffer_nr",   PARAM_INT, "Nr. of buffer"}, 
	  {NULL}}},
	{"BufferImageNorm",buffer_img_norm, "Set/Get buffer display norm.",
	 {{"buffer_nr",   PARAM_INT, "Nr. of buffer [-1=Live]"}, 
	  {"min",         PARAM_INT, "Normalization min value [-1=Get]"}, 
	  {"max",         PARAM_INT, "Normalization max value [-1=Get]"}, 
	  {"auto_range",  PARAM_INT, "Auto-range [-1=Get]"}, 
	  {NULL}}},
#endif // WITH_IMAGE
	{"RoISet",        roi_set, "Set a Scatter Gather based RoI",
	 {{"full_width",  PARAM_INT, "Full frame width"}, 
	  {"full_height", PARAM_INT, "Full frame height"}, 
	  {"roi_left",    PARAM_INT, "RoI left border"}, 
	  {"roi_top",     PARAM_INT, "RoI top border"}, 
	  {NULL}}},
	{"RoIReSet",      roi_reset, "Reset a Scatter Gather based RoI",
	 {{NULL}}},
	{"SGSet",         sg_set, "Set a Scatter Gather table",
	 {{"img_config",  PARAM_INT, "Device image config [-1=List]"}, 
	  {NULL}}},
	{"DevSet",        dev_set, "Define the device list for meta dev.",
	 {{"dev_list",    PARAM_STR, "Dev. list: dev_nr[,dev_nr...]"},
	  {NULL}}},
	{"StartAcq",      start_acq, "Start an image acquisition",
	 {{"start_buffer",PARAM_INT, "Nr. of buffer to start from"}, 
	  {"nr_frames",   PARAM_INT, "Total nr. of frames to acq."}, 
	  {"timeout",     PARAM_INT, "Timeout in us [0=NoBlock, -1=Forever]"}, 
	  {NULL}}},
	{"StopAcq",       stop_acq, "Stop the current acquisition",
	 {{NULL}}},
	{"AcqStatus",     acq_status, "Check if the acquisition is running",
	 {{NULL}}},
	{"RegAcqCb",      reg_acq_cb, "Register an acquisition callback",
	 {{"buffer_nr",   PARAM_INT, "Nr. of buffer [-1=Any, -2=Each]"}, 
	  {"frame_nr",    PARAM_INT, "Frame idx in buffer [-1=Any]"}, 
	  {"count",       PARAM_INT, "Nr. of (re)write times [-1=Any]"}, 
	  {"acq_frame_nr",PARAM_INT, "Frame idx in acq. [-1=Any, -2=Each]"}, 
	  {"timeout",     PARAM_INT, "Timeout in us [-1=Forever]"}, 
	  {NULL}}},
	{"RegStatusCb",   reg_status_cb, "Register a CCD status callback",
	 {{"timeout",     PARAM_INT, "Timeout in us [-1=Forever]"}, 
	  {NULL}}},
	{"RegSerRxCb",    reg_ser_rx_cb, "Register a serial RX callback",
	 {{"buffer_len",  PARAM_INT, "Length of the RX buffer [0=10KBytes]"}, 
	  {"term_str",    PARAM_STR, "Terminator (EOS) string"}, 
	  {"timeout",     PARAM_INT, "Timeout in us [-1=Forever]"}, 
	  {"raw",         PARAM_INT, "Print raw string (not ASCII)"}, 
	  {"filter_tout", PARAM_INT, "Filter timeout events"}, 
	  {NULL}}},
	{"UnregCb",       unreg_cb, "Unregister a callback",
	 {{"cb_nr",       PARAM_INT, "Nr. of callback"}, 
	  {NULL}}},
	{"UnregAllCb",    unreg_all_cb, "Unregister all callbacks & autosave",
	 {{NULL}}},
	{"CbActive",      cb_active, "Activate/deactivate a callback",
	 {{"cb_nr",       PARAM_INT, "Nr. of callback"}, 
	  {"active",      PARAM_INT, "Callback active [0=No,1=Yes,-1=Get]"}, 
	  {NULL}}},
	{"CbList",        cb_list, "List all the registered callbacks",
	 {{"type",        PARAM_STR, "[Acq, Status, SerialRx, SerialTx]"}, 
	  {NULL}}},
	{"GetFrame",      get_frame, "Wait and get a frame info",
	 {{"buffer_nr",   PARAM_INT, "Nr. of buffer [-1=Any]"}, 
	  {"frame_nr",    PARAM_INT, "Frame idx in buffer [-1=Any]"}, 
	  {"count",       PARAM_INT, "Nr. of (re)write times [-1=Any]"}, 
	  {"acq_frame_nr",PARAM_INT, "Frame idx in acq. [-1=Any]"}, 
	  {"timeout",     PARAM_INT, "Timeout in us [0=NoBlock, -1=Forever]"}, 
	  {NULL}}},
	{"FrameAddr",     frame_addr, "Get the frame address",
	 {{"buffer_nr",   PARAM_INT, "Nr. of buffer [-1=AcqFrameNr]"}, 
	  {"frame_nr",    PARAM_INT, "Frame idx in buffer or acq."}, 
	  {NULL}}},
	{"CcdStatus",     ccd_status, "Get CCD status, can block until change",
	 {{"timeout",     PARAM_INT, "Timeout in us [-1=BlockForever]"}, 
	  {NULL}}},
	{"RoiAccBuffer",  roi_acc_buffer, "Create the RoI accumulation buffer",
	 {{"col_beg",     PARAM_INT, "RoI first column"}, 
	  {"col_end",     PARAM_INT, "RoI last  column"}, 
	  {"row_beg",     PARAM_INT, "RoI first row"}, 
	  {"row_end",     PARAM_INT, "RoI last  row"}, 
	  {"nr_frames",   PARAM_INT, "Number of frames in buffer [0=Auto]"}, 
	  {"file_prefix", PARAM_STR, "File name prefix [\"\"=NoSave]"}, 
	  {"file_idx",    PARAM_INT, "File name index [-1=KeepLast]"}, 
	  {NULL}}},
	{"FoclaOpen",     focla_open_dev, "Open the FOCLA device",
	 {{NULL}}},
	{"FoclaClose",    focla_close_dev, "Close the FOCLA device",
	 {{NULL}}},
	{"FoclaReadReg",  focla_read, "Read a FOCLA register",
	 {{"reg",         PARAM_INT, "Register nr [0-15]"}, 
	  {NULL}}},
	{"FoclaWriteReg", focla_write, "Write on a FOCLA register",
	 {{"reg",         PARAM_INT, "Register nr [0-15]"}, 
	  {"val",         PARAM_INT, "Value to write"}, 
	  {NULL}}},
	{"FoclaParamList",focla_param_list, "List available FOCLA parameters",
	 {{NULL}}},
	{"FoclaGetParam", focla_get_par, "Get a FOCLA parameter",
	 {{"param",       PARAM_STR, "Name of the parameter"}, 
	  {NULL}}},
	{"FoclaSetParam", focla_set_par, "Set a FOCLA parameter",
	 {{"param",       PARAM_STR, "Name of the parameter"}, 
	  {"val",         PARAM_INT, "Value to set"}, 
	  {NULL}}},
	{"FoclaSignalList", focla_sig_list, "List FOCLA pulsable signal names",
	 {{NULL}}},
	{"FoclaPulseStart", focla_pulse_start, "Start a train pulses on CL CCx",
	 {{"cam_nr",      PARAM_INT, "Camera number [0-1]"}, 
	  {"sig_name",    PARAM_STR, "Signal name"}, 
	  {"polarity",    PARAM_INT, "Pulse polarity [0=Pos, 1=Neg]"}, 
	  {"width_us",    PARAM_INT, "Pulse width in usec"}, 
	  {"delay_us",    PARAM_INT, "Pulse delay in usec"}, 
	  {"nr_pulse",    PARAM_INT, "Nr. pulses to send"}, 
	  {NULL}}},
	{"FoclaPulseStop", focla_pulse_stop, "Stop the FOCLA CL CCx pulses",
	 {{"cam_nr",      PARAM_INT, "Camera number [0-1]"}, 
	  {"sig_name",    PARAM_STR, "Signal name"}, 
	  {NULL}}},
	{"FoclaPulseStatus", focla_pulse_status, "Get the CL CCx pulses status",
	 {{"cam_nr",      PARAM_INT, "Camera number [0-1]"}, 
	  {"sig_name",    PARAM_STR, "Signal name"}, 
	  {NULL}}},
	{"FoclaSerialRead",focla_serial_read, "Read bytes from the FOCLA "
					      "serial line",
	 {{"nr_bytes",    PARAM_INT, "Number of bytes to read"}, 
	  {"timeout",     PARAM_INT, "Timeout in us [0=Noblock, -1=Forever]"}, 
	  {"raw",         PARAM_INT, "Print raw string (not ASCII)"}, 
	  {NULL}}},
	{"FoclaSerialReadStr",focla_serial_read_str, "Read the FOCLA serial "
						     "line until term.",
	 {{"nr_bytes",    PARAM_INT, "Number of bytes to read"}, 
	  {"term",        PARAM_STR, "Terminator string"}, 
	  {"timeout",     PARAM_INT, "Timeout in us [0=Noblock, -1=Forever]"}, 
	  {"raw",         PARAM_INT, "Print raw string (not ASCII)"}, 
	  {NULL}}},
	{"FoclaSerialWrite",focla_serial_write, "Write bytes to the FOCLA "
						"serial line",
	 {{"string",      PARAM_STR, "String to write - can use \" or '"}, 
	  {"block_size",  PARAM_INT, "Nr. of bytes before a delay"}, 
	  {"delay",       PARAM_INT, "Time between writes, in us"}, 
	  {"block",       PARAM_INT, "Wait until end of TX [0=No, 1=Yes]"}, 
	  {NULL}}},
	{"FoclaSerialFlush",focla_serial_flush, "Flush the FOCLA serial RX "
						"buffer",
	 {{NULL}}},
	{"GetStats",      get_stats, "Get the acquisition statistics",
	 {{NULL}}},
	{"PollSleep",     poll_sleep, "Set the main loop polling sleep time",
	 {{"sleep_time",  PARAM_INT, "Sleep time in usec [-1=Get]"}, 
	  {NULL}}},
	{"FirmwareList",  firmware_list, "List all the known Espia firmwares",
	 {{NULL}}},
	{"GetHwInfo",     get_hw_info, "Get hardware related information",
	 {{NULL}}},
	{"Version",       get_version, "Print the library versions",
	 {{NULL}}},
	{"EdfUsrHeader",  edf_usr_header, "Add custom lines to the EDF header",
	 {{"usr_header",  PARAM_STR, "A string in the format "
	                  "\"key1 = value1 ;[\\nkey2 = value2 ;...]"
	                  " or \"\" to cancel the previously set string"}, 
	  {NULL}}},
	{NULL}
};


/*--------------------------------------------------------------------------
 * app_framework polling functions
 *--------------------------------------------------------------------------*/

#ifdef WITH_IMAGE
int image_refresh( app_context_t context )
{
	return image_poll();
}
#endif // WITH_IMAGE

poll_funct *test_poll_funct[] = {
#ifdef WITH_IMAGE
	(poll_funct *) image_refresh,
#endif // WITH_IMAGE
};

int test_poll_funct_len = (sizeof(test_poll_funct) / 
			   sizeof(test_poll_funct[0]));

/*--------------------------------------------------------------------------
 * init / cleanup
 *--------------------------------------------------------------------------*/

int test_init(app_context_t context, int argc, char *argv[])
{
	int i, len, ret;
	struct app_data *app_data;
	struct device_data *dev_data;
	struct buffer_save_data *bsd;
	union param devnr;

	ret = -ENOMEM;
	len = sizeof(*app_data);
	app_data = (struct app_data *) calloc(1, len);
	if (app_data == NULL) {
		context_err(context, "Error alloc. device data (%d bytes)\n", 
			    len);
		goto out;
	}

	init_context(context, all_funcs, test_error_funct, app_data);

	for (i = 0; i < ESPIA_TEST_MAX_NR_DEV; i++) {
		dev_data = &app_data->device[i];
		dev_data->dev = ESPIA_DEV_INVAL;
		dev_data->focla = FOCLA_DEV_INVAL;
		dev_data->last_frame_cb   = (int) SCDXIPCI_INVALID;
		dev_data->buff_save_cb    = (int) SCDXIPCI_INVALID;
		dev_data->roi_acc_buff_cb = (int) SCDXIPCI_INVALID;
		edf_str_lock_init(dev_data);
		dev_data->edf_usr_str = NULL;
		bsd = &dev_data->buff_save_data;
		bsd->dev_data = dev_data;
		last_frame_init(&dev_data->last_frame);
		last_frame_init(&bsd->last_frame);
		buffer_save_lock_init(dev_data);
#ifdef WITH_IMAGE
		image_data_init(&dev_data->live_img);
		image_data_init(&dev_data->roi_acc_buff.img_data);
#endif // WITH_IMAGE
		img_roi_reset(&dev_data->roi_acc_buff.roi);
	}
	app_data->active_dev = (int) SCDXIPCI_INVALID;
	
	app_data->saved_argv = (char **) calloc( argc+1, sizeof(char *) );
	if (app_data->saved_argv == NULL) {
		context_err(context, "Error alloc. argv copy table (%d*%d "
			    "bytes)\n", argc + 1, sizeof(char *));
		goto out;
	}

	app_data->saved_argc = argc;
	for( i=0; i<argc; i++ ) {
		app_data->saved_argv[i] = strdup(argv[i]);
		if (app_data->saved_argv[i] == NULL) {
			context_err(context, "Error alloc. argv %d copy (%d "
				    "bytes)\n", i, strlen(argv[i]));
			goto out;
		}
	}
	app_data->saved_argv[argc] = NULL;

#ifdef WITH_IMAGE
	if (image_init(app_data->saved_argc, app_data->saved_argv) < 0) {
		ret = ESPIA_TEST_ERR_IMGINIT;
		goto out;
	}
	app_data->image_init_flag = 1;
#endif // WITH_IMAGE

	init_context_poll(context, test_poll_funct, test_poll_funct_len, 
			  ESPIA_TEST_POLL_TIME);
	ret = ESPIA_OK;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-')
			continue;

		devnr.i = atoi(argv[i]);
		ret = open_dev(context, &devnr);
		if (ret < 0) {
			context_err(context, "Error opening device %d: %s\n",
				    devnr.i, espia_test_strerror(ret));
			goto out;
		}
	}


 out:
	if ((ret < 0) && (app_data != NULL))
		test_cleanup(context);

	return ret;
}

int test_cleanup(app_context_t context)
{
	struct app_data *app_data = app_data_from_context(context);
	int i, ret, app_ret = 0;
	union param devnr;


	for (i = 0; i < ESPIA_TEST_MAX_NR_DEV; i++) {
		if (app_data->device[i].dev == ESPIA_DEV_INVAL) 
			continue;

		devnr.i = i;

#define cleanup(funct, what)						  \
		ret = funct(context, &devnr);				  \
		if (ret < 0) {						  \
			context_err(context, "Error " what " device %d: " \
				    "%s\n", i, espia_test_strerror(ret)); \
			app_ret = -1;					  \
		}

		cleanup(select_dev, "selecting");
		cleanup(close_dev,  "closing");

#undef cleanup
	}

#ifdef WITH_IMAGE
	if( app_data->image_init_flag )
		image_exit();
#endif // WITH_IMAGE

	if( app_data->saved_argc && app_data->saved_argv ) {
		for( i=0; i<app_data->saved_argc; i++ )
			if ( app_data->saved_argv[i] )
				free( app_data->saved_argv[i] );
		free( app_data->saved_argv );
	}

	free(app_data);
	
	cleanup_context(context);

	return app_ret;
}


/*--------------------------------------------------------------------------
 * gives the error associated to a return value, NULL means no error
 *--------------------------------------------------------------------------*/

char *test_error_funct(app_context_t context, struct funct_data *fdata,
		       int ret)
{
	return (ret >= ESPIA_OK) ? NULL : espia_test_strerror(ret);
}


/*--------------------------------------------------------------------------
 * espia_test str error
 *--------------------------------------------------------------------------*/

static deb_const_str error_str_array[] = {
	{ESPIA_TEST_ERR_OPEN,	"Device already open"},
	{ESPIA_TEST_ERR_NOOPEN,	"Device not open"},
	{ESPIA_TEST_ERR_DEVNR,	"Device number REALLY too bad"},
	{ESPIA_TEST_ERR_SELECT,	"No device currently selected"},
	{ESPIA_TEST_ERR_NOMEM,	"Error allocating memory"},
	{ESPIA_TEST_ERR_CBNR,	"Invalid callback nr"},
	{ESPIA_TEST_ERR_CBTYPE,	"Invalid callback type"},
	{ESPIA_TEST_ERR_CBREG,	"Only one callback of that type allowed"},
	{ESPIA_TEST_ERR_IMGINIT,"Error initializing image module"},
	{ESPIA_TEST_ERR_IMGSIZE,"Image width/height/len doesn't fit buffer"},
	{ESPIA_TEST_ERR_IMGCRT ,"Error creating image"},
	{ESPIA_TEST_ERR_IMGEXST,"Buffer image already exists"},
	{ESPIA_TEST_ERR_IMGMISS,"Buffer image does not exist"},
	{ESPIA_TEST_ERR_PARAM,  "Invalid parameter name"},
	{ESPIA_TEST_ERR_FRMOP,  "Invalid frame operation name"},
	{ESPIA_TEST_ERR_CBFUNCT,"Invalid CB function"},
	{ESPIA_TEST_ERR_NOBUFFER,"No buffer allocated yet"},
	{ESPIA_TEST_ERR_ROIACC, "Image can only be created with RoiAccBuffer"},
	{ESPIA_TEST_ERR_DUMPPAR,"Invalid buffer offset or nr_bytes"},
	{ESPIA_TEST_ERR_OPTION, "Invalid option name"},
	{ESPIA_TEST_ERR_MAPINIT,"Serial map not yet initialized"},
	{ESPIA_TEST_ERR_MAPSIZE,"Serial map size exceeded"},
	{ESPIA_TEST_ERR_BUFFSAVE,"Buffer save data is invalid"},
	{ESPIA_TEST_ERR_FOCLASIG,"Invalid FOCLA signal"},
	{ESPIA_TEST_ERR_EDFFORM,"User string doesn't match EDF header format"},
	{ESPIA_TEST_ERR_INTERNAL,"Internal error"},
	{ESPIA_TEST_ERR_MAXFRMOP,"Maximum number of frame post-op reached"},
};

INIT_STATIC_CONST_STR_ARRAY_PREFIX(error_strings, error_str_array, 
				   "ESPIA_TEST");

char *espia_test_strerror(int ret)
{
	return GET_CONST_STR(error_strings, ret, focla_strerror(ret));
}


/*--------------------------------------------------------------------------
 * open / close / select
 *--------------------------------------------------------------------------*/

int open_dev(app_context_t context, union param *pars)
{
	struct app_data *app_data = app_data_from_context(context);
	int ret, dev_nr = pars[0].i;
	struct device_data *dev_data = &app_data->device[dev_nr];

	if (!is_good_dev_nr(dev_nr))
		return ESPIA_TEST_ERR_DEVNR;
	if (dev_data->dev != ESPIA_DEV_INVAL)
		return ESPIA_TEST_ERR_OPEN;

	ret = espia_open(dev_nr, &dev_data->dev);
	if (ret < 0)
		return ret;

	dev_data->dev_nr = dev_nr;
	app_data->active_dev = dev_nr;
	return ESPIA_OK;
}


int close_dev(app_context_t context, union param *pars)
{
	int ret;
	union param edf_usr_str;
	struct app_data *app_data = app_data_from_context(context);
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;
	
#define cleanup(funct, what)						\
	ret = funct(context, pars);					\
	if (ret < 0) {							\
		context_err(context, "Error " what " device %d: %s\n",	\
			    dev_data->dev_nr, espia_test_strerror(ret)); \
		return ret;						\
	}

	cleanup(stop_acq,          "stopping acq of");
	cleanup(unreg_all_cb,      "unregistering cbs of");
	cleanup(buffer_free,       "freeing buffer of");
	cleanup(serial_mmap_close, "closing serial files of");
	cleanup(focla_close_dev,   "closing FOCLA dev of");

#undef cleanup

	edf_usr_str.s[0] = 0;
	edf_usr_str.len = 0;
	edf_usr_header(context, &edf_usr_str);

	ret = espia_close(dev_data->dev);
	if (ret < 0)
		return ret;
	dev_data->dev = ESPIA_DEV_INVAL;
	app_data->active_dev = (int) SCDXIPCI_INVALID;
	return ESPIA_OK;
}


int select_dev(app_context_t context, union param *pars)
{
	struct app_data *app_data = app_data_from_context(context);
	int dev_nr = pars[0].i;
	struct device_data *dev_data = &app_data->device[dev_nr];

	if (!is_good_dev_nr(dev_nr))
		return ESPIA_TEST_ERR_DEVNR;
	if (dev_data->dev == ESPIA_DEV_INVAL)
		return ESPIA_TEST_ERR_NOOPEN;

	app_data->active_dev = dev_nr;
	return ESPIA_OK;
}


int dev_list(app_context_t context, union param *pars)
{
	struct app_data *app_data = app_data_from_context(context);
	struct device_data *dev_data;
	char *sel;
	int i, open = 0;

	dev_data = app_data->device;
	for (i = 0; i < ESPIA_TEST_MAX_NR_DEV; i++, dev_data++) {
		if (dev_data->dev != ESPIA_DEV_INVAL) {
			sel = (i == app_data->active_dev) ? " (selected)" : "";
			context_out(context, "Device #%d is open%s\n", i, sel);
			open++;
		}
	}
	if (open == 0)
		context_out(context, "No device open\n");
	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * driver option
 *--------------------------------------------------------------------------*/

int option_list(app_context_t context, union param *pars)
{
	struct espia_option *eoption;
	int i, nr_option, ret;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;
	
	nr_option = -1;
	ret = espia_get_option_data(dev_data->dev, &nr_option, NULL);
	if (ret < 0)
		return ret;

	context_out(context, "  Name                       Code \n");
	context_out(context, "----------------------------------\n");

	for (i = 0; i < nr_option; i++) {
		ret = espia_get_option_data(dev_data->dev, &i, &eoption);
		if (ret < 0)
			return ret;
		context_out(context, " %-27s ",  eoption->name);
		if (strlen(eoption->name) > 27)
			context_out(context, "\n %27s ", "");
		context_out(context, "%2d\n", eoption->option);
	}

	return ESPIA_OK;
}


int get_option(app_context_t context, union param *pars)
{
	struct espia_option *eoption;
	int ret, i, first, end, nr_option, option, action, val;
	char *name;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	nr_option = -1;
	ret = espia_get_option_data(dev_data->dev, &nr_option, NULL);
	if (ret < 0)
		return ret;

	name = get_param_str(&pars[0]);
	if (strcasecmp(name, "ALL") == 0) {
		first = 0;
		end = nr_option;
	} else {
		for (first = 0; first < nr_option; first++) {
			ret = espia_get_option_data(dev_data->dev, &first, 
						    &eoption);
			if (ret < 0)
				return ret;
			if (strcasecmp(name, eoption->name) == 0)
				break;
		}
		if (first == nr_option)
			return ESPIA_TEST_ERR_OPTION;
		end = first + 1;
	}

	context_out(context, "  Name                       Value     \n");
	context_out(context, "---------------------------------------\n");

	action = SCDXIPCI_OPT_RD;
	for (i = first; i < end; i++) {
		ret = espia_get_option_data(dev_data->dev, &i, &eoption);
		if (ret < 0)
			return ret;
		option = eoption->option;
		
		ret = espia_option(dev_data->dev, option, action, &val);
		if (ret < 0)
			return ret;
		context_out(context, " %-27s ",  eoption->name);
		if (strlen(eoption->name) > 27)
			context_out(context, "\n %27s ", "");
		context_out(context, "%d (0x%x)\n", val, val);
	}

	return ret;
}


int set_option(app_context_t context, union param *pars)
{
	struct espia_option *eoption;
	int ret, i, nr_option, option, action, val;
	char *name;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	nr_option = -1;
	ret = espia_get_option_data(dev_data->dev, &nr_option, NULL);
	if (ret < 0)
		return ret;

	name = get_param_str(&pars[0]);
	action = SCDXIPCI_OPT_RD_WR;
	for (i = 0; i < nr_option; i++) {
		ret = espia_get_option_data(dev_data->dev, &i, &eoption);
		if (ret < 0)
			return ret;
		if (strcasecmp(name, eoption->name) == 0)
			break;
	}
	if (i == nr_option)
		return ESPIA_TEST_ERR_OPTION;

	context_out(context, "  Name                     Prev. Value \n");
	context_out(context, "---------------------------------------\n");
	
	val = pars[1].i;
	option = eoption->option;
	ret = espia_option(dev_data->dev, option, action, &val);
	if (ret < 0)
		return ret;
	context_out(context, " %-27s ",  eoption->name);
	if (strlen(eoption->name) > 27)
		context_out(context, "\n %27s ", "");
	context_out(context, "%d (0x%x)\n", val, val);

	return ret;
}


/*--------------------------------------------------------------------------
 * debug level
 *--------------------------------------------------------------------------*/

int set_debug_level(app_context_t context, union param *pars)
{
	int ret;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;
	ret = espia_debug_level(dev_data->dev, &pars[0].i, pars[1].i);
	if (ret == ESPIA_OK)
		context_out(context, "Previous debug level: %d\n", pars[0].i);
	return ret;
}


/*--------------------------------------------------------------------------
 * read/write register
 *--------------------------------------------------------------------------*/

int read_register(app_context_t context, union param *pars)
{
	int ret;
	unsigned int val;
	char binbuf[32 + 7 + 1];
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	ret = espia_read_register(dev_data->dev, pars[0].i, &val);
	if (ret == ESPIA_OK)
		context_out(context, "Register 0x%02x value: %010u [%s]\n", 
			    pars[0].i, val, bin_repr(binbuf, val));

	return ret;
}

int write_register(app_context_t context, union param *pars)
{
	int ret;
	unsigned int val = pars[1].i;
	char binbuf[32 + 7 + 1];
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	ret = espia_write_register(dev_data->dev, pars[0].i, &val,
				      pars[2].i);
	if (ret == ESPIA_OK)
		context_out(context, "Prev. register 0x%02x value: %010u "
			    "[%s]\n", pars[0].i, val, bin_repr(binbuf, val));

	return ret;
}


/*--------------------------------------------------------------------------
 * param list 
 *--------------------------------------------------------------------------*/

int param_list(app_context_t context, union param *pars)
{
	struct espia_param *eparam;
	int i, nr_param, ret;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;
	
	nr_param = -1;
	ret = espia_get_param_data(dev_data->dev, &nr_param, NULL);
	if (ret < 0)
		return ret;

	context_out(context, "  Name                       Reg   Mask   Shift  "
		    "Description            \n");
	context_out(context, "-------------------------------------------------"
		    "-----------------------\n");

	for (i = 0; i < nr_param; i++) {
		ret = espia_get_param_data(dev_data->dev, &i, &eparam);
		if (ret < 0)
			return ret;
		context_out(context, " %-27s ",  eparam->name);
		if (strlen(eparam->name) > 27)
			context_out(context, "\n %27s ", "");
		context_out(context, "%2d 0x%08x %2d  %s\n", eparam->reg, 
			    eparam->mask, eparam->shift, eparam->desc);
	}

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * get/set parameter
 *--------------------------------------------------------------------------*/

int get_param(app_context_t context, union param *pars)
{
	char *param;
	int i, first, end, len, *param_arr, nr_param, ret;
	unsigned int *val_arr;
	struct espia_param *eparam;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	nr_param = -1;
	ret = espia_get_param_data(dev_data->dev, &nr_param, NULL);
	if (ret < 0)
		return ret;

	param = get_param_str(&pars[0]);
	if (strcasecmp(param, "ALL") == 0) {
		first = 0;
		end = nr_param;
	} else {
		for (first = 0; first < nr_param; first++) {
			ret = espia_get_param_data(dev_data->dev, &first, 
						   &eparam);
			if (ret < 0)
				return ret;
			if (strcasecmp(param, eparam->name) == 0)
				break;
		}
		if (first == nr_param)
			return ESPIA_TEST_ERR_PARAM;
		end = first + 1;
	}
	len = end - first;

	val_arr = NULL;

	param_arr = (int *) calloc(len, sizeof(*param_arr));
	if (param_arr == NULL) {
		ret = ESPIA_TEST_ERR_NOMEM;
		goto out;
	}
	val_arr = (unsigned int *) calloc(len, sizeof(*val_arr));
	if (val_arr == NULL) {
		ret = ESPIA_TEST_ERR_NOMEM;
		goto out;
	}

	for (i = 0; i < len; i++)
		param_arr[i] = first + i;
	ret = espia_get_param(dev_data->dev, param_arr, val_arr, len);
	if (ret < 0)
		goto out;

	context_out(context, " Name                                       "
		    "Value   \n");
	context_out(context, "--------------------------------------------"
		    "--------\n");
	for (i = 0; i < len; i++) {
		ret = espia_get_param_data(dev_data->dev, &param_arr[i], 
					   &eparam);
		if (ret < 0)
			goto out;
		context_out(context, " %-40s  %u (0x%x)\n", eparam->name,
			    val_arr[i], val_arr[i]);
	}	
 out:
	if (param_arr)
		free(param_arr);
	if (val_arr)
		free(val_arr);

	return ret;
}

int set_param(app_context_t context, union param *pars)
{
	char *param;
	int idx, nr_param, ret;
	unsigned int val;
	struct espia_param *eparam = NULL;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	nr_param = -1;
	ret = espia_get_param_data(dev_data->dev, &nr_param, NULL);
	if (ret < 0)
		return ret;

	param = get_param_str(&pars[0]);
	for (idx = 0; idx < nr_param; idx++) {
		ret = espia_get_param_data(dev_data->dev, &idx, &eparam);
		if (ret < 0)
			return ret;
		if (strcasecmp(param, eparam->name) == 0)
			break;
	}
	if (idx == nr_param)
		return ESPIA_TEST_ERR_PARAM;

	val = pars[1].i;
	ret = espia_set_param(dev_data->dev, &idx, &val, 1);
	if (ret >= 0)
		context_out(context, "Previous %s value: %u (0x%x)\n",
			    eparam->name, val, val);
	return ret;
}


/*--------------------------------------------------------------------------
 * reset link
 *--------------------------------------------------------------------------*/

int reset_link(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;
	return espia_reset_link(dev_data->dev);
}


/*--------------------------------------------------------------------------
 * serial helpers
 *--------------------------------------------------------------------------*/

char *get_char_repr(char ch) 
{
	static char c[2] = {0, 0};
	switch ((c[0] = ch)) {
	case '\n': return "\\n";
	case '\r': return "\\r";
	case '\t': return "\\t";
	case '"':  return "\\""\"";
	default:   return c;
	}
}

void print_serial_str(app_context_t context, char *buffer, 
		      int nr_bytes, int raw)
{
	context_out(context, "Received %d bytes%c\n", nr_bytes,
		(nr_bytes > 0) ? ':' : '.');
	if (raw)
		dump_data(context, buffer, nr_bytes);
	else
		context_out(context, "%s", buffer);
}

int alloc_serial_buffer(app_context_t context, int nr_bytes, char **buffer_ptr)
{
	*buffer_ptr = NULL;
	if (nr_bytes > 0) {
		*buffer_ptr = calloc(nr_bytes, 1);
		if (*buffer_ptr == NULL) {
			context_err(context, "Error cannot allocate %ld "
				    "bytes!\n", nr_bytes);
			return ESPIA_TEST_ERR_NOMEM;
		}
	}

	return ESPIA_OK;
}

int print_free_serial_buffer(app_context_t context, char *buffer, int nr_bytes,
			     int raw, int ret)
{
	if (!buffer) {
		if (ret == ESPIA_OK)
			context_out(context, "Available %ld byte(s).\n", 
				    nr_bytes);
		return ret;
	}

	if (ret == ESPIA_OK)
		print_serial_str(context, buffer, nr_bytes, raw);

	free(buffer);

	return ret;
}


/*--------------------------------------------------------------------------
 * serial read/write/flush
 *--------------------------------------------------------------------------*/

int serial_read(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	unsigned long nr_bytes = pars[0].i;
	int ret, raw = pars[2].i;
	char *buffer;

	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	ret = alloc_serial_buffer(context, nr_bytes, &buffer);
	if (ret < 0)
		return ret;

	ret = espia_ser_read(dev_data->dev, buffer, &nr_bytes, pars[1].i);
	return print_free_serial_buffer(context, buffer, nr_bytes, raw, ret);
}

int serial_read_str(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	unsigned long nr_bytes = pars[0].i;
	int ret, raw = pars[3].i;
	char *buffer;

	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	ret = alloc_serial_buffer(context, nr_bytes, &buffer);
	if (ret < 0)
		return ret;

	ret = espia_ser_read_str(dev_data->dev, buffer, &nr_bytes,
				 pars[1].s, pars[1].len, pars[2].i);
	return print_free_serial_buffer(context, buffer, nr_bytes, raw, ret);
}

int serial_write(app_context_t context, union param *pars)
{
	int ret;
	struct device_data *dev_data = dev_data_from_context(context);
	unsigned long nr_bytes = pars[0].len;
	char *action;

	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;
	ret = espia_ser_write(dev_data->dev, pars[0].s, &nr_bytes, pars[1].i, 
			      pars[2].i, pars[3].i);
	if (ret == ESPIA_OK) {
		action = (pars[0].len > 0) ? "Transmited" : "Still to TX:";
		context_out(context, "%s %ld bytes.\n", action, nr_bytes);
	}

	return ret;
}

int serial_flush(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;
	return espia_ser_flush(dev_data->dev);
}


/*--------------------------------------------------------------------------
 * serial read/write
 *--------------------------------------------------------------------------*/

// --- arg#0 = max bytes to read or write
int serial_mmap_init(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	struct serial_mmap_data *mmap_data;
	char filename[256];
	char *buffer;

	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	mmap_data= &dev_data->serial_mmap;
	mmap_data->init= 0;
	mmap_data->max_bytes= pars[0].i;
	mmap_data->mem_size= mmap_data->max_bytes + sizeof(int);
	buffer= calloc(mmap_data->mem_size, 1);

	sprintf(filename, ESPIA_TEST_SERIAL_FILENAME, dev_data->dev_nr, "write");
	mmap_data->write_file= open(filename, O_RDWR|O_TRUNC|O_CREAT, \
					S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);

	if (mmap_data->write_file > -1) {
		write(mmap_data->write_file, buffer, mmap_data->mem_size);
		fsync(mmap_data->write_file);
            	lseek(mmap_data->write_file, 0, SEEK_SET);
		mmap_data->write_mmap= mmap(NULL, mmap_data->mem_size, \
					PROT_WRITE|PROT_READ,MAP_SHARED, \
					mmap_data->write_file, 0);
		context_out(context, "%s created.\n", filename);
	}
	else {
		context_out(context, "Failed to create file %s.\n", filename);
		return ESPIA_TEST_ERR_MAPINIT;
	}

	sprintf(filename, ESPIA_TEST_SERIAL_FILENAME, dev_data->dev_nr, "read");
	mmap_data->read_file= open(filename, O_RDWR|O_TRUNC|O_CREAT, \
					S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);

	if (mmap_data->read_file > -1) {
		write(mmap_data->read_file, buffer, mmap_data->mem_size);
		fsync(mmap_data->read_file);
		lseek(mmap_data->write_file, 0, SEEK_SET);
		mmap_data->read_mmap= mmap(NULL, mmap_data->mem_size, \
					PROT_WRITE|PROT_READ,MAP_SHARED, \
					mmap_data->read_file, 0);
		context_out(context, "%s created.\n", filename);
	}
	else {
		munmap(mmap_data->write_mmap, mmap_data->mem_size);
		close(mmap_data->write_file);

		context_out(context, "Failed to create file %s\n", filename);
		return ESPIA_TEST_ERR_MAPINIT;
	}

	mmap_data->init= 1;
	free(buffer);
	return ESPIA_OK;
}

int serial_mmap_close(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	struct serial_mmap_data *mmap_data;
	char filename[256];

	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	mmap_data= &dev_data->serial_mmap;
	if (mmap_data->init) {
		munmap(mmap_data->read_mmap, mmap_data->mem_size);
		close(mmap_data->read_file);
		sprintf(filename, ESPIA_TEST_SERIAL_FILENAME, dev_data->dev_nr, "read");
		unlink(filename);
		context_out(context, "%s removed.\n", filename);
		munmap(mmap_data->write_mmap, mmap_data->mem_size);
		close(mmap_data->write_file);
		sprintf(filename, ESPIA_TEST_SERIAL_FILENAME, dev_data->dev_nr, "write");
		unlink(filename);
		context_out(context, "%s removed.\n", filename);
	}
	mmap_data->init= 0;
	return ESPIA_OK;
}

// --- Arguments
// $0 = block_size = Nr. of bytes before a delay
// $1 = delay = Time between writes, in us
// $2 = block = Wait until end of TX [0=No, 1=Yes]
// #3 = print out serial string written (debug)
int serial_mmap_write(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	struct serial_mmap_data *mmap_data;
	int ret, rlen;
	unsigned long nr_bytes;
	char *buffer;

	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	mmap_data= &dev_data->serial_mmap;
	if (!mmap_data->init)
		return ESPIA_TEST_ERR_MAPINIT;

	memcpy(&rlen, mmap_data->write_mmap, sizeof(int));
	nr_bytes= (unsigned long)rlen;

	if (rlen > mmap_data->max_bytes)
		return ESPIA_TEST_ERR_MAPSIZE;

	buffer= calloc(rlen, 1);
	memcpy(buffer, mmap_data->write_mmap+sizeof(int), rlen);

	if (pars[3].i)
		print_serial_str(context, buffer, nr_bytes, 1);

	ret = espia_ser_write(dev_data->dev, buffer, &nr_bytes, pars[0].i, 
			      pars[1].i, pars[2].i);
	if (ret == ESPIA_OK) {
		context_out(context, "Transmited %ld bytes.\n", nr_bytes);
	}

	free(buffer);
	return ret;
}

// --- Arguments :
// $0 = nr_bytes = Number of bytes to read
// $1 = timeout = Timeout in us [0=Noblock, -1=Forever]
// $2 = print out read raw string 
int serial_mmap_read(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	struct serial_mmap_data *mmap_data;
	unsigned long nr_bytes = pars[0].i;
	int ret, output = pars[2].i;
	char *buffer;

	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	mmap_data= &dev_data->serial_mmap;
	if (!mmap_data->init)
		return ESPIA_TEST_ERR_MAPINIT;

	if ((int) nr_bytes > mmap_data->max_bytes)
		return ESPIA_TEST_ERR_MAPSIZE;

	buffer = NULL;
	if (nr_bytes > 0) {
		buffer = calloc(nr_bytes, 1);
		if (buffer == NULL) {
			context_err(context, "Error cannot allocate %ld "
				    "bytes!\n", nr_bytes);
			return ESPIA_TEST_ERR_NOMEM;
		}
	}

	ret = espia_ser_read(dev_data->dev, buffer, &nr_bytes, pars[1].i);
	if (ret == ESPIA_OK) {
		context_out(context, "Available %ld byte(s).\n", 
				    nr_bytes);

		memcpy(mmap_data->read_mmap, &nr_bytes, sizeof(int));
		memcpy(mmap_data->read_mmap+sizeof(int), buffer, nr_bytes);
		msync(mmap_data->read_mmap, nr_bytes+sizeof(int), MS_ASYNC);

		if (output)
			print_serial_str(context, buffer, nr_bytes, 1);
	}

	free(buffer);
	return ret;
}

/*--------------------------------------------------------------------------
 * buffer post-operation
 *--------------------------------------------------------------------------*/

int frame_post_op_list(app_context_t context, union param *pars)
{
	struct espia_frm_op_fn_set *fn_set;
	int ret, nr_op, op;

	nr_op = -1;
	ret = espia_get_frm_op_fn_set(&nr_op, NULL);
	if (ret < 0)
		return ret;

	context_out(context, 
		    "  Name                 Description                   "
		    "            \n"
		    "-----------------------------------------------------"
		    "------------\n");
	for (op = 0; op < nr_op; op++) {
		ret = espia_get_frm_op_fn_set(&op, &fn_set);
		if (ret < 0)
			return ret;

		context_out(context, " %-20s  %s\n", 
			    fn_set->name, fn_set->desc);
	}

	return ESPIA_OK;
}

int frame_post_op_from_name(app_context_t context, char *name,
			    struct espia_frm_op_fn_set **fn_set)
{
	int ret, nr_op, op;

	nr_op = -1;
	ret = espia_get_frm_op_fn_set(&nr_op, NULL);
	if (ret < 0)
		return ret;

	for (op = 0; op < nr_op; op++) {
		ret = espia_get_frm_op_fn_set(&op, fn_set);
		if (ret < 0)
			return ret;

		if (strcasecmp((*fn_set)->name, name) == 0)
			return ESPIA_OK;
	}

	return ESPIA_TEST_ERR_FRMOP;
}

int frame_post_op(app_context_t context, union param *pars)
{
	char *ptr, *term, *list;
	int nr_op, ret;
	struct espia_frm_op_fn_set *fn_set;
	struct espia_frm_op_info op_info[ESPIA_TEST_MAX_OPS];
	struct device_data *dev_data;

	dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	list = get_param_str(&pars[0]);
	if (list != NULL) {
		list = strdup(list);
		if (list == NULL)
			return ESPIA_TEST_ERR_NOMEM;
	}

	for (nr_op = 0, ptr = list; ptr && *ptr; nr_op++, ptr = term) {
		term = strchr(ptr, ',');
		if (term != NULL)
			*term++ = 0;
		ret = frame_post_op_from_name(context, ptr, &fn_set);
		if (ret < 0)
			goto out;
		if (nr_op == ESPIA_TEST_MAX_OPS) {
			ret = ESPIA_TEST_ERR_MAXFRMOP;
			goto out;
		}
		op_info[nr_op].fn_set = fn_set;
		op_info[nr_op].data = &dev_data->post_op_fn_data;
	}

	ret = espia_set_frm_op_fn(dev_data->dev, op_info, nr_op);
 out:
	if (list != NULL)
		free(list);

	return ret;
}


/*--------------------------------------------------------------------------
 * last frame cb funct/install/uninstall
 *--------------------------------------------------------------------------*/

int last_frame_cb(app_context_t context, struct device_data *dev_data,
		  struct cb_priv_data *priv_data,
		  struct espia_cb_data *cb_data)
{
	struct img_frame_info *finfo = &cb_data->info.acq.cb_finfo;

	if (!finished_espia_frame_info(finfo, cb_data->ret))
		last_frame_update(&dev_data->last_frame, finfo);

	return ESPIA_OK;
}

int last_frame_cb_install(app_context_t context)
{
	struct espia_cb_data cb_data;
	struct img_frame_info *finfo;
	struct cb_priv_data priv_data;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	init_cb_priv_data(&priv_data);

	cb_data.type        = ESPIA_CB_ACQ;
	finfo               = &cb_data.info.acq.req_finfo;
	finfo->buffer_nr    = ESPIA_ACQ_ANY;
	finfo->frame_nr     = ESPIA_ACQ_ANY;
	finfo->round_count  = ESPIA_ACQ_ANY;
	finfo->acq_frame_nr = ESPIA_ACQ_EACH;
	cb_data.timeout     = SCDXIPCI_BLOCK_FOREVER;
	priv_data.cb_funct  = last_frame_cb;

	return reg_gen_cb(context, &cb_data, &priv_data, 
			  ETEST_CB_INTERN_ACTIVE, &dev_data->last_frame_cb);
}


/*--------------------------------------------------------------------------
 * buffer alloc/free/list
 *--------------------------------------------------------------------------*/

int buffer_alloc(app_context_t context, union param *pars)
{
	int ret, display, page_size_1, virt_buffers, virt_frames, frame_factor;
	int real_buffers, real_frames, real_frame_size, real_buffer_size; 
#ifdef WITH_IMAGE
	struct image_data *img_data;
#endif // WITH_IMAGE
	struct espia_frame_dim *fdim;
	op_fn_data *op_data;
	struct buffer_data *bdata;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	virt_buffers   = pars[0].i;
	virt_frames    = pars[1].i;

	fdim = &dev_data->frame_dim;
	fdim->width    = pars[2].i;
	fdim->height   = pars[3].i;
	fdim->depth    = pars[4].i;

	real_buffers    = virt_buffers;
	real_frames     = virt_frames;
	real_frame_size = espia_frame_mem_size(fdim);
	if (virt_frames == 1) {
		page_size_1 = getpagesize() - 1;
		real_frame_size += page_size_1;
		real_frame_size &= ~page_size_1;
	}

	real_buffer_size = real_frame_size * real_frames;
	frame_factor = 1;
	if (real_buffer_size < ESPIA_TEST_MIN_BUFFER_SIZE) {
		frame_factor = ESPIA_TEST_MIN_BUFFER_SIZE / real_buffer_size;
		real_frames *= frame_factor;
		real_buffers += frame_factor - 1;
		virt_buffers = real_buffers;  /* The real virtual buffer number */
		real_buffers /= frame_factor;
	}

	ret = espia_buffer_alloc(dev_data->dev, real_buffers, 
				 real_frames, real_frame_size);
	if (ret != ESPIA_OK)
		return ret;

	bdata = (struct buffer_data *) calloc(virt_buffers, sizeof(*bdata));
	if (bdata == NULL) {
		espia_buffer_free(dev_data->dev);
		return ESPIA_TEST_ERR_NOMEM;
	}

	dev_data->buff_data = bdata;
	dev_data->nr_buffer = virt_buffers;
        dev_data->buffer_frames = virt_frames;
	dev_data->real_frame_factor = frame_factor;
	dev_data->real_frame_size = real_frame_size;

	op_data = &dev_data->post_op_fn_data;
	op_data->frame_dim = *fdim;

#ifdef WITH_IMAGE
	for (pars[0].i = 0; pars[0].i < virt_buffers; pars[0].i++) {
		img_data = image_data_from_buffer_nr(dev_data, pars[0].i);
		image_data_init(img_data);
	}
#endif // WITH_IMAGE

	ret = last_frame_cb_install(context);
	if (ret < 0)
		goto out;

	display = pars[5].i;
	if (!display)
		return ESPIA_OK;

#ifdef WITH_IMAGE
	for (pars[0].i = 0; pars[0].i < virt_buffers; pars[0].i++) {
		ret = buffer_img(context, pars);
		if (ret < 0)
			goto out;
	}
#else // WITH_IMAGE
	ret = ESPIA_TEST_ERR_IMGINIT;
#endif // WITH_IMAGE

 out:
	if (ret < 0)
		buffer_free(context, pars);

	return (ret < 0) ? ret : ESPIA_OK;
}


int buffer_free(app_context_t context, union param *pars)
{
	int ret, cb_nr;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	roi_acc_buffer_del(context);

#ifdef WITH_IMAGE
	ret = buffer_img_del_all(context, pars);
	if (ret != ESPIA_OK)
		return ret;
#endif // WITH_IMAGE

	cb_nr = dev_data->last_frame_cb;
	if (!scdxipci_is(INVALID, cb_nr))
		unreg_gen_cb(context, dev_data, cb_nr, ETEST_CB_INTERN, 
			     &dev_data->last_frame_cb);

	if (dev_data->buff_data != NULL) {
		free(dev_data->buff_data);
		dev_data->buff_data = NULL;
	}

	ret = espia_buffer_free(dev_data->dev);
	if (ret != ESPIA_OK)
		return ret;

	dev_data->nr_buffer = 0;

	return ESPIA_OK;
}

int buffer_list(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	struct espia_frame_dim *fdim;
	struct img_buffer_info binfo;
	struct buffer_data *bdata;
	int ret, nr, nr_frames;
	char dim[64], *img;

	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;
	else if (dev_data->nr_buffer == 0) {
		context_out(context, "No allocated buffer\n");
		return ESPIA_OK;
	}
		
	fdim = &dev_data->frame_dim;
	bdata = dev_data->buff_data;

	context_out(context, 
		    " Buff #        Addr         Frames    Frm. Dim      "
		    "Frm. Size     Tot Size   Image  \n"
		    "----------------------------------------------------"
		    "--------------------------------\n");

	sprintf(dim, "%ldx%ldx%ld", fdim->width, fdim->height, fdim->depth);

	for (nr = 0; nr < dev_data->nr_buffer; nr++, bdata++) {
		ret = virt_buffer_info(dev_data, nr, &binfo);
		if (ret < 0)
			return ret;
		nr_frames = binfo.nr_frames;
#ifdef WITH_IMAGE
		img = (bdata->img.img != IMAGE_INVALID) ? "Yes" : "No";
#else // WITH_IMAGE
		img = "---";
#endif // WITH_IMAGE
		context_out(context, "%5d    %14p  %7d    %11s  %10ld    "
			    "%10ld    %s\n", nr, binfo.ptr, nr_frames, dim, 
			    binfo.frame_size, nr_frames * binfo.frame_size,
			    img);
	}

	return ESPIA_OK;
}

int buffer_dump(app_context_t context, union param *pars)
{
	int ret, buffer_nr, offset, nr_bytes, buffer_len;
	char *ptr;
	struct img_buffer_info binfo;

	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	buffer_nr = pars[0].i;
	ret = virt_buffer_info(dev_data, buffer_nr, &binfo);
	if (ret < 0)
		return ret;
	buffer_len = binfo.nr_frames * binfo.frame_size;
	
	offset = pars[1].i;
	nr_bytes = pars[2].i;
	if ((offset < 0) || (nr_bytes <= 0) || 
            ((offset + nr_bytes) > buffer_len))
		return ESPIA_TEST_ERR_DUMPPAR;
	
	ptr = binfo.ptr + offset;
	context_out(context, "Buffer %d (%p) - Dump from %p:\n\n", 
		    buffer_nr, binfo.ptr, ptr);

	return dump_data(context, ptr, nr_bytes);
}


int buffer_save_edf_header(app_context_t context, FILE *fout, 
			   struct img_buffer_info *binfo, 
			   unsigned long frame_nr,
			   struct espia_frame_dim *fdim, 
			   struct img_frame_info *finfo,
			   struct device_data *dev_data
			   )
{
	time_t ctime_now;
	struct timeval tod_now;
	char time_str[64], buffer[EDF_HEADER_BUFFER_LEN], *p;
	int i, depth, width, height, len, rem;
	size_t ret;

	time(&ctime_now);
	gettimeofday(&tod_now, NULL);

	ctime_r(&ctime_now, time_str);
	time_str[strlen(time_str) - 1] = 0;

	depth  = fdim->depth;
	width  = fdim->width;
	height = fdim->height;

	p = buffer;
	p += sprintf(p, "{\n");
	p += sprintf(p, "HeaderID = EH:%06lu:000000:000000 ;\n", frame_nr);
	p += sprintf(p, "Image = %lu ;\n", frame_nr);
	p += sprintf(p, "ByteOrder = LowByteFirst ;\n");
	p += sprintf(p, "DataType = %s ;\n", 
		     (depth == 1) ? "UnsignedByte" :
		     ((depth == 2) ? "UnsignedShort" : "UnsignedLong"));
	p += sprintf(p, "Size = %ld ;\n", binfo->frame_size);
	p += sprintf(p, "Dim_1 = %d ;\n", width);
	p += sprintf(p, "Dim_2 = %d ;\n", height);

	p += sprintf(p, "time = %s ;\n", time_str);
	p += sprintf(p, "time_of_day = %ld.%06ld ;\n", 
		     tod_now.tv_sec, tod_now.tv_usec);
	if (finfo != NULL)
		p += sprintf(p, "time_of_frame = %.6f ;\n", 
			     finfo->time_us * 1e-6);
	edf_str_lock(dev_data);
	if( (NULL != dev_data->edf_usr_str) && (strlen(dev_data->edf_usr_str) 
	                      < (size_t)(buffer + EDF_HEADER_BUFFER_LEN - p)) )
		p += sprintf(p, "%s", dev_data->edf_usr_str);
	edf_str_unlock(dev_data);

	i = p - buffer;
	len = i;
	rem = len % EDF_HEADER_LEN;
	if (rem > 0)
		len += EDF_HEADER_LEN - rem;
	p += sprintf(p, "%*s}\n", len - (i + 2), "");
	len = p - buffer;

	ret = fwrite(buffer, len, 1, fout);
	if (ret != 1)
		return -ENOSPC;
	return ESPIA_OK;
}

int buffer_save_file_open(app_context_t context, struct buffer_save_data *bsd)
{
	char *fname, *prefix, *name_suffix, *suffix, *error;
	FILE *fout;
	int flags, fd, overwrite, idx, format, ret;
	struct device_data *dev_data = bsd->dev_data;

	ret = buffer_save_lock(dev_data);
	if (ret < 0)
		return -1;

	fout = bsd->fout;
	if (fout != NULL)
		goto unlock;

	prefix = bsd->prefix;
	name_suffix = bsd->suffix;
	idx = bsd->idx;
	format = bsd->format;
	overwrite = bsd->overwrite;

	switch (format) {
	case FMT_RAW:
		suffix = "raw"; break;
	case FMT_EDF:
		suffix = "edf"; break;
	default:
		ret = -EINVAL;
		goto unlock;
	}

	fname = bsd->file_name;
	sprintf(fname, "%s%04d%s.%s", prefix, idx, name_suffix, suffix);

	flags  = O_CREAT | O_WRONLY | O_LARGEFILE;
	flags |= overwrite ? O_TRUNC : O_APPEND;
	fd = open(fname, flags, FILE_SAVE_MODE);
	if (fd >= 0)
		fout = fdopen(fd, overwrite ? "wb" : "ab");
	if (fout == NULL) {
		error = strerror(errno);
		context_err(context, "Error opening %s: %s\n", fname, error);
		fname[0] = 0;
		ret = -1;
		goto unlock;
	}
	setbuf(fout, NULL);
	bsd->fout = fout;

 unlock:
	buffer_save_unlock(dev_data);

	return ret;
}

int buffer_save_file_close(app_context_t context, struct buffer_save_data *bsd)
{
	FILE *fout;
	int ret;
	char *error;
	struct device_data *dev_data = bsd->dev_data;

	ret = buffer_save_lock(dev_data);
	if (ret < 0)
		return -1;
	
	fout = bsd->fout;
	if (fout == NULL)
		goto unlock;

	ret = fclose(fout);
	if (ret != 0) {
		error = strerror(errno);
		context_err(context, "Error closing file %s: %s\n", 
			    bsd->file_name, error);
		ret = -1;
		goto unlock;
	}
	bsd->fout = NULL;
	bsd->file_name[0] = 0;
	bsd->idx++;

 unlock:
	buffer_save_unlock(dev_data);

	return ret;
}

int buffer_save_file(app_context_t context, struct buffer_save_data *bsd,
		     unsigned long buffer_nr, struct img_frame_info *finfo)
{
	struct device_data *dev_data = bsd->dev_data;
	char *fname, *ptr, *error, print_fname[FILE_FULL_NAME_LEN];
	FILE *fout;
	struct img_buffer_info binfo;
	struct espia_frame_dim *fdim;
	unsigned long frame, len, frame_nr;
	int ret, format;

	ret = buffer_save_lock(dev_data);
	if (ret < 0)
		return -1;

	ret = buffer_info_from_nr(dev_data, buffer_nr, &binfo);
	if (ret < 0)
		goto unlock;

	format = bsd->format;
	fname = bsd->file_name;

	fdim = frame_dim_from_buffer_nr(dev_data, buffer_nr);

	ptr = binfo.ptr;
	for (frame = 1; frame <= binfo.nr_frames; frame++) {
		fout = bsd->fout;
		if (fout == NULL) {
			ret = buffer_save_file_open(context, bsd);
			if (ret < 0)
				goto unlock;
			fout = bsd->fout;
		}
		frame_nr = bsd->file_frames + 1;

		if (bsd->print_name)
			strcpy(print_fname, fname);

		switch (format) {
		case FMT_EDF:
			ret = buffer_save_edf_header(context, fout, &binfo,
						     frame_nr, fdim, finfo,
						     dev_data);
			if (ret < 0)
				goto unlock;

		case FMT_RAW:
			len = binfo.frame_size;
			ret = fwrite(ptr, len, 1, fout);
			if (ret != 1) {
				error = strerror(errno);
				context_err(context, "Error writing frame %d "
					    "(%ld bytes) on %s: %s\n", 
					    frame_nr, len, fname, error);
				ret = -1;
				goto unlock;
			}
		}
		ptr += binfo.frame_size;

		bsd->file_frames++;
		bsd->file_frames %= bsd->tot_file_frames;
		if (bsd->file_frames == 0) {
			ret = buffer_save_file_close(context, bsd);
			if (ret < 0)
				goto unlock;
		}
	}

	if (bsd->print_name) 
		context_out(context, "Buffer #%ld of dev. %d saved on %s\n", 
			    buffer_nr, dev_data->dev_nr, print_fname);

 unlock:
	buffer_save_unlock(dev_data);
	
	return ret;
}

int buffer_save_cb(app_context_t context, struct device_data *dev_data,
		   struct cb_priv_data *priv_data,
		   struct espia_cb_data *cb_data)
{
	struct buffer_save_data *bsd = &dev_data->buff_save_data;
	struct img_frame_info *finfo = &cb_data->info.acq.cb_finfo;
	int ret;

	if (finished_espia_frame_info(finfo, cb_data->ret))
		return ESPIA_OK;

	ret = buffer_save_file(context, bsd, finfo->buffer_nr,
			       finfo);

	last_frame_update(&bsd->last_frame, finfo);
	return ret;
}

int buffer_save_cb_print(app_context_t context, struct buffer_save_data *bsd)
{
	struct device_data *dev_data = bsd->dev_data;
	int ret;

	ret = buffer_save_lock(dev_data);
	if (ret < 0)
		return ret;

	context_out(context, "Buffer auto-save status:\n");
	context_out(context, "  active=%d, prefix=\"%s\", idx=%d, format=%d\n", 
		    auto_save_active(dev_data), bsd->prefix, bsd->idx, 
		    bsd->format);
	context_out(context, "  file_name=\"%s\"\n", bsd->file_name);
	context_out(context, "  tot_file_frames=%d, "
		    "curr_file_frames=%d, overwrite=%d\n", 
		    bsd->tot_file_frames, bsd->file_frames, bsd->overwrite);

	buffer_save_unlock(dev_data);

	return ESPIA_OK;
}

int buffer_save_cb_install(app_context_t context, 
			   struct buffer_save_data *bsd)
{
	struct device_data *dev_data = bsd->dev_data;
	struct espia_cb_data cb_data;
	struct img_frame_info *finfo = &cb_data.info.acq.req_finfo;
	struct cb_priv_data priv_data;
	int prev_idx, ret;

	ret = buffer_save_lock(dev_data);
	if (ret < 0)
		return ret;

	ret = buffer_save_file_close(context, &dev_data->buff_save_data);
	if (ret < 0)
		goto unlock;

	prev_idx = dev_data->buff_save_data.idx;
	dev_data->buff_save_data = *bsd;
	if (dev_data->buff_save_data.idx < 0)
		dev_data->buff_save_data.idx = prev_idx;

	if (auto_save_active(dev_data))
		goto unlock;

	init_cb_priv_data(&priv_data);

	cb_data.type	    = ESPIA_CB_ACQ;
	finfo->buffer_nr    = ESPIA_ACQ_EACH;
	finfo->frame_nr	    = 0;
	finfo->round_count  = ESPIA_ACQ_ANY;
	finfo->acq_frame_nr = 0;
	cb_data.timeout	    = SCDXIPCI_BLOCK_FOREVER;
	priv_data.cb_funct  = buffer_save_cb;

	ret = reg_gen_cb(context, &cb_data, &priv_data, 
			 ETEST_CB_INTERN_ACTIVE, &dev_data->buff_save_cb);

 unlock:
	buffer_save_unlock(dev_data);
	return ret;
}

int buffer_save_cb_uninstall(app_context_t context)
{
	int cb_nr, ret;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	if (!auto_save_active(dev_data))
		return ESPIA_OK;	

	cb_nr = dev_data->buff_save_cb;
	ret = unreg_gen_cb(context, dev_data, cb_nr, ETEST_CB_INTERN, 
			    &dev_data->buff_save_cb);
	if (ret < 0)
		return ret;

	ret = buffer_save_lock(dev_data);
	if (ret < 0)
		return ret;

	ret = buffer_save_file_close(context, &dev_data->buff_save_data);
	if (ret < 0)
		goto unlock;

 unlock:
	buffer_save_unlock(dev_data);
	return ret;
}

int buffer_save(app_context_t context, union param *pars)
{
	struct buffer_save_data bsd;
	int buffer_nr, auto_save, ret = 0;
	char *fname;

	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	buffer_nr = pars[0].i;
	auto_save = buffer_is_live(buffer_nr);
	fname = get_param_str(&pars[1]);
	if (auto_save) {
		if (strlen(fname) == 0)
			return buffer_save_cb_uninstall(context);
		if (strcmp(fname, "?") == 0)
			return buffer_save_cb_print(context, 
						    &dev_data->buff_save_data);
	}

	bsd.dev_data = dev_data;
	strncpy(bsd.prefix, fname, FILE_PREFIX_NAME_LEN);
	bsd.idx = 0;
	bsd.suffix[0] = 0;
	bsd.format = 0;
	bsd.file_name[0] = 0;
	bsd.tot_file_frames = 1;
	bsd.overwrite = 0;
	bsd.print_name = !auto_save;
	bsd.fout = NULL;
	bsd.file_frames = 0;
	last_frame_init(&bsd.last_frame);

	if (chk_opt_param_present(&pars[2]))
		bsd.idx = pars[2].i;
	if (chk_opt_param_present(&pars[3]))
		bsd.format = pars[3].i;
	if (chk_opt_param_present(&pars[4]))
		bsd.tot_file_frames = pars[4].i;
	if (bsd.tot_file_frames <= 0)
		return -EINVAL;
	if (chk_opt_param_present(&pars[5]))
		bsd.overwrite = pars[5].i;

	if (auto_save)
		ret = buffer_save_cb_install(context, &bsd);
	else {
		struct img_frame_info finfo;

		finfo.buffer_nr    =  buffer_nr;
		finfo.frame_nr     =  0;  /* ??? */
		finfo.round_count  = -1;
		finfo.acq_frame_nr = -1;

		espia_get_frame(dev_data->dev, &finfo , 0);
		ret = buffer_save_file(context, &bsd, buffer_nr, &finfo
				       /*NULL*/);
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * buffer clear
 *--------------------------------------------------------------------------*/

int buffer_clear(app_context_t context, union param *pars)
{
	struct img_buffer_info binfo;
#ifdef WITH_IMAGE
	struct image_data *img_data;
#endif // WITH_IMAGE
	int buffer_nr, first, last, len, ret = 0;

	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	buffer_nr = pars[0].i;
	if (buffer_is_live(buffer_nr)) {
		first = 0;
		last = dev_data->nr_buffer - 1;
	} else {
		first = last = buffer_nr;
	}

	for (buffer_nr = first; buffer_nr <= last; buffer_nr++) {
		ret = buffer_info_from_nr(dev_data, buffer_nr, &binfo);
		if (ret < 0)
			break;

		len = binfo.nr_frames * binfo.frame_size;
		memset(binfo.ptr, 0, len);

#ifdef WITH_IMAGE
		get_image_data_check(img_data, dev_data, buffer_nr);
		if (img_data->img != IMAGE_INVALID)
			image_update(img_data->img);
#endif // WITH_IMAGE
	}

#ifdef WITH_IMAGE
	if (!buffer_is_roi_acc(first)) {
		get_image_data_check(img_data, dev_data, (int) SCDXIPCI_ANY);
		if (img_data->img != IMAGE_INVALID)
			image_update(img_data->img);
	}
#endif // WITH_IMAGE

	return ret;
}


#ifdef WITH_IMAGE

/*--------------------------------------------------------------------------
 * generic image 
 *--------------------------------------------------------------------------*/

int gen_img_cb(app_context_t context, struct device_data *dev_data,
	       struct cb_priv_data *priv_data,
	       struct espia_cb_data *cb_data)
{
	struct image_data *img_data = priv_data->img_data;
	struct img_frame_info *finfo = &cb_data->info.acq.cb_finfo;

	if (finished_espia_frame_info(finfo, cb_data->ret))
		return ESPIA_OK;

	if (img_data->cb_funct == NULL) {
		image_update(img_data->img);
		return ESPIA_OK;
	}

	return img_data->cb_funct(context, dev_data, img_data, cb_data);
}

void gen_img_destroy(void *data)
{
	struct image_data *img_data = (struct image_data *) data;
	app_context_t context = img_data->context;
	struct device_data *dev_data = img_data->dev_data;

	unreg_gen_cb(context, dev_data, img_data->cb_nr, ETEST_CB_INTERN,
		     &img_data->cb_nr);
	if (img_data->del_funct != NULL)
		img_data->del_funct(context, dev_data, img_data);
	img_data->img = IMAGE_INVALID;
}


int gen_img_add(app_context_t context, int buffer_nr, img_cb *cb_funct, 
		struct img_frame_info *req_finfo, img_del *del_funct)
{
	int ret;
	struct image_data *img_data;
	struct espia_cb_data cb_data;
	struct cb_priv_data priv_data;
	struct img_frame_info *cb_req_finfo = &cb_data.info.acq.req_finfo;
	struct img_buffer_info binfo;
	struct espia_frame_dim *fdim;
	char caption[BUFFER_IMG_NAME_LEN];

	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	get_image_data_check(img_data, dev_data, buffer_nr);
	if (img_data->img != IMAGE_INVALID)
		return ESPIA_TEST_ERR_IMGEXST;

	img_data->context   = context;
	img_data->dev_data  = dev_data;
	img_data->del_funct = del_funct;
	img_data->cb_funct  = cb_funct;

	buffer_img_name(caption, dev_data, buffer_nr);
	ret = image_create(&img_data->img, caption);
	if (ret != 0)
		return ESPIA_TEST_ERR_IMGCRT;

	ret = image_close_cb(img_data->img, gen_img_destroy, img_data);
	if (ret != 0) {
		ret = ESPIA_TEST_ERR_IMGCRT;
		goto close_cb_error;
	}

	if (!buffer_is_live(buffer_nr)) {
		ret = buffer_info_from_nr(dev_data, buffer_nr, &binfo);
		if (ret < 0)
			goto binfo_error;
		fdim = frame_dim_from_buffer_nr(dev_data, buffer_nr);
		img_data->buffer_ptr = binfo.ptr;
		image_set_buffer(img_data->img, binfo.ptr, fdim->width, 
				 fdim->height * binfo.nr_frames, fdim->depth);
	} else
		img_data->buffer_ptr = NULL;

	init_cb_priv_data(&priv_data);

	cb_data.type		= ESPIA_CB_ACQ;
	*cb_req_finfo		= *req_finfo;
	cb_data.timeout		= SCDXIPCI_BLOCK_FOREVER;
	priv_data.cb_funct	= gen_img_cb;
	priv_data.img_data	= img_data;

	ret = reg_gen_cb(context, &cb_data, &priv_data, ETEST_CB_INTERN_ACTIVE,
			 &img_data->cb_nr);
	if (ret < 0)
		goto reg_cb_error;
	
	return ESPIA_OK;

 reg_cb_error:
 binfo_error:
	image_close_cb(img_data->img, NULL, NULL);
 close_cb_error:
	image_destroy(img_data->img);
	img_data->img = IMAGE_INVALID;
	return ret;
}

int gen_img_del(app_context_t context, int buffer_nr)
{
	struct image_data *img_data;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	get_image_data_check(img_data, dev_data, buffer_nr);
	if (img_data->img == IMAGE_INVALID)
		return ESPIA_TEST_ERR_IMGMISS;
	
	image_destroy(img_data->img);

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * buffer image alloc/update/destroy/del_all
 *--------------------------------------------------------------------------*/

int buffer_img_cb(app_context_t context, struct device_data *dev_data,
		  struct image_data *img_data, struct espia_cb_data *cb_data)
{
	struct img_frame_info *finfo = &cb_data->info.acq.cb_finfo;
	struct espia_frame_dim *fdim = &dev_data->frame_dim;
	unsigned long width, height, depth;
	void *buffer_ptr = finfo->buffer_ptr;

	if (buffer_ptr != img_data->buffer_ptr) {
		width  = fdim->width;
		height = fdim->height * dev_data->buffer_frames;
		depth  = fdim->depth;
		image_set_buffer(img_data->img, buffer_ptr, width, height, 
				 depth);
		img_data->buffer_ptr = buffer_ptr;
	} else {
		image_update(img_data->img);
	}
	return ESPIA_OK;
}

void buffer_img_destroy(app_context_t context, struct device_data *dev_data,
			struct image_data *img_ata)
{
}

int buffer_img(app_context_t context, union param *pars)
{
	int buffer_nr = pars[0].i;
	struct img_frame_info req_finfo;

	if (buffer_is_roi_acc(buffer_nr))
		return ESPIA_TEST_ERR_ROIACC;

	req_finfo.buffer_nr    = buffer_nr;
	req_finfo.frame_nr     = ESPIA_ACQ_ANY;
	req_finfo.round_count  = ESPIA_ACQ_ANY;
	req_finfo.acq_frame_nr = ESPIA_ACQ_ANY;
	return gen_img_add(context, buffer_nr, buffer_img_cb, &req_finfo,
			   buffer_img_destroy);
}

int buffer_img_del(app_context_t context, union param *pars)
{
	int buffer_nr = pars[0].i;
	return gen_img_del(context, buffer_nr);
}

int buffer_img_del_all(app_context_t context, union param *pars)
{
	union param buffer_nr;
	int i;
	struct image_data *img_data;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	if (dev_data->buff_data == NULL)
		return ESPIA_OK;

	for (i = -1; i < dev_data->nr_buffer; i++) {
		img_data = image_data_from_buffer_nr(dev_data, i);
		if (img_data->img != IMAGE_INVALID) {
			buffer_nr.i = i;
			buffer_img_del(context, &buffer_nr);
		}
	}

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * buffer image rates/norm
 *--------------------------------------------------------------------------*/

int buffer_img_rates(app_context_t context, union param *pars)
{
	int buffer_nr = pars[0].i;
	struct image_data *img_data;
	float update, refresh;
	char name[BUFFER_IMG_NAME_LEN];
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	get_image_data_check(img_data, dev_data, buffer_nr);
	if (img_data->img == IMAGE_INVALID)
		return ESPIA_TEST_ERR_IMGMISS;
	
	image_get_rates(img_data->img, &update, &refresh);
	buffer_img_name(name, dev_data, buffer_nr);
	context_out(context, "%s: update rate: %.1f, refresh rate: %.1f\n", 
		    name, update, refresh);

	return ESPIA_OK;
}


int buffer_img_norm(app_context_t context, union param *pars)
{
	int auto_range, buffer_nr = pars[0].i;
	struct image_data *img_data;
	unsigned long min_val, max_val;
	char name[BUFFER_IMG_NAME_LEN];
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	get_image_data_check(img_data, dev_data, buffer_nr);
	if (img_data->img == IMAGE_INVALID)
		return ESPIA_TEST_ERR_IMGMISS;
	
	image_get_norm(img_data->img, &min_val, &max_val, &auto_range);
	if (!scdxipci_is(REQUEST, pars[1].i))
		min_val    = pars[1].i;
	if (!scdxipci_is(REQUEST, pars[2].i))
		max_val    = pars[2].i;
	if (!scdxipci_is(REQUEST, pars[3].i))
		auto_range = pars[3].i;
	image_set_norm(img_data->img, min_val, max_val, auto_range);
	buffer_img_name(name, dev_data, buffer_nr);
	context_out(context, "%s norm.: min-val: %ld, max-val: %ld, "
		    "auto-range: %d\n", name, min_val, max_val, auto_range);

	return ESPIA_OK;
}

#endif // WITH_IMAGE

/*--------------------------------------------------------------------------
 * set generic SG
 *--------------------------------------------------------------------------*/

int set_gen_sg(app_context_t context, int img_config, 
	       struct espia_frame_dim *frame_dim, struct espia_roi *roi)
{
	struct scdxipci_sg_table *sg_list;
	int ret, rel_ret, nr_dev, i;

	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	ret = espia_create_sg(dev_data->dev, img_config, frame_dim, roi,
			      &sg_list, &nr_dev);
	if (ret < 0)
		return ret;

	for (i = 0; i < nr_dev; i++) {
		ret = espia_set_sg(dev_data->dev, i, &sg_list[i]);
		if (ret < 0)
			break;
	}

	rel_ret = espia_release_sg(dev_data->dev, sg_list, nr_dev);
	return (ret < 0) ? ret : rel_ret;
}

/*--------------------------------------------------------------------------
 * RoI set / reset
 *--------------------------------------------------------------------------*/

int roi_set(app_context_t context, union param *pars)
{
	struct espia_frame_dim frame_dim;
	struct espia_roi roi;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;
	else if (dev_data->nr_buffer == 0)
		return ESPIA_TEST_ERR_NOBUFFER;

	frame_dim.width  = pars[0].i;
	frame_dim.height = pars[1].i;
	frame_dim.depth  = dev_data->frame_dim.depth;
	roi.left         = pars[2].i;
	roi.top          = pars[3].i;
	roi.width        = dev_data->frame_dim.width;
	roi.height       = dev_data->frame_dim.height;

	return set_gen_sg(context, ESPIA_SG_NORM, &frame_dim, &roi);
}


int roi_reset(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	return set_gen_sg(context, ESPIA_SG_NORM, &dev_data->frame_dim, NULL);
}


/*--------------------------------------------------------------------------
 * SG set
 *--------------------------------------------------------------------------*/

int sg_config_list(app_context_t context)
{
	char *desc;
	int conf;

	context_out(context, "  Code      Description                     \n"
			     "--------------------------------------------\n");
	for (conf = 0; conf < espia_sg_img_config_nr; conf++) {
		desc = espia_sg_img_config_desc[conf];
		context_out(context, "    %d       %s\n", conf, desc);
	}

	return ESPIA_OK;
}

int sg_set(app_context_t context, union param *pars)
{
	unsigned long img_config = pars[0].i;
	struct device_data *dev_data = dev_data_from_context(context);
	if (scdxipci_is(ANY, img_config))
		return sg_config_list(context);

	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;
	else if (dev_data->nr_buffer == 0)
		return ESPIA_TEST_ERR_NOBUFFER;

	return set_gen_sg(context, img_config, &dev_data->frame_dim, NULL);
}


/*--------------------------------------------------------------------------
 * dev list set
 *--------------------------------------------------------------------------*/

int dev_set(app_context_t context, union param *pars)
{
	int dev_list[ESPIA_TEST_MAX_NR_DEV], nr_dev, idx, ret;
	char *list, *ptr, *term;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	list = get_param_str(&pars[0]);
	if (list != NULL) {
		list = strdup(list);
		if (list == NULL)
			return ESPIA_TEST_ERR_NOMEM;
	}

	for (nr_dev = 0, ptr = list; ptr && *ptr; nr_dev++, ptr = term) {
		term = strchr(ptr, ',');
		if (term != NULL)
			*term++ = 0;
		idx = atoi(ptr);
		if (!is_good_dev_nr(idx) || (idx == ESPIA_META_DEV)) {
			ret = ESPIA_TEST_ERR_DEVNR;
			goto out;
		}

		dev_list[nr_dev] = idx;
	}

	ret = espia_set_dev(dev_data->dev, dev_list, nr_dev);
 out:
	if (list != NULL)
		free(list);

	return ret;
}


/*--------------------------------------------------------------------------
 * acq start/stop/status
 *--------------------------------------------------------------------------*/

int start_acq(app_context_t context, union param *pars)
{
	int ret, nr_frames;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	nr_frames = pars[1].i;
	ret = roi_acc_buffer_init(context, nr_frames);
	if (ret < 0)
		return ret;

	last_frame_reset(&dev_data->last_frame);
	last_frame_reset(&dev_data->buff_save_data.last_frame);

	ret = espia_start_acq(dev_data->dev, pars[0].i, nr_frames, pars[2].i);
	return ret;
}

int stop_acq(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	return espia_stop_acq(dev_data->dev);
}

int acq_status(app_context_t context, union param *pars)
{
	int ret, active;
	unsigned long acq_run_nr;
	struct buffer_save_data *bsd;
	struct img_frame_info last_frame;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;
	ret = espia_acq_active(dev_data->dev, &acq_run_nr);
	if (ret < 0)
		return ret;

	active = (ret > 0);
	context_out(context, "Acquisition %ld is %srunning.\n", 
		    acq_run_nr, active ? "" : "not ");
	if (!active && (acq_run_nr == 0))
		return ESPIA_OK;

	context_out(context, "\nLast acquired frame:\n");
	last_frame_get(&dev_data->last_frame, &last_frame);
	if (last_frame_available(&last_frame))
		print_finfo(context, NULL, &last_frame, 0);
	else
		context_out(context, "  No available yet\n");
		
	if (!auto_save_active(dev_data))
		return ESPIA_OK;

	context_out(context, "Last saved frame:\n");
	bsd = &dev_data->buff_save_data;
	last_frame_get(&bsd->last_frame, &last_frame);
	if (last_frame_available(&last_frame))
		print_finfo(context, NULL, &last_frame, 0);
	else
		context_out(context, "  No available yet\n");

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * callback helpers & function
 *--------------------------------------------------------------------------*/

void sort_arr(int *arr, int dim)
{
	int *end, *p1, *p2, aux;

	end = arr + dim;
	for (p1 = arr; p1 < end - 1; p1++)
		for (p2 = p1 + 1; p2 < end; p2++)
			if (*p2 < *p1) {
				aux = *p2;
				*p2 = *p1;
				*p1 = aux;
			}
}

void print_finfo(app_context_t context, 
		 struct img_frame_info *req_finfo,
		 struct img_frame_info *cb_finfo, int ret)
{
	double sec = (double) cb_finfo->time_us / 1e6;

	if (finished_espia_frame_info(cb_finfo, ret)) {
		context_out(context, "  Aborted or finished after %.3f s\n",
			    sec);
		return;
	}

	context_out(context, "  buffer_nr=%ld, frame_nr=%ld, round_count=%ld,"
		    " acq_frame_nr=%ld\n", cb_finfo->buffer_nr, 
		    cb_finfo->frame_nr,	cb_finfo->round_count, 
		    cb_finfo->acq_frame_nr);
	context_out(context, "  sec=%.3f, pixels=%ld\n\n", sec,
		    cb_finfo->pixels);
}

int acq_cb_funct(app_context_t context, struct device_data *dev_data, 
		 struct cb_priv_data *priv_data,
		 struct espia_cb_data *cb_data)
{
	int each = espia_cb_each(&cb_data->info.acq.req_finfo);
	int ret = cb_data->ret;
	char *each_str;

	each_str =  (each == ESPIA_EACH_BUFFER) ? " - Each buffer" :
		   ((each == ESPIA_EACH_FRAME)  ? " - Each frame"  :
		                                  "");
	context_out(context, "\n\nAcq. callback #%d for dev #%d%s:\n", 
		    cb_data->cb_nr, dev_data->dev_nr, each_str);
	print_finfo(context, &cb_data->info.acq.req_finfo, 
		    &cb_data->info.acq.cb_finfo, ret);
	return ESPIA_OK;
}

int status_cb_funct(app_context_t context, struct device_data *dev_data, 
		    struct cb_priv_data *priv_data,
		    struct espia_cb_data *cb_data)
{
	unsigned char ccd_status = cb_data->info.status.ccd_status;

	context_out(context, "\n\nStatus callback #%d for dev #%d:\n", 
		    cb_data->cb_nr, dev_data->dev_nr);
	context_out(context, "CCD status: %d (0x%02x)\n", 
		    ccd_status, ccd_status);
	return ESPIA_OK;
}

int ser_rx_cb_funct(app_context_t context, struct device_data *dev_data, 
		    struct cb_priv_data *priv_data,
		    struct espia_cb_data *cb_data)
{
	struct espia_cb_serial *cb_serial = &cb_data->info.serial;

	context_out(context, "\n\nSer. Rx callback #%d for dev #%d:\n", 
		    cb_data->cb_nr, dev_data->dev_nr);
	print_serial_str(context, cb_serial->buffer, 
			 cb_serial->nr_bytes, priv_data->print_raw);
	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * generic callback function
 *--------------------------------------------------------------------------*/

int virt_acq_cb(app_context_t context, struct device_data *dev_data, 
		struct cb_priv_data *priv_data, struct espia_cb_data *cb_data)
{
	struct img_frame_info *cb_finfo, real_cb_finfo;
	struct img_frame_info *req_finfo, real_req_finfo;
	gen_cb *cb_funct = priv_data->cb_funct;
	int finished, ret = 0;
	unsigned long good_frame_nr = SCDXIPCI_ANY;

	cb_finfo = &cb_data->info.acq.cb_finfo;
	req_finfo = &cb_data->info.acq.req_finfo;
	real_cb_finfo = *cb_finfo;

	finished = finished_img_frame_info(cb_finfo);
	if (!finished) {
		virt_frame_info(dev_data, cb_finfo);
		good_frame_nr = req_finfo->frame_nr;
	}

	if (finished) {
		// must inform that acq. was aborted
	} else if (req_finfo->buffer_nr == ESPIA_ACQ_EACH) {
		good_frame_nr = dev_data->buffer_frames - 1;
	} else if (scdxipci_is(ANY, req_finfo->buffer_nr)) {
		// check frame below
	} else if (cb_finfo->buffer_nr != req_finfo->buffer_nr) {
		goto out;
	}

	if (!scdxipci_is(ANY, good_frame_nr) && 
	    !scdxipci_is(ANY, cb_finfo->frame_nr) && 
	    (cb_finfo->frame_nr != good_frame_nr))
		goto out;

	real_req_finfo = *req_finfo;
	*req_finfo = priv_data->req_finfo;

	ret = cb_funct(context, dev_data, priv_data, cb_data);

	*req_finfo = real_req_finfo;

 out:
	if (!finished)
		*cb_finfo = real_cb_finfo;

	return ret;
}


int gen_cb_funct(struct espia_cb_data *cb_data)
{
	struct cb_priv_data *priv_data = priv_data_from_cb_data(cb_data);
	app_context_t context = priv_data->context;
	struct device_data *dev_data = priv_data->dev_data;
	gen_cb *cb_funct = priv_data->cb_funct;
	int ret = cb_data->ret;
	char *type;

	if (ret >= 0) {
		if (cb_needs_virtual_conv(dev_data, cb_data))
			cb_funct = &virt_acq_cb;
		return cb_funct(context, dev_data, priv_data, cb_data);
	}

	switch (cb_data->type) {
	case ESPIA_CB_ACQ:	  type = "Acq.";       break;
	case ESPIA_CB_CCD_STATUS: type = "CCD Status"; break;
	case ESPIA_CB_SERIAL_TX:  type = "Ser. Tx";    break;
	case ESPIA_CB_SERIAL_RX:  type = "Ser. Rx";    break;
	default:		  type = "Unknown";    break;
	}

	context_err(context, "\n\n%s callback #%d on dev. #%d:\n",
		    type, cb_data->cb_nr, dev_data->dev_nr);
	if ((ret == SCDXIPCI_ERR_TIMEOUT) && !priv_data->filter_tout)
		context_err(context, "  Timeout!\n");
	else 
		context_err(context, "  Error %d (0x%x): %s\n", ret, ret,
			    espia_test_strerror(cb_data->ret));
	if (espia_fatal_error(cb_data->ret)) {
		context_err(context, "  Aborting!\n");
		return ESPIA_ERR_ABORT;
	}

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * callback register/unregister/unregister_all/list
 *--------------------------------------------------------------------------*/

int check_real_acq_cb(struct device_data *dev_data, 
		      struct espia_cb_data *cb_data)
{
	struct img_frame_info *req_finfo = &cb_data->info.acq.req_finfo;
	struct cb_priv_data *priv_data = priv_data_from_cb_data(cb_data);
	unsigned long virt_buffer_nr = req_finfo->buffer_nr;

	priv_data->req_finfo = *req_finfo;

	if (virt_buffer_nr == ESPIA_ACQ_EACH) {
		req_finfo->buffer_nr    = ESPIA_ACQ_ANY;
		req_finfo->acq_frame_nr = ESPIA_ACQ_EACH;
	} else if (!scdxipci_is(ANY, virt_buffer_nr)) {
		req_finfo->buffer_nr    = real_buffer_nr(dev_data, 
							 virt_buffer_nr, 0);
	}

	// we cannot fix a particular buffer frame_nr on a single cb
	req_finfo->frame_nr = ESPIA_ACQ_ANY;

	return 0;
}


int reg_gen_cb(app_context_t context, struct espia_cb_data *cb_data,
	       struct cb_priv_data *priv_data, int user_act, int *cb_nr_ptr)
{
	int ret, len, cb_nr = (int) SCDXIPCI_INVALID;
	struct cb_priv_data **aux, *real_priv_data = NULL;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data) {
		ret = ESPIA_TEST_ERR_SELECT;
		goto out;
	}

	if (priv_data->cb_funct == NULL) {
		ret = ESPIA_TEST_ERR_CBFUNCT;
		goto out;
	}

	len = sizeof(*priv_data);
	real_priv_data = (struct cb_priv_data *) malloc(len);
	if (real_priv_data == NULL) {
		ret = ESPIA_TEST_ERR_NOMEM;
		context_err(context, "Error allocating cb priv. data "
			    "(%d bytes)\n", len);
		goto out;
	}
        *real_priv_data = *priv_data;

	real_priv_data->dev_data = dev_data;
	real_priv_data->context  = context;
	real_priv_data->user_cb  = user_act & ETEST_CB_USER;
	cb_data->dev             = dev_data->dev;
	cb_data->data            = real_priv_data;
	cb_data->cb              = gen_cb_funct;

	if (cb_needs_virtual_conv(dev_data, cb_data)) {
		ret = check_real_acq_cb(dev_data, cb_data);
		if (ret < 0)
			goto out;
	}
		
	ret = espia_register_callback(dev_data->dev, cb_data, &cb_nr);
	if (ret < 0)
		goto out;

	if (user_act & ETEST_CB_ACTIVE) {
		ret = espia_callback_active(dev_data->dev, cb_nr, 1);
		if (ret < 0)
			goto out;
	}

	if (cb_nr >= dev_data->nr_cb) {
		len = (cb_nr + 1) * sizeof(*dev_data->cbs);
		aux = (struct cb_priv_data **) realloc(dev_data->cbs, len);
		if (aux == NULL) {
			ret = ESPIA_TEST_ERR_NOMEM;
			goto out;
		}
		len -= (dev_data->nr_cb + 1) * sizeof(*dev_data->cbs);
		memset(&aux[dev_data->nr_cb], 0 , len);
		dev_data->cbs = aux;
		dev_data->nr_cb = cb_nr + 1;
	}
	dev_data->cbs[cb_nr] = real_priv_data;

	if (user_act & ETEST_CB_USER)
		context_out(context, "Registered cb #%d\n", cb_nr);

	ret = ESPIA_OK;
 out:
	if (ret < 0) {
		if (!scdxipci_is(INVALID, cb_nr))
			espia_unregister_callback(dev_data->dev, cb_nr);
		if (real_priv_data)
			free(real_priv_data);
	}

	if (cb_nr_ptr != NULL)
		*cb_nr_ptr = (ret >= 0) ? cb_nr : (int) SCDXIPCI_INVALID;

	return ret;
}

int unreg_gen_cb(app_context_t context, struct device_data *dev_data,
		 int cb_nr, int user_cb, int *cb_nr_ptr)
{
	int ret;
	struct cb_priv_data *priv_data;

	priv_data = get_cb_nr(dev_data, cb_nr);
	if ((priv_data == NULL) || (user_cb && !priv_data->user_cb))
		return ESPIA_TEST_ERR_CBNR;
		
	ret = espia_unregister_callback(dev_data->dev, cb_nr);
	if (ret < 0)
		return ret;

	free(priv_data);
	dev_data->cbs[cb_nr] = NULL;
	if (cb_nr_ptr != NULL)
		*cb_nr_ptr = (int) SCDXIPCI_INVALID;
	if (user_cb)
		context_out(context, "Unregistered cb #%d for dev #%d\n", 
			    cb_nr, dev_data->dev_nr);
	return ESPIA_OK;
}

int reg_acq_cb(app_context_t context, union param *pars)
{
	struct espia_cb_data cb_data;
	struct cb_priv_data priv_data;
	struct img_frame_info *req_finfo = &cb_data.info.acq.req_finfo;

	init_cb_priv_data(&priv_data);

	cb_data.type		= ESPIA_CB_ACQ;
	req_finfo->buffer_nr	= pars[0].i;
	req_finfo->frame_nr     = pars[1].i;
	req_finfo->round_count  = pars[2].i;
	req_finfo->acq_frame_nr = pars[3].i;
	cb_data.timeout 	= pars[4].i;
	priv_data.cb_funct	= acq_cb_funct;
		
	return reg_gen_cb(context, &cb_data, &priv_data, ETEST_CB_USER, NULL);
}

int reg_status_cb(app_context_t context, union param *pars)
{
	struct espia_cb_data cb_data;
	struct cb_priv_data priv_data;

	init_cb_priv_data(&priv_data);

	cb_data.type        = ESPIA_CB_CCD_STATUS;
	cb_data.timeout     = pars[0].i;
	priv_data.cb_funct  = status_cb_funct;
	return reg_gen_cb(context, &cb_data, &priv_data, ETEST_CB_USER, NULL);
}

int reg_ser_rx_cb(app_context_t context, union param *pars)
{
	int cb_nr, ret;
	struct espia_cb_data cb_data, *cb_datap;
	struct espia_cb_serial *cb_serial = &cb_data.info.serial;
	struct cb_priv_data priv_data;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	for (cb_nr = 0; cb_nr < dev_data->nr_cb; cb_nr++) {
		ret = espia_get_callback(dev_data->dev, cb_nr, &cb_datap);
		if (ret < 0)
			return ret;
		if (cb_datap->type == ESPIA_CB_SERIAL_RX)
			return ESPIA_TEST_ERR_CBREG;
	}

	init_cb_priv_data(&priv_data);

	pars[1].s[pars[1].len] = '\0';

	cb_data.type           = ESPIA_CB_SERIAL_RX;
	cb_serial->buffer      = NULL;
	cb_serial->buffer_len  = pars[0].i;
	cb_serial->term        = pars[1].s;
	cb_serial->term_len    = pars[1].len;
	cb_data.timeout        = pars[2].i;
	priv_data.print_raw    = pars[3].i;
	priv_data.filter_tout  = pars[4].i;
	priv_data.cb_funct     = ser_rx_cb_funct;

	return reg_gen_cb(context, &cb_data, &priv_data, ETEST_CB_USER, NULL);
}

int unreg_cb(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;
	return unreg_gen_cb(context, dev_data, pars[0].i, ETEST_CB_USER, NULL);
}

int unreg_all_cb(app_context_t context, union param *pars)
{
	int cb_nr;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	for (cb_nr = 0; cb_nr < dev_data->nr_cb; cb_nr++)
		if (is_user_cb(dev_data, cb_nr))
			unreg_gen_cb(context, dev_data, cb_nr, ETEST_CB_USER, 
				     NULL);
	if (auto_save_active(dev_data))
		buffer_save_cb_uninstall(context);

	return ESPIA_OK;
}

int cb_active(app_context_t context, union param *pars)
{
	int ret, cb_nr;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	cb_nr = pars[0].i;
	if (!is_user_cb(dev_data, cb_nr))
		return ESPIA_TEST_ERR_CBNR;
		
	ret = espia_callback_active(dev_data->dev, cb_nr, pars[1].i);
	if (ret >= 0) {
		context_out(context, "Callback #%d prevously %sactive\n",
			    cb_nr, !ret ? "not " : "");
		ret = 0;
	}
	return ret;
}

int list_acq_cb(app_context_t context)
{
	int ret, cb_nr, active;
	struct espia_cb_data *cb_data;
	struct img_frame_info *finfo;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	context_out(context, "Cb #    Buffer    Frame Nr    Count    "
		    "Acq. Frame     Timeout    Active\n");
	context_out(context, "---------------------------------------"
		    "--------------------------------\n");
	for (cb_nr = 0; cb_nr < dev_data->nr_cb; cb_nr++) {
		if (!is_user_cb(dev_data, cb_nr))
			continue;
		ret = espia_get_callback(dev_data->dev, cb_nr, &cb_data);
		if (ret < 0)
			return ret;
		else if (cb_data->type != ESPIA_CB_ACQ)
			continue;
		active = espia_callback_active(dev_data->dev, cb_nr, -1);
		finfo = &cb_data->info.acq.req_finfo;
		context_out(context, "%3d     %4ld      %4ld       %4ld"
			    "        %4ld      %10ld   %4d\n", cb_nr, 
			    finfo->buffer_nr, finfo->frame_nr, 
			    finfo->round_count, finfo->acq_frame_nr, 
			    cb_data->timeout, active);
	}
	return ESPIA_OK;
}

int list_status_cb(app_context_t context)
{
	int ret, cb_nr, active;
	struct espia_cb_data *cb_data;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	context_out(context, "Cb #      Timeout    Active\n");
	context_out(context, "---------------------------\n");
	for (cb_nr = 0; cb_nr < dev_data->nr_cb; cb_nr++) {
		if (!is_user_cb(dev_data, cb_nr))
			continue;
		ret = espia_get_callback(dev_data->dev, cb_nr, &cb_data);
		if (ret < 0)
			return ret;
		else if (cb_data->type != ESPIA_CB_CCD_STATUS)
			continue;
		active = espia_callback_active(dev_data->dev, cb_nr, -1);
		context_out(context, "%3d    %10ld   %4d\n", cb_nr, 
			    cb_data->timeout, active);
	}
	return ESPIA_OK;
}

int list_ser_tx_cb(app_context_t context)
{
	return ESPIA_OK;
}

int list_ser_rx_cb(app_context_t context)
{
	int ret, cb_nr, active;
	char term[80], *src, *dest = term;
	struct espia_cb_data *cb_data;
	struct espia_cb_serial *cb_serial;
	struct cb_priv_data *priv_data;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	context_out(context, "Cb #    Buff. Len    Term      Timeout    "
		    "Prn. Raw   Filter Tout.   Active\n");
	context_out(context, "------------------------------------------"
		    "--------------------------------\n");
	for (cb_nr = 0; cb_nr < dev_data->nr_cb; cb_nr++) {
		if (!is_user_cb(dev_data, cb_nr))
			continue;
		ret = espia_get_callback(dev_data->dev, cb_nr, &cb_data);
		if (ret < 0)
			return ret;
		else if (cb_data->type != ESPIA_CB_SERIAL_RX)
			continue;
		active = espia_callback_active(dev_data->dev, cb_nr, -1);
		cb_serial = &cb_data->info.serial;
		priv_data = priv_data_from_cb_data(cb_data);
		src = cb_serial->term;
		*dest++ = '"';
		while (src && *src)
			dest += sprintf(dest, "%s", get_char_repr(*src++));
		strcpy(dest, "\"");
		context_out(context, "%3d      %6ld      %-6s %10ld       "
			    "%2d          %2d        %4d\n", cb_nr, 
			    cb_serial->buffer_len, term, cb_data->timeout, 
			    priv_data->print_raw, priv_data->filter_tout, 
			    active);
	}
	return ESPIA_OK;
}

int cb_list(app_context_t context, union param *pars)
{
	struct {
		int type;
		char *str;
		int (*func)(app_context_t context);
	} *fptr, func_arr[] = {
		{ESPIA_CB_ACQ,        "Acq",      list_acq_cb},
		{ESPIA_CB_CCD_STATUS, "Status",   list_status_cb},
		{ESPIA_CB_SERIAL_TX,  "SerialTx", list_ser_tx_cb},
		{ESPIA_CB_SERIAL_RX,  "SerialRx", list_ser_rx_cb},
	};
	int ntypes = sizeof(func_arr) / sizeof(func_arr[0]);
	int i, cb_nr, ret;
	struct espia_cb_data *cb_data;

	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;
	if (dev_data->nr_cb == 0) {
		context_out(context, "No registered callback\n");
		return ESPIA_OK;
	}

	for (i = 0, fptr = func_arr; i < ntypes; i++, fptr++) {
		pars[0].s[pars[0].len] = '\0';
		if (strcasecmp(pars[0].s, fptr->str) != 0)
			continue;

		for (cb_nr = 0; cb_nr < dev_data->nr_cb; cb_nr++) {
			if (!is_user_cb(dev_data, cb_nr))
				continue;
			ret = espia_get_callback(dev_data->dev, cb_nr,
						 &cb_data);
			if (ret < 0)
				return ret;
			if (cb_data->type == fptr->type)
				return fptr->func(context);
		}

		context_out(context, "No %s callback registered\n", 
			    fptr->str);
		return ESPIA_OK;
	}
	return ESPIA_TEST_ERR_CBTYPE;
}


/*--------------------------------------------------------------------------
 * get frame / frame addr
 *--------------------------------------------------------------------------*/

int get_frame(app_context_t context, union param *pars)
{
	int ret, timeout;
	struct img_frame_info req_finfo, cb_finfo;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	req_finfo.buffer_nr	= pars[0].i;
	req_finfo.frame_nr	= pars[1].i;
	req_finfo.round_count	= pars[2].i;
	req_finfo.acq_frame_nr	= pars[3].i;
	timeout			= pars[4].i;

	cb_finfo = req_finfo;
	ret = espia_get_frame(dev_data->dev, &cb_finfo, timeout);
	if (ret < 0)
		return ret;
	print_finfo(context, &req_finfo, &cb_finfo, ret);
	return ESPIA_OK;
}

int frame_addr(app_context_t context, union param *pars)
{
	int ret;
	void *addr;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	ret = espia_frame_address(dev_data->dev, pars[0].i, pars[1].i, &addr);
	if (ret >= 0)
		context_out(context, "Frame address: %p\n", addr);
	return ret;
}


/*--------------------------------------------------------------------------
 * CCD status
 *--------------------------------------------------------------------------*/

int ccd_status(app_context_t context, union param *pars)
{
	int ret;
	unsigned char ccd_status;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	ret = espia_ccd_status(dev_data->dev, &ccd_status, pars[0].i);
	if (ret == SCDXIPCI_OK)
		context_out(context, "CCD status: %d (0x%02x)\n", 
			    ccd_status, ccd_status);
	return ret;
}


/*--------------------------------------------------------------------------
 * RoI accumulation buffer
 *--------------------------------------------------------------------------*/

int roi_acc_buffer_init(app_context_t context, int nr_frames)
{
	int len, create, update, ret;
	struct espia_frame_dim *fdim;
	struct roi_acc_buffer *roi_acc;
#ifdef WITH_IMAGE
	struct image_data *img_data;
#endif // WITH_IMAGE
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	roi_acc = &dev_data->roi_acc_buff;
#ifdef WITH_IMAGE
	img_data = &roi_acc->img_data;
#endif // WITH_IMAGE
	fdim = &dev_data->frame_dim;

	if (!img_roi_valid(&roi_acc->roi, fdim))
		return ESPIA_OK;

	create = ((roi_acc->nr_frames == 0) && (nr_frames > 0) &&
		  (nr_frames != roi_acc->real_nr_frames));
	update = ((roi_acc->nr_frames > 0) || 
		  ((nr_frames > 0) && 
		   (nr_frames == roi_acc->real_nr_frames)));
	if (!update && !create)
		return ESPIA_OK;

	if (create) {
		roi_acc->real_nr_frames = nr_frames;
		ret = roi_acc_buffer_install(context);
		if (ret < 0)
			return ret;
	} else {
		len = (img_roi_size(&roi_acc->roi) * fdim->depth *
		       roi_acc->real_nr_frames);
		memset(roi_acc->buffer_ptr, 0, len);
#ifdef WITH_IMAGE
		image_update(img_data->img);
#endif // WITH_IMAGE
	}

	return ESPIA_OK;
}

#ifdef WITH_IMAGE

void roi_acc_buffer_img_destroy(app_context_t context, 
				struct device_data *dev_data,
				struct image_data *img_data)
{
	struct roi_acc_buffer *roi_acc = &dev_data->roi_acc_buff;
	free(roi_acc->buffer_ptr);
	roi_acc->buffer_ptr = NULL;
	roi_acc->real_nr_frames = 0;
}

#endif // WITH_IMAGE

int roi_acc_buffer_cb(app_context_t context, struct device_data *dev_data,
		      struct image_data *img_data,
		      struct espia_cb_data *cb_data)
{
	struct img_frame_info *finfo = &cb_data->info.acq.cb_finfo;
	struct espia_frame_dim *fdim = &dev_data->frame_dim;
	struct roi_acc_buffer *roi_acc = &dev_data->roi_acc_buff;
	struct buffer_save_data buff_save;
	struct image_roi *roi = &roi_acc->roi;
	int roi_row_len, frm_row_len, roi_height, depth, row, ret;
	char *src_ptr, *dst_ptr;
	void *frm_ptr;

	if ((int) finfo->acq_frame_nr >= roi_acc->real_nr_frames)
		return ESPIA_OK;

	ret = espia_frame_address(dev_data->dev, finfo->buffer_nr, 
				  finfo->frame_nr, &frm_ptr);
	if (ret < 0) {
		context_err(context, "Error %d in espia_frame_address(%ld,%ld):"
			    " %s\n", ret, finfo->buffer_nr, finfo->frame_nr,
			    espia_test_strerror(ret));
		return ret;
	}

	src_ptr = (char *) frm_ptr;
	depth = fdim->depth;
	roi_row_len = img_roi_width(roi) * depth;
	frm_row_len = fdim->width * depth;
	roi_height = img_roi_height(roi);
	dst_ptr = (char *) roi_acc->buffer_ptr;
	dst_ptr += roi_row_len * roi_height * finfo->acq_frame_nr;
	src_ptr += frm_row_len * roi->row_beg + (roi->col_beg * depth);
	for (row = 0; row < roi_height; row++) {
		memcpy(dst_ptr, src_ptr, roi_row_len);
		dst_ptr += roi_row_len;
		src_ptr += frm_row_len;
	}
#ifdef WITH_IMAGE
	image_update(img_data->img);
#endif // WITH_IMAGE

	if (((int) finfo->acq_frame_nr < roi_acc->real_nr_frames - 1) ||
	    !roi_acc_auto_save_active(dev_data))
		return ESPIA_OK;

	buff_save.dev_data = dev_data;
	strcpy(buff_save.prefix, roi_acc->file_prefix);
	buff_save.idx = roi_acc->file_idx++;
	buff_save.suffix[0] = 0;
	buff_save.format = FMT_EDF;
	buff_save.file_frames = 0;
	buff_save.tot_file_frames = 1;
	buff_save.overwrite = 0;
	buff_save.print_name = 0;
	buff_save.fout = NULL;

	return buffer_save_file(context, &buff_save, ROI_ACC_BUFFER, NULL);
}

int roi_acc_buffer_install(app_context_t context)
{
	int ret, len, nr_frames;
	struct img_frame_info req_finfo;
	struct espia_frame_dim *dev_fdim, *racc_fdim;
	struct roi_acc_buffer *roi_acc;
	struct image_roi *roi;
	struct device_data *dev_data = dev_data_from_context(context);

	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	roi_acc = &dev_data->roi_acc_buff;
	nr_frames = roi_acc->real_nr_frames;
	roi_acc_buffer_del(context);
	roi_acc->real_nr_frames = nr_frames;

	roi = &roi_acc->roi;
	dev_fdim = &dev_data->frame_dim;
	len = img_roi_size(roi) * dev_fdim->depth;
	roi_acc->buffer_ptr = calloc(nr_frames, len);
	if (roi_acc->buffer_ptr == NULL)
		return ESPIA_TEST_ERR_NOMEM;

	racc_fdim = &roi_acc->frame_dim;
	racc_fdim->width  = img_roi_width(roi);
	racc_fdim->height = img_roi_height(roi) * nr_frames;
	racc_fdim->depth  = dev_fdim->depth;

#ifdef WITH_IMAGE
	req_finfo.buffer_nr	= ESPIA_ACQ_ANY;
	req_finfo.frame_nr	= ESPIA_ACQ_ANY;
	req_finfo.round_count	= ESPIA_ACQ_ANY;
	req_finfo.acq_frame_nr	= ESPIA_ACQ_EACH;
	ret = gen_img_add(context, ROI_ACC_BUFFER, roi_acc_buffer_cb, 
			  &req_finfo, roi_acc_buffer_img_destroy);
	if (ret < 0) {
		free(roi_acc->buffer_ptr);
		roi_acc->buffer_ptr = NULL;
		roi_acc->real_nr_frames = 0;
	}
	return ret;
#else // WITH_IMAGE
	return 0;
#endif // WITH_IMAGE
}

int roi_acc_buffer_del(app_context_t context)
{
#ifdef WITH_IMAGE
	gen_img_del(context, ROI_ACC_BUFFER);
#endif // WITH_IMAGE
	return ESPIA_OK;
}

int roi_acc_buffer(app_context_t context, union param *pars)
{
	int act, nr_frames, ret = 0;
	struct roi_acc_buffer *roi_acc;
	struct image_roi *roi;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	roi_acc = &dev_data->roi_acc_buff;
	roi = &roi_acc->roi;
	roi->col_beg = pars[0].i;
	roi->col_end = pars[1].i;
	roi->row_beg = pars[2].i;
	roi->row_end = pars[3].i;
	nr_frames = pars[4].i;
	roi_acc->nr_frames = nr_frames;
	strcpy(roi_acc->file_prefix, get_param_str(&pars[5]));
	if (pars[6].i >= 0)
		roi_acc->file_idx = pars[6].i;
	act = img_roi_valid(roi, &dev_data->frame_dim);
	if (act && (nr_frames > 0)) {
		roi_acc->real_nr_frames = nr_frames;
		ret = roi_acc_buffer_install(context);
	} else if (!act) {
		context_out(context, "Invalid RoI, deactivating\n");
		ret = roi_acc_buffer_del(context);
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * FOCLA open / close
 *--------------------------------------------------------------------------*/

int focla_open_dev(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	return focla_open(dev_data->dev, &dev_data->focla);
}

int focla_close_dev(app_context_t context, union param *pars)
{
	int ret;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	if (dev_data->focla == FOCLA_DEV_INVAL)
		return FOCLA_OK;

	ret = focla_close(dev_data->focla);
	if (ret < 0)
		return ret;

	dev_data->focla = FOCLA_DEV_INVAL;
	return FOCLA_OK;
}


/*--------------------------------------------------------------------------
 * FOCLA reg read / write
 *--------------------------------------------------------------------------*/

int focla_read(app_context_t context, union param *pars)
{
	int ret, reg = pars[0].i;
	unsigned char val;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	ret = focla_read_reg(dev_data->focla, reg, &val);
	if (ret == FOCLA_OK)
		context_out(context, "FOCLA reg %d: 0x%02x (%d)\n", reg,
			    val, val);
	return ret;
}


int focla_write(app_context_t context, union param *pars)
{
	int ret, reg = pars[0].i;
	unsigned char prev_val, val = pars[1].i;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	ret = focla_read_reg(dev_data->focla, reg, &prev_val);
	if (ret != FOCLA_OK)
		return ret;

	context_out(context, "Previous value of FOCLA reg %d: 0x%02x (%d)\n",
		    reg, prev_val, prev_val);

	return focla_write_reg(dev_data->focla, reg, val);
}


/*--------------------------------------------------------------------------
 * FOCLA param list 
 *--------------------------------------------------------------------------*/

int focla_param_list(app_context_t context, union param *pars)
{
	struct espia_param *fparam;
	int i, nr_param, ret;
	
	nr_param = -1;
	ret = focla_get_param_data(&nr_param, NULL);
	if (ret < 0)
		return ret;

	context_out(context, "   Name         Reg    Mask   Shift    "
		    "Description      \n");
	context_out(context, "---------------------------------------"
		    "-----------------\n");

	for (i = 0; i < nr_param; i++) {
		ret = focla_get_param_data(&i, &fparam);
		if (ret < 0)
			return ret;
		context_out(context, "  %-12s   %d     0x%02x     %d      "
			    "%s\n", fparam->name, fparam->reg, fparam->mask, 
			    fparam->shift, fparam->desc);
	}

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * FOCLA param get / set
 *--------------------------------------------------------------------------*/

int focla_get_par(app_context_t context, union param *pars)
{
	struct espia_param *fparam;
	int ret, i, val, nr_param, first, end;
	char *name = get_param_str(&pars[0]);
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	nr_param = -1;
	ret = focla_get_param_data(&nr_param, NULL);
	if (ret < 0)
		return ret;

	if (strcasecmp(name, "ALL") == 0) {
		first = 0;
		end = nr_param;
	} else {
		for (first = 0; first < nr_param; first++) {
			ret = focla_get_param_data(&first, &fparam);
			if (ret < 0)
				return ret;
			if (strcasecmp(fparam->name, name) == 0)
				break;
		}
		if (first == nr_param)
			return ESPIA_TEST_ERR_PARAM;
		end = first + 1;
	}

	context_out(context, " Name                                       "
		    "Value   \n");
	context_out(context, "--------------------------------------------"
		    "--------\n");

	for (i = first; i < end; i++) {
		ret = focla_get_param(dev_data->focla, i, &val);
		if (ret < 0)
			return ret;
		ret = focla_get_param_data(&i, &fparam);
		if (ret < 0)
			return ret;
		context_out(context, " %-40s  0x%02x (%d)\n", fparam->name,
			    val, val);
	}

	return ret;
}


int focla_set_par(app_context_t context, union param *pars)
{
	struct espia_param *fparam;
	int ret, unlock_ret, i, val, nr_param;
	char *name = get_param_str(&pars[0]);
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	nr_param = -1;
	ret = focla_get_param_data(&nr_param, NULL);
	if (ret < 0)
		return ret;

	for (i = 0; i < nr_param; i++) {
		ret = focla_get_param_data(&i, &fparam);
		if (ret < 0)
			return ret;
		if (strcasecmp(fparam->name, name) == 0)
			break;
	}
	if (i == nr_param)
		return ESPIA_TEST_ERR_PARAM;

	ret = focla_lock(dev_data->focla);
	if (ret < 0)
		return ret;
		
	ret = focla_get_param(dev_data->focla, i, &val);
	if (ret < 0)
		goto unlock;
	context_out(context, "Previous FOCLA %s value: 0x%02x (%d)\n", 
		    fparam->name, val, val);
	
	ret = focla_set_param(dev_data->focla, i, pars[1].i);

 unlock:
	unlock_ret = focla_unlock(dev_data->focla);
	return (ret < 0) ? ret : unlock_ret;
}


/*--------------------------------------------------------------------------
 * FOCLA signal list
 *--------------------------------------------------------------------------*/

int focla_sig_list(app_context_t context, union param *pars)
{
	struct focla_signal *fsig;
	int i, nr_sig, ret;
	
	nr_sig = -1;
	ret = focla_get_sig_data(&nr_sig, NULL);
	if (ret < 0)
		return ret;

	context_out(context, "  Name           Description          \n");
	context_out(context, "--------------------------------------\n");

	for (i = 0; i < nr_sig; i++) {
		ret = focla_get_sig_data(&i, &fsig);
		if (ret < 0)
			return ret;
		context_out(context, "  %-12s   %s\n", fsig->name, fsig->desc);
	}

	return ESPIA_OK;
}



/*--------------------------------------------------------------------------
 * FOCLA signal pulse start/stop/status
 *--------------------------------------------------------------------------*/

int get_focla_sig(char *sig_name) 
{
	struct focla_signal *fsig;
	int ret, sig, nr_sig;

	nr_sig = -1;
	ret = focla_get_sig_data(&nr_sig, NULL);
	if (ret < 0)
		return ret;

	for (sig = 0; sig < nr_sig; sig++) {
		ret = focla_get_sig_data(&sig, &fsig);
		if (ret < 0)
			return ret;
		if (strcasecmp(fsig->name, sig_name) == 0)
			return sig;
	}

	return ESPIA_TEST_ERR_FOCLASIG;
}


int focla_pulse_start(app_context_t context, union param *pars)
{
	int width_arr[2] = {pars[3].i, pars[4].i};
	int sig_nr, nr_stage = sizeof(width_arr) / sizeof(width_arr[0]);
	char *sig_name = get_param_str(&pars[1]);
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	sig_nr = get_focla_sig(sig_name);
	if (sig_nr < 0)
		return sig_nr;
	return focla_sig_pulse_start(dev_data->focla, pars[0].i, sig_nr, 
				     pars[2].i, width_arr, nr_stage, pars[5].i);
}

int focla_pulse_stop(app_context_t context, union param *pars)
{
	char *sig_name = get_param_str(&pars[1]);
	int sig_nr;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	sig_nr = get_focla_sig(sig_name);
	if (sig_nr < 0)
		return sig_nr;
	return focla_sig_pulse_stop(dev_data->focla, pars[0].i, sig_nr);
}


int focla_pulse_status(app_context_t context, union param *pars)
{
	int cam_nr, sig_nr, active, ret, curr_pulse, curr_stage;
	char *sig_name = get_param_str(&pars[1]);
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	cam_nr = pars[0].i;
	sig_nr = get_focla_sig(sig_name);
	if (sig_nr < 0)
		return sig_nr;
	ret = focla_sig_pulse_status(dev_data->focla, cam_nr, sig_nr, 
				     &active, &curr_pulse, &curr_stage);
	if (ret < 0)
		return ret;

	context_out(context, "FOCLA %d %s pulse status:\n", 
		    cam_nr + 1, sig_name);
	context_out(context, "  active=%d, curr_pulse=%d, curr_stage=%d\n", 
		    active, curr_pulse, curr_stage);

	return FOCLA_OK;
}



/*--------------------------------------------------------------------------
 * FOCLA serial read/write/flush
 *--------------------------------------------------------------------------*/

int focla_serial_read(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	unsigned long nr_bytes = pars[0].i;
	int ret, raw = pars[2].i;
	char *buffer;

	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	ret = alloc_serial_buffer(context, nr_bytes, &buffer);
	if (ret < 0)
		return ret;

	ret = focla_ser_read(dev_data->focla, buffer, &nr_bytes, pars[1].i);
	return print_free_serial_buffer(context, buffer, nr_bytes, raw, ret);
}

int focla_serial_read_str(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	unsigned long nr_bytes = pars[0].i;
	int ret, raw = pars[3].i;
	char *buffer;

	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	ret = alloc_serial_buffer(context, nr_bytes, &buffer);
	if (ret < 0)
		return ret;

	ret = focla_ser_read_str(dev_data->focla, buffer, &nr_bytes,
				 pars[1].s, pars[1].len, pars[2].i);
	return print_free_serial_buffer(context, buffer, nr_bytes, raw, ret);
}

int focla_serial_write(app_context_t context, union param *pars)
{
	int ret;
	struct device_data *dev_data = dev_data_from_context(context);
	unsigned long nr_bytes = pars[0].len;
	char *action;

	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;
	ret = focla_ser_write(dev_data->focla, pars[0].s, &nr_bytes, pars[1].i, 
			      pars[2].i, pars[3].i);
	if (ret == ESPIA_OK) {
		action = (pars[0].len > 0) ? "Transmited" : "Still to TX:";
		context_out(context, "%s %ld bytes.\n", action, nr_bytes);
	}

	return ret;
}

int focla_serial_flush(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;
	return focla_ser_flush(dev_data->focla);
}


/*--------------------------------------------------------------------------
 * Poll sleep time
 *--------------------------------------------------------------------------*/

int poll_sleep(app_context_t context, union param *pars)
{
	int ret;

	ret = set_poll_sleep_time(context, pars[0].i);
	context_out(context, "Previous polling sleep time: %d usec\n", ret);

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * get stats
 *--------------------------------------------------------------------------*/

#define STAT_DESC_LEN		40

#define print_stat(context, desc, format, val)		\
	context_out(context, "%-*s " format "\n", STAT_DESC_LEN, \
		    desc ":", val)

#define print_dev_stat(context, nr, desc, format, val)			\
	context_out(context, "Dev. #%d %-*s " format "\n", nr, STAT_DESC_LEN-8,\
		    desc ":", val)

int get_stats(app_context_t context, union param *pars)
{
	int ret, i;
	struct scdxipci_stats stats;
	struct scdxipci_dev_stats *dev_stats;
	unsigned long cnt, page_size;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	ret = scdxipci_get_stats(dev_data->dev, &stats);
	if (ret != SCDXIPCI_OK)
		return ret;

	context_out(context, "Acquisition statistics:\n\n");

	// SG data info
	dev_stats = stats.dev_stats;
	for (i = 0; i < SCDXIPCI_NR_DEV_STATS; i++, dev_stats++) {
		cnt = dev_stats->sg_avail_count;
		if (cnt == 0)
			continue;

		print_dev_stat(context, i, "total of SG table switches",  
			       "%ld", cnt);
		print_dev_stat(context, i, "min. nr. of avail. SG tables", 
			       "%ld", dev_stats->sg_avail_min);
		print_dev_stat(context, i, "ave. nr. of avail. SG tables", 
			       "%.1f", (double) dev_stats->sg_avail_acc / cnt);
	}

	// copy frame info
	cnt = stats.copy_count;
	print_stat(context, "Nr. of buffer pages", "%ld", 
		   stats.nr_pages);
	print_stat(context, "Nr. of buffer pages 64-bit", "%ld", 
		   stats.nr_pages64);
	print_stat(context, "Total of frame copies", "%ld", cnt);
	if (cnt > 0) {
		print_stat(context, "Average copy nr. of pages", "%lld", 
			   stats.copy_pages / cnt);
		print_stat(context, "Average copy time", "%lld usec",
			   stats.copy_usec / cnt);
	}
	page_size = getpagesize();
	if (stats.copy_usec > 0) 
		print_stat(context, "Average copy speed", "%lld MB/s",
			   stats.copy_pages * page_size / stats.copy_usec);
	
	// get acq_frame_nr info
	cnt = stats.afn_call;
	print_stat(context, "Total of AFN calls", "%ld", cnt);
	if (cnt > 0) {
		cnt = stats.afn_count;
		print_stat(context, "Total of AFN searches", "%ld", cnt);
	}
	if (cnt > 0) {
		print_stat(context, "Average AFN time", "%.2f usec",
			   (double) stats.afn_usec / cnt);
		print_stat(context, "Average AFN iters", "%.2f", 
			   (double) stats.afn_iter / cnt);
		print_stat(context, "Max AFN iters", "%ld", 
			   stats.afn_max_iter);
		print_stat(context, "Average AFN steps", "%.2f", 
			   (double) stats.afn_step / cnt);
		print_stat(context, "Max AFN steps", "%ld", 
			   stats.afn_max_step);
	}
	
	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * firmware list
 *--------------------------------------------------------------------------*/

int firmware_list(app_context_t context, union param *pars)
{
	int nr_firmware, i, ret;
	struct espia_firmware *firmware;

	nr_firmware = -1;
	ret = espia_get_firmware_data(&nr_firmware, NULL);
	if (ret < 0)
		return ret;

	context_out(context, " Firmware      Checksum\n");
	context_out(context, "-----------------------\n");
	
	for (i = 0; i < nr_firmware; i++) {
		ret = espia_get_firmware_data(&i, &firmware);
		if (ret < 0)
			return ret;

		context_out(context, " %-13s  0x%04x\n",
			    firmware->name, firmware->checksum);
	}

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * get hw info
 *--------------------------------------------------------------------------*/

#define HW_INFO_DESC_LEN		20

#define print_hw_info(context, desc, format, val)		\
	context_out(context, "%-*s " format "\n", HW_INFO_DESC_LEN, \
		    desc ":", val)
#define print_hw_info_mod(context, desc, format, mod, val)	    \
	context_out(context, "%-*s " format "\n", HW_INFO_DESC_LEN, \
		    desc ":", mod, val)
#define print_hw_info2(context, desc, format, val, format2, val2)   \
	context_out(context, "%-*s " format " " format2 "\n",	    \
		    HW_INFO_DESC_LEN, desc ":", val, val2)

int get_hw_info(app_context_t context, union param *pars)
{
	int ret, action, mod;
	struct scdxipci_hw_info hw_info;
	struct scdxipci_board_id_info *board_id_info = &hw_info.board_id;
	struct scdxipci_firmware_info *firmware_info = &hw_info.firmware;
	struct scdxipci_card_id card_id;
	char *str;
	struct device_data *dev_data = dev_data_from_context(context);
	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	action = SCDXIPCI_HW_INFO_GET;
	memset(&hw_info, 0, sizeof(hw_info));
	memset(&card_id, 0, sizeof(card_id));
	board_id_info->len = sizeof(card_id);
	board_id_info->ptr = &card_id;
	ret = espia_hw_info(dev_data->dev, action, &hw_info);
	if (ret != ESPIA_OK)
		return ret;

	context_out(context, "Hardware information:\n\n");

	// basic hw info
	switch (hw_info.board_type) {
	case SCDXIPCI_ESPIA_PCI:     str = "Espia-PCI";     break;
	case SCDXIPCI_ESPIA_EXPRESS: str = "Espia-Express"; break;
	default:                     str = "Unknown";
	}
	print_hw_info(context, "Board Type",  "%s", str);
	print_hw_info(context, "Board Index", "%d", hw_info.board_idx);

	switch (hw_info.dev_type) {
	case SCDXIPCI_DEV_1_FO :     str = "1 Fiber-Optic link";  break;
	default:                     str = "Unknown";
	}
	print_hw_info(context, "Device Type",  "%s", str);
	print_hw_info(context, "Device Index", "%d", hw_info.dev_idx);

	// board id info
	print_hw_info(context, "Board ID Start",  "%d", board_id_info->start);
	print_hw_info(context, "Board ID Length", "%d", board_id_info->len);

	mod = scdxipci_ser_nr_len(board_id_info->ser_nr);
	print_hw_info_mod(context, "Serial Number",       "%0*d", mod, 
			  board_id_info->ser_nr);

	str = (card_id.CODE_TEST == TESTEE) ? "TESTED" : "PROTO";
	print_hw_info2(context, "Test Code", "0x%08x", card_id.CODE_TEST,
		       "(%s)", str);
	str = board_id_info->crc_ok ? "OK" : "BAD";
	print_hw_info2(context, "CRC",       "0x%08x", card_id.CRC,
		       "(%s)", str);
	
	// firmware info
	print_hw_info(context, "Firmware Start",  "%d", firmware_info->start);
	print_hw_info(context, "Firmware Length", "%d", firmware_info->len);
	print_hw_info(context, "Firmware Checksum","0x%04x", 
		      firmware_info->checksum);
	print_hw_info(context, "Firmware Name",   "%s", firmware_info->name);

	return SCDXIPCI_OK;
}


/*--------------------------------------------------------------------------
 * library versions
 *--------------------------------------------------------------------------*/

int get_version(app_context_t context, union param *pars)
{
	struct espia_lib_version ver;
	int ret;

	ret = espia_lib_version(&ver);
	if (ret < 0)
		return ret;

	context_out(context, "Compiled version: %s\n", ver.compile);
	context_out(context, "Running  version: %s\n", ver.run);

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------*
 * Custom EDF header functions
 *--------------------------------------------------------------------------*/

int edf_usr_header(app_context_t context, union param *pars)
{
	int ret, l, need_nl;
	regex_t re;
	char *usr_str;
	struct device_data *dev_data = dev_data_from_context(context);

	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	usr_str = get_param_str(&pars[0]);

	l = strlen(usr_str);
	if( l > 0 ) {
		/* Check if user_str is in the right format ??? */

#define RE_KEY  "[A-Za-z0-9_]+"
#define RE_VAL  "[^;]+"
#define RE_COMM "[^\n]*"
#define RE_STMT "(" RE_KEY "[ \t]*=[ \t]*" RE_VAL ")?;" RE_COMM
#define RE_LINE "(" RE_STMT ")?\n?"
		ret = regcomp(&re, "^(" RE_LINE ")*$", REG_EXTENDED|REG_NOSUB);
		if( ret != 0) {
			char re_err[128];
			
			regerror( ret, &re, re_err, 127 );
			context_err(context, "Error compiling the EDF "
				    "format regular expression: %s\n", 
				    re_err);
			return ESPIA_TEST_ERR_INTERNAL; 
		}
		ret = regexec( &re, usr_str, (size_t)0, NULL, 0);
		regfree(&re);
		if( ret != 0 )
			return ESPIA_TEST_ERR_EDFFORM;
	}

	edf_str_lock(dev_data);

	if(dev_data->edf_usr_str) {
		free( dev_data->edf_usr_str );
		dev_data->edf_usr_str = NULL;
	}

	if (l == 0) {
		ret = ESPIA_OK;
		goto out;
	}

	need_nl = ( usr_str[l-1] != '\n' );
	if (need_nl)
		l++;

	dev_data->edf_usr_str = (char *) malloc(l+1);
	if (dev_data->edf_usr_str == NULL) {
		ret = ESPIA_TEST_ERR_NOMEM;
		goto out;
	}
			
	strcpy( dev_data->edf_usr_str, usr_str );
	if (need_nl) 
		strcat(dev_data->edf_usr_str, "\n");

	ret = ESPIA_OK;
out:
	edf_str_unlock(dev_data);

	return ret;
}
