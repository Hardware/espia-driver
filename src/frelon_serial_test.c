#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "espia_lib.h"


#define TIME_OUT	(60 * 1000 * 1000)

void print_err(int ret, char *funct_name)
{
	fprintf(stderr, "Error %d in %s: %s\n", 
		ret, funct_name, espia_strerror(ret));
}

void print_err_exit(int ret, char *funct_name)
{
	print_err(ret, funct_name);
	exit(1);
}

#define check_ret(funct_name, ret_val) \
	do { \
		int _ret = ret_val; \
		if (_ret < 0) \
			print_err_exit(_ret, funct_name); \
	} while (0)


double elapsed_time(struct timeval *t0, struct timeval *t1)
{
	return (t1->tv_sec - t0->tv_sec) + (t1->tv_usec - t0->tv_usec) * 1e-6;
}

int espia_send(espia_t dev, char *buffer, int buffer_len, char *term)
{
	unsigned long len;
	struct timeval t0, t1;
	double elapsed;
	int ret, elapsed_ms;

	len = strlen(buffer);
	printf("Sending %2ld bytes: %s", len, buffer);
	check_ret("espia_ser_write", 
		  espia_ser_write(dev, buffer, &len, 10000, 0, 0));
	len = buffer_len;
	memset(buffer, 0, len);
	gettimeofday(&t0, NULL);
	ret = espia_ser_read_str(dev, buffer, &len, term, strlen(term),
				     TIME_OUT);
	gettimeofday(&t1, NULL);
	elapsed = elapsed_time(&t0, &t1);
	elapsed_ms = (int) (elapsed * 1e3 + 0.5);
	printf("Received %2ld bytes in %2d ms: %s", len, elapsed_ms, buffer);
	check_ret("espia_ser_read_str", ret);
	if (elapsed > 1) {
		fprintf(stderr, "Error: answer arrived %g sec!!\n", elapsed);
		exit(1);
	}
	return 0;
}

int espia_send_check(espia_t dev, char *buffer, int buffer_len, char *term,
		     char *resp)
{
	espia_send(dev, buffer, buffer_len, term);
	if (strcmp(resp, buffer) != 0) {
		fprintf(stderr, "Error: expecting %d bytes: %s", 
			(int) strlen(resp), resp);
		exit(1);
	}
	return 0;
}

int main(int argc, char *argv[])
{
	espia_t dev;
	unsigned long long i = 0;
	int nr, len;
	char buffer[1024], resp[1024], *term = "\r\n";

	nr = 0;
	if (argc > 1)
		nr = atoi(argv[1]);

	printf("Opening espia #%d\n", nr);
	check_ret("espia_open", espia_open(nr, &dev));
	
	printf("Flushing serial line\n");
	check_ret("espia_ser_flush", espia_ser_flush(dev));

	len = sizeof(buffer);

	while (1) {
		nr = i++ & 0xffff;
		sprintf(buffer, ">N%d\r\n", nr);
		espia_send_check(dev, buffer, len, term, "!OK\r\n");

		sprintf(buffer, ">N?\r\n");
		sprintf(resp, "!OK:%d\r\n", nr);
		espia_send_check(dev, buffer, len, term, resp);
	}	


	check_ret("espia_close", espia_close(dev));

	return 0;
}
