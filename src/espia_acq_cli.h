#ifndef ESPIA_TEST_CLI_H
#define ESPIA_TEST_CLI_H


struct cli_data {
	etest_t		 etest;
	int		 active_dev;
	void 		*lib_handle;  /* Or an array of handels? */
};


/*--------------------------------------------------------------------------
 * macro and inline functions
 *--------------------------------------------------------------------------*/

#define cli_data_from_context(c) ((struct cli_data *) context_app_data(c))

#if 0  
/* This it to replace dev_data_from_context().
   We should not use this! It breaks data privacy! Move it to lib ???
   The other option is to add device[] field in the cli_data, which will
   point to the same data and will be "hidden" */
static inline
struct device_data *dev_ptr_from_context(app_context_t context)
{
	struct cli_data *clidata = cli_data_from_context(context);
	int actdev = clidata->active_dev;
	/* Introduce dev_data_from_cli_data() instead of this below??? */
	return (actdev >= 0) ? &(clidata->etest->device[actdev]) : NULL;
}
#endif /* 0 */


static inline
int last_frame_available(struct img_frame_info *finfo)
{
	int avail;
	avail = !(scdxipci_is(INVALID, finfo->buffer_nr)   &&
		  scdxipci_is(INVALID, finfo->frame_nr)    &&
		  scdxipci_is(INVALID, finfo->round_count) &&
		  scdxipci_is(INVALID, finfo->acq_frame_nr));
	return avail;
}


/*--------------------------------------------------------------------------
 * function prototypes
 *--------------------------------------------------------------------------*/

int cli_test_init(app_context_t context, int argc, char *argv[]);
int cli_test_cleanup(app_context_t context);

err_funct test_error_funct;

app_funct cli_open_dev, cli_close_dev;
app_funct cli_select_dev, cli_dev_list;
app_funct cli_option_list, cli_get_option, cli_set_option, cli_set_debug_level;
app_funct cli_read_register, cli_write_register, cli_param_list, cli_get_param, cli_set_param;
app_funct cli_reset_link;
app_funct cli_serial_read, cli_serial_read_str, cli_serial_write, cli_serial_flush;
app_funct cli_serial_mmap_init, cli_serial_mmap_close, cli_serial_mmap_write, 
	  cli_serial_mmap_read;
app_funct cli_frame_post_op, cli_frame_post_op_list, cli_frame_post_op_list_active;
app_funct cli_buffer_alloc, cli_buffer_free, cli_buffer_list, cli_buffer_dump, cli_buffer_save;
app_funct cli_buffer_clear;
app_funct cli_buffer_img, cli_buffer_img_del, cli_buffer_img_del_all;
app_funct cli_buffer_img_rates, cli_buffer_img_norm;
app_funct cli_roi_set, cli_roi_reset, cli_sg_set, cli_dev_set;
app_funct cli_start_acq, cli_stop_acq, cli_acq_status;
app_funct cli_reg_acq_cb, cli_reg_status_cb, cli_reg_ser_rx_cb;
app_funct cli_unreg_cb, cli_unreg_all_cb, cli_cb_active, cli_cb_list;
app_funct cli_get_frame, cli_frame_addr;
app_funct cli_ccd_status, cli_get_stats;
app_funct cli_roi_acc_buffer;
app_funct cli_focla_open_dev, cli_focla_close_dev, cli_focla_read, cli_focla_write;
app_funct cli_focla_param_list, cli_focla_get_par, cli_focla_set_par;
app_funct cli_focla_sig_list;
app_funct cli_focla_pulse_start, cli_focla_pulse_stop, cli_focla_pulse_status;
app_funct cli_focla_serial_read, cli_focla_serial_read_str, cli_focla_serial_write, 
	  cli_focla_serial_flush;
app_funct cli_firmware_list;
app_funct cli_poll_sleep;
app_funct cli_get_version;
app_funct cli_plugin_reg, cli_plugin_list, cli_plugin_active, cli_plugin_delete,
          cli_plugin_output;
app_funct cli_edf_usr_header;
app_funct cli_nonblock_save_mode, cli_nonblock_frame_save, 
          cli_nonblock_save_stop;


#if 0  /* Moved to espia_test_lib_priv.h */
int reg_gen_cb(app_context_t context, struct espia_cb_data *cb_data,
	       struct cb_priv_data *priv_data, int user_act, int *cb_nr_ptr);
int unreg_gen_cb(app_context_t context, struct device_data *dev_data,
		 int cb_nr, int user_cb, int *cb_nr_ptr);
int gen_cb_funct(struct espia_cb_data *cb_data);

int gen_img_add(app_context_t context, int buffer_nr, img_cb *cb_funct, 
		struct img_frame_info *req_finfo, img_del *del_funct);
int gen_img_del(app_context_t context, int buffer_nr);

int roi_acc_buffer_init(app_context_t context, int nr_frames);
int roi_acc_buffer_install(app_context_t context);
int roi_acc_buffer_del(app_context_t context);

int set_gen_sg(app_context_t context, int img_config, 
	       struct espia_frame_dim *frame_dim, struct espia_roi *roi);
#endif /* 0 */

void print_finfo(app_context_t context, 
		 struct img_frame_info *req_finfo,
		 struct img_frame_info *cb_finfo, int ret);


/*--------------------------------------------------------------------------
 * the table with the CLI functions
 *--------------------------------------------------------------------------*/

extern struct funct_data cli_funcs[];


#endif /* ESPIA_TEST_CLI_H */
