/****************************************************************************
 * File:	scdxipci_test.c
 * $Header: /segfs/bliss/cvs/driver/ESRF/espia/src/scdxipci_test.c,v 2.13 2011/05/06 11:43:51 ahoms Exp $
 * Project:	SECAD Espia card Linux driver
 * Description:	Driver library test program
 * Author(s):	SECAD S.A.
 *		A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 ****************************************************************************/

#include "scdxipci_lib.h"
#include "app_framework.h"
#include "esrfdebug.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <unistd.h>

// the program name
char *progname;

#define dev_data_from_context(c) ((struct device_data *) context_app_data(c))
#define BUFFER_LIST_INC		10

/*--------------------------------------------------------------------------
 * application specific data
 *--------------------------------------------------------------------------*/

#define SCDXIPCI_TEST_ERR_BASE		(SCDXIPCI_ERR_BASE_LIB - 500)
#define SCDXIPCI_TEST_ERR_BUFFLIST	(SCDXIPCI_TEST_ERR_BASE - 1)
#define SCDXIPCI_TEST_ERR_BUFFNR	(SCDXIPCI_TEST_ERR_BASE - 2)


struct device_data {
	char *fname;
	scdxipci_t dev;
	struct img_buffer_info *buffers;
	unsigned long max_nr_buffer;
        unsigned long frame_size;
};

#define is_used(buffer)		((buffer)->ptr != NULL)


/*--------------------------------------------------------------------------
 * function prototypes
 *--------------------------------------------------------------------------*/

void usage();
err_funct error_funct;

app_funct driver_option;
app_funct read_register, write_register;
app_funct reset_link;
app_funct serial_read, serial_write;
app_funct set_frame_size;
app_funct buffer_map, buffer_unmap, buffer_list, frame_address;
app_funct start_acq, stop_acq, acq_active, get_frame;
app_funct ccd_status, get_stats, get_hw_info;

unsigned long first_buffer_nr(struct device_data *dev_data);

int buffer_unmap_base(app_context_t context, int buffer_nr);
int print_frame_info (app_context_t context, 
		      struct img_frame_info *finfo, int ret);

/*--------------------------------------------------------------------------
 * the table with the application functions
 *--------------------------------------------------------------------------*/

struct funct_data all_funcs[] = {
	{"Option",        driver_option, "Set/get a driver option",
	 {{"option",      PARAM_INT, "Option code [0=Debug, 1=FifoReset]"}, 
	  {"action",      PARAM_INT, "Action [1=RD, 2=WR, 3=RD/WR]"}, 
	  {"val",         PARAM_INT, "Value [used only in Write]"}, 
	  {NULL}}},
	{"ReadRegister",  read_register, "Read from a card register",
	 {{"reg_off",     PARAM_INT, "Register offset in bytes [0-255]"}, 
	  {NULL}}},
	{"WriteRegister", write_register, "Read/Write to a card register",
	 {{"reg_off",     PARAM_INT, "Register offset in bytes [0-255]"}, 
	  {"data",        PARAM_INT, "Value to write"}, 
	  {"mask",        PARAM_INT, "Mask of bits to modify"}, 
	  {NULL}}},
	{"ResetLink",     reset_link, "Reset the Aurora link",
	 {{NULL}}},
	{"SerialRead",    serial_read, "Read bytes from the serial line",
	 {{"nr_bytes",    PARAM_INT, "Number of bytes to read"}, 
	  {"term",        PARAM_STR, "Terminator string"}, 
	  {"timeout",     PARAM_INT, "Timeout in us [-1=BlockForever"}, 
	  {"raw",         PARAM_INT, "Print raw string (not ASCII)"}, 
	  {NULL}}},
	{"SerialWrite",   serial_write, "Write bytes to the serial line",
	 {{"string",      PARAM_STR, "String to write - can use \" or '"}, 
	  {"block_size",  PARAM_INT, "Nr. of bytes before a delay"}, 
	  {"delay",       PARAM_INT, "Time between writes, in us"}, 
	  {"block",       PARAM_INT, "Wait until end of TX [0=No, 1=Yes]"}, 
	  {NULL}}},
	{"FrameSize",     set_frame_size, "Set/get the frame size",
	 {{"frame_size",  PARAM_INT, "Nr of bytes per frame [-1=Get]"}, 
	  {NULL}}},
	{"BufferMap",     buffer_map, "Create and map an acq. buffer",
	 {{"nr_frames",   PARAM_INT, "Nr. of frames in buffer"}, 
	  {NULL}}},
	{"BufferUnmap",   buffer_unmap, "Unmap and free the acq. buffer",
	 {{"buffer_nr",   PARAM_INT, "Index of the buffer to unmap"}, 
	  {NULL}}},
	{"BufferList",    buffer_list, "Lists all the allocated buffers",
	 {{NULL}}},
	{"FrameAddress",  frame_address, "Get a specific frame address",
	 {{"buffer_nr",   PARAM_INT, "Index of the buffer"}, 
	  {"frame_nr",    PARAM_INT, "Index of the frame"}, 
	  {NULL}}},
	{"StartAcq",      start_acq, "Start an acq. of frames and block",
	 {{"buffer",      PARAM_INT, "Index of the first buffer"}, 
	  {"acq_frame_nr",PARAM_INT, "Nr. of frames to acq. [0=Infinite]"}, 
	  {"timeout",     PARAM_INT, "Timeout in us [-1=BlockForever]"}, 
	  {NULL}}},
	{"StopAcq",       stop_acq, "Stop the current acq. (if running)",
	 {{NULL}}},
	{"AcqActive",     acq_active, "Check if the acquisition is running",
	 {{NULL}}},
	{"GetFrame",      get_frame, "Get frame info, block if not ready",
	 {{"buffer",      PARAM_INT, "Index of the buffer [-1=Any]"}, 
	  {"frame_nr",    PARAM_INT, "Index of frame in buffer [-1=Any]"}, 
	  {"round_count", PARAM_INT, "Frame (over)write number [-1=Any]"}, 
	  {"acq_frame_nr",PARAM_INT, "Index of frame in acq [-1=Any]"}, 
	  {"acq_run_nr",  PARAM_INT, "Nr. of acq. runs"}, 
	  {"timeout",     PARAM_INT, "Timeout in us [-1=BlockForever]"}, 
	  {NULL}}},
	{"CcdStatus",     ccd_status, "Get CCD status, can block until change",
	 {{"timeout",     PARAM_INT, "Timeout in us [-1=BlockForever]"}, 
	  {NULL}}},
	{"GetStats",      get_stats, "Get the acquisition statistics",
	 {{NULL}}},
	{"GetHwInfo",     get_hw_info, "Get hardware related information",
	 {{NULL}}},
	{NULL}
};


/*--------------------------------------------------------------------------
 * voila the main program
 *--------------------------------------------------------------------------*/

int main(int argc, char **argv)
{
	char *ptr;
	int ret;
	app_context_t context;
	struct device_data dev_data;

	progname = argv[0];
	while ((ptr = strchr(progname, '/')) != NULL)
		progname = ++ptr;

	if (argc == 1)
		usage();

	context = alloc_context("scdxipcitest", argc, argv);
	if (context == CONTEXT_INVALID) {
		fprintf(stderr, "Error allocating context\n");
		exit(1);
	}
		
	dev_data.fname = argv[1];
	ret = scdxipci_open(dev_data.fname, &dev_data.dev);
	if (ret != SCDXIPCI_OK) {
		context_err(context, "Error opening %s: %s\n", dev_data.fname,
			    scdxipci_strerror(ret));
		exit(1);
	}

	dev_data.buffers = NULL;
	dev_data.max_nr_buffer = 0;
	dev_data.frame_size = SCDXIPCI_INVALID;
	scdxipci_frame_size(dev_data.dev, &dev_data.frame_size);

	init_context(context, all_funcs, error_funct, &dev_data);
	exec_application(context);

	while (dev_data.max_nr_buffer > 0) {
		ret = buffer_unmap_base(context, first_buffer_nr(&dev_data));
		if (ret < 0)
			context_err(context, "Error unmapping buffer: %s\n",
				    error_funct(context, NULL, ret));
	}

	ret = scdxipci_close(dev_data.dev);
	if (ret != SCDXIPCI_OK) {
		context_err(context, "Error closing %s: %s\n", dev_data.fname,
			    scdxipci_strerror(ret));
		ret = 1;
	}
	
	free_context(context);

	return ret;
}


/*--------------------------------------------------------------------------
 * usage
 *--------------------------------------------------------------------------*/

void usage()
{
	fprintf(stderr, " Usage: %s dev_fname\n", progname);
	exit(1);
}


/*--------------------------------------------------------------------------
 * gives the error associated to a return value, NULL means no error
 *--------------------------------------------------------------------------*/

static deb_const_str error_str_array[] = {
	{SCDXIPCI_TEST_ERR_BUFFLIST,	"Error (re)allocating buffer list"},
	{SCDXIPCI_TEST_ERR_BUFFNR,	"Invalid buffer index"},
};

INIT_STATIC_CONST_STR_ARRAY_PREFIX(error_strings, error_str_array, 
				   "SCDXIPCI_TEST");

char *error_funct(app_context_t context, struct funct_data *fdata,
		  int ret)
{
	if (ret >= SCDXIPCI_OK)
		return NULL;
	return GET_CONST_STR(error_strings, ret, scdxipci_strerror(ret));
}


/*--------------------------------------------------------------------------
 * driver option
 *--------------------------------------------------------------------------*/

int driver_option(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	int ret, option, action;

	option = pars[0].i;
	action = pars[1].i;
	ret = scdxipci_option(dev_data->dev, option, action, &pars[2].i);
	if ((ret == SCDXIPCI_OK) && (action & SCDXIPCI_OPT_RD)) 
		context_out(context, "Previous option %d value: %d (0x%x)\n", 
			    option, pars[2].i, pars[2].i);

	return ret;
}


/*--------------------------------------------------------------------------
 * read/write register
 *--------------------------------------------------------------------------*/

int read_register(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	int ret;
	unsigned int val;
	char binbuf[32 + 7 + 1];

	ret = scdxipci_read_register(dev_data->dev, pars[0].i, &val);
	if (ret == SCDXIPCI_OK)
		context_out(context, "Register 0x%02x value: %010u [%s]\n", 
			    pars[0].i, val, bin_repr(binbuf, val));

	return ret;
}

int write_register(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	int ret;
	unsigned int val = pars[1].i;
	char binbuf[32 + 7 + 1];

	ret = scdxipci_write_register(dev_data->dev, pars[0].i, &val,
				      pars[2].i);
	if (ret == SCDXIPCI_OK)
		context_out(context, "Register 0x%02x prev. value: %010u "
			    "[%s]\n", pars[0].i, val, bin_repr(binbuf, val));

	return ret;
}


/*--------------------------------------------------------------------------
 * reset link
 *--------------------------------------------------------------------------*/

int reset_link(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	return scdxipci_reset_link(dev_data->dev);
}


/*--------------------------------------------------------------------------
 * serial read/write
 *--------------------------------------------------------------------------*/

char *get_char_repr(char ch) 
{
	static char c[2] = {0, 0};
	switch ((c[0] = ch)) {
	case '\n': return "\\n";
	case '\r': return "\\r";
	case '\t': return "\\t";
	case '"':  return "\\""\"";
	default:   return c;
	}
}

int serial_read(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	unsigned long i, nr_bytes = pars[0].i;
	int ret, raw = pars[3].i;
	char *buffer, chr;

	buffer = NULL;
	if (nr_bytes > 0) {
		buffer = calloc(nr_bytes, 1);
		if (buffer == NULL) {
			context_err(context, "Error cannot allocate %ld "
				    "bytes!\n", nr_bytes);
			errno = ENOMEM;
			return -1;
		}
	}

	ret = scdxipci_ser_read(dev_data->dev, buffer, &nr_bytes, 
				pars[1].s, pars[1].len, pars[2].i);
	if (!buffer) {
		if (ret == SCDXIPCI_OK)
			context_out(context, "Available %ld byte(s).\n", 
				    nr_bytes);
		return ret;
	}

	if (ret == SCDXIPCI_OK) {
		context_out(context, "Received %ld byte(s)%c\n", nr_bytes,
			(nr_bytes > 0) ? ':' : '.');
		for (i = 0; i < nr_bytes; i++) {
			chr = buffer[i]; 
			if (!raw) {
				context_out(context, "%c", chr);
				continue;
			}
			if (i == 0)
				context_out(context, "\"");
			context_out(context, "%s", get_char_repr(chr));
			if (strchr("\r\n", chr) == NULL)
				continue;
			if ((chr == '\r') && (i < nr_bytes - 1) &&
			    (buffer[i + 1] == '\n'))
				continue;
			context_out(context, "\"\n\"");
		}				
	}

	free(buffer);
	return ret;
}

int serial_write(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	unsigned long nr_bytes = pars[0].len;
	int ret;
	char *action;

	ret = scdxipci_ser_write(dev_data->dev, pars[0].s, &nr_bytes,
				 pars[1].i, pars[2].i, pars[3].i);
	if (ret == SCDXIPCI_OK) {
		action = (pars[0].len > 0) ? "Transmited" : "Still to TX:";
		context_out(context, "%s %ld bytes.\n", action, nr_bytes);
	}

	return ret;
}

/*--------------------------------------------------------------------------
 * buffer frames
 *--------------------------------------------------------------------------*/

int set_frame_size(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	int ret;
	unsigned long frame_size = pars[0].i;
	
	ret = scdxipci_frame_size(dev_data->dev, &frame_size);
	if (ret != SCDXIPCI_OK)
		return ret;

	if (frame_size != dev_data->frame_size) {
		context_err(context, "Warning!!! buffer frames in driver "
			    "(%ld) was different than local (%ld)\n", frame_size,
			    dev_data->frame_size);
	} else
		context_out(context, "Previous frames per buffer: %ld\n", 
			    frame_size);
	if (pars[0].i != -1)
		dev_data->frame_size = pars[0].i;

	return SCDXIPCI_OK;
}


/*--------------------------------------------------------------------------
 * buffer map/unmap
 *--------------------------------------------------------------------------*/

int buffer_map(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	int ret, nr_frames, len, prot, aux;
	struct img_buffer_info *buffer;
	void *ptr;
	unsigned long nr;

	nr_frames = pars[0].i;
	prot =  PROT_READ | PROT_WRITE;
	ret = scdxipci_map_buffer(dev_data->dev, nr_frames, prot, MAP_SHARED, 
				  &nr);
	if (ret != SCDXIPCI_OK)
		return ret;

	while (nr >= dev_data->max_nr_buffer) {
		aux = dev_data->max_nr_buffer + BUFFER_LIST_INC;
		len = sizeof(*dev_data->buffers) * aux;
		ptr = realloc(dev_data->buffers, len);
		if (ptr == NULL)
			return SCDXIPCI_TEST_ERR_BUFFLIST;
		dev_data->buffers = (struct img_buffer_info *) ptr;
		len = BUFFER_LIST_INC * sizeof(*dev_data->buffers);
		memset(dev_data->buffers + dev_data->max_nr_buffer, 0, len);
		dev_data->max_nr_buffer = aux;
	}
		
	buffer = &dev_data->buffers[nr];
	buffer->nr = nr;
	buffer->ptr = NULL;
	ret = scdxipci_buffer_info(dev_data->dev, buffer);
	context_out(context, "Buffer #%ld, %ld frames of %ld bytes each, "
		    "alloc. at 0x%p\n", nr, buffer->nr_frames, 
		    buffer->frame_size, buffer->ptr);

	return SCDXIPCI_OK;
}


int buffer_unmap(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	int nr;

	nr = pars[0].i;
	if ((nr < 0) || (nr >= (int) dev_data->max_nr_buffer))
		return SCDXIPCI_TEST_ERR_BUFFNR;

	return buffer_unmap_base(context, nr);
}


int buffer_unmap_base(app_context_t context, int buffer_nr)
{
	struct device_data *dev_data = dev_data_from_context(context);
	struct img_buffer_info *buffer;
	int ret, len, used = 0;
	unsigned long nr, new_nr;
	void *ptr;

	ret = scdxipci_unmap_buffer(dev_data->dev, buffer_nr);
	if (ret != SCDXIPCI_OK)
		return ret;
	
	buffer = &dev_data->buffers[buffer_nr];
	len = buffer->nr_frames * buffer->frame_size;
	context_out(context, "Buffer #%d with %d bytes at 0x%p freed\n", 
		    buffer_nr, len, buffer->ptr);
	memset(buffer, 0, sizeof(*buffer));


	while (dev_data->max_nr_buffer > 0) {
		new_nr = dev_data->max_nr_buffer - BUFFER_LIST_INC;
		if (new_nr < 0)
			new_nr = 0;
		for (nr = new_nr; nr < dev_data->max_nr_buffer; nr++)
			if ((used = is_used(&dev_data->buffers[nr])))
				break;
		if (used)
			break;

		len = sizeof(*dev_data->buffers) * new_nr;
		ptr = realloc(dev_data->buffers, len);
		if ((ptr == NULL) && (len > 0)) {
			context_err(context, "Warning: could not realloc "
				    "buffer list (to %d bytes)\n", len);
			break;
		} else {
			dev_data->buffers = (struct img_buffer_info *) ptr;
			dev_data->max_nr_buffer = new_nr;
		}
	}

	return SCDXIPCI_OK;

}


/*--------------------------------------------------------------------------
 * buffer list
 *--------------------------------------------------------------------------*/

int buffer_list(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	unsigned long nr, nr_frames;
	struct img_buffer_info *buffer;

	if (scdxipci_is(INVALID, first_buffer_nr(dev_data))) {
		context_out(context, "No allocated buffer\n");
		return SCDXIPCI_OK;
	}

	context_out(context, 
		    " Buff #        Addr         Frames      Size       "
		    "Tot Size\n"
		    "---------------------------------------------------"
		    "--------\n");
	for (nr = 0; nr < dev_data->max_nr_buffer; nr++) {
		buffer = &dev_data->buffers[nr];
		if (!is_used(buffer))
			continue;
		nr_frames = buffer->nr_frames;
		context_out(context, "%5ld    %14p  %7ld   %10ld   %10ld\n", 
			    nr, buffer->ptr, nr_frames, buffer->frame_size, 
			    nr_frames * buffer->frame_size);
	}

	return SCDXIPCI_OK;
}


/*--------------------------------------------------------------------------
 * frame address
 *--------------------------------------------------------------------------*/

int frame_address(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	void *addr;
	int ret;

	ret = scdxipci_frame_address(dev_data->dev, pars[0].i, pars[1].i,
				     &addr);
	if (ret >= 0) {
		context_out(context, "Frame #%d of buffer #%d is at 0x%p\n",
			    pars[0].i, pars[1].i, addr);
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * start/stop/active acq.
 *--------------------------------------------------------------------------*/

int start_acq(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	struct img_frame_info finfo;
	int ret, nr = pars[0].i;
	if ((nr < 0) || (nr >= (int) dev_data->max_nr_buffer))
		return SCDXIPCI_TEST_ERR_BUFFNR;

	finfo.buffer_nr = dev_data->buffers[nr].nr;
	finfo.acq_frame_nr = pars[1].i;
	ret = scdxipci_start_dma(dev_data->dev, &finfo, pars[2].i);
	if (pars[2].i != 0)
		ret = print_frame_info(context, &finfo, ret);
	return ret;
}

int stop_acq(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	return scdxipci_stop_dma(dev_data->dev);
}

int acq_active(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	unsigned long acq_run_nr;
	int ret = scdxipci_dma_active(dev_data->dev, &acq_run_nr);
	if (ret >= 0) {
		context_out(context, "Acquisition %ld is %srunning\n", 
			    acq_run_nr, (ret == 0) ? "not " : "");
		ret = 0;
	}
	return ret;
}


/*--------------------------------------------------------------------------
 * get frame info
 *--------------------------------------------------------------------------*/

int get_frame(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	struct img_frame_info finfo;
	int ret, nr = pars[0].i;
	if ((nr < -1) || (nr >= (int) dev_data->max_nr_buffer) || 
	    scdxipci_is(INVALID, first_buffer_nr(dev_data)))
		return SCDXIPCI_TEST_ERR_BUFFNR;

	finfo.buffer_nr = (nr >= 0) ? dev_data->buffers[nr].nr : SCDXIPCI_ANY;
	finfo.frame_nr     = pars[1].i;
	finfo.round_count  = pars[2].i;
	finfo.acq_frame_nr = pars[3].i;
	finfo.acq_run_nr   = pars[4].i;
	ret = scdxipci_get_frame(dev_data->dev, &finfo, pars[5].i);
	return print_frame_info(context, &finfo, ret);
}

int print_frame_info(app_context_t context, struct img_frame_info *finfo,
		     int ret)
{
	double sec;
	if (ret == SCDXIPCI_OK) {
		sec = (double) finfo->time_us / 1e6;
		context_out(context, "Frame %ld of buffer #%ld (#%ld) ready "
			    "after %.3f s with %ld pixels\n", finfo->frame_nr,
			    finfo->buffer_nr, finfo->acq_frame_nr, sec, 
			    finfo->pixels);
	} 

	return ret;
}


/*--------------------------------------------------------------------------
 * CCD status
 *--------------------------------------------------------------------------*/

int ccd_status(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	int ret;
	unsigned char ccd_status;

	ret = scdxipci_ccd_status(dev_data->dev, &ccd_status, pars[0].i);
	if (ret == SCDXIPCI_OK)
		context_out(context, "CCD status: %d (0x%02x)\n", 
			    ccd_status, ccd_status);
	return ret;
}


/*--------------------------------------------------------------------------
 * first buffer number
 *--------------------------------------------------------------------------*/

unsigned long first_buffer_nr(struct device_data *dev_data)
{
	unsigned long nr;

	for (nr = 0; nr < dev_data->max_nr_buffer; nr++)
		if (is_used(&dev_data->buffers[nr]))
			return nr;

	return SCDXIPCI_INVALID;
}


/*--------------------------------------------------------------------------
 * get stats
 *--------------------------------------------------------------------------*/

#define STAT_DESC_LEN		40

#define print_stat(context, desc, format, val)		\
	context_out(context, "%-*s " format "\n", STAT_DESC_LEN, \
		    desc ":", val)

#define print_dev_stat(context, nr, desc, format, val)			\
	context_out(context, "Dev. #%d %-*s " format "\n", nr, STAT_DESC_LEN-8,\
		    desc ":", val)

int get_stats(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	int ret, i;
	struct scdxipci_stats stats;
	struct scdxipci_dev_stats *dev_stats;
	unsigned long cnt, page_size;

	ret = scdxipci_get_stats(dev_data->dev, &stats);
	if (ret != SCDXIPCI_OK)
		return ret;

	context_out(context, "Acquisition statistics:\n\n");

	// SG data info
	dev_stats = stats.dev_stats;
	for (i = 0; i < SCDXIPCI_NR_DEV_STATS; i++, dev_stats++) {
		cnt = dev_stats->sg_avail_count;
		if (cnt == 0)
			continue;

		print_dev_stat(context, i, "total of SG table switches",  
			       "%ld", cnt);
		print_dev_stat(context, i, "min. nr. of avail. SG tables", 
			       "%ld", dev_stats->sg_avail_min);
		print_dev_stat(context, i, "ave. nr. of avail. SG tables", 
			       "%.1f", (double) dev_stats->sg_avail_acc / cnt);
	}

	// copy frame info
	cnt = stats.copy_count;
	print_stat(context, "Nr. of buffer pages", "%ld", 
		   stats.nr_pages);
	print_stat(context, "Nr. of buffer pages 64-bit", "%ld", 
		   stats.nr_pages64);
	print_stat(context, "Total of frame copies", "%ld", cnt);
	if (cnt > 0) {
		print_stat(context, "Average copy nr. of pages", "%lld", 
			   stats.copy_pages / cnt);
		print_stat(context, "Average copy time", "%lld usec",
			   stats.copy_usec / cnt);
	}
	page_size = getpagesize();
	if (stats.copy_usec > 0) 
		print_stat(context, "Average copy speed", "%lld MB/s",
			   stats.copy_pages * page_size / stats.copy_usec);
	
	// get acq_frame_nr info
	cnt = stats.afn_call;
	print_stat(context, "Total of AFN calls", "%ld", cnt);
	if (cnt > 0) {
		cnt = stats.afn_count;
		print_stat(context, "Total of AFN searches", "%ld", cnt);
	}
	if (cnt > 0) {
		print_stat(context, "Average AFN time", "%.2f usec",
			   (double) stats.afn_usec / cnt);
		print_stat(context, "Average AFN iters", "%.2f", 
			   (double) stats.afn_iter / cnt);
		print_stat(context, "Max AFN iters", "%ld", 
			   stats.afn_max_iter);
		print_stat(context, "Average AFN steps", "%.2f", 
			   (double) stats.afn_step / cnt);
		print_stat(context, "Max AFN steps", "%ld", 
			   stats.afn_max_step);
	}

	return SCDXIPCI_OK;
}


/*--------------------------------------------------------------------------
 * get hw info
 *--------------------------------------------------------------------------*/

#define HW_INFO_DESC_LEN		20

#define print_hw_info(context, desc, format, val)		    \
	context_out(context, "%-*s " format "\n", HW_INFO_DESC_LEN, \
		    desc ":", val)
#define print_hw_info_mod(context, desc, format, mod, val)	    \
	context_out(context, "%-*s " format "\n", HW_INFO_DESC_LEN, \
		    desc ":", mod, val)
#define print_hw_info2(context, desc, format, val, format2, val2)   \
	context_out(context, "%-*s " format " " format2 "\n",	    \
		    HW_INFO_DESC_LEN, desc ":", val, val2)

int get_hw_info(app_context_t context, union param *pars)
{
	struct device_data *dev_data = dev_data_from_context(context);
	int ret, action, mod;
	struct scdxipci_hw_info hw_info;
	struct scdxipci_board_id_info *board_id_info = &hw_info.board_id;
	struct scdxipci_firmware_info *firmware_info = &hw_info.firmware;
	struct scdxipci_card_id card_id;
	char *str;

	action = SCDXIPCI_HW_INFO_GET;
	memset(&hw_info, 0, sizeof(hw_info));
	memset(&card_id, 0, sizeof(card_id));
	board_id_info->len = sizeof(card_id);
	board_id_info->ptr = &card_id;
	ret = scdxipci_hw_info(dev_data->dev, action, &hw_info);
	if (ret != SCDXIPCI_OK)
		return ret;

	context_out(context, "Hardware information:\n\n");

	// basic hw info
	switch (hw_info.board_type) {
	case SCDXIPCI_ESPIA_PCI:     str = "Espia-PCI";     break;
	case SCDXIPCI_ESPIA_EXPRESS: str = "Espia-Express"; break;
	default:                     str = "Unknown";
	}
	print_hw_info(context, "Board Type",  "%s", str);
	print_hw_info(context, "Board Index", "%d", hw_info.board_idx);

	switch (hw_info.dev_type) {
	case SCDXIPCI_DEV_1_FO :     str = "1 Fiber-Optic link";  break;
	default:                     str = "Unknown";
	}
	print_hw_info(context, "Device Type",  "%s", str);
	print_hw_info(context, "Device Index", "%d", hw_info.dev_idx);

	// board id info
	print_hw_info(context, "Board ID Start",  "%d", board_id_info->start);
	print_hw_info(context, "Board ID Length", "%d", board_id_info->len);

	mod = scdxipci_ser_nr_len(board_id_info->ser_nr);
	print_hw_info_mod(context, "Serial Number",       "%0*d", mod, 
			  board_id_info->ser_nr);
	
	str = (card_id.CODE_TEST == TESTEE) ? "TESTED" : "PROTO";
	print_hw_info2(context, "Test Code", "0x%08x", card_id.CODE_TEST,
		       "(%s)", str);
	str = board_id_info->crc_ok ? "OK" : "BAD";
	print_hw_info2(context, "CRC",       "0x%08x", card_id.CRC,
		       "(%s)", str);
	
	// firmware info
	print_hw_info(context, "Firmware Start",  "%d", firmware_info->start);
	print_hw_info(context, "Firmware Length", "%d", firmware_info->len);
	print_hw_info(context, "Firmware Checksum","0x%04x", 
		      firmware_info->checksum);
	print_hw_info(context, "Firmware Name",   "%s", firmware_info->name);

	return SCDXIPCI_OK;
}
