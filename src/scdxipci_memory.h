/****************************************************************************
 * File:	scdxipci_memory.h
 * $Header: /segfs/bliss/cvs/driver/ESRF/espia/src/scdxipci_memory.h,v 2.1 2010/12/21 14:50:37 ahoms Exp $
 * Project:	SECAD Espia card Linux driver
 * Description:	Memory related routines
 * Author(s):	A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 *		S. Petitdemange, BLISS, ESRF (petitdem@esrf.fr)
 ****************************************************************************/

#ifndef _SCDXIPCI_MEMORY_H
#define _SCDXIPCI_MEMORY_H

#ifdef __x86_64__

/*--------------------------------------------------------------------------
 * SSE2 Memory Copy 
 *
 * Copyright(C) 2006, William Chan
 * All rights reserved.
 * 
 *   Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   
 *   1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   2) Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   3) Redistributions of source code must be provided at free of charge.
 *   4) Redistributions in binary forms must be provided at free of charge.
 *   5) Redistributions of source code within another distribution must be
 *     provided at free of charge including the distribution which is
 *     redistributing the source code. Also, the distribution which is
 *     redistributing the source code must have its source code
 *     redistributed as well.
 *   6) Redistribution of binary forms within another distribution must be
 *     provided at free of charge.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER / CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *--------------------------------------------------------------------------*/
 
static inline
void aligned_memcpy_sse2(void* dest, const void* src, 
			 const unsigned long size)
{
	long d0, d1, d2;

	__asm__ __volatile__(						
		/* %0: counter (RBX), %1: dest (RDI), %2: src (RSI) */	
		/* divide counter by 128 (8 * 128bit registers) */
		"  shrq $7, %0\n"
		"loop_copy:\n"
		/* SSE2 prefetch */
		"  prefetchnta 128(%2)\n"
		"  prefetchnta 160(%2)\n"
		"  prefetchnta 192(%2)\n"
		"  prefetchnta 224(%2)\n"
		/* move data from src to registers */
		"  movdqa 0(%2), %%xmm0\n"
		"  movdqa 16(%2), %%xmm1\n"
		"  movdqa 32(%2), %%xmm2\n"
		"  movdqa 48(%2), %%xmm3\n"
		"  movdqa 64(%2), %%xmm4\n"
		"  movdqa 80(%2), %%xmm5\n"
		"  movdqa 96(%2), %%xmm6\n"
		"  movdqa 112(%2), %%xmm7\n"
		/* move data from registers to dest */
		"  movntdq %%xmm0, 0(%1)\n"
		"  movntdq %%xmm1, 16(%1)\n"
		"  movntdq %%xmm2, 32(%1)\n"
		"  movntdq %%xmm3, 48(%1)\n"
		"  movntdq %%xmm4, 64(%1)\n"
		"  movntdq %%xmm5, 80(%1)\n"
		"  movntdq %%xmm6, 96(%1)\n"
		"  movntdq %%xmm7, 112(%1)\n"
		/* update pointers and counter and loop */
		"  addq $128, %1\n"
		"  addq $128, %2\n"
		"  decq %0\n"
		"  jnz loop_copy\n"
		: "=&b" (d0), "=&D" (d1), "=&S" (d2)
		: "0" (size), "1" (dest), "2" (src)
		: "memory");

}

#define scdxipci_copy_page(dst, src) aligned_memcpy_sse2(dst, src, PAGE_SIZE)

#else  /* ! __x86_64__ */

#define scdxipci_copy_page(dst, src) copy_page(dst, src)

#endif /* __x86_64__ */


#endif /* _SCDXIPCI_MEMORY_H */
