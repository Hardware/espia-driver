/****************************************************************************
 * File:	focla_lib.c
 * $Header: /segfs/bliss/cvs/driver/ESRF/espia/src/focla_lib.c,v 2.12 2017/11/15 12:19:30 ahoms Exp $
 * Project:	SECAD Espia card Linux driver
 * Description:	Fiber Optic Camera Link Adapter (FOCLA) library source code
 * Author(s):	SECAD S.A.
 *		A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 ****************************************************************************/

#define _GNU_SOURCE

#include "esrfdebug.h"
#include "focla_lib.h"
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#define FOCLA_ESCAPE		0x25
#define FOCLA_READ(reg)		(0x40 | ((reg) & 0x0f))
#define FOCLA_WRITE(reg)	(0x00 | ((reg) & 0x0f))

#define FOCLA_TIMEOUT		10000 // usec
#define FOCLA_BLOCK		SCDXIPCI_TX_BLOCK


/*--------------------------------------------------------------------------
 * rcs id
 *--------------------------------------------------------------------------*/

static char rcs_id[] __attribute__((unused)) = 
	"$Header: /segfs/bliss/cvs/driver/ESRF/espia/src/focla_lib.c,v 2.12 2017/11/15 12:19:30 ahoms Exp $"; 


/*--------------------------------------------------------------------------
 * basic constants
 *--------------------------------------------------------------------------*/

enum {
	FOCLA_SIG_CC1,
	FOCLA_SIG_CC2,
	FOCLA_SIG_CC3,
	FOCLA_SIG_CC4,
	FOCLA_SIG_OUT1,
	FOCLA_SIG_OUT2,
	FOCLA_SIG_OUT3,
	FOCLA_SIG_TRIG,
	FOCLA_NR_SIG,
};


/*--------------------------------------------------------------------------
 * the library data associated to each device
 *--------------------------------------------------------------------------*/

struct focla_dev;

struct pulse_data {
	struct focla_dev       *pdev;
	int			cam_nr;
	int			sig_nr;
	int			param;
	pthread_t		thread;
	int			polarity;
	int			stage_width_us[FOCLA_PULSE_MAX_NR_STAGE];
	int			nr_stage;
	int			nr_pulse;
	int			curr_pulse;
	int			curr_stage;
};

struct focla_dev {
	espia_t			dev;
	pthread_mutex_t		lock;
	struct pulse_data	cam_pulse_data[FOCLA_NR_CAM][FOCLA_NR_SIG];
};

static struct focla_dev devices[FOCLA_MAX_NR_DEV];

#define dev_from_pdev(pdev)			\
	((pdev) - devices)

static pthread_mutex_t devices_lock = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
static int devices_inited = 0;


/*--------------------------------------------------------------------------
 * helpers
 *--------------------------------------------------------------------------*/

#define check_serial_put(ret, len, req_len, pdev)			\
	do {								\
		if (((ret) == ESPIA_OK) && ((len) != (req_len)))	\
			(ret) = FOCLA_ERR_SERIAL;			\
		if ((ret) != ESPIA_OK) {				\
			put_pdev(pdev);					\
			return (ret);					\
		}							\
	} while (0)


#define check_val_put(v, mn, mx, err, pdev)				\
	do {								\
		if (((v) < (mn)) || ((v) >= (mx))) {			\
			if (pdev)					\
				put_pdev(pdev);				\
			return (err); 					\
		}							\
	} while (0)

#define check_val(v, mn, mx, err)					\
	check_val_put(v, mn, mx, err, NULL)

#define check_reg(reg, pdev)						\
	check_val_put(reg, 0, FOCLA_NR_REG, FOCLA_ERR_REGNR, pdev)

#define check_param(param)						\
	check_val(param, 0, focla_param_table_len, FOCLA_ERR_PARAM)

#define check_ptr_put(ptr, pdev)					\
	do {								\
		if ((ptr) == NULL) {					\
			if ((pdev) != NULL)				\
				put_pdev(pdev);				\
			return FOCLA_ERR_PTR;				\
		}							\
	} while (0)

#define check_ptr(ptr)							\
	check_ptr_put(ptr, NULL)

#define check_cam_put(cam_nr, pdev)					\
	check_val_put(cam_nr, FOCLA_CAM_1, FOCLA_NR_CAM,		\
		      FOCLA_ERR_CAMNR, pdev)

#define check_cam(cam_nr)						\
	check_cam_put(cam_nr, NULL)

#define check_sig_put(sig_nr, pdev)					\
	check_val_put(sig_nr, 0, FOCLA_NR_SIG, FOCLA_ERR_SIGNR, pdev);

#define check_sig(sig_nr)						\
	check_sig_put(sig_nr, NULL)

#define check_cam_sig_put(cam_nr, sig_nr, pdev)				\
	do {								\
		check_cam_put(cam_nr, pdev);				\
		check_sig_put(sig_nr, pdev);				\
	} while (0)

#define focla_pulse_data(pdev, cam_nr, sig_nr)				\
	(&(pdev)->cam_pulse_data[cam_nr][sig_nr])

#define endless_pulse(pulse_data)					\
	 ((pulse_data)->nr_pulse == 0)
	
#define pulse_data_active(pulse_data)					\
	((pulse_data)->thread &&					\
	 (endless_pulse(pulse_data) ||					\
	  ((pulse_data)->curr_pulse < (pulse_data)->nr_pulse)))

#define pulse_width_is_wait_frame(width)				\
	((unsigned long) (width) == FOCLA_PULSE_WAIT_FRAME)


/*--------------------------------------------------------------------------
 * error messages
 *--------------------------------------------------------------------------*/

static deb_const_str error_str_array[] = {
	{FOCLA_OK,		"No error"},
	{FOCLA_ERR_ESPIADEV,	"Invalid ESPIA dev"},
	{FOCLA_ERR_DEVOPEN,	"FOCLA dev already open"},
	{FOCLA_ERR_NODEV,	"Invalid FOCLA dev: use focla_open"},
	{FOCLA_ERR_SERIAL,	"Serial command/response xfer incomplete"},
	{FOCLA_ERR_REGNR,	"Invalid FOCLA register"},
	{FOCLA_ERR_REGVAL,	"Invalid FOCLA register value"},
	{FOCLA_ERR_PARAM,	"Invalid FOCLA parameter"},
	{FOCLA_ERR_PTR,		"Invalid pointer"},
	{FOCLA_ERR_CAMNR,	"Invalid FOCLA cam. nr"},
	{FOCLA_ERR_SIGNR,	"Invalid FOCLA sig. nr"},
	{FOCLA_ERR_IONR,	"Invalid FOCLA IO type nr"},
	{FOCLA_ERR_PULSEACT,	"FOCLA CL CC pulse already active"},
	{FOCLA_ERR_PULSEPAR,	"Invalid FOCLA CL CC pulse parameter(s)"},
};

INIT_STATIC_CONST_STR_ARRAY_PREFIX(error_strings, error_str_array, "FOCLA");

char *focla_strerror(int error)
{
	return GET_CONST_STR(error_strings, error, espia_strerror(error));
}


/*--------------------------------------------------------------------------
 * device array initialization
 *--------------------------------------------------------------------------*/

static inline void check_devices_init()
{
	int i;
	struct focla_dev *pdev;
	pthread_mutexattr_t mutex_attr;

	pthread_mutex_lock(&devices_lock);

	if (!devices_inited) {
		pthread_mutexattr_init(&mutex_attr);
		pthread_mutexattr_settype(&mutex_attr, 
					  PTHREAD_MUTEX_RECURSIVE);
		
		pdev = devices;
		for (i = 0; i < FOCLA_MAX_NR_DEV; i++, pdev++) {
			pthread_mutex_init(&pdev->lock, &mutex_attr);
			pdev->dev = ESPIA_DEV_INVAL;
		}

		pthread_mutexattr_destroy(&mutex_attr);

		devices_inited = 1;
	}

	pthread_mutex_unlock(&devices_lock);
}


/*--------------------------------------------------------------------------
 * get / put
 *--------------------------------------------------------------------------*/

static inline int lock_dev(struct focla_dev *pdev)
{
	return pthread_mutex_lock(&pdev->lock);
}

static inline int unlock_dev(struct focla_dev *pdev)
{
	return pthread_mutex_unlock(&pdev->lock);
}

static inline struct focla_dev *get_pdev(focla_t dev)
{
	struct focla_dev *pdev = &devices[dev];

	if ((dev < 0) || (dev >= FOCLA_MAX_NR_DEV))
		return NULL;

	check_devices_init();

	lock_dev(pdev);

	if (pdev->dev == ESPIA_DEV_INVAL) {
		pthread_mutex_unlock(&pdev->lock);
		return NULL;
	}

	return pdev; 
}

static inline void put_pdev(struct focla_dev *pdev)
{
	unlock_dev(pdev);
}


/*--------------------------------------------------------------------------
 * open / close
 *--------------------------------------------------------------------------*/

int focla_open(espia_t espia_dev, focla_t *dev_ptr)
{
	int cam_nr, sig_nr, param, ret = FOCLA_OK;
	focla_t dev;
	struct focla_dev *pdev;
	struct pulse_data *pulse_data;

	if ((espia_dev < 0) || (espia_dev >= FOCLA_MAX_NR_DEV))
		return FOCLA_ERR_ESPIADEV;

	check_devices_init();

	dev = espia_dev;
	pdev = &devices[dev];

	lock_dev(pdev);

	if (pdev->dev != ESPIA_DEV_INVAL) {
		ret = FOCLA_ERR_DEVOPEN;
		goto unlock;
	}

	pdev->dev = espia_dev;

	// init pulse data
	for (cam_nr = FOCLA_CAM_1; cam_nr < FOCLA_NR_CAM; cam_nr++) {
		for (sig_nr = 0; sig_nr < FOCLA_NR_SIG; sig_nr++) {
			ret = focla_get_sig_param(cam_nr, sig_nr);
			if (ret < 0)
				goto unlock;
			param = ret;

			pulse_data = focla_pulse_data(pdev, cam_nr, sig_nr);
			memset(pulse_data, 0, sizeof(*pulse_data));
			pulse_data->pdev   = pdev;
			pulse_data->cam_nr = cam_nr;
			pulse_data->sig_nr = sig_nr;
			pulse_data->param  = param;
		}
	}
	*dev_ptr = dev;
	ret = 0;

 unlock:
	unlock_dev(pdev);

	return ret;
}

int focla_close(focla_t dev)
{
	int cam_nr, sig_nr;
	struct focla_dev *pdev = get_pdev(dev);
	if (pdev == NULL)
		return FOCLA_ERR_NODEV;

	// stop pulse threads
	for (cam_nr = FOCLA_CAM_1; cam_nr < FOCLA_NR_CAM; cam_nr++)
		for (sig_nr = 0; sig_nr < FOCLA_NR_SIG; sig_nr++) 
			focla_sig_pulse_stop(dev, cam_nr, sig_nr);

	pdev->dev = ESPIA_DEV_INVAL;
	put_pdev(pdev);

	return FOCLA_OK;
}


/*--------------------------------------------------------------------------
 * lock / unlock
 *--------------------------------------------------------------------------*/

int focla_lock(focla_t dev)
{
	struct focla_dev *pdev = get_pdev(dev);
	if (pdev == NULL)
		return FOCLA_ERR_NODEV;
	return FOCLA_OK;
}

int focla_unlock(focla_t dev)
{
	struct focla_dev *pdev = get_pdev(dev);
	if (pdev == NULL)
		return FOCLA_ERR_NODEV;
	put_pdev(pdev);
	put_pdev(pdev);
	return FOCLA_OK;
}


/*--------------------------------------------------------------------------
 * register read / write
 *--------------------------------------------------------------------------*/

int focla_read_reg(focla_t dev, int reg, unsigned char *val_ptr)
{
	char write_cmd[2] = { FOCLA_ESCAPE,  FOCLA_READ(reg) };
	unsigned long len, req_len;
	int ret;
	struct focla_dev *pdev = get_pdev(dev);
	if (pdev == NULL)
		return FOCLA_ERR_NODEV;

	check_reg(reg, pdev);

	len = req_len = sizeof(write_cmd);
	ret = espia_ser_write(pdev->dev, write_cmd, &len, 1, 0, FOCLA_BLOCK);
	check_serial_put(ret, len, req_len, pdev);

	len = req_len = 1;
	ret = espia_ser_read(pdev->dev, (char *) val_ptr, &len, FOCLA_TIMEOUT);
	check_serial_put(ret, len, req_len, pdev);

	put_pdev(pdev);
	return ret;
}


int focla_write_reg(focla_t dev, int reg, unsigned char val)
{
	char write_cmd[3] = { FOCLA_ESCAPE,  FOCLA_WRITE(reg), val };
	unsigned long len, req_len;
	int ret;
	struct focla_dev *pdev = get_pdev(dev);
	if (pdev == NULL)
		return FOCLA_ERR_NODEV;

	check_reg(reg, pdev);

	len = req_len = sizeof(write_cmd);
	ret = espia_ser_write(pdev->dev, write_cmd, &len, 1, 0, FOCLA_BLOCK);
	check_serial_put(ret, len, req_len, pdev);

	put_pdev(pdev);
	return ret;
}


/*--------------------------------------------------------------------------
 * parameter get / set
 *--------------------------------------------------------------------------*/

int focla_get_bit(focla_t dev, int reg, 
		  unsigned char mask, int shift, int *val_ptr)
{
	unsigned char aux;
	int ret;

	check_ptr(val_ptr);

	ret = focla_read_reg(dev, reg, &aux);
	if (ret != FOCLA_OK)
		return ret;

	*val_ptr = (aux & mask) >> shift;
	return FOCLA_OK;
}

int focla_set_bit(focla_t dev, int reg, 
		  unsigned char mask, int shift, int val)
{
	unsigned char aux;
	int ret;
	struct focla_dev *pdev;

	val <<= shift;
	if (((mask & 0xff) != mask) || ((val & mask) != val))
		return FOCLA_ERR_REGVAL;

	pdev = get_pdev(dev);
	if (pdev == NULL)
		return FOCLA_ERR_NODEV;

	ret = focla_read_reg(dev, reg, &aux);
	if (ret != FOCLA_OK)
		goto put;

	aux = (aux & ~mask) | val;
	ret = focla_write_reg(dev, reg, aux);
 put:
	put_pdev(pdev);
	return ret;
}


/*--------------------------------------------------------------------------
 * parameter table
 *--------------------------------------------------------------------------*/

enum {
	FOCLA_PARAM_TRIG_MODE,
	FOCLA_PARAM_TRIG,
	FOCLA_PARAM_CAM_SEL,
	FOCLA_PARAM_TEST_IMAGE,
	FOCLA_PARAM_CL1_PIX_PACK,
	FOCLA_PARAM_CL2_PIX_PACK,
	FOCLA_PARAM_CL1_CC1,
	FOCLA_PARAM_CL1_CC2,
	FOCLA_PARAM_CL1_CC3,
	FOCLA_PARAM_CL1_CC4,
	FOCLA_PARAM_CL2_CC1,
	FOCLA_PARAM_CL2_CC2,
	FOCLA_PARAM_CL2_CC3,
	FOCLA_PARAM_CL2_CC4,
	FOCLA_PARAM_CL1_IN1,
	FOCLA_PARAM_CL1_IN2,
	FOCLA_PARAM_CL1_IN3,
	FOCLA_PARAM_CL2_IN1,
	FOCLA_PARAM_CL2_IN2,
	FOCLA_PARAM_CL2_IN3,
	FOCLA_PARAM_CL1_OUT1,
	FOCLA_PARAM_CL1_OUT2,
	FOCLA_PARAM_CL1_OUT3,
	FOCLA_PARAM_CL2_OUT1,
	FOCLA_PARAM_CL2_OUT2,
	FOCLA_PARAM_CL2_OUT3,
};

#define FOCLA_IO_MASK_SHIFT(cam, x)			\
	FOCLA_IO_MASK(cam, x),	FOCLA_IO_SHIFT(cam, x)
	
static struct espia_param focla_param_table[] = {
	{"TRIG_MODE",			FOCLA_REG_CTRL, 
	 FOCLA_CTRL_TRIG_MODE_MASK,	FOCLA_CTRL_TRIG_MODE_SHIFT, 
	 "Trigger mode [0=Cont. 1=Single]"},
	{"TRIG",			FOCLA_REG_CTRL, 
	 FOCLA_CTRL_TRIG_MASK,		FOCLA_CTRL_TRIG_SHIFT, 
	 "Trigger the FOCLA in Single TRIG_MODE"},
	{"CAM_SEL",			FOCLA_REG_CTRL,
	 FOCLA_CTRL_CAM_SEL_MASK,	FOCLA_CTRL_CAM_SEL_SHIFT,
	 "Camera select [0=1, 1=2]"},
	{"TEST_IMAGE",			FOCLA_REG_CTRL,
	 FOCLA_CTRL_TEST_IMAGE_MASK,	FOCLA_CTRL_TEST_IMAGE_SHIFT,
	 "Activate the test image"},
	{"CL1_PIX_PACK", 		FOCLA_REG_PIX_PACK,
	 FOCLA_PIX_PACK_MASK(FOCLA_CAM_1), FOCLA_PIX_PACK_SHIFT(FOCLA_CAM_1),
	 "Camera 1 pixel pack"},
	{"CL2_PIX_PACK", 		FOCLA_REG_PIX_PACK,
	 FOCLA_PIX_PACK_MASK(FOCLA_CAM_2), FOCLA_PIX_PACK_SHIFT(FOCLA_CAM_2),
	 "Camera 2 pixel pack"},
	{"CL1_CC1", 			FOCLA_REG_CL_CC(FOCLA_CAM_1),
	 FOCLA_CC_MASK(FOCLA_CC_1),	FOCLA_CC_SHIFT(FOCLA_CC_1),
	 "Camera 1 CC1 signal"},
	{"CL1_CC2", 			FOCLA_REG_CL_CC(FOCLA_CAM_1),
	 FOCLA_CC_MASK(FOCLA_CC_2),	FOCLA_CC_SHIFT(FOCLA_CC_2),
	 "Camera 1 CC2 signal"},
	{"CL1_CC3", 			FOCLA_REG_CL_CC(FOCLA_CAM_1),
	 FOCLA_CC_MASK(FOCLA_CC_3),	FOCLA_CC_SHIFT(FOCLA_CC_3),
	 "Camera 1 CC3 signal"},
	{"CL1_CC4", 			FOCLA_REG_CL_CC(FOCLA_CAM_1),
	 FOCLA_CC_MASK(FOCLA_CC_4),	FOCLA_CC_SHIFT(FOCLA_CC_4),
	 "Camera 1 CC4 signal"},
	{"CL2_CC1", 			FOCLA_REG_CL_CC(FOCLA_CAM_2),
	 FOCLA_CC_MASK(FOCLA_CC_1),	FOCLA_CC_SHIFT(FOCLA_CC_1),
	 "Camera 2 CC1 signal"},
	{"CL2_CC2", 			FOCLA_REG_CL_CC(FOCLA_CAM_2),
	 FOCLA_CC_MASK(FOCLA_CC_2),	FOCLA_CC_SHIFT(FOCLA_CC_2),
	 "Camera 2 CC2 signal"},
	{"CL2_CC3", 			FOCLA_REG_CL_CC(FOCLA_CAM_2),
	 FOCLA_CC_MASK(FOCLA_CC_3),	FOCLA_CC_SHIFT(FOCLA_CC_3),
	 "Camera 2 CC3 signal"},
	{"CL2_CC4", 			FOCLA_REG_CL_CC(FOCLA_CAM_2),
	 FOCLA_CC_MASK(FOCLA_CC_4),	FOCLA_CC_SHIFT(FOCLA_CC_4),
	 "Camera 2 CC4 signal"},
	{"CL1_IN1", 			FOCLA_REG_IO_CTRL(FOCLA_IO_IN),
	 FOCLA_IO_MASK_SHIFT(FOCLA_CAM_1, FOCLA_IO_1),
	 "Camera 1 ext. input 1"},
	{"CL1_IN2", 			FOCLA_REG_IO_CTRL(FOCLA_IO_IN),
	 FOCLA_IO_MASK_SHIFT(FOCLA_CAM_1, FOCLA_IO_2),
	 "Camera 1 ext. input 2"},
	{"CL1_IN3", 			FOCLA_REG_IO_CTRL(FOCLA_IO_IN),
	 FOCLA_IO_MASK_SHIFT(FOCLA_CAM_1, FOCLA_IO_3),
	 "Camera 1 ext. input 3"},
	{"CL2_IN1", 			FOCLA_REG_IO_CTRL(FOCLA_IO_IN),
	 FOCLA_IO_MASK_SHIFT(FOCLA_CAM_2, FOCLA_IO_1),
	 "Camera 2 ext. input 1"},
	{"CL2_IN2", 			FOCLA_REG_IO_CTRL(FOCLA_IO_IN),
	 FOCLA_IO_MASK_SHIFT(FOCLA_CAM_2, FOCLA_IO_2),
	 "Camera 2 ext. input 2"},
	{"CL2_IN3", 			FOCLA_REG_IO_CTRL(FOCLA_IO_IN),
	 FOCLA_IO_MASK_SHIFT(FOCLA_CAM_2, FOCLA_IO_3),
	 "Camera 2 ext. input 3"},
	{"CL1_OUT1", 			FOCLA_REG_IO_CTRL(FOCLA_IO_OUT),
	 FOCLA_IO_MASK_SHIFT(FOCLA_CAM_1, FOCLA_IO_1),
	 "Camera 1 ext output 1"},
	{"CL1_OUT2", 			FOCLA_REG_IO_CTRL(FOCLA_IO_OUT),
	 FOCLA_IO_MASK_SHIFT(FOCLA_CAM_1, FOCLA_IO_2),
	 "Camera 1 ext output 2"},
	{"CL1_OUT3", 			FOCLA_REG_IO_CTRL(FOCLA_IO_OUT),
	 FOCLA_IO_MASK_SHIFT(FOCLA_CAM_1, FOCLA_IO_3),
	 "Camera 1 ext output 3"},
	{"CL2_OUT1", 			FOCLA_REG_IO_CTRL(FOCLA_IO_OUT),
	 FOCLA_IO_MASK_SHIFT(FOCLA_CAM_2, FOCLA_IO_1),
	 "Camera 2 ext output 1"},
	{"CL2_OUT2", 			FOCLA_REG_IO_CTRL(FOCLA_IO_OUT),
	 FOCLA_IO_MASK_SHIFT(FOCLA_CAM_2, FOCLA_IO_2),
	 "Camera 2 ext output 2"},
	{"CL2_OUT3", 			FOCLA_REG_IO_CTRL(FOCLA_IO_OUT),
	 FOCLA_IO_MASK_SHIFT(FOCLA_CAM_2, FOCLA_IO_3),
	 "Camera 2 ext output 3"},
};


static int focla_param_table_len = (sizeof(focla_param_table) / 
				    sizeof(focla_param_table[0]));


/*--------------------------------------------------------------------------
 * parameter data
 *--------------------------------------------------------------------------*/

int focla_get_param_data(int *param_nr_ptr, struct espia_param **param_ptr)
{
	int param_nr;

	check_ptr(param_nr_ptr);

	param_nr = *param_nr_ptr;
	if (param_nr == -1) {
		*param_nr_ptr = focla_param_table_len;
		return FOCLA_OK;
	}

	check_param(param_nr);
	check_ptr(param_ptr);

	*param_ptr = &focla_param_table[param_nr];
	return FOCLA_OK;
}


/*--------------------------------------------------------------------------
 * table parameter get / set
 *--------------------------------------------------------------------------*/

int focla_get_param(focla_t dev, int param, int *val_ptr)
{
	struct espia_param *fparam = focla_param_table + param;
	check_param(param);
	return focla_get_bit(dev, fparam->reg, fparam->mask, fparam->shift,
			     val_ptr);
}

int focla_set_param(focla_t dev, int param, int  val)
{
	struct espia_param *fparam = focla_param_table + param;
	check_param(param);
	return focla_set_bit(dev, fparam->reg, fparam->mask, fparam->shift,
			     val);
}


/*--------------------------------------------------------------------------
 * specific parameter get / set
 *--------------------------------------------------------------------------*/

int focla_get_cam_sel(focla_t dev, int *cam_ptr)
{	
	return focla_get_param(dev, FOCLA_PARAM_CAM_SEL, cam_ptr);
}

int focla_set_cam_sel(focla_t dev, int cam)
{
	return focla_set_param(dev, FOCLA_PARAM_CAM_SEL, cam);
}

int focla_get_trig_mode(focla_t dev, int *trig_mode_ptr)
{
	return focla_get_param(dev, FOCLA_PARAM_TRIG_MODE, trig_mode_ptr);
}

int focla_set_trig_mode(focla_t dev, int trig_mode)
{
	return focla_set_param(dev, FOCLA_PARAM_TRIG_MODE, trig_mode);
}

int focla_get_cc_param(int cam_nr, int cc_nr)
{
	check_val(cam_nr, FOCLA_CAM_1, FOCLA_NR_CAM, FOCLA_ERR_CAMNR);
	check_val(cc_nr, FOCLA_CC_1, FOCLA_NR_CC, FOCLA_ERR_SIGNR);
	return (FOCLA_PARAM_CL1_CC1 + cam_nr * FOCLA_NR_CC + cc_nr);
}

int focla_get_cc(focla_t dev, int cam_nr, int cc_nr, int *val_ptr)
{
	int param = focla_get_cc_param(cam_nr, cc_nr);
	if (param < 0)
		return param;
	return focla_get_param(dev, param, val_ptr);
}

int focla_set_cc(focla_t dev, int cam_nr, int cc_nr, int val)
{
	int param = focla_get_cc_param(cam_nr, cc_nr);
	if (param < 0)
		return param;
	return focla_set_param(dev, param, val);
}

int focla_get_io_param(int io_type, int cam_nr, int io_nr)
{
	check_val(io_type, FOCLA_IO_IN, FOCLA_NR_IO_TYPE, FOCLA_ERR_IONR);
	check_val(cam_nr, FOCLA_CAM_1, FOCLA_NR_CAM, FOCLA_ERR_CAMNR);
	check_val(io_nr, FOCLA_IO_1, FOCLA_NR_IO, FOCLA_ERR_SIGNR);
	return (FOCLA_PARAM_CL1_IN1 + 
		(io_type * FOCLA_NR_CAM + cam_nr) * FOCLA_NR_IO + io_nr);
}

int focla_get_io(focla_t dev, int io_type, int cam_nr, int io_nr, int *val_ptr)
{
	int param = focla_get_io_param(io_type, cam_nr, io_nr);
	if (param < 0)
		return param;
	return focla_get_param(dev, param, val_ptr);
}

int focla_set_io(focla_t dev, int io_type, int cam_nr, int io_nr, int val)
{
	int param = focla_get_io_param(io_type, cam_nr, io_nr);
	if (param < 0)
		return param;
	return focla_set_param(dev, param, val);
}


/*--------------------------------------------------------------------------
 * Pulsable signals 
 *--------------------------------------------------------------------------*/

static struct focla_signal focla_signal_table[] = {
	{"CC1",  "CL CC1 signal"},
	{"CC2",  "CL CC2 signal"},
	{"CC3",  "CL CC3 signal"},
	{"CC4",  "CL CC4 signal"},
	{"OUT1", "Camera ext. output 1"},
	{"OUT2", "Camera ext. output 2"},
	{"OUT3", "Camera ext. output 3"},
	{"TRIG", "FOCLA soft. trigger"},
};

static int focla_signal_table_len = (sizeof(focla_signal_table) / 
				     sizeof(focla_signal_table[0]));

/*--------------------------------------------------------------------------
 * Signal data
 *--------------------------------------------------------------------------*/

int focla_get_sig_data(int *sig_nr_ptr, struct focla_signal **sig_ptr)
{
	int sig_nr;

	check_ptr(sig_nr_ptr);

	sig_nr = *sig_nr_ptr;
	if (sig_nr == -1) {
		*sig_nr_ptr = focla_signal_table_len;
		return FOCLA_OK;
	}

	check_sig(sig_nr);
	check_ptr(sig_ptr);

	*sig_ptr = &focla_signal_table[sig_nr];
	return FOCLA_OK;
}


/*--------------------------------------------------------------------------
 * Signal pulse start/stop/status
 *--------------------------------------------------------------------------*/

int focla_get_sig_param(int cam_nr, int sig_nr)
{
	int off;

	check_cam_sig_put(cam_nr, sig_nr, NULL);
	switch (sig_nr) {
	case FOCLA_SIG_CC1:
	case FOCLA_SIG_CC2:
	case FOCLA_SIG_CC3:
	case FOCLA_SIG_CC4:
		off = FOCLA_SIG_CC1 - FOCLA_CC_1;
		return focla_get_cc_param(cam_nr, sig_nr - off);
	case FOCLA_SIG_OUT1:
	case FOCLA_SIG_OUT2:
	case FOCLA_SIG_OUT3:
		off = FOCLA_SIG_OUT1 - FOCLA_IO_1;
		return focla_get_io_param(FOCLA_IO_OUT, cam_nr, sig_nr - off); 
	case FOCLA_SIG_TRIG:
		return FOCLA_PARAM_TRIG;
	default:
		return FOCLA_ERR_SIGNR;
	};
}

static int sig_pulse_get_afn(struct pulse_data *pulse_data,
			     unsigned long *acq_frame_nr)
{
	espia_t dev = pulse_data->pdev->dev;
	struct img_frame_info finfo;
	unsigned long timeout;
	int ret, ask;

	ask = (*acq_frame_nr == SCDXIPCI_ANY);

	finfo.buffer_nr    = SCDXIPCI_ANY;
	finfo.frame_nr     = SCDXIPCI_ANY;
	finfo.round_count  = SCDXIPCI_ANY;
	finfo.acq_frame_nr = *acq_frame_nr;

	timeout = ask ? SCDXIPCI_NO_BLOCK : SCDXIPCI_BLOCK_FOREVER;
	ret = espia_get_frame(dev, &finfo, timeout);
	
	*acq_frame_nr = finfo.acq_frame_nr;
	return ret;
}

static void *sig_pulse_thread(void *data)
{
	struct pulse_data *pulse_data = (struct pulse_data *) data;
	struct focla_dev *pdev = pulse_data->pdev;
	focla_t dev = dev_from_pdev(pdev);
	unsigned long acq_frame_nr = SCDXIPCI_ANY;
	int sig_nr, param, level, width, ret, val;

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	lock_dev(pdev);

	for (val = 0; val < pulse_data->nr_stage; val++) {
		width = pulse_data->stage_width_us[val];
		if (!pulse_width_is_wait_frame(width))
			continue;

		ret = sig_pulse_get_afn(pulse_data, &acq_frame_nr);
		if (ret == SCDXIPCI_ERR_NOTREADY)
			acq_frame_nr = -1;
		else if (ret < 0)
			goto unlock;
		break;
	}

	while (pulse_data_active(pulse_data)) {
		sig_nr = pulse_data->sig_nr;
		param  = pulse_data->param;
		level = (pulse_data->curr_stage + 1) % 2;
		if (pulse_data->polarity == FOCLA_PULSE_NEGATIVE)
			level = !level;
		width = pulse_data->stage_width_us[pulse_data->curr_stage];

		unlock_dev(pdev);

		pthread_testcancel();
		val = level;
		if ((sig_nr >= FOCLA_SIG_CC1) && (sig_nr <= FOCLA_SIG_CC4))
			val = level ? FOCLA_CC_HIGH : FOCLA_CC_LOW;
		ret = focla_set_param(dev, param, val);
		if (ret < 0)
			goto out;
		if (width > 0)
			usleep(width);
		else if (pulse_width_is_wait_frame(width)) {
			acq_frame_nr++;
			ret = sig_pulse_get_afn(pulse_data, &acq_frame_nr);
			if (ret < 0)
				goto out;
		}
		pthread_testcancel();

		lock_dev(pdev);
		pulse_data->curr_stage++;
		if ((pulse_data->curr_stage % pulse_data->nr_stage) == 0) {
			pulse_data->curr_stage = 0;
			pulse_data->curr_pulse++;
		}
	}

 unlock:
	unlock_dev(pdev);

 out:
	return NULL;
}

int focla_sig_pulse_start(focla_t dev, int cam_nr, int sig_nr, int polarity, 
			  int *stage_width_us_arr, int nr_stage, int nr_pulse)
{
	int level, i, ret, len, *ptr;
	struct pulse_data *pulse_data;
	struct focla_dev *pdev = get_pdev(dev);
	if (pdev == NULL)
		return FOCLA_ERR_NODEV;

	check_cam_sig_put(cam_nr, sig_nr, pdev);

	check_ptr_put(stage_width_us_arr, pdev);
	check_val_put(nr_stage, 1, FOCLA_PULSE_MAX_NR_STAGE + 1, 
		      FOCLA_ERR_PULSEPAR, pdev);

	ret = FOCLA_ERR_PULSEPAR;
	if (nr_pulse < 0)
		goto put;

	ptr = stage_width_us_arr;
	for (i = 0; i < nr_stage; i++, ptr++)
		if (!pulse_width_is_wait_frame(*ptr) && (*ptr < 0))
			goto put;

	pulse_data = focla_pulse_data(pdev, cam_nr, sig_nr);
	if (pulse_data_active(pulse_data)) {
		ret = FOCLA_ERR_PULSEACT;
		goto put;
	} else if (pulse_data->thread != 0) {
		pthread_join(pulse_data->thread, NULL);
		pulse_data->thread = 0;
	}

	ret = focla_get_param(dev, pulse_data->param, &level);
	if (ret < 0)
	    goto put;

	len = sizeof(*pulse_data->stage_width_us) * nr_stage;
	pulse_data->polarity = polarity;
	memcpy(pulse_data->stage_width_us, stage_width_us_arr, len);
	pulse_data->nr_stage = nr_stage;
	pulse_data->nr_pulse = nr_pulse;
	pulse_data->curr_pulse = 0;
	pulse_data->curr_stage = 0;

	ret = pthread_create(&pulse_data->thread, NULL, sig_pulse_thread, 
			     pulse_data);
 put:
	put_pdev(pdev);
	return ret;
}

int focla_sig_pulse_stop(focla_t dev, int cam_nr, int sig_nr)
{
	int level;
	struct pulse_data *pulse_data;
	struct focla_dev *pdev = get_pdev(dev);
	if (pdev == NULL)
		return FOCLA_ERR_NODEV;

	check_cam_sig_put(cam_nr, sig_nr, pdev);

	pulse_data = focla_pulse_data(pdev, cam_nr, sig_nr);

	if (pulse_data->thread == 0)
		goto put;
	if (pulse_data_active(pulse_data)) 
		pthread_cancel(pulse_data->thread);
	pthread_join(pulse_data->thread, NULL);
	pulse_data->thread = 0;

	level = (pulse_data->polarity == FOCLA_PULSE_POSITIVE) ? 0 : 1;
	if ((sig_nr >= FOCLA_SIG_CC1) && (sig_nr <= FOCLA_SIG_CC4))
		level = level ? FOCLA_CC_HIGH : FOCLA_CC_LOW;
	focla_set_param(dev, pulse_data->param, level);

 put:
	put_pdev(pdev);
	return FOCLA_OK;
}

int focla_sig_pulse_status(focla_t dev, int cam_nr, int sig_nr, 
			   int *active_ptr, int *curr_pulse_ptr, 
			   int *curr_stage_ptr)
{
	struct pulse_data *pulse_data;
	struct focla_dev *pdev = get_pdev(dev);
	if (pdev == NULL)
		return FOCLA_ERR_NODEV;

	check_cam_sig_put(cam_nr, sig_nr, pdev);
	check_ptr_put(active_ptr, pdev);

	pulse_data = focla_pulse_data(pdev, cam_nr, sig_nr);

	*active_ptr = pulse_data_active(pulse_data);
	if (curr_pulse_ptr != NULL) 
		*curr_pulse_ptr = pulse_data->curr_pulse;
	if (curr_stage_ptr != NULL) 
		*curr_stage_ptr = pulse_data->curr_stage;

	put_pdev(pdev);
	return FOCLA_OK;
}


/*--------------------------------------------------------------------------
 * serial read/write/flush
 *--------------------------------------------------------------------------*/

int focla_ser_read(focla_t dev, char *buffer, unsigned long *nr_bytes_ptr, 
		    unsigned long timeout)
{
	int ret;
	struct focla_dev *pdev = get_pdev(dev);
	if (pdev == NULL)
		return FOCLA_ERR_NODEV;

	ret = espia_ser_read(pdev->dev, buffer, nr_bytes_ptr, timeout);

	put_pdev(pdev);
	return ret;
}

int focla_ser_read_str(focla_t dev, char *buffer, unsigned long *nr_bytes_ptr, 
		       char *term, unsigned long term_bytes,
		       unsigned long timeout)
{
	int ret;
	struct focla_dev *pdev = get_pdev(dev);
	if (pdev == NULL)
		return FOCLA_ERR_NODEV;

	ret = espia_ser_read_str(pdev->dev, buffer, nr_bytes_ptr, 
				 term, term_bytes, timeout);

	put_pdev(pdev);
	return ret;
}

int focla_ser_write(focla_t dev, char *buffer, unsigned long *nr_bytes_ptr, 
		    unsigned long block_size, unsigned long delay, int block)
{
	int ret;
	struct focla_dev *pdev = get_pdev(dev);
	if (pdev == NULL)
		return FOCLA_ERR_NODEV;

	ret = espia_ser_write(pdev->dev, buffer, nr_bytes_ptr, block_size, 
			      delay, block);

	put_pdev(pdev);
	return ret;
}

int focla_ser_flush(focla_t dev)
{
	int ret;
	struct focla_dev *pdev = get_pdev(dev);
	if (pdev == NULL)
		return FOCLA_ERR_NODEV;

	ret = espia_ser_flush(pdev->dev);

	put_pdev(pdev);
	return ret;
}

