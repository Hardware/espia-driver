/****************************************************************************
 * File:	focla_lib.h
 * $Header: /segfs/bliss/cvs/driver/ESRF/espia/src/focla_lib.h,v 2.11 2008/07/23 12:28:12 ahoms Exp $
 * Project:	SECAD Espia card Linux driver
 * Description:	Fiber Optic Camera Link Adapter (FOCLA) library header
 * Author(s):	SECAD S.A.
 *		A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 ****************************************************************************/

#ifndef _FOCLA_H
#define _FOCLA_H

#include "espia_lib.h"

#define FOCLA_MAX_NR_DEV		(ESPIA_MAX_NR_DEV - 1)
#define FOCLA_DEV_INVAL			ESPIA_DEV_INVAL	

// Control register
#define FOCLA_REG_CTRL			0
#define FOCLA_CTRL_TRIG_MODE_MASK	0x07
#define FOCLA_CTRL_TRIG_MODE_SHIFT	0
#define FOCLA_CTRL_TRIG_MASK		0x08
#define FOCLA_CTRL_TRIG_SHIFT		3
#define FOCLA_CTRL_CAM_SEL_MASK		0x10
#define FOCLA_CTRL_CAM_SEL_SHIFT	4
#define FOCLA_CTRL_TEST_IMAGE_MASK	0x80		
#define FOCLA_CTRL_TEST_IMAGE_SHIFT	7

#define FOCLA_CTRL_TRIG_CONT		0
#define FOCLA_CTRL_TRIG_SINGLE		1


// Select camera 1 - 2
enum {
	FOCLA_CAM_1,
	FOCLA_CAM_2,
	FOCLA_NR_CAM,
};

// Pixel packing register
#define FOCLA_REG_PIX_PACK		1
// Pix. Pack. camera 1 - 2
#define FOCLA_PIX_PACK_MASK(cam)	(0x0f << FOCLA_PIX_PACK_SHIFT(cam))
#define FOCLA_PIX_PACK_SHIFT(cam)	((cam) * 4)

// CL_CC registers
#define FOCLA_REG_CL_CC(cam)		(2 + (cam))

// CL_CC signals 1 - 4
enum {
	FOCLA_CC_1,
	FOCLA_CC_2,
	FOCLA_CC_3,
	FOCLA_CC_4,
	FOCLA_NR_CC,
};

#define FOCLA_CC_MASK(cc)		(0x03 << FOCLA_CC_SHIFT(cc))
#define FOCLA_CC_SHIFT(cc)		((cc) * 2)

#define FOCLA_CC_EXT			0
#define FOCLA_CC_RESERVED		1
#define FOCLA_CC_LOW			2
#define FOCLA_CC_HIGH			3

// General Input/Output registers
enum {
	FOCLA_IO_IN,
	FOCLA_IO_OUT,
	FOCLA_NR_IO_TYPE,
};

#define FOCLA_REG_IO_CTRL(iotype)	(4 + (iotype))

// I/O signals 1 - 3
enum {
	FOCLA_IO_1,
	FOCLA_IO_2,
	FOCLA_IO_3,
	FOCLA_NR_IO,
};

#define FOCLA_IO_MASK(cam, x)		(0x01 << FOCLA_IO_SHIFT(cam, x))
#define FOCLA_IO_SHIFT(cam, x)		(FOCLA_IO_CAM_SHIFT(cam) + x)
#define FOCLA_IO_CAM_SHIFT(cam)		((cam) * 3)

// CL pulse
#define FOCLA_PULSE_POSITIVE		0
#define FOCLA_PULSE_NEGATIVE		1
#define FOCLA_PULSE_MAX_NR_STAGE	32
#define FOCLA_PULSE_WAIT_FRAME		SCDXIPCI_INVALID

#define FOCLA_NR_REG			16

#define FOCLA_OK			ESPIA_OK
#define	FOCLA_ERR_BASE			(ESPIA_ERR_BASE - 50)
#define FOCLA_ERR_ESPIADEV		(FOCLA_ERR_BASE - 1)
#define FOCLA_ERR_DEVOPEN		(FOCLA_ERR_BASE - 2)
#define FOCLA_ERR_NODEV			(FOCLA_ERR_BASE - 3)
#define FOCLA_ERR_SERIAL		(FOCLA_ERR_BASE - 4)
#define FOCLA_ERR_REGNR			(FOCLA_ERR_BASE - 5)
#define FOCLA_ERR_REGVAL		(FOCLA_ERR_BASE - 6)
#define FOCLA_ERR_PARAM			(FOCLA_ERR_BASE - 7)
#define FOCLA_ERR_PTR			(FOCLA_ERR_BASE - 8)
#define FOCLA_ERR_CAMNR			(FOCLA_ERR_BASE - 9)
#define FOCLA_ERR_SIGNR			(FOCLA_ERR_BASE - 10)
#define FOCLA_ERR_IONR			(FOCLA_ERR_BASE - 11)
#define FOCLA_ERR_PULSEACT		(FOCLA_ERR_BASE - 12)
#define FOCLA_ERR_PULSEPAR		(FOCLA_ERR_BASE - 13)

typedef unsigned long focla_t;

struct focla_signal {
	char	*name;
	char	*desc;
};

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

char *focla_strerror(int error);

// open/close
int focla_open(espia_t espia_dev, focla_t *dev_ptr);
int focla_close(focla_t dev);

// lock/unlock
int focla_lock(focla_t dev);
int focla_unlock(focla_t dev);

// register read/write
int focla_read_reg(focla_t dev, int reg, unsigned char *val_ptr);
int focla_write_reg(focla_t dev, int reg, unsigned char val);

// high-level parameter get/set
int focla_get_bit(focla_t dev, int reg, 
		  unsigned char mask, int shift, int *val_ptr);
int focla_set_bit(focla_t dev, int reg, 
		  unsigned char mask, int shift, int  val);

int focla_get_param_data(int *param_nr_ptr, struct espia_param **param_ptr);

int focla_get_param(focla_t dev, int param, int *val_ptr);
int focla_set_param(focla_t dev, int param, int  val);

int focla_get_cam_sel(focla_t dev, int *cam_ptr);
int focla_set_cam_sel(focla_t dev, int cam);
int focla_get_trig_mode(focla_t dev, int *trig_mode_ptr);
int focla_set_trig_mode(focla_t dev, int trig_mode);
int focla_get_cc_param(int cam_nr, int cc_nr);
int focla_get_cc(focla_t dev, int cam_nr, int cc_nr, int *val_ptr);
int focla_set_cc(focla_t dev, int cam_nr, int cc_nr, int val);
int focla_get_io_param(int io_type, int cam_nr, int io_nr);
int focla_get_io(focla_t dev, int io_type, int cam_nr, int io_nr, 
		 int *val_ptr);
int focla_set_io(focla_t dev, int io_type, int cam_nr, int io_nr, int val);

// CL_CC pulsable signals
int focla_get_sig_data(int *sig_nr_ptr, struct focla_signal **signal_ptr);
int focla_get_sig_param(int cam_nr, int sig_nr);

// CL_CC pulse start/stop/status
int focla_sig_pulse_start (focla_t dev, int cam_nr, int sig_nr, int polarity, 
			   int *stage_width_us_arr, int nr_stage, int nr_pulse);
int focla_sig_pulse_stop  (focla_t dev, int cam_nr, int sig_nr);
int focla_sig_pulse_status(focla_t dev, int cam_nr, int sig_nr, 
			   int *active_ptr, int *curr_pulse_ptr, 
			   int *curr_stage_ptr);

// serial read/write/flush [see espia_lib.h]
int focla_ser_read (focla_t dev, char *buffer, unsigned long *nr_bytes_ptr, 
		    unsigned long timeout);
int focla_ser_read_str(focla_t dev, char *buffer, unsigned long *nr_bytes_ptr, 
		       char *term, unsigned long term_bytes,
		       unsigned long timeout);
int focla_ser_write(focla_t dev, char *buffer, unsigned long *nr_bytes_ptr, 
		    unsigned long block_size, unsigned long delay, int block);
int focla_ser_flush(focla_t dev);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* _FOCLA_H */
