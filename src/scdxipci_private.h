/****************************************************************************
 * File:	scdxipci_private.h
 * $Header: /segfs/bliss/cvs/driver/ESRF/espia/src/scdxipci_private.h,v 2.34 2017/11/15 12:19:30 ahoms Exp $
 * Project:	SECAD Espia card Linux driver
 * Description:	Driver private header file
 * Author(s):	SECAD S.A.
 *		A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 ****************************************************************************/

#ifndef _SCDXIPCI_PRIVATE_H
#define _SCDXIPCI_PRIVATE_H

#include "scdxipci.h"
#include "circ_buffer.h"
#include "scdxipci_memory.h"

#include <linux/pci.h>
#include <linux/pci_ids.h>
#include <linux/ktime.h>


/*--------------------------------------------------------------------------
 * The debug stuff
 *--------------------------------------------------------------------------*/

#define DEB_HEADING		SCDXIPCI_DRV_NAME

#include "esrfdebug.h"

EXTERN_DEBUG_LEVEL(debug_level);

#define SCDXIPCI_DEB_ALWAYS	0

#define DEB_ALWAYS(fmt, ...)	\
	DPRINTK(SCDXIPCI_DEB_ALWAYS, fmt , ## __VA_ARGS__)
#define DEB_ERRORS(fmt, ...)	\
	DPRINTK(SCDXIPCI_DEB_ERRORS, fmt , ## __VA_ARGS__)
#define DEB_PROBE(fmt, ...)	\
	DPRINTK(SCDXIPCI_DEB_PROBE,  fmt , ## __VA_ARGS__)
#define DEB_OPEN(fmt, ...)	\
	DPRINTK(SCDXIPCI_DEB_OPEN,   fmt , ## __VA_ARGS__)
#define DEB_IOCTL(fmt, ...)	\
	DPRINTK(SCDXIPCI_DEB_IOCTL,  fmt , ## __VA_ARGS__)
#define DEB_PARAMS(fmt, ...)	\
	DPRINTK(SCDXIPCI_DEB_PARAMS, fmt , ## __VA_ARGS__)
#define DEB_SERIAL(fmt, ...)	\
	DPRINTK(SCDXIPCI_DEB_SERIAL, fmt , ## __VA_ARGS__)
#define DEB_INTS(fmt, ...)	\
	DPRINTK(SCDXIPCI_DEB_INTS,   fmt , ## __VA_ARGS__)
#define DEB_REGS(fmt, ...)	\
	DPRINTK(SCDXIPCI_DEB_REGS,   fmt , ## __VA_ARGS__)
#define DEB_FLASH(fmt, ...)	\
	DPRINTK(SCDXIPCI_DEB_FLASH,  fmt , ## __VA_ARGS__)


/*--------------------------------------------------------------------------
 * The hardware debug 
 *--------------------------------------------------------------------------*/

#define PARPORT_NR	2
#define PARPORT_1	0
#define PARPORT_BASE_1	0x378
#define PARPORT_2	1
#define PARPORT_BASE_2	0x278

#define PARPORT_DATOUT	0
#define PARPORT_CTLOUT	2

static inline void parport_out(unsigned pport, unsigned short data)
{
	unsigned char val;
	static unsigned short port_base[PARPORT_NR] = {
		PARPORT_BASE_1, PARPORT_BASE_2
	};

	if (pport >= PARPORT_NR)
		return;

	val = data & 0xff;
	outb(val, port_base[pport] + PARPORT_DATOUT);
	val = (data >> 8) ^ 0xe;
	val = ((val & 0x3) << 2) | ((val & 0x4) >> 1) |	((val & 0x8) >> 3);
	outb(val, port_base[pport] + PARPORT_CTLOUT);
}


extern unsigned short scdxipci_ppdeb_val[PARPORT_NR];
extern spinlock_t scdxipci_ppdeb_lock;

// the 12 parport #1 debug bits are:
//   0-2 dev 0 int status
//   3-5 dev 1 int status
//   6-7 eof tasklet status
//   8   in get_frame from buffer/frame_nr
//   9   in get_frame from acq_frame_nr
//   10  dev 0 heartbeat
//   11  dev 1 heartbeat

#define PPDEB_CODE(mask, bit) 	((mask) << 16 | (bit))
#define PPDEB_MASK(code)	((code) >> 16)
#define PPDEB_BIT(code)		((code) & 0xffff)

static inline void scdxipci_ppdeb_set(unsigned long code)
{
	int pport = PARPORT_1;
	unsigned long flags;

	spin_lock_irqsave(&scdxipci_ppdeb_lock, flags);
	scdxipci_ppdeb_val[pport] &= ~PPDEB_MASK(code);
	scdxipci_ppdeb_val[pport] |=  PPDEB_BIT(code);
	parport_out(pport, scdxipci_ppdeb_val[pport]);
	spin_unlock_irqrestore(&scdxipci_ppdeb_lock, flags);
}

#define PPDEB_BIT_SET(bit, act)	\
	scdxipci_ppdeb_set(PPDEB_CODE(bit, (act) ? (bit) : 0))

#define PPDEB_STATE(dev, stat)	((stat) << (!(dev)->nr ? 3 : 0))
#define PPDEB_DEV_MASK(dev)	PPDEB_STATE(dev, 7)
#define PPDEB_DEV_OUT		0
#define PPDEB_DEV_IN_IRQ	1
#define PPDEB_DEV_LOCK_DEV	2
#define PPDEB_DEV_LOCK_ACQ	3
#define PPDEB_DEV_NORM_FRM	4
#define PPDEB_DEV_EOF		5
#define PPDEB_DEV_LAST_FRM	6
#define PPDEB_DEV_LAST_EOF	7

#define PPDEB_DEV_CODE(d, f)    PPDEB_CODE(PPDEB_DEV_MASK(d), PPDEB_STATE(d, f))
#define PPDEB_DEV_SET(d, f)	scdxipci_ppdeb_set(PPDEB_DEV_CODE(d, f))

#define PPDEB_EOF_MASK		0xc0
#define PPDEB_EOF_OUT		0
#define PPDEB_EOF_LOCK_ACQ	1
#define PPDEB_EOF_WAKEUP	2
#define PPDEB_EOF_END		3

#define PPDEB_EOF_CODE(stat)	PPDEB_CODE(PPDEB_EOF_MASK, (stat) << 6)
#define PPDEB_EOF_SET(stat)	scdxipci_ppdeb_set(PPDEB_EOF_CODE(stat))

#define PPDEB_GETFRM_BUFF_NR	(1 << 8)
#define PPDEB_GETFRM_ACQ_NR	(1 << 9)
#define PPDEB_GETFRM_SET(type, act) \
	PPDEB_BIT_SET(PPDEB_GETFRM_##type, act)

#define PPDEB_GETFRM_ACQ	(1 << 9)
	
#define PPDEB_HEARTBEAT_USEC	100000
#define PPDEB_HEARTBEAT_EXPIRES	(jiffies + PPDEB_HEARTBEAT_USEC * HZ / 1000000)

#define PPDEB_HEARTBEAT_BIT(dev)	\
	(1 << (!(dev)->nr ? 10 : 11))
#define PPDEB_HEARTBEAT_SET(dev, act)	\
	PPDEB_BIT_SET(PPDEB_HEARTBEAT_BIT(dev), act)


/*--------------------------------------------------------------------------
 * Device Info, Parameters & Constants
 *--------------------------------------------------------------------------*/

#define SCDXIPCI_VENDOR_ID	PCI_VENDOR_ID_XILINX
#define SCDXIPCI_DEV_ID		0x0300
#define SCDXIPCI_SUBVENDOR_ID_0	0x0000
#define SCDXIPCI_SUBDEV_ID_0	0x0000
#define SCDXIPCI_SUBVENDOR_ID_1	0x3275
#define SCDXIPCI_SUBDEV_ID_1	0x40f9
#define SCDXIPCI_DEV_ID_2	0x6018
#define SCDXIPCI_SUBVENDOR_ID_2	0xe31f
#define SCDXIPCI_SUBDEV_ID_2	0x80e7
#define SCDXIPCI_CLASS		(PCI_CLASS_SP_OTHER << 8)
#define SCDXIPCI_CLASS_MASK	(0xffff << 8)

#define SCDXIPCI_PCI_FLASH_LEN		0x060000L
#define SCDXIPCI_EXPRESS_FLASH_LEN	0x540000L

#define SCDXIPCI_PCI_BRD_ID_LEN		sizeof(struct scdxipci_card_id)
#define SCDXIPCI_EXPRESS_BRD_ID_LEN	sizeof(struct scdxipci_card_id)

#define SCDXIPCI_PCI_BRD_ID_START	\
	(SCDXIPCI_PCI_FLASH_LEN     - SCDXIPCI_PCI_BRD_ID_LEN)
#define SCDXIPCI_EXPRESS_BRD_ID_START	\
	(SCDXIPCI_EXPRESS_FLASH_LEN - SCDXIPCI_EXPRESS_BRD_ID_LEN)

#define SCDXIPCI_PCI_FW_LEN		(  93953 * 4)
#define SCDXIPCI_EXPRESS_FW_LEN		(1366243 * 4)

#define SCDXIPCI_CAP_NONE		0
#define SCDXIPCI_CAP_BAD_SER_TIMING	(1 << 0)
#define SCDXIPCI_CAP_GOOD_RESET_SG	(1 << 1)

#define SCDXIPCI_CAP_SET_P1		\
	(SCDXIPCI_CAP_NONE)
#define SCDXIPCI_CAP_SET_P2		\
	(SCDXIPCI_CAP_BAD_SER_TIMING)

#define SCDXIPCI_CAP_SET_1		\
	(SCDXIPCI_CAP_BAD_SER_TIMING)
#define SCDXIPCI_CAP_SET_2		\
	(SCDXIPCI_CAP_NONE)
#define SCDXIPCI_CAP_SET_3		\
	(SCDXIPCI_CAP_GOOD_RESET_SG)

#define SCDXIPCI_RESETLINK_WAIT	10000 

#define SCDXIPCI_SER_BUFF_LEN	(128 * 1024)

#define SCDXIPCI_SG_BLOCK_SIZE	sizeof(struct scdxipci_sg_block)
#define SCDXIPCI_SG_TABLE_LEN	(128 * 1024)	// <16 K desc -> <64 MB frames
#define SCDXIPCI_SG_MAX_BLOCKS	((SCDXIPCI_SG_TABLE_LEN /	\
				  SCDXIPCI_SG_BLOCK_SIZE) - 1)
#define SCDXIPCI_NR_SG_TABLE	5

#define SCDXIPCI_NAME_LEN	50
#define SCDXIPCI_FRAME_NAME_LEN	50

#define SCDXIPCI_REG_READ	0
#define SCDXIPCI_REG_WRITE	1

#define SCDXIPCI_USED		0
#define SCDXIPCI_UNUSED		1

#define SCDXIPCI_BUFF_NORM	0
#define SCDXIPCI_BUFF_AUX	1

#define SCDXIPCI_FRAME_FIRST	0
#define SCDXIPCI_FRAME_CONT	1

#define SCDXIPCI_NR_AUX_FRAMES	5

#define SCDXIPCI_AFN_MAX_ITER	10

#define SCDXIPCI_LOCK_ACQDEV_DELAY	5

#define SCDXIPCI_MEMCPY_SCHED_PRIORITY	30
#define SCDXIPCI_MEMCPY_SCHED_POLICY	SCHED_FIFO

#define SCDXIPCI_SIMUCOND_ISR_SGOVER	(1 << 0)
#define SCDXIPCI_SIMUCOND_ISR_NOMEM	(1 << 1)
#define SCDXIPCI_SIMUCOND_ISR_NOFRAME	(1 << 2)
#define SCDXIPCI_SIMUCOND_ISR_NOMAPPED	(1 << 3)
#define SCDXIPCI_SIMUCOND_EOFT_OVERRUN	(1 << 4)

#define SCDXIPCI_FLASH_READ_DELAY	5

#define SCDXIPCI_CRC32_POLY		0x04c11db7
#define SCDXIPCI_CRC32_INIT		0xffffffff

#define SCDXIPCI_MAX_EOF_TSKT_RETRIES	16

#define SCDXIPCI_RELEASED_PAGE_MARKER	0xff

#define simu_cond(err)				\
	(simu_cond_flags & SCDXIPCI_SIMUCOND_##err)

#define scdxipci_min(a, b) \
	((a) < (b)) ? (a) : (b)
#define scdxipci_max(a, b) \
	((a) > (b)) ? (a) : (b)

/*--------------------------------------------------------------------------
 * OS version stuff
 *--------------------------------------------------------------------------*/

#ifdef ESRFVER_REMAP_PAGE_32
#define SCDXIPCI_BUFF_PAGE_32		1
#else
#define SCDXIPCI_BUFF_PAGE_32		0
#endif 


/*--------------------------------------------------------------------------
 * Init / cleanup structure
 *--------------------------------------------------------------------------*/

enum {
	MOD_INIT_CHR_DEV,
	MOD_INIT_CLASS,
	MOD_INIT_FRAME_PAGE_CACHE,
	MOD_INIT_FRAME_CACHE,
	MOD_INIT_BUFFER_CACHE,
	MOD_INIT_PCI_DRV,
	MOD_INIT_META_CLASS_DEV_OBJ,
	MOD_INIT_META_CLASS_DEV_ADD,
};

enum {
	BRD_INIT_ENABLE,
	BRD_INIT_REGION,
	BRD_INIT_MAP,
	BRD_INIT_DMA,
	BRD_INIT_DEV_ALLOC,
	BRD_INIT_DEV_INIT,
	BRD_INIT_INC_IDX,
};

enum {
	DEV_INIT_SER_RX,
	DEV_INIT_SER_TX,
	DEV_INIT_TX_THREAD,
	DEV_INIT_SG_TABLE,
	DEV_INIT_IRQ,
	DEV_INIT_LIST,
	DEV_INIT_CLASS_DEV_OBJ,
	DEV_INIT_CLASS_DEV_ADD,
	DEV_INIT_HEARTBEAT,
};

#define init_flags_bit(b)	(1 << (b))
#define init_flags_check(f, b)	((*(f) & init_flags_bit(b)) != 0)
#define init_flags_set(f, b)	do { *(f) |= init_flags_bit(b); } while (0)
#define init_flags_init(f)	do { *(f) = 0; } while (0)

typedef unsigned long init_flags_t;


/*--------------------------------------------------------------------------
 * The driver structures
 *--------------------------------------------------------------------------*/

struct data_acq;
struct scdxipci_dev;

struct img_frame_page {
	struct img_frame_page	 *next;
	struct page		 *page;
};

struct img_frame {
	struct list_head	  frame_list;
	struct img_buffer	 *buffer;
	unsigned long		  frame_nr;
	struct img_frame_page	 *first_page;
	unsigned long		  nr_pages;
	unsigned long		  nr_pages64;
	unsigned long		  acq_frame_nr;
	unsigned long		  nr_pixels;
	ktime_t			  time_stamp;
	unsigned long		  round_count;
	atomic_t		  dev_count;
};

struct img_buffer {
	unsigned long		  nr;
	struct data_acq		 *acq;
	char			  name[SCDXIPCI_NAME_LEN];
	struct list_head	  frame_list;
	struct vm_area_struct	 *vma;
	atomic_t		  open_count;	// track vm_open/close calls
	atomic_t		  use_count;	// cannot be free if used
	wait_queue_head_t	  use_wq;	// proc. waiting to free
};

struct acq_wait {
	struct list_head	  wait_list;
	wait_queue_head_t	  wq;
	volatile int		  woken_up;
	struct img_frame_info	 *frame_info;
	unsigned long		  thread_id;
};

struct acq_dev {
	unsigned long		  idx;
	struct data_acq		 *acq;
	struct scdxipci_dev	 *dev;
	struct scdxipci_sg_table  sg_table;	// user-defined SG table
	int			  sg_table_validated;
	struct tasklet_struct	  sg_task;
	unsigned long		  finished_frames;
	unsigned long		  aux_buffer_curr_frame;
};

struct eof_data {
	struct list_head	  list;
	struct img_frame	 *ready_frame;
	struct img_frame	 *mapped_frame;
	struct img_frame	 *next_frame;
	int			  acq_end;
	int			  error;
};

struct data_acq {
	int			  enabled;
	int			  is_meta;
	struct acq_dev		 *dev;
	unsigned long		  nr_dev;
	char			  name[SCDXIPCI_NAME_LEN];
	spinlock_t		  lock;
	struct img_buffer	 *aux_buffer;
	struct img_buffer	**buffer_arr;
	unsigned long		  buffer_arr_len;
	unsigned long		  nr_buffers;
	unsigned long		  default_frame_size;
	unsigned long		  removed_buffers;
	int			  keep_released_pages;
	unsigned long		  nr_deleted_pages;
	struct img_frame_page	 *deleted_frame_pages;
	struct img_frame_page	 *last_deleted_frame_page;
	ktime_t			  start_time;
	struct img_buffer	 *start_buffer;
	struct img_frame	 *curr_frame;
	unsigned long		  acq_frame_nr;
	unsigned long		  tot_frames;
	unsigned long		  acq_run_nr;
	struct list_head	  wait_list;
	wait_queue_head_t	  wait_wq;
	struct scdxipci_stats	  stats;
	struct tasklet_struct	  eof_task;
	struct list_head	  eof_data_list;
	kthread_t		  memcpy_thread;
	struct list_head	  memcpy_data_list;
	wait_queue_head_t	  memcpy_wq;
	struct img_frame_page	 *garbage_page;
	unsigned long		  garbage_len;
	int			  no_fifo_reset;
	int			  error;
};

struct sg_data {
	struct scdxipci_sg_table  sg_table;	// SG table used by the dev.
	struct img_frame	 *buffer_frame;	// target (user) frame
	struct img_frame	 *mapped_frame;	// really mapped frame
	struct scatterlist	 *sg_list;	// aux SG list for the OS
	unsigned long		  sg_list_len;
	unsigned long		  sg_list_mapped;
	unsigned long		  dma_dwords;
	unsigned long		  frame_dwords;
	int			  being_mapped;
};

struct scdxipci_dev {
	struct list_head	  dev_list;
	int			  nr;
	struct scdxipci_board	 *board;
	int			  idx;
	int			  type;
	char			 *base_addr;
	char			  name[SCDXIPCI_NAME_LEN];
	spinlock_t		  lock;
	struct tasklet_struct	  rx_ser_task;
	struct circ_buffer	  rx_buff;
	struct circ_buffer	  tx_buff;
	volatile unsigned long	  tx_block_size;
	volatile unsigned long	  tx_delay;
	kthread_t		  tx_thread;
	struct acq_dev		 *curr_acq;
	struct sg_data		  sg_data[SCDXIPCI_NR_SG_TABLE];
	struct sg_data		 *curr_sg_data;
	struct sg_data		 *oldest_sg_data;
	volatile int		  ccd_status;
	volatile int		  ccd_status_arrived;
	wait_queue_head_t	  ccd_status_wq;
	struct timer_list	  heartbeat;
	unsigned long		  heartbeat_count;
	int			  no_dma64;
#ifndef SCDXIPCI_NO_CLASS_DEV
	ESRFVER_CLASS_DEV_TYPE	 *class_dev;
#endif
	init_flags_t		  init_flags;
};

struct firmware_data {
	char			  name[SCDXIPCI_FW_NAME_LEN];
	int			  checksum;
	int			  dev_per_board;
	int			  dev_type;
	unsigned int		  capabilities;
};

struct board_type {
	int			  type;
	char			  name[SCDXIPCI_NAME_LEN];
	int			  dev_per_board;
	int			  regs_size;
	int			  firmware_start;
	int			  firmware_len;
	struct firmware_data	 *firmware_array;
	int			  board_id_start;
	int			  board_id_len;
};

struct scdxipci_board {
	struct board_type	 *type;
	struct pci_dev		 *dev;
	char			  name[SCDXIPCI_NAME_LEN];
	int			  firmware_checksum;
	struct firmware_data	 *firmware;
	char			 *base_addr;
	int			  idx;
	int			  ser_nr;
	int			  crc_ok;
	struct scdxipci_dev	 *scd_dev;
	int			  nr_scd_dev;
	init_flags_t		  init_flags;
};

/*--------------------------------------------------------------------------
 * The global variables
 *--------------------------------------------------------------------------*/

extern char drv_revision[];
extern init_flags_t mod_init_flags;
extern kmem_cache_t *buffer_cache, *frame_cache, *frame_page_cache;
extern struct file_operations scdxipci_file_ops;

#ifndef SCDXIPCI_NO_CLASS_DEV
extern struct class scdxipci_class;
extern ESRFVER_CLASS_DEV_TYPE *scdxipci_meta_class_dev;
#endif

extern int major;
extern int simu_cond_flags; // flags for simulating error conditions

extern struct board_type scdxipci_board_type_list[SCDXIPCI_NR_BRD_TYPES];

/*--------------------------------------------------------------------------
 * Function prototypes
 *--------------------------------------------------------------------------*/

/*---------------- in scdxipci_main.c -----------------------*/

int  __init scdxipci_driver_init(void);
void __exit scdxipci_driver_exit(void);

int  scdxipci_probe (struct pci_dev *dev, const struct pci_device_id *id);
void scdxipci_remove(struct pci_dev *dev);

int  scdxipci_dev_add(struct scdxipci_dev *scd_dev, 
		      struct scdxipci_board *board, int idx);
void scdxipci_dev_del(struct scdxipci_dev *scd_dev);

int  scdxipci_init_sg_data   (struct scdxipci_dev *scd_dev);
void scdxipci_release_sg_data(struct scdxipci_dev *scd_dev);

#ifndef ESRFVER_NO_CLASS_DEV
#ifdef ESRFVER_CLASS_DEV_NO_DEVT
ssize_t scdxipci_class_dev_read_dev(ESRFVER_CLASS_DEV_TYPE *class_dev,
				    char *buffer);
#endif

void scdxipci_class_dev_release(ESRFVER_CLASS_DEV_TYPE *class_dev);
#endif

ESRFVER_TIMER_FUNCTION(scdxipci_heartbeat_timer, data);

struct scdxipci_dev *scdxipci_find_dev(int dev_nr);

int  scdxipci_check_reg(unsigned long reg_off);
u32  scdxipci_read_reg (struct scdxipci_dev *scd_dev, unsigned long reg_off);
void scdxipci_write_reg(struct scdxipci_dev *scd_dev, unsigned long reg_off, 
			u32 val);

void scdxipci_reset_link(struct scdxipci_dev *scd_dev);

DECLARE_IRQ_HANDLER(scdxipci_irq_handler, irq, data);
void scdxipci_dma_irq_handler   (struct scdxipci_dev *scd_dev);
void scdxipci_status_irq_handler(struct scdxipci_dev *scd_dev);

void scdxipci_rx_ser_tasklet(unsigned long data);

int  scdxipci_get_term(char *usr_term, unsigned long term_len, char **term);
void scdxipci_put_term(char *term);
int scdxipci_ser_read (struct scdxipci_dev *scd_dev, char *usr_buff, 
		       unsigned long buff_len, char *usr_term,
		       unsigned long term_len, unsigned long timeout);
int scdxipci_ser_write(struct scdxipci_dev *scd_dev, char *usr_buff, 
		       unsigned long len, unsigned long block_size,
		       unsigned long delay, int block);

int scdxipci_ser_write_thread_start(struct scdxipci_dev *scd_dev);
int scdxipci_ser_write_thread_stop (struct scdxipci_dev *scd_dev);
int scdxipci_ser_write_thread(void *data);

void scdxipci_activate_dma(struct scdxipci_dev *scd_dev, int flags);
void scdxipci_abort_dma   (struct scdxipci_dev *scd_dev);

int  scdxipci_pci_map_frame  (struct scdxipci_dev *scd_dev, 
			      struct sg_data *sg_data, 
			      struct img_frame *frame);
void scdxipci_pci_unmap_frame(struct scdxipci_dev *scd_dev, 
			      struct sg_data *sg_data);

int scdxipci_ccd_status(struct scdxipci_dev *scd_dev, unsigned long timeout);

int scdxipci_dma_align(struct scdxipci_dev *scd_dev);

int scdxipci_hw_info(struct scdxipci_dev *scd_dev, int action,
		     struct scdxipci_hw_info *hw_info, int usr_spc);

void scdxipci_init_flash(struct scdxipci_board *scd_board);
u16 scdxipci_read_flash_word(struct scdxipci_board *scd_board, int offset);
u32 scdxipci_read_flash_dword(struct scdxipci_board *scd_board, int offset);

u32 scdxipci_crc32_init(void);
u32 scdxipci_crc32_one(u8 data, u32 crc_acc);
u32 scdxipci_crc32_end(u32 crc_acc);

int scdxipci_init_board_id_firmware(struct scdxipci_board *scd_board);


/*---------------- in scdxipci_file_ops.c ---------------------*/
#define HAVE_UNLOCKED_IOCTL 1
int scdxipci_open  (struct inode *inode, struct file *file);
int scdxipci_close (struct inode *inode, struct file *file);
#ifdef HAVE_UNLOCKED_IOCTL
long scdxipci_ioctl(struct file *file, unsigned int cmd, unsigned long arg);
#else
int scdxipci_ioctl (struct inode *inode, struct file *file,
		    unsigned int cmd, unsigned long arg);
#endif
int scdxipci_mmap  (struct file *file, struct vm_area_struct *vma);

int scdxipci_option(struct data_acq *acq, struct scdxipci_ioc_option *optp);

int scdxipci_ioc_reg(struct scdxipci_dev *scd_dev, 
		     struct scdxipci_ioc_register *regp, int write);

int scdxipci_ioc_start_dma(struct data_acq *acq, 
			   struct scdxipci_ioc_dma *dmap);
int scdxipci_ioc_get_frame(struct data_acq *acq, 
			   struct scdxipci_ioc_dma *dmap);
int scdxipci_buffer_info(struct data_acq *acq, struct img_buffer_info *binfo);

int scdxipci_ioc_set_sg(struct data_acq *acq, struct scdxipci_ioc_sg *sgp);
int scdxipci_ioc_set_dev(struct data_acq *acq, 
			 struct scdxipci_ioc_dev_list *devp);


/*---------------- in scdxipci_acq_buffer.c ---------------------*/

struct data_acq       *scdxipci_acq_alloc    (int minor);
struct acq_dev        *scdxipci_acq_dev_alloc(struct data_acq *acq, 
					      unsigned long nr_dev);
struct img_buffer     *scdxipci_buffer_alloc (struct data_acq *acq, 
					      int buffer_type, 
					      int nr_frames, 
					      unsigned long frame_pages, 
					      struct vm_area_struct *vma);
struct img_frame      *scdxipci_frame_alloc  (int buffer_type, 
					      int frame_pages, 
					      struct vm_area_struct *vma,
					      unsigned long vaddr);
struct img_frame_page *scdxipci_frame_page_alloc(gfp_t gfp_flags,
						 struct vm_area_struct *vma,
						 unsigned long vaddr);

void scdxipci_acq_free       (struct data_acq *acq);
void scdxipci_acq_dev_free   (struct acq_dev *acq_dev_arr, 
			      unsigned long nr_dev);
void scdxipci_buffer_free    (struct img_buffer *buffer);
void scdxipci_frame_free     (struct img_frame *frame);
void scdxipci_frame_page_free(struct img_frame_page *fpage);

int scdxipci_acq_frame_size(struct data_acq *acq, unsigned long frame_size);
void scdxipci_buffer_set_name(struct img_buffer *buffer, char *name);
void scdxipci_acq_eof_tasklet(unsigned long data);
int  scdxipci_acq_eof_cleanup(struct data_acq *acq);

int  scdxipci_acq_dev_update_sg_data(struct acq_dev *acq_dev);
void scdxipci_acq_dev_sg_tasklet(unsigned long data);

int  scdxipci_memcpy_thread_start(struct data_acq *acq);
void scdxipci_memcpy_thread_stop (struct data_acq *acq);
int  scdxipci_memcpy_thread(void *data);
struct eof_data *scdxipci_memcpy_eof_data(struct data_acq *acq);
int  scdxipci_frame_copy(struct img_frame *dst, struct img_frame *src);
void scdxipci_frame_finish(struct data_acq *acq, struct eof_data *eof_data);
void scdxipci_acq_end_cleanup(struct data_acq *acq);

int  scdxipci_buffer_add   (struct data_acq *acq, struct img_buffer *buffer);
void scdxipci_buffer_remove(struct data_acq *acq, struct img_buffer *buffer);

int  scdxipci_buffer_map  (struct img_buffer *buffer, 
			   struct vm_area_struct *vma);
void scdxipci_buffer_unmap(struct img_buffer *buffer, 
			   struct vm_area_struct *vma, unsigned long nr_pages);

void scdxipci_vm_open (struct vm_area_struct *vma);
void scdxipci_vm_close(struct vm_area_struct *vma);

int scdxipci_buffer_check(struct data_acq *acq, struct img_buffer *buffer);

struct img_buffer *scdxipci_buffer_from_uadd(struct data_acq *acq, void *uadd);

int  scdxipci_acq_start(struct data_acq *acq, unsigned long start_buffer_nr,
			unsigned long nr_frames);
void scdxipci_acq_stop (struct data_acq *acq);
struct img_frame *scdxipci_get_start_frame(struct data_acq *acq,
					   unsigned long start_buffer_nr,
					   struct img_buffer **start_buffer);

int scdxipci_acq_wait_get_frame(struct data_acq *acq, 
				struct img_frame_info *finfo, 
				unsigned long timeout, unsigned long thread_id);
int scdxipci_find_by_acq_frame_nr(struct data_acq *acq, 
				  struct img_frame_info *finfo, 
				  struct img_frame **pframe, int *block);
int scdxipci_find_by_buffer_frame(struct data_acq *acq, 
				  struct img_frame_info *finfo, 
				  struct img_frame **pframe, int *block);

void scdxipci_update_frame_info(struct data_acq *acq, 
				struct img_frame_info *finfo, 
				struct img_frame *frame);

int scdxipci_acq_block(struct acq_wait *wait, unsigned long timeout);
void scdxipci_wakeup(struct data_acq *acq, struct img_frame *frame);

int scdxipci_acq_check_wait(struct data_acq *acq, unsigned long thread_id,
			    unsigned long timeout);

int scdxipci_get_stats(struct data_acq *acq, struct scdxipci_stats *usr_stats);

int acq_dev_validate_sg_table(struct acq_dev *acq_dev, 
			      struct scdxipci_sg_table *sg_table);

int scdxipci_acq_set_sg(struct data_acq *acq, unsigned long dev_idx,
			struct scdxipci_sg_table *sg);
int scdxipci_acq_set_dev(struct data_acq *acq, int *dev_list,
			 unsigned long nr_dev);


/*--------------------------------------------------------------------------
 * Helper functions & macros
 *--------------------------------------------------------------------------*/

#define is_invalid(val) \
	scdxipci_is(INVALID, val)
#define is_invalid32(val) \
	scdxipci_is(INVALID32, val)
#define is_any(val) \
	scdxipci_is(ANY, val)

#define scdxipci_lock_dev(scd_dev, flags) \
	spin_lock_irqsave(&(scd_dev)->lock, flags)
#define scdxipci_unlock_dev(scd_dev, flags) \
	spin_unlock_irqrestore(&(scd_dev)->lock, flags)
#define scdxipci_lock_acq(acq, flags) \
	spin_lock_irqsave(&(acq)->lock, flags)
#define scdxipci_unlock_acq(acq, flags) \
	spin_unlock_irqrestore(&(acq)->lock, flags)


static inline int scdxipci_lock_acq_from_dev(struct scdxipci_dev *scd_dev)
{
        struct acq_dev *acq_dev;
	spinlock_t *dev_lock = &scd_dev->lock;

	for (;;) {
		barrier();
		acq_dev = (struct acq_dev *) scd_dev->curr_acq;
		if (acq_dev == NULL)
			break;
		
		if (spin_trylock(&acq_dev->acq->lock))
			return 1;

		spin_unlock(dev_lock);
		udelay(SCDXIPCI_LOCK_ACQDEV_DELAY);
		spin_lock(dev_lock);
	}
	return 0;
}

static inline
void scdxipci_unlock_acq_dev(struct scdxipci_dev *scd_dev)
{
	spin_unlock(&scd_dev->curr_acq->acq->lock);
}


// not sure if static inline is optimized enogh...
#define scdxipci_nr_sg_data(scd_dev)	SCDXIPCI_NR_SG_TABLE

static inline
struct sg_data *scdxipci_first_sg_data(struct scdxipci_dev *scd_dev)
{
	return &scd_dev->sg_data[0];
}

static inline
unsigned long scdxipci_sg_data_idx(struct scdxipci_dev *scd_dev, 
				   struct sg_data *sg_data)
{
	unsigned long table_idx = sg_data - scdxipci_first_sg_data(scd_dev);
	if (sg_data && (table_idx < SCDXIPCI_NR_SG_TABLE))
		return table_idx;
	return SCDXIPCI_INVALID;
}

static inline
struct sg_data *scdxipci_next_sg_data(struct scdxipci_dev *scd_dev, 
				      struct sg_data *sg_data)
{
	unsigned long table_idx = scdxipci_sg_data_idx(scd_dev, sg_data);
	if (is_invalid(table_idx))
		return NULL;
	table_idx = (table_idx + 1) % SCDXIPCI_NR_SG_TABLE;
	return &scd_dev->sg_data[table_idx];
}

static inline
struct sg_data *scdxipci_prev_sg_data(struct scdxipci_dev *scd_dev, 
				      struct sg_data *sg_data)
{
	unsigned long table_idx = scdxipci_sg_data_idx(scd_dev, sg_data);
	if (is_invalid(table_idx))
		return NULL;
	else if (table_idx == 0)
		table_idx = SCDXIPCI_NR_SG_TABLE;
	return &scd_dev->sg_data[table_idx - 1];
}

#define scdxipci_for_each_sg_data(sg_data, scd_dev)	    \
	for ((sg_data) = scdxipci_first_sg_data(scd_dev);   \
	     (sg_data) < (scdxipci_first_sg_data(scd_dev) + \
			  SCDXIPCI_NR_SG_TABLE);	    \
	     (sg_data)++)

static inline
struct sg_data *scdxipci_get_curr_sg_data(struct scdxipci_dev *scd_dev)
{
	return scd_dev->curr_sg_data;
}

static inline
void scdxipci_set_curr_sg_data(struct scdxipci_dev *scd_dev, 
			       struct sg_data *sg_data)
{
	void *ptr;
	u32 addr;
	scd_dev->curr_sg_data = sg_data;
	ptr = sg_data ? sg_data->sg_table.ptr : NULL;
	addr = (u32) (ptr ? virt_to_phys(ptr) : 0);
	scdxipci_write_reg(scd_dev, SCATTER_GATHER_ADDRESS_REGISTER, addr);
}

static inline
void scdxipci_clear_curr_sg_data(struct scdxipci_dev *scd_dev)
{
	scdxipci_set_curr_sg_data(scd_dev, NULL);
}

static inline
int system_has_page64(void)
{
	return (get_num_physpages() > 0x100000);
}

static inline
char *img_frame_name(char *name_buffer, struct img_frame *frame)
{
	sprintf(name_buffer, "%ld:%ld", frame->buffer->nr, frame->frame_nr);
	return name_buffer;
}

static inline
gfp_t img_frame_gfp_flags(int buffer_type)
{
	return (buffer_type == SCDXIPCI_BUFF_AUX) ? 
		(GFP_KERNEL | GFP_DMA32) : GFP_HIGHUSER;
}

static inline int img_frame_page_is64(struct img_frame_page *fpage) 
{
	u64 addr = page_to_phys(fpage->page);
	return ((addr & ((1LL << 32) - 1)) != addr);
}

static inline void img_frame_init(struct img_frame *frame)
{
	frame->acq_frame_nr = SCDXIPCI_INVALID;
	frame->round_count = SCDXIPCI_INVALID;
	frame->nr_pixels = 0;
	frame->time_stamp = ktime_set(0, 0);
	atomic_set(&frame->dev_count, 0);
}

static inline unsigned long img_frame_pages(struct img_frame *frame)
{
	return frame->nr_pages;
}

static inline unsigned long img_frame_size(struct img_frame *frame)
{
	return img_frame_pages(frame) * PAGE_SIZE;
}

static inline 
struct img_frame_page *img_frame_get_page(struct img_frame *frame, 
					  unsigned long page_nr)
{
	struct img_frame_page *fpage = frame->first_page;
	unsigned long i;
	if (page_nr >= img_frame_pages(frame))
		return NULL;
	for (i = 0; i != page_nr; i++)
		fpage = fpage->next;
	return fpage;
}


static inline
int img_frame_has_page64(struct img_frame *frame) 
{
	return (frame->nr_pages64 > 0);
}

static inline
struct img_frame *img_frame_from_list(struct list_head *list)
{
	return list_entry(list, struct img_frame, frame_list);
}

static inline
struct data_acq *img_frame_acq(struct img_frame *frame)
{
	return frame->buffer ? frame->buffer->acq : NULL;
}

static inline
struct img_frame *img_frame_get(struct img_frame *frame)
{
	struct img_buffer *buffer = frame->buffer;
	if (!buffer)
		return NULL;
	atomic_inc(&buffer->use_count);
	return frame;
}

static inline
void img_frame_put(struct img_frame *frame)
{
	struct img_buffer *buffer = frame->buffer;
	int use = atomic_read(&buffer->use_count);

	FENTRY("img_frame_put");

	if (use <= 0)
		DEB_ERRORS("%s use count is %d\n", buffer->name, use);

	if (atomic_dec_and_test(&buffer->use_count))
		wake_up_interruptible(&buffer->use_wq);
}

static inline
int img_buffer_is_used(struct img_buffer *buffer)
{
	struct data_acq *acq = buffer->acq;
	unsigned long flags;
	int used;

	FENTRY("img_buffer_is_used");

	if (!acq)
		return 0;

	scdxipci_lock_acq(acq, flags);
	used = atomic_read(&buffer->use_count);
	scdxipci_unlock_acq(acq, flags);

	DEB_PARAMS("%s: used=%d\n", buffer->name, used);

	return used;
}

static inline
void *img_buffer_uadd(struct img_buffer *buffer)
{
	return (void *) ((buffer && buffer->vma) ? buffer->vma->vm_start : 0);
}

static inline
struct img_buffer *acq_buffer(struct data_acq *acq, unsigned long buffer_nr)
{
	struct img_buffer **arr = acq->buffer_arr;
	return (arr && (buffer_nr < acq->nr_buffers)) ? arr[buffer_nr] : NULL;
}

static inline
struct img_buffer *img_buffer_from_vma(struct vm_area_struct *vma)
{
	return (struct img_buffer *) vma->vm_private_data;
}

static inline
struct img_buffer *img_buffer_from_finfo(struct data_acq *acq, 
					 struct img_frame_info *finfo)
{
	return acq_buffer(acq, finfo->buffer_nr);
}

static inline
struct img_frame *img_buffer_first_frame(struct img_buffer *buffer)
{
	return img_frame_from_list(buffer->frame_list.next);
}

static inline
struct img_frame *img_buffer_last_frame(struct img_buffer *buffer)
{
	return img_frame_from_list(buffer->frame_list.prev);
}

static inline
unsigned long img_buffer_round_count(struct img_buffer *buffer)
{
	return img_buffer_last_frame(buffer)->round_count;
}

static inline 
unsigned long img_buffer_frames(struct img_buffer *buffer)
{
	struct list_head *list;
	int nr_frames = 0;
	list_for_each(list, &buffer->frame_list)
		nr_frames++;
	return nr_frames;
}

static inline 
struct img_frame *img_buffer_get_frame(struct img_buffer *buffer, 
				       unsigned long frame_nr)
{
	struct list_head *list;
	struct img_frame *frame;

	FENTRY("img_buffer_get_frame");

	list_for_each(list, &buffer->frame_list) {
		frame = img_frame_from_list(list);
		if (frame->frame_nr == frame_nr)
			return frame;
	}

	DEB_ERRORS("%s has only %ld frames, asked frame %ld\n", buffer->name, 
		   img_buffer_frames(buffer), frame_nr);
	return NULL;
}

static inline 
void img_buffer_pages(struct img_buffer *buffer, 
			       unsigned long *nr_pages, 
			       unsigned long *nr_pages64)
{
	struct list_head *list;
	struct img_frame *frame;
	if (nr_pages)
		*nr_pages   = 0;
	if (nr_pages64)
		*nr_pages64 = 0;
	list_for_each(list, &buffer->frame_list) {
		frame = img_frame_from_list(list);
		if (nr_pages)
			*nr_pages   += frame->nr_pages;
		if (nr_pages64)
			*nr_pages64 += frame->nr_pages64;
	}
}

#define acq_get_buffer_check(buffer_nr, buffer, acq) \
	((buffer = acq_buffer(acq, buffer_nr)) || (buffer_nr < acq->nr_buffers))

#define acq_for_each_buffer(buffer_nr, buffer, acq) \
	for (buffer_nr = 0; \
	     acq_get_buffer_check(buffer_nr, buffer, acq); buffer_nr++)

#define acq_for_each_good_buffer(buffer_nr, buffer, acq) \
	acq_for_each_buffer(buffer_nr, buffer, acq)	 \
		if (buffer != NULL)

static inline
unsigned long acq_first_buffer_nr(struct data_acq *acq, int unused)
{
	struct img_buffer **ptr;
	unsigned long buffer_nr, max_buffer_nr;

	ptr = acq->buffer_arr;
	max_buffer_nr = acq->buffer_arr_len / sizeof(*ptr);
	for (buffer_nr = 0; buffer_nr < max_buffer_nr; buffer_nr++, ptr++)
		if ((unused != 0) == (*ptr == NULL))
			return buffer_nr;

	return SCDXIPCI_INVALID;
}

static inline
struct img_buffer *acq_first_buffer(struct data_acq *acq)
{
	unsigned long buffer_nr = acq_first_buffer_nr(acq, SCDXIPCI_USED);
	if (is_invalid(buffer_nr))
		return NULL;
	return acq->buffer_arr[buffer_nr];
}

static inline
struct img_buffer *acq_prev_buffer(struct data_acq *acq, 
				   struct img_buffer *buffer)
{
	unsigned long buffer_nr = buffer->nr;
	do {
		if (buffer_nr == 0) 
			buffer_nr = acq->nr_buffers;
		buffer = acq_buffer(acq, --buffer_nr);
	} while (buffer == NULL);

	return buffer;
}

static inline
struct img_buffer *acq_next_buffer(struct data_acq *acq, 
				   struct img_buffer *buffer)
{
	unsigned long buffer_nr = buffer->nr;
	do {
		if (++buffer_nr == acq->nr_buffers) 
			buffer_nr = 0;
		buffer = acq_buffer(acq, buffer_nr);
	} while (buffer == NULL);

	return buffer;
}


static inline
struct img_frame *acq_prev_frame(struct data_acq *acq, struct img_frame *frame)
{
	struct img_buffer *buffer = frame->buffer;

	if (frame == img_buffer_first_frame(buffer)) {
		buffer = acq_prev_buffer(acq, buffer);
		frame = img_buffer_last_frame(buffer);
	} else
		frame = img_frame_from_list(frame->frame_list.prev);
	return frame;
}


static inline
struct img_frame *acq_next_frame(struct data_acq *acq, struct img_frame *frame)
{
	struct img_buffer *buffer = frame->buffer;

	if (frame == img_buffer_last_frame(buffer)) {
		buffer = acq_next_buffer(acq, buffer);
		frame = img_buffer_first_frame(buffer);
	} else
		frame = img_frame_from_list(frame->frame_list.next);
	return frame;
}


static inline void acq_frame_nr_init(struct data_acq *acq)
{
	acq->acq_frame_nr = SCDXIPCI_INVALID;
}

static inline int scd_dev_is(struct scdxipci_dev *scd_dev, int board_type_id)
{
	return (scd_dev->board->type->type == board_type_id);
}

static inline int scd_dev_has_cap(struct scdxipci_dev *scd_dev, int cap)
{
	struct firmware_data *firmware = scd_dev->board->firmware;
	return firmware && ((firmware->capabilities & cap) == cap);
}


static inline int acq_dev_has_dma64(struct acq_dev *acq_dev)
{
	return (scd_dev_is(acq_dev->dev, SCDXIPCI_ESPIA_EXPRESS) &&
		(scd_dev_has_cap(acq_dev->dev, SCDXIPCI_CAP_GOOD_RESET_SG) ||
		 acq_dev->acq->keep_released_pages) && 
		!acq_dev->dev->no_dma64);
}

static inline
struct img_frame *acq_dev_get_map_frame(struct acq_dev *acq_dev, 
					struct img_frame *frame)
{
	unsigned long aux_frame_nr;
	char name[SCDXIPCI_FRAME_NAME_LEN];

	FENTRY("acq_dev_get_map_frame");

	if (img_frame_has_page64(frame) && !acq_dev_has_dma64(acq_dev)) {
		aux_frame_nr = acq_dev->aux_buffer_curr_frame;
		acq_dev->aux_buffer_curr_frame++;
		acq_dev->aux_buffer_curr_frame %= SCDXIPCI_NR_AUX_FRAMES;
		DEB_PARAMS("Frame %s has 64-bit pages, using aux. buffer "
			   "frame %ld\n", img_frame_name(name, frame), 
			   aux_frame_nr);
		frame = img_buffer_get_frame(acq_dev->acq->aux_buffer, 
					     aux_frame_nr);
	}

	return frame;
}

static inline
int acq_dev_map_frame(struct acq_dev *acq_dev, struct sg_data *sg_data,
		      struct img_frame *frame)
{
	struct img_frame *map_frame;
	int ret;

	map_frame = acq_dev_get_map_frame(acq_dev, frame);
	if (map_frame == NULL)
		return -EINVAL;

	ret = scdxipci_pci_map_frame(acq_dev->dev, sg_data, map_frame);
	if (ret >= 0)
		sg_data->buffer_frame = frame;
	return ret;
}

static inline
unsigned long acq_dev_frame_dwords(struct acq_dev *acq_dev, 
				   struct sg_data *sg_data,
				   unsigned long dma_dwords)
{
	unsigned long frame_dwords, block_dwords, i, sg_len;
	struct scdxipci_sg_block *block;

	sg_len = acq_dev->sg_table.len;
	if (sg_len == 0)
		return dma_dwords;
	else if (dma_dwords == sg_data->dma_dwords)
		return sg_data->frame_dwords;
	
	frame_dwords = 0;
	block = acq_dev->sg_table.ptr;
	for (i = 0; (dma_dwords > 0) && (i < sg_len); i++, block++) {
		block_dwords = block->size;
		if (dma_dwords < block_dwords)
			block_dwords = dma_dwords;
		if (!is_invalid32(block->address))
			frame_dwords += block_dwords;
		dma_dwords -= block_dwords;
	}

	return frame_dwords;
}

static inline
unsigned long acq_inc_run_nr(struct data_acq *acq)
{
	return ++acq->acq_run_nr;
}

static inline
unsigned long acq_get_run_nr(struct data_acq *acq)
{
	return acq->acq_run_nr;
}

static inline 
struct data_acq *data_acq_from_file(struct file *file)
{
	return (struct data_acq *) file->private_data;
}

static inline
int is_current_acq_dev(struct acq_dev *acq_dev)
{
	return (acq_dev->dev && (acq_dev == acq_dev->dev->curr_acq));
}

static inline
int is_current_acq_dev_safe(struct acq_dev *acq_dev)
{
	unsigned long flags, curr_acq;
	struct scdxipci_dev *scd_dev = acq_dev->dev;
	if (scd_dev == NULL)
		return 0;
	scdxipci_lock_dev(scd_dev, flags);
	curr_acq = is_current_acq_dev(acq_dev);
	scdxipci_unlock_dev(scd_dev, flags);
	return curr_acq;
}

static inline
int is_current_acq(struct data_acq *acq) 
{
	return acq->dev && is_current_acq_dev_safe(acq->dev);
}

static inline
int is_empty_acq(struct data_acq *acq)
{
	return (acq_first_buffer(acq) == NULL);
}

static inline
void scdxipci_wakeup_all(struct data_acq *acq) 
{
	scdxipci_wakeup(acq, NULL);
}

static inline
int scdxipci_thread_is_waiting(struct data_acq *acq, unsigned long thread_id)
{
	int found = 0;
	unsigned long flags;
	struct list_head *list;
	struct acq_wait *wait;

	scdxipci_lock_acq(acq, flags);

	list_for_each(list, &acq->wait_list) {
		wait = list_entry(list, struct acq_wait, wait_list);
		if (wait->thread_id == thread_id) {
			found = 1;
			break;
		}
	}
	scdxipci_unlock_acq(acq, flags);

	return found;
}

static inline
int scdxipci_memcpy_thread_active(struct data_acq *acq)
{
	return acq->memcpy_thread != NULL;
}

static inline 
void scdxipci_check_kept_pages(struct data_acq *acq)
{
	struct img_frame_page *fpage;
	int i, bad;
	unsigned char *p;

	FENTRY("scdxipci_check_kept_pages");
	DEB_PARAMS("Checking %ld released pages\n", acq->nr_deleted_pages);
	while (acq->deleted_frame_pages) {
		acq->nr_deleted_pages--;
		fpage = acq->deleted_frame_pages;
		bad = 0;
		acq->deleted_frame_pages = fpage->next;
		p = kmap(fpage->page);
		for (i = 0; i < PAGE_SIZE; i++)
			if (p[i] != SCDXIPCI_RELEASED_PAGE_MARKER)
				bad++;
		if (bad > 0)
			DEB_ERRORS("!!!! Page %ld has %d bad bytes !!!!\n", 
				   acq->nr_deleted_pages, bad);
		kunmap(fpage->page);
		scdxipci_frame_page_free(fpage);
	}
	if (acq->nr_deleted_pages != 0)
		DEB_ERRORS("!!!! Ooops nr_deleted_pages=%ld !!!!\n",
			   acq->nr_deleted_pages);
}

static inline
unsigned long scdxipci_timeout(unsigned long timeout)
{
	return circ_buffer_timeout(timeout);
}

static inline
unsigned long scdxipci_elapsed_usec(ktime_t t0, ktime_t t)
{
	return ktime_to_ns(ktime_sub(t, t0)) / 1000;
}

static inline
void scdxipci_unmap_unavailable_frame(struct scdxipci_dev *scd_dev, 
				      struct img_frame *frame)
{
	struct sg_data *sg_data;
	scdxipci_for_each_sg_data(sg_data, scd_dev)
		if (frame == sg_data->mapped_frame)
			scdxipci_pci_unmap_frame(scd_dev, sg_data);
}

static inline
void scdxipci_unmap_acq(struct scdxipci_dev *scd_dev, struct data_acq *acq)
{
	struct sg_data *sg_data;
	struct img_frame *frame;
	scdxipci_for_each_sg_data(sg_data, scd_dev) {
		frame = sg_data->mapped_frame;
		if (frame && (frame->buffer->acq == acq))
			scdxipci_pci_unmap_frame(scd_dev, sg_data);
	}
}

#ifndef ESRFVER_NO_CLASS_DEV

static inline 
struct scdxipci_dev *scdxipci_dev_from_class(ESRFVER_CLASS_DEV_TYPE *class_dev) 
{
	return (struct scdxipci_dev *) ESRFVER_CLASS_DEV_GET_DATA(class_dev);
}

static inline
dev_t scdxipci_cdev_nr_from_class(ESRFVER_CLASS_DEV_TYPE *class_dev)
{
	struct scdxipci_dev *scd_dev = scdxipci_dev_from_class(class_dev);
	int is_meta = (scd_dev == NULL);
	int dev_nr = is_meta ? SCDXIPCI_META_DEV : scd_dev->nr;
	return MKDEV(major, dev_nr);
}

#endif


#endif /* _SCDXIPCI_PRIVATE_H */
