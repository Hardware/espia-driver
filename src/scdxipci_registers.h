/****************************************************************************
 * File:	scdxipci_registers.h
 * $Header: /segfs/bliss/cvs/driver/ESRF/espia/src/scdxipci_registers.h,v 2.9 2008/02/08 14:20:41 ahoms Exp $
 * Project:	SECAD cards Linux & Windows drivers
 * Description:	Definition of card registers
 * Author(s):	SECAD S.A.
 ****************************************************************************/

#ifndef _SCDXIPCI_REGISTERS_H
#define _SCDXIPCI_REGISTERS_H

/*--------------------------------------------------------------------------
 *          This file is included in both Window and Linux sources
 *--------------------------------------------------------------------------*/

// Boards status register definition
#define STATUS_REGISTER				0
#define INTERRUPT				(1<<0)
#define INTERRUPT_DMA				(1<<1)
#define INTERRUPT_SCATTER_GATHER		(1<<2)
#define DATA_FLASH_FPGA_READY			(1<<3)
#define FPGA_AUX_DONE				(1<<4)
#define FPGA_AUX_INIT				(1<<5)
#define TRANSFERT_ERROR				(1<<8)

/* SWINGO Board*/
#define SWINGO_SPI_READY			(1<<16)

/* ESPIA Board*/
#define DCM_LOCKED_LED 				(1<<16)
#define DATA_VLD_LED				(1<<17)
#define CHAN_READY_LED				(1<<18)
#define SLOT_64					(1<<19)
#define CHAN_UP_LED				(1<<20)
#define AURORA_HARD_ERROR			(1<<21)
#define AURORA_SOFT_ERROR			(1<<22)
#define AURORA_FRAME_ERROR			(1<<23)
#define TAMPON_OVERFLOW				(1<<24)
#define SERIAL_DATA_AVAILABLE			(1<<25)
#define	STATUS_CHANGED				(1<<26)
#define	INTERRUPT_USER				(1<<27)

// control register definition
#define CONTROL_REGISTER			(4*1)
#define INTERRUPT_DMA_ON			(1<<0) 
#define DMA_START				(1<<1) 
#define DMA_RAM					(1<<2) 
#define DMA_READ				(1<<3)	// READ from PC RAM 
							// to Board
#define INTERRUPT_SCATTER_GATHER_ON		(1<<4) 
#define SCATTER_GATHER_START			(1<<5) 
#define TEST_COUNTER				(1<<6) 
#define USE_BURST				(1<<7) 
#define DATA_FLASH_ON				(1<<8) 
#define FPGA_AUX_DEACTIVATE_PROG		(1<<9)
#define DATA_FROM_PCI_FPGA			(1<<10)
#define USE_64_BITS				(1<<11)
#define RESET_SCATTER_GATHER			(1<<12)

/* SWINGO Board CONTROL REGISTER Extensions*/
#define SWINGO_MASQUE_ADD_RESET			(1<<16)

/* ESPIA Board CONTROL REGISTER Extensions*/
#define USER_RESET_NOT				(1<<16)
#define RESET_LINK				(1<<17)
#define MODE_0					(1<<18)
#define MODE_1					(1<<19)
#define ENA_TX					(1<<20)
#define DECODE_ESPIA				(1<<21)
#define CLEAR_ERRORS				(1<<22)
#define COMPTEUR_TEST_TAMPON			(1<<23)
#define USE_EOI					(1<<24)
#define INTERRUPT_ON_SERIAL			(1<<25)
#define INTERRUPT_ON_SERIAL_CLEAR		(1<<26)
#define INTERRUPT_ON_STATUS_CHANGE 		(1<<27)
#define INTERRUPT_ON_STATUS_CHANGE_CLEAR	(1<<28)
// 29
#define DO_NOT_RESET_FIFOS			(1<<30)
#define ESPIA_125MHz				(1<<31)


#define BUFFER_ADDRESS_REGISTER			(4*2)
#define DMA_SIZE_REGISTER			(4*3)	// in BYTES 


/* This register must contain the address an array of structures of type */

struct scdxipci_sg_block {
	unsigned int		 size;
	unsigned int		 address;
};

#define SCATTER_GATHER_ADDRESS_REGISTER		(4*4)	// in BYTES 

#define DISCONNECT_REGISTER			(4*5)	// in BYTES 
#define RETRY_REGISTER				(4*6)	// in BYTES 

/* Boards with data Flash */
#define DATA_FLASH_REGISTER			(4*7)	// in BYTES

/* Boards with Flash EPROM */
#define FLASH_EPROM_FLASH_ADD_REGISTER		(4*7)	// in BYTES
#define FLASH_EPROM_FLASH_DATA_REGISTER		(4*8)	// in BYTES


/* Boards with a second FPGA*/
#define DATA_FPGA_AUX_REGISTER			(4*8)	// in BYTES


/* SWINGO REGISTERS*/
#define SWINGO_SEUIL_REGISTER			(4*9)	// DWORD

#define SWINGO_MODE_REGISTER			(4*10)	// DWORD
#define SWINGO_MODE_FLASH			(1<<0)
#define SWINGO_MODE_SYNCH(x)			((x) & 3)<<2)
#define SWINGO_MODE_SYNCH_EXT			(1<<3)
#define SWINGO_MODE_EXTRACT_SYNCH		(1<<4)
#define SWINGO_MODE_SIMULATION			(1<<5)
#define SWINGO_MODE_HAUTE_RESOLUTION		(1<<6)
#define SWINGO_MODE_PASSE_BAS			(1<<7)

#define SWINGO_MASQUE_REGISTER			(4*11)	// DWORD	


/* ESPIA REGISTERS*/
#define ESPIA_SERIAL_STATUS			(4*9)
#define ESPIA_SERIAL_WRITE			(1<<16)
#define ESPIA_SERIAL_FIFO_EMPTY			(1<<16)
#define ESPIA_SERIAL_FIFO_FULL			(1<<17)
#define ESPIA_SERIAL_FIFO_SIZE			(255)

#define ESPIA_PIXEL_COUNTER			(4*10)


/* IPCIPLUS */
/* CONTROL REGISTER EXTENSIONS */
#define IPCIPLUS_VIDEO_RESET			(1<<(16+0))
#define USE_ADC_NOT				(1<<(16+1))
#define FORMAT_8_bits				(0<<(16+2))
#define FORMAT_16_bits				(1<<(16+2))
#define FORMAT_32_bits				(2<<(16+2))

#define RJ45_MODE_MASK				(3<<(16+5))
#define RJ45_AS_IO				(0<<(16+5))
#define RJ45_AS_MODES				(1<<(16+5))
#define RJ45_AS_MODES_LOAD			(2<<(16+5))

#define READ_SDRAM				(1<<(16+7))
#define COLOR_THRESHOLD				(1<<(16+8))
#define NO_PARITY				(1<<(16+9))



/* REGISTRES IPCIPLUS */
#define IPCIPLUS_CROPPING_X			0x24
#define IPCIPLUS_IO_REGISTER			0x28

/*
MODE : RJ45_AS_IO
Bits   7..6   : Inputs
       5..0   : Outputs

MODE : RJ45_AS_MODES
Write: 19..0  : REG_MODES
Read : 31     : Ready
       29..28 : ErrorCode
       7..0   : Data
*/

#define IPCIPLUS_CROPPING_Y                                   0x2c
#define IPCIPLUS_SELCAM_CLKPLL                                0x30
// Bits                       Fonction
// 11..0                      DIVISEUR DU PLL
// 13..12                     MUX BT C        0: J1 ..  3:J4
// 15..14                     MUX BT Y        0: J1 ..  3:J4
// 17..16                     MUX ADC         0: J1 ..  3:J4
#define IPCIPLUS_SDRAM_DATA			0x34
#define IPCIPLUS_CNT_FRAME			0x38
#define IPCIPLUS_SDRAM_ADD			0x3C

#define IPCI_TEST_VALID				0x8d71622f


#define PROTO					0x87119ef7
#define TESTEE					0x8da1622f

struct scdxipci_card_id {
	unsigned int NumeroSerie;
	unsigned int CODE_TEST;
	unsigned int libre[64];
	unsigned int CRC;
};

struct scdxipci_card_id_v1 {
	unsigned int NumeroSerie;
	unsigned int CODE_TEST;
	unsigned int libre[64];
};

#define t_CARTE_ID	scdxipci_card_id
#define t_CARTE_ID_VI	scdxipci_card_id_v1

#define TAILLE_BLOC_CODE_FPGA			0x60000L
#define TAILLE_BLOC_CODE_CHECKSUM		(93953 * 4)

#define ADRESSE_ID	\
	(TAILLE_BLOC_CODE_FPGA - sizeof(struct scdxipci_card_id))


#endif /*_SCDXIPCI_REGISTERS_H */
