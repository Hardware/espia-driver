/****************************************************************************
 * File:	espia_test_main.c
 * $Header: /segfs/bliss/cvs/driver/ESRF/espia/src/espia_test_main.c,v 2.8 2008/10/22 17:04:33 ahoms Exp $
 * Project:	SECAD Espia card Linux driver
 * Description:	Espia library test program main function
 * Author(s):	SECAD S.A.
 *		A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 ****************************************************************************/

#include "espia_test.h"

// the program name
char *progname;


void usage();


/*--------------------------------------------------------------------------
 * voila the main program
 *--------------------------------------------------------------------------*/

int main(int argc, char **argv)
{
	app_context_t context;
	char *ptr;
	int ret, aux;

	progname = argv[0];
	while ((ptr = strchr(progname, '/')) != NULL)
		progname = ++ptr;

	if (argc > 2)
		usage();

	context = alloc_context("espia_test", argc, argv);
	if (context == CONTEXT_INVALID) {
		fprintf(stderr, "Error allocating context\n");
		return 1;
	}

	ret = test_init(context, argc, argv);
	if (ret != 0)
		goto out;

	ret = exec_application(context);

	aux = test_cleanup(context);
	if ((aux != 0) && (ret == 0))
		ret = aux;

 out:
	if (ret < 0)
		free_context(context);
	return (ret == 0) ? 0 : 1;
}


/*--------------------------------------------------------------------------
 * usage
 *--------------------------------------------------------------------------*/

void usage()
{
	fprintf(stderr, " Usage: %s [dev_nr]\n", progname);
	exit(1);
}


