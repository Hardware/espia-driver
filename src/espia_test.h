/****************************************************************************
 * File:	espia_test.h
 * $Header: /segfs/bliss/cvs/driver/ESRF/espia/src/espia_test.h,v 2.25 2020/07/23 11:00:41 ahoms Exp $
 * Project:	SECAD Espia card Linux driver
 * Description:	Library test program routines header
 * Author(s):	SECAD S.A.
 *		A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 ****************************************************************************/

#ifndef _ESPIA_TEST_H
#define _ESPIA_TEST_H

#define _GNU_SOURCE

#include "espia_lib.h"
#include "esrfdebug.h"
#include "app_framework.h"
#ifdef WITH_IMAGE
#include "imageapi.h"
#endif // WITH_IMAGE
#include "focla_lib.h"

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/mman.h>


/*--------------------------------------------------------------------------
 * application specific data
 *--------------------------------------------------------------------------*/

#define ESPIA_TEST_ERR_BASE	(ESPIA_ERR_BASE - 500)
#define ESPIA_TEST_ERR_OPEN	(ESPIA_TEST_ERR_BASE - 1)
#define ESPIA_TEST_ERR_NOOPEN	(ESPIA_TEST_ERR_BASE - 2)
#define ESPIA_TEST_ERR_DEVNR	(ESPIA_TEST_ERR_BASE - 3)
#define ESPIA_TEST_ERR_SELECT	(ESPIA_TEST_ERR_BASE - 4)
#define ESPIA_TEST_ERR_NOMEM	(ESPIA_TEST_ERR_BASE - 5)
#define ESPIA_TEST_ERR_CBNR	(ESPIA_TEST_ERR_BASE - 6)
#define ESPIA_TEST_ERR_CBTYPE	(ESPIA_TEST_ERR_BASE - 7)
#define ESPIA_TEST_ERR_CBREG	(ESPIA_TEST_ERR_BASE - 8)
#define ESPIA_TEST_ERR_IMGINIT	(ESPIA_TEST_ERR_BASE - 9)
#define ESPIA_TEST_ERR_IMGSIZE	(ESPIA_TEST_ERR_BASE - 10)
#define ESPIA_TEST_ERR_IMGCRT	(ESPIA_TEST_ERR_BASE - 11)
#define ESPIA_TEST_ERR_IMGEXST	(ESPIA_TEST_ERR_BASE - 12)
#define ESPIA_TEST_ERR_IMGMISS	(ESPIA_TEST_ERR_BASE - 13)
#define ESPIA_TEST_ERR_PARAM	(ESPIA_TEST_ERR_BASE - 14)
#define ESPIA_TEST_ERR_FRMOP	(ESPIA_TEST_ERR_BASE - 15)
#define ESPIA_TEST_ERR_CBFUNCT  (ESPIA_TEST_ERR_BASE - 16)
#define ESPIA_TEST_ERR_NOBUFFER (ESPIA_TEST_ERR_BASE - 17)
#define ESPIA_TEST_ERR_ROIACC   (ESPIA_TEST_ERR_BASE - 18)
#define ESPIA_TEST_ERR_DUMPPAR  (ESPIA_TEST_ERR_BASE - 19)
#define ESPIA_TEST_ERR_OPTION   (ESPIA_TEST_ERR_BASE - 20)
#define ESPIA_TEST_ERR_MAPINIT  (ESPIA_TEST_ERR_BASE - 21)
#define ESPIA_TEST_ERR_MAPSIZE  (ESPIA_TEST_ERR_BASE - 22)
#define ESPIA_TEST_ERR_BUFFSAVE (ESPIA_TEST_ERR_BASE - 23)
#define ESPIA_TEST_ERR_FOCLASIG (ESPIA_TEST_ERR_BASE - 24)
#define ESPIA_TEST_ERR_EDFFORM  (ESPIA_TEST_ERR_BASE - 25)
#define ESPIA_TEST_ERR_INTERNAL (ESPIA_TEST_ERR_BASE - 26)
#define ESPIA_TEST_ERR_MAXFRMOP	(ESPIA_TEST_ERR_BASE - 27)

#define ESPIA_TEST_MAX_NR_DEV	(2 * ESPIA_MAX_NR_DEV)
#define ESPIA_TEST_MAX_OPS	64

#define ESPIA_TEST_POLL_TIME	10000	// us

#define FILE_PREFIX_NAME_LEN	512
#define FILE_SUFFIX_NAME_LEN	64
#define FILE_FULL_NAME_LEN	(FILE_PREFIX_NAME_LEN + 4 + \
				 FILE_SUFFIX_NAME_LEN + 4)

#define EDF_HEADER_LEN		1024
#define EDF_HEADER_BUFFER_LEN	(10 * EDF_HEADER_LEN)

#define ROI_ACC_BUFFER		(-2)

#define ESPIA_TEST_MIN_BUFFER_SIZE	(128 * 1024)

enum {
	FMT_RAW,
	FMT_EDF,
};


#define is_good_dev_nr(n)	((n >= 0) && (n < ESPIA_TEST_MAX_NR_DEV))

struct device_data;
struct cb_priv_data;
struct image_data;

typedef int gen_cb(app_context_t context, 
		   struct device_data *dev_data,
		   struct cb_priv_data *priv_data,
		   struct espia_cb_data *cb_data);

#ifdef WITH_IMAGE

typedef int img_cb(app_context_t context, 
		   struct device_data *dev_data,
		   struct image_data *img_data,
		   struct espia_cb_data *cb_data);

typedef void img_del(app_context_t context, 
		     struct device_data *dev_data,
		     struct image_data *img_data);

struct image_data {
	app_context_t		 context;
	image_t			 img;
	struct device_data	*dev_data;
	int			 cb_nr;
	void			*buffer_ptr;
	img_cb			*cb_funct;
	img_del			*del_funct;
};

#endif // WITH_IMAGE

struct buffer_data {
#ifdef WITH_IMAGE
	struct image_data	 img;
#endif // WITH_IMAGE
};

typedef struct espia_frm_op_fn_data op_fn_data;

struct image_roi {
	int			 col_beg;
	int			 col_end;
	int			 row_beg;
	int			 row_end;
};

struct last_frame_info {
	struct img_frame_info	 finfo;
	pthread_mutex_t		 lock;
};

struct buffer_save_data {
	struct device_data	*dev_data;
	char			 prefix[FILE_PREFIX_NAME_LEN];
	int			 idx;
	char			 suffix[FILE_SUFFIX_NAME_LEN];
	int			 format;
	char			 file_name[FILE_FULL_NAME_LEN];
	int			 file_frames;
	int			 tot_file_frames;
	int			 overwrite;
	int			 print_name;
	FILE			*fout;
	struct last_frame_info	 last_frame;
};

struct roi_acc_buffer {
	void			*buffer_ptr;
#ifdef WITH_IMAGE
	struct image_data	 img_data;
#endif // WITH_IMAGE
	struct image_roi	 roi;
	int			 nr_frames;
	int			 real_nr_frames;
	struct espia_frame_dim	 frame_dim;
	char			 file_prefix[FILE_PREFIX_NAME_LEN];
	int			 file_idx;
};

struct cb_priv_data {
	app_context_t		 context;
	struct device_data	*dev_data;
	gen_cb			*cb_funct;
	int			 user_cb;
	int			 print_raw;
	int			 filter_tout;
#ifdef WITH_IMAGE
	struct image_data	*img_data;
#endif // WITH_IMAGE
	struct img_frame_info	 req_finfo;
};

struct serial_mmap_data {
	short			 init;
	int 			 max_bytes;
	int			 mem_size;
	int			 write_file;
	int			 read_file;
	void			*write_mmap;
	void			*read_mmap;
};

struct device_data {
	int			 dev_nr;
	espia_t			 dev;
	focla_t			 focla;
	struct buffer_data	*buff_data;
	int			 nr_buffer;
	int			 buffer_frames;
	int			 real_frame_factor;
	int			 real_frame_size;
	struct espia_frame_dim	 frame_dim;
	op_fn_data		 post_op_fn_data;
	struct cb_priv_data    **cbs;
	int			 nr_cb;
	struct last_frame_info	 last_frame;
	int			 last_frame_cb;
	struct buffer_save_data  buff_save_data;
	int			 buff_save_cb;
	pthread_mutex_t		 buff_save_lock;
	struct roi_acc_buffer	 roi_acc_buff;
	int			 roi_acc_buff_cb;
#ifdef WITH_IMAGE
	struct image_data	 live_img;
#endif // WITH_IMAGE
	struct serial_mmap_data	 serial_mmap;
	char                    *edf_usr_str;
	pthread_mutex_t		 edf_str_lock;
};

struct app_data {
	int			 active_dev;
	struct device_data	 device[ESPIA_TEST_MAX_NR_DEV];
	int                      saved_argc;
	char                   **saved_argv;
	int                      image_init_flag;
};


static inline
void img_roi_reset(struct image_roi *roi)
{
	roi->col_beg = roi->col_end = roi->row_beg = roi->row_end = -1;
}

static inline
int img_roi_width(struct image_roi *roi)
{
	return roi->col_end - roi->col_beg + 1;
}

static inline
int img_roi_height(struct image_roi *roi)
{
	return roi->row_end - roi->row_beg + 1;
}

static inline
int img_roi_valid(struct image_roi *roi, struct espia_frame_dim *frame_dim)
{
	if ((roi->col_beg < 0) || (img_roi_width(roi)  <= 0) ||
	    (roi->row_beg < 0) || (img_roi_height(roi) <= 0))
		return 0;

	if (frame_dim == NULL)
		return 1;

	return ((roi->col_end < (int) frame_dim->width) && 
		(roi->row_end < (int) frame_dim->height));
}

static inline
int img_roi_size(struct image_roi *roi)
{
	return img_roi_width(roi) * img_roi_height(roi);
}

#define app_data_from_context(c) ((struct app_data *) context_app_data(c))

static inline
struct device_data *dev_data_from_context(app_context_t context)
{
	struct app_data *appdata = app_data_from_context(context);
	int actdev = appdata->active_dev;
	return (actdev >= 0) ? &appdata->device[actdev] : NULL;
}

#define priv_data_from_cb_data(cb_data) \
	((struct cb_priv_data *) (cb_data)->data)

#define get_cb_nr(dev_data, cb_nr) \
	((((cb_nr) >= 0) && ((cb_nr) < (dev_data)->nr_cb)) ?	\
	 (dev_data)->cbs[cb_nr] : NULL)

static inline
int is_user_cb(struct device_data *dev_data, int cb_nr)
{
	struct cb_priv_data *priv_data = get_cb_nr(dev_data, cb_nr);
	return priv_data && priv_data->user_cb;
}

#define buffer_is_live(buffer_nr) \
	scdxipci_is(ANY, buffer_nr)
#define buffer_is_roi_acc(buffer_nr) \
	((buffer_nr) == ROI_ACC_BUFFER)

static inline 
void init_cb_priv_data(struct cb_priv_data *priv_data)
{
	memset(priv_data, 0, sizeof(*priv_data));
}

#ifdef WITH_IMAGE

static inline
struct image_data *image_data_from_buffer_nr(struct device_data *dev_data, 
					     int buffer_nr)
{
	if (buffer_is_roi_acc(buffer_nr))
		return &dev_data->roi_acc_buff.img_data;
	else if (buffer_is_live(buffer_nr))
		return &dev_data->live_img;
	else if ((buffer_nr >= 0) && (buffer_nr < dev_data->nr_buffer))
		return &dev_data->buff_data[buffer_nr].img;
	else
		return NULL;
}

#define get_image_data_check(img_data, dev_data, buffer_nr) \
{\
	img_data = image_data_from_buffer_nr(dev_data, buffer_nr); \
	if (img_data == NULL) \
		return SCDXIPCI_ERR_BUFFER; \
}

static inline
void image_data_init(struct image_data *img_data)
{
	img_data->img = IMAGE_INVALID;
	img_data->cb_nr = (int) SCDXIPCI_INVALID;
	img_data->buffer_ptr = NULL;
}

#define BUFFER_IMG_NAME_LEN	64

#endif // WITH_IMAGE

static inline
int has_virtual_buffers(struct device_data *dev_data)
{
	return (dev_data->real_frame_factor != 1);
}

static inline
int real_buffer_nr(struct device_data *dev_data, int virt_buffer,
		   int virt_frame)
{
	return virt_buffer / dev_data->real_frame_factor;
}

static inline
int real_frame_nr(struct device_data *dev_data, int virt_buffer,
		  int virt_frame)
{
	return virt_buffer % dev_data->real_frame_factor + virt_frame;
}

static inline
int virt_buffer_nr(struct device_data *dev_data, int real_buffer,
		      int real_frame)
{
	return (real_buffer * dev_data->real_frame_factor + 
		real_frame / dev_data->buffer_frames);
}

static inline
int virt_frame_nr(struct device_data *dev_data, int real_buffer,
		     int real_frame)
{
	return real_frame % dev_data->buffer_frames;
}

static inline
void *virt_buffer_ptr(struct device_data *dev_data, void *real_buffer_ptr,
		      int virt_buffer_nr)
{
	char *ptr = (char *) real_buffer_ptr;
	int frame_nr = real_frame_nr(dev_data, virt_buffer_nr, 0);
	return ptr + frame_nr * dev_data->real_frame_size;
}


static inline
int virt_buffer_info(struct device_data *dev_data, int virt_buffer, 
		     struct img_buffer_info *binfo)
{
	int ret, real_buffer;

	real_buffer = real_buffer_nr(dev_data, virt_buffer, 0);
	ret = espia_buffer_info(dev_data->dev, real_buffer, binfo);
	if (ret < 0)
		return ret;

	binfo->ptr = virt_buffer_ptr(dev_data, binfo->ptr, virt_buffer);
	binfo->nr = virt_buffer;
	binfo->nr_frames /= dev_data->real_frame_factor;
	binfo->frame_size = espia_frame_mem_size(&dev_data->frame_dim);

	return ret;
}

static inline
void virt_frame_info(struct device_data *dev_data, 
		     struct img_frame_info *finfo)
{
	int real_buffer = finfo->buffer_nr;
	int real_frame  = finfo->frame_nr;
	finfo->buffer_nr  = virt_buffer_nr(dev_data, real_buffer, real_frame);
	finfo->frame_nr   = virt_frame_nr(dev_data, real_buffer, real_frame);
	finfo->buffer_ptr = virt_buffer_ptr(dev_data, finfo->buffer_ptr,
					    finfo->buffer_nr);
}

static inline
int cb_needs_virtual_conv(struct device_data *dev_data,
			  struct espia_cb_data *cb_data)
{
	return ((cb_data->type == ESPIA_CB_ACQ) && 
		has_virtual_buffers(dev_data));
}

static inline
char *buffer_img_name(char *name, struct device_data *dev_data, int buffer_nr)
{
	int bytes = sprintf(name, "Dev. #%d ", dev_data->dev_nr);
	if (buffer_is_roi_acc(buffer_nr))
		strcat(name, "RoI Acc. Buffer");
	else if (buffer_is_live(buffer_nr))
		strcat(name, "Live");
	else
		sprintf(name + bytes, "Buffer #%d", buffer_nr);
	return name;
}

static inline
struct espia_frame_dim *frame_dim_from_buffer_nr(struct device_data *dev_data,
						 int buffer_nr)
{
	struct roi_acc_buffer *roi_acc = &dev_data->roi_acc_buff;
	return buffer_is_roi_acc(buffer_nr) ? &roi_acc->frame_dim : 
					      &dev_data->frame_dim;
}

static inline
int buffer_info_from_nr(struct device_data *dev_data, int buffer_nr,
			  struct img_buffer_info *binfo)
{
	struct roi_acc_buffer *roi_acc;
	struct espia_frame_dim *fdim;

	if (!buffer_is_roi_acc(buffer_nr))
		return virt_buffer_info(dev_data, buffer_nr, binfo);

	roi_acc = &dev_data->roi_acc_buff;
	fdim = &roi_acc->frame_dim;
	binfo->nr = buffer_nr;
	binfo->nr_frames = 1;
	binfo->frame_size = espia_frame_mem_size(fdim);
	binfo->ptr = roi_acc->buffer_ptr;
	return ESPIA_OK;
}


#define auto_save_active(dev_data) \
	(!scdxipci_is(INVALID, (dev_data)->buff_save_cb))

static inline
int roi_acc_auto_save_active(struct device_data *dev_data)
{
	if (!auto_save_active(dev_data))
		return 0;
	return strlen(dev_data->roi_acc_buff.file_prefix) > 0;
}


static inline
void buffer_save_lock_init(struct device_data *dev_data)
{
	pthread_mutexattr_t mutex_attr;

	pthread_mutexattr_init(&mutex_attr);
	pthread_mutexattr_settype(&mutex_attr, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&dev_data->buff_save_lock, &mutex_attr);
	pthread_mutexattr_destroy(&mutex_attr);
}


static inline
int buffer_save_lock(struct device_data *dev_data)
{
	if (dev_data == NULL)
		return ESPIA_TEST_ERR_BUFFSAVE;
	pthread_mutex_lock(&dev_data->buff_save_lock);
	return 0;
}


static inline
void buffer_save_unlock(struct device_data *dev_data)
{
	pthread_mutex_unlock(&dev_data->buff_save_lock);
}


static inline
void edf_str_lock_init(struct device_data *dev_data)
{
	pthread_mutexattr_t mutex_attr;

	pthread_mutexattr_init(&mutex_attr);
	pthread_mutexattr_settype(&mutex_attr, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&dev_data->edf_str_lock, &mutex_attr);
	pthread_mutexattr_destroy(&mutex_attr);
}


static inline
int edf_str_lock(struct device_data *dev_data)
{
	if (dev_data == NULL)
		return ESPIA_TEST_ERR_NOOPEN;
	pthread_mutex_lock(&dev_data->edf_str_lock);
	return 0;
}


static inline
void edf_str_unlock(struct device_data *dev_data)
{
	pthread_mutex_unlock(&dev_data->edf_str_lock);
}


static inline
void last_frame_reset(struct last_frame_info *last_frame)
{
	struct img_frame_info *finfo = &last_frame->finfo;
	pthread_mutex_lock(&last_frame->lock);
	finfo->buffer_nr   = finfo->frame_nr     = SCDXIPCI_INVALID;
	finfo->round_count = finfo->acq_frame_nr = SCDXIPCI_INVALID;
	pthread_mutex_unlock(&last_frame->lock);
}

static inline
void last_frame_init(struct last_frame_info *last_frame)
{
	pthread_mutex_init(&last_frame->lock, NULL);
	last_frame_reset(last_frame);
}

static inline
void last_frame_update(struct last_frame_info *last_frame,
		       struct img_frame_info *finfo)
{
	pthread_mutex_lock(&last_frame->lock);
	last_frame->finfo = *finfo;
	pthread_mutex_unlock(&last_frame->lock);
}

static inline
void last_frame_get(struct last_frame_info *last_frame,
		    struct img_frame_info *finfo)
{
	pthread_mutex_lock(&last_frame->lock);
	*finfo = last_frame->finfo;
	pthread_mutex_unlock(&last_frame->lock);
}


static inline
int last_frame_available(struct img_frame_info *finfo)
{
	int avail;
	avail = !(scdxipci_is(INVALID, finfo->buffer_nr)   &&
		  scdxipci_is(INVALID, finfo->frame_nr)    &&
		  scdxipci_is(INVALID, finfo->round_count) &&
		  scdxipci_is(INVALID, finfo->acq_frame_nr));
	return avail;
}


/*--------------------------------------------------------------------------
 * function prototypes
 *--------------------------------------------------------------------------*/

int test_init(app_context_t context, int argc, char *argv[]);
int test_cleanup(app_context_t context);

char *espia_test_strerror(int ret);
err_funct test_error_funct;

app_funct open_dev, close_dev;
app_funct select_dev, dev_list;
app_funct option_list, get_option, set_option, set_debug_level;
app_funct read_register, write_register, param_list, get_param, set_param;
app_funct reset_link;
app_funct serial_read, serial_read_str, serial_write, serial_flush;
app_funct serial_mmap_init, serial_mmap_close, serial_mmap_write, 
	  serial_mmap_read;
app_funct frame_post_op, frame_post_op_list;
app_funct buffer_alloc, buffer_free, buffer_list, buffer_dump, buffer_save;
app_funct buffer_clear;
app_funct buffer_img, buffer_img_del, buffer_img_del_all;
app_funct buffer_img_rates, buffer_img_norm;
app_funct roi_set, roi_reset, sg_set, dev_set;
app_funct start_acq, stop_acq, acq_status;
app_funct reg_acq_cb, reg_status_cb, reg_ser_rx_cb;
app_funct unreg_cb, unreg_all_cb, cb_active, cb_list;
app_funct get_frame, frame_addr;
app_funct ccd_status, get_stats;
app_funct roi_acc_buffer, roi_acc_img_norm;
app_funct focla_open_dev, focla_close_dev, focla_read, focla_write;
app_funct focla_param_list, focla_get_par, focla_set_par;
app_funct focla_sig_list;
app_funct focla_pulse_start, focla_pulse_stop, focla_pulse_status;
app_funct focla_serial_read, focla_serial_read_str, focla_serial_write, 
	  focla_serial_flush;
app_funct firmware_list, get_hw_info;
app_funct poll_sleep;
app_funct get_version;
app_funct edf_usr_header;


#define ETEST_CB_INTERN		0x00
#define ETEST_CB_USER		0x01
#define ETEST_CB_INACTIVE	0x00
#define ETEST_CB_ACTIVE		0x02
#define ETEST_CB_INTERN_ACTIVE	(ETEST_CB_INTERN | ETEST_CB_ACTIVE)

int reg_gen_cb(app_context_t context, struct espia_cb_data *cb_data,
	       struct cb_priv_data *priv_data, int user_act, int *cb_nr_ptr);
int unreg_gen_cb(app_context_t context, struct device_data *dev_data,
		 int cb_nr, int user_cb, int *cb_nr_ptr);
int gen_cb_funct(struct espia_cb_data *cb_data);

#ifdef WITH_IMAGE
int gen_img_add(app_context_t context, int buffer_nr, img_cb *cb_funct, 
		struct img_frame_info *req_finfo, img_del *del_funct);
int gen_img_del(app_context_t context, int buffer_nr);
#endif // WITH_IMAGE

void print_finfo(app_context_t context, 
		 struct img_frame_info *req_finfo,
		 struct img_frame_info *cb_finfo, int ret);

int roi_acc_buffer_init(app_context_t context, int nr_frames);
int roi_acc_buffer_install(app_context_t context);
int roi_acc_buffer_del(app_context_t context);

int set_gen_sg(app_context_t context, int img_config, 
	       struct espia_frame_dim *frame_dim, struct espia_roi *roi);


/*--------------------------------------------------------------------------
 * the table with the application functions
 *--------------------------------------------------------------------------*/

extern struct funct_data all_funcs[];


#endif /* _ESPIA_TEST_H */
