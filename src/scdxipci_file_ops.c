/****************************************************************************
 * File:	scdxipci_file_ops.c
 * $Header: /segfs/bliss/cvs/driver/ESRF/espia/src/scdxipci_file_ops.c,v 2.21 2017/11/15 12:19:30 ahoms Exp $
 * Project:	SECAD Espia card Linux driver
 * Description:	Driver file operations code
 * Author(s):	SECAD S.A.
 *		A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 *
 * $Log: scdxipci_file_ops.c,v $
 * Revision 2.21  2017/11/15 12:19:30  ahoms
 * * Ported to gcc-4.9, Linux 3.16 kernels and Qt4
 * * Avoid 64-bit DMA if pci_set_dma_mask(64) fails
 * * Refactored FW capabilities definition, now using capability sets
 * * Improved error checking and reporting in espia_[acq_]lib
 *
 * Revision 2.20  2016/03/07 11:39:37  ahoms
 * * Fixed bug crashing PC when a board with unknown firmware issues an IRQ
 * * Moved KEEP_RELEASED_PAGES compilation flag to acq. driver option
 * * Added GOOD_RESET_SG board capability, introduced in firmware 2014.12.03,
 *   restoring the previously disabled DMA to 64-bit pages functionality
 *
 * Revision 2.19  2012/06/15 10:33:35  ahoms
 * * Fixed problems in RedHat EL 4 with 64-bit pages:
 *   + Typo bug in ESRFVER_REMAP_PAGE_32 conditional,
 *     hiding error when remapping user pages
 *   + Added systematic verification of potential problems by
 *     checking num_physpages [SCDXIPCI_ERR_TOOMUCHRAM]
 * * Added Espia-Express 2012.05.22 and 2012.06.13 FW checksums
 * * Fixed EspiaAcqLib.FoclaDevClass.getTriggerMode signature
 *
 * Revision 2.18  2011/09/30 19:36:18  ahoms
 * * Added user-defined SG table validation checking before
 *   acq. start since it is persistent after frame size change
 *
 * Revision 2.17  2011/04/08 13:23:28  ahoms
 * * Implemented firmware reading in kernel at device probe
 * * Added scdxipci_board with ser_nr and firmware info,
 *   responsible of common board resource enabling/allocation,
 *   and the possibility of having 1, 2 or 4 logical scdxipci_devs
 * * Added HW_INFO IOCTL returning:
 *   + board/device types/numbers
 *   + flash board_firmware info (start, len, checksum, name)
 *     and reading
 *   + flash board_id info (start, len, ser/nr) and reading
 *
 * Revision 2.16  2009/02/02 16:02:17  ahoms
 * Always verify that all the active acq. cb threads are already waiting
 * for the first frame (and wait if not) before the [SG] DMA is started.
 *
 * Revision 2.15  2008/05/17 17:53:26  ahoms
 * Started re-organization of frame post-op structures; using library function
 * to retrie table entries for ensuring binary compatibility
 * Address frame post-ops by their names in espia_test; splited FramePostOpList.
 * First implementation of VERT_2_QUAD frame post-op for Aviex detector.
 * Implemented endless FOCLA CL CC pulse trains.
 * Added FOCLA_PULSE_WAIT_FRAME: wait for next frame before next pulse stage.
 * Return -EFAULT after an invalid user space pointer instead of -EACCES.
 * Using static _espia_lib_h_revision when checking compiled/running version.
 *
 * Revision 2.14  2008/03/26 15:11:59  ahoms
 * Implemented aux. frame memory copy in a kernel thread.
 * Increased number of aux. buffers to 10.
 * Buffer pointer array is now vmalloc'ed.
 * Changed acq. name to "espia_%d_acq" instead of "Acq. %p".
 *
 * Revision 2.13  2008/03/18 08:50:28  ahoms
 * Moved next frame SG table preparation from IRQ handler to EOF tasklet.
 * Limited to one the number of frames handled in EOF tasklet.
 * Corrected file names in serial_mmap_close [MP].
 *
 * Revision 2.12  2008/03/02 17:32:21  ahoms
 * Added support for driver options; implemented DEBUG_LEVEL and NO_FIFO_RESET.
 * Fixed bug crashing PC on redhate5 when SG is active without a garbage region.
 * Dev. SG lists for PCI frame mapping are now vmalloc'ed at driver startup.
 * Added cal. time, time of day, and time of frame in EDF header (espia_test).
 * Disable IRQs during memcopy & lock acq. when updating stats. in EOF tasklet
 * scdxipci_dma_align must now be called with dev. locked.
 * Check specified devices to set_dev before allocating acq_dev array.
 *
 * Revision 2.11  2008/02/12 11:31:32  ahoms
 * Increased the maximum SG table length to 128 KB (16 K desc, 64 MB frames)
 * Added SG table overflow checking.
 * Using vmalloc for user defined SG tables.
 *
 * Revision 2.10  2008/02/08 14:20:41  ahoms
 * Ported to x86_64 architecture.
 * Implemented multiple SG tables for low frame switch overhead,
 * next frame is mapped just after the DMA is started.
 * Input data FIFO is not erased on start from second frame on.
 *
 * Revision 2.9  2007/11/01 21:25:43  ahoms
 * Corrected ROI handling in acq_set_sg; always unmap the current
 * the current frame if device is not busy in another acquisition.
 * Added static RCS Id to scdxipci_file_ops and scdxipci_acq_buffer.
 *
 * Revision 2.8  2007/10/23 15:25:18  ahoms
 * Ported to Linux 2.6.18; using class_device instead of class_simple.
 * Unified code with Linux 2.4 through esrfversion.h.
 * Changed scdxipci_private.h macros with static inline functions.
 * Espia and FOCLA parameter tables are no longer exported (for future
 * binary compatibility), they can be browsed with get_param_data.
 * Updated espia parameters: DO_NOT_RESET_FIFOS and ESPIA_125MHZ.
 * Added TRIG and TEST_IMAGE FOCLA parameters; implemented focla_strerror.
 * Implemented retrieval of card ID and firmware data.
 * Using generic image interface in espia_test.
 * Improved scdxipci_find_by_buffer_frame code.
 *
 * Revision 2.7  2007/08/23 17:20:40  ahoms
 * Ported to Linux 2.6: using pci_name/enable_device/set_master;
 * using "espia%d" for resource allocation name; not setting PCI dev. name;
 * implemented sysfs class_simple devices; serial Tx uses kthread.
 * [Temporarly] disabled "very high vemory" (64-bit) buffers.
 * Adapted to new circ_buffer_getc/putc flags (CIRC_BUFFER_KTHREAD).
 * Moved app_framework, circ_buffer and image to driver common.
 * Making proper dev. initialization sequence.
 * Improved buffer array management so it does not have empty entries;
 * using long long integer arithmetic instead of double in get_frame/AFN.
 * Moved focla_param to espia_param, implemented basic Espia parameters.
 * Changed write_register to accept the mask of bits to modify;
 * verify the proper register offset alignment.
 * Renamed focla_lib functions to follow espia_lib style.
 * Fixed several espia_test bugs.
 *
 * Revision 2.6  2007/07/10 19:09:59  ahoms
 * Remove acknowledgement potential serial IRQs on every FIFO read
 * (big overhead); temporally disable interrupts instead.
 * Check if serial FIFO becomes full and warn a message.
 * Fixed SCDXIPCI_IOC_WRITE_REGISTER code to return the original reg. value.
 *
 * Revision 2.5  2007/07/06 14:47:49  ahoms
 * Simplified cb thread termination: removed
 * synchronization and cancelation (bug in SuSE 8.2 pthread)
 *
 * Revision 2.4  2007/07/03 14:08:45  ahoms
 * Implemented Espia RoI through SG lists: xfers
 * with invalid frame addr. are dump into a garbage buffer.
 * Check SG desc. are properly aligned if using 64-bit xfers.
 * Let RESET_SCATTER_GATHER active when aborting an acq. so
 * next frame from CCD is aborted immediately.
 *
 * Revision 2.3  2007/04/25 16:50:52  ahoms
 * Support LARGEFILE (> 2GB) in espia_test program
 *
 * Revision 2.2  2007/02/20 17:22:10  ahoms
 * Fixed acq_dev locking with a 5 usec delay before reacquiring lock.
 * Protect driver get_frame when freeing (loosing) buffers.
 * Using a new optimized algorithm in acq_frame_nr search.
 * Added acq_frame_nr info to acq. stats; incl. cb_nr in espia_cb_data.
 * Implemented parallel port debug, including heartbeat timer.
 * Using new app_framework context interface (alloc, init, err/out).
 * Improved error checking and report. in driver and espia_test.
 * In espia_test:
 *   Centralized callback management
 *   Implemented RoI Accumulation buffer
 *   Allow saving multiple acq. frames in a single file
 *   Acq. status shows last acq/saved frame from callbacks
 *
 * Revision 2.1  2007/01/13 12:30:00  ahoms
 * Added user-specified scatter gather lists.
 * Implemented meta device (minor=255), controlling
 * acquisitions on one or more real devices.
 * Acquisition lock independent of devices.
 * Dev. cmds (ser-line, reg. R/W, etc.) forbidden in meta acq.
 * Completed pci_unmap_frame by clearing dev. sg-list;
 * clear the sg-list pointer register on driver exit.
 * End-of-frame treatment (including memcpy if necessary)
 * is done in tasklet (IRQ enabled).
 * Increased to infitity nr. of retries when
 * allocating 32-bit only frame pages.
 * Implemented vert. flip sg-list on 1 and 2 adapters.
 * Added espia_frame_dim; always passed to frame post-op.
 *
 * Revision 2.0  2006/12/13 17:43:19  ahoms
 * Merged release 1.99.1.14 into main trunk
 *
 * Revision 1.99.1.14  2006/12/01 15:55:15  ahoms
 * Corrected serial read timeout to expire when the user requested.
 * Prepared term. search in drv. serial read: not implemented yet in circ_buffer
 * Avoid extra serial interrupts by acknowledging after every read.
 * Added espia_read/write/register; corrected reg. checking in driver.
 * Added frame post-op functionality with buffer-based thread protection;
 * implemented interlaced pixel swap, vert. flip and check for NULL lines.
 * Added buffer clear, auto-save and live-image functionalities to espia_test.
 * Modify kernel mapping of 64-bit frame pages to user space.
 * Set the time in the frame notifying that acq. finished.
 * Split espia_test to separate main from test routines (for Python wrapper).
 * Check for NULL pointers in parameters to espia_lib functions.
 * Avoid registering acq. callbacks when an acq. is running.
 * Using thread synchronization when terminating or finishing callback threads.
 * Fixed bug of EACH_BUFFER callback aborting when acq. finished.
 * Implemented proper clean-up in espia_close;
 * do the same in espia_test close_dev.
 * Using __attribute__ unused for RCS ID strings.
 *
 * Revision 1.99.1.13  2006/10/23 13:33:35  ahoms
 * Added support of 64-bit memory pages, not directly accessible by the card;
 * using temporary aux. kernel buffer when frames contain such pages.
 * Implemented acquisition statistics, to be read by the user.
 * Free already allocated buffers when buffer allocation fails.
 * Fixed bug when waiting for cb. thread that already finished.
 * Added buffer dump and save in espia test program.
 *
 * Revision 1.99.1.12  2006/10/03 17:54:36  ahoms
 * Added FOCLA interface library
 *
 * Revision 1.99.1.11  2006/10/03 09:50:37  ahoms
 * Both driver and library check for the same version string in dev. open.
 * Access buffers in kernel from an array instead of a linked list;
 * all user space layers specify buffers by their nr and not their addr.
 * Removed espia_frame_info, moved buffer_info and frame_address to driver.
 * Library now specifies the frame_size instead of the frames_per_buffer.
 * Cancel acq. threads only outside callback execution.
 * Added Qt-based buffer display with efficient thread mechanism.
 * Using standard wait_event_interruptible_timeout return code.
 *
 * Revision 1.99.1.10  2006/06/07 09:22:39  ahoms
 * Included new Espia firmware (05 May 2006): new subsystem vendor/dev ID.
 * Activate the PCI burst transfer mode in espia_open.
 *
 * Revision 1.99.1.9  2006/04/13 16:56:14  ahoms
 * Changed espia_ser_read_str to wait only for first terminator
 *
 * Revision 1.99.1.8  2005/12/09 15:15:58  ahoms
 * Fixed bug in circ_buffer misinterpreting character 0xff.
 * Increased kernel serial buffers to 128KB (one PRIAM config. block).
 * Corrected debug message in espia_ser_write.
 *
 * Revision 1.99.1.7  2005/10/25 19:49:14  ahoms
 * Added CCD status and serial Rx callback support.
 * Return timeout as a (negative) error.
 * Removed flags from espia_start_acq.
 *
 * Revision 1.99.1.6  2005/09/20 14:58:22  ahoms
 * Atomic register read/write from user space.
 * Improved serial line blocking and timing; added block xfer.
 * Implemented DMA overrun notification in callbacks.
 * Added CCD status with blocking capabilities.
 *
 * Revision 1.99.1.5  2005/08/28 22:09:26  ahoms
 * Added DMA acq. active call; introduced the acq. run number
 * to solve the problem of wait requests between acquisitions.
 * Implemented espia_ser_read_str and espia_frame_address.
 * Using thread mutex & cond. to activate/deactivate callbacks.
 * Implemented ESPIA_EACH_BUFFER/FRAME in callbacks.
 *
 * Revision 1.99.1.4  2005/08/23 02:50:42  ahoms
 * Implemented buffer and acquisition functions in espia_lib
 *
 * Revision 1.99.1.3  2005/08/21 23:45:33  ahoms
 * Implemented image acquisition and (flexible, blocking) get frame.
 *
 * Revision 1.99.1.2  2005/08/17 23:24:54  ahoms
 * Implemented buffer memory mapping with multiple frames (big buffer).
 * Using linked lists and lookaside caches for optimum memory use.
 *
 * Revision 1.99.1.1  2005/08/15 21:35:04  ahoms
 * Using new circ_buffer features; Tx kernel thread always alive.
 * Improved timeout and interrupt control.
 * Renamed scdxipci_dev field minor by dev_nr; open possibility of
 * having several minors per device.
 *
 * Revision 1.99  2005/08/12 15:05:46  ahoms
 * Rewritten driver from scratch. Using kernel PCI driver interface.
 * Implemented a generic menu-driver test application framework.
 * Implemented serial line with circular buffers and TX thread.
 *
 * Revision 1.1-1.16  2005/03/11-2005/06/06  ahoms
 * Improved driver stability with spinlocks and (high)memory management.
 * Implemented reset_link, ser_put/get, read_ccd_status in driver.
 * Multiple frames acquisition independent of user program readout speed.
 *
 * Revision 1.0  2005/02/12 12:00:00  fernande
 * Multithread version
 *
 * Revision 0.1  2003/06/14 12:00:00  fernande
 * Initial Release from SECAD
 *
 ****************************************************************************/

#include "scdxipci_private.h"

#include <linux/module.h>
#include <asm/uaccess.h>


/*--------------------------------------------------------------------------
 * RCS Id 
 *--------------------------------------------------------------------------*/

static char rcs_id[] __attribute__((unused)) = 
	"$Header: /segfs/bliss/cvs/driver/ESRF/espia/src/scdxipci_file_ops.c,v 2.21 2017/11/15 12:19:30 ahoms Exp $";


/*--------------------------------------------------------------------------
 * IOCTL debug
 *--------------------------------------------------------------------------*/

#define CMD_CONST_NAME(x)       {SCDXIPCI_IOC_##x, #x}

deb_const_str scdxipci_ioctl_cmds_array[] = {
	CMD_CONST_NAME(VERSION),
        CMD_CONST_NAME(OPTION),
        CMD_CONST_NAME(READ_REGISTER),
        CMD_CONST_NAME(WRITE_REGISTER),
        CMD_CONST_NAME(RESET_LINK),
        CMD_CONST_NAME(REQUEST_ASYNC),
        CMD_CONST_NAME(ASYNC_STATUS),
        CMD_CONST_NAME(FRAME_SIZE),
        CMD_CONST_NAME(START_DMA),
        CMD_CONST_NAME(GET_FRAME),
        CMD_CONST_NAME(STOP_DMA),
        CMD_CONST_NAME(DMA_ACTIVE),
        CMD_CONST_NAME(SER_READ),
        CMD_CONST_NAME(SER_WRITE),
        CMD_CONST_NAME(CCD_STATUS),
        CMD_CONST_NAME(BUFFER_INFO),
        CMD_CONST_NAME(STATS),
        CMD_CONST_NAME(SET_SG),
        CMD_CONST_NAME(SET_DEV),
	CMD_CONST_NAME(CHK_WAIT),
};

INIT_CONST_STR_ARRAY(scdxipci_ioctl_cmds, scdxipci_ioctl_cmds_array);


/*--------------------------------------------------------------------------
 * Driver file operations
 *--------------------------------------------------------------------------*/

struct file_operations scdxipci_file_ops = {
	owner:		THIS_MODULE,
	open:		scdxipci_open,
	release:	scdxipci_close,
#ifdef HAVE_UNLOCKED_IOCTL 
	unlocked_ioctl:	scdxipci_ioctl,
#else
#if LINUX_VERSION_CODE < KERNEL_VERSION(5, 15, 0)
	ioctl:		scdxipci_ioctl,
#endif	
#endif
	mmap:		scdxipci_mmap,
};

/*--------------------------------------------------------------------------
 * Device open / close 
 *--------------------------------------------------------------------------*/

int scdxipci_open(struct inode *inode, struct file *file)
{
	int ret, meta_acq, minor = MINOR(inode->i_rdev);
	struct data_acq *acq;

	FENTRY("scdxipci_open");

	acq = scdxipci_acq_alloc(minor);
	if (acq == NULL)
		return -ENOMEM;

	DEB_PARAMS("Created %s\n", acq->name);
	meta_acq = (minor == SCDXIPCI_META_DEV);
	if (!meta_acq) {
		ret = scdxipci_acq_set_dev(acq, &minor, 1);
		if (ret < 0) {
			scdxipci_acq_free(acq);
			return ret;
		}
	}

	DEB_OPEN("minor: %d%s\n", minor, meta_acq ? " - meta" : "");
	file->private_data = acq;

	return 0;
}


int scdxipci_close(struct inode *inode, struct file *file)
{
	struct data_acq *acq = data_acq_from_file(file);

	FENTRY("scdxipci_close");
	DEB_OPEN("%s\n", acq->name);

	file->private_data = NULL;
	scdxipci_acq_free(acq);

	return 0;
}


/*--------------------------------------------------------------------------
 * mmap
 *--------------------------------------------------------------------------*/

int scdxipci_mmap(struct file *file, struct vm_area_struct *vma)
{
	struct data_acq *acq = data_acq_from_file(file);
	unsigned long nr_frames, frame_size, align_frame_size, start, size;
	unsigned long frame_pages;
	struct img_buffer *buffer;
	char name[SCDXIPCI_NAME_LEN];

	FENTRY("scdxipci_mmap");

	start = vma->vm_start;
	size = vma->vm_end - start; // in bytes
	DEB_PARAMS("%s, start=0x%lx, end=0x%lx, size=%ld\n", acq->name, 
		   start, vma->vm_end, size);
	
	frame_size = acq->default_frame_size;
	if (frame_size == 0) {
		DEB_ERRORS("Invalid acq. frame size: 32-bit aux. buffer "
			   "alloc. failed!\n");
		return -EINVAL;
	}
	align_frame_size = PAGE_ALIGN(frame_size);
	nr_frames = size / align_frame_size;
	if (nr_frames == 0) {
		DEB_ERRORS("Error: buffer smaller than frame size (%ld)!|n",
			   frame_size);
		return -EINVAL;
	} else if ((nr_frames > 1) && (((size % frame_size) != 0) ||
				       (frame_size & ~PAGE_MASK) != 0)) {
		DEB_ERRORS("Warning: buffer size %ld is not multiple of "
			   "frame size %ld, or frames are not page-aligned\n", 
			   size, frame_size);
		size = nr_frames * align_frame_size;
		DEB_ERRORS("Using %ld frames of %ld bytes (%ld buffer size)\n",
			   nr_frames, align_frame_size, size);
	}

	acq->error = 0;
	frame_pages = align_frame_size / PAGE_SIZE;
	buffer = scdxipci_buffer_alloc(acq, SCDXIPCI_BUFF_NORM, nr_frames, 
				       frame_pages, vma);
	if (buffer == NULL) {
		DEB_ERRORS("Failed to allocate buffer: size=%ld, "
			   "start=0x%08lx\n", size, start);
		return (acq->error < 0) ? acq->error : -ENOMEM;
	}

	// Everything's ready, insert buffer
	if (scdxipci_buffer_add(acq, buffer) < 0) {
		scdxipci_buffer_free(buffer);
		return -ENOMEM;
	}

	sprintf(name, "%s Buffer #%ld", acq->name, buffer->nr);
	scdxipci_buffer_set_name(buffer, name);
	return 0;
}


/*--------------------------------------------------------------------------
 * IOCTL
 *--------------------------------------------------------------------------*/

#ifdef HAVE_UNLOCKED_IOCTL
long scdxipci_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
#else
int scdxipci_ioctl(struct inode *inode, struct file *file,
		   unsigned int cmd, unsigned long arg)
#endif
{
	struct data_acq *acq = data_acq_from_file(file);
	struct acq_dev *acq_dev;
	struct scdxipci_dev *scd_dev = NULL;
	union scdxipci_ioc largs;
	unsigned int arglen;
	int block, only_meta, only_norm, cmd_nr, ret = SCDXIPCI_OK;
	unsigned long flags, delay;
	char cmd_str_buff[64], *cmd_str;
	char version[SCDXIPCI_VERSION_LEN];

	struct scdxipci_ioc_version     *verp   = &largs.ver;
	struct scdxipci_ioc_option      *optp   = &largs.opt;
	struct scdxipci_ioc_register    *regp   = &largs.reg;
	struct scdxipci_ioc_serial      *serp   = &largs.ser;
	struct scdxipci_ioc_dma         *dmap   = &largs.dma;
	struct scdxipci_ioc_dma_act     *actp   = &largs.act;
	struct scdxipci_ioc_buffer_info *buffp  = &largs.buff;
	struct scdxipci_ioc_stats	*statsp = &largs.stats;
	struct scdxipci_ioc_sg		*sgp    = &largs.sg;
	struct scdxipci_ioc_dev_list	*devp   = &largs.dev;
	struct scdxipci_ioc_chk_wait	*waitp  = &largs.wait;
	struct scdxipci_ioc_hw_info	*hwp    = &largs.hw;

	FENTRY("scdxipci_ioctl");

	sprintf(cmd_str_buff, "0x%08x", cmd);
	cmd_str = GET_CONST_STR(scdxipci_ioctl_cmds, cmd, cmd_str_buff);
	DEB_IOCTL("%s, cmd=%s, arg=0x%08lx\n", acq->name, cmd_str, arg);
     
	if (_IOC_TYPE(cmd) != SCDXIPCI_IOC_MAGIC) {
		DEB_ERRORS("Non-%s command (magic: 0x%02x). Has your device "
			   "entry in /dev the good major number?\n", 
			   SCDXIPCI_DRV_NAME, _IOC_TYPE(cmd));
		return -EINVAL;
	}
	arglen = (_IOC_DIR(cmd) != _IOC_NONE) ? _IOC_SIZE(cmd) : 0;
	if (arglen > sizeof(largs)) {
		DEB_ERRORS("Arguments too big. Using a newer driver lib?\n");
		return SCDXIPCI_ERR_VERSION;
	}

	if (_IOC_DIR(cmd) & _IOC_WRITE) {
		DEB_PARAMS("Copying %d bytes from user space\n", arglen);
		if (copy_from_user(&largs, (void *) arg, arglen) != 0) {
			DEB_ERRORS("Could not copy %d bytes from user "
				   "address 0x%08lx\n", arglen, arg);
			return -EFAULT;
		}
	}

	if (cmd == SCDXIPCI_IOC_VERSION) {
		DEB_PARAMS("Library %s\n", verp->version);
		pretty_rcs_revision(version, drv_revision);
		ret = strncmp(verp->version, version, SCDXIPCI_VERSION_LEN);
		if (ret != 0) {
			DEB_ERRORS("Invalid library %s, driver is %s.\n", 
				   verp->version, drv_revision);
			return SCDXIPCI_ERR_VERSION;
		}
		acq->enabled = 1;
		return SCDXIPCI_OK;
	} else if (!acq->enabled) {
		DEB_ERRORS("Version cmd not successfully executed yet.\n");
		DEB_ERRORS("Maybe you need a library update?\n");
		return SCDXIPCI_ERR_VERSION;
	}

	cmd_nr = _IOC_NR(cmd);
	only_meta = (cmd_nr & SCDXIPCI_IOC_META);
	only_norm = (cmd_nr & SCDXIPCI_IOC_NORM);
	if (!acq->is_meta && only_meta) {
		DEB_ERRORS("cmd %s only allowed on meta dev. acq.\n", cmd_str);
		return SCDXIPCI_ERR_NOMETA;
	} else if (acq->is_meta && only_norm) {
		DEB_ERRORS("cmd %s not allowed on meta dev. acq.\n", cmd_str);
		return SCDXIPCI_ERR_META;
	}
	acq_dev = acq->dev;
	if (!acq->is_meta)
		scd_dev = acq_dev->dev;

	switch (cmd_nr) {
	case _IOC_NR(SCDXIPCI_IOC_OPTION):
		ret = scdxipci_option(acq, optp);
		break;
		
	case _IOC_NR(SCDXIPCI_IOC_READ_REGISTER):
		ret = scdxipci_ioc_reg(scd_dev, regp, SCDXIPCI_REG_READ);
		break;
		
	case _IOC_NR(SCDXIPCI_IOC_WRITE_REGISTER):
		ret = scdxipci_ioc_reg(scd_dev, regp, SCDXIPCI_REG_WRITE);
		break;
		
	case _IOC_NR(SCDXIPCI_IOC_RESET_LINK):
		scdxipci_lock_dev(scd_dev, flags);
		scdxipci_reset_link(scd_dev);
		scdxipci_unlock_dev(scd_dev, flags);
		break;

	case _IOC_NR(SCDXIPCI_IOC_SER_READ):
		ret = scdxipci_ser_read(scd_dev, serp->buffer, serp->nr_bytes, 
					serp->term, serp->term_bytes, 
					serp->timeout);
		if (ret >= 0) {
			serp->nr_bytes = ret;
			ret = SCDXIPCI_OK;
		}
		break;

	case _IOC_NR(SCDXIPCI_IOC_SER_WRITE):
		block = (serp->timeout & SCDXIPCI_TX_BLOCK) != 0;
		delay = serp->timeout;
		if ((serp->nr_bytes > 0) || (delay != SCDXIPCI_BLOCK_FOREVER))
			delay &= ~SCDXIPCI_TX_BLOCK;
		ret = scdxipci_ser_write(scd_dev, serp->buffer, serp->nr_bytes,
					 serp->block_size, delay, block);
		if (ret >= 0) {
			serp->nr_bytes = ret;
			ret = SCDXIPCI_OK;
		}
		break;

	case _IOC_NR(SCDXIPCI_IOC_FRAME_SIZE):
		ret = scdxipci_acq_frame_size(acq, arg);
		break;

	case _IOC_NR(SCDXIPCI_IOC_START_DMA):
		ret = scdxipci_ioc_start_dma(acq, dmap);
		break;

	case _IOC_NR(SCDXIPCI_IOC_GET_FRAME):
		ret = scdxipci_ioc_get_frame(acq, dmap);
		break;

	case _IOC_NR(SCDXIPCI_IOC_STOP_DMA):
		scdxipci_acq_stop(acq);
		break;

	case _IOC_NR(SCDXIPCI_IOC_DMA_ACTIVE):
		scdxipci_lock_acq(acq, flags);
		actp->acq_run_nr = acq_get_run_nr(acq);
		actp->active = is_current_acq(acq);
		scdxipci_unlock_acq(acq, flags);
		DEB_PARAMS("%s %srunning\n", acq->name,
			   (actp->active == 0) ? "not " : "");
		break;

	case _IOC_NR(SCDXIPCI_IOC_CCD_STATUS):
		ret = scdxipci_ccd_status(scd_dev, arg);
		break;

	case _IOC_NR(SCDXIPCI_IOC_BUFFER_INFO):
		ret = scdxipci_buffer_info(acq, &buffp->binfo);
		break;

	case _IOC_NR(SCDXIPCI_IOC_STATS):
		ret = scdxipci_get_stats(acq, &statsp->stats);
		break;

	case _IOC_NR(SCDXIPCI_IOC_SET_SG):
		ret = scdxipci_ioc_set_sg(acq, sgp);
		break;

	case _IOC_NR(SCDXIPCI_IOC_SET_DEV):
		ret = scdxipci_ioc_set_dev(acq, devp);
		break;

	case _IOC_NR(SCDXIPCI_IOC_CHK_WAIT):
		ret = scdxipci_acq_check_wait(acq, waitp->thread_id, 
					      waitp->timeout);
		break;

	case _IOC_NR(SCDXIPCI_IOC_HW_INFO):
		ret = scdxipci_hw_info(scd_dev, hwp->action, &hwp->hw_info, 1);
		break;

	default:
		DEB_ERRORS("Command %s not implemented.\n", cmd_str);
		DEB_ERRORS("Using a newer driver lib.?\n");
		ret = SCDXIPCI_ERR_VERSION;
	}

	if (_IOC_DIR(cmd) & _IOC_READ) {
		DEB_PARAMS("Copying %d bytes to user space\n", arglen);
		if (copy_to_user((void *) arg, &largs, arglen) != 0) {
			DEB_ERRORS("Could not copy %d bytes to user "
				   "address 0x%08lx\n", arglen, arg);
			return -EFAULT;
		}
	}

	DEB_IOCTL("ret=%d\n", ret);
	return ret;
}


/*--------------------------------------------------------------------------
 * Driver option
 * Set and/or get a driver option value
 *--------------------------------------------------------------------------*/

int scdxipci_option(struct data_acq *acq, struct scdxipci_ioc_option *optp)
{
	int *var_ptr, prev_val, action, lock_acq, option = optp->option;
	unsigned long flags = 0;
	static char *opt_name[SCDXIPCI_NR_OPT] = {
		"DEBUG_LEVEL", "NO_FIFO_RESET", "KEEP_RELEASED_PAGES",
	};
	static char *act_name[4] = {
		"NONE", "RD", "WR", "RD/WR"
	};
	char *name;

	FENTRY("scdxipci_option");

	if ((option < 0) || (option >= SCDXIPCI_NR_OPT)) {
		DEB_ERRORS("Invalid option: %d (nr_options=%d)\n", option, 
			   SCDXIPCI_NR_OPT);
		return -EINVAL;
	}
	name = opt_name[option];
	action = optp->action;

	var_ptr = NULL;
	lock_acq = 0;
	switch (optp->option) {
	case SCDXIPCI_OPT_DEBUG_LEVEL:
		var_ptr = &debug_level;
		break;
	case SCDXIPCI_OPT_NO_FIFO_RESET:
		var_ptr = &acq->no_fifo_reset;
		lock_acq = 1;
		break;
	case SCDXIPCI_OPT_KEEP_RELEASED_PAGES:
		var_ptr = &acq->keep_released_pages;
		lock_acq = 1;
		break;
	}

	if (var_ptr == NULL) {
		DEB_ERRORS("Error: option %s variable ptr is NULL!!", name);
		return SCDXIPCI_ERR_INTERNAL;
	}

	if (lock_acq)
		scdxipci_lock_acq(acq, flags);

	prev_val = *var_ptr;
	DEB_PARAMS("option=%d (%s), action=%d (%s), prev_val=%d, req_val=%d\n",
		   option, name, action, act_name[action & 3], prev_val, 
		   optp->value);

	if (action & SCDXIPCI_OPT_WR)
		*var_ptr = optp->value;
	if (action & SCDXIPCI_OPT_RD)
		optp->value = prev_val;

	if (lock_acq)
		scdxipci_unlock_acq(acq, flags);

	return SCDXIPCI_OK;
}


/*--------------------------------------------------------------------------
 * IOCTL register read/write
 *--------------------------------------------------------------------------*/

int scdxipci_ioc_reg(struct scdxipci_dev *scd_dev, 
		     struct scdxipci_ioc_register *regp, int write)
{
	unsigned long flags, val, aux;

	if (scdxipci_check_reg(regp->reg_off) < 0)
		return -EINVAL;

	scdxipci_lock_dev(scd_dev, flags);
	val = scdxipci_read_reg(scd_dev, regp->reg_off);
	if (write) {
		aux = (val & ~regp->mask) | (regp->data & regp->mask);
		scdxipci_write_reg(scd_dev, regp->reg_off, aux);
	}
	regp->data = val;
	scdxipci_unlock_dev(scd_dev, flags);
	return SCDXIPCI_OK;
}


/*--------------------------------------------------------------------------
 * IOCTL start dma
 *--------------------------------------------------------------------------*/

int scdxipci_ioc_start_dma(struct data_acq *acq, 
			   struct scdxipci_ioc_dma *dmap)
{
	struct img_frame_info *finfo = &dmap->finfo;
	int ret;

	FENTRY("scdxipci_ioc_start_dma");
	DEB_PARAMS("buffer_nr=%ld, acq_frame_nr=%ld, timeout=%ld\n", 
		   finfo->buffer_nr, finfo->acq_frame_nr, dmap->timeout);

	ret = scdxipci_acq_start(acq, finfo->buffer_nr, finfo->acq_frame_nr);
	if ((ret < 0) || (dmap->timeout == 0))
		return ret;

	finfo->buffer_nr = SCDXIPCI_ANY;
	finfo->frame_nr = SCDXIPCI_ANY;
	finfo->round_count = SCDXIPCI_ANY;
	finfo->acq_run_nr = acq_get_run_nr(acq);
	if (finfo->acq_frame_nr-- == 0)	// wait for last frame. endless acq?
		finfo->acq_frame_nr--;	// wait for VERY high acq_frame_nr

	ret = scdxipci_ioc_get_frame(acq, dmap);
	return (ret == -ERESTARTSYS) ? -EINTR : ret;
}


/*--------------------------------------------------------------------------
 * IOCTL get frame
 *--------------------------------------------------------------------------*/

int scdxipci_ioc_get_frame(struct data_acq *acq, 
			   struct scdxipci_ioc_dma *dmap)
{
	struct img_frame_info *finfo = &dmap->finfo;
	struct img_buffer *buffer;
	unsigned long flags;
	int ret;

	FENTRY("scdxipci_ioc_get_frame");
	DEB_PARAMS("buffer_nr=%ld, frame_nr=%ld, round_count=%ld, "
		   "acq_frame_nr=%ld, acq_run_nr=%ld (curr=%ld) timeout=%ld, "
		   "thread_id=%ld\n",
		   finfo->buffer_nr, finfo->frame_nr, finfo->round_count, 
		   finfo->acq_frame_nr, finfo->acq_run_nr, acq_get_run_nr(acq),
		   dmap->timeout, dmap->thread_id);

	ret = scdxipci_acq_wait_get_frame(acq, finfo, dmap->timeout, 
					  dmap->thread_id);

	if (!is_invalid(finfo->buffer_nr)) {
		scdxipci_lock_acq(acq, flags);
		buffer = acq_buffer(acq, finfo->buffer_nr);
		finfo->buffer_ptr = img_buffer_uadd(buffer);
		scdxipci_unlock_acq(acq, flags);
	} else
		finfo->buffer_ptr = NULL;

	return ret;
}


/*--------------------------------------------------------------------------
 * Get buffer info
 *--------------------------------------------------------------------------*/

int scdxipci_buffer_info(struct data_acq *acq, struct img_buffer_info *binfo)
{
	struct img_buffer *buffer;
	unsigned long flags, frame_size;
	int ask_nr, ask_ptr, ret = SCDXIPCI_OK;

	FENTRY("scdxipci_buffer_info");
	DEB_PARAMS("binfo-nr=%ld, binfo-ptr=0x%p\n", binfo->nr, binfo->ptr);

	ask_nr = is_invalid(binfo->nr);
	ask_ptr = (binfo->ptr == NULL);
	if (ask_nr == ask_ptr) {
		DEB_ERRORS("one and only one among nr and ptr must be "
			   "specified\n");
		return -EINVAL;
	}

	scdxipci_lock_acq(acq, flags);

	if (ask_ptr) {
		buffer = acq_buffer(acq, binfo->nr);
		binfo->ptr = img_buffer_uadd(buffer);
		if (binfo->ptr == NULL)
			DEB_ERRORS("could not find buffer #%ld\n", binfo->nr);
	} else {
		buffer = scdxipci_buffer_from_uadd(acq, binfo->ptr);
		binfo->nr = buffer ? buffer->nr : SCDXIPCI_INVALID;
		if (is_invalid(binfo->nr))
			DEB_ERRORS("could not find buffer @ 0x%p\n",
				   binfo->ptr);
	}
	if (buffer != NULL) {
		binfo->nr_frames = img_buffer_frames(buffer);
		frame_size = img_frame_size(img_buffer_first_frame(buffer));
		binfo->frame_size = frame_size;
	} else
		ret = SCDXIPCI_ERR_BUFFER;

	scdxipci_unlock_acq(acq, flags);

	return ret;
}


/*--------------------------------------------------------------------------
 * Set scatter gather
 *--------------------------------------------------------------------------*/

int scdxipci_ioc_set_sg(struct data_acq *acq, struct scdxipci_ioc_sg *sgp)
{
	struct scdxipci_sg_table new_sg, *req_sg = &sgp->table;
	void *ptr;
	int len, ret = 0;

	FENTRY("scdxipci_ioc_set_sg");
	DEB_PARAMS("dev_idx=%ld, req_sg=0x%p, req_sg_len=%ld\n", 
		   sgp->dev_idx, req_sg->ptr, req_sg->len);

	new_sg.ptr = NULL;
	new_sg.len = 0;
	if (req_sg->len > 0) {
		len = req_sg->len * sizeof(*new_sg.ptr);
		ptr = vmalloc(len);
		if (ptr == NULL) {
			DEB_ERRORS("error alloc. %ld SG blocks (%d bytes)\n", 
				   req_sg->len, len);
			return -ENOMEM;
		}
		new_sg.ptr = (struct scdxipci_sg_block *) ptr;
		new_sg.len = req_sg->len;

		if (copy_from_user(new_sg.ptr, req_sg->ptr, len) != 0) {
			DEB_ERRORS("error copying SG table from user space: "
				   "%d bytes @ 0x%p\n", len, req_sg->ptr);
			ret = -EFAULT;
			goto out;
		}
	}

	ret = scdxipci_acq_set_sg(acq, sgp->dev_idx, &new_sg);

 out:
	if (new_sg.len > 0)
		vfree(new_sg.ptr);

	return ret;
}


/*--------------------------------------------------------------------------
 * Set device list
 *--------------------------------------------------------------------------*/

int scdxipci_ioc_set_dev(struct data_acq *acq, 
			 struct scdxipci_ioc_dev_list *devp)
{
	int *dev_list, len, ret, nr_dev = devp->nr_dev;

	FENTRY("scdxipci_ioc_set_dev");
	DEB_PARAMS("nr_dev=%d\n", nr_dev);

	len = nr_dev * sizeof(*dev_list);
	dev_list = (int *) kmalloc(len, GFP_KERNEL);
	if (dev_list == NULL) {
		DEB_ERRORS("error alloc. list of %d dev. idx (%d bytes)\n",
			   nr_dev, len);
		return -ENOMEM;
	}
	
	if (copy_from_user(dev_list, devp->dev_list, len) != 0) {
		DEB_ERRORS("error copying dev. list from user space: "
			   "%d bytes @ 0x%p\n", len, devp->dev_list);
		ret = -EFAULT;
		goto out;
	}

	ret = scdxipci_acq_set_dev(acq, dev_list, nr_dev);

 out:
	kfree(dev_list);

	return ret;
}


