/****************************************************************************
 * File:	espia_test_cli.c
 * $Header: /segfs/bliss/cvs/driver/ESRF/espia/src/espia_acq_cli.c,v 2.25 2020/07/23 11:00:41 ahoms Exp $
 * Project:	SECAD Espia card Linux driver
 * Description:	Espia test program, CLI part
 * Author(s):	SECAD S.A.
 *		A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 *              A. Kirov, BLISS, ESRF (assen.kirov@esrf.fr)
 ****************************************************************************/

#define _GNU_SOURCE

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/mman.h>
#include <dlfcn.h>

#include "espia_lib.h"
#include "esrfdebug.h"
#include "app_framework.h"
#ifdef WITH_IMAGE
#include "imageapi.h"
#endif // WITH_IMAGE
#include "focla_lib.h"

#include "espia_acq_lib.h"
#include "espia_acq_cli.h"


/*--------------------------------------------------------------------------
 * the table with the CLI functions
 *--------------------------------------------------------------------------*/

struct funct_data cli_funcs[] = {
	{"Open",          cli_open_dev, "Open an Espia device",
	 {{"dev_nr",      PARAM_INT, "Number of device [0-254, 255=Meta]"}, 
	  {NULL}}},
	{"Select",        cli_select_dev, "Select an Espia device as active",
	 {{"dev_nr",      PARAM_INT, "Number of device [0-255]"}, 
	  {NULL}}},
	{"Close",         cli_close_dev, "Close the active Espia device",
	 {{NULL}}},
	{"DevList",       cli_dev_list, "List all open devices",
	 {{NULL}}},
	{"StopAcq",       cli_stop_acq, "Stop the current acquisition",
	 {{NULL}}},
	{"AcqStatus",     cli_acq_status, "Check if the acquisition is running",
	 {{NULL}}},
	{"SetOption",     cli_set_option, "Set a driver option",
	 {{"option",      PARAM_STR, "Option name"}, 
	  {"val",         PARAM_INT, "Value [used only in Write]"}, 
	  {NULL}}},
	{"SerialMmapClose",cli_serial_mmap_close, "Close mmap files for serial ops",
	 {{NULL}}},
	{"BufferAlloc",   cli_buffer_alloc, "Alloc buffers, made of frames",
	 {{"nr_buffer",   PARAM_INT, "Nr. of buffers"}, 
	  {"buff_frm",    PARAM_INT, "Nr. of frames per buffer"}, 
	  {"width",       PARAM_INT, "Nr. of columns per frame"}, 
	  {"height",      PARAM_INT, "Nr. of rows per frame"}, 
	  {"depth",       PARAM_INT, "Image bits per pixel (in bytes)"}, 
	  {"display",     PARAM_INT, "Create a display for each buffer"}, 
	  {NULL}}},
	{"BufferFree",    cli_buffer_free, "Free the allocated buffers",
	 {{NULL}}},
#ifdef WITH_IMAGE
	{"BufferImage",   cli_buffer_img, "Create buffer display window",
	 {{"buffer_nr",   PARAM_INT, "Nr. of buffer [-1=Live]"}, 
	  {NULL}}},
	{"BufferImageDel",cli_buffer_img_del, "Destroy buffer display window",
	 {{"buffer_nr",   PARAM_INT, "Nr. of buffer [-1=Live]"}, 
	  {NULL}}},
#endif // WITH_IMAGE
	{"DevSet",        cli_dev_set, "Define the device list for meta dev.",
	 {{"dev_list",    PARAM_STR, "Dev. list: dev_nr[,dev_nr...]"},
	  {NULL}}},
	{"UnregAllCb",    cli_unreg_all_cb, "Unregister all callbacks & autosave",
	 {{NULL}}},
	{"FoclaOpen",     cli_focla_open_dev, "Open the FOCLA device",
	 {{NULL}}},
	{"FoclaClose",    cli_focla_close_dev, "Close the FOCLA device",
	 {{NULL}}},
	{"FoclaGetParam", cli_focla_get_par, "Get a FOCLA parameter",
	 {{"param",       PARAM_STR, "Name of the parameter"}, 
	  {NULL}}},
	{"FoclaSetParam", cli_focla_set_par, "Set a FOCLA parameter",
	 {{"param",       PARAM_STR, "Name of the parameter"}, 
	  {"val",         PARAM_INT, "Value to set"}, 
	  {NULL}}},
	{"StartAcq",      cli_start_acq, "Start an image acquisition",
	 {{"start_buffer",PARAM_INT, "Nr. of buffer to start from"}, 
	  {"nr_frames",   PARAM_INT, "Total nr. of frames to acq."}, 
	  {"timeout",     PARAM_INT, "Timeout in us [0=NoBlock, -1=Forever]"}, 
	  {NULL}}},
	{"SGSet",         cli_sg_set, "Set a Scatter Gather table",
	 {{"img_config",  PARAM_INT, "Device image config [-1=List]"}, 
	  {NULL}}},
	{"BufferClear",   cli_buffer_clear, "Clear buffer to 0",
	 {{"buffer_nr",   PARAM_INT, "Nr. of buffer [-1=All]"}, 
	  {NULL}}},
	{"BufferSave",    cli_buffer_save, "Save buffer in a file",
	 {{"buffer_nr",   PARAM_INT, "Nr. of buffer [-1=Auto-save, "
	                                                    "-2=non-blocking]"}, 
	  {"fname_prefix",PARAM_STR, "Name prefix of file to write [?=Get]"},
	  {"fname_idx",   PARAM_INT_OPT, "Index of file in serie [-1=Keep]"},
	  {"format",      PARAM_INT_OPT, "Format of file [0=Raw, 1=EDF]"}, 
	  {"file_frames", PARAM_INT_OPT, "Nr. of frames per file"}, 
	  {"overwrite",   PARAM_INT_OPT, "Force file truncation (overwrite)"}, 
	  {NULL}}},
	{"GetFrame",      cli_get_frame, "Wait and get a frame info",
	 {{"buffer_nr",   PARAM_INT, "Nr. of buffer [-1=Any]"}, 
	  {"frame_nr",    PARAM_INT, "Frame idx in buffer [-1=Any]"}, 
	  {"count",       PARAM_INT, "Nr. of (re)write times [-1=Any]"}, 
	  {"acq_frame_nr",PARAM_INT, "Frame idx in acq. [-1=Any]"}, 
	  {"timeout",     PARAM_INT, "Timeout in us [0=NoBlock, -1=Forever]"}, 
	  {NULL}}},
	{"FramePostOp",   cli_frame_post_op, "Set active frame post-operations",
	 {{"op_list",     PARAM_STR, "Op list: op_name[,op_name...]"},
	  {NULL}}},
	{"FramePostOpList",cli_frame_post_op_list, "List avail. frame post-op",
	 {{NULL}}},
	{"FPOListActive", cli_frame_post_op_list_active, "List the active frame post-op",
	 {{NULL}}},
	{"OptionList",    cli_option_list, "List available driver options",
	 {{NULL}}},
	{"GetOption",     cli_get_option, "Get one or all driver options",
	 {{"option",      PARAM_STR, "Option name or ALL"}, 
	  {NULL}}},
	{"ResetLink",     cli_reset_link, "Reset the Aurora link",
	 {{NULL}}},
	{"SerialRead",    cli_serial_read, "Read bytes from the serial line",
	 {{"nr_bytes",    PARAM_INT, "Number of bytes to read"}, 
	  {"timeout",     PARAM_INT, "Timeout in us [0=Noblock, -1=Forever]"}, 
	  {"raw",         PARAM_INT, "Print raw string (not ASCII)"}, 
	  {NULL}}},
	{"SerialReadStr", cli_serial_read_str, "Read the serial line until term.",
	 {{"nr_bytes",    PARAM_INT, "Number of bytes to read"}, 
	  {"term",        PARAM_STR, "Terminator string"}, 
	  {"timeout",     PARAM_INT, "Timeout in us [0=Noblock, -1=Forever]"}, 
	  {"raw",         PARAM_INT, "Print raw string (not ASCII)"}, 
	  {NULL}}},
	{"SerialWrite",   cli_serial_write, "Write bytes to the serial line",
	 {{"string",      PARAM_STR, "String to write - can use \" or '"}, 
	  {"block_size",  PARAM_INT, "Nr. of bytes before a delay"}, 
	  {"delay",       PARAM_INT, "Time between writes, in us"}, 
	  {"block",       PARAM_INT, "Wait until end of TX [0=No, 1=Yes]"}, 
	  {NULL}}},
	{"SerialFlush",   cli_serial_flush, "Flush the serial RX buffer",
	 {{NULL}}},
	{"FoclaSerialRead",cli_focla_serial_read, "Read bytes from the FOCLA "
					      "serial line",
	 {{"nr_bytes",    PARAM_INT, "Number of bytes to read"}, 
	  {"timeout",     PARAM_INT, "Timeout in us [0=Noblock, -1=Forever]"}, 
	  {"raw",         PARAM_INT, "Print raw string (not ASCII)"}, 
	  {NULL}}},
	{"FoclaSerialReadStr",cli_focla_serial_read_str, "Read the FOCLA serial"
						     " line until term.",
	 {{"nr_bytes",    PARAM_INT, "Number of bytes to read"}, 
	  {"term",        PARAM_STR, "Terminator string"}, 
	  {"timeout",     PARAM_INT, "Timeout in us [0=Noblock, -1=Forever]"}, 
	  {"raw",         PARAM_INT, "Print raw string (not ASCII)"}, 
	  {NULL}}},
	{"FoclaSerialWrite",cli_focla_serial_write, "Write bytes to the FOCLA "
						"serial line",
	 {{"string",      PARAM_STR, "String to write - can use \" or '"}, 
	  {"block_size",  PARAM_INT, "Nr. of bytes before a delay"}, 
	  {"delay",       PARAM_INT, "Time between writes, in us"}, 
	  {"block",       PARAM_INT, "Wait until end of TX [0=No, 1=Yes]"}, 
	  {NULL}}},
	{"FoclaSerialFlush",cli_focla_serial_flush, "Flush the FOCLA serial RX "
						"buffer",
	 {{NULL}}},
	{"FoclaPulseStart", cli_focla_pulse_start, "Start a train pulses on CL CCx",
	 {{"cam_nr",      PARAM_INT, "Camera number [0-1]"}, 
	  {"sig_name",    PARAM_STR, "Signal name"}, 
	  {"polarity",    PARAM_INT, "Pulse polarity [0=Pos, 1=Neg]"}, 
	  {"width_us",    PARAM_INT, "Pulse width in usec"}, 
	  {"delay_us",    PARAM_INT, "Pulse delay in usec"}, 
	  {"nr_pulse",    PARAM_INT, "Nr. pulses to send"}, 
	  {NULL}}},
	{"FoclaPulseStop", cli_focla_pulse_stop, "Stop the FOCLA CL CCx pulses",
	 {{"cam_nr",      PARAM_INT, "Camera number [0-1]"}, 
	  {"sig_name",    PARAM_STR, "Signal name"}, 
	  {NULL}}},
	{"FoclaPulseStatus", cli_focla_pulse_status, "Get the CL CCx pulses status",
	 {{"cam_nr",      PARAM_INT, "Camera number [0-1]"}, 
	  {"sig_name",    PARAM_STR, "Signal name"}, 
	  {NULL}}},
	{"GetStats",      cli_get_stats, "Get the acquisition statistics",
	 {{NULL}}},
	{"DebugLevel",    cli_set_debug_level, "Set driver or lib. debug level",
	 {{"deb_lvl",     PARAM_INT, "Debug level [0=None, 3=Param, -1=Get]"}, 
	  {"drv",         PARAM_INT, "Set driver [=1] or lib [=0] deb lvl"}, 
	  {NULL}}},
	{"ReadRegister",  cli_read_register, "Read from a card register",
	 {{"reg_off",     PARAM_INT, "Register offset in bytes [0-255]"}, 
	  {NULL}}},
	{"WriteRegister", cli_write_register, "Read/write an Espia register",
	 {{"reg_off",     PARAM_INT, "Register offset in bytes [0-255]"}, 
	  {"data",        PARAM_INT, "Value to write"}, 
	  {"mask",        PARAM_INT, "Mask of bits to modify"}, 
	  {NULL}}},
	{"ParamList",     cli_param_list, "List available Espia parameters",
	 {{NULL}}},
	{"GetParam",      cli_get_param, "Read one or all Espia parameters",
	 {{"param",       PARAM_STR, "Espia parameter name or ALL"}, 
	  {NULL}}},
	{"SetParam",      cli_set_param, "Set the specified Espia parameter",
	 {{"param",       PARAM_STR, "Espia parameter name or ALL"}, 
	  {"val",         PARAM_INT, "Parameter value"}, 
	  {NULL}}},
	{"SerialMmapInit",cli_serial_mmap_init, "Initialize mmap files for "
	                                    "serial ops",
	 {{"max_bytes",   PARAM_INT, "Maximum nb of bytes in transfer"},
	  {NULL}}},
	{"SerialMmapRead",cli_serial_mmap_read, "Read bytes from serial and write "
					 "to mmap file",
	 {{"nr_bytes",    PARAM_INT, "Number of bytes to read"},
	  {"timeout",     PARAM_INT, "Timeout in us [0=Noblock, -1=Forever]"},
	  {"print", 	  PARAM_INT, "Print out raw string read [0=No, 1=Yes]"},
	  {NULL}}},
	{"SerialMmapWrite", cli_serial_mmap_write, "Write bytes read from mmap "
					       "file to serial",
	 {{"block_size",  PARAM_INT, "Nr. of bytes before a delay"},
	  {"delay",       PARAM_INT, "Time between writes, in us"},
	  {"block",       PARAM_INT, "Wait until end of TX [0=No, 1=Yes]"},
	  {"print",       PARAM_INT, "Print out raw str written [0=No, 1=Yes]"},
	  {NULL}}},
	{"BufferList",    cli_buffer_list, "List the allocated buffers' info",
	 {{NULL}}},
	{"BufferDump",    cli_buffer_dump, "Show the binary data in buffer",
	 {{"buffer_nr",   PARAM_INT, "Nr. of buffer"}, 
	  {"offset",      PARAM_INT, "Nr. bytes to skip at begin"}, 
	  {"nr_bytes",    PARAM_INT, "Nr. of bytes to dump"}, 
	  {NULL}}},
#ifdef WITH_IMAGE
	{"BufferImageRates",cli_buffer_img_rates, "Print buffer display rates",
	 {{"buffer_nr",   PARAM_INT, "Nr. of buffer"}, 
	  {NULL}}},
	{"BufferImageNorm",cli_buffer_img_norm, "Set/Get buffer display norm.",
	 {{"buffer_nr",   PARAM_INT, "Nr. of buffer [-1=Live]"}, 
	  {"min",         PARAM_INT, "Normalization min value [-1=Get]"}, 
	  {"max",         PARAM_INT, "Normalization max value [-1=Get]"}, 
	  {"auto_range",  PARAM_INT, "Auto-range [-1=Get]"}, 
	  {NULL}}},
#endif // WITH_IMAGE
	{"RoISet",        cli_roi_set, "Set a Scatter Gather based RoI",
	 {{"full_width",  PARAM_INT, "Full frame width"}, 
	  {"full_height", PARAM_INT, "Full frame height"}, 
	  {"roi_left",    PARAM_INT, "RoI left border"}, 
	  {"roi_top",     PARAM_INT, "RoI top border"}, 
	  {NULL}}},
	{"RoIReSet",      cli_roi_reset, "Reset a Scatter Gather based RoI",
	 {{NULL}}},
	{"RegAcqCb",      cli_reg_acq_cb, "Register an acquisition callback",
	 {{"buffer_nr",   PARAM_INT, "Nr. of buffer [-1=Any, -2=Each]"}, 
	  {"frame_nr",    PARAM_INT, "Frame idx in buffer [-1=Any]"}, 
	  {"count",       PARAM_INT, "Nr. of (re)write times [-1=Any]"}, 
	  {"acq_frame_nr",PARAM_INT, "Frame idx in acq. [-1=Any, -2=Each]"}, 
	  {"timeout",     PARAM_INT, "Timeout in us [-1=Forever]"}, 
	  {NULL}}},
	{"RegStatusCb",   cli_reg_status_cb, "Register a CCD status callback",
	 {{"timeout",     PARAM_INT, "Timeout in us [-1=Forever]"}, 
	  {NULL}}},
	{"RegSerRxCb",    cli_reg_ser_rx_cb, "Register a serial RX callback",
	 {{"buffer_len",  PARAM_INT, "Length of the RX buffer [0=10KBytes]"}, 
	  {"term_str",    PARAM_STR, "Terminator (EOS) string"}, 
	  {"timeout",     PARAM_INT, "Timeout in us [-1=Forever]"}, 
	  {"raw",         PARAM_INT, "Print raw string (not ASCII)"}, 
	  {"filter_tout", PARAM_INT, "Filter timeout events"}, 
	  {NULL}}},
	{"UnregCb",       cli_unreg_cb, "Unregister a callback",
	 {{"cb_nr",       PARAM_INT, "Nr. of callback"}, 
	  {NULL}}},
	{"CbActive",      cli_cb_active, "Activate/deactivate a callback",
	 {{"cb_nr",       PARAM_INT, "Nr. of callback"}, 
	  {"active",      PARAM_INT, "Callback active [0=No,1=Yes,-1=Get]"}, 
	  {NULL}}},
	{"CbList",        cli_cb_list, "List all the registered callbacks",
	 {{"type",        PARAM_STR, "[Acq, Status, SerialRx, SerialTx]"}, 
	  {NULL}}},
	{"FrameAddr",     cli_frame_addr, "Get the frame address",
	 {{"buffer_nr",   PARAM_INT, "Nr. of buffer [-1=AcqFrameNr]"}, 
	  {"frame_nr",    PARAM_INT, "Frame idx in buffer or acq."}, 
	  {NULL}}},
	{"CcdStatus",     cli_ccd_status, "Get CCD status, can block until change",
	 {{"timeout",     PARAM_INT, "Timeout in us [-1=BlockForever]"}, 
	  {NULL}}},
	{"RoiAccBuffer",  cli_roi_acc_buffer, "Create the RoI accumulation buffer",
	 {{"col_beg",     PARAM_INT, "RoI first column"}, 
	  {"col_end",     PARAM_INT, "RoI last  column"}, 
	  {"row_beg",     PARAM_INT, "RoI first row"}, 
	  {"row_end",     PARAM_INT, "RoI last  row"}, 
	  {"nr_frames",   PARAM_INT, "Number of frames in buffer [0=Auto]"}, 
	  {"file_prefix", PARAM_STR, "File name prefix [\"\"=NoSave]"}, 
	  {"file_idx",    PARAM_INT, "File name index [-1=KeepLast]"}, 
	  {NULL}}},
	{"FoclaReadReg",  cli_focla_read, "Read a FOCLA register",
	 {{"reg",         PARAM_INT, "Register nr [0-15]"}, 
	  {NULL}}},
	{"FoclaWriteReg", cli_focla_write, "Write on a FOCLA register",
	 {{"reg",         PARAM_INT, "Register nr [0-15]"}, 
	  {"val",         PARAM_INT, "Value to write"}, 
	  {NULL}}},
	{"FoclaParamList",cli_focla_param_list, "List available FOCLA parameters",
	 {{NULL}}},
	{"FoclaSignalList", cli_focla_sig_list, "List FOCLA pulsable signal names",
	 {{NULL}}},
	{"PollSleep",     cli_poll_sleep, "Set the main loop polling sleep time",
	 {{"sleep_time",  PARAM_INT, "Sleep time in usec [-1=Get]"}, 
	  {NULL}}},
	{"FirmwareList",  cli_firmware_list, "List all the known Espia firmwares",
	 {{NULL}}},
	{"Version",       cli_get_version, "Print the library versions",
	 {{NULL}}},
	{"PluginReg",     cli_plugin_reg, "Register library plugin",
	 {{"type",        PARAM_INT, "Plugin type [1=FPO, 2=Acq CB]"}, 
	  {"plugin_func", PARAM_STR, "Plugin function name"}, 
	  {NULL}}},
	{"PluginList",    cli_plugin_list, "List installed plugins",
	 {{NULL}}},
	{"PluginActive",  cli_plugin_active, "Activate/deactivate a plugin",
	 {{"type",        PARAM_INT, "Plugin type [1=FPO, 2=Acq CB]"}, 
	  {"plug_nr",     PARAM_INT, "Nr. of the plugin"}, 
	  {"active",      PARAM_INT, "Plugin active [0=No,1=Yes]"}, 
	  {NULL}}},
	{"PluginDel",     cli_plugin_delete, "Delete a plugin",
	 {{"type",        PARAM_INT, "Plugin type [1=FPO, 2=Acq CB]"}, 
	  {"plug_nr",     PARAM_INT, "Nr. of the plugin"}, 
	  {NULL}}},
	{"PluginOutput",  cli_plugin_output, "Set a plugin frame dim. change",
	 {{"type",        PARAM_INT, "Plugin type [1=FPO, 2=Acq CB]"}, 
	  {"plug_nr",     PARAM_INT, "Nr. of the plugin"}, 
	  {"width",       PARAM_INT, "Frame width"}, 
	  {"height",      PARAM_INT, "Frame height"}, 
	  {"depth",       PARAM_INT, "Frame depth"}, 
	  {NULL}}},
	{"EdfUsrHeader",  cli_edf_usr_header, "Add custom lines to the EDF header",
	 {{"usr_header",  PARAM_STR, "A string in the format "
	                  "\"key1 = value1 ;[\\nkey2 = value2 ;...]"
	                  " or \"\" to cancel the previously set string"}, 
	  {NULL}}},
	{"NonblockFrameSave", cli_nonblock_frame_save, "Nonblocking frame save",
	 {{"acq_frame_nr",PARAM_INT, "Frame idx in the acquisition"},
	  {NULL}}},
	{"NonblockSaveMode", cli_nonblock_save_mode, "Set non-block saving mode",
	 {{"mode",      PARAM_INT, "Serial [1] or parallel [0] (def.) saving"}, 
	  {NULL}}},
	{"NonblockSaveDisable", cli_nonblock_save_stop, "Disable non-blocking "
	                                                "buffer save",
	 {{NULL}}},
	{NULL}
};


/*--------------------------------------------------------------------------
 * app_framework polling functions
 *--------------------------------------------------------------------------*/

#ifdef WITH_IMAGE

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_image_refresh( app_context_t context )
{
	struct cli_data *cli_data = cli_data_from_context(context);

	return lib_image_refresh( cli_data->etest );
}

#endif // WITH_IMAGE

poll_funct *test_poll_funct[] = {
#ifdef WITH_IMAGE
	(poll_funct *) cli_image_refresh,
#endif // WITH_IMAGE
};

int test_poll_funct_len = (sizeof(test_poll_funct) / 
			   sizeof(test_poll_funct[0]));


/*--------------------------------------------------------------------------
 * init / cleanup
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_test_init(app_context_t context, int argc, char *argv[])
{
	int len, ret, i;
	union param dev_nr;
	struct cli_data *cli_data;

	len = sizeof(struct cli_data);
	cli_data = (struct cli_data *) calloc(1, len);
	if (cli_data == NULL) {
		context_err(context, "Error alloc. CLI data (%d bytes)\n", len);
		return -ENOMEM;
	}

	cli_data->lib_handle = NULL;
	cli_data->active_dev = (int) SCDXIPCI_INVALID;
	
	init_context(context, cli_funcs, test_error_funct, cli_data);

	ret = lib_test_init(&cli_data->etest, argc, argv);
	if (ret < 0) {
		context_err(context,"Error opening devices : %s\n",
			    espia_test_strerror(ret));
		goto out;
	}

	init_context_poll(context, test_poll_funct, test_poll_funct_len, 
			  ESPIA_TEST_POLL_TIME);

	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-')
			continue;

		dev_nr.i = atoi( argv[i] );
		ret = cli_open_dev( context, &dev_nr );
		if (ret != ESPIA_OK)
			goto out;  /* or continue ??? */
	}

 out:
	if (ret < 0)
		cli_test_cleanup(context);

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_test_cleanup(app_context_t context)
{
	int cli_ret=ESPIA_OK;
	struct cli_data *cli_data=cli_data_from_context(context);


	if( cli_data->lib_handle ) 
		dlclose(cli_data->lib_handle);

	if (cli_data->etest)
		cli_ret = lib_test_cleanup( cli_data->etest );

	free(cli_data);
	cleanup_context(context);

	return cli_ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic : Gives the error associated to a return value, NULL means no  *
 *             error                                                        *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
char *test_error_funct(app_context_t context, struct funct_data *fdata,
		       int ret)
{
	return (ret >= ESPIA_OK) ? NULL : espia_test_strerror(ret);
}


/*--------------------------------------------------------------------------
 * open / close / select
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_open_dev(app_context_t context, union param *pars)
{
	int ret, dev_nr=pars[0].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	ret = lib_open_dev( cli_data->etest, dev_nr );
	if (ret < 0) {
		context_err(context, "Error opening device number %d : %s\n",
		             dev_nr, espia_test_strerror(ret) );
	} else {
		cli_data->active_dev = dev_nr;  /* select device ??? */
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_close_dev(app_context_t context, union param *pars)
{
	int ret;
	struct cli_data *cli_data=cli_data_from_context(context);


	/* ??? Always closing only the active device ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else
		ret = lib_close_dev( cli_data->etest, cli_data->active_dev );

	if (ret < 0) {
		context_err(context, "Error closing device number %d : %s\n",
		             cli_data->active_dev, espia_test_strerror(ret) );
	} else {
		cli_data->active_dev = (int) SCDXIPCI_INVALID;
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_select_dev(app_context_t context, union param *pars)
{
	espia_t espia_dev;
	int ret, dev_nr=pars[0].i;
	struct cli_data *cli_data = cli_data_from_context(context);

	if (!is_good_dev_nr(dev_nr))
		return ESPIA_TEST_ERR_DEVNR;

	ret = lib_get_dev( cli_data->etest, dev_nr, &espia_dev );
	if (ret < 0)
		return ret;
	else if (espia_dev == ESPIA_DEV_INVAL)
		return ESPIA_TEST_ERR_NOOPEN;

	cli_data->active_dev = dev_nr;
	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_dev_list(app_context_t context, union param *pars)
{
	char *sel;
	int i, open=0, ret;
	espia_t espia_dev;
	struct cli_data *cli_data=cli_data_from_context(context);


	for ( i = 0; i < ESPIA_TEST_MAX_NR_DEV; i++ ) {

		ret = lib_get_dev( cli_data->etest, i, &espia_dev );
		if (ret < 0)
			return ret;
		if ( espia_dev != ESPIA_DEV_INVAL ) {
			sel = (i == cli_data->active_dev) ? " (selected)" : "";
			context_out(context, "Device #%d is open%s\n", i, sel);
			open++;
		}
	}
	if (open == 0)
		context_out(context, "No device open\n");

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * driver option
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_option_list(app_context_t context, union param *pars)
{
	int i, nr_option, ret;
	struct lib_option option;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;

	nr_option = -1;
	ret = lib_espia_get_option_data( cli_data->etest, cli_data->active_dev,
	                                 &nr_option, NULL );
	if (ret < 0)
		return ret;

	context_out(context, "  Name                       Code \n");
	context_out(context, "----------------------------------\n");

	for (i = 0; i < nr_option; i++) {
		ret = lib_espia_get_option_data(cli_data->etest, 
		                                cli_data->active_dev,
		                                &i, &option);
		if (ret < 0)
			return ret;
		context_out(context, " %-27s ", option.name);
		if (strlen(option.name) > 27)
			context_out(context, "\n %27s ", "");
		context_out(context, "%2d\n", option.attr);
	}

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_get_option( app_context_t context, union param *pars )
{
	char *name;
	struct lib_option eoption;
	int ret, i, first, end, nr_option, option, action, val;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;

	nr_option = -1;
	ret = lib_espia_get_option_data( cli_data->etest, cli_data->active_dev,
	                                 &nr_option, NULL );
	if (ret < 0)
		return ret;

	name = get_param_str(&pars[0]);
	if (strcasecmp(name, "ALL") == 0) {
		first = 0;
		end = nr_option;
	} else {
		for (first = 0; first < nr_option; first++) {
			ret = lib_espia_get_option_data( cli_data->etest, 
			                                 cli_data->active_dev, 
			                                 &first, &eoption );
			if (ret < 0)
				return ret;
			if (strcasecmp(name, eoption.name) == 0)
				break;
		}
		if (first == nr_option)
			return ESPIA_TEST_ERR_OPTION;
		end = first + 1;
	}

	context_out(context, "  Name                       Value     \n");
	context_out(context, "---------------------------------------\n");

	action = SCDXIPCI_OPT_RD;
	for (i = first; i < end; i++) {
		ret = lib_espia_get_option_data( cli_data->etest, 
		                                 cli_data->active_dev, 
		                                 &i, &eoption );
		if (ret < 0)
			return ret;

		option = eoption.attr;
		ret = lib_espia_option( cli_data->etest, cli_data->active_dev,
		                        option, action, &val );
		if (ret < 0)
			return ret;

		context_out(context, " %-27s ",  eoption.name);
		if (strlen(eoption.name) > 27)
			context_out(context, "\n %27s ", "");
		context_out(context, "%d (0x%x)\n", val, val);
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_set_option(app_context_t context, union param *pars)
{
	int ret, old_val, new_val=pars[1].i;
	char *name = get_param_str(&pars[0]);
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else
		ret = lib_set_option( cli_data->etest, cli_data->active_dev,
		                         name, new_val, &old_val );

	if (ret >= 0) {
		context_out(context, "  Name                     Prev. Value \n");
		context_out(context, "---------------------------------------\n");
		context_out(context, " %-27s ", name);
		if( strlen(name) > 27 )
			context_out(context, "\n %27s ", "");
		context_out(context, "%d (0x%x)\n", old_val, old_val);
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * debug level
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_set_debug_level(app_context_t context, union param *pars)
{
	int ret, deb_lvl, drv;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		deb_lvl = pars[0].i;
		drv = pars[1].i;
		ret = lib_set_debug_level(cli_data->etest, cli_data->active_dev,
		                           &deb_lvl, drv);
	}

	if (ret >= 0) {
		context_out(context, "Previous debug level: %d\n", deb_lvl);
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * read/write register
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_read_register(app_context_t context, union param *pars)
{
	int ret;
	unsigned int val;
	unsigned long reg_off;
	char binbuf[32 + 7 + 1];
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		reg_off = pars[0].i;
		ret = lib_read_register( cli_data->etest, cli_data->active_dev,
		                         reg_off, &val );
	}

	if (ret >= 0) {
		context_out(context, "Register 0x%02x value: %010u [%s]\n", 
			    reg_off, val, bin_repr(binbuf, val));
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_write_register(app_context_t context, union param *pars)
{
	int ret;
	unsigned long reg_off;
	unsigned int val, mask;
	char binbuf[32 + 7 + 1];
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		val = pars[1].i;
		mask = pars[2].i;
		reg_off = pars[0].i;
		ret = lib_write_register( cli_data->etest, cli_data->active_dev,
		                          reg_off, &val, mask );
	}

	if (ret >= 0) {
		context_out(context, "Prev. register 0x%02x value: %010u "
			    "[%s]\n", reg_off, val, bin_repr(binbuf, val));
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * param list 
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_param_list(app_context_t context, union param *pars)
{
	int ret, i, nr_param;
	struct espia_param eparam;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		return ESPIA_TEST_ERR_SELECT;

	nr_param = -1;
	ret = lib_espia_get_param_data( cli_data->etest, cli_data->active_dev,
	                                &nr_param, NULL );
	if (ret < 0)
		return ret;

	context_out(context, "  Name                       Reg   Mask   Shift  "
		    "Description            \n");
	context_out(context, "-------------------------------------------------"
		    "-----------------------\n");

	for (i = 0; i < nr_param; i++) {
		ret = lib_espia_get_param_data( cli_data->etest, 
		                                cli_data->active_dev,
	                                        &i, &eparam );
		if (ret < 0)
			return ret;
		context_out(context, " %-27s ",  eparam.name);
		if (strlen(eparam.name) > 27)
			context_out(context, "\n %27s ", "");
		context_out(context, "%2d 0x%08x %2d  %s\n", eparam.reg, 
			    eparam.mask, eparam.shift, eparam.desc);
	}

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * get/set parameter
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_get_param(app_context_t context, union param *pars)
{
	char *param;
	unsigned int *val_arr;
	struct espia_param eparam;
	int ret, i, first, end, len, *param_arr, nr_param;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		return ESPIA_TEST_ERR_SELECT;

	nr_param = -1;
	ret = lib_espia_get_param_data( cli_data->etest, cli_data->active_dev,
	                                &nr_param, NULL );
	if (ret < 0)
		return ret;

	param = get_param_str(&pars[0]);
	if (strcasecmp(param, "ALL") == 0) {
		first = 0;
		end = nr_param;
	} else {
		for (first = 0; first < nr_param; first++) {
			ret = lib_espia_get_param_data( cli_data->etest, 
		                                        cli_data->active_dev,
		                                        &first, &eparam );
			if (ret < 0)
				return ret;
			if (strcasecmp(param, eparam.name) == 0)
				break;
		}
		if (first == nr_param)
			return ESPIA_TEST_ERR_PARAM;
		end = first + 1;
	}
	len = end - first;

	val_arr = NULL;

	param_arr = (int *) calloc(len, sizeof(*param_arr));
	if (param_arr == NULL) {
		ret = -ENOMEM;
		goto out;
	}
	val_arr = (unsigned int *) calloc(len, sizeof(*val_arr));
	if (val_arr == NULL) {
		ret = -ENOMEM;
		goto out;
	}

	for (i = 0; i < len; i++)
		param_arr[i] = first + i;
	ret = lib_espia_get_param( cli_data->etest, cli_data->active_dev,
	                           param_arr, val_arr, len );
	if (ret < 0)
		goto out;

	context_out(context, " Name                                       "
		    "Value   \n");
	context_out(context, "--------------------------------------------"
		    "--------\n");
	for (i = 0; i < len; i++) {
		ret = lib_espia_get_param_data( cli_data->etest, 
		                                cli_data->active_dev, 
		                                &param_arr[i], &eparam );
		if (ret < 0)
			goto out;
		context_out(context, " %-40s  %u (0x%x)\n", eparam.name,
			    val_arr[i], val_arr[i]);
	}	
 out:
	if (param_arr)
		free(param_arr);
	if (val_arr)
		free(val_arr);

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_set_param(app_context_t context, union param *pars)
{
#if 1
	int ret;
	char *param;
	unsigned int val;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		param = get_param_str(&pars[0]);
		val = pars[1].i;
		ret = lib_set_param( cli_data->etest, cli_data->active_dev,
		                     param, &val );
	}

	if (ret >= 0) {
		context_out( context, "Previous %s value: %u (0x%x)\n",
			     param, val, val);
	}

	return ret;

#else

	char *param;
	int idx, nr_param, ret;
	unsigned int val;
	struct espia_param eparam;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		return ESPIA_TEST_ERR_SELECT;

	nr_param = -1;
	ret = lib_espia_get_param_data( cli_data->etest, cli_data->active_dev,
	                                &nr_param, NULL );
	if (ret < 0)
		return ret;

	param = get_param_str(&pars[0]);
	for (idx = 0; idx < nr_param; idx++) {
		ret = lib_espia_get_param_data( cli_data->etest, 
		                                cli_data->active_dev, 
		                                &idx, &eparam );
		if (ret < 0)
			return ret;
		if (strcasecmp(param, eparam.name) == 0)
			break;
	}
	if (idx == nr_param)
		return ESPIA_TEST_ERR_PARAM;

	val = pars[1].i;
	ret = lib_espia_set_param( cli_data->etest, cli_data->active_dev, 
	                           &idx, &val, 1 );
	if (ret >= 0)
		context_out(context, "Previous %s value: %u (0x%x)\n",
			    eparam.name, val, val);
	return ret;
#endif /* 0/1 */
}


/*--------------------------------------------------------------------------
 * reset link
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_reset_link(app_context_t context, union param *pars)
{
	int ret;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else
		ret = lib_reset_link( cli_data->etest, cli_data->active_dev );

	return ret;
}


/*--------------------------------------------------------------------------
 * serial helpers
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
char *get_char_repr(char ch) 
{
	static char c[2] = {0, 0};
	switch ((c[0] = ch)) {
	case '\n': return "\\n";
	case '\r': return "\\r";
	case '\t': return "\\t";
	case '"':  return "\\""\"";
	default:   return c;
	}
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
void print_serial_str(app_context_t context, char *buffer, 
		      int nr_bytes, int raw)
{
	context_out(context, "Received %d bytes%c\n", nr_bytes,
		(nr_bytes > 0) ? ':' : '.');
	if (raw)
		dump_data(context, buffer, nr_bytes);
	else
		context_out(context, "%s", buffer);
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int alloc_serial_buffer(app_context_t context, int nr_bytes, char **buffer_ptr)
{
	*buffer_ptr = NULL;
	if (nr_bytes > 0) {
		*buffer_ptr = calloc(nr_bytes, 1);
		if (*buffer_ptr == NULL) {
			context_err(context, "Error cannot allocate %ld "
				    "bytes!\n", nr_bytes);
			return ESPIA_TEST_ERR_NOMEM;
		}
	}

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int print_free_serial_buffer(app_context_t context, char *buffer, int nr_bytes,
			     int raw, int ret)
{
	if (!buffer) {
		if (ret == ESPIA_OK)
			context_out(context, "Available %ld byte(s).\n", 
				    nr_bytes);
		return ret;
	}

	if (ret == ESPIA_OK)
		print_serial_str(context, buffer, nr_bytes, raw);

	free(buffer);

	return ret;
}


/*--------------------------------------------------------------------------
 * serial read/write/flush
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_serial_read(app_context_t context, union param *pars)
{
	char *buffer;
	long timeout=pars[1].i;
	unsigned long nr_bytes=pars[0].i;
	int ret, raw=pars[2].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {

		ret = alloc_serial_buffer(context, nr_bytes, &buffer);
		if (ret < 0)
			return ret;

		ret = lib_serial_read( cli_data->etest, cli_data->active_dev,
		                       buffer, &nr_bytes, timeout );

		print_free_serial_buffer( context, buffer, nr_bytes, raw, ret );
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_serial_read_str(app_context_t context, union param *pars)
{
	char *buffer;
	char *term=pars[1].s;
	long timeout=pars[2].i;
	int ret, raw=pars[3].i;
	unsigned long nr_bytes=pars[0].i, term_bytes=pars[1].len;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {

		ret = alloc_serial_buffer(context, nr_bytes, &buffer);
		if (ret < 0)
			return ret;

		ret = lib_serial_read_str( cli_data->etest, 
		                           cli_data->active_dev,
		                           buffer, &nr_bytes, term, 
		                           term_bytes, timeout );

		print_free_serial_buffer( context, buffer, nr_bytes, raw, ret );
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_serial_write(app_context_t context, union param *pars)
{
	int ret, block=pars[3].i;
	char *buffer=pars[0].s, *action;
	unsigned long nr_bytes=pars[0].len, block_size=pars[1].i, delay=pars[2].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		ret = lib_serial_write( cli_data->etest, cli_data->active_dev,
					buffer, &nr_bytes, block_size, delay,
					block );
	}

	if( ret == ESPIA_OK ) {
		action = (pars[0].len > 0) ? "Transmited" : "Still to TX:";
		context_out(context, "%s %ld bytes.\n", action, nr_bytes);
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_serial_flush(app_context_t context, union param *pars)
{
	int ret;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		ret = lib_serial_flush( cli_data->etest, cli_data->active_dev );
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * serial read/write
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN : arg#0 = max bytes to read or write                           *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_serial_mmap_init(app_context_t context, union param *pars)
{
	int ret, max_bytes;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		max_bytes = pars[0].i;
		ret = lib_serial_mmap_init( cli_data->etest, 
		                            cli_data->active_dev, max_bytes );
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_serial_mmap_close(app_context_t context, union param *pars)
{
	int ret;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else
		ret = lib_serial_mmap_close( cli_data->etest, 
		                             cli_data->active_dev );

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN : $0 = block_size = Nr. of bytes before a delay                *
 *             $1 = delay = Time between writes, in us                      *
 *             $2 = block = Wait until end of TX [0=No, 1=Yes]              *
 *             #3 = print out serial string written (debug)                 *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_serial_mmap_write(app_context_t context, union param *pars)
{
	int ret, block=pars[2].i;
	unsigned long nr_bytes, block_size=pars[0].i, delay=pars[1].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else
		ret = lib_serial_mmap_write( cli_data->etest, 
		                             cli_data->active_dev, &nr_bytes,
		                             block_size, delay, block );

	if (ret >= 0) {
		context_out(context, "Transmited %ld bytes.\n", nr_bytes);
		if (pars[3].i) {
			/* The written data is private, sorry... */
			context_out( context, "The written string is not "
			                      "available.\n" );
		}
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN : $0 = nr_bytes = Number of bytes to read                      *
 *             $1 = timeout = Timeout in us [0=Noblock, -1=Forever]         *
 *             $2 = print out read raw string                               *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_serial_mmap_read(app_context_t context, union param *pars)
{
	char *buffer=NULL;
	long timeout=pars[1].i;
	unsigned long nr_bytes=pars[0].i;
	int ret, output=pars[2].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		if (nr_bytes > 0) {
			buffer = calloc(nr_bytes, 1);
			if (buffer == NULL) {
				context_err(context, "Error cannot allocate %ld"
					    " bytes!\n", nr_bytes);
				return ESPIA_TEST_ERR_NOMEM;
			}
		}

		ret = lib_serial_mmap_read( cli_data->etest, 
		                            cli_data->active_dev,
		                            buffer, &nr_bytes, timeout );

		if (ret == ESPIA_OK) {
			context_out(context, "Available %ld byte(s).\n", 
					    nr_bytes);
			if (output)
				print_serial_str(context, buffer, nr_bytes, 1);
		}
	}

	if( buffer ) free(buffer);
	return ret;
}


/*--------------------------------------------------------------------------
 * buffer post-operation
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_frame_post_op_list(app_context_t context, union param *pars)
{
	struct espia_frm_op_fn_set *fn_set;
	int ret, nr_op, op;

	nr_op = -1;
	ret = espia_get_frm_op_fn_set(&nr_op, NULL);
	if (ret < 0)
		return ret;

	context_out(context, 
		    "  Name                 Description                   "
		    "            \n"
		    "-----------------------------------------------------"
		    "------------\n");
	for (op = 0; op < nr_op; op++) {
		ret = espia_get_frm_op_fn_set(&op, &fn_set);
		if (ret < 0)
			return ret;

		if( ! strcmp(fn_set->name, "ACQ_PLUGIN_I") ) 
			continue;  /* Do not display the ACQ_PLUGIN_I FPO */

		context_out(context, " %-20s  %s\n", 
			    fn_set->name, fn_set->desc);
	}

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_frame_post_op_list_active( app_context_t context, union param *pars )
{
	int ret, nr_op, op;
	struct espia_frm_op_info op_info[ESPIA_TEST_MAX_OPS];
	struct espia_frm_op_fn_set *fn_set;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		ret = lib_frame_post_op_get_active( cli_data->etest, 
		                                    cli_data->active_dev,
		                                    op_info, &nr_op );

		context_out(context, 
			    "  Name                 Description                   "
			    "            \n"
			    "-----------------------------------------------------"
			    "------------\n");
		for (op = 0; op < nr_op; op++) {
			fn_set = op_info[op].fn_set;
			context_out( context, " %-20s  %s\n", 
			             fn_set->name, fn_set->desc );
		}

	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_frame_post_op(app_context_t context, union param *pars)
{
	int ret;
	char *list;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		list = get_param_str( &pars[0] );
		ret = lib_frame_post_op( cli_data->etest, cli_data->active_dev,
		                         list );
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * buffer alloc/free/list
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_buffer_alloc(app_context_t context, union param *pars)
{
	int ret, nr_buffer, frames, width, height, depth, display;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		nr_buffer  = pars[0].i;
		frames     = pars[1].i;
		width      = pars[2].i;
		height     = pars[3].i;
		depth      = pars[4].i;
		display    = pars[5].i;

		ret = lib_buffer_alloc( cli_data->etest, cli_data->active_dev,
		             nr_buffer, frames, width, height, depth, display );
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_buffer_free(app_context_t context, union param *pars)
{
	int ret;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else
		ret = lib_buffer_free( cli_data->etest, cli_data->active_dev );

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_buffer_list(app_context_t context, union param *pars)
{
	char dim[64], *img;
	struct espia_frame_dim fdim;
	struct img_buffer_info binfo;
	int ret, nr, nr_frames, nr_buffer;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		return ESPIA_TEST_ERR_SELECT;

	ret = lib_get_buffer_data( cli_data->etest, cli_data->active_dev, 
	                           &nr_buffer, &fdim );
	if( ret < 0 )
		return ret;
	else if( 0 == nr_buffer ) {
		context_out(context, "No allocated buffer\n");
		return ESPIA_OK;
	}

	context_out(context, 
		    " Buff #        Addr         Frames    Frm. Dim      "
		    "Frm. Size     Tot Size   Image  \n"
		    "----------------------------------------------------"
		    "--------------------------------\n");

	sprintf(dim, "%ldx%ldx%ld", fdim.width, fdim.height, fdim.depth);

	for (nr = 0; nr < nr_buffer; nr++) {
		ret = lib_virt_buffer_info( cli_data->etest, 
		                            cli_data->active_dev,
		                            nr, &binfo );
		if (ret < 0)
			return ret;
		nr_frames = binfo.nr_frames;
#ifdef WITH_IMAGE
		img = (lib_image_active(cli_data->etest, cli_data->active_dev, 
		                        nr) > 0) ? "Yes" : "No";
#else // WITH_IMAGE
		img = "---";
#endif // WITH_IMAGE
		context_out(context, "%5d    %14p  %7d    %11s  %10ld    "
			    "%10ld    %s\n", nr, binfo.ptr, nr_frames, dim, 
			    binfo.frame_size, nr_frames * binfo.frame_size,
			    img);
	}

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_buffer_dump(app_context_t context, union param *pars)
{
	unsigned char *ptr;
	long buffer_nr;
	int ret, offset, nr_bytes, buffer_len;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		buffer_nr = pars[0].i;
		offset = pars[1].i;
		nr_bytes = pars[2].i;

		ret = lib_get_data( cli_data->etest, cli_data->active_dev,
		                    buffer_nr, -1, &ptr, &buffer_len );
		
		if ((offset < 0) || (nr_bytes <= 0) || 
		    ((offset + nr_bytes) > buffer_len))
			ret = ESPIA_TEST_ERR_DUMPPAR;
		else {
			context_out( context, "Buffer %d (%p) - Dump from %p:\n\n", 
			             buffer_nr, ptr, ptr+offset );
			ptr += offset;
			ret = dump_data(context, ptr, nr_bytes);
		}
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_buffer_save_cb_print( app_context_t context )
{
	int ret, auto_save, idx, format, tot_file_frames, file_frames, overwrite;
	char prefix[FILE_PREFIX_NAME_LEN], file_name[FILE_FULL_NAME_LEN];
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		return ESPIA_TEST_ERR_SELECT;

	ret = lib_buffer_save_cb_get( cli_data->etest, cli_data->active_dev,
	                               &auto_save, prefix, &idx, &format,
	                               file_name, &tot_file_frames, 
	                               &file_frames, &overwrite );
	if (ret < 0)
		return ret;

	context_out(context, "Buffer auto-save status:\n");
	context_out(context, "  active=%d, prefix=\"%s\", idx=%d, format=%d\n", 
		    auto_save, prefix, idx, format);
	context_out(context, "  file_name=\"%s\"\n", file_name);
	context_out(context, "  tot_file_frames=%d, "
		    "curr_file_frames=%d, overwrite=%d\n", 
		    tot_file_frames, file_frames, overwrite);

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_buffer_save(app_context_t context, union param *pars)
{
	char *fname_pr;
	long buffer_nr;
	int ret, fname_idx=0, format=0, file_frames=1, overwrite=0;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		return ESPIA_TEST_ERR_SELECT;

	buffer_nr = pars[0].i;
	fname_pr = get_param_str(&pars[1]);

	if( (-1 == buffer_nr) && (strcmp(fname_pr, "?") == 0) ) {
		return cli_buffer_save_cb_print( context );
	}

	if (chk_opt_param_present(&pars[2]))
		fname_idx = pars[2].i;
	if (chk_opt_param_present(&pars[3]))
		format = pars[3].i;
	if (chk_opt_param_present(&pars[4]))
		file_frames = pars[4].i;
	if (file_frames <= 0)
		return -EINVAL;
	if (chk_opt_param_present(&pars[5]))
		overwrite = pars[5].i;

	ret = lib_buffer_save( cli_data->etest, cli_data->active_dev,
	       buffer_nr, fname_pr, fname_idx, format, file_frames, overwrite );

	return ret;
}


/*--------------------------------------------------------------------------
 * buffer clear
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_buffer_clear(app_context_t context, union param *pars)
{
	int ret, buffer_nr=pars[0].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		ret = lib_buffer_clear( cli_data->etest, cli_data->active_dev, 
		                        buffer_nr );
	}

	if (ret < 0) {
		context_err(context, "Error clearing buffer %d : %s\n", 
		            buffer_nr, espia_test_strerror(ret));
	}

	return ret;
}

#ifdef WITH_IMAGE

/*--------------------------------------------------------------------------
 * buffer image alloc/update/destroy/del_all
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_buffer_img(app_context_t context, union param *pars)
{
	int ret;
	long buffer_nr=pars[0].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		ret = lib_buffer_img( cli_data->etest, cli_data->active_dev,
		              buffer_nr );
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_buffer_img_del(app_context_t context, union param *pars)
{
	int ret;
	int buffer_nr = pars[0].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else
		ret = lib_buffer_img_del( cli_data->etest, cli_data->active_dev,
		                          buffer_nr );

	if (ret < 0) {
		context_err( context, "Error destroying display window buffer "
		                     "#%d on device #%d : %s\n", buffer_nr, 
		                     cli_data->active_dev,
		                     espia_test_strerror(ret) );
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_buffer_img_del_all(app_context_t context, union param *pars)
{
	struct cli_data *cli_data = cli_data_from_context(context);
	int ret;


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else
		ret = lib_buffer_img_del_all( cli_data->etest, 
		                              cli_data->active_dev );

	return ret;
}


/*--------------------------------------------------------------------------
 * buffer image rates/norm
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_buffer_img_rates(app_context_t context, union param *pars)
{
	float update, refresh;
	char name[BUFFER_IMG_NAME_LEN];
	int ret, buffer_nr=pars[0].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		return ESPIA_TEST_ERR_SELECT;

	ret = lib_buffer_img_name( cli_data->etest, 
				   cli_data->active_dev, buffer_nr, name );
	if (ret < 0)
		return ret;

	ret = lib_buffer_img_rates( cli_data->etest, 
				    cli_data->active_dev, buffer_nr,
				    &update, &refresh );
	if (ret >= 0) {
		context_out(context, "%s: update rate: %.1f, refresh rate: %.1f\n", 
		            name, update, refresh);
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_buffer_img_norm(app_context_t context, union param *pars)
{
	char name[BUFFER_IMG_NAME_LEN];
	long min_val=pars[1].i, max_val=pars[2].i;
	int ret, buffer_nr=pars[0].i, auto_range=pars[3].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		return ESPIA_TEST_ERR_SELECT;

	ret = lib_buffer_img_name( cli_data->etest, 
				   cli_data->active_dev, buffer_nr, name );
	if (ret < 0)
		return ret;

	ret = lib_buffer_img_norm( cli_data->etest, 
				   cli_data->active_dev, buffer_nr,
				   &min_val, &max_val, &auto_range );
	if (ret >= 0) {
		context_out(context, "%s norm.: min-val: %ld, max-val: %ld, "
		        "auto-range: %d\n", name, min_val, max_val, auto_range);
	}

	return ret;
}

#endif // WITH_IMAGE

/*--------------------------------------------------------------------------
 * RoI set / reset
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_roi_set(app_context_t context, union param *pars)
{
	int ret;
	unsigned long frame_width  = pars[0].i;
	unsigned long frame_height = pars[1].i;
	unsigned long roi_left     = pars[2].i;
	unsigned long roi_top      = pars[3].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		ret = lib_roi_set( cli_data->etest, cli_data->active_dev,
		                 frame_width, frame_height, roi_left, roi_top );
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_roi_reset(app_context_t context, union param *pars)
{
	int ret;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else
		ret = lib_roi_reset( cli_data->etest, cli_data->active_dev );

	return ret;
}


/*--------------------------------------------------------------------------
 * SG set
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int sg_config_list(app_context_t context)
{
	char *desc;
	int conf;

	context_out(context, "  Code      Description                     \n"
			     "--------------------------------------------\n");
	for (conf = 0; conf < espia_sg_img_config_nr; conf++) {
		desc = espia_sg_img_config_desc[conf];
		context_out(context, "    %d       %s\n", conf, desc);
	}

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_sg_set(app_context_t context, union param *pars)
{
	int ret;
	unsigned long img_config = pars[0].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	if (scdxipci_is(ANY, img_config))
		return sg_config_list(context);

	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		ret = lib_sg_set( cli_data->etest, cli_data->active_dev,
		                  img_config );
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * dev list set
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_dev_set(app_context_t context, union param *pars)
{
	char *list, *ptr, *term;
	int ret, dev_list[ESPIA_TEST_MAX_NR_DEV], nr_dev, idx;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		list = get_param_str(&pars[0]);

		for (nr_dev = 0, ptr = list; ptr && *ptr; nr_dev++, ptr = term) {
			term = strchr(ptr, ',');
			if (term != NULL)
				*term++ = 0;
			idx = atoi(ptr);
			if (!is_good_dev_nr(idx) || (idx == ESPIA_META_DEV))
				return ESPIA_TEST_ERR_DEVNR;

			dev_list[nr_dev] = idx;
		}

		ret = lib_dev_set( cli_data->etest, cli_data->active_dev,
		                   dev_list, nr_dev );
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * acq start/stop/status
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_start_acq(app_context_t context, union param *pars)
{
	long timeout;
	int ret, start_buffer, nr_frames;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		start_buffer = pars[0].i;
		nr_frames    = pars[1].i;
		timeout      = pars[2].i;

		ret = lib_start_acq( cli_data->etest, cli_data->active_dev,
		             start_buffer, nr_frames, timeout );
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_stop_acq(app_context_t context, union param *pars)
{
	int ret;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always stopping acq on the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else
		ret = lib_stop_acq( cli_data->etest, cli_data->active_dev );

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_acq_status(app_context_t context, union param *pars)
{
	unsigned long acq_run_nr;
	int ret, acq_active, auto_save_active;
	struct img_frame_info last_acq_frame, last_saved_frame;
	struct cli_data *cli_data=cli_data_from_context(context);


	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else
		ret = lib_acq_status( cli_data->etest, cli_data->active_dev,
		                     &acq_run_nr, &acq_active,&auto_save_active,
		                     &last_acq_frame, &last_saved_frame );

	if( ret < 0 ) {
		return ret;
	}

	context_out(context, "Acquisition %ld is %srunning.\n", 
		    acq_run_nr, acq_active ? "" : "not ");

	if( !acq_active && !acq_run_nr )
		return ESPIA_OK;

	context_out(context, "\nLast acquired frame:\n");
	if (last_frame_available(&last_acq_frame))
		print_finfo(context, NULL, &last_acq_frame, 0);
	else
		context_out(context, "  No available yet\n");

	if ( !auto_save_active )
		return ESPIA_OK;

	context_out(context, "Last saved frame:\n");
	if (last_frame_available(&last_saved_frame))
		print_finfo(context, NULL, &last_saved_frame, 0);
	else
		context_out(context, "  No available yet\n");

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * callback helpers & functions
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
void print_finfo(app_context_t context, 
		 struct img_frame_info *req_finfo,
		 struct img_frame_info *cb_finfo, int ret)
{
	double sec = (double) cb_finfo->time_us / 1e6;

	if (finished_espia_frame_info(cb_finfo, ret)) {
		context_out(context, "  Aborted or finished after %.3f s\n",
			    sec);
		return;
	}

	context_out(context, "  buffer_nr=%ld, frame_nr=%ld, round_count=%ld,"
		    " acq_frame_nr=%ld\n", cb_finfo->buffer_nr, 
		    cb_finfo->frame_nr,	cb_finfo->round_count, 
		    cb_finfo->acq_frame_nr);
	context_out(context, "  sec=%.3f, pixels=%ld\n\n", sec,
		    cb_finfo->pixels);
}


/*--------------------------------------------------------------------------
 * callback register/unregister/unregister_all/list
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_acq_cb_funct( etest_t etest, int dev_nr, struct espia_cb_data *cb_data, 
                      void *user_data )
{
	int each = espia_cb_each(&cb_data->info.acq.req_finfo);
	app_context_t context = (app_context_t) user_data;
	char *each_str;


	if( ! context ) return ESPIA_ERR_PTRNULL;

	each_str =  (each == ESPIA_EACH_BUFFER) ? " - Each buffer" :
		    ((each == ESPIA_EACH_FRAME)  ? " - Each frame"  : "");

	context_out(context, "\n\nAcq. callback #%d for dev #%d%s:\n", 
		    cb_data->cb_nr, dev_nr, each_str);
	print_finfo(context, &cb_data->info.acq.req_finfo, 
		    &cb_data->info.acq.cb_finfo, cb_data->ret);

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_reg_acq_cb(app_context_t context, union param *pars)
{
	int ret;
	long buffer_nr    = pars[0].i;
	long frame_nr     = pars[1].i;
	long round_count  = pars[2].i;
	long acq_frame_nr = pars[3].i;
	long timeout      = pars[4].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		ret = lib_reg_acq_cb( cli_data->etest, cli_data->active_dev,
		      buffer_nr, frame_nr, round_count, acq_frame_nr, timeout,
		      cli_acq_cb_funct, (void *) context );
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_status_cb_funct( etest_t etest, int dev_nr, 
                         struct espia_cb_data *cb_data, 
                         void *user_data )
{
	unsigned char ccd_status = cb_data->info.status.ccd_status;
	app_context_t context = (app_context_t) user_data;

	context_out(context, "\n\nStatus callback #%d for dev #%d:\n", 
		    cb_data->cb_nr, dev_nr);
	context_out(context, "CCD status: %d (0x%02x)\n", 
		    ccd_status, ccd_status);

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_reg_status_cb(app_context_t context, union param *pars)
{
	int ret;
	long timeout = pars[0].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		ret = lib_reg_status_cb( cli_data->etest, cli_data->active_dev,
		                         timeout, cli_status_cb_funct, 
		                         (void *) context );
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_ser_rx_cb_funct( etest_t etest, int dev_nr, 
                         struct espia_cb_data *cb_data, 
                         void *user_data )
{
	struct espia_cb_serial *cb_serial = &cb_data->info.serial;
	struct cb_priv_data *priv_data = priv_data_from_cb_data(cb_data);
	app_context_t context = (app_context_t) user_data;

	context_out(context, "\n\nSer. Rx callback #%d for dev #%d:\n", 
		    cb_data->cb_nr, dev_nr);
	print_serial_str(context, cb_serial->buffer, 
			 cb_serial->nr_bytes, priv_data->print_raw);

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_reg_ser_rx_cb(app_context_t context, union param *pars)
{
	int ret;
	unsigned long buffer_len  = pars[0].i;
	char *term                = pars[1].s;
	unsigned long term_len    = pars[1].len;
	long timeout              = pars[2].i;
	int print_raw             = pars[3].i;
	int filter_tout           = pars[4].i;
	struct cli_data *cli_data = cli_data_from_context(context);

	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		pars[1].s[pars[1].len] = '\0';
		ret = lib_reg_ser_rx_cb( cli_data->etest, cli_data->active_dev,
		                buffer_len, term, term_len, timeout, print_raw, 
		                filter_tout, cli_ser_rx_cb_funct, 
		                (void *) context );
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_unreg_cb(app_context_t context, union param *pars)
{
	int ret;
	long cb_nr = pars[0].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		ret = lib_unreg_cb( cli_data->etest, cli_data->active_dev,
		                    cb_nr );
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_unreg_all_cb(app_context_t context, union param *pars)
{
	int ret;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else
		ret = lib_unreg_all_cb( cli_data->etest, cli_data->active_dev );

	if (ret >= 0) {
		context_out(context, "Unregistered all cb and autosave for"
		            " dev #%d\n", cli_data->active_dev);
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_cb_active(app_context_t context, union param *pars)
{
	int ret, cb_nr=pars[0].i, active=pars[1].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else
		ret = lib_cb_active( cli_data->etest, cli_data->active_dev,
		                     cb_nr, active );

	if (ret >= 0) {
		context_out(context, "Callback #%d prevously %sactive\n",
			    cb_nr, !ret ? "not " : "");
		ret = ESPIA_OK;
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int list_acq_cb(app_context_t context)
{
	int ret, cb_nr, active, num_cb, user_cb;
	struct espia_cb_data cb_data;
	struct img_frame_info *finfo;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;

	ret = lib_get_num_cb( cli_data->etest, cli_data->active_dev, &num_cb );
	if (ret < 0)
		return ret;

	context_out(context, "Cb #    Buffer    Frame Nr    Count    "
		    "Acq. Frame     Timeout    Active\n");
	context_out(context, "---------------------------------------"
		    "--------------------------------\n");
	for (cb_nr = 0; cb_nr < num_cb; cb_nr++) {

		ret = lib_is_user_cb(cli_data->etest, cli_data->active_dev, 
		                     cb_nr, &user_cb);
		if (ret < 0)
			return ret;
		else if( !user_cb )
			continue;

		ret = lib_espia_get_callback(cli_data->etest, 
		                             cli_data->active_dev, 
		                             cb_nr, &cb_data);
		if (ret < 0)
			return ret;
		else if (cb_data.type != ESPIA_CB_ACQ)
			continue;

		active = lib_cb_active(cli_data->etest, cli_data->active_dev,
		                       cb_nr, -1);

		finfo = &cb_data.info.acq.req_finfo;
		context_out(context, "%3d     %4ld      %4ld       %4ld"
			    "        %4ld      %10ld   %4d\n", cb_nr, 
			    finfo->buffer_nr, finfo->frame_nr, 
			    finfo->round_count, finfo->acq_frame_nr, 
			    cb_data.timeout, active);
	}
	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int list_status_cb(app_context_t context)
{
	struct espia_cb_data cb_data;
	int ret, cb_nr, active, num_cb, user_cb;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;

	ret = lib_get_num_cb( cli_data->etest, cli_data->active_dev, &num_cb );
	if (ret < 0)
		return ret;

	context_out(context, "Cb #      Timeout    Active\n");
	context_out(context, "---------------------------\n");
	for (cb_nr = 0; cb_nr < num_cb; cb_nr++) {

		ret = lib_is_user_cb(cli_data->etest, cli_data->active_dev, 
		                     cb_nr, &user_cb);
		if (ret < 0)
			return ret;
		else if( !user_cb )
			continue;

		ret = lib_espia_get_callback(cli_data->etest, 
		                             cli_data->active_dev, 
		                             cb_nr, &cb_data);
		if (ret < 0)
			return ret;
		else if (cb_data.type != ESPIA_CB_CCD_STATUS)
			continue;

		active = lib_cb_active(cli_data->etest, cli_data->active_dev,
		                       cb_nr, -1);

		context_out(context, "%3d    %10ld   %4d\n", cb_nr, 
			    cb_data.timeout, active);
	}
	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int list_ser_tx_cb(app_context_t context)
{
	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int list_ser_rx_cb(app_context_t context)
{
	int ret, cb_nr, active, user_cb, num_cb;
	char term[80], *src, *dest = term;
	struct espia_cb_data cb_data;
	struct espia_cb_serial *cb_serial;
	struct cb_priv_data *priv_data;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;

	ret = lib_get_num_cb( cli_data->etest, cli_data->active_dev, &num_cb );
	if (ret < 0)
		return ret;

	context_out(context, "Cb #    Buff. Len    Term      Timeout    "
		    "Prn. Raw   Filter Tout.   Active\n");
	context_out(context, "------------------------------------------"
		    "--------------------------------\n");
	for (cb_nr = 0; cb_nr < num_cb; cb_nr++) {

		ret = lib_is_user_cb(cli_data->etest, cli_data->active_dev, 
		                     cb_nr, &user_cb);
		if (ret < 0)
			return ret;
		else if( !user_cb )
			continue;

		ret = lib_espia_get_callback(cli_data->etest, 
		                             cli_data->active_dev, 
		                             cb_nr, &cb_data);
		if (ret < 0)
			return ret;
		else if (cb_data.type != ESPIA_CB_SERIAL_RX)
			continue;

		active = lib_cb_active(cli_data->etest, cli_data->active_dev,
		                       cb_nr, -1);

		cb_serial = &cb_data.info.serial;
		priv_data = (struct cb_priv_data *) cb_data.data; /* ??? */
		src = cb_serial->term;
		*dest++ = '"';
		while (src && *src)
			dest += sprintf(dest, "%s", get_char_repr(*src++));
		strcpy(dest, "\"");
		context_out(context, "%3d      %6ld      %-6s %10ld       "
			    "%2d          %2d        %4d\n", cb_nr, 
			    cb_serial->buffer_len, term, cb_data.timeout, 
			    priv_data->print_raw, priv_data->filter_tout, 
			    active);
	}
	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_cb_list(app_context_t context, union param *pars)
{
	struct {
		int type;
		char *str;
		int (*func)(app_context_t context);
	} *fptr, func_arr[] = {
		{ESPIA_CB_ACQ,        "Acq",      list_acq_cb},
		{ESPIA_CB_CCD_STATUS, "Status",   list_status_cb},
		{ESPIA_CB_SERIAL_TX,  "SerialTx", list_ser_tx_cb},
		{ESPIA_CB_SERIAL_RX,  "SerialRx", list_ser_rx_cb},
	};
	int ntypes = sizeof(func_arr) / sizeof(func_arr[0]);
	int i, cb_nr, ret, num_cb, user_cb;
	struct espia_cb_data cb_data;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;

	ret = lib_get_num_cb( cli_data->etest, cli_data->active_dev, &num_cb );
	if (ret < 0)
		return ret;

	if (num_cb == 0) {
		context_out(context, "No registered callback\n");
		return ESPIA_OK;
	}

	for (i = 0, fptr = func_arr; i < ntypes; i++, fptr++) {
		pars[0].s[pars[0].len] = '\0';
		if (strcasecmp(pars[0].s, fptr->str) != 0)
			continue;

		for (cb_nr = 0; cb_nr < num_cb; cb_nr++) {

			ret = lib_is_user_cb(cli_data->etest, 
			                     cli_data->active_dev, cb_nr, 
			                     &user_cb);
			if (ret < 0)
				return ret;
			else if( !user_cb )
				continue;

			ret = lib_espia_get_callback( cli_data->etest, 
			                              cli_data->active_dev, 
			                              cb_nr, &cb_data );
			if (ret < 0)
				return ret;
			else if (cb_data.type == fptr->type)
				return fptr->func(context);
		}

		context_out(context, "No %s callback registered\n", 
			    fptr->str);
		return ESPIA_OK;
	}
	return ESPIA_TEST_ERR_CBTYPE;
}


/*--------------------------------------------------------------------------
 * get frame / frame addr
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_get_frame(app_context_t context, union param *pars)
{
	int ret;
	long timeout;
	struct img_frame_info req_finfo, cb_finfo;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		req_finfo.buffer_nr	= pars[0].i;
		req_finfo.frame_nr	= pars[1].i;
		req_finfo.round_count	= pars[2].i;
		req_finfo.acq_frame_nr	= pars[3].i;
		timeout			= pars[4].i;

		cb_finfo = req_finfo;

		ret = lib_get_frame( cli_data->etest, cli_data->active_dev,
		                     &cb_finfo, timeout );
	}

	if (ret >= 0) {
		print_finfo(context, &req_finfo, &cb_finfo, ret);
		ret = ESPIA_OK;
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_frame_addr(app_context_t context, union param *pars)
{
	int ret, data_len;
	unsigned char *addr;
	long buffer_nr=pars[0].i, frame_nr=pars[1].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;

	else {
		ret = lib_get_data( cli_data->etest, cli_data->active_dev,
		                    buffer_nr, frame_nr, &addr, &data_len );
	}

	if (ret >= 0) {
		context_out(context, "Frame address: %p\n", (void *) addr);
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * CCD status
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_ccd_status(app_context_t context, union param *pars)
{
	int ret;
	long timeout=pars[0].i;
	unsigned char ccd_status;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else
		ret = lib_ccd_status(cli_data->etest, cli_data->active_dev,
		                     &ccd_status, timeout);

	if (ret == SCDXIPCI_OK) {
		context_out(context, "CCD status: %d (0x%02x)\n", 
			    ccd_status, ccd_status);
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * RoI accumulation buffer
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_roi_acc_buffer(app_context_t context, union param *pars)
{
	int ret;
	int col_beg = pars[0].i;
	int col_end = pars[1].i;
	int row_beg = pars[2].i;
	int row_end = pars[3].i;
	int nr_frames = pars[4].i;
	char *file_prefix = get_param_str(&pars[5]);
	int file_idx = pars[6].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		ret = lib_roi_acc_buffer( cli_data->etest, cli_data->active_dev,
		                          col_beg, col_end, row_beg, row_end, 
		                          nr_frames, file_prefix, file_idx );
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * FOCLA open / close
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_focla_open_dev(app_context_t context, union param *pars)
{
	int ret;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else
		ret = lib_focla_open_dev(cli_data->etest, cli_data->active_dev);

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_focla_close_dev(app_context_t context, union param *pars)
{
	int ret;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else
		ret = lib_focla_close_dev(cli_data->etest, cli_data->active_dev);

	return ret;
}


/*--------------------------------------------------------------------------
 * FOCLA reg read / write
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_focla_read(app_context_t context, union param *pars)
{
	unsigned char val;
	int ret, reg=pars[0].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else
		ret = lib_focla_read(cli_data->etest, cli_data->active_dev,
		                     reg, &val);

	if (ret == FOCLA_OK) {
		context_out(context, "FOCLA reg %d: 0x%02x (%d)\n", reg,
			    val, val);
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_focla_write(app_context_t context, union param *pars)
{
	int ret, reg=pars[0].i;
	unsigned char old_val, new_val=pars[1].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		ret = lib_focla_read(cli_data->etest, cli_data->active_dev,
		                     reg, &old_val);
		if( FOCLA_OK == ret ) {
			context_out(context, "Previous value of FOCLA reg %d:"
			 " 0x%02x (%d)\n", reg, old_val, old_val);

			ret = lib_focla_write(cli_data->etest, 
			                      cli_data->active_dev,
					      reg, new_val);
		}
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * FOCLA param list 
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_focla_param_list(app_context_t context, union param *pars)
{
	struct espia_param *fparam;
	int i, nr_param, ret;
	
	nr_param = -1;
	ret = focla_get_param_data(&nr_param, NULL);
	if (ret < 0)
		return ret;

	context_out(context, "   Name         Reg    Mask   Shift    "
		    "Description      \n");
	context_out(context, "---------------------------------------"
		    "-----------------\n");

	for (i = 0; i < nr_param; i++) {
		ret = focla_get_param_data(&i, &fparam);
		if (ret < 0)
			return ret;
		context_out(context, "  %-12s   %d     0x%02x     %d      "
			    "%s\n", fparam->name, fparam->reg, fparam->mask, 
			    fparam->shift, fparam->desc);
	}

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * FOCLA param get / set
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_focla_get_par(app_context_t context, union param *pars)
{
	struct espia_param *fparam;
	int ret, i, val, nr_param, first, end;
	char *name = get_param_str(&pars[0]);
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;

	nr_param = -1;
	ret = focla_get_param_data(&nr_param, NULL);
	if (ret < 0)
		return ret;

	if (strcasecmp(name, "ALL") == 0) {
		first = 0;
		end = nr_param;
	} else {
		for (first = 0; first < nr_param; first++) {
			ret = focla_get_param_data(&first, &fparam);
			if (ret < 0)
				return ret;
			if (strcasecmp(fparam->name, name) == 0)
				break;
		}
		if (first == nr_param)
			return ESPIA_TEST_ERR_PARAM;
		end = first + 1;
	}

	context_out(context, " Name                                       "
		    "Value   \n");
	context_out(context, "--------------------------------------------"
		    "--------\n");

	for (i = first; i < end; i++) {
		ret = lib_focla_get_param(cli_data->etest, cli_data->active_dev,
		                          i, &val);
		if (ret < 0)
			return ret;
		ret = focla_get_param_data(&i, &fparam);
		if (ret < 0)
			return ret;
		context_out(context, " %-40s  0x%02x (%d)\n", fparam->name,
			    val, val);
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_focla_set_par(app_context_t context, union param *pars)
{
	int ret, old_val, new_val=pars[1].i;
	char *name = get_param_str(&pars[0]);
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else
		ret = lib_focla_set_par( cli_data->etest, cli_data->active_dev,
		                         name, new_val, &old_val );

	if (ret >= 0) {
		context_out(context, "Previous FOCLA %s value: 0x%02x (%d)\n", 
		            name, old_val, old_val);
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * FOCLA signal list
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_focla_sig_list(app_context_t context, union param *pars)
{
	struct focla_signal *fsig;
	int i, nr_sig, ret;
	
	nr_sig = -1;
	ret = focla_get_sig_data(&nr_sig, NULL);
	if (ret < 0)
		return ret;

	context_out(context, "  Name           Description          \n");
	context_out(context, "--------------------------------------\n");

	for (i = 0; i < nr_sig; i++) {
		ret = focla_get_sig_data(&i, &fsig);
		if (ret < 0)
			return ret;
		context_out(context, "  %-12s   %s\n", fsig->name, fsig->desc);
	}

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * FOCLA signal pulse start/stop/status
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_focla_pulse_start(app_context_t context, union param *pars)
{
	char *sig_name = get_param_str(&pars[1]);
	int ret, cam_nr=pars[0].i, polarity=pars[2].i;
	int width_us=pars[3].i, delay_us=pars[4].i, nr_pulse=pars[5].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else
		ret = lib_focla_pulse_start( cli_data->etest, 
		                             cli_data->active_dev,
		                             cam_nr, sig_name, polarity,
		                             width_us, delay_us, nr_pulse );

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_focla_pulse_stop(app_context_t context, union param *pars)
{
	int ret, cam_nr=pars[0].i;
	char *sig_name = get_param_str(&pars[1]);
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else
		ret = lib_focla_pulse_stop( cli_data->etest, 
		                            cli_data->active_dev,
		                            cam_nr, sig_name );

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_focla_pulse_status(app_context_t context, union param *pars)
{
	char *sig_name;
	int ret, cam_nr, active, curr_pulse, curr_stage;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		cam_nr=pars[0].i;
		sig_name = get_param_str(&pars[1]);

		ret = lib_focla_pulse_status( cli_data->etest, 
		                              cli_data->active_dev,
		                              cam_nr, sig_name, &active,
		                              &curr_pulse, &curr_stage );
	}

	if (ret >= 0) {
		context_out(context, "FOCLA %d %s pulse status:\n", 
		            cam_nr + 1, sig_name);
		context_out(context, "  active=%d, curr_pulse=%d, curr_stage=%d\n", 
		            active, curr_pulse, curr_stage);
		ret = FOCLA_OK;
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * FOCLA serial read/write/flush
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_focla_serial_read(app_context_t context, union param *pars)
{
	char *buffer;
	int ret, raw=pars[2].i;
	long timeout=pars[1].i;
	unsigned long nr_bytes=pars[0].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {

		ret = alloc_serial_buffer(context, nr_bytes, &buffer);
		if (ret < 0)
			return ret;

		ret = lib_focla_serial_read( cli_data->etest, 
		                             cli_data->active_dev,
		                             buffer, &nr_bytes, timeout );

		print_free_serial_buffer( context, buffer, nr_bytes, raw, ret );
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_focla_serial_read_str(app_context_t context, union param *pars)
{
	char *buffer;
	char *term=pars[1].s;
	int ret, raw=pars[3].i;
	long timeout=pars[2].i;
	unsigned long nr_bytes=pars[0].i, term_bytes=pars[1].len;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {

		ret = alloc_serial_buffer(context, nr_bytes, &buffer);
		if (ret < 0)
			return ret;

		ret = lib_focla_serial_read_str( cli_data->etest, 
		                                 cli_data->active_dev,
		                                 buffer, &nr_bytes, term, 
		                                 term_bytes, timeout );

		print_free_serial_buffer( context, buffer, nr_bytes, raw, ret );
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_focla_serial_write(app_context_t context, union param *pars)
{
	int ret, block=pars[3].i;
	char *buffer=pars[0].s, *action;
	unsigned long nr_bytes=pars[0].len, block_size=pars[1].i, delay=pars[2].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		ret = lib_focla_serial_write( cli_data->etest, 
		                              cli_data->active_dev,
		                              buffer, &nr_bytes, block_size, 
		                              delay, block );
	}

	if( ret == ESPIA_OK ) {
		action = (pars[0].len > 0) ? "Transmited" : "Still to TX:";
		context_out(context, "%s %ld bytes.\n", action, nr_bytes);
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_focla_serial_flush(app_context_t context, union param *pars)
{
	int ret;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		ret = lib_focla_serial_flush( cli_data->etest, 
		                              cli_data->active_dev );
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * Poll sleep time
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_poll_sleep(app_context_t context, union param *pars)
{
	int ret;

	ret = set_poll_sleep_time(context, pars[0].i);
	context_out(context, "Previous polling sleep time: %d usec\n", ret);

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * get stats
 *--------------------------------------------------------------------------*/

#define STAT_DESC_LEN		40

#define print_stat(context, desc, format, val)		\
	context_out(context, "%-*s " format "\n", STAT_DESC_LEN, \
		    desc ":", val)

#define print_dev_stat(context, nr, desc, format, val)			\
	context_out(context, "Dev. #%d %-*s " format "\n", nr, STAT_DESC_LEN-8,\
		    desc ":", val)

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_get_stats(app_context_t context, union param *pars)
{
	int ret, i;
	struct scdxipci_stats stats;
	struct scdxipci_dev_stats *dev_stats;
	unsigned long cnt, page_size;
	struct cli_data *cli_data = cli_data_from_context(context);


	/* ??? Always working with the active device only ???
	   Yes. This comes from dev_data_from_context() */
	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		ret = lib_get_stats( cli_data->etest, cli_data->active_dev,
		                     &stats );
	}

	if (ret < 0) {
		return ret;
	}

	context_out(context, "Acquisition statistics:\n\n");

	// SG data info
	dev_stats = stats.dev_stats;
	for (i = 0; i < SCDXIPCI_NR_DEV_STATS; i++, dev_stats++) {
		cnt = dev_stats->sg_avail_count;
		if (cnt == 0)
			continue;

		print_dev_stat(context, i, "total of SG table switches",  
			       "%ld", cnt);
		print_dev_stat(context, i, "min. nr. of avail. SG tables", 
			       "%ld", dev_stats->sg_avail_min);
		print_dev_stat(context, i, "ave. nr. of avail. SG tables", 
			       "%.1f", (double) dev_stats->sg_avail_acc / cnt);
	}

	// copy frame info
	cnt = stats.copy_count;
	print_stat(context, "Nr. of buffer pages", "%ld", 
		   stats.nr_pages);
	print_stat(context, "Nr. of buffer pages 64-bit", "%ld", 
		   stats.nr_pages64);
	print_stat(context, "Total of frame copies", "%ld", cnt);
	if (cnt > 0) {
		print_stat(context, "Average copy nr. of pages", "%lld", 
			   stats.copy_pages / cnt);
		print_stat(context, "Average copy time", "%lld usec",
			   stats.copy_usec / cnt);
	}
	page_size = getpagesize();
	if (stats.copy_usec > 0) 
		print_stat(context, "Average copy speed", "%lld MB/s",
			   stats.copy_pages * page_size / stats.copy_usec);
	
	// get acq_frame_nr info
	cnt = stats.afn_call;
	print_stat(context, "Total of AFN calls", "%ld", cnt);
	if (cnt > 0) {
		cnt = stats.afn_count;
		print_stat(context, "Total of AFN searches", "%ld", cnt);
	}
	if (cnt > 0) {
		print_stat(context, "Average AFN time", "%.2f usec",
			   (double) stats.afn_usec / cnt);
		print_stat(context, "Average AFN iters", "%.2f", 
			   (double) stats.afn_iter / cnt);
		print_stat(context, "Max AFN iters", "%ld", 
			   stats.afn_max_iter);
		print_stat(context, "Average AFN steps", "%.2f", 
			   (double) stats.afn_step / cnt);
		print_stat(context, "Max AFN steps", "%ld", 
			   stats.afn_max_step);
	}
	
	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * firmware list
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_firmware_list(app_context_t context, union param *pars)
{
	int nr_firmware, i, ret;
	struct espia_firmware *firmware;

	nr_firmware = -1;
	ret = espia_get_firmware_data(&nr_firmware, NULL);
	if (ret < 0)
		return ret;

	context_out(context, " Firmware      Checksum\n");
	context_out(context, "-----------------------\n");
	
	for (i = 0; i < nr_firmware; i++) {
		ret = espia_get_firmware_data(&i, &firmware);
		if (ret < 0)
			return ret;

		context_out(context, " %-13s  0x%04x\n",
			    firmware->name, firmware->checksum);
	}

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * library versions
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_get_version(app_context_t context, union param *pars)
{
	struct espia_lib_version ver;
	int ret;

	ret = lib_get_version(&ver);
	if (ret < 0)
		return ret;

	context_out(context, "Compiled version: %s\n", ver.compile);
	context_out(context, "Running  version: %s\n", ver.run);

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------*
 * AcqLib plugin interface
 *--------------------------------------------------------------------------*/

#define PLUGIN_LIB_NAME "libacqplugin.so"  /* Or make it an argument? */


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_plugin_reg(app_context_t context, union param *pars)
{
	char *err;
	int ret, type;
	char *fplug_name;
	void *lib_handle;
	void *fplug;
	struct cli_data *cli_data = cli_data_from_context(context);


	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		type = pars[0].i;
		fplug_name = get_param_str(&pars[1]);

		if( ! (lib_handle=cli_data->lib_handle) ) {
			lib_handle = dlopen(PLUGIN_LIB_NAME, RTLD_LAZY);
			if( NULL == lib_handle ) {
				context_err( context, "Failed to dlopen %s : %s\n", 
					     PLUGIN_LIB_NAME, dlerror() );
				return -1;  /* ??? */
			}
			cli_data->lib_handle = lib_handle;
		}

		fplug = dlsym( lib_handle, fplug_name );
		err = dlerror();
		if( err != NULL ) {
			context_err( context, "Failed to load %s function from "
			         "%s : %s\n", fplug_name, PLUGIN_LIB_NAME, err);
			return -1;  /* ??? */
		}

		if( (type<1) || (2<type) ) {
			context_err(context, "Error: Plugin type must "
			                     "be either 1 or 2!\n");
			return -1;  /* ??? */
		} else {
			ret = lib_reg_plugin( cli_data->etest, 
			                      cli_data->active_dev,
			                      type, fplug, (void *)((long)type),
			                      fplug_name );
		}
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_plugin_list( app_context_t context, union param *pars )
{
	int i, ret;
	struct plugin_t *plugin;
	struct lib_plugins_data plugins_data;
	struct cli_data *cli_data = cli_data_from_context(context);


	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		ret = lib_get_plugins_data( cli_data->etest, 
		                            cli_data->active_dev, 
		                            &plugins_data );
		if( ret < 0 )
			return -1;  /* ??? */

		context_out( context, "Type 1 plugins:\n" );
		context_out( context, "-------------------------------------------------------------------------------\n" );
		context_out( context, "number  function            active              userData            description\n" );
		context_out( context, "-------------------------------------------------------------------------------\n" );
		for( i=0; i<plugins_data.num_plugins_I; i++ )
		{
			context_out( context, "%-8d", i+1 );
			plugin = &plugins_data.plugin_list_I[i];
			context_out( context, "%-20p", plugin->fun );
			context_out( context, "%-20d", plugin->active );
			context_out( context, "%-20p", plugin->userData );
			if( plugin->desc )
				context_out( context, "%-20s\n", plugin->desc );
		}


		context_out( context, "\nType 2 plugins:\n" );
		context_out( context, "-------------------------------------------------------------------------------\n" );
		context_out( context, "number  function            active              userData            description\n" );
		context_out( context, "-------------------------------------------------------------------------------\n" );
		for( i=0; i<plugins_data.num_plugins_II; i++ )
		{
			context_out( context, "%-8d", i+1 );
			plugin = &plugins_data.plugin_list_II[i];
			context_out( context, "%-20p", plugin->fun );
			context_out( context, "%-20d", plugin->active );
			context_out( context, "%-20p", plugin->userData );
			if( plugin->desc )
				context_out( context, "%-20s\n", plugin->desc );
		}
	}

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_plugin_active(app_context_t context, union param *pars)
{
	int ret, type=pars[0].i, plugin_nr=pars[1].i, active=pars[2].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	if( cli_data->active_dev < 0 )
		return ESPIA_TEST_ERR_SELECT;

	if( (active < 0) || (1 < active) ) {
		context_err(context, "Error! Active parameter must be 0 or 1.\n");
		return ESPIA_TEST_ERR_BADPAR;
	}

	ret = lib_plugin_active( cli_data->etest, cli_data->active_dev,
	                         type, plugin_nr, active );

	if (ret >= 0) {
		context_out( context, "Plugin type %d, #%d prevously "
		             "%sactive\n", type, plugin_nr, !ret ? 
		             "not " : "" );
		ret = ESPIA_OK;
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_plugin_delete(app_context_t context, union param *pars)
{
	int ret, type=pars[0].i, plugin_nr=pars[1].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	if( cli_data->active_dev < 0 )
		return ESPIA_TEST_ERR_SELECT;

	ret = lib_plugin_delete( cli_data->etest, cli_data->active_dev,
	                         type, plugin_nr );

	if (ret >= 0) {
		context_out( context, "Plugin type %d, #%d deleted!",
		             type, plugin_nr );
		ret = ESPIA_OK;
	}

	return ret;
}


int cli_plugin_output(app_context_t context, union param *pars)
{
	int ret, type=pars[0].i, plugin_nr=pars[1].i, 
	    width=pars[2].i, height=pars[3].i, depth=pars[4].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	if( cli_data->active_dev < 0 )
		return ESPIA_TEST_ERR_SELECT;

	ret = lib_plugin_output( cli_data->etest, cli_data->active_dev,
	                         type, plugin_nr, width, height, depth );

	return ret;
}

/*--------------------------------------------------------------------------*
 * Custom EDF header functions
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int cli_edf_usr_header(app_context_t context, union param *pars)
{
	int ret;
	char *usr_str;
	struct cli_data *cli_data = cli_data_from_context(context);


	if( cli_data->active_dev < 0 )
		ret = ESPIA_TEST_ERR_SELECT;
	else {
		usr_str = get_param_str(&pars[0]);

		ret = lib_edf_usr_header( cli_data->etest, cli_data->active_dev,
		                          usr_str );
	}

	return ret;
}


/*--------------------------------------------------------------------------*
 * Parallel saving functions
 *--------------------------------------------------------------------------*/

int cli_nonblock_frame_save(app_context_t context, union param *pars)
{
	int ret, acq_fr_nr=pars[0].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	if( cli_data->active_dev < 0 )
		return ESPIA_TEST_ERR_SELECT;

	ret = lib_nonblock_frame_save( cli_data->etest, cli_data->active_dev,
	                                acq_fr_nr );

	return ret;
}


int cli_nonblock_save_mode(app_context_t context, union param *pars)
{
	int ret, mode=pars[0].i;
	struct cli_data *cli_data = cli_data_from_context(context);


	if( cli_data->active_dev < 0 )
		return ESPIA_TEST_ERR_SELECT;

	ret = lib_nonblock_save_mode( cli_data->etest, cli_data->active_dev,
	                              mode );

	return ret;
}


int cli_nonblock_save_stop(app_context_t context, union param *pars)
{
	int ret;
	struct cli_data *cli_data = cli_data_from_context(context);


	if( cli_data->active_dev < 0 )
		return ESPIA_TEST_ERR_SELECT;

	ret = lib_nonblock_save_cleanup( cli_data->etest, cli_data->active_dev );

	return ret;
}

