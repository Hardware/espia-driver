#ifndef ESPIA_TEST_LIB_PRIV_H
#define ESPIA_TEST_LIB_PRIV_H



#define FILE_SAVE_MODE	(S_IRUSR | S_IRGRP | S_IROTH | \
			 S_IWUSR | S_IWGRP | S_IWOTH)

#define ESPIA_TEST_SERIAL_FILENAME "/tmp/espia%d_serial_%s"


enum {
	FMT_RAW,
	FMT_EDF,
};


struct device_data;
struct lib_data;


typedef struct espia_frm_op_fn_data op_fn_data;

struct image_roi {
	int			 col_beg;
	int			 col_end;
	int			 row_beg;
	int			 row_end;
};

struct last_frame_info {
	struct img_frame_info	 finfo;
	pthread_mutex_t		 lock;
};

struct buffer_save_data {
	struct device_data	*dev_data;
	char			 prefix[FILE_PREFIX_NAME_LEN];
	int			 idx;
	char			 suffix[FILE_SUFFIX_NAME_LEN];
	int			 format;
	char			 file_name[FILE_FULL_NAME_LEN];
	int			 file_frames;
	int			 tot_file_frames;
	int			 overwrite;
	int			 print_name;
	FILE			*fout;
	struct last_frame_info	 last_frame;
};

#ifdef WITH_IMAGE
struct image_data {
	image_t			 img;
	struct device_data	*dev_data;
	int			 cb_nr;
	void			*buffer_ptr;
	img_cb			*cb_funct;
	img_del			*del_funct;
};
#endif // WITH_IMAGE

struct buffer_data {
#ifdef WITH_IMAGE
	struct image_data	 img;
#endif // WITH_IMAGE
};

struct roi_acc_buffer {
	void			*buffer_ptr;
#ifdef WITH_IMAGE
	struct image_data	 img_data;
#endif // WITH_IMAGE
	struct image_roi	 roi;
	int			 nr_frames;
	int			 real_nr_frames;
	struct espia_frame_dim	 frame_dim;
	char			 file_prefix[FILE_PREFIX_NAME_LEN];
	int			 file_idx;
};

struct serial_mmap_data {
	short			 init;
	int 			 max_bytes;
	int			 mem_size;
	int			 write_file;
	int			 read_file;
	void			*write_mmap;
	void			*read_mmap;
};


struct job_data_t {
	struct buffer_save_data  bsd;  /* bsd or *bsd ? Use dev_data->bsd ? */
	int 			 acq_fr_nr;
	char 			*usr_string; /* Copy of the EDF user string */
	int 			 worker;
};


struct thread_info_t {
	pthread_t 		 thread_id;
	pthread_mutex_t 	 cnd_mutex;  /* Condition var mutex */
	pthread_cond_t 		 condition;  /* To wait for the predecessor ? */
	struct job_data_t 	 job_data;
	int 			 previous;
/* 	int 			 saving_state; // ???
	char 			*file_name;  // Do we need this if we have bsd?
	int 			 fd;  // Do we need this if we have bsd?
	struct img_frame_info 	*finfo;  // We must get this just before saving
	pthread_mutex_t 	 lock;  // ??? */
};

#define MAX_NUM_PARALLEL_THREADS 10

struct thread_pool_t {
	struct thread_info_t 	*threads;  /* A dynamic array */
	int 			 num_threads;
	struct job_data_t 	 job_data;
	sem_t 			 job_sem;
	int 			 last;  /* The last thread who took a job */
	int 			 mode;  /* serial(1)/parallel(0) flag */
	pthread_mutex_t 	 lock;
};


struct device_data {
	int			 dev_nr;
	espia_t			 dev;
	focla_t			 focla;
	struct buffer_data	*buff_data;
	int			 nr_buffer;
	int			 buffer_frames;
	int			 real_frame_factor;
	int			 real_frame_size;
	struct espia_frame_dim	 alloc_frame_dim;
	struct espia_frame_dim	 plugin_frame_dim;
	op_fn_data		 post_op_fn_data;
	struct cb_priv_data    **cbs;
	int			 nr_cb;
	struct last_frame_info	 last_frame;
	int			 last_frame_cb;
	struct buffer_save_data  buff_save_data;
	int			 buff_save_cb;
	pthread_mutex_t		 buff_save_lock;
	struct roi_acc_buffer	 roi_acc_buff;
	int			 roi_acc_buff_cb;
#ifdef WITH_IMAGE
	struct image_data	 live_img;
#endif // WITH_IMAGE
	struct serial_mmap_data	 serial_mmap;
	struct lib_data         *lib_data;
	char                    *edf_usr_str;
	pthread_mutex_t		 edf_str_lock;
	struct lib_plugins_data *plugins_data;
	struct thread_pool_t    *thread_pool;
};

typedef struct device_data * etest_dev_t;


struct lib_data {
	struct device_data	 device[ESPIA_TEST_MAX_NR_DEV];
	int                      saved_argc;
	char                   **saved_argv;
#ifdef WITH_IMAGE
	int                      image_init_flag;
#endif // WITH_IMAGE
};


/*--------------------------------------------------------------------------
 * macro and inline functions
 *--------------------------------------------------------------------------*/

#define get_check_lib_data(plib, etest)			\
	plib = (struct lib_data *) etest;		\
	if (plib == NULL)				\
		return ESPIA_TEST_ERR_NOTINIT
	        

#define get_check_dev_data( dev_data, etest, dev_nr )      \
{                                                          \
	struct lib_data *plib;				   \
	get_check_lib_data(plib, etest);		   \
                                                           \
	if (!is_good_dev_nr(dev_nr))                       \
		return ESPIA_TEST_ERR_DEVNR;               \
                                                           \
	dev_data = &plib->device[dev_nr];                  \
	if ( dev_data->dev == ESPIA_DEV_INVAL )            \
		return ESPIA_TEST_ERR_NOOPEN;              \
}


static inline
void img_roi_reset(struct image_roi *roi)
{
	roi->col_beg = roi->col_end = roi->row_beg = roi->row_end = -1;
}

static inline
int img_roi_width(struct image_roi *roi)
{
	return roi->col_end - roi->col_beg + 1;
}

static inline
int img_roi_height(struct image_roi *roi)
{
	return roi->row_end - roi->row_beg + 1;
}

static inline
int img_roi_valid(struct image_roi *roi, struct espia_frame_dim *frame_dim)
{
	if ((roi->col_beg < 0) || (img_roi_width(roi)  <= 0) ||
	    (roi->row_beg < 0) || (img_roi_height(roi) <= 0))
		return 0;

	if (frame_dim == NULL)
		return 1;

	return ((roi->col_end < (int) frame_dim->width) && 
		(roi->row_end < (int) frame_dim->height));
}

static inline
int img_roi_size(struct image_roi *roi)
{
	return img_roi_width(roi) * img_roi_height(roi);
}

#define get_cb_nr(dev_data, cb_nr) \
	((((cb_nr) >= 0) && ((cb_nr) < (dev_data)->nr_cb)) ?	\
	 (dev_data)->cbs[cb_nr] : NULL)

static inline
int is_user_cb(struct device_data *dev_data, int cb_nr)
{
	struct cb_priv_data *priv_data = get_cb_nr(dev_data, cb_nr);
	return priv_data && priv_data->user_cb;
}

#define buffer_is_live(buffer_nr) \
	scdxipci_is(ANY, buffer_nr)
#define buffer_is_roi_acc(buffer_nr) \
	((buffer_nr) == ROI_ACC_BUFFER)

static inline 
void init_cb_priv_data(struct cb_priv_data *priv_data)
{
	memset(priv_data, 0, sizeof(*priv_data));
}

#ifdef WITH_IMAGE

static inline
struct image_data *image_data_from_buffer_nr(struct device_data *dev_data, 
					     int buffer_nr)
{
	if (buffer_is_roi_acc(buffer_nr))
		return &dev_data->roi_acc_buff.img_data;
	else if (buffer_is_live(buffer_nr))
		return &dev_data->live_img;
	else if ((buffer_nr >= 0) && (buffer_nr < dev_data->nr_buffer))
		return &dev_data->buff_data[buffer_nr].img;
	else
		return NULL;
}

#define get_image_data_check(img_data, dev_data, buffer_nr) \
{\
	img_data = image_data_from_buffer_nr(dev_data, buffer_nr); \
	if (img_data == NULL) \
		return ESPIA_TEST_ERR_BUFFNUM; \
}

static inline
void image_data_init(struct image_data *img_data)
{
	img_data->img = IMAGE_INVALID;
	img_data->cb_nr = (int) SCDXIPCI_INVALID;
	img_data->buffer_ptr = NULL;
}

static inline
int image_active(struct image_data *img_data)
{
	return img_data && (img_data->img != IMAGE_INVALID);
}

#endif // WITH_IMAGE

static inline
int has_virtual_buffers(struct device_data *dev_data)
{
	return (dev_data->real_frame_factor != 1);
}

static inline
int real_buffer_nr(struct device_data *dev_data, int virt_buffer,
		   int virt_frame)
{
	return virt_buffer / dev_data->real_frame_factor;
}

static inline
int real_frame_nr(struct device_data *dev_data, int virt_buffer,
		  int virt_frame)
{
	return virt_buffer % dev_data->real_frame_factor + virt_frame;
}

static inline
int virt_buffer_nr(struct device_data *dev_data, int real_buffer,
		      int real_frame)
{
	return (real_buffer * dev_data->real_frame_factor + 
		real_frame / dev_data->buffer_frames);
}

static inline
int virt_frame_nr(struct device_data *dev_data, int real_buffer,
		     int real_frame)
{
	return real_frame % dev_data->buffer_frames;
}

static inline
void *virt_buffer_ptr(struct device_data *dev_data, void *real_buffer_ptr,
		      int virt_buffer_nr)
{
	char *ptr = (char *) real_buffer_ptr;
	int frame_nr = real_frame_nr(dev_data, virt_buffer_nr, 0);
	return ptr + frame_nr * dev_data->real_frame_size;
}


static inline
int virt_buffer_info(struct device_data *dev_data, int virt_buffer, 
		     struct img_buffer_info *binfo)
{
	int ret, real_buffer;

	real_buffer = real_buffer_nr(dev_data, virt_buffer, 0);
	ret = espia_buffer_info(dev_data->dev, real_buffer, binfo);
	if (ret < 0)
		return ret;

	binfo->ptr = virt_buffer_ptr(dev_data, binfo->ptr, virt_buffer);
	binfo->nr = virt_buffer;
	binfo->nr_frames /= dev_data->real_frame_factor;
	binfo->frame_size = espia_frame_mem_size(&dev_data->alloc_frame_dim);

	return ret;
}

static inline
void virt_frame_info(struct device_data *dev_data, 
		     struct img_frame_info *finfo)
{
	int real_buffer = finfo->buffer_nr;
	int real_frame  = finfo->frame_nr;
	finfo->buffer_nr  = virt_buffer_nr(dev_data, real_buffer, real_frame);
	finfo->frame_nr   = virt_frame_nr(dev_data, real_buffer, real_frame);
	finfo->buffer_ptr = virt_buffer_ptr(dev_data, finfo->buffer_ptr,
					    finfo->buffer_nr);
}

static inline
int virt_frame_address(struct device_data *dev_data, 
		       int virt_buffer, int virt_frame, void **buffer_ptr)
{
	int real_buffer, real_frame;

	if (scdxipci_is(ANY, virt_buffer)) {
		real_buffer = virt_buffer;
		real_frame  = virt_frame;
	} else {
		real_buffer = real_buffer_nr(dev_data, virt_buffer, virt_frame);
		real_frame  = real_frame_nr(dev_data, virt_buffer, virt_frame);
	}
	
	return espia_frame_address(dev_data->dev, real_buffer, real_frame, 
				   buffer_ptr);
}

static inline
int cb_needs_virtual_conv(struct device_data *dev_data,
			  struct espia_cb_data *cb_data)
{
	return ((cb_data->type == ESPIA_CB_ACQ) && 
		has_virtual_buffers(dev_data));
}

static inline
char *buffer_img_name(char *name, struct device_data *dev_data, int buffer_nr)
{
	int bytes = sprintf(name, "Dev. #%d ", dev_data->dev_nr);
	if (buffer_is_roi_acc(buffer_nr))
		strcat(name, "RoI Acc. Buffer");
	else if (buffer_is_live(buffer_nr))
		strcat(name, "Live");
	else
		sprintf(name + bytes, "Buffer #%d", buffer_nr);
	return name;
}

static inline
struct espia_frame_dim *frame_dim_from_buffer_nr(struct device_data *dev_data,
						 int buffer_nr)
{
	struct roi_acc_buffer *roi_acc = &dev_data->roi_acc_buff;
	return buffer_is_roi_acc(buffer_nr) ? &roi_acc->frame_dim : 
					      &dev_data->plugin_frame_dim;
}

static inline
int buffer_info_from_nr(struct device_data *dev_data, int buffer_nr,
			  struct img_buffer_info *binfo)
{
	struct roi_acc_buffer *roi_acc;
	struct espia_frame_dim *fdim;

	if (!buffer_is_roi_acc(buffer_nr))
		return virt_buffer_info(dev_data, buffer_nr, binfo);

	roi_acc = &dev_data->roi_acc_buff;
	fdim = &roi_acc->frame_dim;
	binfo->nr = buffer_nr;
	binfo->nr_frames = 1;
	binfo->frame_size = espia_frame_mem_size(fdim);
	binfo->ptr = roi_acc->buffer_ptr;
	return ESPIA_OK;
}


#define auto_save_active(dev_data) \
	(!scdxipci_is(INVALID, (dev_data)->buff_save_cb))

static inline
int roi_acc_auto_save_active(struct device_data *dev_data)
{
	if (!auto_save_active(dev_data))
		return 0;
	return strlen(dev_data->roi_acc_buff.file_prefix) > 0;
}


static inline
void buffer_save_lock_init(struct device_data *dev_data)
{
	pthread_mutexattr_t mutex_attr;

	pthread_mutexattr_init(&mutex_attr);
	pthread_mutexattr_settype(&mutex_attr, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&dev_data->buff_save_lock, &mutex_attr);
	pthread_mutexattr_destroy(&mutex_attr);
}


static inline
int buffer_save_lock(struct device_data *dev_data)
{
	if (dev_data == NULL)
		return ESPIA_TEST_ERR_BUFFSAVE;
	pthread_mutex_lock(&dev_data->buff_save_lock);
	return 0;
}


static inline
void buffer_save_unlock(struct device_data *dev_data)
{
	pthread_mutex_unlock(&dev_data->buff_save_lock);
}


static inline
void edf_str_lock_init(struct device_data *dev_data)
{
	pthread_mutexattr_t mutex_attr;

	pthread_mutexattr_init(&mutex_attr);
	pthread_mutexattr_settype(&mutex_attr, PTHREAD_MUTEX_RECURSIVE); /*???*/
	pthread_mutex_init(&dev_data->edf_str_lock, &mutex_attr);
	pthread_mutexattr_destroy(&mutex_attr);
}


static inline
int edf_str_lock(struct device_data *dev_data)
{
	if (dev_data == NULL)
		return ESPIA_TEST_ERR_NOOPEN;  /* ??? */
	pthread_mutex_lock(&dev_data->edf_str_lock);
	return 0;
}


static inline
void edf_str_unlock(struct device_data *dev_data)
{
	pthread_mutex_unlock(&dev_data->edf_str_lock);
}


static inline
void last_frame_reset(struct last_frame_info *last_frame)
{
	struct img_frame_info *finfo = &last_frame->finfo;
	pthread_mutex_lock(&last_frame->lock);
	finfo->buffer_nr   = finfo->frame_nr     = SCDXIPCI_INVALID;
	finfo->round_count = finfo->acq_frame_nr = SCDXIPCI_INVALID;
	pthread_mutex_unlock(&last_frame->lock);
}

static inline
void last_frame_init(struct last_frame_info *last_frame)
{
	pthread_mutex_init(&last_frame->lock, NULL);
	last_frame_reset(last_frame);
}

static inline
void last_frame_update(struct last_frame_info *last_frame,
		       struct img_frame_info *finfo)
{
	pthread_mutex_lock(&last_frame->lock);
	last_frame->finfo = *finfo;
	pthread_mutex_unlock(&last_frame->lock);
}

static inline
void last_frame_get(struct last_frame_info *last_frame,
		    struct img_frame_info *finfo)
{
	pthread_mutex_lock(&last_frame->lock);
	*finfo = last_frame->finfo;
	pthread_mutex_unlock(&last_frame->lock);
}


/*--------------------------------------------------------------------------
 * function prototypes
 *--------------------------------------------------------------------------*/

int lib_buffer_save_cb_install( struct buffer_save_data *bsd );

int lib_buffer_save_cb_uninstall( struct device_data *dev_data );

int lib_buffer_save_file_close( struct buffer_save_data *bsd );

int lib_buffer_save_cb( etest_t etest, int dev_nr, 
                         struct espia_cb_data *cb_data, 
                         void *user_data );

int lib_reg_gen_cb(struct device_data *dev_data, struct espia_cb_data *cb_data,
	          struct cb_priv_data *priv_data, int user_act, int *cb_nr_ptr);

int lib_unreg_gen_cb( struct device_data *dev_data, int cb_nr, int user_cb, 
                      int *cb_nr_ptr );

int lib_gen_cb_funct(struct espia_cb_data *cb_data);

int lib_roi_acc_buffer_init(struct device_data *dev_data, int nr_frames);

int lib_roi_acc_buffer_install( struct device_data *dev_data );

int lib_roi_acc_buffer_del( struct device_data *dev_data );

#ifdef WITH_IMAGE
int lib_gen_img_add(struct device_data *dev_data, int buffer_nr, 
                    img_cb *cb_funct, struct img_frame_info *req_finfo, 
                    img_del *del_funct);
#endif // WITH_IMAGE

int lib_gen_img_del( struct device_data *dev_data, int buffer_nr );

int lib_last_frame_cb_install( struct device_data *dev_data );

int lib_set_gen_sg(struct device_data *dev_data, int img_config, 
	       struct espia_frame_dim *frame_dim, struct espia_roi *roi);

int lib_nonblock_save_init( etest_t etest, int dev_nr, 
                            struct buffer_save_data *bsd, int num_threads );


/*--------------------------------------------------------------------------
 * CLI callbacks
 *--------------------------------------------------------------------------*/

extern int cli_acq_cb_funct( void *context, int dev_nr, 
                             struct espia_cb_data *cb_data );



#endif /* ESPIA_TEST_LIB_PRIV_H */
