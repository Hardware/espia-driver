#ifndef ESPIA_TEST_LIB_H
#define ESPIA_TEST_LIB_H


/*--------------------------------------------------------------------------
 * application specific data ???
 *--------------------------------------------------------------------------*/

#define ESPIA_TEST_INVALID	SCDXIPCI_INVALID

#define ESPIA_TEST_ERR_BASE	(ESPIA_ERR_BASE - 500)
#define ESPIA_TEST_ERR_OPEN	(ESPIA_TEST_ERR_BASE - 1)
#define ESPIA_TEST_ERR_NOOPEN	(ESPIA_TEST_ERR_BASE - 2)
#define ESPIA_TEST_ERR_DEVNR	(ESPIA_TEST_ERR_BASE - 3)
#define ESPIA_TEST_ERR_SELECT	(ESPIA_TEST_ERR_BASE - 4)
#define ESPIA_TEST_ERR_NOMEM	(ESPIA_TEST_ERR_BASE - 5)
#define ESPIA_TEST_ERR_CBNR	(ESPIA_TEST_ERR_BASE - 6)
#define ESPIA_TEST_ERR_CBTYPE	(ESPIA_TEST_ERR_BASE - 7)
#define ESPIA_TEST_ERR_CBREG	(ESPIA_TEST_ERR_BASE - 8)
#define ESPIA_TEST_ERR_IMGINIT	(ESPIA_TEST_ERR_BASE - 9)
#define ESPIA_TEST_ERR_IMGSIZE	(ESPIA_TEST_ERR_BASE - 10)
#define ESPIA_TEST_ERR_IMGCRT	(ESPIA_TEST_ERR_BASE - 11)
#define ESPIA_TEST_ERR_IMGEXST	(ESPIA_TEST_ERR_BASE - 12)
#define ESPIA_TEST_ERR_IMGMISS	(ESPIA_TEST_ERR_BASE - 13)
#define ESPIA_TEST_ERR_PARAM	(ESPIA_TEST_ERR_BASE - 14)
#define ESPIA_TEST_ERR_FRMOP	(ESPIA_TEST_ERR_BASE - 15)
#define ESPIA_TEST_ERR_CBFUNCT  (ESPIA_TEST_ERR_BASE - 16)
#define ESPIA_TEST_ERR_NOBUFFER (ESPIA_TEST_ERR_BASE - 17)
#define ESPIA_TEST_ERR_ROIACC   (ESPIA_TEST_ERR_BASE - 18)
#define ESPIA_TEST_ERR_DUMPPAR  (ESPIA_TEST_ERR_BASE - 19)
#define ESPIA_TEST_ERR_OPTION   (ESPIA_TEST_ERR_BASE - 20)
#define ESPIA_TEST_ERR_MAPINIT  (ESPIA_TEST_ERR_BASE - 21)
#define ESPIA_TEST_ERR_MAPSIZE  (ESPIA_TEST_ERR_BASE - 22)
#define ESPIA_TEST_ERR_BUFFSAVE (ESPIA_TEST_ERR_BASE - 23)
#define ESPIA_TEST_ERR_FOCLASIG (ESPIA_TEST_ERR_BASE - 24)
#define ESPIA_TEST_ERR_EDFFORM  (ESPIA_TEST_ERR_BASE - 25)
#define ESPIA_TEST_ERR_INTERNAL (ESPIA_TEST_ERR_BASE - 26)
#define ESPIA_TEST_ERR_MAXFRMOP (ESPIA_TEST_ERR_BASE - 27)
#define ESPIA_TEST_ERR_NOTINIT  (ESPIA_TEST_ERR_BASE - 28)
#define ESPIA_TEST_ERR_BADPAR   (ESPIA_TEST_ERR_BASE - 29)
#define ESPIA_TEST_ERR_INVPLUG  (ESPIA_TEST_ERR_BASE - 30)
#define ESPIA_TEST_ERR_NOPLUGD  (ESPIA_TEST_ERR_BASE - 31)
#define ESPIA_TEST_ERR_MAXPLUG  (ESPIA_TEST_ERR_BASE - 32)
#define ESPIA_TEST_ERR_BUFFNUM  (ESPIA_TEST_ERR_BASE - 33)
#define ESPIA_TEST_ERR_ANY      (ESPIA_TEST_ERR_BASE - 34)
#define ESPIA_TEST_ERR_AUTOACT  (ESPIA_TEST_ERR_BASE - 35)


#define ESPIA_TEST_MAX_NR_DEV	(2 * ESPIA_MAX_NR_DEV)
#define ESPIA_TEST_MAX_OPS	64

#define ESPIA_TEST_POLL_TIME	10000	// us

#define FILE_PREFIX_NAME_LEN	512
#define FILE_SUFFIX_NAME_LEN	64
#define FILE_FULL_NAME_LEN	(FILE_PREFIX_NAME_LEN + 4 + \
				 FILE_SUFFIX_NAME_LEN + 4)

#define EDF_HEADER_LEN		1024
#define EDF_HEADER_BUFFER_LEN	(10 * EDF_HEADER_LEN)

#define ROI_ACC_BUFFER		(-2)

#define ESPIA_TEST_MIN_BUFFER_SIZE	(128 * 1024)

#define ETEST_CB_INTERN		0x00
#define ETEST_CB_USER		0x01
#define ETEST_CB_INACTIVE	0x00
#define ETEST_CB_ACTIVE		0x02
#define ETEST_CB_INTERN_ACTIVE	(ETEST_CB_INTERN | ETEST_CB_ACTIVE)

#define BUFFER_IMG_NAME_LEN	64

#define is_good_dev_nr(n)	((n >= 0) && (n < ESPIA_TEST_MAX_NR_DEV))


/***************************************************
 * SWIGVAR prefix before a variable name means that*
 * the variable gets special treatment from SWIG   *
 * and its name should be changed simultaneously   *
 * here and in the SWIG interface espia_acq_lib.i  *
 ***************************************************/
#ifdef SWIGVAR  /* or #if ??? */
 #undef SWIGVAR
 #define SWIGVAR
#else
 #define SWIGVAR
#endif /* SWIGVAR */


struct device_data;
struct cb_priv_data;
struct image_data;


struct lib_data;
typedef struct lib_data *etest_t;
/* Use a pointer typemap in SWIG later to do Python wrapping */

typedef int gen_cb(etest_t etest, int dev_nr, 
                   struct espia_cb_data *cb_data, 
                   void *user_data);

#ifdef WITH_IMAGE
typedef int img_cb(struct device_data *dev_data,
		   struct image_data *img_data,
		   struct espia_cb_data *cb_data);

typedef void img_del(struct device_data *dev_data,
		     struct image_data *img_data);
#endif // WITH_IMAGE

struct cb_priv_data {
	struct device_data	*dev_data;
	gen_cb			*cb_funct;
	void                    *user_data;
	int			 user_cb;
	int			 print_raw;
	int			 filter_tout;
#ifdef WITH_IMAGE
	struct image_data	*img_data;
#endif // WITH_IMAGE
	struct img_frame_info	 req_finfo;
};

#define priv_data_from_cb_data(cb_data) \
	((struct cb_priv_data *) (cb_data)->data)


/* This is a helper structure - an analog of espia_option from espia_lib.h */
#define ESPIA_OPT_MAX_LEN 127
struct lib_option {
	char name[ESPIA_OPT_MAX_LEN+1];
	int  attr;
};


/*--------------------------------------------------------------------------
 * macro and inline functions
 *--------------------------------------------------------------------------*/



/*--------------------------------------------------------------------------
 * function prototypes
 *--------------------------------------------------------------------------*/

char * espia_test_strerror( int ret );

int          lib_get_invalid( SWIGVAR unsigned long *invalid );

int          lib_test_init( etest_t *etest, SWIGVAR int argc, SWIGVAR char *argv[] );

int       lib_test_cleanup( etest_t etest );

int        lib_get_dev( etest_t etest, int dev_nr, SWIGVAR espia_t *espia_dev );

int           lib_open_dev( etest_t etest, int dev_nr );

int          lib_close_dev( etest_t etest, int dev_nr );

int lib_espia_get_option_data( etest_t etest, int dev_nr, SWIGVAR int *nr_option, 
                                struct lib_option *option );

int       lib_espia_option( etest_t etest, int dev_nr, int option, int action, 
                             int *val );

int         lib_get_option( etest_t etest, int dev_nr, char *name, 
                             SWIGVAR int *old_val );

int         lib_set_option( etest_t etest, int dev_nr, char *name, 
                             int new_val, SWIGVAR int *old_val );

int    lib_set_debug_level( etest_t etest, int dev_nr, SWIGVAR int *deb_lvl,
                             int drv);

int      lib_read_register( etest_t etest, int dev_nr, unsigned long reg_off,
                             SWIGVAR unsigned int *old_val );

int     lib_write_register( etest_t etest, int dev_nr, unsigned long reg_off,
                             SWIGVAR unsigned int *reg_val, unsigned int mask );

int lib_espia_get_param_data( etest_t etest, int dev_nr, SWIGVAR int *nr_param, 
                               struct espia_param *eparam );

int    lib_espia_get_param( etest_t etest, int dev_nr, int *param_arr, 
                             unsigned int *val_arr, int nr_param );

int          lib_get_param( etest_t etest, int dev_nr, char *param, 
                             SWIGVAR unsigned int *old_val );

int    lib_espia_set_param( etest_t etest, int dev_nr, int *param_arr, 
                             unsigned int *val_arr, int nr_param );

int          lib_set_param( etest_t etest, int dev_nr, char *param, 
                             SWIGVAR unsigned int *param_val );

int         lib_reset_link( etest_t etest, int dev_nr );

int        lib_serial_read( etest_t etest, int dev_nr, SWIGVAR char *in_buffer, 
                             SWIGVAR unsigned long *rd_bytes_p, long timeout );

int lib_serial_get_avail_bytes( etest_t etest, int dev_nr, 
                                 SWIGVAR unsigned long *avail_bytes_p );

int    lib_serial_read_str( etest_t etest, int dev_nr, SWIGVAR char *in_buffer,
                             SWIGVAR unsigned long *rd_bytes_p, SWIGVAR char *term,
                             SWIGVAR unsigned long term_bytes, long timeout );

int       lib_serial_write( etest_t etest, int dev_nr, char *out_buffer,
                             SWIGVAR unsigned long *wr_bytes_p, 
                             unsigned long block_size,
                             unsigned long delay, int block );

int       lib_serial_flush( etest_t etest, int dev_nr );

int   lib_serial_mmap_init( etest_t etest, int dev_nr, int max_bytes );

int  lib_serial_mmap_close( etest_t etest, int dev_nr );

int  lib_serial_mmap_write( etest_t etest, int dev_nr, 
                             SWIGVAR unsigned long *nr_written_p,
		             unsigned long block_size, unsigned long delay, 
		             int block );

int   lib_serial_mmap_read( etest_t etest, int dev_nr, SWIGVAR char *in_buffer, 
                             SWIGVAR unsigned long *rd_bytes_p, long timeout );

int      lib_frame_post_op( etest_t etest, int dev_nr, char *list );

int lib_frame_post_op_get_active( etest_t etest, int dev_nr,
	                           struct espia_frm_op_info op_info[], 
	                           int *nr_op );

int  lib_frame_post_op_del( etest_t etest, int dev_nr, char *fpo_name );

int       lib_buffer_alloc( etest_t etest, int dev_nr, int nr_buffer,
                             int frames, int width, int height, int depth, 
                             int display);

int        lib_buffer_free( etest_t etest, int dev_nr );

int    lib_get_buffer_data( etest_t etest, int dev_nr, int *nr_buffer,
                             struct espia_frame_dim *fdim );

int   lib_virt_buffer_info( etest_t etest, int dev_nr, int nr, 
                             struct img_buffer_info *binfo );

#ifdef WITH_IMAGE

int       lib_image_active( etest_t etest, int dev_nr, int buffer_nr );

int         lib_buffer_img( etest_t etest, int dev_nr, long buffer_nr );

int     lib_buffer_img_del( etest_t etest, int dev_nr, int buffer_nr );

int lib_buffer_img_del_all( etest_t etest, int dev_nr );

#endif // WITH_IMAGE

int lib_buffer_save_cb_get( etest_t etest, int dev_nr, SWIGVAR int *auto_save, 
                             SWIGVAR char* prefix, SWIGVAR int* idx, SWIGVAR int* format, 
                             SWIGVAR char* file_name, SWIGVAR int* tot_file_frames, 
                             SWIGVAR int* file_frames, SWIGVAR int* overwrite );

int        lib_buffer_save( etest_t etest, int dev_nr, long buffer_nr, 
                             char* fname_pr, int fname_idx, int format, 
                             int file_frames, int overwrite );

int       lib_buffer_clear( etest_t etest, int dev_nr, int buffer_nr );

int    lib_buffer_img_name( etest_t etest, int dev_nr, int buffer_nr,
			     SWIGVAR char *buf_img_name );

int   lib_buffer_img_rates( etest_t etest, int dev_nr, int buffer_nr,
			     SWIGVAR float *upd_rate, 
			     SWIGVAR float *ref_rate );

int    lib_buffer_img_norm( etest_t etest, int dev_nr, int buffer_nr,
                             SWIGVAR long *img_min_val, 
                             SWIGVAR long *img_max_val, SWIGVAR int *img_auto_range );

int            lib_roi_set( etest_t etest, int dev_nr, unsigned long frame_width,
                             unsigned long frame_height, unsigned long roi_left,
                             unsigned long roi_top );

int          lib_roi_reset( etest_t etest, int dev_nr );

int             lib_sg_set( etest_t etest, int dev_nr, unsigned long img_conf );

int            lib_dev_set( etest_t etest, int dev_nr, SWIGVAR int dev_list[], 
                             SWIGVAR int nr_dev );

int          lib_start_acq( etest_t etest, int dev_nr, int start_buffer, 
                             int nr_frames, long timeout );

int           lib_stop_acq( etest_t etest, int dev_nr );

int         lib_acq_status( etest_t etest, int dev_nr,
                             SWIGVAR unsigned long *acq_run_nr, 
                             SWIGVAR int *acq_active, SWIGVAR int *auto_save_active,
                             struct img_frame_info *last_acq_frame, 
                             struct img_frame_info *last_saved_frame );

int   lib_auto_save_active( etest_t etest, int dev_nr );

int         lib_reg_acq_cb( etest_t etest, int dev_nr, long buffer_nr,
                             long frame_nr, long round_count, long acq_frame_nr,
                             long timeout, gen_cb cb_funct, 
                             void *user_data );

int      lib_reg_status_cb( etest_t etest, int dev_nr, long timeout, 
                             gen_cb cb_funct, void *user_data );

int      lib_reg_ser_rx_cb( etest_t etest, int dev_nr, unsigned long buffer_len,
                             SWIGVAR char* term, SWIGVAR unsigned long term_bytes, 
                             long timeout, int print_raw, int filter_tout, 
                             gen_cb cb_funct, void *user_data );

int           lib_unreg_cb( etest_t etest, int dev_nr, int cb_nr );

int       lib_unreg_all_cb( etest_t etest, int dev_nr );

int          lib_cb_active( etest_t etest, int dev_nr, int cb_nr, int active );

int         lib_get_num_cb( etest_t etest, int dev_nr, SWIGVAR int *num_cb );

int         lib_is_user_cb( etest_t etest, int dev_nr, int cb_nr, 
                             SWIGVAR int *user_cb );

int lib_espia_get_callback( etest_t etest, int dev_nr, int cb_nr, 
                             struct espia_cb_data *cb_data_ptr );

int          lib_get_frame( etest_t etest, int dev_nr, 
                             struct img_frame_info *cb_finfo, long timeout );

int         lib_ccd_status( etest_t etest, int dev_nr, 
                             SWIGVAR unsigned char *ccd_status, long timeout );

int     lib_roi_acc_buffer( etest_t etest, int dev_nr, int col_beg, int col_end,
                             int row_beg, int row_end, int nr_frames, 
                             char *file_prefix, int file_idx );

int     lib_focla_open_dev( etest_t etest, int dev_nr );

int    lib_focla_close_dev( etest_t etest, int dev_nr );

int         lib_focla_read( etest_t etest, int dev_nr, int reg_nr, 
                             SWIGVAR unsigned char *old_val );

int         lib_focla_write( etest_t etest, int dev_nr, int reg_nr, 
                             unsigned char new_val );

int lib_focla_get_param_data( SWIGVAR int *nr_param, 
                               struct espia_param *param_ptr );

int    lib_focla_get_param( etest_t etest, int dev_nr, int par_nr, 
                             SWIGVAR int *old_val );

int      lib_focla_get_par( etest_t etest, int dev_nr, char *name, 
                             SWIGVAR int *old_val );

int      lib_focla_set_par( etest_t etest, int dev_nr, char *name, 
                             int new_val, SWIGVAR int *old_val );

int lib_focla_get_sig_data( int *nr_param, struct focla_signal *sig_ptr );

int  lib_focla_pulse_start( etest_t etest, int dev_nr, int cam_nr, 
                             char *sig_name, int polarity, int width_us,
                             int delay_us, int nr_pulse );

int   lib_focla_pulse_stop( etest_t etest, int dev_nr, int cam_nr, 
                             char *sig_name );

int lib_focla_pulse_status( etest_t etest, int dev_nr, int cam_nr, 
                             char *sig_name, SWIGVAR int *pulse_active, 
                             SWIGVAR int *curr_pulse, SWIGVAR int *curr_stage );

int  lib_focla_serial_read( etest_t etest, int dev_nr, SWIGVAR char *in_buffer, 
                             SWIGVAR unsigned long *rd_bytes_p, long timeout );

int lib_focla_serial_get_avail_bytes( etest_t etest, int dev_nr, 
                                       SWIGVAR unsigned long *avail_bytes_p );

int lib_focla_serial_read_str( etest_t etest, int dev_nr, SWIGVAR char *in_buffer,
                             SWIGVAR unsigned long *rd_bytes_p, SWIGVAR char *term,
                             SWIGVAR unsigned long term_bytes, long timeout );

int lib_focla_serial_write( etest_t etest, int dev_nr, char *out_buffer,
                            SWIGVAR unsigned long *wr_bytes_p, 
                            unsigned long block_size,
                            unsigned long delay, int block );

int lib_focla_serial_flush( etest_t etest, int dev_nr );

int          lib_get_stats( etest_t etest, int dev_nr, 
                                                 struct scdxipci_stats *stats );

int lib_espia_get_firmware_data( int *nr_param, 
			          struct espia_firmware *firmware_ptr );

int        lib_get_version( struct espia_lib_version *ver );


int           lib_get_data( etest_t etest, int dev_nr, long buf_nr, long frame_nr, 
                             SWIGVAR unsigned char **data_ptr, SWIGVAR int *data_len
                         /*, struct espia_frame_dim *frame_dim*/ );

int     lib_get_frame_info( etest_t etest, int dev_nr, int buf_nr, int frame_nr,
                             struct espia_frame_dim *frame_dim,
                             struct img_frame_info *frame_info );

int         lib_reg_plugin( etest_t etest, int dev_nr, int plugin_type, 
                             void *fptr, void *userData, char *desc );

int   lib_get_plugins_data( etest_t etest, int dev_nr, 
                             struct lib_plugins_data *plugins_data );

int      lib_plugin_active( etest_t etest, int dev_nr, int plugin_type,
                             int plugin_nr, int active );

int      lib_plugin_delete( etest_t etest, int dev_nr, int plugin_type,
                             int plugin_nr );

int      lib_plugin_output( etest_t etest, int dev_nr, int plugin_type,
	                     int plugin_nr, int width, int height, int depth );

int     lib_edf_usr_header( etest_t etest, int dev_nr, char *usr_str );

int lib_nonblock_frame_save( etest_t etest, int dev_nr, int acq_fr_nr );

int lib_nonblock_save_mode( etest_t etest, int dev_nr, int mode );

int lib_nonblock_save_cleanup( etest_t etest, int dev_nr );

#ifdef WITH_IMAGE
int      lib_image_refresh( etest_t etest );
#endif // WITH_IMAGE


/*--------------------------------------------------------------------------*
 * Ignore everything below this line! It is just for testing!
 *--------------------------------------------------------------------------*/

int               lib_show( etest_t etest, int dev_nr );
int          lib_dump_data( void *data, int len );


#endif /* ESPIA_TEST_LIB_H */
