/****************************************************************************
 * File:	espia_test_lib.c
 * $Header: /segfs/bliss/cvs/driver/ESRF/espia/src/espia_acq_lib.c,v 2.35 2020/07/23 11:00:41 ahoms Exp $
 * Project:	SECAD Espia card Linux driver
 * Description:	Espia test program, library part
 * Author(s):	SECAD S.A.
 *		A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 *              A. Kirov, BLISS, ESRF (assen.kirov@esrf.fr)
 ****************************************************************************/

#define _GNU_SOURCE

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <syslog.h>
#include <regex.h>

#include "espia_lib.h"
#include "esrfdebug.h"
#ifdef WITH_IMAGE
#include "imageapi.h"
#endif // WITH_IMAGE
#include "focla_lib.h"

#include "espia_acq_lib.h"
#include "espia_acq_lib_priv.h"

#define lib_log_debug(format, ...) syslog(LOG_DEBUG, format, ## __VA_ARGS__)
#define lib_log_info(format, ...) syslog(LOG_INFO, format, ## __VA_ARGS__)
#define lib_log_err(format, ...) syslog(LOG_ERR, format, ## __VA_ARGS__)


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_get_invalid( unsigned long *invalid )
{
	*invalid = ESPIA_TEST_INVALID;
	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_get_dev( etest_t etest, int dev_nr, espia_t *espia_dev )
{
	struct lib_data *plib;
	get_check_lib_data(plib, etest);

	if (!is_good_dev_nr(dev_nr))
		return ESPIA_TEST_ERR_DEVNR; 

	*espia_dev = plib->device[dev_nr].dev;

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * init / cleanup
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_test_init(etest_t *etest, int argc, char *argv[])
{
	int i, ret;
	struct lib_data *lib_data;
	struct device_data *dev_data;
	struct buffer_save_data *bsd;

	openlog("espia_acq_lib", LOG_PID|LOG_CONS, LOG_USER);
	setlogmask(0xFF);

	ret = ESPIA_TEST_ERR_NOMEM;
	lib_data = (struct lib_data *) calloc(1, sizeof(struct lib_data));
	if (lib_data == NULL)
		goto out;

	for (i = 0; i < ESPIA_TEST_MAX_NR_DEV; i++) {
		dev_data = &lib_data->device[i];
		dev_data->dev = ESPIA_DEV_INVAL;
		dev_data->focla = FOCLA_DEV_INVAL;
		dev_data->last_frame_cb   = (int) SCDXIPCI_INVALID;
		dev_data->buff_save_cb    = (int) SCDXIPCI_INVALID;
		dev_data->roi_acc_buff_cb = (int) SCDXIPCI_INVALID;
		bsd = &dev_data->buff_save_data;
		bsd->dev_data = dev_data;
		last_frame_init(&dev_data->last_frame);
		last_frame_init(&bsd->last_frame);
		buffer_save_lock_init(dev_data);
#ifdef WITH_IMAGE
		image_data_init(&dev_data->live_img);
		image_data_init(&dev_data->roi_acc_buff.img_data);
#endif // WITH_IMAGE
		img_roi_reset(&dev_data->roi_acc_buff.roi);
		dev_data->lib_data = lib_data;
		edf_str_lock_init(dev_data);
		dev_data->edf_usr_str = NULL;
		dev_data->thread_pool = NULL;
	}

	lib_data->saved_argv = (char **) calloc(argc+1, sizeof(char*));
	if (lib_data->saved_argv == NULL)
		goto out;

	lib_data->saved_argc = argc;
	for( i=0; i<argc; i++ ) {
		lib_data->saved_argv[i] = strdup(argv[i]);
		if (lib_data->saved_argv[i] == NULL)
			goto out;
	}
	lib_data->saved_argv[argc] = NULL;
	ret = ESPIA_OK;

#ifdef WITH_IMAGE
	if (image_init(lib_data->saved_argc, lib_data->saved_argv) < 0) {
		ret = ESPIA_TEST_ERR_IMGINIT;
		goto out;
	}
	lib_data->image_init_flag = 1;
#endif // WITH_IMAGE

 out:
	*etest = (etest_t) lib_data;
	if (( ret != ESPIA_OK ) && ( *etest != NULL ))
		lib_test_cleanup( *etest );

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_test_cleanup( etest_t etest )
{
	struct lib_data *plib = (struct lib_data *) etest;
	int i, ret, lib_ret=ESPIA_OK;


	closelog();

	if( NULL == plib )
		return -EINVAL; 

	for (i = 0; i < ESPIA_TEST_MAX_NR_DEV; i++) {
		if (plib->device[i].dev == ESPIA_DEV_INVAL) 
			continue;

		ret = lib_close_dev( etest, i );
		if( ret < 0 ) {
			lib_ret = ret;
			continue;
		}
	}

#ifdef WITH_IMAGE
	if( plib->image_init_flag )
		image_exit();
#endif // WITH_IMAGE

	if( plib->saved_argc && plib->saved_argv ) {
		for( i=0; i<plib->saved_argc; i++ )
			if (plib->saved_argv[i])
				free( plib->saved_argv[i] );
		free( plib->saved_argv );
	}

	free(plib); 

	return lib_ret;
}


/*--------------------------------------------------------------------------
 * espia_test str error
 *--------------------------------------------------------------------------*/

static deb_const_str error_str_array[] = {
	{ESPIA_TEST_ERR_OPEN,	"Device already open"},
	{ESPIA_TEST_ERR_NOOPEN,	"Device not open"},
	{ESPIA_TEST_ERR_DEVNR,	"Device number REALLY too bad"},
	{ESPIA_TEST_ERR_SELECT,	"No device currently selected"},
	{ESPIA_TEST_ERR_NOMEM,	"Error allocating memory"},
	{ESPIA_TEST_ERR_CBNR,	"Invalid callback nr"},
	{ESPIA_TEST_ERR_CBTYPE,	"Invalid callback type"},
	{ESPIA_TEST_ERR_CBREG,	"Only one callback of that type allowed"},
	{ESPIA_TEST_ERR_IMGINIT,"Error initializing image module"},
	{ESPIA_TEST_ERR_IMGSIZE,"Image width/height/len doesn't fit buffer"},
	{ESPIA_TEST_ERR_IMGCRT ,"Error creating image"},
	{ESPIA_TEST_ERR_IMGEXST,"Buffer image already exists"},
	{ESPIA_TEST_ERR_IMGMISS,"Buffer image does not exist"},
	{ESPIA_TEST_ERR_PARAM,  "Invalid parameter name"},
	{ESPIA_TEST_ERR_FRMOP,  "Invalid frame operation name"},
	{ESPIA_TEST_ERR_CBFUNCT,"Invalid CB function"},
	{ESPIA_TEST_ERR_NOBUFFER,"No buffer allocated yet"},
	{ESPIA_TEST_ERR_ROIACC, "Image can only be created with RoiAccBuffer"},
	{ESPIA_TEST_ERR_DUMPPAR,"Invalid buffer offset or nr_bytes"},
	{ESPIA_TEST_ERR_OPTION, "Invalid option name"},
	{ESPIA_TEST_ERR_MAPINIT,"Serial map not yet initialized"},
	{ESPIA_TEST_ERR_MAPSIZE,"Serial map size exceeded"},
	{ESPIA_TEST_ERR_BUFFSAVE,"Buffer save data is invalid"},
	{ESPIA_TEST_ERR_FOCLASIG,"Invalid FOCLA signal"},
	{ESPIA_TEST_ERR_EDFFORM,"User string doesn't match EDF header format"},
	{ESPIA_TEST_ERR_INTERNAL,"Internal error"},
	{ESPIA_TEST_ERR_MAXFRMOP,"Maximum number of frame post-op reached"},
	{ESPIA_TEST_ERR_NOTINIT,"Component not initialized"},
	{ESPIA_TEST_ERR_BADPAR, "Incorrect input parameter"},
	{ESPIA_TEST_ERR_INVPLUG,"Invalid plugin"},
	{ESPIA_TEST_ERR_NOPLUGD,"Plugins data not initialized"},
	{ESPIA_TEST_ERR_MAXPLUG,"Maximum number of plugins already reached"},
	{ESPIA_TEST_ERR_BUFFNUM,"Invalid buffer number"},
	{ESPIA_TEST_ERR_ANY,    "Both buffer_nr and acq_frame_nr are ANY"},
	{ESPIA_TEST_ERR_AUTOACT,"Auto buffer save already active"},
};

INIT_STATIC_CONST_STR_ARRAY_PREFIX(error_strings, error_str_array, 
				   "ESPIA_TEST");

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
char *espia_test_strerror(int ret)
{
	return GET_CONST_STR(error_strings, ret, focla_strerror(ret));
}


/*--------------------------------------------------------------------------
 * open / close / select
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_open_dev(etest_t etest, int dev_nr)
{
	struct lib_data *plib = (struct lib_data *) etest;
	struct device_data *dev_data;
	int ret;

	if (!is_good_dev_nr(dev_nr))
		return ESPIA_TEST_ERR_DEVNR;

	dev_data = &plib->device[dev_nr];
	if (dev_data->dev != ESPIA_DEV_INVAL)
		return ESPIA_TEST_ERR_OPEN;

	ret = espia_open(dev_nr, &dev_data->dev);
	if (ret < 0)
		return ret;

	dev_data->dev_nr = dev_nr;
	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_close_dev( etest_t etest, int dev_nr )
{
	struct device_data *dev_data;
	int ret;

	get_check_dev_data(dev_data, etest, dev_nr);

#define cleanup(funct, what)						\
	ret = funct( etest, dev_nr );					\
	if (ret < 0) {							\
		return ret;						\
	}

	cleanup(lib_stop_acq,          "stopping acq of");
	cleanup(lib_unreg_all_cb,      "unregistering cbs of");
	cleanup(lib_buffer_free,       "freeing buffer of");
	cleanup(lib_serial_mmap_close, "closing serial files of");
	cleanup(lib_focla_close_dev,   "closing FOCLA dev of");

#undef cleanup

	lib_edf_usr_header(etest, dev_nr, NULL);

	lib_nonblock_save_cleanup( etest, dev_nr );

	if( (ret = espia_close(dev_data->dev)) < 0 )
		return ret;

	dev_data->dev = ESPIA_DEV_INVAL;

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * driver option
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_espia_get_option_data( etest_t etest, int dev_nr, int *nr_option, 
                               struct lib_option *option )
{
	int ret;
	struct espia_option *eoption=NULL;
	struct device_data *dev_data;


	if( NULL != option )
		memset( option, 0, sizeof(struct lib_option) );

	get_check_dev_data(dev_data, etest, dev_nr);

	if( -1 == *nr_option )
		ret = espia_get_option_data(dev_data->dev, nr_option, NULL);
	else
	{
		ret = espia_get_option_data(dev_data->dev, nr_option, &eoption);
		if( (ret >= 0) && (NULL != eoption) && (NULL != option) )
		{
			option->attr = eoption->option;
			strncpy(option->name, eoption->name, ESPIA_OPT_MAX_LEN);
		}
	}

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_espia_option(etest_t etest, int dev_nr, int option, int action, int *val)
{
	struct device_data *dev_data;

	get_check_dev_data( dev_data, etest, dev_nr );

	return espia_option( dev_data->dev, option, action, val );
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_get_option( etest_t etest, int dev_nr, char *name, int *old_val )
{
	struct espia_option *eoption;
	int ret, i, nr_option, option, action, val;
	struct device_data *dev_data;


	*old_val = 0;  /* ??? */
	get_check_dev_data(dev_data, etest, dev_nr);

	nr_option = -1;
	ret = espia_get_option_data(dev_data->dev, &nr_option, NULL);
	if (ret < 0)
		return ret;

	for ( i=0; i<nr_option; i++ ) {
		ret = espia_get_option_data(dev_data->dev, &i, &eoption);
		if (ret < 0)
			return ret;
		if( strcasecmp(name, eoption->name) == 0 )
			break;
	}
	if (i == nr_option)
		return ESPIA_TEST_ERR_OPTION;

	action = SCDXIPCI_OPT_RD;
	option = eoption->option;
	ret = espia_option( dev_data->dev, option, action, &val );
	if (ret >= 0)
		*old_val = val;

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_set_option( etest_t etest, int dev_nr, char *name, 
                    int new_val, int *old_val )
{
	struct espia_option *eoption;
	int ret, i, nr_option, option, action, val;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	nr_option = -1;
	ret = espia_get_option_data(dev_data->dev, &nr_option, NULL);
	if (ret < 0)
		return ret;

	action = SCDXIPCI_OPT_RD_WR;
	for (i = 0; i < nr_option; i++) {
		ret = espia_get_option_data(dev_data->dev, &i, &eoption);
		if (ret < 0)
			return ret;
		if (strcasecmp(name, eoption->name) == 0)
			break;
	}
	if (i == nr_option)
		return ESPIA_TEST_ERR_OPTION;

	option = eoption->option;

	val = new_val;
	ret = espia_option(dev_data->dev, option, action, &val);
	*old_val = val;

	return ret;
}


/*--------------------------------------------------------------------------
 * debug level
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int    lib_set_debug_level( etest_t etest, int dev_nr, int *deb_lvl, int drv)
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return espia_debug_level(dev_data->dev, deb_lvl, drv);
}


/*--------------------------------------------------------------------------
 * read/write register
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_read_register( etest_t etest, int dev_nr, unsigned long reg_off,
                       unsigned int *old_val )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return espia_read_register(dev_data->dev, reg_off, old_val);
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_write_register( etest_t etest, int dev_nr, unsigned long reg_off,
                        unsigned int *reg_val, unsigned int mask )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return espia_write_register(dev_data->dev, reg_off, reg_val, mask);
}


/*--------------------------------------------------------------------------
 * param list 
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_espia_get_param_data( etest_t etest, int dev_nr, int *nr_param, 
                              struct espia_param *eparam )
{
	int ret;
	struct espia_param *eparam_p=NULL;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	ret = espia_get_param_data(dev_data->dev, nr_param, 
	                                          (eparam ? &eparam_p : NULL) );

	if( eparam ) {
		if( (ret >= 0) && (NULL != eparam_p) )
			memcpy( eparam, eparam_p, sizeof(struct espia_param) );
		else
			memset( eparam, 0, sizeof(struct espia_param) );
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * get/set parameter
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_espia_get_param( etest_t etest, int dev_nr, int *param_arr, 
                         unsigned int *val_arr, int nr_param )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return espia_get_param(dev_data->dev, param_arr, val_arr, nr_param);
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_get_param( etest_t etest, int dev_nr, char *param, unsigned int *old_val )
{
	unsigned int *val_arr;
	struct espia_param *eparam;
	int ret, i, first, end, len, *param_arr, nr_param;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	nr_param = -1;
	ret = espia_get_param_data(dev_data->dev, &nr_param, NULL);
	if (ret < 0)
		return ret;

	for (first = 0; first < nr_param; first++) {
		ret = espia_get_param_data(dev_data->dev, &first, &eparam);
		if (ret < 0)
			return ret;
		if (strcasecmp(param, eparam->name) == 0)
			break;
	}
	if (first == nr_param)
		return ESPIA_TEST_ERR_PARAM;
	end = first + 1;
	len = end - first;  /* Yes, I know it is 1... This is for the future */

	val_arr = NULL;

	param_arr = (int *) calloc(len, sizeof(*param_arr));
	if (param_arr == NULL) {
		ret = ESPIA_TEST_ERR_NOMEM;
		goto out;
	}
	val_arr = (unsigned int *) calloc(len, sizeof(*val_arr));
	if (val_arr == NULL) {
		ret = ESPIA_TEST_ERR_NOMEM;
		goto out;
	}

	for (i = 0; i < len; i++)
		param_arr[i] = first + i;

	ret = espia_get_param(dev_data->dev, param_arr, val_arr, len);
	if (ret >= 0)
		*old_val = val_arr[0];  /* Only one parameter! */
 out:
	if (param_arr)
		free(param_arr);
	if (val_arr)
		free(val_arr);

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_espia_set_param( etest_t etest, int dev_nr, int *param_arr, 
                         unsigned int *val_arr, int nr_param )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return espia_set_param(dev_data->dev, param_arr, val_arr, nr_param);
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_set_param( etest_t etest, int dev_nr, char *param, 
                   unsigned int *param_val )
{
	int ret, idx, nr_param;
	struct espia_param *eparam=NULL;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	nr_param = -1;
	ret = espia_get_param_data(dev_data->dev, &nr_param, NULL);
	if (ret < 0)
		return ret;

	for (idx = 0; idx < nr_param; idx++) {
		ret = espia_get_param_data(dev_data->dev, &idx, &eparam);
		if (ret < 0)
			return ret;
		if (strcasecmp(param, eparam->name) == 0)
			break;
	}
	if (idx == nr_param)
		return ESPIA_TEST_ERR_PARAM;

	return espia_set_param(dev_data->dev, &idx, param_val, 1);
}


/*--------------------------------------------------------------------------
 * reset link
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_reset_link( etest_t etest, int dev_nr )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return espia_reset_link(dev_data->dev);
}


/*--------------------------------------------------------------------------
 * serial read/write/flush
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 *     Notes : The buffer should be allocated before and must be at least   *
 *             *rd_bytes_p of size;                                         *
 ****************************************************************************/
int lib_serial_read( etest_t etest, int dev_nr, char *in_buffer, 
                      unsigned long *rd_bytes_p, long timeout )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return espia_ser_read( dev_data->dev, in_buffer, rd_bytes_p, 
	                       timeout );
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_serial_get_avail_bytes( etest_t etest, int dev_nr, 
                                unsigned long *avail_bytes_p )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	*avail_bytes_p = 0;
	return espia_ser_read( dev_data->dev, NULL, avail_bytes_p, 0 );
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 *     Notes : The buffer should be allocated before and must be at least   *
 *             *rd_bytes_p of size;                                         *
 ****************************************************************************/
int lib_serial_read_str( etest_t etest, int dev_nr, char *in_buffer,
                          unsigned long *rd_bytes_p, char *term,
                          unsigned long term_bytes, long timeout )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return espia_ser_read_str( dev_data->dev, in_buffer, rd_bytes_p, term, 
	                           term_bytes, timeout );
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_serial_write( etest_t etest, int dev_nr, char *out_buffer,
                      unsigned long *wr_bytes_p, unsigned long block_size,
                      unsigned long delay, int block )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return espia_ser_write( dev_data->dev, out_buffer, wr_bytes_p, 
	                        block_size, delay, block );
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_serial_flush( etest_t etest, int dev_nr )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return espia_ser_flush(dev_data->dev);
}


/*--------------------------------------------------------------------------
 * serial read/write
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_serial_mmap_init( etest_t etest, int dev_nr, int max_bytes )
{
	char *buffer;
	int ret;
	char filename[256];
	struct serial_mmap_data *mmap_data;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	mmap_data= &dev_data->serial_mmap;
	mmap_data->init= 0;
	mmap_data->max_bytes= max_bytes;
	mmap_data->mem_size= mmap_data->max_bytes + sizeof(int);
	buffer= calloc(mmap_data->mem_size, 1);

	sprintf(filename, ESPIA_TEST_SERIAL_FILENAME, dev_data->dev_nr, "write");
	mmap_data->write_file= open(filename, O_RDWR|O_TRUNC|O_CREAT, \
					S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);

	if (mmap_data->write_file < 0) {
		free(buffer);
		lib_log_err("Failed to create file %s.\n", filename);
		return ESPIA_TEST_ERR_MAPINIT;
	}
	
	ret = write(mmap_data->write_file, buffer, mmap_data->mem_size);
	if (ret != mmap_data->mem_size) {
		close(mmap_data->write_file);
		free(buffer);
		lib_log_err("Failed to write %d bytes to file %s\n",
			    mmap_data->mem_size, filename);
		return ESPIA_TEST_ERR_MAPINIT;
	}

	fsync(mmap_data->write_file);
	lseek(mmap_data->write_file, 0, SEEK_SET);
	mmap_data->write_mmap= mmap(NULL, mmap_data->mem_size,
				    PROT_WRITE|PROT_READ,MAP_SHARED,
				    mmap_data->write_file, 0);
	lib_log_info("%s created.\n", filename);

	sprintf(filename, ESPIA_TEST_SERIAL_FILENAME, dev_data->dev_nr, "read");
	mmap_data->read_file= open(filename, O_RDWR|O_TRUNC|O_CREAT, \
					S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);

	if (mmap_data->read_file < 0) {
		munmap(mmap_data->write_mmap, mmap_data->mem_size);
		close(mmap_data->write_file);
		free(buffer);
		lib_log_err("Failed to create file %s\n", filename);
		return ESPIA_TEST_ERR_MAPINIT;
	}

	ret = write(mmap_data->read_file, buffer, mmap_data->mem_size);
	if (ret != mmap_data->mem_size) {
		close(mmap_data->read_file);
		munmap(mmap_data->write_mmap, mmap_data->mem_size);
		close(mmap_data->write_file);
		free(buffer);
		lib_log_err("Failed to write %d bytes to file %s\n",
			    mmap_data->mem_size, filename);
		return ESPIA_TEST_ERR_MAPINIT;
	}

	fsync(mmap_data->read_file);
	lseek(mmap_data->write_file, 0, SEEK_SET);
	mmap_data->read_mmap= mmap(NULL, mmap_data->mem_size,
				   PROT_WRITE|PROT_READ,MAP_SHARED,
				   mmap_data->read_file, 0);
	lib_log_info("%s created.\n", filename);

	mmap_data->init= 1;
	free(buffer);
	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_serial_mmap_close( etest_t etest, int dev_nr )
{
	char filename[256];
	struct serial_mmap_data *mmap_data;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	mmap_data= &dev_data->serial_mmap;
	if (mmap_data->init) {
		munmap(mmap_data->read_mmap, mmap_data->mem_size);
		close(mmap_data->read_file);
		sprintf(filename, ESPIA_TEST_SERIAL_FILENAME, dev_data->dev_nr, "read");
		unlink(filename);
		lib_log_info("%s removed.\n", filename);
		munmap(mmap_data->write_mmap, mmap_data->mem_size);
		close(mmap_data->write_file);
		sprintf(filename, ESPIA_TEST_SERIAL_FILENAME, dev_data->dev_nr, "write");
		unlink(filename);
		lib_log_info("%s removed.\n", filename);
	}
	mmap_data->init= 0;

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_serial_mmap_write( etest_t etest, int dev_nr, 
                            unsigned long *nr_written_p,
		            unsigned long block_size, unsigned long delay, 
		            int block )
{
	char *buffer;
	int ret, rlen;
	struct serial_mmap_data *mmap_data;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	mmap_data = &dev_data->serial_mmap;
	if (!mmap_data->init)
		return ESPIA_TEST_ERR_MAPINIT;

	memcpy(&rlen, mmap_data->write_mmap, sizeof(int));
	*nr_written_p = (unsigned long)rlen;

	if (rlen > mmap_data->max_bytes)
		return ESPIA_TEST_ERR_MAPSIZE;

	buffer= calloc(rlen, 1);
	memcpy(buffer, mmap_data->write_mmap+sizeof(int), rlen);

	ret = espia_ser_write(dev_data->dev, buffer, nr_written_p, block_size, 
			      delay, block);

	free(buffer);

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int   lib_serial_mmap_read( etest_t etest, int dev_nr, char *in_buffer, 
                             unsigned long *rd_bytes_p, long timeout )
{
	int ret;
	struct serial_mmap_data *mmap_data;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	mmap_data = &dev_data->serial_mmap;

	if (!mmap_data->init)
		return ESPIA_TEST_ERR_MAPINIT;

	if ((int) *rd_bytes_p > mmap_data->max_bytes)
		return ESPIA_TEST_ERR_MAPSIZE;

	ret = espia_ser_read(dev_data->dev, in_buffer, rd_bytes_p, timeout);
	if (ret == ESPIA_OK) {
		memcpy(mmap_data->read_mmap, rd_bytes_p, sizeof(int));
		memcpy(mmap_data->read_mmap+sizeof(int), in_buffer, *rd_bytes_p);
		msync(mmap_data->read_mmap, *rd_bytes_p+sizeof(int), MS_ASYNC);
	}
	return ret;
}


/*--------------------------------------------------------------------------
 * buffer post-operation
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_frame_post_op_from_name(char *name, struct espia_frm_op_fn_set **fn_set)
{
	int ret, nr_op, op;

	nr_op = -1;
	ret = espia_get_frm_op_fn_set(&nr_op, NULL);
	if (ret < 0)
		return ret;

	for (op = 0; op < nr_op; op++) {
		ret = espia_get_frm_op_fn_set(&op, fn_set);
		if (ret < 0)
			return ret;

		if (strcasecmp((*fn_set)->name, name) == 0)
			return ESPIA_OK;
	}

	return ESPIA_TEST_ERR_FRMOP;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_frame_post_op( etest_t etest, int dev_nr, char *list )
{
	char *ptr, *term;
	int nr_op, ret, plugf=0;
	struct espia_frm_op_fn_set *fn_set;
	struct espia_frm_op_info op_info[ESPIA_TEST_MAX_OPS];
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);
	
	if (list != NULL) {
		list = strdup(list);
		if (list == NULL)
			return ESPIA_TEST_ERR_NOMEM;
	}

	if( ! strcmp(list, "ACQ_PLUGIN_I") ) {
		struct espia_frm_op_info op_info_old[ESPIA_TEST_MAX_OPS];
		int nr_op_old;

		ret = espia_get_current_frm_op( dev_data->dev, op_info_old, 
		                                &nr_op_old );
		if (ret < 0) {
			goto out;
		}

		ret = lib_frame_post_op_from_name( "ACQ_PLUGIN_I", &fn_set );
		if (ret < 0) {
			goto out;
		}

		for( nr_op=0; nr_op<nr_op_old; nr_op++ ) {
			if( (op_info[nr_op].fn_set=op_info_old[nr_op].fn_set)
			     == fn_set ) plugf = 1;
			op_info[nr_op].data   = op_info_old[nr_op].data;
		}

		if( ! plugf ) {
			if( nr_op >= ESPIA_TEST_MAX_OPS ) {
				ret = ESPIA_TEST_ERR_MAXFRMOP;
				goto out;
			}
			op_info[nr_op].fn_set = fn_set;
			op_info[nr_op].data   = &dev_data->post_op_fn_data;
			nr_op++;
		}
	} else {
		for( nr_op = 0, ptr = list; 
		     ptr && *ptr && (nr_op < ESPIA_TEST_MAX_OPS);
		     nr_op++, ptr = term ) 
		{
			term = strchr(ptr, ',');
			if (term != NULL)
				*term++ = 0;
			ret = lib_frame_post_op_from_name( ptr, &fn_set );
			if (ret < 0)
				goto out;

			if( ! strcmp(ptr, "ACQ_PLUGIN_I") )
				plugf = 1;

			op_info[nr_op].fn_set = fn_set;
			op_info[nr_op].data   = &dev_data->post_op_fn_data;
		}

		if( (! plugf) && dev_data->plugins_data &&
		              dev_data->plugins_data->num_plugins_I )  /* ??? */
		{
			ret = lib_frame_post_op_from_name( "ACQ_PLUGIN_I", 
			                                   &fn_set );
			if (ret < 0)
				goto out;

			if( nr_op >= ESPIA_TEST_MAX_OPS ) {
				ret = ESPIA_TEST_ERR_MAXFRMOP;
				goto out;
			}
			op_info[nr_op].fn_set = fn_set;
			op_info[nr_op].data   = &dev_data->post_op_fn_data;
			nr_op++;
		}
	}

	ret = espia_set_frm_op_fn(dev_data->dev, op_info, nr_op);

 out:
	if (list != NULL)
		free(list);

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_frame_post_op_get_active( etest_t etest, int dev_nr,
	                          struct espia_frm_op_info op_info[], 
	                          int *nr_op )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return espia_get_current_frm_op( dev_data->dev, op_info, nr_op );
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_frame_post_op_del( etest_t etest, int dev_nr, char *fpo_name )
{
	int ret, nr_op, i, j;
	struct espia_frm_op_fn_set *fn_set;
	struct espia_frm_op_info op_info[ESPIA_TEST_MAX_OPS];
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	ret = espia_get_current_frm_op( dev_data->dev, op_info, &nr_op );
	if( ret < 0 ) return ret;

	ret = lib_frame_post_op_from_name( fpo_name, &fn_set );
	if( ret < 0 ) return ret;

	for( i=0; i<nr_op; i++ ) {
		if( op_info[i].fn_set == fn_set ) {
			--nr_op;
			for( j=i; j<nr_op; j++ ) op_info[j] = op_info[j+1];
		}
	}

	ret = espia_set_frm_op_fn(dev_data->dev, op_info, nr_op);

	return ret;
}


/*--------------------------------------------------------------------------
 * last frame cb funct/install/uninstall
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_last_frame_cb( etest_t etest, int dev_nr, 
                       struct espia_cb_data *cb_data, 
                       void *user_data )
{
	struct img_frame_info *finfo = &cb_data->info.acq.cb_finfo;
	struct device_data *dev_data;

	get_check_dev_data(dev_data, etest, dev_nr);

	if (!finished_espia_frame_info(finfo, cb_data->ret))
		last_frame_update(&dev_data->last_frame, finfo);

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_last_frame_cb_install( struct device_data *dev_data )
{
	struct espia_cb_data cb_data;
	struct img_frame_info *finfo;
	struct cb_priv_data priv_data;


	init_cb_priv_data(&priv_data);

	cb_data.type        = ESPIA_CB_ACQ;
	finfo               = &cb_data.info.acq.req_finfo;
	finfo->buffer_nr    = ESPIA_ACQ_ANY;
	finfo->frame_nr     = ESPIA_ACQ_ANY;
	finfo->round_count  = ESPIA_ACQ_ANY;
	finfo->acq_frame_nr = ESPIA_ACQ_EACH;
	cb_data.timeout     = SCDXIPCI_BLOCK_FOREVER;
	priv_data.cb_funct  = lib_last_frame_cb;

	return lib_reg_gen_cb(dev_data, &cb_data, &priv_data, 
			  ETEST_CB_INTERN_ACTIVE, &dev_data->last_frame_cb);
}


/*--------------------------------------------------------------------------
 * buffer alloc/free/list
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_buffer_alloc( etest_t etest, int dev_nr, int nr_buffer,
                       int frames, int width, int height, int depth, 
                       int display)
{
	int ret, page_size_1, virt_buffers, virt_frames, frame_factor, i;
	int real_buffers, real_frames, real_frame_size, real_buffer_size; 
#ifdef WITH_IMAGE
	struct image_data *img_data;
#endif // WITH_IMAGE
	struct espia_frame_dim *fdim;
	op_fn_data *op_data;
	struct buffer_data *bdata;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	virt_buffers   = nr_buffer;
	virt_frames    = frames;

	fdim = &dev_data->alloc_frame_dim;
	fdim->width    = width;
	fdim->height   = height;
	fdim->depth    = depth;
	dev_data->plugin_frame_dim = *fdim;

	real_buffers    = virt_buffers;
	real_frames     = virt_frames;
	real_frame_size = espia_frame_mem_size(fdim);
	if (virt_frames == 1) {
		page_size_1 = getpagesize() - 1;
		real_frame_size += page_size_1;
		real_frame_size &= ~page_size_1;
	}

	real_buffer_size = real_frame_size * real_frames;
	frame_factor = 1;
	if (real_buffer_size < ESPIA_TEST_MIN_BUFFER_SIZE) {
		frame_factor = ESPIA_TEST_MIN_BUFFER_SIZE / real_buffer_size;
		real_frames *= frame_factor;
		real_buffers += frame_factor - 1;
		virt_buffers = real_buffers;  /* The real virtual buffer number */
		real_buffers /= frame_factor;
	}

	ret = espia_buffer_alloc(dev_data->dev, real_buffers, 
				 real_frames, real_frame_size);
	if (ret != ESPIA_OK)
		return ret;

	bdata = (struct buffer_data *) calloc(virt_buffers, sizeof(*bdata));
	if (bdata == NULL) {
		espia_buffer_free(dev_data->dev);
		return ESPIA_TEST_ERR_NOMEM;
	}

	dev_data->buff_data = bdata;
	dev_data->nr_buffer = virt_buffers;
        dev_data->buffer_frames = virt_frames;
	dev_data->real_frame_factor = frame_factor;
	dev_data->real_frame_size = real_frame_size;

	op_data = &dev_data->post_op_fn_data;
	op_data->frame_dim = *fdim;

#ifdef WITH_IMAGE
	for (i = 0; i < virt_buffers; i++) {
		img_data = image_data_from_buffer_nr(dev_data, i);
		image_data_init(img_data);
	}
#endif // WITH_IMAGE

	ret = lib_last_frame_cb_install(dev_data);
	if (ret < 0)
		goto out;

	if (!display)
		return ESPIA_OK;

#ifdef WITH_IMAGE
	for (i = 0; i < virt_buffers; i++) {
		ret = lib_buffer_img( etest, dev_nr, i );
		if (ret < 0)
			goto out;
	}
#else // WITH_IMAGE
	ret = ESPIA_TEST_ERR_IMGINIT;
#endif // WITH_IMAGE

 out:
	if (ret < 0)
		lib_buffer_free(etest, dev_nr);

	return (ret < 0) ? ret : ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_buffer_free( etest_t etest, int dev_nr )
{
	struct device_data *dev_data;
	int ret, cb_nr;


	get_check_dev_data(dev_data, etest, dev_nr);

	lib_roi_acc_buffer_del(dev_data);

#ifdef WITH_IMAGE
	ret = lib_buffer_img_del_all(etest, dev_nr);
	if (ret != ESPIA_OK)
		return ret;
#endif // WITH_IMAGE

	cb_nr = dev_data->last_frame_cb;
	if (!scdxipci_is(INVALID, cb_nr))
		lib_unreg_gen_cb(dev_data, cb_nr, ETEST_CB_INTERN, 
			     &dev_data->last_frame_cb);

	if (dev_data->buff_data != NULL) {
		free(dev_data->buff_data);
		dev_data->buff_data = NULL;
	}

	ret = espia_buffer_free(dev_data->dev);
	if (ret != ESPIA_OK)
		return ret;

	dev_data->nr_buffer = 0;

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_get_buffer_data( etest_t etest, int dev_nr, int *nr_buffer,
                         struct espia_frame_dim *fdim )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	*nr_buffer = dev_data->nr_buffer;
	memcpy( fdim, &dev_data->alloc_frame_dim, sizeof(struct espia_frame_dim) );

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_virt_buffer_info( etest_t etest, int dev_nr, int nr, 
                          struct img_buffer_info *binfo )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	if( (nr < 0) || (nr >= dev_data->nr_buffer) )
		return ESPIA_TEST_ERR_BUFFNUM;

	return virt_buffer_info(dev_data, nr, binfo);
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_buffer_save_edf_header( FILE *fout, 
			   unsigned long frame_nr,
			   struct espia_frame_dim *fdim, 
			   struct img_frame_info *finfo,
			   struct device_data *dev_data )
{
	time_t ctime_now;
	struct timeval tod_now;
	char time_str[64], buffer[EDF_HEADER_BUFFER_LEN], *p;
	int i, depth, width, height, len, rem;
	size_t ret;

	time(&ctime_now);
	gettimeofday(&tod_now, NULL);

	ctime_r(&ctime_now, time_str);
	time_str[strlen(time_str) - 1] = 0;

	depth  = fdim->depth;
	width  = fdim->width;
	height = fdim->height;

	p = buffer;
	p += sprintf(p, "{\n");
	p += sprintf(p, "HeaderID = EH:%06lu:000000:000000 ;\n", frame_nr);
	p += sprintf(p, "Image = %lu ;\n", frame_nr);
	p += sprintf(p, "ByteOrder = LowByteFirst ;\n");
	p += sprintf(p, "DataType = %s ;\n", 
		     (depth == 1) ? "UnsignedByte" :
		     ((depth == 2) ? "UnsignedShort" : "UnsignedLong"));
	p += sprintf(p, "Size = %ld ;\n", espia_frame_mem_size(fdim));
	p += sprintf(p, "Dim_1 = %d ;\n", width);
	p += sprintf(p, "Dim_2 = %d ;\n", height);

	p += sprintf(p, "time = %s ;\n", time_str);
	p += sprintf(p, "time_of_day = %ld.%06ld ;\n", 
		     tod_now.tv_sec, tod_now.tv_usec);
	if (finfo != NULL)
		p += sprintf(p, "time_of_frame = %.6f ;\n", 
			     finfo->time_us * 1e-6);
	edf_str_lock(dev_data);
	if( (NULL != dev_data->edf_usr_str) && (strlen(dev_data->edf_usr_str) 
	                      < (size_t)(buffer + EDF_HEADER_BUFFER_LEN - p)) )
		p += sprintf(p, "%s", dev_data->edf_usr_str);
	edf_str_unlock(dev_data);

	i = p - buffer;
	len = i;
	rem = len % EDF_HEADER_LEN;
	if (rem > 0)
		len += EDF_HEADER_LEN - rem;
	p += sprintf(p, "%*s}\n", len - (i + 2), "");
	len = p - buffer;

	ret = fwrite(buffer, len, 1, fout);
	if (ret != 1)
		return -ENOSPC;
	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_buffer_save_file_open( struct buffer_save_data *bsd )
{
	char *fname, *prefix, *name_suffix, *suffix, *error;
	FILE *fout;
	int flags, fd, overwrite, idx, format, ret;
	struct device_data *dev_data = bsd->dev_data;

	ret = buffer_save_lock(dev_data);
	if (ret < 0)
		return -1;

	fout = bsd->fout;
	if (fout != NULL)
		goto unlock;

	prefix = bsd->prefix;
	name_suffix = bsd->suffix;
	idx = bsd->idx;
	format = bsd->format;
	overwrite = bsd->overwrite;

	switch (format) {
	case FMT_RAW:
		suffix = "raw"; break;
	case FMT_EDF:
		suffix = "edf"; break;
	default:
		ret = -EINVAL;
		goto unlock;
	}

	fname = bsd->file_name;
	sprintf(fname, "%s%04d%s.%s", prefix, idx, name_suffix, suffix);

	flags  = O_CREAT | O_WRONLY | O_LARGEFILE;
	flags |= overwrite ? O_TRUNC : O_APPEND;
	fd = open(fname, flags, FILE_SAVE_MODE);
	if (fd >= 0)
		fout = fdopen(fd, overwrite ? "wb" : "ab");
	if (fout == NULL) {
		error = strerror(errno);
		lib_log_err("Error opening %s: %s\n", fname, error);
		fname[0] = 0;
		ret = -1;
		goto unlock;
	}
	setbuf(fout, NULL);
	bsd->fout = fout;

 unlock:
	buffer_save_unlock(dev_data);

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_buffer_save_file_close( struct buffer_save_data *bsd )
{
	FILE *fout;
	int ret;
	struct device_data *dev_data = bsd->dev_data;


	ret = buffer_save_lock(dev_data);
	if (ret < 0)
		return -1;
	
	fout = bsd->fout;
	if (fout == NULL)
		goto unlock;

	ret = fclose(fout);
	if (ret != 0) {
		char *error;
		error = strerror(errno);
		lib_log_err("Error closing file %s: %s\n", 
			    bsd->file_name, error);
		ret = -1;
		goto unlock;
	}
	bsd->fout = NULL;
	bsd->file_name[0] = 0;
	bsd->idx++;

 unlock:
	buffer_save_unlock(dev_data);

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_buffer_save_file( struct buffer_save_data *bsd,
		     unsigned long buffer_nr, struct img_frame_info *finfo )
{
	struct device_data *dev_data = bsd->dev_data;
	char *fname, *ptr, *error, print_fname[FILE_FULL_NAME_LEN];
	FILE *fout;
	struct img_buffer_info binfo;
	struct espia_frame_dim *fdim;
	unsigned long frame, len, frame_nr;
	int ret, format;

	ret = buffer_save_lock(dev_data);
	if (ret < 0)
		return -1;

	ret = buffer_info_from_nr(dev_data, buffer_nr, &binfo);
	if (ret < 0)
		goto unlock;

	format = bsd->format;
	fname = bsd->file_name;

	fdim = frame_dim_from_buffer_nr(dev_data, buffer_nr);

	ptr = binfo.ptr;
	for (frame = 1; frame <= binfo.nr_frames; frame++) {
		fout = bsd->fout;
		if (fout == NULL) {
			ret = lib_buffer_save_file_open(bsd);
			if (ret < 0)
				goto unlock;
			fout = bsd->fout;
		}
		frame_nr = bsd->file_frames + 1;

		if (bsd->print_name)
			strcpy(print_fname, fname);

		switch (format) {
		case FMT_EDF:
			ret = lib_buffer_save_edf_header( fout,
					      frame_nr, fdim, finfo, dev_data );
			if (ret < 0)
				goto unlock;

		case FMT_RAW:
			len = espia_frame_mem_size(fdim);
			ret = fwrite(ptr, len, 1, fout);
			if (ret != 1) {
				error = strerror(errno);
				lib_log_err("Error writing frame %lu "
					    "(%ld bytes) on %s: %s\n", 
					    frame_nr, len, fname, error);
				ret = -1;
				goto unlock;
			}
		}
		ptr += binfo.frame_size;

		bsd->file_frames++;
		bsd->file_frames %= bsd->tot_file_frames;
		if (bsd->file_frames == 0) {
			ret = lib_buffer_save_file_close(bsd);
			if (ret < 0)
				goto unlock;
		}
	}

	if (bsd->print_name) {
		lib_log_info("Buffer #%ld of dev. %d saved on %s\n", 
			     buffer_nr, dev_data->dev_nr, print_fname);
	}

 unlock:
	buffer_save_unlock(dev_data);
	
	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_buffer_save_cb( etest_t etest, int dev_nr, 
                        struct espia_cb_data *cb_data, 
                        void *user_data )
{
	struct device_data *dev_data;
	struct buffer_save_data *bsd;
	struct img_frame_info *finfo = &cb_data->info.acq.cb_finfo;
	int ret;

	get_check_dev_data(dev_data, etest, dev_nr);

	bsd = &dev_data->buff_save_data;

	if (finished_espia_frame_info(finfo, cb_data->ret))
		return ESPIA_OK;

	ret = lib_buffer_save_file( bsd, finfo->buffer_nr, finfo );

	last_frame_update(&bsd->last_frame, finfo);
	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_buffer_save_cb_install( struct buffer_save_data *bsd )
{
	struct device_data *dev_data = bsd->dev_data;
	struct espia_cb_data cb_data;
	struct img_frame_info *finfo = &cb_data.info.acq.req_finfo;
	struct cb_priv_data priv_data;
	int prev_idx, ret;

	ret = buffer_save_lock(dev_data);
	if (ret < 0)
		return ret;

	ret = lib_buffer_save_file_close( &dev_data->buff_save_data ); /* bsd??? */
	if (ret < 0)
		goto unlock;

	prev_idx = dev_data->buff_save_data.idx;
	dev_data->buff_save_data = *bsd;
	if (dev_data->buff_save_data.idx < 0)
		dev_data->buff_save_data.idx = prev_idx;

	if (auto_save_active(dev_data))
		goto unlock;

	init_cb_priv_data(&priv_data);

	cb_data.type	    = ESPIA_CB_ACQ;
	finfo->buffer_nr    = ESPIA_ACQ_EACH;
	finfo->frame_nr	    = 0;
	finfo->round_count  = ESPIA_ACQ_ANY;
	finfo->acq_frame_nr = 0;
	cb_data.timeout	    = SCDXIPCI_BLOCK_FOREVER;
	priv_data.cb_funct  = lib_buffer_save_cb;

	ret = lib_reg_gen_cb(dev_data, &cb_data, &priv_data, 
			 ETEST_CB_INTERN_ACTIVE, &dev_data->buff_save_cb);

 unlock:
	buffer_save_unlock(dev_data);
	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_buffer_save_cb_uninstall( struct device_data *dev_data )
{
	int cb_nr, ret;


	if ( NULL == dev_data )
		return ESPIA_TEST_ERR_DEVNR;  /* ??? */

	if ( dev_data->dev == ESPIA_DEV_INVAL )
		return ESPIA_TEST_ERR_NOOPEN;

	if (!auto_save_active(dev_data))
		return ESPIA_OK;	

	cb_nr = dev_data->buff_save_cb;

	ret = lib_unreg_gen_cb(dev_data, cb_nr, ETEST_CB_INTERN, 
			    &dev_data->buff_save_cb);
	if (ret < 0)
		return ret;

	ret = buffer_save_lock(dev_data);
	if (ret < 0)
		return ret;

	ret = lib_buffer_save_file_close(&dev_data->buff_save_data);
	if (ret < 0)
		goto unlock;

 unlock:
	buffer_save_unlock(dev_data);
	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_buffer_save_cb_get( etest_t etest, int dev_nr, int *auto_save, 
                             char* prefix, int* idx, int* format, 
                             char* file_name, int* tot_file_frames, 
                             int* file_frames, int* overwrite )
{
	int ret;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	ret = buffer_save_lock(dev_data);
	if (ret < 0)
		return ret;

	*auto_save = auto_save_active(dev_data);

	strncpy(prefix,    dev_data->buff_save_data.prefix, FILE_PREFIX_NAME_LEN);
	*idx =             dev_data->buff_save_data.idx;
	*format =          dev_data->buff_save_data.format;
	strncpy(file_name, dev_data->buff_save_data.file_name, FILE_FULL_NAME_LEN);
	*tot_file_frames = dev_data->buff_save_data.tot_file_frames;
	*file_frames     = dev_data->buff_save_data.file_frames;
	*overwrite       = dev_data->buff_save_data.overwrite;

	buffer_save_unlock(dev_data);

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_buffer_save( etest_t etest, int dev_nr, long buffer_nr, 
                     char* fname_pr, int fname_idx, int format, 
                     int file_frames, int overwrite )
{
	struct buffer_save_data bsd;
	int ret=0, auto_save, nonb_save;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	auto_save = buffer_is_live(buffer_nr);
	nonb_save = (-2 == buffer_nr);

	if( auto_save && (strlen(fname_pr) == 0) ) {
		return lib_buffer_save_cb_uninstall( dev_data );
	}

	bsd.dev_data = dev_data;
	strncpy(bsd.prefix, fname_pr, FILE_PREFIX_NAME_LEN);
	bsd.idx = fname_idx;
	bsd.suffix[0] = 0;
	bsd.format = format;
	bsd.file_name[0] = 0;
	bsd.tot_file_frames = file_frames;
	if (bsd.tot_file_frames <= 0)
		return -EINVAL;
	bsd.overwrite = overwrite;
	bsd.print_name = !auto_save;
	bsd.fout = NULL;
	bsd.file_frames = 0;
	last_frame_init(&bsd.last_frame);

	if (auto_save)
		ret = lib_buffer_save_cb_install( &bsd );
	else if( nonb_save ) {
		if (auto_save_active(dev_data))
			return ESPIA_TEST_ERR_AUTOACT;

		ret = lib_nonblock_save_init( etest, dev_nr, &bsd,
		                                     MAX_NUM_PARALLEL_THREADS );
	} else {
		struct img_frame_info finfo;

		finfo.buffer_nr    =  buffer_nr;
		finfo.frame_nr     =  0;  /* ??? */
		finfo.round_count  = -1;
		finfo.acq_frame_nr = -1;

		espia_get_frame(dev_data->dev, &finfo , 0);
		ret = lib_buffer_save_file( &bsd, buffer_nr, &finfo/*NULL*/ );
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * buffer clear
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_buffer_clear( etest_t etest, int dev_nr, int buffer_nr )
{
	struct img_buffer_info binfo;
#ifdef WITH_IMAGE
	struct image_data *img_data;
#endif // WITH_IMAGE
	int first, last, len, ret = 0;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	if (buffer_is_live(buffer_nr)) {
		first = 0;
		last = dev_data->nr_buffer - 1;
	} else {
		first = last = buffer_nr;
	}

	for (buffer_nr = first; buffer_nr <= last; buffer_nr++) {
		ret = buffer_info_from_nr(dev_data, buffer_nr, &binfo);
		if (ret < 0)
			break;

		len = binfo.nr_frames * binfo.frame_size;
		memset(binfo.ptr, 0, len);

#ifdef WITH_IMAGE
		get_image_data_check(img_data, dev_data, buffer_nr);
		if (image_active(img_data))
			image_update(img_data->img);
#endif // WITH_IMAGE
	}

#ifdef WITH_IMAGE
	if (!buffer_is_roi_acc(first)) {
		get_image_data_check(img_data, dev_data, (int) SCDXIPCI_ANY);
		if (image_active(img_data))
			image_update(img_data->img);
	}
#endif // WITH_IMAGE

	return ret;
}


#ifdef WITH_IMAGE

/*--------------------------------------------------------------------------
 * generic image 
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_gen_img_cb( etest_t etest, int dev_nr, 
                    struct espia_cb_data *cb_data, 
                    void *user_data )
{
	struct device_data *dev_data;
	struct cb_priv_data *priv_data = priv_data_from_cb_data(cb_data);
	struct image_data *img_data = priv_data->img_data;
	struct img_frame_info *finfo = &cb_data->info.acq.cb_finfo;


	get_check_dev_data(dev_data, etest, dev_nr);

	if (finished_espia_frame_info(finfo, cb_data->ret))
		return ESPIA_OK;

	if (img_data->cb_funct == NULL) {
		image_update(img_data->img);
		return ESPIA_OK;
	}

	return img_data->cb_funct(dev_data, img_data, cb_data);
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
void lib_gen_img_destroy(void *data)
{
	struct image_data *img_data = (struct image_data *) data;
#if 0  /* CLI context is not in the library */
	app_context_t context = img_data->context;
#endif /* 0 */
	struct device_data *dev_data = img_data->dev_data;

	lib_unreg_gen_cb( dev_data, img_data->cb_nr, ETEST_CB_INTERN,
		     &img_data->cb_nr );

	if (img_data->del_funct != NULL)
		img_data->del_funct( dev_data, img_data );

	img_data->img = IMAGE_INVALID;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_gen_img_add(struct device_data *dev_data, int buffer_nr, 
                    img_cb *cb_funct, struct img_frame_info *req_finfo, 
                    img_del *del_funct)
{
	int ret;
	struct image_data *img_data;
	struct espia_cb_data cb_data;
	struct cb_priv_data priv_data;
	struct img_frame_info *cb_req_finfo = &cb_data.info.acq.req_finfo;
	struct img_buffer_info binfo;
	struct espia_frame_dim *fdim;
	char caption[BUFFER_IMG_NAME_LEN];

	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;  /* ??? */

	get_image_data_check(img_data, dev_data, buffer_nr);
	if (image_active(img_data))
		return ESPIA_TEST_ERR_IMGEXST;

#if 0  /* CLI context is not in the library */
	img_data->context   = context;
#endif /* 0 */
	img_data->dev_data  = dev_data;
	img_data->del_funct = del_funct;
	img_data->cb_funct  = cb_funct;

	buffer_img_name(caption, dev_data, buffer_nr);
	ret = image_create(&img_data->img, caption);
	if (ret != 0)
		return ESPIA_TEST_ERR_IMGCRT;

	ret = image_close_cb(img_data->img, lib_gen_img_destroy, img_data);
	if (ret != 0) {
		ret = ESPIA_TEST_ERR_IMGCRT;
		goto close_cb_error;
	}

	if (!buffer_is_live(buffer_nr)) {
		ret = buffer_info_from_nr(dev_data, buffer_nr, &binfo);
		if (ret < 0)
			goto binfo_error;
		fdim = frame_dim_from_buffer_nr(dev_data, buffer_nr);
		img_data->buffer_ptr = binfo.ptr;
		image_set_buffer(img_data->img, binfo.ptr, fdim->width, 
				 fdim->height * binfo.nr_frames, fdim->depth);
	} else
		img_data->buffer_ptr = NULL;

	init_cb_priv_data(&priv_data);

	cb_data.type		= ESPIA_CB_ACQ;
	*cb_req_finfo		= *req_finfo;
	cb_data.timeout		= SCDXIPCI_BLOCK_FOREVER;
	priv_data.cb_funct	= lib_gen_img_cb;
	priv_data.img_data	= img_data;

	ret = lib_reg_gen_cb( dev_data, &cb_data, &priv_data, 
	                      ETEST_CB_INTERN_ACTIVE, &img_data->cb_nr);
	if (ret < 0)
		goto reg_cb_error;
	
	return ESPIA_OK;

 reg_cb_error:
 binfo_error:
	image_close_cb(img_data->img, NULL, NULL);
 close_cb_error:
	image_destroy(img_data->img);
	img_data->img = IMAGE_INVALID;
	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_gen_img_del(struct device_data *dev_data, int buffer_nr)
{
	struct image_data *img_data;


	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;  /* ??? */

	get_image_data_check(img_data, dev_data, buffer_nr);
	if (!image_active(img_data))
		return ESPIA_TEST_ERR_IMGMISS;
	
	image_destroy(img_data->img);

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_image_active( etest_t etest, int dev_nr, int buffer_nr )
{
	struct image_data *img_data;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	get_image_data_check(img_data, dev_data, buffer_nr);

	return image_active(img_data);
}


/*--------------------------------------------------------------------------
 * buffer image alloc/update/destroy/del_all
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_buffer_img_cb(struct device_data *dev_data,
		  struct image_data *img_data, struct espia_cb_data *cb_data)
{
	struct img_frame_info *finfo = &cb_data->info.acq.cb_finfo;
	struct espia_frame_dim *fdim = &dev_data->plugin_frame_dim;
	unsigned long width, height, depth;
	void *buffer_ptr = finfo->buffer_ptr;

	if (buffer_ptr != img_data->buffer_ptr) {
		width  = fdim->width;
		height = fdim->height * dev_data->buffer_frames;
		depth  = fdim->depth;
		image_set_buffer(img_data->img, buffer_ptr, width, height, 
				 depth);
		img_data->buffer_ptr = buffer_ptr;
	} else {
		image_update(img_data->img);
	}
	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
void lib_buffer_img_destroy(struct device_data *dev_data,
			struct image_data *img_ata)
{
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_buffer_img( etest_t etest, int dev_nr, long buffer_nr )
{
	struct img_frame_info req_finfo;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	if (buffer_is_roi_acc(buffer_nr))
		return ESPIA_TEST_ERR_ROIACC;

	req_finfo.buffer_nr    = buffer_nr;
	req_finfo.frame_nr     = ESPIA_ACQ_ANY;
	req_finfo.round_count  = ESPIA_ACQ_ANY;
	req_finfo.acq_frame_nr = ESPIA_ACQ_ANY;

	return lib_gen_img_add(dev_data, buffer_nr, lib_buffer_img_cb, 
	                       &req_finfo, lib_buffer_img_destroy);
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_buffer_img_del( etest_t etest, int dev_nr, int buffer_nr )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return lib_gen_img_del(dev_data, buffer_nr);
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_buffer_img_del_all(etest_t etest, int dev_nr)
{
	int i;
	struct image_data *img_data;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	if (dev_data->buff_data == NULL)
		return ESPIA_OK;

	for (i = -1; i < dev_data->nr_buffer; i++) {
		img_data = image_data_from_buffer_nr(dev_data, i);
		if (image_active(img_data)) {
			lib_buffer_img_del(etest, dev_nr, i);
		}
	}

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * buffer image rates/norm
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_buffer_img_name( etest_t etest, int dev_nr, int buffer_nr,
			 char *buf_img_name )
{
	struct image_data *img_data;
	struct device_data *dev_data;

	get_check_dev_data(dev_data, etest, dev_nr);

	get_image_data_check(img_data, dev_data, buffer_nr);
	if (!image_active(img_data))
		return ESPIA_TEST_ERR_IMGMISS;

	buffer_img_name(buf_img_name, dev_data, buffer_nr);

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_buffer_img_rates( etest_t etest, int dev_nr, int buffer_nr,
                           float *upd_rate, float *ref_rate )
{
	struct image_data *img_data;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	get_image_data_check(img_data, dev_data, buffer_nr);
	if (!image_active(img_data))
		return ESPIA_TEST_ERR_IMGMISS;
	
	image_get_rates(img_data->img, upd_rate, ref_rate);

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_buffer_img_norm( etest_t etest, int dev_nr, int buffer_nr,
                          long *img_min_val, long *img_max_val, 
			  int *img_auto_range )
{
	int auto_range;
	struct image_data *img_data;
	unsigned long min_val, max_val;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	get_image_data_check(img_data, dev_data, buffer_nr);
	if (!image_active(img_data))
		return ESPIA_TEST_ERR_IMGMISS;
	
	image_get_norm(img_data->img, &min_val, &max_val, &auto_range);
	if (!scdxipci_is(REQUEST, *img_min_val))
		min_val    = *img_min_val;
	if (!scdxipci_is(REQUEST, *img_max_val))
		max_val    = *img_max_val;
	if (!scdxipci_is(REQUEST, *img_auto_range))
		auto_range = *img_auto_range;
	image_set_norm(img_data->img, min_val, max_val, auto_range);

	*img_min_val = min_val;
	*img_max_val = max_val;
	*img_auto_range = auto_range;

	return ESPIA_OK;
}

#endif // WITH_IMAGE

/*--------------------------------------------------------------------------
 * RoI set / reset
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_roi_set( etest_t etest, int dev_nr, unsigned long frame_width,
                 unsigned long frame_height, unsigned long roi_left,
                 unsigned long roi_top )
{
	struct espia_roi roi;
	struct espia_frame_dim frame_dim, *plugin_frame_dim;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);
	if (dev_data->nr_buffer == 0)
		return ESPIA_TEST_ERR_NOBUFFER;

	plugin_frame_dim = &dev_data->plugin_frame_dim;

	frame_dim.width  = frame_width;
	frame_dim.height = frame_height;
	frame_dim.depth  = plugin_frame_dim->depth;
	roi.left         = roi_left;
	roi.top          = roi_top;
	roi.width        = plugin_frame_dim->width;
	roi.height       = plugin_frame_dim->height;

	return lib_set_gen_sg(dev_data, ESPIA_SG_NORM, &frame_dim, &roi);
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_roi_reset( etest_t etest, int dev_nr )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return lib_set_gen_sg(dev_data, ESPIA_SG_NORM, &dev_data->plugin_frame_dim, 
	                      NULL);
}


/*--------------------------------------------------------------------------
 * set generic SG
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_set_gen_sg(struct device_data *dev_data, int img_config, 
	       struct espia_frame_dim *frame_dim, struct espia_roi *roi)
{
	struct scdxipci_sg_table *sg_list;
	int ret, rel_ret, nr_dev, i;

	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;

	ret = espia_create_sg(dev_data->dev, img_config, frame_dim, roi,
			      &sg_list, &nr_dev);
	if (ret < 0)
		return ret;

	for (i = 0; i < nr_dev; i++) {
		ret = espia_set_sg(dev_data->dev, i, &sg_list[i]);
		if (ret < 0)
			break;
	}

	rel_ret = espia_release_sg(dev_data->dev, sg_list, nr_dev);
	return (ret < 0) ? ret : rel_ret;
}


/*--------------------------------------------------------------------------
 * SG set
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_sg_set( etest_t etest, int dev_nr, unsigned long img_conf )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);
	if ( dev_data->nr_buffer == 0 )
		return ESPIA_TEST_ERR_NOBUFFER;

	return lib_set_gen_sg( dev_data, img_conf, &dev_data->alloc_frame_dim, NULL );
}


/*--------------------------------------------------------------------------
 * dev list set
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_dev_set( etest_t etest, int dev_nr, int dev_list[], int nr_dev )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return espia_set_dev(dev_data->dev, dev_list, nr_dev);
}


/*--------------------------------------------------------------------------
 * acq start/stop/status
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_start_acq( etest_t etest, int dev_nr, int start_buffer, 
                    int nr_frames, long timeout )
{
	int ret;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	ret = lib_roi_acc_buffer_init(dev_data, nr_frames);
	if (ret < 0)
		return ret;

	last_frame_reset(&dev_data->last_frame);
	last_frame_reset(&dev_data->buff_save_data.last_frame);

	ret = espia_start_acq(dev_data->dev, start_buffer, nr_frames, timeout);
	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_stop_acq( etest_t etest, int dev_nr )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return espia_stop_acq(dev_data->dev);
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_acq_status( etest_t etest, int dev_nr,
                    unsigned long *acq_run_nr, 
                    int *acq_active, int *auto_save_active,
                    struct img_frame_info *last_acq_frame, 
                    struct img_frame_info *last_saved_frame )
{
	int ret;
	struct buffer_save_data *bsd;
	struct device_data *dev_data;


	*acq_run_nr = *acq_active = *auto_save_active = 0;
	memset(last_acq_frame, 0, sizeof(struct img_frame_info));
	memset(last_saved_frame, 0, sizeof(struct img_frame_info));

	get_check_dev_data(dev_data, etest, dev_nr);

	ret = espia_acq_active(dev_data->dev, acq_run_nr);
	if (ret < 0)
		return ret;

	*acq_active = (ret > 0);
	if( (! *acq_active) && (! *acq_run_nr) )
		return ESPIA_OK;

	last_frame_get(&dev_data->last_frame, last_acq_frame);
		
	if( !(*auto_save_active = auto_save_active(dev_data)) )
		return ESPIA_OK;

	bsd = &dev_data->buff_save_data;
	last_frame_get(&bsd->last_frame, last_saved_frame);

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_auto_save_active( etest_t etest, int dev_nr )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return 	auto_save_active(dev_data);
}


/*--------------------------------------------------------------------------
 * callback helpers & function
 *--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------
 * generic callback function
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_virt_acq_cb( etest_t etest, int dev_nr, struct espia_cb_data *cb_data, 
                    void *user_data )
{
	struct device_data *dev_data;
	struct cb_priv_data *priv_data = priv_data_from_cb_data(cb_data);
	struct img_frame_info *cb_finfo, real_cb_finfo;
	struct img_frame_info *req_finfo, real_req_finfo;
	gen_cb *cb_funct = priv_data->cb_funct;
	int finished, ret = 0;
	unsigned long good_frame_nr = SCDXIPCI_ANY;

	get_check_dev_data(dev_data, etest, dev_nr);

	cb_finfo = &cb_data->info.acq.cb_finfo;
	req_finfo = &cb_data->info.acq.req_finfo;
	real_cb_finfo = *cb_finfo;

	finished = finished_img_frame_info(cb_finfo);
	if (!finished) {
		virt_frame_info(dev_data, cb_finfo);
		good_frame_nr = req_finfo->frame_nr;
	}

	if (finished) {
		// must inform that acq. was aborted
	} else if (req_finfo->buffer_nr == ESPIA_ACQ_EACH) {
		good_frame_nr = dev_data->buffer_frames - 1;
	} else if (scdxipci_is(ANY, req_finfo->buffer_nr)) {
		// check frame below
	} else if (cb_finfo->buffer_nr != req_finfo->buffer_nr) {
		goto out;
	}

	if (!scdxipci_is(ANY, good_frame_nr) && 
	    !scdxipci_is(ANY, cb_finfo->frame_nr) && 
	    (cb_finfo->frame_nr != good_frame_nr))
		goto out;

	real_req_finfo = *req_finfo;
	*req_finfo = priv_data->req_finfo;

	ret = cb_funct(etest, dev_nr, cb_data, user_data);

	*req_finfo = real_req_finfo;

 out:
	if (!finished)
		*cb_finfo = real_cb_finfo;

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_gen_cb_funct(struct espia_cb_data *cb_data)
{
	struct cb_priv_data *priv_data = priv_data_from_cb_data(cb_data);
#if 0  /* CLI context is not in the library */
	app_context_t context = priv_data->context;
#endif /* 0 */
	char *type;
	struct device_data *dev_data = priv_data->dev_data;
	gen_cb *cb_funct = priv_data->cb_funct;
	int ret = cb_data->ret;
	etest_t etest = (etest_t) dev_data->lib_data;
	int dev_nr=dev_data->dev_nr;

	if (ret >= 0) {
		if (cb_needs_virtual_conv(dev_data, cb_data))
			cb_funct = &lib_virt_acq_cb;
		return cb_funct(etest, dev_nr, cb_data, priv_data->user_data);
	}

	switch (cb_data->type) {
	case ESPIA_CB_ACQ:	  type = "Acq.";       break;
	case ESPIA_CB_CCD_STATUS: type = "CCD Status"; break;
	case ESPIA_CB_SERIAL_TX:  type = "Ser. Tx";    break;
	case ESPIA_CB_SERIAL_RX:  type = "Ser. Rx";    break;
	default:		  type = "Unknown";    break;
	}
	lib_log_err("\n\n%s callback #%d on dev. #%d:\n",
		    type, cb_data->cb_nr, dev_data->dev_nr);

	if ((ret == SCDXIPCI_ERR_TIMEOUT) && !priv_data->filter_tout)
		lib_log_err("  Timeout!\n");
	else 
		lib_log_err("  Error %d (0x%x): %s\n", ret, ret,
			    espia_test_strerror(cb_data->ret));

	if (espia_fatal_error(cb_data->ret)) {
		lib_log_err("  Aborting!\n");
		return ESPIA_ERR_ABORT;
	}

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * callback register/unregister/unregister_all/list
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int check_real_acq_cb(struct device_data *dev_data, 
		      struct espia_cb_data *cb_data)
{
	struct img_frame_info *req_finfo = &cb_data->info.acq.req_finfo;
	struct cb_priv_data *priv_data = priv_data_from_cb_data(cb_data);
	unsigned long virt_buffer_nr = req_finfo->buffer_nr;

	priv_data->req_finfo = *req_finfo;

	if (virt_buffer_nr == ESPIA_ACQ_EACH) {
		req_finfo->buffer_nr    = ESPIA_ACQ_ANY;
		req_finfo->acq_frame_nr = ESPIA_ACQ_EACH;
	} else if (!scdxipci_is(ANY, virt_buffer_nr)) {
		req_finfo->buffer_nr    = real_buffer_nr(dev_data, 
							 virt_buffer_nr, 0);
	}

	// we cannot fix a particular buffer frame_nr on a single cb
	req_finfo->frame_nr = ESPIA_ACQ_ANY;

	return 0;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_reg_gen_cb(struct device_data *dev_data, struct espia_cb_data *cb_data, 
                   struct cb_priv_data *priv_data, int user_act, int *cb_nr_ptr)
{
	int ret, len, cb_nr = (int) SCDXIPCI_INVALID;
	struct cb_priv_data **aux, *real_priv_data = NULL;


	if ( NULL == dev_data ) {
		ret = ESPIA_TEST_ERR_DEVNR;  /* ??? */
		goto out;
	}

	if ( dev_data->dev == ESPIA_DEV_INVAL ) {
		ret = ESPIA_TEST_ERR_NOOPEN;
		goto out;
	}

	if (priv_data->cb_funct == NULL) {
		ret = ESPIA_TEST_ERR_CBFUNCT;
		goto out;
	}

	len = sizeof(*priv_data);
	real_priv_data = (struct cb_priv_data *) malloc(len);
	if (real_priv_data == NULL) {
		ret = ESPIA_TEST_ERR_NOMEM;
		lib_log_err("Error allocating cb priv. data "
			    "(%d bytes)\n", len);
		goto out;
	}
        *real_priv_data = *priv_data;

	real_priv_data->dev_data = dev_data;
#if 0  /* CLI context is not in the library */
	real_priv_data->context  = context;
#endif /* 0 */
	real_priv_data->user_cb  = user_act & ETEST_CB_USER;
	cb_data->dev             = dev_data->dev;
	cb_data->data            = real_priv_data;
	cb_data->cb              = lib_gen_cb_funct;

	if (cb_needs_virtual_conv(dev_data, cb_data)) {
		ret = check_real_acq_cb(dev_data, cb_data);
		if (ret < 0)
			goto out;
	}

	ret = espia_register_callback(dev_data->dev, cb_data, &cb_nr);
	if (ret < 0)
		goto out;

	if (user_act & ETEST_CB_ACTIVE) {
		ret = espia_callback_active(dev_data->dev, cb_nr, 1);
		if (ret < 0)
			goto out;
	}

	if (cb_nr >= dev_data->nr_cb) {
		len = (cb_nr + 1) * sizeof(*dev_data->cbs);
		aux = (struct cb_priv_data **) realloc(dev_data->cbs, len);
		if (aux == NULL) {
			ret = ESPIA_TEST_ERR_NOMEM;
			goto out;
		}
		len -= (dev_data->nr_cb + 1) * sizeof(*dev_data->cbs);
		memset(&aux[dev_data->nr_cb], 0 , len);
		dev_data->cbs = aux;
		dev_data->nr_cb = cb_nr + 1;
	}
	dev_data->cbs[cb_nr] = real_priv_data;

	if (user_act & ETEST_CB_USER) {
		lib_log_info("Registered cb #%d\n", cb_nr);
	}

	ret = ESPIA_OK;
 out:
	if (ret < 0) {
		if (!scdxipci_is(INVALID, cb_nr))
			espia_unregister_callback(dev_data->dev, cb_nr);
		if (real_priv_data)
			free(real_priv_data);
	}

	if (cb_nr_ptr != NULL)
		*cb_nr_ptr = (ret >= 0) ? cb_nr : (int) SCDXIPCI_INVALID;

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_unreg_gen_cb( struct device_data *dev_data,
		      int cb_nr, int user_cb, int *cb_nr_ptr)
{
	struct cb_priv_data *priv_data;
	int ret;


	if ( NULL == dev_data )
		return ESPIA_TEST_ERR_DEVNR;  /* ??? */

	if ( dev_data->dev == ESPIA_DEV_INVAL )
		return ESPIA_TEST_ERR_NOOPEN;  /* ??? */

	priv_data = get_cb_nr(dev_data, cb_nr);
	if ((priv_data == NULL) || (user_cb && !priv_data->user_cb))
		return ESPIA_TEST_ERR_CBNR;
		
	ret = espia_unregister_callback(dev_data->dev, cb_nr);
	if (ret < 0)
		return ret;

	free(priv_data);
	dev_data->cbs[cb_nr] = NULL;
	if (cb_nr_ptr != NULL)
		*cb_nr_ptr = (int) SCDXIPCI_INVALID;

	if (user_cb)
		lib_log_info("Unregistered cb #%d for dev #%d\n", 
			     cb_nr, dev_data->dev_nr);

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_reg_acq_cb( etest_t etest, int dev_nr, long buffer_nr,
                    long frame_nr, long round_count, long acq_frame_nr,
                    long timeout, gen_cb cb_funct, 
                    void *user_data )
{
	struct espia_cb_data cb_data;
	struct cb_priv_data priv_data;
	struct img_frame_info *req_finfo = &cb_data.info.acq.req_finfo;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	init_cb_priv_data(&priv_data);

	cb_data.type		= ESPIA_CB_ACQ;
	req_finfo->buffer_nr	= buffer_nr;
	req_finfo->frame_nr     = frame_nr;
	req_finfo->round_count  = round_count;
	req_finfo->acq_frame_nr = acq_frame_nr;
	cb_data.timeout 	= timeout;
	priv_data.cb_funct	= cb_funct; /* was lib_acq_cb_funct */
	priv_data.user_data     = user_data;
		
	return lib_reg_gen_cb(dev_data, &cb_data, &priv_data, ETEST_CB_USER, 
	                      NULL);
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_reg_status_cb( etest_t etest, int dev_nr, long timeout, 
                       gen_cb cb_funct, void *user_data )
{
	struct espia_cb_data cb_data;
	struct cb_priv_data priv_data;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	init_cb_priv_data(&priv_data);

	cb_data.type        = ESPIA_CB_CCD_STATUS;
	cb_data.timeout     = timeout;
	priv_data.cb_funct  = cb_funct;  /* was lib_status_cb_funct */
	priv_data.user_data = user_data;
	return lib_reg_gen_cb(dev_data, &cb_data, &priv_data, ETEST_CB_USER, 
	                      NULL);
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_reg_ser_rx_cb( etest_t etest, int dev_nr, unsigned long buffer_len,
                       char* term, unsigned long term_bytes, long timeout,
                       int print_raw, int filter_tout, gen_cb cb_funct,
                       void *user_data )
{
	int cb_nr, ret;
	struct espia_cb_data cb_data, *cb_datap;
	struct espia_cb_serial *cb_serial = &cb_data.info.serial;
	struct cb_priv_data priv_data;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	for (cb_nr = 0; cb_nr < dev_data->nr_cb; cb_nr++) {
		ret = espia_get_callback(dev_data->dev, cb_nr, &cb_datap);
		if (ret < 0)
			return ret;
		if (cb_datap->type == ESPIA_CB_SERIAL_RX)
			return ESPIA_TEST_ERR_CBREG;
	}

	init_cb_priv_data(&priv_data);

	cb_data.type           = ESPIA_CB_SERIAL_RX;
	cb_serial->buffer      = NULL;
	cb_serial->buffer_len  = buffer_len;
	cb_serial->term        = term;
	cb_serial->term_len    = term_bytes;
	cb_data.timeout        = timeout;
	priv_data.print_raw    = print_raw;
	priv_data.filter_tout  = filter_tout;
	priv_data.cb_funct     = cb_funct;  /* was lib_ser_rx_cb_funct */
	priv_data.user_data    = user_data;

	return lib_reg_gen_cb(dev_data, &cb_data, &priv_data, ETEST_CB_USER, 
	                      NULL);
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_unreg_cb( etest_t etest, int dev_nr, int cb_nr )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return lib_unreg_gen_cb(dev_data, cb_nr, ETEST_CB_USER, NULL);
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_unreg_all_cb( etest_t etest, int dev_nr )
{
	int cb_nr;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	for (cb_nr = 0; cb_nr < dev_data->nr_cb; cb_nr++) {
		if ( is_user_cb(dev_data, cb_nr) )
			lib_unreg_gen_cb(dev_data, cb_nr, ETEST_CB_USER, NULL);
	}

	if (auto_save_active(dev_data))
		lib_buffer_save_cb_uninstall(dev_data);

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_cb_active( etest_t etest, int dev_nr, int cb_nr, int active )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	if (!is_user_cb(dev_data, cb_nr))
		return ESPIA_TEST_ERR_CBNR;
		
	return espia_callback_active(dev_data->dev, cb_nr, active);
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_get_num_cb( etest_t etest, int dev_nr, int *num_cb )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	*num_cb = dev_data->nr_cb;

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_is_user_cb( etest_t etest, int dev_nr, int cb_nr, int *user_cb )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	*user_cb = is_user_cb(dev_data, cb_nr);

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_espia_get_callback( etest_t etest, int dev_nr, int cb_nr, 
                            struct espia_cb_data *cb_data_ptr )
{
	int ret;
	struct espia_cb_data *cb_data;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	ret = espia_get_callback(dev_data->dev, cb_nr, &cb_data);
	if( ret >= 0 ) {
		memcpy( cb_data_ptr, cb_data, sizeof(struct espia_cb_data) );
		/* What about the pointer members? Do we need "deeper" copy? */
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * get frame / frame addr
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_get_frame( etest_t etest, int dev_nr, 
                    struct img_frame_info *cb_finfo, long timeout )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return espia_get_frame(dev_data->dev, cb_finfo, timeout);
}


/*--------------------------------------------------------------------------
 * CCD status
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_ccd_status( etest_t etest, int dev_nr, 
                    unsigned char *ccd_status, long timeout )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return 	espia_ccd_status(dev_data->dev, ccd_status, timeout);
}


/*--------------------------------------------------------------------------
 * RoI accumulation buffer
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_roi_acc_buffer_init( struct device_data *dev_data, int nr_frames )
{
	int len, create, update, ret;
	struct espia_frame_dim *fdim;
	struct roi_acc_buffer *roi_acc;
#ifdef WITH_IMAGE
	struct image_data *img_data;
#endif // WITH_IMAGE


	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;  /* ??? */

	roi_acc = &dev_data->roi_acc_buff;
#ifdef WITH_IMAGE
	img_data = &roi_acc->img_data;
#endif // WITH_IMAGE
	fdim = &dev_data->plugin_frame_dim;

	if (!img_roi_valid(&roi_acc->roi, fdim))
		return ESPIA_OK;

	create = ((roi_acc->nr_frames == 0) && (nr_frames > 0) &&
		  (nr_frames != roi_acc->real_nr_frames));
	update = ((roi_acc->nr_frames > 0) || 
		  ((nr_frames > 0) && 
		   (nr_frames == roi_acc->real_nr_frames)));
	if (!update && !create)
		return ESPIA_OK;

	if (create) {
		roi_acc->real_nr_frames = nr_frames;
		ret = lib_roi_acc_buffer_install(dev_data);
		if (ret < 0)
			return ret;
	} else {
		len = (img_roi_size(&roi_acc->roi) * fdim->depth *
		       roi_acc->real_nr_frames);
		memset(roi_acc->buffer_ptr, 0, len);
#ifdef WITH_IMAGE
		image_update(img_data->img);
#endif // WITH_IMAGE
	}

	return ESPIA_OK;
}


#ifdef WITH_IMAGE

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
void lib_roi_acc_buffer_img_destroy( struct device_data *dev_data,
				struct image_data *img_data )
{
	struct roi_acc_buffer *roi_acc = &dev_data->roi_acc_buff;
	free(roi_acc->buffer_ptr);
	roi_acc->buffer_ptr = NULL;
	roi_acc->real_nr_frames = 0;
}

#endif // WITH_IMAGE

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_roi_acc_buffer_cb( struct device_data *dev_data,
		      struct image_data *img_data,
		      struct espia_cb_data *cb_data )
{
	struct img_frame_info *finfo = &cb_data->info.acq.cb_finfo;
	struct espia_frame_dim *fdim = &dev_data->plugin_frame_dim;
	struct roi_acc_buffer *roi_acc = &dev_data->roi_acc_buff;
	struct buffer_save_data buff_save;
	struct image_roi *roi = &roi_acc->roi;
	int roi_row_len, frm_row_len, roi_height, depth, row, ret;
	char *src_ptr, *dst_ptr;
	void *frm_ptr;

	if ((int) finfo->acq_frame_nr >= roi_acc->real_nr_frames)
		return ESPIA_OK;

	ret = virt_frame_address(dev_data, finfo->buffer_nr, 
				 finfo->frame_nr, &frm_ptr);
	if (ret < 0) {
		lib_log_err("Error %d in espia_frame_address(%ld,%ld):"
			    " %s\n", ret, finfo->buffer_nr, finfo->frame_nr,
			    espia_test_strerror(ret));
		return ret;
	}

	src_ptr = (char *) frm_ptr;
	depth = fdim->depth;
	roi_row_len = img_roi_width(roi) * depth;
	frm_row_len = fdim->width * depth;
	roi_height = img_roi_height(roi);
	dst_ptr = (char *) roi_acc->buffer_ptr;
	dst_ptr += roi_row_len * roi_height * finfo->acq_frame_nr;
	src_ptr += frm_row_len * roi->row_beg + (roi->col_beg * depth);
	for (row = 0; row < roi_height; row++) {
		memcpy(dst_ptr, src_ptr, roi_row_len);
		dst_ptr += roi_row_len;
		src_ptr += frm_row_len;
	}
#ifdef WITH_IMAGE
	image_update(img_data->img);
#endif // WITH_IMAGE

	if (((int) finfo->acq_frame_nr < roi_acc->real_nr_frames - 1) ||
	    !roi_acc_auto_save_active(dev_data))
		return ESPIA_OK;

	buff_save.dev_data = dev_data;
	strcpy(buff_save.prefix, roi_acc->file_prefix);
	buff_save.idx = roi_acc->file_idx++;
	buff_save.suffix[0] = 0;
	buff_save.format = FMT_EDF;
	buff_save.file_frames = 0;
	buff_save.tot_file_frames = 1;
	buff_save.overwrite = 0;
	buff_save.print_name = 0;
	buff_save.fout = NULL;

	return lib_buffer_save_file( &buff_save, ROI_ACC_BUFFER, NULL );
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_roi_acc_buffer_install( struct device_data *dev_data )
{
	int ret, len, nr_frames;
	struct img_frame_info req_finfo;
	struct espia_frame_dim *dev_fdim, *racc_fdim;
	struct roi_acc_buffer *roi_acc;
	struct image_roi *roi;


	if (!dev_data)
		return ESPIA_TEST_ERR_SELECT;  /* ??? */

	roi_acc = &dev_data->roi_acc_buff;
	nr_frames = roi_acc->real_nr_frames;
	lib_roi_acc_buffer_del( dev_data );
	roi_acc->real_nr_frames = nr_frames;

	roi = &roi_acc->roi;
	dev_fdim = &dev_data->plugin_frame_dim;
	len = img_roi_size(roi) * dev_fdim->depth;
	roi_acc->buffer_ptr = calloc(nr_frames, len);
	if (roi_acc->buffer_ptr == NULL)
		return ESPIA_TEST_ERR_NOMEM;

	racc_fdim = &roi_acc->frame_dim;
	racc_fdim->width  = img_roi_width(roi);
	racc_fdim->height = img_roi_height(roi) * nr_frames;
	racc_fdim->depth  = dev_fdim->depth;

#ifdef WITH_IMAGE
	req_finfo.buffer_nr	= ESPIA_ACQ_ANY;
	req_finfo.frame_nr	= ESPIA_ACQ_ANY;
	req_finfo.round_count	= ESPIA_ACQ_ANY;
	req_finfo.acq_frame_nr	= ESPIA_ACQ_EACH;
	ret = lib_gen_img_add(dev_data, ROI_ACC_BUFFER, lib_roi_acc_buffer_cb, 
			  &req_finfo, lib_roi_acc_buffer_img_destroy);
	if (ret < 0) {
		free(roi_acc->buffer_ptr);
		roi_acc->buffer_ptr = NULL;
		roi_acc->real_nr_frames = 0;
	}
	return ret;
#else // WITH_IMAGE
	return 0;
#endif // WITH_IMAGE
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_roi_acc_buffer_del(struct device_data *dev_data)
{
#ifdef WITH_IMAGE
	lib_gen_img_del(dev_data, ROI_ACC_BUFFER);
#endif // WITH_IMAGE
	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_roi_acc_buffer( etest_t etest, int dev_nr, int col_beg, int col_end,
                         int row_beg, int row_end, int nr_frames, 
                         char *file_prefix, int file_idx )
{
	int act, ret=0;
	struct image_roi *roi;
	struct roi_acc_buffer *roi_acc;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	roi_acc = &dev_data->roi_acc_buff;
	roi = &roi_acc->roi;
	roi->col_beg = col_beg;
	roi->col_end = col_end;
	roi->row_beg = row_beg;
	roi->row_end = row_end;
	roi_acc->nr_frames = nr_frames;
	strcpy(roi_acc->file_prefix, file_prefix);
	if (file_idx >= 0)
		roi_acc->file_idx = file_idx;
	act = img_roi_valid(roi, &dev_data->plugin_frame_dim);
	if (act && (nr_frames > 0)) {
		roi_acc->real_nr_frames = nr_frames;
		ret = lib_roi_acc_buffer_install(dev_data);
	} else if (!act) {
		lib_log_info("Invalid RoI, deactivating\n");
		ret = lib_roi_acc_buffer_del(dev_data);
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * FOCLA open / close
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_focla_open_dev( etest_t etest, int dev_nr )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return focla_open(dev_data->dev, &dev_data->focla);
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_focla_close_dev( etest_t etest, int dev_nr )
{
	int ret;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	if (dev_data->focla == FOCLA_DEV_INVAL)
		return FOCLA_OK;

	ret = focla_close(dev_data->focla);
	if (ret < 0)
		return ret;

	dev_data->focla = FOCLA_DEV_INVAL;

	return FOCLA_OK;
}


/*--------------------------------------------------------------------------
 * FOCLA reg read / write
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_focla_read( etest_t etest, int dev_nr, int reg_nr, 
                    unsigned char *old_val )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return focla_read_reg(dev_data->focla, reg_nr, old_val);
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_focla_write( etest_t etest, int dev_nr, int reg_nr, 
                     unsigned char new_val )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return focla_write_reg(dev_data->focla, reg_nr, new_val);
}


/*--------------------------------------------------------------------------
 * FOCLA param list 
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_focla_get_param_data( int *nr_param, struct espia_param *param_ptr )
{
	int ret;
	struct espia_param *fparam;


	ret = focla_get_param_data( nr_param, (param_ptr ? &fparam : NULL) );

	if( param_ptr ) {
		if( (ret >= 0) && (NULL != fparam) )
			memcpy( param_ptr, fparam, sizeof(struct espia_param) );
		else
			memset( param_ptr, 0, sizeof(struct espia_param) );
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * FOCLA param get / set
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_focla_get_param( etest_t etest, int dev_nr, int par_nr, int *old_val )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);
#if 0  /* Should we check this ??? */
	if (dev_data->focla == FOCLA_DEV_INVAL)
		return FOCLA_OK;
#endif /* 0 */
	return focla_get_param(dev_data->focla, par_nr, old_val);
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_focla_get_par( etest_t etest, int dev_nr, char *name, int *old_val )
{
	int ret, i, nr_param;
	struct espia_param *fparam;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	nr_param = -1;
	ret = focla_get_param_data(&nr_param, NULL);
	if (ret < 0)
		return ret;

	for (i = 0; i < nr_param; i++) {
		ret = focla_get_param_data(&i, &fparam);
		if (ret < 0)
			return ret;
		if (strcasecmp(fparam->name, name) == 0)
			break;
	}
	if (i == nr_param)
		return ESPIA_TEST_ERR_PARAM;

	ret = focla_get_param(dev_data->focla, i, old_val);

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_focla_set_par( etest_t etest, int dev_nr, char *name, 
                       int new_val, int *old_val )
{
	struct espia_param *fparam;
	int ret, unlock_ret, i, nr_param;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	nr_param = -1;
	ret = focla_get_param_data(&nr_param, NULL);
	if (ret < 0)
		return ret;

	for (i = 0; i < nr_param; i++) {
		ret = focla_get_param_data(&i, &fparam);
		if (ret < 0)
			return ret;
		if (strcasecmp(fparam->name, name) == 0)
			break;
	}
	if (i == nr_param)
		return ESPIA_TEST_ERR_PARAM;

	ret = focla_lock(dev_data->focla);
	if (ret < 0)
		return ret;
		
	ret = focla_get_param(dev_data->focla, i, old_val);
	if (ret < 0)
		goto unlock;
	
	ret = focla_set_param(dev_data->focla, i, new_val);

 unlock:
	unlock_ret = focla_unlock(dev_data->focla);
	return (ret < 0) ? ret : unlock_ret;
}


/*--------------------------------------------------------------------------
 * FOCLA signal list
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_focla_get_sig_data(int *nr_param, struct focla_signal *sig_ptr)
{
	int ret;
	struct focla_signal *fsig;


	ret = focla_get_sig_data( nr_param, (sig_ptr ? &fsig : NULL) );

	if( sig_ptr ) {
		if( (ret >= 0) && (NULL != fsig) )
			memcpy( sig_ptr, fsig, sizeof(struct focla_signal) );
		else
			memset( sig_ptr, 0, sizeof(struct focla_signal) );
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * FOCLA signal pulse start/stop/status
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int get_focla_sig(char *sig_name) 
{
	struct focla_signal *fsig;
	int ret, sig, nr_sig;

	nr_sig = -1;
	ret = focla_get_sig_data(&nr_sig, NULL);
	if (ret < 0)
		return ret;

	for (sig = 0; sig < nr_sig; sig++) {
		ret = focla_get_sig_data(&sig, &fsig);
		if (ret < 0)
			return ret;
		if (strcasecmp(fsig->name, sig_name) == 0)
			return sig;
	}

	return ESPIA_TEST_ERR_FOCLASIG;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_focla_pulse_start( etest_t etest, int dev_nr, int cam_nr, 
                           char *sig_name, int polarity, int width_us,
                           int delay_us, int nr_pulse )
{
	int width_arr[2] = {width_us, delay_us};
	int sig_nr, nr_stage = sizeof(width_arr) / sizeof(width_arr[0]);
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	sig_nr = get_focla_sig(sig_name);
	if (sig_nr < 0)
		return sig_nr;

	return focla_sig_pulse_start(dev_data->focla, cam_nr, sig_nr, 
				     polarity, width_arr, nr_stage, nr_pulse);
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_focla_pulse_stop( etest_t etest, int dev_nr, int cam_nr, 
                          char *sig_name )
{
	int sig_nr;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	sig_nr = get_focla_sig(sig_name);
	if (sig_nr < 0)
		return sig_nr;

	return focla_sig_pulse_stop(dev_data->focla, cam_nr, sig_nr);
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_focla_pulse_status( etest_t etest, int dev_nr, int cam_nr, 
                             char *sig_name, int *pulse_active, int *curr_pulse, 
                             int *curr_stage )
{
	int sig_nr;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	sig_nr = get_focla_sig(sig_name);
	if (sig_nr < 0)
		return sig_nr;
	return focla_sig_pulse_status(dev_data->focla, cam_nr, sig_nr, 
				      pulse_active, curr_pulse, curr_stage);
}


/*--------------------------------------------------------------------------
 * FOCLA serial read/write/flush
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 *     Notes : The buffer should be allocated before and must be at least   *
 *             *rd_bytes_p of size;                                         *
 ****************************************************************************/
int lib_focla_serial_read( etest_t etest, int dev_nr, char *in_buffer, 
                            unsigned long *rd_bytes_p, long timeout )
{
	struct device_data *dev_data;


	get_check_dev_data( dev_data, etest, dev_nr );

	return focla_ser_read( dev_data->focla, in_buffer, rd_bytes_p, 
	                       (unsigned long) timeout );
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_focla_serial_get_avail_bytes( etest_t etest, int dev_nr, 
                                      unsigned long *avail_bytes_p )
{
	struct device_data *dev_data;


	get_check_dev_data( dev_data, etest, dev_nr );

	*avail_bytes_p = 0;
	return focla_ser_read( dev_data->focla, NULL, avail_bytes_p, 0 );
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 *     Notes : The buffer should be allocated before and must be at least   *
 *             *rd_bytes_p of size;                                         *
 ****************************************************************************/
int lib_focla_serial_read_str( etest_t etest, int dev_nr, char *in_buffer,
                               unsigned long *rd_bytes_p, char *term,
                               unsigned long term_bytes, long timeout )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return focla_ser_read_str( dev_data->focla, in_buffer, rd_bytes_p, 
	                           term, term_bytes, (unsigned long) timeout );
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_focla_serial_write( etest_t etest, int dev_nr, char *out_buffer,
                            unsigned long *wr_bytes_p, unsigned long block_size,
                            unsigned long delay, int block )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return focla_ser_write( dev_data->focla, out_buffer, wr_bytes_p, 
	                        block_size, delay, block );
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_focla_serial_flush( etest_t etest, int dev_nr )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return focla_ser_flush(dev_data->focla);
}


/*--------------------------------------------------------------------------
 * get stats
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_get_stats( etest_t etest, int dev_nr, struct scdxipci_stats *stats )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	return scdxipci_get_stats(dev_data->dev, stats);
}


/*--------------------------------------------------------------------------
 * firmware list
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_espia_get_firmware_data(int *nr_param, 
			        struct espia_firmware *firmware_ptr)
{
	int ret;
	struct espia_firmware *firmware;


	ret = espia_get_firmware_data( nr_param, (firmware_ptr ? 
	                                                    &firmware : NULL) );

	if( firmware_ptr ) {
		if( (ret >= 0) && (NULL != firmware) )
			memcpy( firmware_ptr, firmware, 
			                        sizeof(struct espia_firmware) );
		else
			memset( firmware_ptr, 0, 
			                        sizeof(struct espia_firmware) );
	}

	return ret;
}


/*--------------------------------------------------------------------------
 * library versions
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_get_version( struct espia_lib_version *ver )
{
	return espia_lib_version(ver);
}


/*--------------------------------------------------------------------------*
 * New data retrieval functions
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN : etest_t etest, int dev_nr, int buf_nr, int frame_nr          *
 *             If buf_nr == -1 then frame_nr is absolute, otherwise it is   *
 *             in the buffer or if it is -1 then we want the whole buffer   *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_get_data( etest_t etest, int dev_nr, long buf_nr, long frame_nr, 
                   unsigned char **data_ptr, int *data_len
               /*, struct espia_frame_dim *frame_dim*/ )
{
	int ret, frame_size;
	struct img_buffer_info binfo;
	struct espia_frame_dim *fdim;
	struct device_data *dev_data;


	*data_ptr = NULL; *data_len = 0;

	get_check_dev_data(dev_data, etest, dev_nr);

	if( (buf_nr < -1) || (buf_nr >= dev_data->nr_buffer) )
		return ESPIA_TEST_ERR_BUFFNUM;
	else if( scdxipci_is(ANY, buf_nr) && scdxipci_is(ANY, frame_nr) )
		return ESPIA_TEST_ERR_ANY;
	
	/* AH: buffer_nr == -1 -> buffer image live -> will work */
	fdim = frame_dim_from_buffer_nr(dev_data, buf_nr);
	if( NULL == fdim )
		return ESPIA_TEST_ERR_INTERNAL;  /* this should not happen */
	frame_size = espia_frame_mem_size(fdim);
	
	if (!scdxipci_is(ANY, frame_nr))
	{
		ret = virt_frame_address(dev_data, buf_nr, frame_nr, 
		                         (void **) data_ptr);
		if( ret < 0 )
			return ret;
		*data_len = frame_size;
	}
	else  /* (frame_nr == ESPIA_ACQ_ANY) && (buf_nr != ESPIA_ACQ_ANY) */
	{
		ret = buffer_info_from_nr(dev_data, buf_nr, &binfo);
		if (ret < 0)
			return ret;
		*data_ptr = binfo.ptr;
		*data_len = binfo.nr_frames * frame_size;
	}

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN : etest_t etest, int dev_nr, int buf_nr, int frame_nr          *
 *             If buf_nr == -1 then frame_nr is absolute, otherwise it is   *
 *             in the buffer                                                *
 *       OUT : struct espia_frame_dim *frame_dim,                           *
 *             struct img_frame_info *frame_info                            *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_get_frame_info( etest_t etest, int dev_nr, int buf_nr, int frame_nr,
                         struct espia_frame_dim *frame_dim,
                         struct img_frame_info *frame_info )
{
	int ret, timeout;
	struct espia_frame_dim *fdim=NULL;
	struct device_data *dev_data;


	memset( frame_info, 0, sizeof(struct img_frame_info) );
	memset( frame_dim, 0, sizeof(struct espia_frame_dim) );

	get_check_dev_data(dev_data, etest, dev_nr);

/*	if( frame_nr < 0 ) ???
	{
		// Alejandro: get buffer dimensions ???
		return -1;
	}
	else */if( buf_nr < 0 )
	{
		frame_info->buffer_nr	 = buf_nr;  /* "Nr. of buffer [-1=Any]" */
		frame_info->frame_nr	 = -1;  /* "Frame idx in buffer [-1=Any]" */
		frame_info->round_count	 = -1;  /* "Nr. of (re)write times [-1=Any]" */
		frame_info->acq_frame_nr = frame_nr; /* "Frame idx in acq. [-1=Any]" */
	}
	else
	{
		frame_info->buffer_nr	 = buf_nr;  /* "Nr. of buffer [-1=Any]" */
		frame_info->frame_nr	 = frame_nr;  /* "Frame idx in buffer [-1=Any]" */
		frame_info->round_count	 = -1;  /* "Nr. of (re)write times [-1=Any]" */
		frame_info->acq_frame_nr = -1; /* "Frame idx in acq. [-1=Any]" */
	}
	timeout = 0; /* ??? "Timeout in us [0=NoBlock, -1=Forever]" */
	ret = lib_get_frame(etest, dev_nr, frame_info, timeout);
	if( ret < 0 )
		return ret;

	fdim = frame_dim_from_buffer_nr(dev_data, buf_nr);
	if( NULL == fdim )
		return ESPIA_TEST_ERR_INTERNAL;
	memcpy( frame_dim, fdim, sizeof(struct espia_frame_dim) );

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------*
 * AcqLib plugin interface
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_plugin_II_cb( etest_t etest, int dev_nr, 
                      struct espia_cb_data *cb_data, void *user_data )
{
	int i, ret, fret, acq_frame_nr=cb_data->info.acq.cb_finfo.acq_frame_nr;
	struct lib_plugins_data *plugins_data = 
	       (struct lib_plugins_data *) user_data;
	struct plugin_t *aplugin;
	struct espia_frame_dim *fdim;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	if( NULL == plugins_data ) return ESPIA_TEST_ERR_NOPLUGD;

	fdim = &dev_data->plugin_frame_dim;

	ret = ESPIA_OK;
	pthread_mutex_lock( &plugins_data->lock );
	for( i=0; i<plugins_data->num_plugins_II; i++ ) {
		aplugin = &plugins_data->plugin_list_II[i];
		if( aplugin->active && aplugin->fun ) {
			fret = aplugin->fun( acq_frame_nr,
					     fdim->depth, fdim->width, fdim->height, 
					     cb_data->info.acq.cb_finfo.buffer_ptr, 
					     aplugin->userData );
			if ((fret < 0) && (ret == ESPIA_OK))
				ret = fret;
		}
	}
	pthread_mutex_unlock( &plugins_data->lock );

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_reg_plugin( etest_t etest, int dev_nr, int plugin_type, 
                    void *fptr, void *userData, char *desc )
{
	int ret=ESPIA_OK;
	struct plugin_t *new_plugin;
	plugin_func *fplug = (plugin_func *) fptr;
	struct lib_plugins_data *plugins_data;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	if( (plugins_data=dev_data->plugins_data) == NULL ) {
		plugins_data = (struct lib_plugins_data *) 
		                    malloc( sizeof(struct lib_plugins_data) );
		if( NULL == plugins_data )
			return ESPIA_TEST_ERR_NOMEM;
		memset( plugins_data, 0, sizeof(struct lib_plugins_data) );
		if( pthread_mutex_init(&plugins_data->lock, NULL) ) {
			free( plugins_data );
			lib_log_err("Mutex initialization error");
			return ESPIA_TEST_ERR_INTERNAL;
		}
		plugins_data->acq_cb_nr = SCDXIPCI_INVALID;
		dev_data->plugins_data = plugins_data;
	}

	pthread_mutex_lock( &plugins_data->lock );
	switch( plugin_type ) {
		case 1 : {
			if( plugins_data->num_plugins_I >= MAX_NUM_PLUGINS_I ) {
				ret = ESPIA_TEST_ERR_MAXPLUG;
				break;
			}

			/* Register the plugins FPO */
			if( ! plugins_data->num_plugins_I ) {  /* ??? */
				dev_data->post_op_fn_data.data = plugins_data;
				ret = lib_frame_post_op( etest, dev_nr, 
				                               "ACQ_PLUGIN_I" );
				if( ret < 0 ) break;
			}

			/* Add the new plugin to plugins_data */
			new_plugin = &plugins_data->plugin_list_I[\
			                         plugins_data->num_plugins_I];
			new_plugin->fun = fplug;
			new_plugin->active = 1;
			new_plugin->userData = userData;
			if( desc ) {
				strncpy(new_plugin->desc, desc, PLUG_DESC_LEN-1);
				new_plugin->desc[PLUG_DESC_LEN-1] = '\0';
			}
			++plugins_data->num_plugins_I;
			break;
		}

		case 2 : {
			if( plugins_data->num_plugins_II >= MAX_NUM_PLUGINS_II ) {
				ret = ESPIA_TEST_ERR_MAXPLUG;
				break;
			}

			/* Register a CB for each frame */
			if( plugins_data->acq_cb_nr == SCDXIPCI_INVALID ) {
				int cb_nr;
				struct espia_cb_data cb_data;
				struct cb_priv_data priv_data;
				struct img_frame_info *req_finfo = 
				                    &cb_data.info.acq.req_finfo;

				init_cb_priv_data(&priv_data);
				cb_data.type		= ESPIA_CB_ACQ;
				req_finfo->buffer_nr	= ESPIA_ACQ_ANY;
				req_finfo->frame_nr     = ESPIA_ACQ_ANY;
				req_finfo->round_count  = ESPIA_ACQ_ANY;
				req_finfo->acq_frame_nr = ESPIA_ACQ_EACH;
				cb_data.timeout 	= SCDXIPCI_BLOCK_FOREVER;
				priv_data.cb_funct	= lib_plugin_II_cb;
				priv_data.user_data     = plugins_data;

				ret = lib_reg_gen_cb(dev_data, &cb_data, 
				                     &priv_data, 
						     ETEST_CB_INTERN/*_ACTIVE*/, 
						     &cb_nr);
				if( ret < 0 ) break;
				plugins_data->acq_cb_nr = cb_nr;
			}

			/* Add the new plugin to plugins_data */
			new_plugin = &plugins_data->plugin_list_II[\
			                        plugins_data->num_plugins_II];
			new_plugin->fun = fplug;
			new_plugin->active = 1;
			new_plugin->userData = userData;
			if( desc ) {
				strncpy(new_plugin->desc, desc, PLUG_DESC_LEN-1);
				new_plugin->desc[PLUG_DESC_LEN-1] = '\0';
			}
			++plugins_data->num_plugins_II;

			ret = espia_callback_active( dev_data->dev, 
					           plugins_data->acq_cb_nr, 1 );
			break;
		}
		
		default :
			ret = ESPIA_TEST_ERR_BADPAR;
			break;
	}
	pthread_mutex_unlock( &plugins_data->lock );

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_get_plugins_data( etest_t etest, int dev_nr, 
                          struct lib_plugins_data *plugins_data )
{
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	if( NULL == plugins_data )
		return ESPIA_TEST_ERR_NOPLUGD;

	if( dev_data->plugins_data ) {
		pthread_mutex_lock( &dev_data->plugins_data->lock );
		memcpy( plugins_data, dev_data->plugins_data, 
		        sizeof(struct lib_plugins_data) );
		pthread_mutex_unlock( &dev_data->plugins_data->lock );
	} else {
		memset( plugins_data, 0, sizeof(struct lib_plugins_data) );
	}

	return ESPIA_OK;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_plugin_active( etest_t etest, int dev_nr, int plugin_type,
                       int plugin_nr, int active )
{
	int ret;
	struct plugin_t *plugin;
	struct lib_plugins_data *plugins_data;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	if( NULL == (plugins_data=dev_data->plugins_data) )
		return ESPIA_TEST_ERR_NOPLUGD;

	--plugin_nr;

	pthread_mutex_lock( &dev_data->plugins_data->lock );
	switch( plugin_type ) {
		case 1 : {
			if( (plugin_nr < 0) || 
			    (plugin_nr >= dev_data->plugins_data->num_plugins_I) )
			{
				ret = ESPIA_TEST_ERR_INVPLUG;
				break;
			}

			plugin = &dev_data->plugins_data->plugin_list_I[plugin_nr];
			ret = plugin->active;
			plugin->active = active;
			break;
		} 

		case 2 : {
			int i, numact;

			if( (plugin_nr < 0) || 
			    (plugin_nr >= dev_data->plugins_data->num_plugins_II) )
			{
				ret = ESPIA_TEST_ERR_INVPLUG;
				break;
			}

			plugin = &dev_data->plugins_data->plugin_list_II[plugin_nr];
			ret = plugin->active;

			if( ret == active ) break;

			numact = 0;
			for( i=0; i<dev_data->plugins_data->num_plugins_II; i++ ) {
				if( dev_data->plugins_data->plugin_list_II[i].active )
					numact++;
			}

			plugin->active = active;

			if( active ) {
				if( ! numact )
					espia_callback_active( dev_data->dev, 
					           plugins_data->acq_cb_nr, 1 );
			} else {
				if( numact == 1 )
					espia_callback_active( dev_data->dev, 
					           plugins_data->acq_cb_nr, 0 );
			}
			break;
		} 

		default : {
			ret = ESPIA_TEST_ERR_BADPAR;
			break;
		}
	}
	pthread_mutex_unlock( &dev_data->plugins_data->lock );

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_plugin_delete( etest_t etest, int dev_nr, int plugin_type,
                       int plugin_nr )
{
	int ret=ESPIA_OK, i;
	struct lib_plugins_data *plugins_data;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	if( NULL == (plugins_data=dev_data->plugins_data) )
		return ESPIA_TEST_ERR_NOPLUGD;

	--plugin_nr;

	pthread_mutex_lock( &plugins_data->lock );
	switch( plugin_type ) {
		case 1 : {
			if( (plugin_nr < 0) || 
			    (plugin_nr >= dev_data->plugins_data->num_plugins_I) )
			{
				ret = ESPIA_TEST_ERR_INVPLUG;
				break;
			}

			for( i=plugin_nr+1; i<plugins_data->num_plugins_I; i++ ) {
				plugins_data->plugin_list_I[i-1] =
				                 plugins_data->plugin_list_I[i];
			}

			if( --plugins_data->num_plugins_I == 0 ) {
				/* Remove the FPO. If the buffer is not allocated ??? */
				ret = lib_frame_post_op_del( etest, dev_nr, 
				                               "ACQ_PLUGIN_I" );
			}

			break;
		}

		case 2 : {
			if( (plugin_nr < 0) || 
			    (plugin_nr >= dev_data->plugins_data->num_plugins_II) )
			{
				ret = ESPIA_TEST_ERR_INVPLUG;
				break;
			}

			for( i=plugin_nr+1; i<plugins_data->num_plugins_II; i++ ) {
				plugins_data->plugin_list_II[i-1] =
				                plugins_data->plugin_list_II[i];
			}

			if( --plugins_data->num_plugins_II == 0 ) {
				/* Remove the acq. callback. If the acq is not running ??? */
				ret = espia_unregister_callback(dev_data->dev, 
				                       plugins_data->acq_cb_nr);
				if( ret >= 0 )
					plugins_data->acq_cb_nr = SCDXIPCI_INVALID;
			}

			break;
		}

		default : {
			ret = ESPIA_TEST_ERR_BADPAR;
			break;
		}
	}
	pthread_mutex_unlock( &plugins_data->lock );

	return ret;
}


/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_plugin_output( etest_t etest, int dev_nr, int plugin_type,
	               int plugin_nr, int width, int height, int depth )
{
	int i;
#ifdef WITH_IMAGE
	struct image_data *img_data;
#endif // WITH_IMAGE
	struct espia_frame_dim fdim;
	struct roi_acc_buffer *roi_acc;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	fdim.width  = width;
	fdim.height = height;
	fdim.depth  = depth;

	/* Check if the new values are different from the old ones */
	if( (dev_data->plugin_frame_dim.width  == fdim.width) &&
	    (dev_data->plugin_frame_dim.height == fdim.height) &&
	    (dev_data->plugin_frame_dim.depth  == fdim.depth) )
		return ESPIA_OK;

	if( (width == 0) && (height == 0) && (depth == 0) ) 
		fdim = dev_data->alloc_frame_dim;
	else if( (width <= 0) || (height <= 0) || (depth <= 0) )
		return ESPIA_TEST_ERR_BADPAR;

	/* Check if the new frame is not bigger than the allocated */
	if( espia_frame_mem_size(&fdim)
	                   > espia_frame_mem_size(&dev_data->alloc_frame_dim) )
		return ESPIA_TEST_ERR_IMGSIZE;

	if( espia_acq_active(dev_data->dev, NULL) > 0 )
		return SCDXIPCI_ERR_RUNNING;

	/* Check if the new settings are compatible with the roi_acc */
	roi_acc = &dev_data->roi_acc_buff;
	if(roi_acc->real_nr_frames && (! img_roi_valid(&roi_acc->roi, &fdim))) {
		/* return ESPIA_ERR_ROI_IMGCFG instead of this below ??? */
		lib_log_err("Plugin output settings are incompatible with RoI. "
		            "Deactivating RoI accumulation...");
		lib_roi_acc_buffer_del( dev_data );
	}

	dev_data->plugin_frame_dim = fdim;

#ifdef WITH_IMAGE
	/* Destroy and re-create the active buffer images.
	   Start from -1 (buffer img live), roi acc done already */
	for( i=-1; i < dev_data->nr_buffer; i++ ) {
		img_data = image_data_from_buffer_nr(dev_data, i);
		if( image_active(img_data) ) {
			lib_buffer_img_del( etest, dev_nr, i );
			lib_buffer_img( etest, dev_nr, i );
		}
	}
#endif // WITH_IMAGE

	return ESPIA_OK;
}

/*--------------------------------------------------------------------------*
 * Custom EDF header functions
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_edf_usr_header( etest_t etest, int dev_nr, char *usr_str )
{
	regex_t re;
	int ret, l=0, need_nl;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	if( usr_str && ((l=strlen(usr_str)) > 0) ) {
		/* Check if user_str is in the right format */
#define RE_KEY  "[A-Za-z0-9_]+"
#define RE_VAL  "[^;]+"
#define RE_COMM "[^\n]*"
#define RE_STMT "(" RE_KEY "[ \t]*=[ \t]*" RE_VAL ")?;" RE_COMM
#define RE_LINE "(" RE_STMT ")?\n?"
		ret = regcomp(&re, "^(" RE_LINE ")*$", REG_EXTENDED|REG_NOSUB);
		if( ret != 0) {
			char re_err[128];

			regerror( ret, &re, re_err, 127 );
			lib_log_err( "Error compiling the EDF format regular "
			               "expression: %s\n", re_err );
			return ESPIA_TEST_ERR_INTERNAL;
		}
		ret = regexec( &re, usr_str, (size_t)0, NULL, 0 );
		regfree( &re );
		if( ret != 0 ) {
			return ESPIA_TEST_ERR_EDFFORM; 
		}
	}

	edf_str_lock(dev_data);

	if(dev_data->edf_usr_str) {
		free( dev_data->edf_usr_str );
		dev_data->edf_usr_str = NULL;
	}

	if (l == 0) {
		ret = ESPIA_OK;
		goto out;
	}

	need_nl = ( usr_str[l-1] != '\n' );
	if( need_nl )
		l++;

	dev_data->edf_usr_str = (char *) malloc(l+1);
	if (dev_data->edf_usr_str == NULL) {
		ret = ESPIA_TEST_ERR_NOMEM;
		goto out;
	}
			
	strcpy( dev_data->edf_usr_str, usr_str );
	if (need_nl) 
		strcat(dev_data->edf_usr_str, "\n");

	ret = ESPIA_OK;
out:
	edf_str_unlock(dev_data);

	return ret;
}


/*--------------------------------------------------------------------------*
 * Parallel saving functions
 *--------------------------------------------------------------------------*/

static int lib_thread_id_to_thread_num( struct thread_pool_t *thread_pool, 
                                        pthread_t thread_id )
{
	int i;

	if( NULL == thread_pool )
		return -1;

	for( i=0; i<thread_pool->num_threads; i++ ) {
		if( thread_pool->threads[i].thread_id == thread_id )
			return i;
	}
	return -1;
}


static void *lib_saving_thread( void *arg )
{
	int i, mode;
	pthread_t self_id=pthread_self();
	struct thread_info_t *self=NULL;
	struct thread_pool_t *thread_pool = (struct thread_pool_t *)arg;
	struct img_frame_info finfo;

	while( 1 ) {
		sem_wait(&thread_pool->job_sem);

		pthread_mutex_lock( &thread_pool->lock );

		i = lib_thread_id_to_thread_num(thread_pool, self_id);
		if( i < 0 ) break;  /* ??? */

		thread_pool->job_data.worker = i;

		self = &thread_pool->threads[i];

		/* Copy the job data */
		self->job_data = thread_pool->job_data;
/*		thread_pool->job_data.usr_string = NULL;  // Or strdup? */

		self->previous = thread_pool->last;

		mode = thread_pool->mode;

		pthread_mutex_unlock( &thread_pool->lock );

		if( mode ) {
			/* Wait for the previous thread to finish his job */
		}

		/* Do the job */
		finfo.buffer_nr    = -1;  /* ??? */
		finfo.frame_nr     =  0;  /* ??? */
		finfo.round_count  = -1;
		finfo.acq_frame_nr =  self->job_data.acq_fr_nr;

		espia_get_frame(self->job_data.bsd.dev_data->dev, &finfo , 0);
#if 0  /* Not finished! */
		/* We need edf in the dev struct in order to use this! */
		ret = lib_buffer_save_file( &self->job_data.bsd, -1/* ??? */, 
		                            &finfo );
#endif /* 0 */

		if( self->job_data.usr_string ) 
			free(self->job_data.usr_string);  /* Or do this in main? */
	}

	return (void *)pthread_self();
}


int lib_nonblock_save_init( etest_t etest, int dev_nr, 
                            struct buffer_save_data *bsd, int num_threads )
{
	int i, prev_idx;
	struct thread_pool_t *thread_pool;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	if( NULL != dev_data->thread_pool ) {
		thread_pool = dev_data->thread_pool;
		pthread_mutex_lock( &thread_pool->lock );
		goto setbsd;
	}

	thread_pool = (struct thread_pool_t *) 
	                                malloc( sizeof(struct thread_pool_t) );
	if( NULL == thread_pool )
		return ESPIA_TEST_ERR_NOMEM;
	memset( thread_pool, 0, sizeof(struct thread_pool_t) );

	if( pthread_mutex_init(&thread_pool->lock, NULL) ) {
		free( thread_pool );
		return ESPIA_TEST_ERR_INTERNAL;
	}
	pthread_mutex_lock( &thread_pool->lock );

	thread_pool->last = -2;  /* The first worker has no predecessor */

	dev_data->thread_pool = thread_pool;

	if( sem_init( &thread_pool->job_sem, 0, 0 ) ) {
		pthread_mutex_unlock( &thread_pool->lock );
		lib_nonblock_save_cleanup( etest, dev_nr );
		return ESPIA_TEST_ERR_INTERNAL;
	}

	thread_pool->threads = (struct thread_info_t *) malloc( num_threads *
	                                        sizeof(struct thread_info_t) );
	if( NULL == thread_pool->threads ) {
		pthread_mutex_unlock( &thread_pool->lock );
		lib_nonblock_save_cleanup( etest, dev_nr );
		return ESPIA_TEST_ERR_NOMEM;
	}
	memset( thread_pool->threads, 0, num_threads *
	                                        sizeof(struct thread_info_t) );

	thread_pool->num_threads = num_threads;

	for( i=0; i<num_threads; i++ )
	{
		if( pthread_create( &(thread_pool->threads[i].thread_id), NULL, 
		                   lib_saving_thread, (void *) thread_pool ) ) {
			pthread_mutex_unlock( &thread_pool->lock );
			lib_nonblock_save_cleanup( etest, dev_nr );
			return ESPIA_TEST_ERR_INTERNAL;
		}
	}

 setbsd:
	prev_idx = thread_pool->job_data.bsd.idx;
	thread_pool->job_data.bsd = *bsd;
	if( thread_pool->job_data.bsd.idx < 0 )
		thread_pool->job_data.bsd.idx = prev_idx;

	pthread_mutex_unlock( &thread_pool->lock );

	return ESPIA_OK;
}


int lib_nonblock_frame_save( etest_t etest, int dev_nr, int acq_fr_nr )
{
	struct thread_pool_t *thread_pool;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	thread_pool = dev_data->thread_pool;
	if( NULL == thread_pool ) {
		return ESPIA_TEST_ERR_NOTINIT;
	}

	pthread_mutex_lock( &thread_pool->lock );

	if( -1 == thread_pool->job_data.worker ) {  /* Previous job not taken! */
		if( 1 /* A flag */ ) {
			/* Return an error */
			return ESPIA_TEST_ERR_INTERNAL;  /* ??? */
		} else {
			/* Wait on a condition */
		}
	}
	thread_pool->last = thread_pool->job_data.worker;
	thread_pool->job_data.worker = -1;

	if( thread_pool->job_data.usr_string ) {
#if 0  /* The worker thread frees the usr_string */
		free( thread_pool->job_data.usr_string );
		thread_pool->job_data.usr_string = NULL;
#else
/*		return ESPIA_TEST_ERR_INTERNAL;  // If worker set it to NULL */
#endif /* 0 */
	}

	edf_str_lock( dev_data );
	if( dev_data->edf_usr_str ) {
		thread_pool->job_data.usr_string = strdup(dev_data->edf_usr_str);
		if( NULL == thread_pool->job_data.usr_string ) {
			edf_str_unlock( dev_data );
			pthread_mutex_unlock( &thread_pool->lock );
			lib_nonblock_save_cleanup( etest, dev_nr );
			return ESPIA_TEST_ERR_NOMEM;
		}
	}
	edf_str_unlock( dev_data );

	thread_pool->job_data.acq_fr_nr = acq_fr_nr;

	pthread_mutex_unlock( &thread_pool->lock );

	sem_post( &thread_pool->job_sem );

	return ESPIA_OK;
}


int lib_nonblock_save_mode( etest_t etest, int dev_nr, int mode )
{
	struct thread_pool_t *thread_pool;
	struct device_data *dev_data;


	get_check_dev_data(dev_data, etest, dev_nr);

	thread_pool = dev_data->thread_pool;
	if( NULL == thread_pool ) {
		return ESPIA_TEST_ERR_NOTINIT;
	}

	pthread_mutex_lock( &thread_pool->lock );

	if( (mode < 0) || (1 < mode) )
		return ESPIA_TEST_ERR_BADPAR;

	thread_pool->mode = mode;

	pthread_mutex_unlock( &thread_pool->lock );

	return ESPIA_OK;
}


int lib_nonblock_save_cleanup(etest_t etest, int dev_nr)
{
	int i;
	struct device_data *dev_data;
	struct thread_info_t *thread;
	struct thread_pool_t *thread_pool;


	get_check_dev_data(dev_data, etest, dev_nr);

	thread_pool = dev_data->thread_pool;

	if( NULL == thread_pool ) 
		return ESPIA_OK;

	pthread_mutex_lock( &thread_pool->lock );

	if( thread_pool->threads ) {
		for( i=0; i<thread_pool->num_threads; i++ ) 
		{
			thread = &thread_pool->threads[i];
			if( ! thread->thread_id )
				continue;

			pthread_cancel( thread->thread_id );
#if 0  /* ??? */
			if( thread->job_data.usr_string )
				free( thread->job_data.usr_string );
#endif /* 0 */
		}

		free( thread_pool->threads );
	}

	sem_destroy( &thread_pool->job_sem );
#if 0  /* ??? */
	if( thread_pool->job_data.usr_string )
		free( thread_pool->job_data.usr_string );
#endif /* 0 */
	pthread_mutex_unlock( &thread_pool->lock );
	pthread_mutex_destroy( &thread_pool->lock );

	free( thread_pool );
	dev_data->thread_pool = NULL;
		
	return ESPIA_OK;
}


/*--------------------------------------------------------------------------*
 * Visualization functions
 *--------------------------------------------------------------------------*/

/****************************************************************************
 *  Function :                                                              *
 *     Logic :                                                              *
 *        IN :                                                              *
 *       OUT :                                                              *
 *   Returns :                                                              *
 ****************************************************************************/
int lib_image_refresh( etest_t etest )
{
#ifdef WITH_IMAGE
	return image_poll();
#else // WITH_IMAGE
	return 0;
#endif // WITH_IMAGE
}


/*--------------------------------------------------------------------------*
 * Ignore everything below this line! It is just for testing!
 *--------------------------------------------------------------------------*/

int lib_show( etest_t etest, int dev_nr )
{
	struct lib_data *plib = (struct lib_data *) etest;
	struct device_data *dev_data;
	int i;


	if( is_good_dev_nr(dev_nr) ) {
		dev_data = &plib->device[dev_nr];
		printf("etest->device[%d]->dev_nr = %d,   "
		       "etest->device[%d]->dev = %d\r\n"
		       , dev_nr, dev_data->dev_nr, dev_nr, (int) dev_data->dev);
	} else {
		for (i = 0; i < ESPIA_TEST_MAX_NR_DEV; i++) {
			dev_data = &plib->device[i];
			printf("etest->device[%d]->dev_nr = %d,   "
			       "etest->device[%d]->dev = %d\r\n"
			       , i, dev_data->dev_nr, i, (int) dev_data->dev);
		}
	}

	return 0;
}


int lib_dump_data( void *data, int len )
{
	int offset, i, chrs, linechrs = 16;
	unsigned char *ptr = (unsigned char *) data;

#define isprint7(i)	(((i) < 0x80) && isprint(i))

	printf("Dumping %d bytes at address %lx:\n", len, (unsigned long)data);
	for (offset = 0; offset < len; offset += linechrs) {
		chrs = linechrs;
		if (offset + chrs > len)
			chrs = len - offset;
		printf("%04x:  ", offset);
		for (i = 0; i < chrs; i++)
			printf("%02x ", ptr[i]);
		for (i = chrs; i < linechrs; i++)
			printf("   ");
		printf(" ");
		for (i = 0; i < chrs; i++)
			printf("%c", isprint7(ptr[i]) ? ptr[i] : '.');
		printf("\n");
		ptr += chrs;
	}

	return 0;
}

