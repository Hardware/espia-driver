 /****************************************************************************
 * File:	espia_lib.h
 * $Header: /segfs/bliss/cvs/driver/ESRF/espia/src/espia_lib.h,v 2.23 2020/07/24 08:59:44 ahoms Exp $
 * Project:	SECAD Espia card Linux driver
 * Description:	Espia library header file
 * Author(s):	SECAD S.A.
 *		A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 ****************************************************************************/

#ifndef _ESPIA_LIB_H
#define _ESPIA_LIB_H

#include "scdxipci_lib.h"
#include <stdio.h>
#include <string.h>
#include <pthread.h>


/*--------------------------------------------------------------------------
 * Constant and error definitions
 *--------------------------------------------------------------------------*/

enum {
	ESPIA_EACH_NO,
	ESPIA_EACH_BUFFER,
	ESPIA_EACH_FRAME,
};

enum {
	ESPIA_DEB_NONE,
	ESPIA_DEB_ERRORS,
	ESPIA_DEB_OPEN,
	ESPIA_DEB_PARAMS,
	ESPIA_DEB_TRACE,
	ESPIA_DEB_SG,
};

enum {
	ESPIA_CB_ACQ,
	ESPIA_CB_CCD_STATUS,
	ESPIA_CB_SERIAL_TX,
	ESPIA_CB_SERIAL_RX,
};

#define ESPIA_ACQ_NO_THREAD	0
#define ESPIA_ACQ_CB_THREAD	1

#define ESPIA_ACQ_ANY		SCDXIPCI_ANY
#define ESPIA_ACQ_EACH		(SCDXIPCI_ANY - 1)

#define ESPIA_DEV_INVAL		SCDXIPCI_INVALID
#define ESPIA_META_DEV		SCDXIPCI_META_DEV
#define ESPIA_MAX_NR_DEV	256

#define ESPIA_SER_DEF_LEN	(10 * 1024)


#define	ESPIA_OK		SCDXIPCI_OK
#define	ESPIA_ERR_BASE		(SCDXIPCI_ERR_BASE - 500)
#define ESPIA_ERR_DEVNR		(ESPIA_ERR_BASE -  1)
#define ESPIA_ERR_NOFREEDEV	(ESPIA_ERR_BASE -  2)
#define ESPIA_ERR_NODEV		(ESPIA_ERR_BASE -  3)
#define ESPIA_ERR_ALLOC		(ESPIA_ERR_BASE -  4)
#define ESPIA_ERR_BUFFLIST	(ESPIA_ERR_BASE -  5)
#define ESPIA_ERR_BUFFERFRAMES	(ESPIA_ERR_BASE -  6)
#define ESPIA_ERR_CBTYPE	(ESPIA_ERR_BASE -  7)
#define ESPIA_ERR_CBREG		(ESPIA_ERR_BASE -  8)
#define ESPIA_ERR_CBMEM		(ESPIA_ERR_BASE -  9)
#define ESPIA_ERR_CBPAR		(ESPIA_ERR_BASE - 10)
#define ESPIA_ERR_CBACT		(ESPIA_ERR_BASE - 11)
#define ESPIA_ERR_THREAD	(ESPIA_ERR_BASE - 12)
#define ESPIA_ERR_EACH		(ESPIA_ERR_BASE - 13)
#define ESPIA_ERR_DIDNOTSTOP	(ESPIA_ERR_BASE - 14)
#define ESPIA_ERR_PTRNULL	(ESPIA_ERR_BASE - 15)
#define ESPIA_ERR_ABORT		(ESPIA_ERR_BASE - 16)
#define ESPIA_ERR_ROI_IMGCFG	(ESPIA_ERR_BASE - 17)
#define ESPIA_ERR_ROI_INVAL	(ESPIA_ERR_BASE - 18)
#define ESPIA_ERR_VER_INIT	(ESPIA_ERR_BASE - 19)
#define ESPIA_ERR_MAX		(ESPIA_ERR_BASE - 19)


#define espia_fatal_error(ret)	scdxipci_fatal_error(ret)


enum {
	ESPIA_FRM_OP_SWAP_INTERLACE,
	ESPIA_FRM_OP_FLIP_VERT,
	ESPIA_FRM_OP_CHECK_NULL,
};


enum {
	ESPIA_SG_NORM,
	ESPIA_SG_FLIP_VERT_1,
	ESPIA_SG_FLIP_VERT_2,
	ESPIA_SG_CONCAT_VERT_2,
	ESPIA_SG_CONCAT_VERT_INV_2,
};

/*--------------------------------------------------------------------------
 * The public types and structures of the library
 *--------------------------------------------------------------------------*/

struct espia_lib_version {
	char			*compile;
	char			*run;
	char			*reserved[6];
};

typedef unsigned long espia_t;

struct espia_cb_acq {
	struct img_frame_info	 req_finfo;
	struct img_frame_info	 cb_finfo;
};

struct espia_cb_status {
	unsigned char		 ccd_status;
};

struct espia_cb_serial {
	char			*buffer;
	unsigned long		 buffer_len;
	char			*term;
	unsigned long		 term_len;
	unsigned long		 nr_bytes;
};

union espia_cb_info {
	struct espia_cb_acq	 acq;
	struct espia_cb_status	 status;
	struct espia_cb_serial	 serial;
};

struct espia_cb_data;
typedef int espia_cb(struct espia_cb_data *ainfo);

struct espia_cb_data {
	int			 type;
	espia_cb		*cb;
	espia_t			 dev;
	union espia_cb_info	 info;
	unsigned long		 timeout;
	int			 ret;
	void			*data;
	int			 cb_nr;
};

struct espia_roi {
	unsigned long		 left;
	unsigned long		 top;
	unsigned long		 width;
	unsigned long		 height;
};

struct espia_frame_dim {
	unsigned long		 width;
	unsigned long		 height;
	unsigned long		 depth;
};

struct espia_frm_op_fn_data {
	struct espia_frame_dim	 frame_dim;
	void			*data;
};

#define ESPIA_FRM_OP_CTL_INIT	0
#define ESPIA_FRM_OP_CTL_END	1

typedef int (*espia_frm_op_fn) (espia_t dev, void *frame_ptr, 
				struct espia_frm_op_fn_data *frame_data,
				unsigned long acq_frame_nr);

typedef int (*espia_frm_op_ctl)(espia_t dev, int ctrl_cmd,
				struct espia_frm_op_fn_data *frame_data,
				unsigned long acq_frames);

struct espia_frm_op_fn_set {
	char			*name;
	char			*desc;
	espia_frm_op_fn		 fn;
	espia_frm_op_ctl	 ctl;
};

struct espia_frm_op_info {
	struct espia_frm_op_fn_set	*fn_set;
	void				*data;
};


typedef int plugin_func( long frameNum, int depth, int width, int height, 
                         void *frameData, void *userData );

#define PLUG_DESC_LEN 64

struct plugin_t {
	plugin_func 		*fun;
	int 			 active;
	void 			*userData;
	char 			 desc[PLUG_DESC_LEN];
};

#define MAX_NUM_PLUGINS_I   1
#define MAX_NUM_PLUGINS_II  1

struct lib_plugins_data {
	int 			 num_plugins_I;
	struct plugin_t	 	 plugin_list_I[MAX_NUM_PLUGINS_I];
	int 			 num_plugins_II;
	struct plugin_t	 	 plugin_list_II[MAX_NUM_PLUGINS_II];
	unsigned long 		 acq_cb_nr;  /* For type II plugins */
	pthread_mutex_t 	 lock;
};


#ifndef SWIG  /* Don't wrap this in Python */
extern char *espia_sg_img_config_desc[];
extern int espia_sg_img_config_nr;
#endif /* SWIG */


struct espia_option {
	int			 option;
	char			*name;
};

struct espia_param {
	char			*name;
	unsigned int		 reg;
	unsigned int		 mask;
	int			 shift;
	char			*desc;
};

struct espia_firmware {
	char			*name;
	unsigned short		 checksum;
	unsigned long		 features;
};


#ifndef SWIG  /* Don't wrap this in Python */

/*--------------------------------------------------------------------------
 * Espia library function prototypes
 *--------------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif 

// return a message describing the specified error
char *espia_strerror(int error);

// internal library version routine: should not be called by user
int _espia_internal_lib_version(struct espia_lib_version *ver);

// header file revision used in the next function
static char _espia_lib_h_revision[] __attribute__((unused)) = 
	"$Revision: 2.23 $";

// return the compiled and running library versions
static inline
int espia_lib_version(struct espia_lib_version *ver)
{
	static char *rel = (char *) "$Name: v3_12 $";
	int reserved = sizeof(ver->reserved) / sizeof(ver->reserved[0]);
	char **ptr;

	if (ver == NULL)
		return ESPIA_ERR_PTRNULL;
	memset(ver, 0, sizeof(*ver));

	ptr = &ver->reserved[reserved - 2];
	*ptr++ = rel;
	*ptr++ = _espia_lib_h_revision;
	return _espia_internal_lib_version(ver);
}

// open a device and return its handle in *dev_ptr
int espia_open(int dev_nr, espia_t *dev_ptr);

// close the device
int espia_close(espia_t dev);

// set the debug level (if *deb_lvl_ptr != -1). return previous level
// if drv, set(get) the driver debug level
int espia_debug_level(espia_t dev, int *deb_lvl_ptr, int drv);

// get the option *option_nr_ptr data pointer into *option_ptr.
// if *option_nr_ptr is -1, the total number of options is returned
int espia_get_option_data(espia_t dev, int *option_nr_ptr, 
			  struct espia_option **option_ptr);

// do action (RD | WR | RD_WR) on the specified driver option
int espia_option(espia_t dev, int option, int action, int *val_ptr);

// read the specified register, return value in *data_ptr
int espia_read_register(espia_t dev, unsigned long reg_off, 
			unsigned int *data_ptr);
// write on the specified register, modifying only the bits given by mask with
// the corresponding values in *data_ptr, where the original value is returned
int espia_write_register(espia_t dev, unsigned long reg_off, 
			 unsigned int *data_ptr, unsigned int mask);

// get the parameter *param_nr_ptr data pointer into *param_ptr.
// if *param_nr_ptr is -1, the total number of parameters is returned
int espia_get_param_data(espia_t dev, int *param_nr_ptr, 
			 struct espia_param **param_ptr);

// read nr_param parameters specified in param_arr into val_arr
int espia_get_param(espia_t dev, int *param_arr, unsigned int *val_arr, 
		    int nr_param);
// write nr_param parameters specified in param_arr with val_arr
int espia_set_param(espia_t dev, int *param_arr, unsigned int *val_arr, 
		    int nr_param);

// get the firmware *firmware_nr_ptr data pointer into *firmware_ptr.
// if *firmware_nr_ptr is -1, the total number of firmwares is returned
int espia_get_firmware_data(int *firmware_nr_ptr, 
			    struct espia_firmware **firmware_ptr);

// manage the ESPIA hardware-related information
int espia_hw_info(espia_t dev, int action, struct scdxipci_hw_info *hw_info);

// reset the aurora link
int espia_reset_link(espia_t dev);

// serial read. *nr_bytes_ptr is the length of the buffer when called,
// and the nr of transferred bytes when return. timeout is the maximum time
// to wait, in us; 0 means no block
int espia_ser_read (espia_t dev, char *buffer, unsigned long *nr_bytes_ptr, 
		    unsigned long timeout);
// idem, but return as soon as term is found
int espia_ser_read_str(espia_t dev, char *buffer, unsigned long *nr_bytes_ptr, 
		       char *term, unsigned long term_bytes,
		       unsigned long timeout);

// idem. delay is time between block_size characters writes; 
// block request to wait.
int espia_ser_write(espia_t dev, char *buffer, unsigned long *nr_bytes_ptr, 
		    unsigned long block_size, unsigned long delay, int block);

// flush the serial (read) buffer
int espia_ser_flush(espia_t dev);

// get the information of the frame post-op functs. defined in the library
int espia_get_frm_op_fn_set(int *frm_op_nr_ptr, 
			    struct espia_frm_op_fn_set **fn_set_ptr);

// define frame post-operation function
int espia_set_frm_op_fn(espia_t dev, struct espia_frm_op_info *op_data, 
			unsigned long nr_op);

// alloc nr_buffers with buffer_frames, each of frame_size
int espia_buffer_alloc(espia_t dev, unsigned long nr_buffers, 
		       unsigned long buffer_frames, unsigned long frame_size);
// free all allocated buffers
int espia_buffer_free (espia_t dev);

// get the information about the buffer
int espia_buffer_info(espia_t dev, unsigned long buffer_nr, 
		      struct img_buffer_info *binfo);
// get the frame pointer, if buffer_nr == -1, assume acq_frame_nr
int espia_frame_address(espia_t dev, unsigned long buffer_nr, 
			unsigned long frame_nr, void **addr_ptr);

// start the acquisition
int espia_start_acq (espia_t dev, unsigned long start_buffer, 
		     unsigned long nr_frames, unsigned long timeout);
// stop the acquisition
int espia_stop_acq  (espia_t dev);
// check if the acquisition is running and return its run nr
int espia_acq_active(espia_t dev, unsigned long *acq_run_nr_ptr);
// get frame info
int espia_get_frame(espia_t dev, struct img_frame_info *finfo,
		    unsigned long timeout);

// get the CCD status byte; if requested, block until a new status is received
int espia_ccd_status(espia_t dev, unsigned char *status_ptr, 
		     unsigned long timeout);

// register callback
int espia_register_callback  (espia_t dev, struct espia_cb_data *cb_data, 
			      int *cb_nr_ptr);
// get callback
int espia_get_callback       (espia_t dev, int cb_nr, 
			      struct espia_cb_data **cb_data_ptr);
// callback active
int espia_callback_active(espia_t dev, int cb_nr, int active);
// unregister callback
int espia_unregister_callback(espia_t dev, int cb_nr);

// set SG table
int espia_set_sg(espia_t dev, int dev_idx, struct scdxipci_sg_table *sg);
// crate SG table(s) for specified config
int espia_create_sg(espia_t dev, int img_config, 
		    struct espia_frame_dim *frame_dim, struct espia_roi *roi,
		    struct scdxipci_sg_table **sg_list_ptr, int *nr_dev_ptr);
// release the allocated SG table(s)
int espia_release_sg(espia_t dev, struct scdxipci_sg_table *sg_list, 
		     int nr_dev);

// set the device list for meta dev acq.
int espia_set_dev(scdxipci_t dev, int *dev_list, int nr_dev);

int espia_get_current_frm_op( espia_t dev, struct espia_frm_op_info op_info[], 
			      int *nr_op );

#ifdef __cplusplus
}
#endif 

/*--------------------------------------------------------------------------
 * Some helpers
 *--------------------------------------------------------------------------*/

static inline int finished_espia_frame_info(struct img_frame_info *finfo,
					    int ret)
{
	return finished_img_frame_info(finfo) && (ret == 0);
}
	

static inline int espia_cb_each(struct img_frame_info *finfo)
{
	int each = ESPIA_EACH_NO;
	if (finfo->buffer_nr == ESPIA_ACQ_EACH)
		each = ESPIA_EACH_BUFFER;
	else if (finfo->acq_frame_nr == ESPIA_ACQ_EACH)
		each = ESPIA_EACH_FRAME;
	return each;
}

static inline unsigned long espia_frame_mem_size(struct espia_frame_dim *fdim)
{
	return fdim->width * fdim->height * fdim->depth;
}

#endif /* !SWIG */

#endif /* _ESPIA_LIB_H */
