/****************************************************************************
 * File:	espia_lib.c
 * $Header: /segfs/bliss/cvs/driver/ESRF/espia/src/espia_lib.c,v 2.30 2020/07/24 16:28:06 ahoms Exp $
 * Project:	SECAD Espia card Linux driver
 * Description:	Espia library source code
 * Author(s):	SECAD S.A.
 *		A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 ****************************************************************************/

#include "esrfdebug.h"
#include "espia_lib.h"
#include "scdxipci_lib.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <limits.h>
#include <sys/time.h>


/*--------------------------------------------------------------------------
 * internal constants
 *--------------------------------------------------------------------------*/

#define ANY_BUFFER		0
#define	REAL_BUFFER		1
#define ANY_FRAME_NR		0
#define REAL_FRAME_NR		1
#define BUFFER_FRAME_NR		0
#define ACQ_FRAME_NR		1

#define PARAM_GET		0
#define PARAM_SET		1

#define START_TIMEOUT		5000000 // usec
#define THREAD_END_SLEEP	10000   // usec
enum {
	ESPIA_ACQ_STOP,
	ESPIA_ACQ_RUN,
	ESPIA_ACQ_RUN_THREAD,
};

enum {
	CB_CHECK_INTEG,		// check integrity
	CB_CHECK_REG,		// check registered
	CB_CHECK_FINAL,		// final check
};

/*--------------------------------------------------------------------------
 * rcs id and driver revision
 *--------------------------------------------------------------------------*/

static char rcs_id[] __attribute__((unused)) = 
	"$Header: /segfs/bliss/cvs/driver/ESRF/espia/src/espia_lib.c,v 2.30 2020/07/24 16:28:06 ahoms Exp $"; 

static char espia_lib_release[]  = "$Name: v3_12 $";


/*--------------------------------------------------------------------------
 * the library data associated to each device
 *--------------------------------------------------------------------------*/

struct espia_dev;

// one of this for each buffer
struct buffer_post_op_data {
	pthread_mutex_t		 	  mutex;
	struct img_frame_info		  last_frame;
	struct img_buffer_info		  binfo;
};

struct espia_priv_cb_data {
	struct espia_dev		 *pdev;
	struct espia_cb_data		  cb_data;
	pthread_t			  thread_id;
	pthread_cond_t			  cond;
	int				  active;
	int				  terminate;
	int				  finished;
	int				  count;
	pthread_mutex_t			 *mutex;
};

struct espia_dev {
	int				  dev_nr;
	scdxipci_t			  dev;
	unsigned long			  buffer_frames;
	unsigned long			  frame_size;
	unsigned long			  nr_buffer;
	int				  nr_cb;
	struct espia_priv_cb_data	**cbs;
	int				  acq_frame_nr;
	unsigned long			  acq_run_nr;
	int				  start_buffer;
	struct espia_frm_op_info	 *post_op_info;
	int				  nr_post_op;
	struct buffer_post_op_data	 *buffer_post_op;
	pthread_mutex_t			  mutex;
};

static struct espia_dev devices[ESPIA_MAX_NR_DEV];


/*--------------------------------------------------------------------------
 * debug stuff
 *--------------------------------------------------------------------------*/

DECLARE_STATIC_DEBUG_LEVEL(debug_level, ESPIA_DEB_NONE);

#define DEB_DEVNR		(pdev ? pdev->dev_nr : -1)
#define DEB_HEADING		"ESPIA#%d"
#undef  DEB_ARG
#define DEB_ARG			DEB_DEVNR , DEB_FNAME

#define DEB_ERRORS(fmt, ...)	\
	DPRINTF(ESPIA_DEB_ERRORS, fmt , ## __VA_ARGS__)
#define DEB_OPEN(fmt, ...)	\
	DPRINTF(ESPIA_DEB_OPEN,   fmt , ## __VA_ARGS__)
#define DEB_PARAMS(fmt, ...)	\
	DPRINTF(ESPIA_DEB_PARAMS, fmt , ## __VA_ARGS__)
#define DEB_TRACE(fmt, ...)	\
	DPRINTF(ESPIA_DEB_TRACE,  fmt , ## __VA_ARGS__)
#define DEB_SG(fmt, ...)	\
	DPRINTF(ESPIA_DEB_SG,     fmt , ## __VA_ARGS__)

#define espia_get_check_dev(pdev, dev) \
	do { \
		if (!((pdev) = get_pdev(DEB_FNAME, dev))) \
			return ESPIA_ERR_NODEV; \
	} while (0)

#define espia_check(fname, fret) \
	({ \
		int cret = (fret); \
		if (cret < 0) { \
			DEB_ERRORS("Error in call to %s: %s\n", fname, \
				   espia_strerror(cret)); \
			return cret; \
		} \
		cret; \
	})

#define espia_check_buffer(bnr, real) \
	({ \
		if (((bnr) >= pdev->nr_buffer) && \
		    ((real) || (((bnr) != ESPIA_ACQ_EACH) && \
				((bnr) != ESPIA_ACQ_ANY)))) { \
			DEB_ERRORS("Invalid buffer number: %ld\n", (bnr)); \
			return SCDXIPCI_ERR_BUFFER; \
		} \
		(bnr); \
	})
		
#define lowest_frame_nr(type, real) \
	((real) ? 0 : (((type) == ACQ_FRAME_NR) ? -2 : -1))
#define high_frame_nr_top(type) \
	(((type) == ACQ_FRAME_NR) ? INT_MAX : (int) pdev->buffer_frames)

#define espia_check_frame_nr(fnr, type, real) \
	({ \
		if (((int) (fnr) < lowest_frame_nr(type, real)) || \
		    ((int) (fnr) >= high_frame_nr_top(type))) { \
			DEB_ERRORS("Invalid frame number: %ld\n", (fnr)); \
			return SCDXIPCI_ERR_FRAMENR; \
		} \
		(fnr); \
	})
		
#define espia_check_null_single(ptr) \
	if ((ptr) == NULL) { \
		DEB_ERRORS("Invalid NULL " #ptr "\n"); \
		return ESPIA_ERR_PTRNULL; \
	}

#define espia_check_null(ptr) \
	do { \
		espia_check_null_single(ptr); \
	} while (0)

#define espia_check_null2(ptr1, ptr2) \
	do { \
		espia_check_null_single(ptr1); \
		espia_check_null_single(ptr2); \
	} while (0)

#define espia_check_null3(ptr1, ptr2, ptr3) \
	do { \
		espia_check_null_single(ptr1); \
		espia_check_null_single(ptr2); \
		espia_check_null_single(ptr3); \
	} while (0)


/*--------------------------------------------------------------------------
 * helpers
 *--------------------------------------------------------------------------*/

static inline struct espia_dev *get_pdev(char *fname, espia_t dev)
{
	struct espia_dev *pdev = &devices[dev];

	FENTRY(fname);

	if ((dev < 0) || (dev >= ESPIA_MAX_NR_DEV) || 
	    (pdev->dev == SCDXIPCI_DEV_INVAL)) { 
		pdev = NULL; 
		DEB_ERRORS("%s\n", espia_strerror(ESPIA_ERR_NODEV)); 
	} 
	return pdev; 
}

#define dev_from_pdev(pdev)\
	((pdev) - devices)

static int get_free_dev()
{
	int i;
	static int first_time = 1;

	if (first_time) {
		first_time = 0;
		for (i = 0; i < ESPIA_MAX_NR_DEV; i++)
			devices[i].dev = SCDXIPCI_DEV_INVAL;
	}

	for (i = 0; i < ESPIA_MAX_NR_DEV; i++)
		if (devices[i].dev == SCDXIPCI_DEV_INVAL)
			break;
	if (i == ESPIA_MAX_NR_DEV) {
		return ESPIA_ERR_NOFREEDEV;
	}
	memset(&devices[i], 0, sizeof(devices[i]));

	return i;
}

static char *get_dev_name(int dev_nr, char *buffer)
{
	char suffix[32];

	if ((dev_nr < 0) || (dev_nr >= ESPIA_MAX_NR_DEV))
		return NULL;

	if (dev_nr == ESPIA_META_DEV)
		strcpy(suffix, "meta");
	else
		sprintf(suffix, "%d", dev_nr);
	sprintf(buffer, "/dev/%s%s", SCDXIPCI_DRV_NAME, suffix);
	return buffer;
}

static inline int priv_cb_nr(struct espia_priv_cb_data *pcb_data)
{
	return pcb_data->cb_data.cb_nr;
}

static inline int priv_cb_is_acq(struct espia_priv_cb_data *pcb_data)
{
	return pcb_data && (pcb_data->cb_data.type == ESPIA_CB_ACQ);
}


/*--------------------------------------------------------------------------
 * error messages
 *--------------------------------------------------------------------------*/

static deb_const_str error_str_array[] = {
	{ESPIA_OK,		"No error"},
	{ESPIA_ERR_DEVNR,	"Invalid device number"},
	{ESPIA_ERR_NOFREEDEV,	"Too many devices open"},
	{ESPIA_ERR_NODEV,	"Invalid device handle"},
	{ESPIA_ERR_ALLOC,	"Buffers already allocated"},
	{ESPIA_ERR_BUFFLIST,	"Cannot allocate buffer list"},
	{ESPIA_ERR_BUFFERFRAMES,"Cannot change the nr. of buffer frames"},
	{ESPIA_ERR_CBTYPE,	"Invalid callback type"},
	{ESPIA_ERR_CBREG,	"Callback already/not registered"},
	{ESPIA_ERR_CBMEM,	"Cannot allocate callback data or list"},
	{ESPIA_ERR_CBPAR,	"Callback timeout/function cannot be null"},
	{ESPIA_ERR_CBACT,	"Callback of the same type already active"},
	{ESPIA_ERR_THREAD,	"Error starting callback thread"},
	{ESPIA_ERR_EACH,	"Both buffer_nr and acq_frame_nr are EACH"},
	{ESPIA_ERR_DIDNOTSTOP,	"Acquisition still running after stop"},
	{ESPIA_ERR_PTRNULL,	"Invalid NULL pointer"},
	{ESPIA_ERR_ABORT,	"Acquitition aborted by a user callback"},
	{ESPIA_ERR_ROI_IMGCFG,  "Invalid combination of RoI/image config"},
	{ESPIA_ERR_ROI_INVAL,   "Invalid RoI parameter"},
	{ESPIA_ERR_VER_INIT,    "Inval. ver. init: use espia_lib_version"},
};

INIT_STATIC_CONST_STR_ARRAY_PREFIX(error_strings, error_str_array, "ESPIA");

char *espia_strerror(int error)
{
	return GET_CONST_STR(error_strings, error, scdxipci_strerror(error));
}


/*--------------------------------------------------------------------------
 * library version
 *--------------------------------------------------------------------------*/

int _espia_internal_lib_version(struct espia_lib_version *ver)
{
	static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
	static int inited = 0;
	static char compile[PRETTY_RCS_REV_LEN];
	static char run[PRETTY_RCS_REV_LEN];
	int reserved = sizeof(ver->reserved) / sizeof(ver->reserved[0]);
	char **ptr, *raw_rel, *raw_rev;
	struct espia_dev *pdev = NULL;
	int ret = ESPIA_OK;

	FENTRY("_espia_internal_lib_version");

	espia_check_null(ver);
	
	ptr = &ver->reserved[reserved - 2];
	raw_rel = *ptr++;
	raw_rev = *ptr++;

	if ((raw_rel == NULL) || (raw_rev == NULL)) {
		DEB_ERRORS("invalid version initialization!\n");
		DEB_ERRORS("  please use public espia_lib_version\n");
		return -EINVAL;
	}

	pthread_mutex_lock(&mutex);
	if (inited)
		goto unlock;

	if (numeric_rcs_revision(run, raw_rel, raw_rev) == NULL) {
		DEB_ERRORS("could not get running RCS rel.: %s,%s\n", 
			   raw_rel, raw_rev); 
		ret = SCDXIPCI_ERR_INTERNAL;
		goto unlock;
	} 

	raw_rel = espia_lib_release;
	raw_rev = _espia_lib_h_revision;

	if (numeric_rcs_revision(compile, raw_rel, raw_rev) == NULL) {
		DEB_ERRORS("could not get compiled RCS rel.: %s,%s\n", 
			   raw_rel, raw_rev); 
		ret = SCDXIPCI_ERR_INTERNAL;
		goto unlock;
	} 

	inited = 1;

 unlock:
	pthread_mutex_unlock(&mutex);

	if (ret < 0)
		return ret;

	ver->run = run;
	ver->compile = compile;

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * espia open/close
 *--------------------------------------------------------------------------*/

int espia_open(int dev_nr, espia_t *dev_ptr)
{
	int i, ret;
	scdxipci_t dev;
	char devname[256];
	unsigned int value, mask;
	struct espia_dev *pdev = NULL;

	FENTRY("espia_open");
	espia_check_null(dev_ptr);

	if (!get_dev_name(dev_nr, devname)) {
		ret = ESPIA_ERR_DEVNR;
		DEB_ERRORS("%s: %d\n", espia_strerror(ret), dev_nr);
		return ret;
	}

	i = get_free_dev();
	if (i < 0) {
		DEB_ERRORS("%s", espia_strerror(i));
		return i;
	}
	pdev = &devices[i];
	pdev->dev_nr = dev_nr;
	pthread_mutex_init(&pdev->mutex, NULL);

	ret = scdxipci_open(devname, &dev);
	if (ret != SCDXIPCI_OK) {
		DEB_ERRORS("Error in call to scdxipci_open (dev #%d)%s\n", 
			   dev_nr, scdxipci_strerror(ret));
		return ret;
	}

	pdev->dev = dev;
	pdev->buffer_frames = 1;
	pdev->frame_size = SCDXIPCI_INVALID;
	espia_check("scdxipci_frame_size", 
		    scdxipci_frame_size(dev, &pdev->frame_size));
	DEB_PARAMS("buffer_frames=%ld, frame_size=%ld\n", 
		   pdev->buffer_frames, pdev->frame_size);

	if (dev_nr != ESPIA_META_DEV) {
		DEB_TRACE("configuring espia card ...\n");
		value = DECODE_ESPIA | USE_EOI | USE_64_BITS | USE_BURST;
		mask = value | (MODE_0 | MODE_1 | ENA_TX);
		espia_check("scdxipci_write_register",
			    scdxipci_write_register(dev, CONTROL_REGISTER, 
						    &value, mask));
	}

	pdev->post_op_info = NULL;
	pdev->nr_post_op = 0;
	pdev->buffer_post_op = NULL;

	DEB_OPEN("Open device %s\n", devname);

	*dev_ptr = i;
	return ESPIA_OK;
}


int espia_close(espia_t dev)
{
	struct espia_dev *pdev;
	int cb_nr;

	FENTRY("espia_close");
	espia_get_check_dev(pdev, dev);

	espia_check("espia_stop_acq", espia_stop_acq(dev));

	if (pdev->cbs != NULL) {
		for (cb_nr = 0; cb_nr < pdev->nr_cb; cb_nr++) {
			if (pdev->cbs[cb_nr] != NULL)
				espia_unregister_callback(dev, cb_nr);
		}
		free(pdev->cbs);
	}

	espia_check("espia_buffer_free", espia_buffer_free(dev));
	espia_check("espia_set_frm_op_fn", espia_set_frm_op_fn(dev, NULL, 0));

	DEB_OPEN("Closing device\n");
	espia_check("scdxipci_close", scdxipci_close(pdev->dev));

	pdev->dev = SCDXIPCI_DEV_INVAL;
	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * driver and library debug level
 *--------------------------------------------------------------------------*/

#define option(x) \
	{SCDXIPCI_OPT_##x, #x}

static struct espia_option espia_option_table[] = {
	option(DEBUG_LEVEL),
	option(NO_FIFO_RESET),
	option(KEEP_RELEASED_PAGES),
};

static int espia_option_table_len = (sizeof(espia_option_table) /
				     sizeof(espia_option_table[0]));

int espia_get_option_data(espia_t dev, int *option_nr_ptr, 
			  struct espia_option **option_ptr)
{
	struct espia_dev *pdev; 
	int option_nr;

	FENTRY("espia_get_option_data");
	espia_get_check_dev(pdev, dev);
	espia_check_null(option_nr_ptr);

	option_nr = *option_nr_ptr;
	if (scdxipci_is(REQUEST, option_nr)) {
		*option_nr_ptr = espia_option_table_len;
		return ESPIA_OK;
	} else if ((option_nr < 0) || 
                   (option_nr >= espia_option_table_len)) {
		DEB_ERRORS("invalid option_nr %d, nr. options. is %d\n",
			   option_nr, espia_option_table_len);
		return -EINVAL;
	}

	espia_check_null(option_ptr);
	*option_ptr = &espia_option_table[option_nr];

	return ESPIA_OK;
}


int espia_option(espia_t dev, int option, int action, int *val_ptr)
{
	struct espia_dev *pdev;
	int ret;

	FENTRY("espia_option");
	espia_get_check_dev(pdev, dev);
	espia_check_null(val_ptr);

	ret = scdxipci_option(pdev->dev, option, action, val_ptr);
	return espia_check("scdxipci_option", ret);

}


/*--------------------------------------------------------------------------
 * driver and library debug level
 *--------------------------------------------------------------------------*/

int espia_debug_level(espia_t dev, int *deb_lvl_ptr, int drv)
{
	int option, action, just_read, req_lvl;
	struct espia_dev *pdev;

	FENTRY("espia_debug_level");
	espia_get_check_dev(pdev, dev);
	espia_check_null(deb_lvl_ptr);

	req_lvl = *deb_lvl_ptr;
	just_read = scdxipci_is(REQUEST, req_lvl);
	if (drv) {
		option = SCDXIPCI_OPT_DEBUG_LEVEL;
		action = just_read ? SCDXIPCI_OPT_RD : SCDXIPCI_OPT_RD_WR;
		espia_check("espia_option",
			    espia_option(dev, option, action, deb_lvl_ptr));
	} else {
		*deb_lvl_ptr = debug_level;
		if (!just_read)
			debug_level = req_lvl;
	}
	DEB_PARAMS("drv=%d, deb_lvl=%d, prev_deb_lvl=%d\n", drv,
		   req_lvl, *deb_lvl_ptr);
	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * read/write register
 *--------------------------------------------------------------------------*/

int espia_read_register(espia_t dev, unsigned long reg_off, 
			unsigned int *data_ptr)
{
	struct espia_dev *pdev;
	int ret;

	FENTRY("espia_read_register");
	espia_get_check_dev(pdev, dev);
	espia_check_null(data_ptr);

	ret = scdxipci_read_register(pdev->dev, reg_off, data_ptr);
	DEB_PARAMS("reg_off=0x%04lx, data=0x%x\n", reg_off, *data_ptr);
	return espia_check("scdxipci_read_register", ret);
}


int espia_write_register(espia_t dev, unsigned long reg_off, 
			 unsigned int *data_ptr, unsigned int mask)
{
	struct espia_dev *pdev;
	unsigned int pre_data;
	int ret;

	FENTRY("espia_write_register");
	espia_get_check_dev(pdev, dev);
	espia_check_null(data_ptr);

	pre_data = *data_ptr;
	ret = scdxipci_write_register(pdev->dev, reg_off, data_ptr, mask);
	DEB_PARAMS("reg_off=0x%04lx, pre_data=0x%x mask=0x%x "
		   "post_data=0x%x\n", reg_off, pre_data, mask, *data_ptr);
	return espia_check("scdxipci_write_register", ret);
}


/*--------------------------------------------------------------------------
 * parameter table
 *--------------------------------------------------------------------------*/

#define status(name, bit, desc) \
	{#name, STATUS_REGISTER, 1UL<<(bit), bit, desc}
#define control(name, bit, desc) \
	{#name, CONTROL_REGISTER, 1UL<<(bit), bit, desc}
#define control2(name, bit, desc) \
	{#name, CONTROL_REGISTER, 3UL<<(bit), bit, desc}
#define serial(name, mask, shift, desc) \
	{#name, ESPIA_SERIAL_STATUS, (mask)<<(shift), shift, desc}

static struct espia_param espia_param_table[] = {
	// Status register
	status(INTERRUPT,		0,  "An IRQ is pending"),
	status(INTERRUPT_DMA,		1,  "The DMA is generating an IRQ"),
	status(INTERRUPT_SCATTER_GATHER, 2, "The SG is generating an IRQ"),
	status(TRANSFERT_ERROR,		8,  "An error ocurred during the Xfer"),
	status(DCM_LOCKED,		16, "The Tx clock is stable enough"),
	status(DATA_VLD_LED,		17, "The board receives data"),
	status(CHAN_READY_LED,		18, "The board sends data"),
	status(SLOT_64,			19, "The board is in a 64-bit slot"),
	status(CHAN_UP_LED,		20, "The FO link is init. and active"),
	status(AURORA_HARD_ERROR,	21, "Detected Aurora hard error"),
	status(AURORA_SOFT_ERROR,	22, "Detectec Aurora soft error"),
	status(AURORA_FRAME_ERROR,	23, "Detected Aurora frame error"),
	status(TAMPON_OVERFLOW,		24, "The internal FIFO has overflown"),
	status(SERIAL_DATA_AVAILABLE,	25, "Serial FIFO is not longer empty"),
	status(STATUS_CHANGED,		26, "The CCD status changed"),
	status(INTERRUPT_USER,		27, "Undocumented"),
	status(AUX_SLOT_64,		28, "Replica of SLOT_64 bit"),
	// Control register
	control(INTERRUPT_DMA_ON,	0,  "The end of DMA generates IRQ"),
	control(DMA_START,		1,  "Start DMA (auto-cleared)"),
	control(DMA_RAM,		2,  "DMA to/from internal RAM"),
	control(DMA_READ,		3,  "Data is moved from PC RAM"),
	control(INTERRUPT_SCATTER_GATHER_ON, 4, "The end of SG generates IRQ"),
	control(SCATTER_GATHER_START,	5,  "Start the SG (auto-cleared)"),
	control(TEST_COUNTER,		6,  "Data from count. instead of FIFO"),
	control(USE_BURST,		7,  "Use WRITE_INVALIDATE/READ_LINE"),
	control(DATA_FROM_PCI_FPGA,	10, "FIFO read count. instead of FO"),
	control(USE_64_BITS,		11, "Use whole 64-bit PCI bus"),
	control(RESET_SCATTER_GATHER,	12, "Stop the SG"),
	control(USER_RESET_NOT,		16, "Reset the DCMs"),
	control(RESET_LINK,		17, "Reset the Aurora link"),
	control2(MODE,			18, "Mode: 0=Normal, 1=Camera, 2=Cnt."),
	control(ENA_TX,			20, "Enable data Tx in Modes 1 & 2"),
	control(DECODE_ESPIA,		21, "Decode ESPIA protocol"),
	control(CLEAR_ERRORS,		22, "Clear status latched errors"),
	control(COMPTEUR_TEST_TAMPON,   23, "Undocumented"),
	control(USE_EOI,		24, "Abort the SG when receive EOI"),
	control(INTERRUPT_ON_SERIAL,	25, "Ser. FIFO not empty generate IRQ"),
	control(INTERRUPT_ON_SERIAL_CLEAR, 26, "Clear the serial IRQ bit"),
	control(INTERRUPT_ON_STATUS_CHANGE, 27, 
					    "CCD status change generate IRQ"),
	control(INTERRUPT_ON_STATUS_CHANGE_CLEAR, 28, 
					    "Clear the CCD status IRQ bit"),
	control(DO_NOT_RESET_FIFOS,	30, "Do not reset data FIFO on start"),
	control(ESPIA_125MHZ,		31, "Set link @ 2.5Gbps (125Mpix/s)"),
	// Pixel counter register
	{"PIXEL_COUNTER", ESPIA_PIXEL_COUNTER, 0xffffffff, 0,
					    "Last frame pixel count div. by 2"},
	// Serial status register
	serial(SERIAL_LINK_DATA, 0xff,  0,  "Serial link data byte"),
	serial(CAMERA_STATUS,    0xff,  8,  "Camera status"),
	serial(SERIAL_FIFO_EMPTY,   1,  16, "Serial FIFO is empty"),
	serial(SERIAL_FIFO_FULL,    1,  17, "Serial FIFO is full"),
};

#undef serial
#undef control2
#undef control
#undef status

static int espia_param_table_len = (sizeof(espia_param_table) / 
				    sizeof(espia_param_table[0]));

/*--------------------------------------------------------------------------
 * parameter data
 *--------------------------------------------------------------------------*/

int espia_get_param_data(espia_t dev, int *param_nr_ptr, 
			 struct espia_param **param_ptr)
{
	struct espia_dev *pdev; 
	int param_nr;

	FENTRY("espia_get_param_data");
	espia_get_check_dev(pdev, dev);
	espia_check_null(param_nr_ptr);

	param_nr = *param_nr_ptr;
	if (scdxipci_is(REQUEST, param_nr)) {
		*param_nr_ptr = espia_param_table_len;
		return ESPIA_OK;
	} else if ((param_nr < 0) || (param_nr >= espia_param_table_len)) {
		DEB_ERRORS("invalid param_nr %d, nr. params. is %d\n",
			   param_nr, espia_param_table_len);
		return -EINVAL;
	}

	espia_check_null(param_ptr);
	*param_ptr = &espia_param_table[param_nr];

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * read/write parameter
 *--------------------------------------------------------------------------*/

#define REG_SIZE	4
#define NR_REGS		(SCDXIPCI_REGS_SIZE / REG_SIZE)

static int espia_param_reg_arr(espia_t dev, int *param_arr, 
			       unsigned int *val_arr, int nr_param, 
			       unsigned int **reg_arr, 
			       unsigned int **reg_val, 
			       unsigned int **reg_mask)
{
	int i, idx, len, ret = 0;
	unsigned int val;
	unsigned long reg;
	struct espia_param *eparam;
	struct espia_dev *pdev; 

	FENTRY("espia_param_reg_arr");
	espia_get_check_dev(pdev, dev);

	*reg_val = NULL;
	if (reg_mask != NULL)
		*reg_mask = NULL;

	len = sizeof(**reg_arr);
	*reg_arr = (unsigned int *) calloc(NR_REGS, len);
	if (*reg_arr == NULL) {
		DEB_ERRORS("Error allocating reg. array (%d elements)\n", 
			   NR_REGS);
		ret = -ENOMEM;
		goto out;
	}

	*reg_val = (unsigned int *) calloc(NR_REGS, len);
	if (*reg_val == NULL) {
		DEB_ERRORS("Error allocating reg. val array (%d elements)\n", 
			   NR_REGS);
		ret = -ENOMEM;
		goto out;
	}

	if (reg_mask != NULL) {
		*reg_mask = (unsigned int *) calloc(NR_REGS, len);
		if (*reg_mask == NULL) {
			DEB_ERRORS("Error allocating reg. mask array "
				   "(%d elements)\n", NR_REGS);
			ret = -ENOMEM;
			goto out;
		}
	}
       
	for (i = 0; i < nr_param; i++) {
		idx = param_arr[i];
		if ((idx < 0) || (idx >= espia_param_table_len)) {
			DEB_ERRORS("Invalid param #%d: %d\n", i, idx);
			ret = -EINVAL;
			goto out;
		}
		eparam = &espia_param_table[idx];
		reg = eparam->reg / REG_SIZE;
		if (reg > NR_REGS) {
			DEB_ERRORS("Error: invalid param. %d reg. offset %d\n",
				   idx, eparam->reg);
			ret = SCDXIPCI_ERR_INTERNAL;
			goto out;
		}
		(*reg_arr)[reg] = 1;

		if (reg_mask == NULL)
			continue;
		val = val_arr[i] << eparam->shift;
		if ((val & ~eparam->mask) != 0) {
			DEB_ERRORS("Invalid value %d: 0x%x\n", i, val_arr[i]);
			ret = -EINVAL;
		} else if (((*reg_mask)[reg] & eparam->mask) != 0) {
			DEB_ERRORS("Error: reg. %d bits already requested: "
				   "0x%x\n", eparam->reg, eparam->mask);
			ret = -EINVAL;
		} else {
			(*reg_val)[reg]  |= val;
			(*reg_mask)[reg] |= eparam->mask;
		}
		if (ret < 0)
			goto out;
	}

 out:
	if (ret < 0) {
		if (*reg_arr)
			free(*reg_arr);
		if (*reg_val)
			free(*reg_val);
		if (reg_mask && *reg_mask)
			free(*reg_mask);
	}

	return ret;
}


static int espia_param_base(espia_t dev, int *param_arr, 
			    unsigned int *val_arr, int nr_param, int set)
{
	int ret, reg_off, i;
	unsigned int *reg_arr, *reg_val, *reg_mask;
	struct espia_param *eparam;
	struct espia_dev *pdev;

	FENTRY("espia_param_base");
	espia_get_check_dev(pdev, dev);
	espia_check_null2(param_arr, val_arr);

	ret = espia_param_reg_arr(dev, param_arr, val_arr, nr_param, &reg_arr,
				  &reg_val, set ? &reg_mask : NULL);
	if (ret < 0)
		return ret;

	
	for (i = 0; i < NR_REGS; i++) {
		if (reg_arr[i] == 0)
			continue;

		reg_off = i * REG_SIZE;
		if (set)
			ret = espia_write_register(dev, reg_off, &reg_val[i], 
						   reg_mask[i]);
		else
			ret = espia_read_register (dev, reg_off, &reg_val[i]);
		if (ret < 0)
			goto out;
	}

	for (i = 0; i < nr_param; i++) {
		eparam = &espia_param_table[param_arr[i]];
		reg_off = eparam->reg / REG_SIZE;
		val_arr[i] = (reg_val[reg_off] & eparam->mask) >> eparam->shift;
	}

 out:
	free(reg_arr);
	free(reg_val);
	if (set)
		free(reg_mask);

	return ret;
}

int espia_get_param(espia_t dev, int *param_arr, unsigned int *val_arr, 
		    int nr_param)
{
	return espia_param_base(dev, param_arr, val_arr, nr_param, PARAM_GET);
}

int espia_set_param(espia_t dev, int *param_arr, unsigned int *val_arr, 
		    int nr_param)
{
	return espia_param_base(dev, param_arr, val_arr, nr_param, PARAM_SET);
}


/*--------------------------------------------------------------------------
 * firmware data
 *--------------------------------------------------------------------------*/

static struct espia_firmware espia_firmware_table[] = {
	{"2004.06.18", 0x2fe8},
	{"2006.05.05", 0x4df5},
	{"2007.07.04", 0xa306},
	{"2007.09.27", 0xae05},
	{"2008.07.22", 0xb9fd},
};
static int espia_firmware_table_len = (sizeof(espia_firmware_table) / 
				       sizeof(espia_firmware_table[0]));


int espia_get_firmware_data(int *firmware_nr_ptr, 
			    struct espia_firmware **firmware_ptr)
{
	struct espia_dev *pdev = NULL; 
	int firmware_nr;

	FENTRY("espia_get_firmware_data");
	espia_check_null(firmware_nr_ptr);

	firmware_nr = *firmware_nr_ptr;
	if (scdxipci_is(REQUEST, firmware_nr)) {
		*firmware_nr_ptr = espia_firmware_table_len;
		return ESPIA_OK;
	} else if ((firmware_nr < 0) || 
                   (firmware_nr >= espia_firmware_table_len)) {
		DEB_ERRORS("invalid firmware_nr %d, nr. firmwares. is %d\n",
			   firmware_nr, espia_firmware_table_len);
		return -EINVAL;
	}

	espia_check_null(firmware_ptr);
	*firmware_ptr = &espia_firmware_table[firmware_nr];

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * hardware info
 *--------------------------------------------------------------------------*/

int espia_hw_info(espia_t dev, int action, struct scdxipci_hw_info *hw_info)
{
	struct espia_dev *pdev;

	FENTRY("espia_hw_info");
	espia_get_check_dev(pdev, dev);
	espia_check_null(hw_info);

	DEB_TRACE("Managing hardware info: action=%d\n", action);
	espia_check("scdxipci_hw_info", 
		    scdxipci_hw_info(pdev->dev, action, hw_info));
	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * reset link
 *--------------------------------------------------------------------------*/

int espia_reset_link(espia_t dev)
{
	struct espia_dev *pdev;

	FENTRY("espia_reset_link");
	espia_get_check_dev(pdev, dev);

	DEB_TRACE("reseting the link\n");
	espia_check("scdxipci_reset_link", scdxipci_reset_link(pdev->dev));
	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * serial read/write/flush
 *--------------------------------------------------------------------------*/

int espia_ser_read(espia_t dev, char *buffer, unsigned long *nr_bytes_ptr, 
		      unsigned long timeout)
{
	int ret;
	struct espia_dev *pdev;

	FENTRY("espia_ser_read");
	espia_get_check_dev(pdev, dev);
	espia_check_null(nr_bytes_ptr);  // buffer can be NULL -> just check
	
	DEB_PARAMS("buffer=0x%p, nr_bytes=%ld, timeout=%ld\n", 
		   buffer, *nr_bytes_ptr, timeout);
	ret = scdxipci_ser_read(pdev->dev, buffer, nr_bytes_ptr, NULL, 0,
				timeout);
	espia_check("scdxipci_ser_read", ret);
	DEB_PARAMS("received=%ld\n", *nr_bytes_ptr);
	return ESPIA_OK;
}

static void espia_print_term(char *term, unsigned long term_len)
{
	char *aux_term;
	struct espia_dev *pdev = NULL;

	FENTRY("espia_print_term");

	aux_term = (char *) malloc(term_len + 1);
	if (aux_term == NULL) {
		DEB_ERRORS("Error allocating term. copy (%ld bytes)\n", 
			   term_len + 1);
		return;
	}
	memcpy(aux_term, term, term_len);
	aux_term[term_len] = 0;
	DEB_PARAMS("term '%s' found!\n", aux_term);
	free(aux_term);
}

static char *espia_term_find(char *str, unsigned long str_len, 
			     char *term, unsigned long term_len)
{
	char *ptr, *end;

	end = str + str_len - (term_len - 1);
	for (ptr = str; ptr < end; ptr++) {
		ptr = memchr(ptr, term[0], end - ptr);
		if (ptr == NULL)
			return NULL;
		if (memcmp(ptr, term, term_len) == 0) {
			DEB(SCDXIPCI_DEB_PARAMS, 
			    espia_print_term(term, term_len));
			return ptr;
		}
	}
	return NULL;
}

int espia_ser_read_str(espia_t dev, char *buffer, unsigned long *nr_bytes_ptr, 
		       char *term, unsigned long term_bytes,
		       unsigned long timeout)
{
	int req_bytes, first, correct, ret;
	unsigned long len, tout, elap;
	struct espia_dev *pdev;
	struct timeval t0, t1;
	char *ptr;

	FENTRY("espia_ser_read_str");
	espia_get_check_dev(pdev, dev);

	if (term_bytes == 0) {
		DEB_TRACE("Empty terminator (term_bytes == 0).\n"
			  "  Calling espia_ser_read\n");
		return espia_ser_read(dev, buffer, nr_bytes_ptr, timeout);
	}
	espia_check_null3(buffer, nr_bytes_ptr, term);

	DEB_PARAMS("buffer=%p, nr_bytes=%ld, timeout=%ld, term_bytes=%ld\n", 
		   buffer, *nr_bytes_ptr, timeout, term_bytes);

	gettimeofday(&t0, NULL);

	req_bytes = *nr_bytes_ptr;
	*nr_bytes_ptr = 0;
	ptr = buffer;
	first = 1;
	correct = (!scdxipci_is(NO_BLOCK, timeout) && 
		   !scdxipci_is(BLOCK_FOREVER, timeout));
	tout = SCDXIPCI_NO_BLOCK;

	while (req_bytes > 0) {
		len = first ? (req_bytes - 1) : 1;
		gettimeofday(&t1, NULL);
		ret = espia_ser_read(dev, ptr, &len, tout);
		if ((ret == -ETIMEDOUT) && (*nr_bytes_ptr > 0))
			len = 0;
		else if (ret != ESPIA_OK)
			espia_check("espia_ser_read", ret);

		*nr_bytes_ptr += len;
		if (scdxipci_is(NO_BLOCK, timeout))
			break;
		ptr += len;
		if (espia_term_find(buffer, ptr - buffer, term, term_bytes))
			break;
		req_bytes -= len;
		DEB_PARAMS("req_bytes=%d\n", req_bytes);
		first = 0;

		if (correct) {
			elap = ((t1.tv_sec  - t0.tv_sec) * 1000000 + 
				(t1.tv_usec - t0.tv_usec));
			if (elap >= timeout) {
				if (*nr_bytes_ptr > 0)
					break;
				espia_check("espia_ser_read", -ETIMEDOUT);
			} else
				tout = timeout - elap;
		} else
			tout = timeout;
	}
	DEB_PARAMS("received=%ld\n", *nr_bytes_ptr);
	return ESPIA_OK;
}

int espia_ser_write(espia_t dev, char *buffer, unsigned long *nr_bytes_ptr, 
		    unsigned long block_size, unsigned long delay, int block)
{
	int ret, req_bytes = *nr_bytes_ptr;
	struct espia_dev *pdev;

	FENTRY("espia_ser_write");
	espia_get_check_dev(pdev, dev);
	espia_check_null2(buffer, nr_bytes_ptr);

	ret = scdxipci_ser_write(pdev->dev, buffer, nr_bytes_ptr, block_size,
				 delay, block);
	espia_check("scdxipci_ser_write", ret);
	DEB_PARAMS("buffer=0x%p, nr_bytes=%d, block_size=%ld, delay=%ld, "
		   "block=%d, written=%ld\n", buffer, req_bytes, block_size,
		   delay, block, *nr_bytes_ptr);
	return ESPIA_OK;
}

int espia_ser_flush(espia_t dev)
{
	char buffer[1024];
	unsigned long nr_bytes, tot_flush = 0;
	struct espia_dev *pdev;
	int ret;

	FENTRY("espia_ser_flush");
	espia_get_check_dev(pdev, dev);

	do {
		nr_bytes = sizeof(buffer);
		ret = espia_ser_read(dev, buffer, &nr_bytes, 0);
		if (ret != ESPIA_OK)
			return ret;
		tot_flush += nr_bytes;
	} while (nr_bytes > 0);
	DEB_PARAMS("flushed %ld bytes\n", tot_flush);

	return ESPIA_OK;
}

static void *espia_ser_rx_thread(struct espia_priv_cb_data *pcb_data)
{
	struct espia_dev *pdev = pcb_data->pdev;
	struct espia_cb_data *cb_data = &pcb_data->cb_data;
	struct espia_cb_serial *cb_serial = &cb_data->info.serial;
	espia_t dev = cb_data->dev;
	int ret, read_str, alloc_buff, cb_nr = cb_data->cb_nr;
	char *buffer;
	unsigned long *pbytes, len, timeout;

	FENTRY("espia_ser_rx_thread");
	read_str = (cb_serial->term_len > 0);
	DEB_TRACE("entry, cb_nr=%d, read_str=%d\n", cb_nr, read_str);

	buffer = cb_serial->buffer;
	len = cb_serial->buffer_len;
	alloc_buff = (buffer == NULL);
	if (alloc_buff) {
		buffer = (char *) malloc(len);
		if (buffer == NULL) {
			DEB_ERRORS("Error allocating buffer (%ld bytes)\n", 
				   len);
			return NULL;
		}
		cb_serial->buffer = buffer;
	} 
	pbytes = &cb_serial->nr_bytes;
	timeout = cb_data->timeout;
	while (pcb_data->active) {
		*pbytes = len;
		if (read_str) 
			ret = espia_ser_read_str(dev, buffer, pbytes,
						 cb_serial->term, 
						 cb_serial->term_len, timeout);
		else
			ret = espia_ser_read(dev, buffer, pbytes, timeout);
		if ((ret < 0) && (ret != SCDXIPCI_ERR_TIMEOUT))
			break;
		cb_data->ret = ret;
		DEB_TRACE("Calling cb: %ld bytes\n", *pbytes);
		if (cb_data->cb(cb_data) < 0)
			break;
	}

	if (alloc_buff) {
		free(cb_serial->buffer);
		cb_serial->buffer = NULL;
	}
		
	DEB_TRACE("exit");
	return NULL;
}


/*--------------------------------------------------------------------------
 * frame post-operation functions
 *--------------------------------------------------------------------------*/

int espia_set_frm_op_fn(espia_t dev, struct espia_frm_op_info *op_info, 
			unsigned long nr_op)
{
	struct espia_dev *pdev;
	void *aux_info;
	unsigned long prev_nr_op, len;

	FENTRY("espia_set_frm_op_fn");
	espia_get_check_dev(pdev, dev);

	prev_nr_op = pdev->nr_post_op;
	if ((nr_op == 0) != (prev_nr_op == 0)) {
		if (pdev->nr_buffer > 0) {
			DEB_ERRORS("Cannot set/remove frame post-op funct.\n");
			DEB_ERRORS("if buffers are already allocated.\n");
			return ESPIA_ERR_ALLOC;
		}
	}

	if (nr_op > 0) {
		len = nr_op * sizeof(*op_info);
		aux_info = malloc(len);
		if (aux_info == NULL) {
			DEB_ERRORS("Could not alloc. frm_op_data copy "
				   "(%ld bytes)\n", len);
			return -ENOMEM;
		}
		memcpy(aux_info, op_info, len);
	} else
		aux_info = NULL;

	pthread_mutex_lock(&pdev->mutex);

	if (pdev->post_op_info != NULL)
		free(pdev->post_op_info);
	pdev->post_op_info = (struct espia_frm_op_info *) aux_info;
	pdev->nr_post_op = nr_op;
	
	pthread_mutex_unlock(&pdev->mutex);

	return ESPIA_OK;
}

static int espia_alloc_buffer_post_op(espia_t dev)
{
	struct espia_dev *pdev;
	struct buffer_post_op_data *pop_data;
	unsigned long i, nr_buffer;
	void *ptr;

	FENTRY("espia_alloc_buffer_post_op");
	espia_get_check_dev(pdev, dev);

	nr_buffer = pdev->nr_buffer;
	ptr = calloc(nr_buffer, sizeof(*pop_data));
	if (ptr == NULL) {
		DEB_ERRORS("Error allocating post-op data for %ld buffers\n", 
			   nr_buffer);
		return -ENOMEM;
	}

	pop_data = (struct buffer_post_op_data *) ptr;
	for (i = 0; i < nr_buffer; i++)
		pthread_mutex_init(&pop_data[i].mutex, NULL);
	pdev->buffer_post_op = pop_data;
	return ESPIA_OK;
}

static int espia_init_buffer_post_op(espia_t dev)
{
	struct espia_dev *pdev;
	struct buffer_post_op_data *pop_data;
	struct img_frame_info finfo = {};
	unsigned long i;

	FENTRY("espia_init_buffer_post_op");
	espia_get_check_dev(pdev, dev);

	pop_data = pdev->buffer_post_op;
	finfo.frame_nr = finfo.round_count = SCDXIPCI_INVALID;
	for (i = 0; i < pdev->nr_buffer; i++, pop_data++) {
		finfo.buffer_nr = i;
		pop_data->last_frame = finfo;
		espia_buffer_info(dev, i, &pop_data->binfo);
	}

	return ESPIA_OK;
}

static inline
int espia_apply_one_frame_post_op(espia_t dev, 
				  void *frame_ptr, unsigned long acq_frame_nr)
{
	struct espia_dev *pdev;
	struct espia_frm_op_info *op_info;
	int op, ret = ESPIA_OK;
	espia_frm_op_fn fn;
	void *fn_data;

	FENTRY("espia_apply_one_frame_post_op");
	espia_get_check_dev(pdev, dev);

	op_info = pdev->post_op_info;
	for (op = 0; op < pdev->nr_post_op; op++, op_info++) {
		DEB_TRACE("Calling post_op fn %d: acq_frame=%ld\n", 
			  op, acq_frame_nr);
		fn = op_info->fn_set->fn;
		if (fn == NULL) {
			DEB_ERRORS("Warning: post-op %d function "
				   "is NULL!\n", op);
			continue;
		}
		fn_data = op_info->data;
		ret = fn(dev, frame_ptr, fn_data, acq_frame_nr);
		if (ret < 0) {
			DEB_ERRORS("Error: post-op %d function failed: "
				   "%s (%d)\n", op, espia_strerror(ret), ret);
			break;
		}
	}

	return ret;
}

static int espia_apply_buffer_post_op(espia_t dev, 
				      struct img_frame_info *finfo, 
				      struct buffer_post_op_data *pop_data)
{
	struct espia_dev *pdev;
	struct img_frame_info *last_finfo;
	struct img_buffer_info *binfo = &pop_data->binfo;
	unsigned long i, first_time, buffer_frames, last_frame_nr, frame_nr;
	unsigned long first_frame, nr_frames, frame_size, acq_frame_nr;
	char *frame_ptr, *buffer_ptr = (char *) finfo->buffer_ptr;
	int ret = ESPIA_OK;

	FENTRY("espia_apply_buffer_post_op");
	espia_get_check_dev(pdev, dev);

	frame_size = binfo->frame_size;
	buffer_frames = binfo->nr_frames;

	pthread_mutex_lock(&pop_data->mutex);

	last_finfo = &pop_data->last_frame;
	first_time = scdxipci_is(INVALID, last_finfo->frame_nr);
	if (first_time) {
		last_frame_nr  = -1;
	} else {
		last_frame_nr  = last_finfo->round_count * buffer_frames;
		last_frame_nr += last_finfo->frame_nr;
	}
	frame_nr = finfo->round_count * buffer_frames + finfo->frame_nr;
	first_frame = last_frame_nr + 1;
	nr_frames = frame_nr + 1 - first_frame;
	if (first_frame >= frame_nr + 1) { // TODO: round_count 32-bit wrap
		goto unlock;
	} else if (nr_frames > buffer_frames) {
		first_frame = frame_nr - buffer_frames + 1;
		nr_frames = buffer_frames;
	}

	acq_frame_nr = finfo->acq_frame_nr - (nr_frames - 1);
	for (i = 0; i < nr_frames; i++, acq_frame_nr++) {
		frame_nr = (first_frame + i) % buffer_frames;
		frame_ptr = buffer_ptr + frame_nr * frame_size;

		DEB_TRACE("Calling post_op fns: buffer=%ld, frame=%ld, "
			  "acq_frame=%ld\n", finfo->buffer_nr, frame_nr, 
			  acq_frame_nr);
		ret = espia_apply_one_frame_post_op(dev, frame_ptr, 
						    acq_frame_nr);
		if (ret < 0)
			break;
	}

	*last_finfo = *finfo;

 unlock:
	pthread_mutex_unlock(&pop_data->mutex);

	return ret;
}

static int espia_free_buffer_post_op(espia_t dev)
{
	struct espia_dev *pdev;

	FENTRY("espia_free_buffer_post_op");
	espia_get_check_dev(pdev, dev);

	free(pdev->buffer_post_op);
	pdev->buffer_post_op = NULL;

	return ESPIA_OK;
}

#define pixel_swap_interlace(buffer, type) \
{								\
	type tmp = *(((type *) (buffer)) + 1);			\
	*(((type *) (buffer)) + 1) = *((type *) (buffer));	\
	*((type *) (buffer)) = tmp;				\
}


static int espia_frm_swap_interlace(espia_t dev, void *frame_ptr, 
				    struct espia_frm_op_fn_data *op_data,
				    unsigned long acq_frame_nr)
{
	struct espia_frame_dim *fdim;
	char *frame = (char *) frame_ptr;
	unsigned long i, nr_pixels;

	if (!frame || !op_data)
		return -EINVAL;

	fdim = &op_data->frame_dim;
	nr_pixels = fdim->width * fdim->height;
	for (i = 0; i < nr_pixels; i += 2) {
		switch (fdim->depth) {
		case 1:
			pixel_swap_interlace(frame, unsigned char);
			break;
		case 2:
			pixel_swap_interlace(frame, unsigned short);
			break;
		case 4:
			pixel_swap_interlace(frame, unsigned long);
			break;
		}
		frame += 2 * fdim->depth;
	}

	return ESPIA_OK;
}

static int espia_frm_flip_vert(espia_t dev, void *frame_ptr, 
			       struct espia_frm_op_fn_data *op_data, 
			       unsigned long acq_frame_nr)
{
	struct espia_frame_dim *fdim;
	char *aux, *top, *bottom;
	unsigned long i, rows, cols, depth, row_len;

	if (!frame_ptr || !op_data)
		return -EINVAL;

	fdim = &op_data->frame_dim;
	cols = fdim->width;
	rows = fdim->height;
	depth = fdim->depth;
	row_len = cols * depth;
	
	aux = (char *) malloc(row_len);
	if (aux == NULL) {
		fprintf(stderr, "espia_frm_flip_vert: could not alloc "
			"aux. row: %ld bytes\n", row_len);
		return -ENOMEM;
	}

	top = (char *) frame_ptr;
	bottom = top + (rows - 1) * row_len;
	for (i = 0; i < rows / 2; i++) {
		memcpy(aux,    top,    row_len);
		memcpy(top,    bottom, row_len);
		memcpy(bottom, aux,    row_len);
		top    += row_len;
		bottom -= row_len;
	}

	free(aux);
	return ESPIA_OK;
}

static int espia_frm_check_null(espia_t dev, void *frame_ptr, 
				struct espia_frm_op_fn_data *op_data,
				unsigned long acq_frame_nr)
{
	unsigned long row, col, sum, first_null, nr_null, line_len;
	unsigned char *ptr = (unsigned char *) frame_ptr;
	struct espia_frame_dim *fdim;
	struct espia_dev *pdev;

	if (!frame_ptr || !op_data)
		return -EINVAL;

	fdim = &op_data->frame_dim;
	first_null = SCDXIPCI_INVALID;
	nr_null = 0;
	line_len = fdim->width * fdim->depth;
	for (row = 0; row < fdim->height; row++) {
		sum = 0;
		for (col = 0; col < line_len; col++, ptr++)
			sum += *ptr;
		if (sum == 0) {
			if (scdxipci_is(INVALID, first_null))
				first_null = row;
			nr_null++;
		}
	}

	if (nr_null > 0) {
		pdev = get_pdev("espia_frm_check_null", dev);
		int dev_nr = pdev ? pdev->dev_nr : -1;
		fprintf(stderr, "Dev #%d acq. frame #%ld has %ld NULL line(s) "
		       "starting from %ld\n", dev_nr, acq_frame_nr, nr_null, 
			first_null);
	}

	return ESPIA_OK;
}

static int espia_frm_vert_2_quad_alloc(struct espia_frame_dim *fdim,
				       char **buffer, int *buffer_len)
{
	int len;

	len = espia_frame_mem_size(fdim);
	if (len == 0)
		return -EINVAL;
	else if (len != *buffer_len) {
		if (*buffer_len > 0) {
			free(*buffer);
			*buffer = NULL;
			*buffer_len = 0;
		}

		*buffer = (char *) malloc(len);
		if (*buffer == NULL) {
			fprintf(stderr, "Error allocating %d bytes for "
				"VERT_2_QUAD post-op buffer\n", len);
			return -ENOMEM;
		}
		*buffer_len = len;
	}

	return ESPIA_OK;
}

static int espia_frm_vert_2_quad(espia_t dev, void *frame_ptr, 
				 struct espia_frm_op_fn_data *op_data,
				 unsigned long acq_frame_nr) 
{
	static char *buffer = NULL;
	static int buffer_len = 0;
	struct espia_frame_dim *fdim;
	unsigned long ret, line_len, col, line, line_off;
	unsigned short *pi, *p0, *p1, *p2, *p3;

	if (!frame_ptr || !op_data)
		return -EINVAL;

	fdim = &op_data->frame_dim;
	ret = espia_frm_vert_2_quad_alloc(fdim, &buffer, &buffer_len);
	if (ret < 0)
		return ret;

	line_len = fdim->width;
	pi = (unsigned short *) frame_ptr;
	p2 = (unsigned short *) buffer;
	p1 = p2 + line_len;
	p0 = p2 + line_len * fdim->height;
	p3 = p0 - line_len;

	for (col = 0; col < fdim->width / 2; col++) {
		p0--;
		p1--;
		for (line = 0; line < fdim->height / 2; line++) {
			line_off = line_len * line;
			*(p0 - line_off) = *pi++;
			*(p1 + line_off) = *pi++;
			*(p2 + line_off) = *pi++; 
			*(p3 - line_off) = *pi++;
		}
		p2++;
		p3++;
	}

	memcpy(frame_ptr, buffer, buffer_len);

	return ESPIA_OK;
}


static int lib_plugin_I_cb( espia_t dev, void *frame_ptr, 
                            struct espia_frm_op_fn_data *op_data,
                            unsigned long acq_frame_nr )
{
	int i, ret, fret;
	struct plugin_t *aplugin;
	struct lib_plugins_data *plugins_data = op_data->data;


	ret = ESPIA_OK;
	pthread_mutex_lock( &plugins_data->lock );
	/* or pthread_mutex_trylock() and return on error ??? */
	for( i=0; i<plugins_data->num_plugins_I; i++ ) {
		aplugin = &plugins_data->plugin_list_I[i];
		if( aplugin->active && aplugin->fun ) {
			fret = aplugin->fun( acq_frame_nr,
					     op_data->frame_dim.depth, 
					     op_data->frame_dim.width,
					     op_data->frame_dim.height,
					     frame_ptr, aplugin->userData);
			if ((fret < 0) && (ret == ESPIA_OK))
				ret = fret;
		}
	}
	pthread_mutex_unlock( &plugins_data->lock );

	return ret;
}


static struct espia_frm_op_fn_set espia_frm_op_table[] = {
	{"SWAP_INTERLACE", "Swap adyacent interlaced pixels", 
	 espia_frm_swap_interlace, NULL},
	{"FLIP_VERT", "Vertical image flip",
	 espia_frm_flip_vert, NULL},
	{"CHECK_NULL", "Check for NULL lines",
	 espia_frm_check_null, NULL},
	{"VERT_2_QUAD", "Reconst. vertically read quadrants",
	 espia_frm_vert_2_quad, NULL},
	{"ACQ_PLUGIN_I", "Acq. Lib. plugin type I",
	 lib_plugin_I_cb, NULL},  /* Keep ACQ_PLUGIN_I at the bottom! */
};

static int espia_frm_op_table_len = (sizeof(espia_frm_op_table) / 
				     sizeof(espia_frm_op_table[0]));

int espia_get_frm_op_fn_set(int *frm_op_nr_ptr, 
			    struct espia_frm_op_fn_set **fn_set_ptr)
{
	struct espia_dev *pdev = NULL; 
	int frm_op_nr;

	FENTRY("espia_get_frm_op_fn_set");
	espia_check_null(frm_op_nr_ptr);

	frm_op_nr = *frm_op_nr_ptr;
	if (scdxipci_is(REQUEST, frm_op_nr)) {
		*frm_op_nr_ptr = espia_frm_op_table_len;
		return ESPIA_OK;
	} else if ((frm_op_nr < 0) || 
                   (frm_op_nr >= espia_frm_op_table_len)) {
		DEB_ERRORS("invalid frm_op_nr %d, nr. frm_op. is %d\n",
			   frm_op_nr, espia_frm_op_table_len);
		return -EINVAL;
	}

	espia_check_null(fn_set_ptr);
	*fn_set_ptr = &espia_frm_op_table[frm_op_nr];

	return ESPIA_OK;
}


int espia_get_current_frm_op( espia_t dev, struct espia_frm_op_info op_info[], 
			        int *nr_op)
{
	long size;
	struct espia_dev *pdev;

	FENTRY("espia_get_current_frm_op");
	espia_get_check_dev(pdev, dev);

	pthread_mutex_lock(&pdev->mutex);

	*nr_op = pdev->nr_post_op;
	if( pdev->nr_post_op ) {
		size = pdev->nr_post_op * sizeof(struct espia_frm_op_info);
		memcpy( op_info, pdev->post_op_info, size );
	}
	
	pthread_mutex_unlock(&pdev->mutex);

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * buffer alloc/free/info
 *--------------------------------------------------------------------------*/

int espia_buffer_alloc(espia_t dev, unsigned long nr_buffer, 
		       unsigned long buffer_frames, unsigned long frame_size)
{
	struct espia_dev *pdev;
	int prot, flags, ret;
	unsigned long buffer_nr, len, i;

	FENTRY("espia_buffer_alloc");
	espia_get_check_dev(pdev, dev);

	if ((nr_buffer == 0) || (buffer_frames == 0) || (frame_size == 0)) {
		DEB_ERRORS("Invalid parameters: nr_buffer=%ld, "
			   "buffer_frames=%ld, frame_size=%ld\n", nr_buffer,
			   buffer_frames, frame_size);
		return -EINVAL;
	}

	if (pdev->nr_buffer > 0) {
		DEB_ERRORS("Cannot alloc buffers when already allocated.\n");
		return ESPIA_ERR_ALLOC;
	}

	len = frame_size;
	espia_check("scdxipci_frame_size", 
		    scdxipci_frame_size(pdev->dev, &len));

	pdev->frame_size = frame_size;
	pdev->buffer_frames = buffer_frames;

	prot = PROT_READ | PROT_WRITE;
	flags = MAP_SHARED;
	for (i = 0; i < nr_buffer; i++) {
		ret = scdxipci_map_buffer(pdev->dev, buffer_frames, 
					  prot, flags, &buffer_nr);
		if ((ret < 0) && (i > 0))
			espia_buffer_free(dev);
		espia_check("scdxipci_buffer_map", ret);
			    
		if (buffer_nr != i) {
			DEB_ERRORS("Drv buff nr %ld different from lib %ld\n", 
				   buffer_nr, i);
		}
		pdev->nr_buffer++;
	}

	// check if post-operation data is configured
	if (pdev->post_op_info != NULL) {
		ret = espia_alloc_buffer_post_op(dev);
		if (ret < 0) {
			espia_buffer_free(dev);
			return ret;
		}
	}

	DEB_PARAMS("Allocated %ld buffers, with %ld frames, with %ld bytes\n",
		   pdev->nr_buffer, pdev->buffer_frames, pdev->frame_size);
	return ESPIA_OK;
}

int espia_buffer_free(espia_t dev)
{
	struct espia_dev *pdev;
	int len, nr_buffer, i;

	FENTRY("espia_buffer_free");
	espia_get_check_dev(pdev, dev);

	nr_buffer = pdev->nr_buffer;
	if (nr_buffer == 0) {
		DEB_PARAMS("No buffer to free\n");
		return ESPIA_OK;
	}

	// avoid the threads access buffers that no longer exist
	espia_stop_acq(dev); 

	if (pdev->buffer_post_op != NULL)
		espia_free_buffer_post_op(dev);

	len = pdev->buffer_frames * pdev->frame_size;
	for (i = nr_buffer - 1; i >= 0; i--) {
		espia_check("scdxipci_buffer_unmap", 
			    scdxipci_unmap_buffer(pdev->dev, i));
		pdev->nr_buffer--;
	}

	DEB_PARAMS("Freed %d buffers of %d bytes each\n", nr_buffer, len);
	return ESPIA_OK;
}

int espia_buffer_info(espia_t dev, unsigned long buffer_nr, 
		      struct img_buffer_info *binfo)
{
	struct espia_dev *pdev;

	FENTRY("espia_buffer_info");
	espia_get_check_dev(pdev, dev);

	binfo->nr = buffer_nr;
	binfo->ptr = NULL;
	return espia_check("scdxipci_buffer_info", 
			    scdxipci_buffer_info(pdev->dev, binfo));
}

int espia_frame_address(espia_t dev, unsigned long buffer_nr, 
			unsigned long frame_nr, void **addr_ptr)
{
	int ret, acq_frame, frame_type;
	struct espia_dev *pdev;

	FENTRY("espia_frame_address");
	espia_get_check_dev(pdev, dev);
	espia_check_null(addr_ptr);

	acq_frame = (buffer_nr == ESPIA_ACQ_ANY);
	frame_type = acq_frame ? ACQ_FRAME_NR : BUFFER_FRAME_NR;
	espia_check_frame_nr(frame_nr, frame_type, REAL_FRAME_NR);
	if (acq_frame) {
		buffer_nr = (frame_nr / pdev->buffer_frames) % pdev->nr_buffer;
		frame_nr %= pdev->buffer_frames;
	}
	buffer_nr = espia_check_buffer(buffer_nr, REAL_BUFFER);
	ret = scdxipci_frame_address(pdev->dev, buffer_nr, frame_nr, addr_ptr);
	return espia_check("scdxipci_frame_address", ret);
}


/*--------------------------------------------------------------------------
 * cb thread start/stop
 *--------------------------------------------------------------------------*/

typedef void *espia_thread_func(struct espia_priv_cb_data *pcb_data);
typedef void *pthread_func(void *data);

static int espia_start_cb_thread(struct espia_priv_cb_data *pcb_data,
				 espia_thread_func *thread_func)
{
	int ret, cb_nr = priv_cb_nr(pcb_data);
	struct espia_dev *pdev = pcb_data->pdev;
	FENTRY("espia_start_cb_thread");

	if (pcb_data->thread_id) {
		DEB_ERRORS("Error: cb #%d thread alive!\n", cb_nr);
		return ESPIA_ERR_THREAD;
	}

	pcb_data->terminate = pcb_data->finished = 0;
	ret = pthread_create(&pcb_data->thread_id, NULL, 
			     (pthread_func *) thread_func, pcb_data);
	if (ret != 0) {
		DEB_ERRORS("Error creating cb #%d thread\n", cb_nr);
		pcb_data->thread_id = 0;
		return ESPIA_ERR_THREAD;
	}
	DEB_PARAMS("Started cb #%d thread (%ld)\n", cb_nr, 
		   pcb_data->thread_id);

	return ESPIA_OK;
}

static int espia_stop_cb_thread(struct espia_priv_cb_data *pcb_data)
{
	struct espia_dev *pdev = pcb_data->pdev;
	pthread_t thread_id = pcb_data->thread_id;
	FENTRY("espia_stop_cb_thread");

	DEB_PARAMS("stoping acq.\n");
	espia_check("scdxipci_stop_dma", scdxipci_stop_dma(pdev->dev));

	if (thread_id == 0)
		return ESPIA_OK;

	pthread_mutex_lock(pcb_data->mutex);
	if (!pcb_data->finished) {
		DEB_TRACE("Signaling termination to cb #%d thread (%ld)\n", 
			  priv_cb_nr(pcb_data), thread_id);
		pcb_data->terminate = 1;
		pthread_cond_signal(&pcb_data->cond);
	}
	pthread_mutex_unlock(pcb_data->mutex);

	DEB_TRACE("Joining cb #%d thread (%ld)\n", 
		  priv_cb_nr(pcb_data), thread_id);
	pthread_join(thread_id, NULL);
	pcb_data->thread_id = 0;

	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * check thread wait
 *--------------------------------------------------------------------------*/

static int espia_check_thread_wait(struct espia_priv_cb_data *pcb_data)
{
	struct espia_dev *pdev = pcb_data->pdev;
	unsigned long thread_id = pcb_data->thread_id;
	int ret;

	FENTRY("espia_check_thread_wait");

	DEB_TRACE("Checking cb #%d thread (%ld) entered wait queue\n", 
		  priv_cb_nr(pcb_data), thread_id);
	ret = scdxipci_check_wait(pdev->dev, thread_id, START_TIMEOUT);
	return espia_check("scdxipci_check_wait", ret);
}

/*--------------------------------------------------------------------------
 * cb thread check active
 *--------------------------------------------------------------------------*/

static int espia_thread_check_active(struct espia_priv_cb_data *pcb_data)
{
	int active, cb_nr = priv_cb_nr(pcb_data);
	struct espia_dev *pdev = pcb_data->pdev;
	FENTRY("espia_thread_check_active");

	active = 1;

	pthread_mutex_lock(pcb_data->mutex);
	if (!pcb_data->terminate && !pcb_data->active) {
		DEB_TRACE("Waiting active cb_nr=%d\n", cb_nr);
		pthread_cond_wait(&pcb_data->cond, pcb_data->mutex);
	}
	if (pcb_data->terminate) {
		DEB_TRACE("Terminated cb_nr=%d\n", cb_nr);
		active = 0;
	}
	pthread_mutex_unlock(pcb_data->mutex);

	return active;
}


/*--------------------------------------------------------------------------
 * acq start/stop and threads
 *--------------------------------------------------------------------------*/

static void espia_get_cb_finfo(struct espia_priv_cb_data *pcb_data)
{
	struct espia_cb_data *cb_data = &pcb_data->cb_data;
	struct img_frame_info *req_finfo = &cb_data->info.acq.req_finfo;
	struct img_frame_info *cb_finfo  = &cb_data->info.acq.cb_finfo;
	struct espia_dev *pdev = pcb_data->pdev;

	*cb_finfo = *req_finfo;
	switch (espia_cb_each(req_finfo)) {
	case ESPIA_EACH_BUFFER:
		cb_finfo->buffer_nr = pdev->start_buffer + pcb_data->count;
		cb_finfo->buffer_nr %= pdev->nr_buffer;
		cb_finfo->round_count = pcb_data->count / pdev->nr_buffer;
		cb_finfo->frame_nr = pdev->buffer_frames - 1;
		break;
	case ESPIA_EACH_FRAME:
		cb_finfo->acq_frame_nr = pcb_data->count;
	}
}

static void *espia_acq_thread(struct espia_priv_cb_data *pcb_data)
{
	struct espia_cb_data *cb_data = &pcb_data->cb_data;
	struct espia_dev *pdev = pcb_data->pdev;
	struct img_frame_info *cb_finfo;
	espia_t dev = cb_data->dev;
	struct buffer_post_op_data *pop_data;
	unsigned long last_frame_nr, buffer_nr, timeout = cb_data->timeout;
	int ret, endless, aborted, end, cb_nr = cb_data->cb_nr;

	FENTRY("espia_acq_thread");
	DEB_TRACE("entry, cb_nr=%d\n", cb_nr);
	
	cb_finfo = &cb_data->info.acq.cb_finfo;
	endless = (pdev->acq_frame_nr == 0);
	last_frame_nr = pdev->acq_frame_nr - 1;

	pop_data = pdev->buffer_post_op;

	pcb_data->count = 0;
	while (espia_thread_check_active(pcb_data)) {
		espia_get_cb_finfo(pcb_data);

		ret = espia_get_frame(dev, cb_finfo, timeout);
		if (!pcb_data->active)
			continue;
		else if (ret == SCDXIPCI_ERR_WONTARR)
			break;
		cb_data->ret = ret;

		aborted = finished_espia_frame_info(cb_finfo, cb_data->ret);
		if (aborted)
			DEB_TRACE("Aborted!\n");

		buffer_nr = cb_finfo->buffer_nr;
		if ((ret >= 0) && pop_data && 
		    !scdxipci_is(INVALID, buffer_nr)) {
			ret = espia_apply_buffer_post_op(dev, cb_finfo, 
							 &pop_data[buffer_nr]);
			if (ret != ESPIA_OK)
				cb_data->ret = ret;
		}

		ret = cb_data->cb(cb_data);

		end = aborted;
		if (ret < 0) {
			DEB_ERRORS("User cb #%d returned %d: %s\n", cb_nr,
				   ret, espia_strerror(ret));
			if (ret == ESPIA_ERR_ABORT) {
				DEB_TRACE("Acq. aborted by user cb #%d!\n", 
					  cb_nr);
				scdxipci_stop_dma(pdev->dev);
			}
			end = 1;
		}
		end |= !endless && (cb_finfo->acq_frame_nr == last_frame_nr);
		if (end)
			break;

		if (cb_data->ret != SCDXIPCI_ERR_TIMEOUT)
			pcb_data->count++;
	}

	pthread_mutex_lock(pcb_data->mutex);
	DEB_TRACE("Finished cb_nr=%d\n", cb_nr);
	pcb_data->finished = 1;
	pthread_mutex_unlock(pcb_data->mutex);

	return NULL;
}

static int espia_start_acq_threads(struct espia_dev *pdev)
{
	struct espia_priv_cb_data *pcb_data;
	espia_t dev = dev_from_pdev(pdev);
	int i, ret = ESPIA_OK;

	FENTRY("espia_start_acq_threads");

	// first start all the threads
	for (i = 0; i < pdev->nr_cb; i++) {
		pcb_data = pdev->cbs[i];
		if (!priv_cb_is_acq(pcb_data))
			continue;

		ret = espia_start_cb_thread(pcb_data, espia_acq_thread);
		if (ret != ESPIA_OK)
			goto out;
	}

	// then check they are all waiting for a frame
	for (i = 0; i < pdev->nr_cb; i++) {
		pcb_data = pdev->cbs[i];
		if (!priv_cb_is_acq(pcb_data) || !pcb_data->active)
			continue;

		ret = espia_check_thread_wait(pcb_data);
		if (ret != ESPIA_OK) 
			goto out;
	}

 out:
	if (ret != ESPIA_OK) {
		DEB_ERRORS("Aborting all acq. cbs!\n");
		espia_stop_acq(dev);
	}

	return ret;
}

int espia_start_acq(espia_t dev, unsigned long start_buffer, 
		    unsigned long nr_frames, unsigned long timeout)
{
	struct espia_dev *pdev;
	struct img_frame_info finfo;
	int ret;

	FENTRY("espia_start_acq");
	espia_get_check_dev(pdev, dev);

	// be sure no acquisition is running, and get acq_run_nr
	espia_check("espia_stop_acq", espia_stop_acq(dev));
	ret = espia_check("espia_acq_active",
			  espia_acq_active(dev, &pdev->acq_run_nr));
	if (ret != 0) {
		DEB_ERRORS("Error!! Acquisition still running after stop!!\n");
		return ESPIA_ERR_DIDNOTSTOP;
	}

	start_buffer = espia_check_buffer(start_buffer, REAL_BUFFER);
	pdev->start_buffer = start_buffer;
	pdev->acq_frame_nr = nr_frames;

	// initialise post-op data, if any
	if (pdev->buffer_post_op != NULL)
		espia_init_buffer_post_op(dev);

	// only active cbs will have a "working" thread
	espia_check("espia_start_acq_threads", espia_start_acq_threads(pdev));

	finfo.buffer_nr = start_buffer;
	finfo.acq_frame_nr = nr_frames;
	DEB_PARAMS("starting acq. start_buffer=%ld, nr_frames=%ld, "
		   "timeout=%ld\n", start_buffer, nr_frames, timeout);
	return espia_check("scdxipci_start_dma", 
			   scdxipci_start_dma(pdev->dev, &finfo, timeout));
}


int espia_stop_acq(espia_t dev)
{
	int i;
	struct espia_priv_cb_data *pcb_data;
	struct espia_dev *pdev;
	FENTRY("espia_stop_acq");
	espia_get_check_dev(pdev, dev);

	DEB_PARAMS("stoping acq.\n");
	espia_check("scdxipci_stop_dma", scdxipci_stop_dma(pdev->dev));

	for (i = 0; i < pdev->nr_cb; i++) {
		pcb_data = pdev->cbs[i];
		if (priv_cb_is_acq(pcb_data))
			espia_stop_cb_thread(pcb_data);
	}
	return ESPIA_OK;
}

int espia_acq_active(espia_t dev, unsigned long *acq_run_nr_ptr)
{
	int ret;
	struct espia_dev *pdev;
	FENTRY("espia_acq_active");
	espia_get_check_dev(pdev, dev);

	ret = espia_check("scdxipci_dma_active", 
			  scdxipci_dma_active(pdev->dev, acq_run_nr_ptr));
	DEB_PARAMS("ret=%d\n", ret);
	return ret;
}

/*--------------------------------------------------------------------------
 * get frame
 *--------------------------------------------------------------------------*/

int espia_get_frame(espia_t dev, struct img_frame_info *finfo,
		    unsigned long timeout)
{
	struct espia_dev *pdev;
	
	FENTRY("espia_get_frame");
	espia_get_check_dev(pdev, dev);

	finfo->acq_run_nr = pdev->acq_run_nr;
	DEB_PARAMS("getting info buffer=%ld, frame_nr=%ld, count=%ld, "
		   "acq_frame_nr=%ld, acq_run_nr=%ld, timeout=%ld\n", 
		   finfo->buffer_nr, finfo->frame_nr, 
		   finfo->round_count, finfo->acq_frame_nr, finfo->acq_run_nr,
		   timeout);
	return espia_check("scdxipci_get_frame", 
			   scdxipci_get_frame(pdev->dev, finfo, timeout));
}


/*--------------------------------------------------------------------------
 * CCD status
 *--------------------------------------------------------------------------*/

int espia_ccd_status(espia_t dev, unsigned char *status_ptr, 
		     unsigned long timeout)
{
	struct espia_dev *pdev;
	int ret;

	FENTRY("espia_ccd_status");
	espia_get_check_dev(pdev, dev);
	espia_check_null(status_ptr);

	ret = scdxipci_ccd_status(pdev->dev, status_ptr, timeout);
	DEB_PARAMS("status=0x%02x, timeout=%ld\n", *status_ptr, timeout);
	return espia_check("scdxipci_ccd_status", ret);
}

static void *espia_status_thread(struct espia_priv_cb_data *pcb_data)
{
	struct espia_dev *pdev = pcb_data->pdev;
	struct espia_cb_data *cb_data = &pcb_data->cb_data;
	struct espia_cb_status *cb_status = &cb_data->info.status;
	espia_t dev = cb_data->dev;
	int ret, cb_nr = cb_data->cb_nr;

	FENTRY("espia_status_thread");
	DEB_TRACE("entry, cb_nr=%d\n", cb_nr);

	while (pcb_data->active) {
		ret = espia_ccd_status(dev, &cb_status->ccd_status,
				       cb_data->timeout);
		if ((ret < 0) && (ret != SCDXIPCI_ERR_TIMEOUT))
			break;
		cb_data->ret = ret;
		DEB_TRACE("Calling cb: status=0x%02x (%d)\n", 
			  cb_status->ccd_status, cb_status->ccd_status);
		if (cb_data->cb(cb_data) < 0)
			break;
	}

	DEB_TRACE("exit");
	return NULL;
}


/*--------------------------------------------------------------------------
 * register/get/unregister callback
 *--------------------------------------------------------------------------*/

static int espia_check_acq_cb(struct espia_dev *pdev, 
			      struct espia_cb_data *cb_data,
			      struct espia_cb_data *reg_cb_data, int what)
{
	struct img_frame_info *finfo = &cb_data->info.acq.req_finfo;
	struct img_frame_info *reg_finfo;

	reg_finfo = reg_cb_data ? &reg_cb_data->info.acq.req_finfo : NULL;

	FENTRY("espia_check_acq_cb");
	switch (what) {
	case CB_CHECK_INTEG:
		espia_check_buffer(finfo->buffer_nr, ANY_BUFFER);
		espia_check_frame_nr(finfo->frame_nr, BUFFER_FRAME_NR, 
				     ANY_FRAME_NR);
		espia_check_frame_nr(finfo->acq_frame_nr, ACQ_FRAME_NR, 
				     ANY_FRAME_NR);
		if ((finfo->buffer_nr == ESPIA_ACQ_EACH) && 
		    (finfo->acq_frame_nr == ESPIA_ACQ_EACH)) {
			DEB_ERRORS("Error: both buffer_nr and acq_frame_nr = "
				   "EACH\n");
			return ESPIA_ERR_EACH;
		}
		break;
	case CB_CHECK_REG:
		if (memcmp(reg_finfo, finfo, sizeof(*finfo)) == 0)
			return ESPIA_ERR_CBREG;
		break;
	case CB_CHECK_FINAL:
		// do not allow new cb during an acq: realloc of the 
		// priv_cb_data will crash those already running
		if (espia_acq_active(cb_data->dev, NULL) > 0)
			return SCDXIPCI_ERR_RUNNING;
	}

	return ESPIA_OK;
}

static int espia_check_one_cb(struct espia_dev *pdev, 
			      struct espia_cb_data *cb_data,
			      struct espia_cb_data *reg_cb_data, int what)
{
	FENTRY("espia_check_one_cb");
	switch (cb_data->type) {
	case ESPIA_CB_ACQ:
		return espia_check_acq_cb(pdev, cb_data, reg_cb_data, what);

	case ESPIA_CB_CCD_STATUS:
	case ESPIA_CB_SERIAL_TX:
	case ESPIA_CB_SERIAL_RX:
		// allow only one cb of these types
		if (what == CB_CHECK_REG)
			return ESPIA_ERR_CBREG;
		return ESPIA_OK;

	default:
		DEB_ERRORS("Error: invalid CB type: %d\n", cb_data->type);
		return ESPIA_ERR_CBTYPE;
	}
}

static int espia_check_callback(struct espia_dev *pdev, 
				struct espia_cb_data *cb_data, int *empty)
{
	struct espia_priv_cb_data *pcb_data;
	struct espia_cb_data *reg_cb_data, aux1, aux2;
	int cb_nr, ret;

	FENTRY("espia_check_callback");
	if (cb_data->cb == NULL) {
		DEB_ERRORS("Error: callback function cannot be NULL\n");
		return ESPIA_ERR_CBPAR;
	} else if (cb_data->timeout == 0) {
		DEB_ERRORS("Error: timeout cannot be 0\n");
		return ESPIA_ERR_CBPAR;
	}

	// check CB data integrity
	ret = espia_check_one_cb(pdev, cb_data, NULL, CB_CHECK_INTEG);
	if (ret != ESPIA_OK)
		return ret;

	cb_data->dev = pdev->dev;

	aux1 = *cb_data;
	memset(&aux1.info, 0, sizeof(aux1.info));
	aux1.ret = 0;

	*empty = -1;
	for (cb_nr = 0; cb_nr < pdev->nr_cb; cb_nr++) {
		pcb_data = pdev->cbs[cb_nr];
		if (pcb_data == NULL) {
			if (*empty == -1)
				*empty = cb_nr;
			continue;
		}
		reg_cb_data = &pcb_data->cb_data;
		aux2 = *reg_cb_data;
		memset(&aux2.info, 0, sizeof(aux2.info));
		aux2.ret = 0;
		if (memcmp(&aux1, &aux2, sizeof(aux1)) != 0)
			continue;

		// check CB is not registered
		ret = espia_check_one_cb(pdev, cb_data, reg_cb_data, 
					 CB_CHECK_REG);
		if (ret == ESPIA_ERR_CBREG)
			DEB_ERRORS("Cb already registered: #%d\n", cb_nr);
		if (ret != ESPIA_OK)
			return ret;
	}

	// final check: more complex errors
	return espia_check_one_cb(pdev, cb_data, NULL, CB_CHECK_FINAL);
}

static int espia_cblist_grow(struct espia_dev *pdev)
{
	int cb_nr, len;
	void *ptr;

	FENTRY("espia_cblist_grow");

	cb_nr = pdev->nr_cb + 1;
	len = cb_nr * sizeof(*pdev->cbs);
	ptr = realloc(pdev->cbs, len);
	if (ptr == NULL) {
		DEB_ERRORS("Error (re)allocating list of %d cbs (%d bytes)\n",
			   cb_nr, len);
		return ESPIA_ERR_CBMEM;
	}
	pdev->cbs = (struct espia_priv_cb_data **) ptr;
	pdev->nr_cb = cb_nr;
	DEB_TRACE("cb list len: %d\n", pdev->nr_cb);
	return ESPIA_OK;
}

static int espia_register_ser_rx_cb(struct espia_priv_cb_data *pcb_data)
{
	struct espia_cb_serial *cb_serial = &pcb_data->cb_data.info.serial;
	struct espia_dev *pdev = pcb_data->pdev;
	unsigned long term_len = cb_serial->term_len;
	char *term;
	FENTRY("espia_register_ser_rx_cb");

	if (cb_serial->buffer_len == 0)
		cb_serial->buffer_len = ESPIA_SER_DEF_LEN;
	if (term_len > 0) {
		term = malloc(term_len);
		if (term == NULL) {
			DEB_ERRORS("Error allocating serial Rx term "
				   "(%ld bytes)\n", term_len);
			return  ESPIA_ERR_CBMEM;
		}
		strncpy(term, cb_serial->term, term_len);
		cb_serial->term = term;
	} else
		cb_serial->term = NULL;
	return ESPIA_OK;
}

int espia_register_callback(espia_t dev, struct espia_cb_data *cb_data,
			    int *cb_nr_ptr)
{
	struct espia_dev *pdev;
	struct espia_priv_cb_data *pcb_data;
	int cb_nr, len, ret, empty = 0;

	FENTRY("espia_register_callback");
	espia_get_check_dev(pdev, dev);
	espia_check_null2(cb_data, cb_nr_ptr);

	ret = espia_check_callback(pdev, cb_data, &empty);
	if (ret < 0)
		return ret;
	
	if (empty == -1) {
		ret = espia_cblist_grow(pdev);
		if (ret < 0)
			return ret;
		cb_nr = pdev->nr_cb - 1;
	} else
		cb_nr = empty;

	len = sizeof(*pcb_data);
	pcb_data = (struct  espia_priv_cb_data *) malloc(len);
	if (pcb_data == NULL) {
		DEB_ERRORS("Error allocating cb data (%d bytes)\n", len);
		return ESPIA_ERR_CBMEM;
	}
	memset(pcb_data, 0, len);
	pcb_data->pdev = pdev;
	pcb_data->cb_data = *cb_data;
	pcb_data->cb_data.cb_nr = cb_nr;
	pcb_data->mutex = &pdev->mutex;
	pthread_cond_init(&pcb_data->cond, NULL);

	switch (cb_data->type) {
	case ESPIA_CB_SERIAL_RX:
		espia_check("espia_register_ser_rx_cb",
			    espia_register_ser_rx_cb(pcb_data));
		break;
	}

	pdev->cbs[cb_nr] = pcb_data;
	
	*cb_nr_ptr = cb_nr;
	DEB_PARAMS("registring cb #%d\n", *cb_nr_ptr);
	return ESPIA_OK;
}

int espia_get_callback(espia_t dev, int cb_nr, 
		       struct espia_cb_data **cb_data_ptr)
{
	struct espia_dev *pdev;
	struct espia_priv_cb_data *pcb_data = NULL;

	FENTRY("espia_get_callback");
	espia_get_check_dev(pdev, dev);
	espia_check_null(cb_data_ptr);

	if ((cb_nr >= 0) && (cb_nr < pdev->nr_cb))
		pcb_data = pdev->cbs[cb_nr];
	if (pcb_data == NULL) {
		DEB_ERRORS("Cb #%d not registered\n", cb_nr);
		return ESPIA_ERR_CBREG;
	}
	*cb_data_ptr = &pcb_data->cb_data;
	DEB_PARAMS("retreiving cb #%d\n", cb_nr);
	return ESPIA_OK;
}

static int espia_acq_cb_active(struct espia_priv_cb_data *pcb_data, 
			       int active)
{
	struct espia_dev *pdev = pcb_data->pdev;
	FENTRY("espia_acq_cb_active");
	DEB_PARAMS("%sabling acq. callback #%d\n", active ? "En" : "Dis",
		   priv_cb_nr(pcb_data));
	pcb_data->active = (active != 0);
	if (active)
		pthread_cond_signal(&pcb_data->cond);

	return ESPIA_OK;
}


static int espia_status_cb_active(struct espia_priv_cb_data *pcb_data, 
				  int active)
{
	int ret;
	struct espia_dev *pdev = pcb_data->pdev;
	FENTRY("espia_status_cb_active");
	DEB_PARAMS("%sabling status callback #%d\n", active ? "En" : "Dis",
		   priv_cb_nr(pcb_data));
	if (active) {
		pcb_data->active = 1;
		ret = espia_start_cb_thread(pcb_data, espia_status_thread);
		if (ret != 0) {
			pcb_data->active = 0;
			return ESPIA_ERR_THREAD;
		}
	} else {
		espia_stop_cb_thread(pcb_data);
		pcb_data->active = 0;
	}
	return ESPIA_OK;
}


static int espia_ser_rx_cb_active(struct espia_priv_cb_data *pcb_data, 
				  int active)
{
	int cb_nr, ret;
	struct espia_dev *pdev = pcb_data->pdev;
	struct espia_priv_cb_data *aux;
				  
	FENTRY("espia_ser_rx_cb_active");
	DEB_PARAMS("%sabling serial Rx callback #%d\n", active ? "En" : "Dis",
		   priv_cb_nr(pcb_data));
	if (active) {
		for (cb_nr = 0; cb_nr < pdev->nr_cb; cb_nr++) {
			aux = pdev->cbs[cb_nr];
			if (!aux || 
			    (aux->cb_data.type != ESPIA_CB_SERIAL_RX) ||
			    (aux->thread_id == 0))
				continue;
			// only one serial Rx cb active per dev
			DEB_ERRORS("Serial Rx cb #%d already active\n", cb_nr);
			return ESPIA_ERR_CBACT;
		}

		pcb_data->active = 1;
		ret = espia_start_cb_thread(pcb_data, espia_ser_rx_thread);
		if (ret != 0) {
			pcb_data->active = 0;
			return ESPIA_ERR_THREAD;
		}
	} else {
		espia_stop_cb_thread(pcb_data);
		pcb_data->active = 0;
	}

	return ESPIA_OK;
}


int espia_callback_active(espia_t dev, int cb_nr, int active)
{
	struct espia_dev *pdev;
	struct espia_priv_cb_data *pcb_data = NULL;
	int prev_active, ret = ESPIA_OK;

	FENTRY("espia_callback_active");
	espia_get_check_dev(pdev, dev);

	if ((cb_nr >= 0) && (cb_nr < pdev->nr_cb))
		pcb_data = pdev->cbs[cb_nr];
	if (pcb_data == NULL) {
		DEB_ERRORS("Cb #%d not registered\n", cb_nr);
		return ESPIA_ERR_CBREG;
	}

	pthread_mutex_lock(pcb_data->mutex);
	prev_active = pcb_data->active;
	if (!scdxipci_is(REQUEST, active) && 
            ((active != 0) != pcb_data->active)) {
		switch (pcb_data->cb_data.type) {
		case ESPIA_CB_ACQ:
			ret = espia_acq_cb_active(pcb_data, active);
			break;
		case ESPIA_CB_CCD_STATUS:
			ret = espia_status_cb_active(pcb_data, active);
			break;
		case ESPIA_CB_SERIAL_RX:
			ret = espia_ser_rx_cb_active(pcb_data, active);
			break;
		}
	}
	pthread_mutex_unlock(pcb_data->mutex);
	DEB_PARAMS("Cb #%d, active=%d, prev_active=%d, ret=%d\n", 
		   cb_nr, active, prev_active, ret);
	return (ret == ESPIA_OK) ? prev_active : ret;
}

int espia_unregister_callback(espia_t dev, int cb_nr)
{
	struct espia_dev *pdev;
	struct espia_priv_cb_data *pcb_data = NULL;
	union espia_cb_info *cb_info;
	void *ptr;

	FENTRY("espia_unregister_callback");
	espia_get_check_dev(pdev, dev);

	if ((cb_nr >= 0) && (cb_nr < pdev->nr_cb))
		pcb_data = pdev->cbs[cb_nr];
	if (pcb_data == NULL) {
		DEB_ERRORS("Cb #%d not registered\n", cb_nr);
		return ESPIA_ERR_CBREG;
	}

	// be sure all the threads are stopped for the requested cb
	espia_check("espia_callback_active", 
		    espia_callback_active(dev, cb_nr, 0));
	cb_info = &pcb_data->cb_data.info;
	switch (pcb_data->cb_data.type) {
	case ESPIA_CB_ACQ:
		espia_stop_cb_thread(pcb_data);
		break;
	case ESPIA_CB_SERIAL_RX:
		if (cb_info->serial.term != NULL)
			free(cb_info->serial.term);
		break;
	}

	free(pcb_data);
	pdev->cbs[cb_nr] = NULL;
	DEB_PARAMS("unregistered cb #%d\n", cb_nr);
	while ((pdev->nr_cb > 0) && (pdev->cbs[pdev->nr_cb - 1] == NULL)) {
		ptr = realloc(pdev->cbs, --pdev->nr_cb * sizeof(pcb_data));
		pdev->cbs = (struct espia_priv_cb_data **) ptr;
		DEB_TRACE("cb list len: %d\n", pdev->nr_cb);
	}
	return ESPIA_OK;
}


/*--------------------------------------------------------------------------
 * SG table
 *--------------------------------------------------------------------------*/

int espia_set_sg(espia_t dev, int dev_idx, struct scdxipci_sg_table *sg)
{
	struct espia_dev *pdev;

	FENTRY("espia_set_sg");
	espia_get_check_dev(pdev, dev);
	espia_check_null(sg);
	DEB_PARAMS("dev_idx=%d, sg-ptr=0x%p, sg-len=%ld\n", 
		   dev_idx, sg->ptr, sg->len);

	return espia_check("scdxipci_set_sg", 
			   scdxipci_set_sg(pdev->dev, dev_idx, sg));
}

static int espia_alloc_sg_list(struct espia_dev *pdev, int nr_dev, 
			       struct scdxipci_sg_table **sg_list_ptr)
{
	int len = sizeof(struct scdxipci_sg_table);

	FENTRY("espia_alloc_sg_list");
	*sg_list_ptr = (struct scdxipci_sg_table *) calloc(nr_dev, len);
	if (*sg_list_ptr == NULL) {
		DEB_ERRORS("error allocating %d SG tables (%d bytes each)\n",
			   nr_dev, len);
		return -ENOMEM;
	}
	return ESPIA_OK;
}

static int espia_check_roi(espia_t dev, struct espia_frame_dim *frame_dim,
			   struct espia_roi *roi)
{
	struct espia_dev *pdev;

	FENTRY("espia_check_roi");
	espia_get_check_dev(pdev, dev);

	if ((roi->width == 0) || (roi->left >= frame_dim->width) || 
	    (roi->left + roi->width > frame_dim->width)) {
		DEB_ERRORS("Invalid RoI left (%ld) or width (%ld) value\n",
			   roi->left, roi->width);
		return ESPIA_ERR_ROI_INVAL;
	}
	if ((roi->height == 0) || (roi->top >= frame_dim->height) || 
	    (roi->top + roi->height > frame_dim->height)) {
		DEB_ERRORS("Invalid RoI top (%ld) or height (%ld) value\n",
			   roi->top, roi->height);
		return ESPIA_ERR_ROI_INVAL;
	}
	if ((((roi->left  * frame_dim->depth) % 4) != 0) ||
	    (((roi->width * frame_dim->depth) % 4) != 0)) {
		DEB_ERRORS("Invalid RoI left (%ld) or width (%ld): must be "
			   "DWORD aligned\n", roi->left, roi->width);
		return SCDXIPCI_ERR_DMAALIGN;
	}

	return ESPIA_OK;
}

static int espia_create_sg_roi(espia_t dev, struct espia_frame_dim *frame_dim,
			       struct espia_roi *roi, 
			       struct scdxipci_sg_table **sg_list_ptr, 
			       int *nr_dev_ptr)
{
	struct espia_dev *pdev;
	struct scdxipci_sg_table *sg;
	struct scdxipci_sg_block *sg_desc;
	unsigned int width, height, right, bottom, sg_blocks, block_len, addr;
	unsigned int row, table_len, roi_reg, reg_size[3], good_reg, reg;
	unsigned int page_size, nr_pages, i, offset, aux_len, depth;
	int ret;

	FENTRY("espia_create_sg_roi");
	espia_get_check_dev(pdev, dev);

	if (roi != NULL) {
		ret = espia_check_roi(dev, frame_dim, roi);
		if (ret < 0)
			return ret;
	}

	*nr_dev_ptr = 1;
	ret = espia_alloc_sg_list(pdev, *nr_dev_ptr, sg_list_ptr);
	if ((ret < 0) || (roi == NULL))
		return ret;
	
	sg = *sg_list_ptr;

	right  = roi->left + roi->width;
	bottom = roi->top  + roi->height;
	width  = frame_dim->width;
	height = frame_dim->height;
	depth  = frame_dim->depth;

	// calculate the SG table length
	sg_blocks = height - roi->height; // 1 blk / full unused line
	roi_reg = 0;
	if (roi->left > 0)		  // 1 blk / roi line: unused on left
		reg_size[roi_reg++] = roi->left;
	good_reg = roi_reg;
	reg_size[roi_reg++] = roi->width; // 1 blk / roi line: segment
	if (right < width)		  // 1 blk / roi line: unused on right
		reg_size[roi_reg++] = width - right;  
	sg_blocks += roi->height * roi_reg;

	// check and count page boundaries
	page_size = getpagesize();
	block_len = roi->width * depth;
	nr_pages = (block_len * roi->height + page_size - 1) / page_size;
	for (i = 1; i < nr_pages; i++)
		if (((page_size * i) % block_len) != 0)
			sg_blocks++;
	
	table_len = sg_blocks * sizeof(*sg_desc);
	sg_desc = (struct scdxipci_sg_block *) malloc(table_len);
	if (sg_desc == NULL) {
		DEB_ERRORS("error allocating SG table: %d blks (%d bytes)\n",
			   sg_blocks, table_len);
		ret = -ENOMEM;
		goto out;
	}
	sg->ptr = sg_desc;
	sg->len = sg_blocks;

	block_len = width * depth;
	for (row = 0; row < roi->top; row++, sg_desc++) {
		sg_desc->address = SCDXIPCI_INVALID32;
		sg_desc->size = block_len / 4;
	}
		
	addr = 0;
	for (row = roi->top; row < bottom; row++) {
		for (reg = 0; reg < roi_reg; reg++, sg_desc++) {
			block_len = reg_size[reg] * depth;
			if (reg == good_reg) {
				sg_desc->address = addr;
				// check page boundaries
				offset = addr % page_size;
				if (offset + block_len > page_size) {
					aux_len = page_size - offset;
					sg_desc->size = aux_len / 4;
					addr += aux_len;
					block_len -= aux_len;
					sg_desc++;
					sg_desc->address = addr;
				}
				addr += block_len;
			} else
				sg_desc->address = SCDXIPCI_INVALID32;
			sg_desc->size = block_len / 4;
		}
	}

	block_len = width * depth;
	for (row = bottom; row < height; row++, sg_desc++) {
		sg_desc->address = SCDXIPCI_INVALID32;
		sg_desc->size = block_len / 4;
	}
		
 out:
	if (ret < 0) {
		if (sg->ptr != NULL)
			free(sg->ptr);
		free(*sg_list_ptr);
	}

	return ret;
}


static int espia_create_sg_vert_one(espia_t dev, int nr_dev, int flip, 
				    struct espia_frame_dim *frame_dim,
				    struct scdxipci_sg_table *sg, int dev_idx,
				    int dev_pos)
{
	struct espia_dev *pdev;
	struct scdxipci_sg_block *sg_desc;
	unsigned long addr, row_len, sg_rows, frame_rows, dev_rows;
	unsigned long table_len, block_len, dev_size, page_size, first_xfer;
	unsigned long i, r, a, t;
	int dir, aligned, t1, t2;

#define min_val(a, b) (((a) < (b)) ? (a) : (b))
#define max_val(a, b) (((a) > (b)) ? (a) : (b))

	FENTRY("espia_create_sg_vert_one");
	espia_get_check_dev(pdev, dev);

	dir = !flip ? 1 : -1;
	sg += dev_idx;

	row_len = frame_dim->width * frame_dim->depth;
	frame_rows = frame_dim->height;
	dev_rows = frame_rows / nr_dev;
	dev_size = row_len * dev_rows;

	page_size = getpagesize();
	block_len = !flip ? page_size : row_len;
	if (block_len > page_size) {
		DEB_ERRORS("error: block len (%ld) greater than page size "
			   "(%ld)\n", block_len, page_size);
		return -EINVAL;
	}

	addr = dev_pos * dev_size;
	addr += !flip ? 0 : ((dev_rows - 1) * row_len);

	aligned = !flip;
	aligned |= ((page_size % block_len == 0) && (addr % block_len == 0));
	if (aligned) {
		first_xfer = (page_size - addr % page_size) % page_size;
		first_xfer = min_val(first_xfer, min_val(block_len, dev_size));
		sg_rows = (first_xfer > 0) ? 1 : 0;
		sg_rows += (dev_size - first_xfer + block_len - 1) / block_len;
	} else {
		sg_rows = 0;
		for (i = 0; i < dev_size; i += block_len, sg_rows++) {
			t = min_val(block_len, dev_size - i);
			a = (addr + i * dir) % page_size;
			if (a + t > page_size)
				sg_rows++;
		}
	}

	table_len = sg_rows * sizeof(*sg_desc);
	sg_desc = (struct scdxipci_sg_block *) malloc(table_len);
	if (sg_desc == NULL) {
		DEB_ERRORS("error allocating SG table: %ld rows (%ld bytes)\n",
			   sg_rows, table_len);
		return -ENOMEM;
	}
	sg->ptr = sg_desc;
	sg->len = sg_rows;

#define add_sg(i, a, l)							\
	do {								\
		sg_desc->address = (a);					\
		sg_desc->size = (l) / 4;				\
		DEB_SG("Dev#%d: Xfer-%d[%4ld]: %ld - %d\n",		\
		       dev_pos, (i), sg_desc - sg->ptr, (a), (l));	\
		sg_desc++;						\
	} while (0)

	t = (aligned && first_xfer) ? first_xfer : block_len;
	for (r = dev_size; r > 0; r -= t, addr += dir * t, t = block_len) {
		a = addr % page_size;
		t = min_val(t, r);
		t2 = max_val(0, (int) (a + t - page_size));
		t1 = t - t2;
		add_sg(1, addr, t1);
		if (t2 > 0)
			add_sg(2, addr + t1, t2);
	}

	return ESPIA_OK;

#undef add_sg		       
#undef max_val
#undef min_val
}

					 
static int espia_create_sg_vert(espia_t dev, int img_config,
				struct espia_frame_dim *frame_dim,
				struct scdxipci_sg_table **sg_list_ptr,
				int *nr_dev_ptr)
{
	struct espia_dev *pdev;
	struct scdxipci_sg_table *sg;
	int ret, i, nr_dev, flip, inv, d;

	FENTRY("espia_create_sg_vert");
	espia_get_check_dev(pdev, dev);

	switch (img_config) {
	case ESPIA_SG_FLIP_VERT_1:
		nr_dev = 1;
		break;

	case ESPIA_SG_FLIP_VERT_2:
	case ESPIA_SG_CONCAT_VERT_2:
	case ESPIA_SG_CONCAT_VERT_INV_2:
		nr_dev = 2;
		break;
	}
	inv = (img_config == ESPIA_SG_CONCAT_VERT_INV_2);

	ret = espia_alloc_sg_list(pdev, nr_dev, sg_list_ptr);
	if (ret < 0)
		return ret;
	
	sg = *sg_list_ptr;
	for (i = 0; (i < nr_dev) && (ret >= 0); i++) {
		if (img_config == ESPIA_SG_FLIP_VERT_1)
			flip = 1;
		else if (img_config == ESPIA_SG_FLIP_VERT_2)
			flip = (i == 1);
		else
			flip = 0;
		d = !inv ? i : (nr_dev - 1 - i);
		ret = espia_create_sg_vert_one(dev, nr_dev, flip, frame_dim,
					       sg, i, d);
	}

	if (ret < 0) {
		for (i = 0; i < nr_dev; i++, sg++) {
			if (sg->ptr != NULL)
				free(sg->ptr);
		}
		free(*sg_list_ptr);
	} else {
		*nr_dev_ptr = nr_dev;
	}

	return ret;
}

int espia_create_sg(espia_t dev, int img_config, 
		    struct espia_frame_dim *frame_dim, struct espia_roi *roi,
		    struct scdxipci_sg_table **sg_list_ptr, int *nr_dev_ptr)
{
	struct espia_dev *pdev;
	int ret = 0;

	FENTRY("espia_create_sg");
	espia_get_check_dev(pdev, dev);
	espia_check_null3(frame_dim, sg_list_ptr, nr_dev_ptr);
	DEB_PARAMS("img_config: %d\n", img_config);

	if ((roi != NULL) && (img_config != ESPIA_SG_NORM)) {
		DEB_ERRORS("SG RoI only sopported with with SG_NORM!\n");
		return ESPIA_ERR_ROI_IMGCFG;
	}

	switch (img_config) {
	case ESPIA_SG_NORM:
		ret = espia_create_sg_roi(dev, frame_dim, roi, 
					  sg_list_ptr, nr_dev_ptr);
		break;

	case ESPIA_SG_FLIP_VERT_1:
	case ESPIA_SG_FLIP_VERT_2:
	case ESPIA_SG_CONCAT_VERT_2:
	case ESPIA_SG_CONCAT_VERT_INV_2:
		ret = espia_create_sg_vert(dev, img_config, frame_dim,
					   sg_list_ptr, nr_dev_ptr);
		break;

	default:
		DEB_ERRORS("invalid image config: %d\n", img_config);
		ret = -EINVAL;
	}

	return ret;
}

int espia_release_sg(espia_t dev, struct scdxipci_sg_table *sg_list, int nr_dev)
{
	struct espia_dev *pdev;
	struct scdxipci_sg_table *sg = sg_list;
	int i;

	FENTRY("espia_release_sg");
	espia_get_check_dev(pdev, dev);
	espia_check_null(sg_list);
	DEB_PARAMS("nr_dev: %d\n", nr_dev);

	if (nr_dev <= 0) {
		DEB_ERRORS("invalid nr_dev: %d\n", nr_dev);
		return -EINVAL;
	}

	for (i = 0; i < nr_dev; i++, sg++)
		if (sg->len > 0)
			free(sg->ptr);

	free(sg_list);

	return ESPIA_OK;
}


char *espia_sg_img_config_desc[] = {
	"Normal image - For use with RoI",
	"Vertical image flip            - One adapter",
	"Vertical image flip            - Two adapters",
	"Vertical image concat          - Two adapters",
	"Vertical image inverted concat - Two adapters",
};

int espia_sg_img_config_nr = (sizeof(espia_sg_img_config_desc) / 
			      sizeof(espia_sg_img_config_desc[0]));

/*--------------------------------------------------------------------------
 * set device list
 *--------------------------------------------------------------------------*/

int espia_set_dev(scdxipci_t dev, int *dev_list, int nr_dev)
{
	struct espia_dev *pdev;

	FENTRY("espia_set_dev");
	espia_get_check_dev(pdev, dev);
	espia_check_null(dev_list);
	DEB_PARAMS("nr_dev=%d\n", nr_dev);

	return espia_check("scdxipci_set_dev", 
			   scdxipci_set_dev(pdev->dev, dev_list, nr_dev));
}
