%module espia_acq_lib
%{
/* include C header files necessary to compile the interface */
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <ctype.h>

#include "espia_lib.h"
#include "esrfdebug.h"
/*#include "app_framework.h"*/
#include "imageapi.h"
#include "focla_lib.h"

#include "espia_acq_lib.h"
#include "espia_acq_lib_priv.h"
%}


%include "typemaps.i"

%{
#define SWIG_FILE_WITH_INIT
%}
%include "numpy.i"
%init %{
    import_array();
%}

%include cstring.i


// Create a NumPy array which contains the data pointed by the *data_ptr
// WITHOUT COPYING the data; array.size is *data_len:

%apply ( unsigned char** ARGOUTVIEW_ARRAY1, int* DIM1  )
      {( unsigned char **data_ptr,     int *data_len )};


// Provide a typemap for the (int argc, char *argv[]) arguments
// The value of int argc is implicitly determined by the length of the list,
// so from Python we can call it without argc, only an argv list:

%typemap(in) (int argc, char *argv[]) {
    int i;
    if (!PyList_Check($input)) {
	PyErr_SetString(PyExc_ValueError, "Expecting a list");
	return NULL;
    }
    $1 = PyList_Size($input);
    $2 = (char **) malloc(($1+1)*sizeof(char *));
    for (i = 0; i < $1; i++) {
	PyObject *s = PyList_GetItem($input,i);
	if (!PyString_Check(s)) {
	    free($2);
	    PyErr_SetString(PyExc_ValueError, "List items must be strings");
	    return NULL;
	}
	$2[i] = PyString_AsString(s);
    }
    $2[i] = 0;
}

%typemap(freearg) (int argc, char *argv[]) {
    if ($2) free($2);
}


// A typemap for
// int lib_dev_set( etest_t etest, int dev_nr, int dev_list[], int nr_dev );
// The Python input is a list of ints. The nr_dev is calculated (not needed)

%typemap(in) (int dev_list[], int nr_dev) {
	int i;

	if (!PyList_Check($input)) {
		PyErr_SetString(PyExc_ValueError,"Expected a list");
		return NULL;
	}

	$2 = PyList_Size($input);

	$1 = (int *) malloc($2 * sizeof(int));
	for (i = 0; i < $2; i++) {
		PyObject *o = PyList_GetItem($input,i);
		if (PyInt_Check(o)) {
			$1[i] = (int) PyInt_AsLong(o);
		} else {
			PyErr_SetString(PyExc_ValueError,"Sequence elements "
			                                 "must be integers");
			free($1);
			return NULL;
		}
	}
}

%typemap(freearg) (int dev_list[], int nr_dev) {
   if ($1) free($1);
}


// Handle the output arguments:

%apply unsigned short *OUTPUT { unsigned short *checksum };
%apply unsigned long  *OUTPUT { unsigned long *invalid };
%apply unsigned long  *OUTPUT { unsigned long *acq_run_nr };
%apply unsigned long  *OUTPUT { unsigned long *nr_written_p };
%apply unsigned long  *OUTPUT { unsigned long *avail_bytes_p };
%apply unsigned long  *OUTPUT { unsigned long *espia_dev };
%apply unsigned char  *OUTPUT { unsigned char *ccd_status };
%apply unsigned char  *OUTPUT { unsigned char *old_val };
%apply unsigned int   *OUTPUT { unsigned int *old_val };
%apply float          *OUTPUT { float *upd_rate };
%apply float          *OUTPUT { float *ref_rate };
%apply int            *OUTPUT { int *old_val };
%apply int            *OUTPUT { int *acq_active };
%apply int            *OUTPUT { int *auto_save_active };
%apply int            *OUTPUT { int *pulse_active };
%apply int            *OUTPUT { int *curr_pulse };
%apply int            *OUTPUT { int *curr_stage };
%apply int            *OUTPUT { int *num_cb };
%apply int            *OUTPUT { int *user_cb };


%apply int  *OUTPUT { int  *auto_save };
%cstring_bounded_output(char *prefix, FILE_PREFIX_NAME_LEN );
%apply int  *OUTPUT { int  *idx };
%apply int  *OUTPUT { int  *format };
%cstring_bounded_output(char *file_name, FILE_FULL_NAME_LEN );
%apply int  *OUTPUT { int  *tot_file_frames };
%apply int  *OUTPUT { int  *file_frames };
%apply int  *OUTPUT { int  *overwrite };


%apply unsigned long  *INOUT  { unsigned long *wr_bytes_p };
%apply unsigned int   *INOUT  { unsigned int *reg_val };
%apply unsigned int   *INOUT  { unsigned int *param_val };
%apply int            *INOUT  { int *deb_lvl };
%apply int            *INOUT  { int *nr_param };
%apply long           *INOUT  { long *img_min_val };
%apply long           *INOUT  { long *img_max_val };
%apply int            *INOUT  { int *img_auto_range };
%apply int            *INOUT  { int *nr_option };


// Typemaps template macro for mapping C pointers to Python long ints:

%define %C_pointer_as_Py_long( pType )
	/* Convert from Python --> C */
	%typemap(in) pType {
		$1 = (pType) PyLong_AsVoidPtr($input);
	}

	/* Convert from C --> Python */
	%typemap(out) pType {
		$result = PyLong_FromVoidPtr((void *) $1);
	}

	/* Ignore (pType *) when it is an input argument. Then see below. */
	%typemap(in,numinputs=0) pType *OUTPUT(pType temp) {
		$1 = &temp;
	}

	/* Convert (pType *) to an output result when it is an input argument */
	%typemap(argout) pType *OUTPUT {
		%append_output( PyLong_FromVoidPtr((void*) *$1) );
	}
%enddef


// Apply C_pointer_as_Py_long() on (void *). This is for the "user_data":

%C_pointer_as_Py_long( void * );


// Replace "etest_t" (struct lib_data *) with "unsigned long",
// because Swig does not translate etest_t as it is:
//
//%apply unsigned long { etest_t };
//
// OR provide etest_t typemaps...
// if we have "typedef void * etest_t":

%C_pointer_as_Py_long( etest_t );

// then do:

%apply etest_t *OUTPUT { etest_t *etest };

// OR use this helper function:
//
//%inline %{
//unsigned long _lib_test_init( int argc, char *argv[] ) {
//	etest_t *e;
//	int res;
//
//	res = lib_test_init(&e, argc, argv);
//	return ((res < 0)? (unsigned long) NULL : (unsigned long) e);
//}
//%}
//
// OR, if etest_t is defined already as "unsigned long" in the header, do:
//
//%apply unsigned long *OUTPUT { unsigned long *etest };


// This handles lib_serial_read() and lib_serial_read_str(). From Swig manual:
// "This macro is used to handle bounded character output functions where both a
//  char * and a pointer int * are passed. Initially, the int * parameter points
//  to a value containing the maximum size. On return, this value is assumed to
//  contain the actual number of bytes."
// Note that *rd_bytes_p will not be changed. To obtain the size call len(buffer)

%cstring_output_withsize( char *in_buffer, unsigned long *rd_bytes_p );
%apply (char *STRING, int LENGTH) \
                           { (char *term, unsigned long term_bytes) };


// This below is for lib_buffer_img_rates() and buffer_img_norm().

%cstring_bounded_output(char *buf_img_name, BUFFER_IMG_NAME_LEN);


// This structure below is defined in scdxipci_registers.h

struct scdxipci_card_id {
	unsigned int NumeroSerie;
	unsigned int CODE_TEST;
	unsigned int libre[64];
	unsigned int CRC;
};

// This structure below is defined in focla_lib.h

struct focla_signal {
	char	*name;
	char	*desc;
};


%include "scdxipci.h"
%include "espia_lib.h"
%include "espia_acq_lib.h"


%constant int swig_cb_funct( etest_t etest, int dev_nr, \
                             struct espia_cb_data *cb_data, \
                             void *user_data );


%{

static swig_type_info _swigt__p_espia_cb_data;


PyObject *espia_cb_data2swig_cb_data(struct espia_cb_data *t)
{
	return SWIG_NewPointerObj(t, &_swigt__p_espia_cb_data, 0);
}


int swig_cb_funct( etest_t etest, int dev_nr, 
                   struct espia_cb_data *cb_data, 
                   void *user_data )
{
	PyObject *pyret;
	PyObject *arglist;
	PyObject *py_cb_data;
	PyGILState_STATE gstate;
	PyObject *pyCallback = (PyObject *) user_data;


	if( NULL == pyCallback ) {
		fprintf( stderr, "WARNING! No pyCallback!\n" );
		return ESPIA_OK;
	}

	gstate = PyGILState_Ensure();

	py_cb_data = espia_cb_data2swig_cb_data(cb_data);
	arglist = Py_BuildValue("(iO)", dev_nr, py_cb_data);
	pyret = PyEval_CallObject( pyCallback, arglist );
	Py_DECREF(arglist);
	if( NULL == pyret ) {
		fprintf( stderr, "WARNING! pyCallback returned NULL!\n" );
	} else {
		/* Here maybe use the result */
		Py_DECREF(pyret);
	}

	PyGILState_Release(gstate);

	return ESPIA_OK;
}

%}
