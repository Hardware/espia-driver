#!/usr/bin/env python

"""Setup script for the driver test module."""

import os, sys, glob

from distutils.core import setup
from distutils.extension import Extension

sources = ["PyEspiaDriver.c"]

setup (
        name         = "PyEspiaDriver",
        version      = "1.0",
        description  = "module to test espia driver",
        author       = "BLISS Group",
        author_email = "papillon@esrf.fr",
        url          = "http://www.esrf.fr/computing/bliss/",

        # Description of the modules and packages in the distribution

        ext_modules  = [
                       Extension(
                            name          = "PyEspiaDriver",
                            sources       = sources,
			    # LOCAL includes
                            #include_dirs  = ["./lib"],
			    library_dirs  = ["./lib"],
			    # ESPIA includes
                            include_dirs  = ["./lib", "/segfs/bliss/depot/pythonbliss_3.0/installdir/suse82/extensions/include/python"],
			    #library_dirs  = ["../bin/suse82/2.4.20-64GB-SMP"],
                            libraries     = ["espia"],
                       ),
       ],
)

