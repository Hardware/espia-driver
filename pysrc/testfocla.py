import time, ctypes, os, sys
import Espia
from Espia.EspiaAcqLib import *
import Espia.processlib as pl
import random


CcdDebug.GlobalFlags.set(0xffffffff)


elib = EspiaAcqLib()

espia = EspiaDev(elib, 0)  # open Espia device 0

dev = CLEspiaDev(elib, 0)  # create Focla (Camera Link) device class

focla = dev.getFocla()


start = CcdXY(1044, 962)
size = CcdSize(270, 220)
ccd_size = CcdXY(2048)
roi = CcdRoi(start, size)
tl = roi.getTopLeft()
br = roi.getBottomRight()

roi_tsk = pl.Tasks.SoftRoi()
roi_tsk.setRoi(tl.getX(), br.getX(), tl.getY(), br.getY())

bckg_mgr = pl.BackgroundProcessMgr()
bckg_mgr.addTask(0, roi_tsk)

pool_mgr = pl.PoolThreadMgr.get()
pool_mgr.setBackgroundProcessMgr(bckg_mgr, pool_mgr.Sequential)

sys.path.append(os.getcwd())
lib =  ctypes.CDLL( './processlib.so')
fn = ctypes.addressof(ctypes.c_void_p.in_dll(lib, 'startNewProcess'))
data = pool_mgr.backgroundProcessMgrAddress(pool_mgr.Sequential)

dev.pluginReg(EspiaDevClass.PluginType1, fn, data, 'SoftRoI')


fdim = CcdFrameDim(2048, 2048, 2)
dev.bufferAlloc(100, 1, fdim)


roi_fdim = CcdFrameDim(roi.getWidth(), roi.getHeight(), fdim.getDepth())
dev.pluginOutput(1, 1, roi_fdim)


dev.bufferImageLive()
elib.imageRefresh()
elib.imageRefresh()


dir = '/buffer/locdata1/kirov'
fname = 'test_python_'
prefix = '%s/%s' % (dir, fname)
idx = 0
dev.autoSaveActive(prefix, idx, overwrite=1)

dev.edfUsrHeader('par1 = "par1" ;\npar2 = 2 ;\npar3 = toto ;\n')


dev.setOption('NO_FIFO_RESET', 1)


dev.startAcq(0, 500, 0)

focla.setParam('TEST_IMAGE', 1)


CcdDebug.GlobalFlags.set(0x0)
count = 0
t1 = time.time()

while dev.getAcqStatus().running:
	delay = random.uniform(0,0.01)
	time.sleep(delay)
	dev.edfUsrHeader('par1 = "par1" ;\npar2 = 2 ;\nsleep = %f ;\n' % delay)
	elib.imageRefresh()
	count = count + 1

t2 = time.time()
print "%d iterations for %f seconds" % (count, (t2-t1))
CcdDebug.GlobalFlags.set(0xffffffff)


elib.imageRefresh()

focla.setParam('TEST_IMAGE', 0)


print dev.bufferImageNorm(-1, -1, -1, -1)

elib.imageRefresh()


#dev.stopAcq()


exit()
