import time, ctypes, os, sys
from Espia.EspiaAcqLib import *
import processlib as pl


CcdDebug.GlobalFlags.set(0xffffffff)


elib = EspiaAcqLib()
dev = EspiaDev(elib, 0)


start = CcdXY(1044, 962)
size = CcdSize(270, 220)
ccd_size = CcdXY(2048)
roi = CcdRoi(start, size)
tl = roi.getTopLeft()
br = roi.getBottomRight()

roi_tsk = pl.Tasks.SoftRoi()
roi_tsk.setRoi(tl.getX(), br.getX(), tl.getY(), br.getY())

bckg_mgr = pl.BackgroundProcessMgr()
bckg_mgr.addTask(0, roi_tsk)

pool_mgr = pl.PoolThreadMgr.get()
pool_mgr.setBackgroundProcessMgr(bckg_mgr, pool_mgr.Sequential)

sys.path.append(os.getcwd())
lib =  ctypes.CDLL( './processlib.so')
fn = ctypes.addressof(ctypes.c_void_p.in_dll(lib, 'startNewProcess'))
data = pool_mgr.backgroundProcessMgrAddress(pool_mgr.Sequential)

dev.pluginReg(EspiaDevClass.PluginType1, fn, data, 'SoftRoI')


fdim = CcdFrameDim(2048, 2048, 2)
dev.bufferAlloc(100, 1, fdim)


roi_fdim = CcdFrameDim(roi.getWidth(), roi.getHeight(), fdim.getDepth())
dev.pluginOutput(1, 1, roi_fdim)


dev.bufferImageLive()


ser_line = EspiaSerLine(dev, SerLineConfig())
config = ser_line.getConfig()
config.LineTerm = '\r\n'
print config
ser_line.setConfig(config)


dir = '/buffer/frelonisg12/tests'
fname = 'test_python_'
prefix = '%s/%s' % (dir, fname)
idx = 0
dev.autoSaveActive(prefix, idx, overwrite=1)

dev.edfUsrHeader('par1 = "par1" ;\npar2 = 2 ;\npar3 = toto ;\n')


dev.startAcq(0, 1, 0)

ser_line.write('>S\r\n')
print "Camera response: %s" % ser_line.readLine()

while dev.getAcqStatus().running:
	time.sleep(0.01)

elib.imageRefresh()

print dev.bufferImageNorm(-1, -1, -1, -1)

elib.imageRefresh()
