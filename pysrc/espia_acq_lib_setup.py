
from distutils.core import setup, Extension
import os


if 'QTDIR' in os.environ:
    root_qt_dir  = os.environ['QTDIR']
else:
    raise RuntimeError("QTDIR variable not defined")

qt_lib_dir   = root_qt_dir + "/lib"


libespia_dir = "../src"  # Doesn't work! Python still uses blissadm lib path.


try:
    common_incl = os.environ['COMMON_INCL']
except:
    common_incl = "../../common/include"


try:
    common_lib = os.environ['COMMON_LIB']
except:
    common_lib = "../../common/src"


import numpy


extension_mod = Extension( "_espia_acq_lib"
			  , ["../src/espia_acq_lib.c", "espia_acq_lib_wrap.c"]
			  , include_dirs=[ "../src", common_incl
			                 , numpy.get_include()
			                 ]
			  , extra_objects=[common_lib + "/image.o"]
			  , extra_compile_args=[]
			  , library_dirs=[ #qt_lib_dir, # using --library-dirs="$(QTDIR)/lib"
			                                # in the Makefile instead
			                   libespia_dir ]
			  , libraries=["espia", "m", "pthread", "QtCore", "QtGui", "QtOpenGL", "GL", "X11"])

setup(name = "espia_acq_lib", ext_modules=[extension_mod])
