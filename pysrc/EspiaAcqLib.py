import sys, os, string, time, signal, struct, weakref, re, numpy
from SimpleRecord import Record
from printf2re import PrintfFormat

from Ccd.Core import *

from EdfFile import EdfFile

import espia_acq_lib


class Int:
	Any = Invalid = Live = -1
	
	def __init__(self, from_int=None, from_ulong=None, from_uint=None):
		if from_int is not None:
			self.val = from_int
		elif from_ulong is not None:
			if from_ulong == UnsignedLong.Any:
				self.val = Int.Any
			else:
				self.val = from_ulong
		elif from_uint is not None:
			if from_uint == UnsignedInt.Any:
				self.val = Int.Any
			else:
				self.val = from_uint
			
		else:
			self.val = 0

	def __int__(self):
		return self.val


class UnsignedLong:
	Any = Invalid = Live = espia_acq_lib.lib_get_invalid()[1]

	def __init__(self, from_int=0):
		if from_int == Int.Any:
			self.val = UnsignedLong.Any
		else:
			self.val = from_int

	def __int__(self):
		return self.val

	
class UnsignedInt:
	Any = Invalid = Live = (UnsignedLong.Live & 0xffffffff)


NoBlock = 0
Any = Invalid = Live = Int.Any



class EspiaAcqLibException(Exception):

	def __init__(self, cmd_str, ret):
		self.cmd_str = cmd_str
		self.ret = ret

		err_msg = espia_acq_lib.espia_test_strerror(ret)
		msg = 'Error %d in call %s: %s' % (ret, cmd_str, err_msg)
		Exception.__init__(self, msg)


class EspiaAcqLibProxy(CcdDebug):

	CmdNames = {
		'AcqStatus'           : 'lib_acq_status',
		'AutoSaveStatus'      : 'lib_buffer_save_cb_get',
		'BufferAlloc'         : 'lib_buffer_alloc',
#		'BufferDump'          : 'lib_get_data'
		'BufferClear'         : 'lib_buffer_clear',
		'BufferFree'          : 'lib_buffer_free',
		'BufferImage'         : 'lib_buffer_img',
		'BufferImageDel'      : 'lib_buffer_img_del',
		'BufferImageNorm'     : 'lib_buffer_img_norm',
		'BufferImageRates'    : 'lib_buffer_img_rates',
#		'BufferList'          :
		'BufferSave'          : 'lib_buffer_save',
		'CbActive'            : 'lib_cb_active',
		'CcdStatus'           : 'lib_ccd_status',
#		'CbList'              : 'lib_get_num_cb', 'lib_is_user_cb', 
#                                                 'lib_espia_get_callback'
		'Cleanup'             : 'lib_test_cleanup',
		'Close'               : 'lib_close_dev',
		'DebugLevel'          : 'lib_set_debug_level',
		'DevSet'              : 'lib_dev_set',
#		'FirmwareList'        : 'espia_get_firmware_data'
		'FoclaClose'          : 'lib_focla_close_dev',
		'FoclaGetParam'       : 'lib_focla_get_par',
		'FoclaOpen'           : 'lib_focla_open_dev',
#		'FoclaParamList'      : 'focla_get_param_data'
		'FoclaPulseStart'     : 'lib_focla_pulse_start',
		'FoclaPulseStatus'    : 'lib_focla_pulse_status',
		'FoclaPulseStop'      : 'lib_focla_pulse_stop',
		'FoclaReadReg'        : 'lib_focla_read',
		'FoclaSerialFlush'    : 'lib_focla_serial_flush',
		'FoclaSerialGetAvail' : 'lib_focla_serial_get_avail_bytes',
		'FoclaSerialRead'     : 'lib_focla_serial_read',
		'FoclaSerialReadStr'  : 'lib_focla_serial_read_str',
		'FoclaSerialWrite'    : 'lib_focla_serial_write',
		'FoclaSetParam'       : 'lib_focla_set_par',
#		'FoclaSignalList'     : 'focla_get_sig_data'
		'FoclaWriteReg'       : 'lib_focla_write',
		'FramePostOp'         : 'lib_frame_post_op',
#		'FramePostOpList'     :
		'GetCardID'           : 'lib_get_card_id',
		'GetData'             : 'lib_get_data',        # new!
		'GetDev'              : 'lib_get_dev',
		'GetFrame'            : 'lib_get_frame',
		'GetFrameInfo'        : 'lib_get_frame_info',  # new!
		'GetOption'           : 'lib_get_option',
		'GetParam'            : 'lib_get_param',
		'GetStats'            : 'lib_get_stats',
		'ImageRefresh'	      : 'lib_image_refresh',
		'Init'                : 'lib_test_init',
		'Open'                : 'lib_open_dev',
#		'OptionList'          : 'lib_espia_get_option_data'
#		'ParamList'           : 'lib_espia_get_param_data'
#		'PollSleep'           : ---
		'RegAcqCb'            : 'lib_reg_acq_cb',
		'RegSerRxCb'          : 'lib_reg_ser_rx_cb',
		'RegStatusCb'         : 'lib_reg_status_cb',
		'SerialFlush'         : 'lib_serial_flush',
		'SerialGetAvail'      : 'lib_serial_get_avail_bytes',
		'SerialMmapClose'     : 'lib_serial_mmap_close',
		'SerialMmapInit'      : 'lib_serial_mmap_init',
		'SerialMmapRead'      : 'lib_serial_mmap_read',
		'SerialMmapWrite'     : 'lib_serial_mmap_write',
		'SerialRead'          : 'lib_serial_read',
		'SerialReadStr'       : 'lib_serial_read_str',
		'SerialWrite'         : 'lib_serial_write',
		'ReadRegister'        : 'lib_read_register',
		'ResetLink'           : 'lib_reset_link',
		'RoiAccBuffer'        : 'lib_roi_acc_buffer',
		'RoIReSet'            : 'lib_roi_reset',
		'RoISet'              : 'lib_roi_set',
		'SetOption'           : 'lib_set_option',
		'SetParam'            : 'lib_set_param',
		'SGSet'               : 'lib_sg_set',
		'StartAcq'            : 'lib_start_acq',
		'StopAcq'             : 'lib_stop_acq',
		'UnregAllCb'          : 'lib_unreg_all_cb',
		'UnregCb'             : 'lib_unreg_cb',
		'Version'             : 'lib_get_version',
		'WriteRegister'       : 'lib_write_register',
		'PluginReg'           : 'lib_reg_plugin',
		'PluginActive'        : 'lib_plugin_active',
		'PluginDel'           : 'lib_plugin_delete',
		'PluginOutput'        : 'lib_plugin_output',
		'EdfUsrHeader'        : 'lib_edf_usr_header',
	}

	DebugExclCmds = ['ImageRefresh']
	
	def __init__(self, cmd, lib_handle=None, dev_nr=None):
		CcdDebug.__init__(self)

		self.cmd = cmd
		self.lib_handle = lib_handle
		self.dev_nr = dev_nr
		self.fn_str = self.CmdNames[cmd]
		self.fn = getattr(espia_acq_lib, self.fn_str)

	def __call__(self, *args):
		call_args = []
		if self.lib_handle is not None:
			call_args += [self.lib_handle]
		if self.dev_nr is not None:
			call_args += [self.dev_nr]
		call_args += list(args)
		do_trace = self.cmd not in self.DebugExclCmds
		if do_trace:
			msg = 'Calling: %s' % self.getCmdStr(call_args)
			self.debugTrace(msg)
		ret_n_args = self.fn(*call_args) # Calling the function
		if do_trace:
			self.debugTrace('Return:  %s' % ret_n_args)
		if type(ret_n_args) == type(0): # Only ret?
			ret = ret_n_args
			ret_args = []
		else:
			ret = ret_n_args[0]
			ret_args = ret_n_args[1:]
		return self.checkRet(ret, ret_args, call_args)

	def checkRet(self, ret, ret_args, call_args):
		if ret < 0:  # or "!= 0" ?
			cmd_str = self.getCmdStr(call_args)
			raise EspiaAcqLibException(cmd_str, ret)
		if len(ret_args) > 1:
			return ret_args
		elif len(ret_args) == 1:
			return ret_args[0]
		else:
			return None

	def getCmdStr(self, args):
		if args and args[0] == self.lib_handle:
			args = args[1:]
		args_str = str(args)[1:-1]
		if args_str and args_str[-1] == ',':
			args_str = args_str[-1]
		return '%s(%s)' % (self.cmd, args_str)



class EspiaAcqLib:

	def __init__(self, cmdline_opts=[], display=None):
		self.lib_handle = None
		self.dev_repo = {}
		
		lib_init = EspiaAcqLibProxy('Init')
		self.lib_handle = lib_init(cmdline_opts)

	def __del__(self):
		if self.lib_handle is not None:
			self.command('Cleanup')

	def getHandle(self):
		return self.lib_handle
	
	def __repr__(self):
		return '<EspiaAcqLib with handle 0x%x>' % self.getHandle()

	def __str__(self):
		return repr(self)

	def command(self, cmd, dev_nr=None, *args):
		lib_cmd = EspiaAcqLibProxy(cmd, self.lib_handle, dev_nr)
		return lib_cmd(*args)

	def getEspiaDev(self, dev_nr):
		if dev_nr not in self.dev_repo:
			return None
		ref = self.dev_repo[dev_nr]
		return ref()

	def setEspiaDev(self, dev_nr, dev):
		ref = weakref.ref(dev, self.delRepoDev)
		self.dev_repo[dev_nr] = ref

	def delRepoDev(self, ref):
		for dev_nr, dev_ref in self.dev_repo.items():
			if dev_ref == ref:
				del self.dev_repo[dev_nr]
				break

	def imageRefresh(self):
		self.command('ImageRefresh')

	def getVersion(self):
		lib_version = espia_acq_lib.espia_lib_version()
		ver_cmd = EspiaAcqLibProxy('Version')
		ver_cmd(lib_version)
		return Record(run=lib_version.run, compile=lib_version.compile)
	

class EspiaFrameInfo:

	Format = '<buffer_nr=%d, frame_nr=%d, round_count=%d, ' \
		 'acq_frame_nr=%d, sec=%f, pixels=%d>'
	
	Format_obj = PrintfFormat(Format)

	def __init__(self, buffer_nr=Any, frame_nr=Any, round_count=Any,
		     acq_frame_nr=Any, sec=0, pixels=0,
		     from_str=None, from_lib_finfo=None):
		self.buffer_nr    = buffer_nr
		self.frame_nr     = frame_nr
		self.round_count  = round_count
		self.acq_frame_nr = acq_frame_nr
		self.sec          = sec
		self.pixels       = pixels

		if from_str:
			res = self.Format_obj.sscanf(line)
			if len(res) != 6:
				msg_fmt = 'Invalid frame info string: %s'
				raise CcdError, msg_fmt % from_str

			self.buffer_nr, self.frame_nr       = res[0:2]
			self.round_count, self.acq_frame_nr = res[2:4]
			self.sec, self.pixels               = res[4:6]

		if from_lib_finfo is not None:
			finfo = from_lib_finfo
			ulong2int = lambda x: int(Int(from_ulong=x))
			self.buffer_nr    = ulong2int(finfo.buffer_nr)
			self.frame_nr     = ulong2int(finfo.frame_nr)
			self.round_count  = ulong2int(finfo.round_count)
			self.acq_frame_nr = ulong2int(finfo.acq_frame_nr)
			self.sec          = finfo.time_us * 1e-6
			self.pixels       = ulong2int(pixels)

	def getLibFrameInfo(self):
		finfo              = espia_acq_lib.img_frame_info()
		int2ulong = lambda x: int(UnsignedLong(x))
		finfo.buffer_nr    = int2ulong(self.buffer_nr)
		finfo.frame_nr     = int2ulong(self.frame_nr)
		finfo.round_count  = int2ulong(self.round_count)
		finfo.acq_frame_nr = int2ulong(self.acq_frame_nr)
		finfo.time_us      = int(self.sec * 1e6 + 0.1)
		finfo.pixels       = int2ulong(self.pixels)
		return finfo
		
	def __str__(self):
		return self.Format % (self.buffer_nr, self.frame_nr,
				      self.round_count, self.acq_frame_nr,
				      self.sec, self.pixels)
	
	def __repr__(self):
		return self.__str__()


class EspiaDevClass:

	LastFrame = EspiaFrameInfo()
	BufferFile = "/tmp/espia%d_buffer_"

	PluginType1 = 1
	PluginType2 = 2
	
	def __init__(self, elib, dev_nr):
		self.elib = elib
		self.dev_nr = dev_nr
		self.has_buffer = False
		self.frame_dim = None
		self.buffer_frames = 1
		self.ser_line = None
		self.focla = None
		self.roi_bin = None
		self.sg = 0
		self.is_open = False
		self.open()

	def __del__(self):
		self.close()

	def getEspiaAcqLib(self):
		return self.elib

	def getDevNr(self):
		return self.dev_nr

	def open(self):
		if not self.is_open:
			self.command('Open')
			self.is_open = True

	def close(self):
		if self.is_open:
			self.bufferFree()
			self.command('Close')
			self.is_open = False

	def command(self, cmd, *args):
		return self.elib.command(cmd, self.dev_nr, *args)

	def commandStr(self, cmd):
		tokens = cmd.split()
		for i in range(len(tokens)):
			t = tokens[i]
			if t[0] == '"' and t[-1] == '"':
				tokens[i] = t[1:-1]
			try:
				tokens[i] = int(tokens[i])
			except:
				pass
		ans = self.command(*tokens)
		if type(ans) != type('string'):
			ans = str(ans)
		if type(ans) != type([]):
			ans = [ans]
		return ans
	
	def setDebugLevel(self, deb_level, drv_debug):
		self.command('DebugLevel', deb_level, drv_debug)
		
	def framePostOp(self, postop_list):
		sep = ','
		postop = sep.join(postop_list)
		self.command('FramePostOp', postop)

	def bufferAlloc(self, nr_buffers, buffer_frames, frame_dim, 
			buffer_img=False):
		self.command('BufferAlloc', nr_buffers, buffer_frames,
			     frame_dim.getWidth(), frame_dim.getHeight(),
			     frame_dim.getDepth(), buffer_img)
		self.has_buffer = True
		self.buffer_frames = buffer_frames
		self.frame_dim = frame_dim
		self.setSG(self.sg)

	def bufferFree(self):
		if self.has_buffer:
			self.command('BufferFree')
			self.has_buffer = False

	def bufferClear(self, buffer_nr=Any):
		self.command('BufferClear', buffer_nr)

	def bufferImage(self, buffer_nr):
		self.command('BufferImage', buffer_nr)

	def bufferImageLive(self):
		self.bufferImage(Live)

	def bufferImageRates(self, buffer_nr):
		return self.command('BufferImageRates', buffer_nr)
	
	def bufferImageNorm(self, buffer_nr, min_val, max_val, auto_range):
		return self.command('BufferImageNorm', buffer_nr,
				    min_val, max_val, auto_range)
	
	def setSG(self, sg):
		if self.has_buffer:
			self.command('SGSet', sg)
		self.sg = sg

	def getSG(self):
		return self.sg

	def bufferSaveCmd(self, buffer_nr, prefix, idx=-1, file_frames=None,
			  overwrite=False):
		if file_frames is None:
			file_frames = self.buffer_frames
		return self.command('BufferSave', buffer_nr, prefix, idx, 1,
				    file_frames, overwrite)

	def bufferSave(self, buffer_nr, prefix, idx, file_frames=None,
		       overwrite=False):
		return self.bufferSaveCmd(buffer_nr, prefix, idx, file_frames,
					  overwrite)

	def oldBufferRead(self, buffer_nr, frame_nr=0):
		prefix = self.BufferFile % self.dev_nr
		idx = 9999
		self.bufferSave(buffer_nr, prefix, idx=idx, overwrite=True)
		fname = '%s%04d.edf' % (prefix, idx)
		edf = EdfFile(fname)
		data = edf.GetData(frame_nr)
		del edf
		os.unlink(fname)
		return data

	def bufferRead(self, buffer_nr, frame_nr=0):
		raw_data = self.command('GetData', buffer_nr, frame_nr)
		raw_data.dtype = self.frame_dim.getNumType()
		shape = self.frame_dim.getNumShape()
		data = raw_data.reshape(shape)
		return data
		
	def autoSaveActive(self, prefix, index=0, file_frames=None, 
			   overwrite=False):
		self.bufferSaveCmd(Any, prefix, index, file_frames, overwrite)

	def autoSaveStop(self):
		self.bufferSaveCmd(Any, '')

	def autoSaveStatus(self):
		data = self.command('AutoSaveStatus')
		return Record(active=data[0], prefix=data[1], idx=data[2],
			      format=data[3], file_name=data[4],
			      file_frames=data[5], curr_file_frames=data[6],
			      overwrite=data[7])

	def startAcq(self, start_buff, nr_frames, timeout=NoBlock):
		self.command('StartAcq', start_buff, nr_frames, timeout)

	def stopAcq(self):
		self.command('StopAcq')

	def getFrame(self, frm_info, timeout=NoBlock):
		try:
			lib_finfo = frm_info.getLibFrameInfo()
			req_timeout = int(UnsignedLong(timeout))
			self.command('GetFrame', lib_finfo, req_timeout)
		except EspiaAcqLibException, e:
			s = str(e)
			err_tail = 'Requested frame not ready yet'
			if s.count(err_tail) > 0:
				return None
			raise  # ???
		return EspiaFrameInfo(from_lib_finfo=lib_finfo)

	def getLastFrame(self):
		return self.getFrame(self.LastFrame)

	def getAcqStatus(self):
		lib_last_acq   = EspiaFrameInfo().getLibFrameInfo()
		lib_last_saved = EspiaFrameInfo().getLibFrameInfo()
		ret = self.command('AcqStatus', lib_last_acq, lib_last_saved)
		acq_run_nr, acq_active, auto_save_active = ret
		last_acq_frm   = EspiaFrameInfo(from_lib_finfo=lib_last_acq)
		last_saved_frm = EspiaFrameInfo(from_lib_finfo=lib_last_saved)
		return Record(acq_nr=acq_run_nr, running=acq_active,
			      last_acq_frm=last_acq_frm,
			      last_saved_frm=last_saved_frm)

	def setOption(self, name, val):
		self.command('SetOption', name, val)

	def getOption(self, name):
		return self.command('GetOption', name)

	def pluginReg(self, ptype, fn, data, name):
		self.command('PluginReg', ptype, fn, data, name)

	def pluginActive(self, ptype, pnum, active):
		self.command('PluginActive', ptype, pnum, active)

	def pluginDel(self, ptype, pnum):
		self.command('PluginDel', ptype, pnum)

	def pluginOutput(self, ptype, plugin_nr, fdim):
		self.command('PluginOutput', ptype, plugin_nr, fdim.getWidth(),
			     fdim.getHeight(), fdim.getDepth())

	def edfUsrHeader(self, edf_header_str):
		self.command('EdfUsrHeader', edf_header_str)

	def createSerLine(self, ser_line_config=None):
		return EspiaSerLine(self, ser_line_config)

	def getSerLine(self, ser_line_config=None):
		if self.ser_line is not None:
			return self.ser_line()
		ser_line = self.createSerLine(ser_line_config)
		ref = weakref.ref(ser_line, self.delSerLine)
		self.ser_line = ref
		return ser_line

	def delSerLine(self, ref):
		if self.ser_line == ref:
			self.ser_line = None

	def setFocla(self, focla):
		ref = weakref.ref(focla, self.delFocla)
		self.focla = ref

	def getFocla(self):
		if self.focla is None:
			return None
		return self.focla()

	def delFocla(self, ref):
		if self.focla == ref:
			self.focla = None

	def createRoiBin(self, prev_layer=None):
		return EspiaRoiBin(self, prev_layer)

	def getRoiBin(self, prev_layer=None):
		if self.roi_bin is not None:
			return self.roi_bin()

		roi_bin = self.createRoiBin(prev_layer)
		ref = weakref.ref(roi_bin, self.delRoiBin)
		self.roi_bin = ref
		return roi_bin

	def delRoiBin(self, ref):
		if self.roi_bin == ref:
			self.roi_bin = None

	def resetLink(self):
		return self.command('ResetLink')

	def writeRegister(self, reg_nr, val, mask=0xffffffff):
		return self.command('WriteRegister', reg_nr, val, mask)

	def readRegister(self, reg_nr):
		return self.command('ReadRegister', reg_nr)


class EspiaSerLine(SerLine):

	def __init__(self, espia_dev, config):
		self.dev = espia_dev
		SerLine.__init__(self, config)

	def getEspiaDev(self):
		return self.dev

	def getTimeoutUSec(self, timeout_ms=None, avail=False):
		if timeout_ms is None:
			if avail:
				timeout_ms = 0
			else:
				timeout_ms = self.getConfig().TimeoutMSec
		return timeout_ms * 1000

	def command(self, cmd, *args):
		return self.dev.command(cmd, *args)
	
	def read(self, nr_bytes=SerLine.Available, timeout_ms=None):
		avail = (nr_bytes == SerLine.Available)
		if avail:
			nr_bytes = self.getAvailableBytes()
		timeout_us = self.getTimeoutUSec(timeout_ms, avail)
		return self.command('SerialRead', nr_bytes, timeout_us)

	def getAvailableBytes(self):
		bytes = self.command('SerialGetAvail')
		return bytes
	
	def readLine(self):
		term = self.getConfig().LineTerm
		max_length = 1024 * 1024
		timeout_us = self.getTimeoutUSec()
		return self.command('SerialReadStr', max_length, term,
				    timeout_us)

	def write(self, wstr, bsize=0, bdelay=0, block=False):
		return self.command('SerialWrite', wstr, len(wstr),
				    bsize, bdelay, block)

	def flushInput(self):
		self.command('SerialFlush')


class EspiaRoiBin(NullRoiBin):

	def __init__(self, espia_dev, prev_layer=None):
		self.dev = espia_dev
		NullRoiBin.__init__(self, prev_layer)

	def getDev(self):
		return self.dev


def EspiaDev(elib, dev_nr, dev_class=None):  # Espia device factory
	dev_class = dev_class or EspiaDevClass
	dev = elib.getEspiaDev(dev_nr)
	if dev:
		if not isinstance(dev, dev_class):
			msg = 'An instance of a different class already exists'
			raise EspiaDevError, msg
		return dev
	dev = dev_class(elib, dev_nr)
	elib.setEspiaDev(dev_nr, dev)
	return dev


class FoclaDevClass:

	PixelPack = {'Default': 0, 'Aviex': 2, 'Dalsa': 3, 'Sarnoff': 6}

	# CL CC Levels: Low, High, Ext
	CCLevelArray = [2, 3, 0]

	def __init__(self, espia_dev):
		ser_line = espia_dev.getSerLine()
		ser_line.flushInput()
		
		self.dev = espia_dev
		self.no_cache = False
		self.cl_dev = [None, None]
		self.dev_open = False
		
		self.open()
		self.setTestImage(0)
		self.curr_cam = self.getSelectedCamera()
		
	def __del__(self):
		self.close()

	def open(self):
		if not self.dev_open:
			self.dev.command('FoclaOpen')
			self.dev_open = True

	def close(self):
		if self.dev_open:
			self.dev.command('FoclaClose')
			self.dev_open = False

	def getEspiaDev(self):
		return self.dev

	def disableCache(self):
		self.no_cache = True

	def enableCache(self):
		self.no_cache = False
		self.curr_cam = self.getSelectedCamera()


	def getParam(self, name):
		return self.dev.command('FoclaGetParam', name)

	def setParam(self, name, val):
		self.dev.command('FoclaSetParam', name, val)

	def camParName(self, par_name_fmt, cam_nr):
		return par_name_fmt % (cam_nr + 1)

	def getSelectedCamera(self):
		return self.getParam('CAM_SEL')

	def selectCamera(self, cam_nr):
		if self.no_cache or cam_nr != self.curr_cam:
			self.setParam('CAM_SEL', cam_nr)
			self.curr_cam = cam_nr

	def pixelPackParName(self, cam_nr):
		return self.camParName('CL%d_PIX_PACK', cam_nr)

	def getPixelPack(self, cam_nr):
		parname = self.pixelPackParName(cam_nr)
		pix_pack = self.getParam(parname)
		for name, val in self.PixelPack.items():
			if val == pix_pack:
				return name
		return pix_pack

	def setPixelPack(self, cam_nr, pix_pack=None, name='Default'):
		parname = self.pixelPackParName(cam_nr)
		if pix_pack is None:
			pix_pack = self.PixelPack[name]
		self.setParam(parname, pix_pack)

	def setTestImage(self, test_img_active):
		self.setParam('TEST_IMAGE', test_img_active)

	def getTestImage(self):
		return self.getParam('TEST_IMAGE')
			      
	def setTriggerMode(self, trig_mode):
		self.setParam('TRIG_MODE', trig_mode)

	def getTriggerMode(self):
		return self.getParam('TRIG_MODE')

	def triggerImage(self):
		self.setParam('TRIG', 1)

	def ccLevelParName(self, cam_nr, cc_nr):
		return self.camParName('CL%%d_CC%d' % (cc_nr + 1), cam_nr)

	def ccLevelSet(self, cam_nr, cc_nr, level):
		par_name = self.ccLevelParName(cam_nr, cc_nr)
		self.setParam(par_name, self.CCLevelArray[level])

	def ccLevelGet(self, cam_nr, cc_nr):
		self.ccLevelParName(cam_nr, cc_nr)
		val = self.getParam(par_name)
		return self.CCLevelArray.index(val)

	def ccSignalName(self, cam_nr, cc_nr):
		return 'CC%d' % (cc_nr + 1)

	def ccPulseStart(self, cam_nr, cc_nr, polarity, stage_width_us,
			 nr_pulse=1):
		if len(stage_width_us) not in [1, 2]:
			msg = 'CC Pulse currently support 2 stages only'
			raise CLEspiaDevError, msg
		width = stage_width_us[0]
		if len(stage_width_us) > 1:
			delay = stage_width_us[1]
		else:
			delay = 0
		sig_name = self.ccSignalName(cam_nr, cc_nr)
		self.dev.command('FoclaPulseStart', cam_nr, sig_name,
				 polarity, width, delay, nr_pulse)

	def ccPulseStop(self, cam_nr, cc_nr):
		sig_name = self.ccSignalName(cam_nr, cc_nr)
		self.dev.command('FoclaPulseStop', cam_nr, sig_name)

	def ccPulseStatus(self, cam_nr, cc_nr):
		sig_name = self.ccSignalName(cam_nr, cc_nr)
		res = self.dev.command('FoclaPulseStatus', cam_nr, sig_name)
		active, curr_pulse, curr_stage = res
		return Record(active=active, curr_pulse=curr_pulse,
			      curr_stage=curr_stage)

	def setCLDev(self, cam_nr, cl_dev):
		ref = weakref.ref(cl_dev, self.delCLDev)
		self.cl_dev[cam_nr] = ref

	def getCLDev(self, cam_nr):
		ref = self.cl_dev[cam_nr]
		if ref is None:
			return None
		return ref()

	def delCLDev(self, ref):
		if ref in self.cl_dev:
			cam_nr = self.cl_dev.index(ref)
			self.cl_dev[cam_nr] = None


def FoclaDev(espia_dev, focla_class=None):  # FOCLA device factory
	focla = espia_dev.getFocla()
	if focla:
		return focla
	focla_class = focla_class or FoclaDevClass
	focla = focla_class(espia_dev)
	espia_dev.setFocla(focla)
	return focla


class CLEspiaDevClass:

	def __init__(self, focla, cam_nr):
		self.focla = focla
		self.cam_nr = cam_nr
		self.dev = focla.getEspiaDev()

	def getFocla(self):
		return self.focla

	def getEspiaDev(self):
		return self.dev

	def setPixelPack(self, pix_pack):
		self.focla.setPixelPack(self.cam_nr, pix_pack)

	def getPixelPack(self):
		return self.focla.getPixelPack(self.cam_nr)

	def setPixelPack(self, pix_pack=None, name='Default'):
		self.focla.setPixelPack(self.cam_nr, pix_pack, name)

	def getTriggerMode(self):
		return self.focla.getTriggerMode()

	def setTriggerMode(self, trig_mode):
		self.focla.setTriggerMode(trig_mode)

	def triggerImage(self):
		self.focla.triggerImage()

	def ccLevelSet(self, cc_nr, level):
		self.focla.ccLevelSet(self.cam_nr, cc_nr, level)

	def ccPulseStart(self, cc_nr, polarity, stage_width_us, nr_pulse=1):
		self.focla.ccPulseStart(self.cam_nr, cc_nr, polarity,
					stage_width_us, nr_pulse)

	def ccPulseStop(self, cc_nr):
		self.focla.ccPulseStop(self.cam_nr, cc_nr)

	def ccPulseStatus(self, cc_nr):
		return self.focla.ccPulseStatus(self.cam_nr, cc_nr)

	def select(self):
		self.focla.selectCamera(self.cam_nr)

	def createSerLine(self, ser_line_config=None):
		return CLEspiaSerLine(self, ser_line_config)

	def getSerLine(self, ser_line_config=None):
		if self.ser_line is not None:
			return self.ser_line()

		ser_line = self.createSerLine(ser_line_config)
		self.ser_line = weakref.ref(ser_line, self.delSerLine)
		return ser_line

	def __getattr__(self, name):
		self.select()
		return getattr(self.dev, name)


class CLEspiaSerLine(SerLine):

	def __init__(self, cl_dev, config):
		self.cl_dev = cl_dev
		SerLine.__init__(self, config)

	def getCLDev(self):
		self.cl_dev.select()
		return self.cl_dev

	def getEspiaDev(self):
		dev = self.getCLDev()
		return self.getEspiaDev()

	def getTimeoutUSec(self, timeout_ms=None, avail=False):
		if timeout_ms is None:
			if avail:
				timeout_ms = 0
			else:
				timeout_ms = self.getConfig().TimeoutMSec
		return timeout_ms * 1000

	def command(self, cmd, *args):
		dev = self.getCLDev()
		return dev.command(cmd, *args)
	
	def read(self, nr_bytes=SerLine.Available, timeout_ms=None):
		avail = (nr_bytes == SerLine.Available)
		if avail:
			nr_bytes = self.getAvailableBytes()
		timeout_us = self.getTimeoutUSec(timeout_ms, avail)
		return self.command('FoclaSerialRead', nr_bytes, timeout_us)

	def getAvailableBytes(self):
		bytes = self.command('FoclaSerialGetAvail')
		return bytes
	
	def readLine(self):
		term = self.getConfig().LineTerm
		max_length = 1024 * 1024
		timeout_us = self.getTimeoutUSec()
		return self.command('FoclaSerialReadStr', max_length, term,
				    timeout_us)

	def write(self, wstr, bsize=0, bdelay=0, block=False):
		return self.command('FoclaSerialWrite', wstr, len(wstr),
				    bsize, bdelay, block)

	def flushInput(self):
		self.command('FoclaSerialFlush')


def CLEspiaDev(espia_test, dev_nr, cam_nr=0, espia_dev_class=None,
	       focla_class=None, espia_cl_dev_class=None):
	espia_cl_dev_class = espia_cl_dev_class or CLEspiaDevClass
	espia_dev = EspiaDev(espia_test, dev_nr, espia_dev_class)
	focla = FoclaDev(espia_dev, focla_class)
	cl_dev = focla.getCLDev(cam_nr)
	if cl_dev:
		if not isinstance(cl_dev, espia_cl_dev_class):
			msg = 'An instance of a different class already exists'
			raise CLEspiaDevError, msg
		return cl_dev
	cl_dev = espia_cl_dev_class(focla, cam_nr)
	focla.setCLDev(cam_nr, cl_dev)
	return cl_dev


class EspiaMetaDevCommandProxy:

	def __init__(self, meta_dev, cmd):
		self.meta_dev = meta_dev
		self.cmd = cmd

	def __call__(self, *args, **kw):
		return self.meta_dev.passCommand(self.cmd, *args, **kw)


class EspiaMetaDevClass(EspiaDevClass):

	MetaDevNr = 255
	
	PassCommandList = [ 'setTriggerMode' ]

	def __init__(self, dev_list):
		self.dev_list = dev_list
		espia_test = dev_list[0].getEspiaAcqLib()
		EspiaDevClass.__init__(self, espia_test, self.MetaDevNr)

		if self.dev_list:
			dev_nr = lambda d: d.getDevNr()
			self.devSet(map(dev_nr, dev_list))

	def devSet(self, devnr_list):
		self.command('DevSet', devnr_list)

	def passCommand(self, cmd, *args, **kw):
		ret_list = []
		for dev in self.dev_list:
			fn = getattr(dev, cmd)
			ret = fn(*args, **kw)
			ret_list.append(ret)
		return ret_list

	def __getattr__(self, name):
		if name not in self.PassCommandList:
			msg = 'EspiaMetaDev has no attr/method \'%s\'' % name
			raise AttributeError, msg
		return EspiaMetaDevCommandProxy(self, name)


def EspiaMetaDev(dev_list, meta_dev_class=None):
	MetaDevNr = EspiaMetaDevClass.MetaDevNr
	espia_test = dev_list[0].getEspiaAcqLib()
	dev = espia_test.getEspiaDev(MetaDevNr)
	if dev:
		return dev
	meta_dev_class = meta_dev_class or EspiaMetaDevClass
	dev = meta_dev_class(dev_list)
	espia_test.setEspiaDev(MetaDevNr, dev)
	return dev


def test_focla(focla):
	dev = focla.getEspiaDev()
	elib = dev.getEspiaAcqLib()
	
	dev.framePostOp(['CHECK_NULL', 'CHECK_NULL'])

	fdim = CcdFrameDim(1024, 1024, 2)
	dev.bufferAlloc(512, 1, fdim)

	dev.bufferImageLive()

	focla.setTestImage(0)

	prefix = '/buffer/bestia2/tests/test_'
	idx = 600
	file_frames = 1
	overwrite = 1
	dev.autoSaveActive(prefix, idx, file_frames, overwrite)

	x = dev.autoSaveStatus()
	print "autoSaveStatus:", x
	
	print "acq_status:", dev.getAcqStatus()
	
	dev.setOption('NO_FIFO_RESET', 1)
	dev.startAcq(0, 0, 0)

	finfo = dev.getLastFrame()
	print "finfo:", finfo

	print "acq_status:", dev.getAcqStatus()
	
	focla.setTestImage(1)
	end = False
	timeout = time.time() + 3
	while not end:
		elib.imageRefresh()
		time.sleep(0.01)
		end = time.time() > timeout
		
	finfo = dev.getLastFrame()
	print "finfo:", finfo

	print "acq_status:", dev.getAcqStatus()
	
	dev.stopAcq()

	print "acq_status:", dev.getAcqStatus()
	
	t0 = time.time()
	for i in xrange(100):
		data2 = dev.oldBufferRead(2)
	t = time.time()
	print "old read_time:", (t - t0) / (i + 1)
	print "data2:"
	print data2

	t0 = time.time()
	for i in xrange(1000):
		data2 = dev.bufferRead(2)
	t = time.time()
	print "read_time:", (t - t0) / (i + 1)
	print "data2:"
	print data2

	data4 = dev.bufferRead(4)
	print "data4:"
	print data4

	print "data2 == data4:", data2 == data4
	
	data400 = dev.bufferRead(400)
	print "data400:"
	print data400

	x = dev.autoSaveStatus()
	print "autoSaveStatus:", x

	dev.autoSaveStop()

	x = dev.autoSaveStatus()
	print "autoSaveStatus:", x


def test_frelon(dev):
	ser_config = SerLineConfig()
	ser_config.LineTerm = '\r\n'
	ser_config.TimeoutMSec = 2000
	ser_line = dev.getSerLine(ser_config)

	ser_line.write('\r\n')
	print ser_line.readLine()
	ser_line.flushInput()
	
	ser_line.write('>C\r\n')
	time.sleep(2)

	print "available:", ser_line.getAvailableBytes()
	
	print ser_line.read(10000)


def main(argv):

#	CcdDebug.GlobalFlags.set(0xffffffff)
	
	elib = EspiaAcqLib(argv[1:])

	dev = EspiaDev(elib, 0)
	dev_copy = EspiaDev(elib, 0)
	print "dev_copy is dev:", dev_copy is dev

	try:
		focla = FoclaDev(dev)
	except:
		focla = None

	if focla:
		test_focla(focla)
	else:
		test_frelon(dev)
		
#	mdev = EspiaMetaDev([dev])


if __name__ == '__main__':
	main(sys.argv)
