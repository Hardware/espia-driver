#include <Python.h>
#include <numpy/arrayobject.h>
#include <stdio.h>
#include <structmember.h>
#include "espia_lib.h"


#define DRIVER_NAME	"Espia"

PyDoc_STRVAR(module_doc,
"Espia driver test module");

PyDoc_STRVAR(driver_doc,
"Class to access one espia board");

static PyObject *DriverError;

typedef struct {
    PyObject_VAR_HEAD

    espia_t	dev;
    int		dev_nr;
    int		block_size;
    int		block_delay;
    int 	timeout;
    int		xsize;
    int		ysize;
    int		depth;

} DriverObject;

static PyTypeObject DriverType = {
        PyObject_HEAD_INIT(NULL)
        0,
        DRIVER_NAME,
        sizeof(DriverObject),
        0,
        0,               /* tp_dealloc */
        0,               /* tp_print */
        0,               /* tp_getattr */
        0,               /* tp_setattr */
        0,               /* tp_compare */
        0,               /* tp_repr */
        0,               /* tp_as_number */
        0,               /* tp_as_sequence */
        0,               /* tp_as_mapping */
        0,               /* tp_hash */
        0,               /* tp_call */
        0,               /* tp_str */
        0,               /* tp_getattro */
        0,               /* tp_setattro */
        0,               /* tp_as_buffer */
        Py_TPFLAGS_DEFAULT,            /* tp_flags */
        driver_doc,                    /* tp_doc */
};

static PyObject* driver_new(PyTypeObject* type, PyObject* args, PyObject* kwds) {
    DriverObject* self;

    self= (DriverObject*)type->tp_alloc(type, 0);
    return (PyObject*)self;
}

static int driver_init(DriverObject *self, PyObject *args, PyObject *kwds) {
    int ret, dev_nr;

    if (!PyArg_ParseTuple(args, "i", &(dev_nr))) {
	return -1;
    }

    ret= espia_open(dev_nr, &(self->dev));
    if (ret<0) {
	PyErr_SetString(DriverError, "Cannot open device");
	return -1;
    }

    ret= espia_reset_link(self->dev);

    self->dev_nr= dev_nr;
    self->block_size= 1;
    self->block_delay= 1;
    self->timeout= 50000;
    self->xsize= -1;
    self->ysize= -1;
    self->depth= 2;
    return 0;
}

static void driver_dealloc(DriverObject *self, PyObject *args, PyObject *kwds) {
    int ret;

    ret= espia_close(self->dev);
    self->ob_type->tp_free((PyObject*)self);
}

PyDoc_STRVAR(drv_debug_doc,
"Set/Get driver debug level\n"
"[arg1]= Debug level [0=None, 3=Param]\n"
"Return current level\n");

static PyObject* driver_drv_debug(DriverObject* self, PyObject* args) {
    int lvl = -1;
    int ret;

    if (!PyArg_ParseTuple(args, "|i", &lvl)) {
	return NULL;
    }

    ret= espia_debug_level(self->dev, &lvl, 1);
    if (ret == ESPIA_OK) {
	return Py_BuildValue("i", lvl);
    }
    else {
	PyErr_SetString(DriverError, "Cannot set/get driver debug level");
	return NULL;
    }
}

PyDoc_STRVAR(lib_debug_doc,
"Set/Get library debug level\n"
"[arg1]= Debug level [0=None, 3=Param]\n"
"Return current level\n");

static PyObject* driver_lib_debug(DriverObject* self, PyObject* args) {
    int lvl = -1;
    int ret;

    if (!PyArg_ParseTuple(args, "|i", &lvl)) {
        return NULL;
    }

    ret= espia_debug_level(self->dev, &lvl, 0);
    if (ret == ESPIA_OK) {
        return Py_BuildValue("i", lvl);
    }
    else {
        PyErr_SetString(DriverError, "Cannot set/get library debug level");
        return NULL;
    }
}


PyDoc_STRVAR(serial_write_doc,
"Write on serial link\n"
"\t[arg1]= string to write\n"
"\t[arg2]= Wait until end of TX [0=No, 1=Yes]\n"
"\t       (Optionnal: default to 0)\n"
"Returns Number of bytes written");

static PyObject* driver_serial_write(DriverObject* self, PyObject* args) {
    char *cmd;
    int ret, block;
    unsigned long cmdlen;

    block= 1;

    if (!PyArg_ParseTuple(args, "s#|i", &cmd, &cmdlen, &block)) {
	return NULL;
    }
    ret= espia_ser_write(self->dev, cmd, &cmdlen, self->block_size, self->block_delay, block);

    if (ret == ESPIA_OK) {
	return Py_BuildValue("i", cmdlen);    
    }
    else {
	PyErr_SetString(DriverError, "Failed to write on serial link");
	return NULL;
    }
}
PyDoc_STRVAR(serial_block_doc,
"Set/Get Serial Write Options\n"
"[size]= nr. of bytes before a delay\n"
"[delay]= Time between writes, in us\n"
"Returns (size, delay)");

static PyObject* driver_serial_block(DriverObject* self, PyObject* args, PyObject* keywds) {
    static char *kwlist[]= {"size", "delay", NULL};

    if (!PyArg_ParseTupleAndKeywords(args, keywds, "|ii", kwlist, &(self->block_size), &(self->block_delay))) {
	return NULL;
    }

    return Py_BuildValue("(i,i)", self->block_size, self->block_delay);
}

PyDoc_STRVAR(serial_timeout_doc,
"Set/Get serial read timeout in us [0=Noblock, -1=Forever]");

static PyObject* driver_serial_timeout(DriverObject* self, PyObject* args) {

    if (!PyArg_ParseTuple(args, "|i", &(self->timeout))) {
	return NULL;
    }

    return Py_BuildValue("i", self->timeout);
}

PyDoc_STRVAR(serial_read_doc,
"Read on the serial link\n"
"[arg1]= number of bytes to read\n"
"Return buffer read or None");

static PyObject* driver_serial_read(DriverObject* self, PyObject* args) {
    char *buffer;
    int nrbytes, ret;
    PyObject* RetObj;

    if (!PyArg_ParseTuple(args, "i", &nrbytes)) {
	return NULL;
    }

    buffer= NULL;
    RetObj= NULL;
    if (nrbytes > 0) {
	buffer= calloc(nrbytes+1, 1);
	if (buffer == NULL) {
	    PyErr_SetString(DriverError, "Memory allocation failed");
            return NULL;
        }
        ret= espia_ser_read(self->dev, buffer, (unsigned long*)&nrbytes, (unsigned long)self->timeout);
        if (ret == ESPIA_OK) {
	    if ((nrbytes == 0) && (self->timeout != 0)) {
		PyErr_SetString(DriverError, "Timeout on serial line");
		return NULL;
            } 
	    else {
	        if (nrbytes == 0) {
		    if (buffer) free(buffer);
		    buffer= NULL;
                }
		if (buffer && nrbytes>0)
            	    buffer[nrbytes]= 0;
	    	return Py_BuildValue("z#", buffer, nrbytes);
	    }
	}
	else {
	    PyErr_SetString(DriverError, "Failed to read on serial link");
	    return NULL;
        }
    }
    else {
	PyErr_SetString(DriverError, "Need at least to read one byte");
        return NULL;
    }
}

PyDoc_STRVAR(serial_flush_doc,
"Flush the serial link\n");

static PyObject* driver_serial_flush(DriverObject* self, PyObject *args) {
    int ret;

    ret= espia_ser_flush(self->dev);
    if (ret != ESPIA_OK) {
        PyErr_SetString(DriverError, "Cannot flush serial link");
        return NULL;
    }
    Py_INCREF(Py_None);
    return Py_None;
}
   
PyDoc_STRVAR(driver_status_doc,
"Get status\n"
"[arg1] (optional)= timeout in us  [-1=BlockForever]");

static PyObject* driver_status(DriverObject* self, PyObject *args) {
    int ret;
    unsigned long timeout;
    unsigned char status;

    timeout= 0;
    if (!PyArg_ParseTuple(args, "|i", &timeout)) {
	return NULL;
    }

    ret= espia_ccd_status(self->dev, &status, timeout);
    if (ret != ESPIA_OK) {
	PyErr_SetString(DriverError, "Cannot get board status");
	return NULL;
    }
    
    return Py_BuildValue("i", (int)status);
}

PyDoc_STRVAR(driver_reset_link_doc,
"Reset the Aurora link\n");

static PyObject* driver_reset_link(DriverObject* self, PyObject *args) {
    int ret;

    ret= espia_reset_link(self->dev);
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(set_size_doc,
"Set/Get Frame size\n"
"[arg1] (optional)= xsize\n"
"[arg2] (optional)= ysize\n");

static PyObject* driver_set_size(DriverObject* self, PyObject *args) {

    if (!PyArg_ParseTuple(args, "|iii", &(self->xsize), &(self->ysize), &(self->depth))) {
	return NULL;
    }
    return Py_BuildValue("(i,i)", self->xsize, self->ysize);
}

PyDoc_STRVAR(start_acq_doc,
"Set/Get Frame size\n"
"[arg1] (optional)= number of frames\n"
"[arg2] (optional)= timeout in us  [-1=BlockForever]\n");

static PyObject* driver_start_acq(DriverObject* self, PyObject *args) {
    int ret;
    unsigned long fsize, nframe, timeout;

    timeout= 0;
    nframe= 1;

    if (!PyArg_ParseTuple(args, "|kk", &nframe, &timeout)) {
	return NULL;
    }
    // --- Check if size is defined
    if ((self->xsize==-1)||(self->ysize==-1)) {
	PyErr_SetString(DriverError, "Size is not properly defined");
	return NULL;
    }

    // --- Allocate buffer
    ret= espia_buffer_free(self->dev);
    if (ret != ESPIA_OK) {
	PyErr_SetString(DriverError, "Cannot free buffers");
	return NULL;
    }
    fsize= self->xsize*self->ysize*self->depth;
    ret= espia_buffer_alloc(self->dev, 1, nframe, fsize);
    if (ret != ESPIA_OK) {
	PyErr_SetString(DriverError, "Cannot allocate buffers");
	return NULL;
    }

    // --- Start acquisition
    ret= espia_start_acq(self->dev, 0, nframe, timeout);
    if (ret) {
	PyErr_SetString(DriverError, "Failed to start acquisition");
	return NULL;
    }

    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(stop_acq_doc,
"Stop acquisition\n");

static PyObject* driver_stop_acq(DriverObject* self, PyObject *args) {
    int ret;

    ret= espia_stop_acq(self->dev);
    if (ret != ESPIA_OK) {
	PyErr_SetString(DriverError, "Failed to stop acquisition");
	return NULL;
    }

    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(acq_active_doc,
"Is a acquisition running\n"
"Return 1 if yes, 0 otherwise\n");

static PyObject* driver_acq_active(DriverObject* self, PyObject *args) {
    int ret;
    int acquiring;
    unsigned long acq_run_nr;

    ret= espia_acq_active(self->dev, &acq_run_nr);
    acquiring= (ret!= 0);

    return Py_BuildValue("i", acquiring);
}

PyDoc_STRVAR(last_frame_doc,
"Get last acquired frame number\n");

static PyObject* driver_last_frame(DriverObject* self, PyObject *args) {
    int ret;
    struct img_frame_info efinfo;
    efinfo.buffer_nr= (unsigned long)-1;
    efinfo.frame_nr= (unsigned long)-1;
    efinfo.round_count= (unsigned long)-1;
    efinfo.acq_frame_nr= (unsigned long)-1;

    ret= espia_get_frame(self->dev, &efinfo, 0);
    return Py_BuildValue("l", efinfo.acq_frame_nr);
}

PyDoc_STRVAR(get_frame_doc,
"Get Frame as a Numeric array\n"
"[arg1] (optionnal) Frame number (default: last frame)\n");

static PyObject* driver_get_frame(DriverObject* self, PyObject *args) {
    int ret;
    unsigned long frame;
    void *addr;
    int type;

    PyArrayObject *PyData;
    int dim[2];

    frame= (unsigned long)-1;
    if (!PyArg_ParseTuple(args, "|i", &frame)) {
	return NULL;
    }

    // --- Get Frame address
    ret= espia_frame_address(self->dev, 0, frame, &addr);
    if (ret != ESPIA_OK) {
	PyErr_SetString(DriverError, "Cannot get frame address");
	return NULL;
    }

    // --- Create py array
    dim[0]= self->xsize;
    dim[1]= self->ysize;
    switch (self->depth) {
	case 2: type= NPY_USHORT; break;
	case 4: type= NPY_ULONG; break;
	default: type= NPY_UBYTE;
    }
	
    PyData= (PyArrayObject*)PyArray_SimpleNew(2, dim, NPY_USHORT);
    memcpy(PyArray_DATA(PyData), addr, dim[0]*dim[1]*self->depth);

    return (PyObject*)PyArray_Return(PyData);
}


static PyMemberDef driver_members[]= {
    {"dev_nr", T_INT, offsetof(DriverObject, dev_nr), READONLY, "Device Number"},
    {NULL},
};

static PyMethodDef driver_methods[]= {
    {"DrvDebug", (PyCFunction)driver_drv_debug, METH_VARARGS, drv_debug_doc},
    {"LibDebug", (PyCFunction)driver_lib_debug, METH_VARARGS, lib_debug_doc},
    {"SerialBlock", (PyCFunction)driver_serial_block, METH_VARARGS|METH_KEYWORDS, serial_block_doc},
    {"SerialWrite", (PyCFunction)driver_serial_write, METH_VARARGS, serial_write_doc}, 
    {"SerialTimeout", (PyCFunction)driver_serial_timeout, METH_VARARGS, serial_timeout_doc},
    {"SerialRead", (PyCFunction)driver_serial_read, METH_VARARGS, serial_read_doc}, 
    {"SerialFlush", (PyCFunction)driver_serial_flush, METH_NOARGS, serial_flush_doc},
    {"FrameSize", (PyCFunction)driver_set_size, METH_VARARGS, set_size_doc},
    {"StartAcq", (PyCFunction)driver_start_acq, METH_VARARGS, start_acq_doc},
    {"StopAcq", (PyCFunction)driver_stop_acq, METH_NOARGS, stop_acq_doc},
    {"AcqActive", (PyCFunction)driver_acq_active, METH_NOARGS, acq_active_doc},
    {"LastFrame", (PyCFunction)driver_last_frame, METH_NOARGS, last_frame_doc},
    {"GetFrame", (PyCFunction)driver_get_frame, METH_VARARGS, get_frame_doc},
    {"Status", (PyCFunction)driver_status, METH_VARARGS, driver_status_doc},
    {"ResetLink", (PyCFunction)driver_reset_link, METH_NOARGS, driver_reset_link_doc},
    {NULL, NULL, 0, NULL},
};

static PyMethodDef module_methods[]= {
    {NULL},
};

void initPyEspiaDriver(void) {

    PyObject* module;
    
    module= Py_InitModule3("PyEspiaDriver", module_methods, module_doc);

    import_array();

    DriverError= PyErr_NewException("PyEspiaDriver.EspiaError", NULL, NULL);
    if (DriverError == NULL)
	return;
    Py_INCREF(DriverError);
    PyModule_AddObject(module, "EspiaError", DriverError);

    DriverType.tp_new= driver_new;
    DriverType.tp_init= (initproc)driver_init;
    DriverType.tp_dealloc= (destructor)driver_dealloc;
    DriverType.tp_members= driver_members;
    DriverType.tp_methods= driver_methods;
    if (PyType_Ready(&DriverType)<0) return;
    Py_INCREF(&DriverType);
    PyModule_AddObject(module, DRIVER_NAME, (PyObject*)&DriverType);

}

