import sys
import time
import numpy
import exceptions
import ctypes

try:
    import espia_acq_lib
    print dir(espia_acq_lib)
except ImportError:
    print "Error importing module \"espia_acq_lib\""
    sys.exit(-1)



class EspiaTestException(Exception):
	def __init__( self, ret ):
		self.err_msg = espia_acq_lib.espia_test_strerror(ret)
		Exception.__init__(self, self.err_msg)



class EspiaTestLibProxy:

	CmdNames = {
		'_LibInit'         : 'lib_test_init',
		'_Cleanup'         : 'lib_test_cleanup',
		'Open'             : 'lib_open_dev',
		'FoclaOpen'        : 'lib_focla_open_dev',
		'FoclaSetParam'    : 'lib_focla_set_par',
		'FoclaGetParam'    : 'lib_focla_get_par',
		'BufferAlloc'      : 'lib_buffer_alloc',
		'BufferImage'      : 'lib_buffer_img',
		'SetOption'        : 'lib_set_option',
		'StartAcq'         : 'lib_start_acq',
		'AcqStatus'        : 'lib_acq_status',
		'SerialWrite'      : 'lib_serial_write',
		'SerialGetAvail'   : 'lib_serial_get_avail_bytes',
		'SerialRead'       : 'lib_serial_read',
		'SerialReadStr'    : 'lib_serial_read_str',
		'GetParam'         : 'lib_get_param',
		'SetParam'         : 'lib_set_param',
		'BufferImageRates' : 'lib_buffer_img_rates',
		'BufferImageNorm'  : 'lib_buffer_img_norm',
		'GetCardID'        : 'lib_get_card_id',
		'RegAcqCb'         : 'lib_reg_acq_cb',
		'CbActive'         : 'lib_cb_active',
		'PluginReg'        : 'lib_reg_plugin',
		'PluginActive'     : 'lib_plugin_active',

		'StopAcq'          : 'lib_stop_acq',
		'BufferClear'      : 'lib_buffer_clear',
		'BufferFree'       : 'lib_buffer_free',
		'FoclaClose'       : 'lib_focla_close_dev',
		'Close'            : 'lib_close_dev',
		'_EspiaGetOption'  : 'lib_espia_get_option_data',
		'_GetData'         : 'lib_get_data',
		'_GetFrameInfo'    : 'lib_get_frame_info',
		'_Show'            : 'lib_show'
	}

	NoDebugSet = (['_Cleanup', '_EspiaGetOption'])

	def __init__( self, etest, cmd ):
		self.cmd = cmd
		self.etest = etest
		self.fn_str = self.CmdNames[cmd]
		self.fn = getattr( espia_acq_lib, self.fn_str )

	def __call__(self, *args):
		print "Press ENTER to execute ", self.cmd, args,
		if self.etest is not None:
			args = tuple( [self.etest] + list(args) )
		print " <-> ", self.fn_str, args,
		if self.cmd not in self.NoDebugSet:
			raw_input()
		else:
			print
		ret_n_args = self.fn( *args ) # Calling the function
		if type(ret_n_args) == type(0): # Only ret?
			ret_n_args = [ret_n_args]
		return self.checkRet(*ret_n_args)

	def checkRet(self, ret, *args):
		if ret < 0:  # or "!= 0" ?
			raise EspiaTestException(ret)
		if len(args) > 1:
			return args
		elif len(args) == 1:
			return args[0]
		else:
			return None



class EspiaTest:

	def __init__(self, args=[]):
		self.etest = None
		LibInit = EspiaTestLibProxy(None, '_LibInit')
		self.etest = LibInit(args)
		self.active_dev = -1L # ???

	def __del__(self):
		if self.etest is not None:
			self._Cleanup()

	def __repr__(self):
		return '<EspiaTest with handle 0x%x>' % self.etest

	def __str__(self):
		return repr(self)

	def __getattr__(self, name):
		return EspiaTestLibProxy(self.etest, name) # add active_dev ???

	def Select(self, dev_nr):
		# do some checks...
		self.active_dev = dev_nr



def data_display( data, length ):
	if type(data) != numpy.ndarray:
		return -1

	if data.size > length: 
		l = length
	else:
		l = data.size

	for i in range(int(l/16)):
		min = 16*i
		max = 16*(i+1)
		if max > l: max = l-min
		print "data[%02x] :" % min,
		for j in range(min, max):
			print "%02x" % data[j],
		print ""

	return 0



def print_frame_info(name, frame):
	if frame is None:
		print "Frame \"%s\" is invalid" %name
		return -1
	print "%s:" %name
	if frame.buffer_ptr is not None:
		print "\tbuffer_ptr   = %x"  %(int(frame.buffer_ptr))
	else:
		print "\tbuffer_ptr   = None"
	print "\tbuffer_nr    = %lu" %frame.buffer_nr
	print "\tframe_nr     = %lu" %frame.frame_nr
	print "\tround_count  = %lu" %frame.round_count
	print "\tacq_frame_nr = %lu" %frame.acq_frame_nr
	print "\tacq_run_nr   = %lu" %frame.acq_run_nr
	print "\tpixels       = %lu" %frame.pixels
	print "\ttime_us      = %lu" %frame.time_us
	return 0



def image_refresh(etest):
#	print ".",
	return espia_acq_lib.lib_image_refresh(etest)



def pyCallback(dev_nr, cb_data):
	print "Python acquisition callback from dev %d!" % dev_nr
#	print "Type of cb_data:", type(cb_data)
	print_frame_info("req_finfo", cb_data.info.acq.req_finfo)
	print_frame_info("cb_finfo", cb_data.info.acq.cb_finfo)
	return 0



class acq_cb_class:
	def acq_cb_func(self, *args, **kw):
		return pyCallback(*args, **kw)



def pyOptionList(etest, dev):
	nropt = etest._EspiaGetOption(dev, -1, None)
	option = espia_acq_lib.lib_option()
	options = {}
	for opt in range(nropt):
		etest._EspiaGetOption(dev, opt, option)
		options[option.name] = option.attr
	return options



def main(argv):

	# Get EspiaLib version
	ver = espia_acq_lib.espia_lib_version()
	ret = espia_acq_lib.lib_get_version(ver)
	if ret < 0 :
		raise EspiaTestException(ret)
	print "EspiaLib version:"
	print "  Compiled version: %s" % ver.compile
	print "  Running  version: %s" % ver.run

	# Test init. What about selecting if we pass devices to Open ???
	etest = EspiaTest(argv)

	# "Open 0"
	etest.Open(0)

	# "Select 0" - set etest.active_dev
	etest.Select(0)

	# Debug print to see if the device is open
	etest._Show(etest.active_dev)

	# "FoclaOpen"
	etest.FoclaOpen(etest.active_dev)

	# "FoclaSetParam TEST_IMAGE 0"
	old_val = etest.FoclaSetParam(etest.active_dev, 'TEST_IMAGE', 0)
	print "Previous value : %d" % old_val

	# "FoclaGetParam TEST_IMAGE"
	old_val = etest.FoclaGetParam(etest.active_dev, 'TEST_IMAGE')
	print "Current value : %d" % old_val

	# "OptionList"
	options = pyOptionList(etest, etest.active_dev)
	i = 0
	print "Number of options =", len(options)
	for name in options:
		print "  option number %d:" % i
		print "    option.name =", name
		print "    option.attr =", options[name]
		i = i + 1

	# "PluginReg 1 aplugin"
	plugins_lib = ctypes.CDLL( '../src/libacqplugin.so' )
	aptr = ctypes.addressof(ctypes.c_void_p.in_dll(plugins_lib, 'aplugin'))
	print "aplugin address = %x" % aptr
	etest.PluginReg( etest.active_dev, 1, aptr, 1, 'aplugin' );

	# "BufferAlloc 128 1 1024 1024 2 0"
	etest.BufferAlloc(etest.active_dev, 128, 1, 1024, 1024, 2, 0)

	# "BufferImage -1"
	etest.BufferImage(etest.active_dev, -1)

	# Image window refresh
	image_refresh(etest.etest)

	# "SetOption NO_FIFO_RESET 1"
	etest.SetOption(etest.active_dev, 'NO_FIFO_RESET', 1)

	# "SerialWrite ">C\r\n" 0 0 0"
	wr_buf = ">C\r\n"
	wr_bytes = len(wr_buf)
	if wr_bytes > 0: action = "Transmited"
	else: action = "Still to TX:"
	wr_bytes = etest.SerialWrite( etest.active_dev, wr_buf, wr_bytes, \
	                              0, 0, 0 )
	print "%s %ld bytes." % ( action, wr_bytes )

	# Get number of bytes available on the serial line <-> "SerialRead 0 0 0"
	nr_bytes = etest.SerialGetAvail( etest.active_dev )
	print "Number of bytes available on the serial line: %d" % nr_bytes

	# "SerialRead 10000 0 0"
	nr_bytes = 10000
	buf = etest.SerialRead( etest.active_dev, nr_bytes, 0 )
	nr_bytes = len(buf)
	print "Read %d bytes in the buf" % nr_bytes
	if nr_bytes:
		print "buf :", buf

	# "SerialReadStr 1024 "HARDWARE TRIGGER" 0 0"
	nr_bytes = 1024
	buf = etest.SerialReadStr( etest.active_dev, nr_bytes, \
	                                                  "HARDWARE TRIGGER", 0)
	nr_bytes = len(buf)
	print "Read %d bytes in the buf" % nr_bytes
	if nr_bytes:
		print "buf :", buf

	# "GetParam DO_NOT_RESET_FIFOS"
	no_fifo_reset = etest.GetParam( etest.active_dev, "DO_NOT_RESET_FIFOS" )
	print "DO_NOT_RESET_FIFOS = %d = 0x%x" % (no_fifo_reset, no_fifo_reset)

	# "SetParam DO_NOT_RESET_FIFOS ~"
	#no_fifo_reset = etest.SetParam( etest.active_dev, "DO_NOT_RESET_FIFOS",\
	#                                int(not no_fifo_reset) )
	#print "DO_NOT_RESET_FIFOS was = %d" % no_fifo_reset

	# "GetCardID"
	card_id = espia_acq_lib.scdxipci_card_id()
	firmware = espia_acq_lib.espia_firmware()
	checksum = etest.GetCardID( etest.active_dev, card_id, firmware )
	ser_nr = card_id.NumeroSerie;
	print "Serial Number:     %ld" % ser_nr
	print "CRC:               0x%08x" % card_id.CRC
	print "Firmware Checksum: 0x%04x" % checksum
	if firmware.name is not None :
		print "Firmware Name:     %s" % str(firmware.name)
	else :
		print "Firmware Name:     Unknown"
	print "card_id.libre : ", card_id.libre
	libre = numpy.frombuffer(numpy.int_asbuffer(card_id.libre.__int__(), \
	                         64*numpy.dtype(numpy.int32).itemsize), \
	                         dtype=numpy.int32)
	print libre

	# "RegAcqCb 1 -1 -1 -1 -1"
	acq_cb_obj = acq_cb_class()
	acq_cb_fn = acq_cb_obj.acq_cb_func
	etest.RegAcqCb(etest.active_dev, 1, -1, -1, -1, -1, \
	               espia_acq_lib.swig_cb_funct, id(acq_cb_fn))

	# "CbActive 2 1"
	etest.CbActive(etest.active_dev, 2, 1)

	# "StartAcq 0 0 0"
	etest.StartAcq(etest.active_dev, 0, 0, 0)

	# Image window refresh
	image_refresh(etest.etest)

	# "FoclaSetParam TEST_IMAGE 1"
	old_val = etest.FoclaSetParam(etest.active_dev, 'TEST_IMAGE', 1)
	print "Previous value : %d" % old_val

	# Image window refresh
	for i in range(30):
		image_refresh(etest.etest)
		time.sleep(0.5)

	# "CbActive 2 0"
	etest.CbActive(etest.active_dev, 2, 0)

	# "PluginActive 1 1 0"
	etest.PluginActive( etest.active_dev, 1, 1, 0 )

	# "AcqStatus"
	last_acq_frame = espia_acq_lib.img_frame_info()
	last_saved_frame = espia_acq_lib.img_frame_info()
	acq_run_nr, acq_active, auto_save_active = \
	   etest.AcqStatus( etest.active_dev, last_acq_frame, last_saved_frame )
	print "Acquisition number %d" % acq_run_nr, " is ",
	if acq_active: print "active." 
	else: print "not active."
	print "Auto save active: %d" % auto_save_active
	print_frame_info("last_acq_frame", last_acq_frame)
	print_frame_info("last_saved_frame", last_saved_frame)

	# Get some data
	data = None
	frame_info = espia_acq_lib.img_frame_info()
	frame_dim = espia_acq_lib.espia_frame_dim()
	while 1:
		buf_nr = raw_input("Enter buffer number (or just ENTER to exit) : ")
		if buf_nr == "": 
			print ""; break
		else:
			buf_nr = int(buf_nr)
		frame_nr = int(raw_input("Enter frame number : "))
		data = etest._GetData( etest.active_dev, buf_nr, frame_nr )
		print "data.size =", data.size
		data_display( data, 128 )
		if frame_nr >= 0:
			etest._GetFrameInfo( etest.active_dev, buf_nr, 
			                     frame_nr, frame_dim, frame_info )
			print_frame_info("Frame info", frame_info)
			print "Frame dimensions :"
			print "\twidth  = %d" %frame_dim.width
			print "\theight = %d" %frame_dim.height
			print "\tdepth  = %d" %frame_dim.depth

	# "BufferImageRates 0"
	#name, update, refresh = etest.BufferImageRates( etest.active_dev, 0 )
	#print "%s: update rate: %.1f, refresh rate: %.1f\n" % (name, update, refresh)

	# "BufferImageNorm 0 -1 -1 -1"
	#name, min_val, max_val, auto_range = etest.BufferImageNorm ( \
	#                                       etest.active_dev, 0, -1, -1, -1 )
	#print "%s norm.: min-val: %ld, max-val: %ld, auto-range: %d\n" \
	#                                  % (name, min_val, max_val, auto_range)

	# "StopAcq"
	etest.StopAcq(etest.active_dev)

	# Image window refresh
	image_refresh(etest.etest)

	# "BufferClear -1"
	etest.BufferClear( etest.active_dev, -1 )

	# Image window refresh
	image_refresh(etest.etest)

	# "BufferFree"
	etest.BufferFree(etest.active_dev)

	# "FoclaClose"
	etest.FoclaClose(etest.active_dev)

	# "Close"
	etest.Close(etest.active_dev)



if __name__ == '__main__':
	main(sys.argv)

